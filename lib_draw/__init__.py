"""
Requiert que p5 soit importer dans le fichier important la bibliothèque (avant ou après
l'import d'abr_lib, peu importe. Thanks pyodide).
"""

from .afficheur_compact import AffichageAbCompact, AffichageAbrCompact

__all__ = [
    'AffichageAbCompact',
    'AffichageAbrCompact',
]