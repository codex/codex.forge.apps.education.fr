
# pylint: disable=C0103, C0116, C0115, E0401, E0102, C0415
from lib_draw._base_AB_afficheur import AffichageAbBase


class AffichageAbCompact(AffichageAbBase):

    def calcul_positions(self, arbre, y0):

        def rec(tree, h):
            nonlocal x, H
            if not self.est_vide(tree):
                H = max(H,h)
                rec(self.gauche(tree), h+1)
                dct[tree] = (h,x)
                x += 1
                rec(self.droit(tree), h+1)

        x = H = 0
        self.positions = dct = {}
        rec(arbre, H)

        dh = min( 1.5*self.rayon, (700 - y0 - 2*self.rayon) / (H+1))
        dl = min(self.rayon * 1.5, (750 - 2*self.rayon) / (x or 1))
        x0 = (750 - x*dl) / 2
        dh,dl,x0 = (round(v,1) for v in (dh,dl,x0))

        for tree,(h,x) in dct.items():
            h = y0 + h*dh
            x = x0 + x*dl
            dct[tree] = x,h





class AffichageAbrCompact(AffichageAbCompact):

    def insere_et_affiche(self, lst, abr=None):
        """
        Insère toutes les valeurs de la liste dans l'abr passé en argument (ou en crée un
        nouveau si non fourni), puis trace l'ABR résultant dans la figure, avec un message
        indiquant l'ordre d'insertion des noeuds, et indiquant éventuellement sur quelle
        valeur insérée l'insertion a échoué si une erreur est levée.
        @returns: l'ABR après les insertions.
        """
        done = 'insérés' if not abr or self.est_vide(abr) else "ajoutés"
        i,fail = 0, True
        abr = abr or self.creer_vide()
        try:
            for elt in lst:
                self.insere(abr, elt)
                i += 1
            fail = False
        finally:
            inserted = repr(lst[:i])
            if fail:
                title = f"{done.title()} avec succès : {inserted}\nErreur en insérant {lst[i]}"
            else:
                title = f"Éléments {done} : {inserted}"
            self.changer_arbre(abr, title)

        return abr
