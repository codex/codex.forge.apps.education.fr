
# pylint: disable=C0103, C0116, C0115, E0401, E0102, C0415
import p5, js
from operator import attrgetter



class AffichageAbBase(p5.Sketch):
    """
    Classe abstraite servant de base pour l'affichage d'un arbre binaire quelconque.
    Le constructeur de cette classe prend en charge la définition de toutes les propriétés
    spécifiques à l'Afficheur et lance l'animation automatiquement lors de l'instantiation.

    Les classes héritant de AffichageAbBase doivent implanter la méthode `calcul_positions`,
    qui doit définir le dictionnaire des positions (dict[arbre,(x,y)] de tous les noeuds
    dans le canevas.
    Il est possible de surcharger d'autres méthodes si besoin, notamment `get_coords` si les
    données stockées dans `positions` ne sont plus celles attendues.
    """

    def __init__(
        self,
        arbre = None,
        *,
        gauche      = None,
        droit       = None,
        valeur      = None,
        est_vide    = None,
        insere      = None,
        creer_vide  = None,
        target_id   = None,
        stop_others = True,
        taille_fenetre = (750, 500),
    ):
        self.arbre      = arbre
        self.gauche     = gauche   or attrgetter("gauche")
        self.droit      = droit    or attrgetter("droit")
        self.valeur     = valeur   or attrgetter("valeur")
        self.est_vide   = est_vide or (lambda ab: ab is None or ab.est_vide())
        self.insere     = insere   or (lambda ab,v: ab.insere(v))
        self.creer_vide = creer_vide or (lambda: None)

        self.taille_fenetre = taille_fenetre
        self.rayon = 40
        self.text_font = "Calibri"
        self.text_size = 20
        self.title_weight = 0.2
        self.node_weight = 1
        self.line_weight = 3
        self.coul = 255
        self.fond = (43,42,51)
        self.fill_col = 255

        self.title = ""
        self.was_dark = js.isDark()
        self.positions = {}

        self.run(target=target_id, stop_others=stop_others)


    # -------------------------------------------------------------------


    def changer_arbre(self, arbre, title:str=""):
        """
        Point d'entrée générique: met à jour le canevas pour y tracer l'arbre binaire pass" en
        argument, avec éventuellement un titre additionnel.
        """
        self.title = title
        self.arbre = arbre
        self._draw()

    def calcul_positions(self, arbre, y0:int) -> None:
        """
        Met à jour le contenu de self.positions (dict[arbre,(x,y)]) en calculant les positions
        de tous les "noeuds" de l'arbre dans le canevas. Doit tenir compte des problématiques
        de centrage, largeur/hauteur du/dans le canevas.

        @arbre: racine de l'arbre à dessiner
        @y0: hauteur initiale de la racine dans le canevas
        """
        raise NotImplementedError()


    # -------------------------------------------------------------------


    def _pick_colors(self):
        if js.isDark():
            self.coul = 255
            self.fond = (43,42,51)
            self.fill_col = 255
        else:
            self.coul = 0
            self.fond = (255,)
            self.fill_col = 0

    @p5.hook('draw')
    def _track_light_mode(self):
        now = js.isDark()
        if self.was_dark != now:
            self.was_dark = now
            self._draw()

    def get_coords(self, arbre):
        return self.positions[arbre]


    def _afficher_noeud(self, arbre):
        x,y = self.get_coords(arbre)
        self.p5.strokeWeight(self.line_weight)
        self.p5.fill(*self.fond)
        self.p5.circle(x, y, self.rayon)
        self.p5.strokeWeight(self.node_weight)
        self.p5.fill(self.fill_col)
        self.p5.text(str(self.valeur(arbre)), x, y)

    def _affiche_lien(self, noeud, parent=None):
        if parent:
            x,y = self.get_coords(parent)
            i,j = self.get_coords(noeud)
            self.p5.strokeWeight(self.line_weight)
            self.p5.line(x, y, i, j)

    def _afficher_arbre(self, arbre, parent=None):
        if not self.est_vide(arbre):
            self._affiche_lien(arbre, parent)
            self._afficher_arbre(self.gauche(arbre), arbre)
            self._afficher_arbre(self.droit(arbre), arbre)
            self._afficher_noeud(arbre)


    @p5.hook('setup')
    def _setup(self):
        self.p5.createCanvas(*self.taille_fenetre)
        self.p5.textFont(self.text_font)
        self.p5.textSize(self.text_size)
        self.p5.textAlign(self.p5.CENTER, self.p5.CENTER)
        self.p5.strokeWeight(1)


    def _draw(self):
        # Remise à zéro
        self._pick_colors()
        self.p5.stroke(self.coul)
        self.p5.background(*self.fond)
        self.p5.strokeWeight(self.title_weight)
        self.p5.fill(self.fill_col)

        x,y = self.taille_fenetre[0]//2, self.rayon + 10
        if self.title:
            self.p5.text(self.title, x, y)
            y += self.rayon

        if self.est_vide(self.arbre):
            self.p5.text("(Arbre vide...)", x, y)

        else:
            self.p5.fill(*self.fond)
            self.calcul_positions(self.arbre, y)
            self._afficher_arbre(self.arbre)
