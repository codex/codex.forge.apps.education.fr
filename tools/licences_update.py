from pathlib import Path


# pylint: disable-next=all
"""
# To use in codex_hooks.py fo find the files and build FCVB.txt:

TO_CHANGE = []

def on_page_markdown(
    markdown:str,
    page:Page,
    config:MkDocsConfig,
    **kwargs
):
    meta = page.meta
    if not meta or 'author' not in meta:
        return

    author = meta['author']
    if isinstance(author,str):
        author = [author]
    txt = ' '.join(author).lower()
    got = "bouillot" in txt or "chambon" in txt
    if got:
        TO_CHANGE.append(f'docs/{Path(page.file.src_uri)} | { txt }')


# Also needs in on_post_build:

    file = Path('FCVB.txt')
    file.touch(exist_ok=True)
    file.write_text('\n'.join(map(str, TO_CHANGE)), encoding='utf-8')
"""




SRC = Path('FCVB.txt')
exos = [*map(Path, SRC.read_text(encoding='utf-8').strip().splitlines())]

DRY_RUN = False


for path in exos:
    md = path.read_text(encoding='utf-8')
    assert md.startswith('---\n')
    if DRY_RUN: continue

    md = '---\nlicense: "by-nc-sa"\n' + md[4:]
    path.write_text(md, encoding='utf-8')
