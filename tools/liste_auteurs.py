import markdown
from pathlib import Path
import os



sujets = {
    f"{path}/{file}" : {} for path,dirs,files in os.walk("../docs")
        for file in files
        if file=='index.md' and "exam" not in path
}

auteurs = {}
seul_auteur = {}
premier_auteur = {}
for adresse in sujets:
    try:
        with open(adresse, "r", encoding="utf-8") as fichier_énoncé:
            text = fichier_énoncé.read()
        md = markdown.Markdown(extensions=["meta"])
        md.convert(text)
        metas = md.Meta             # pylint: disable=no-member
        if "author" in metas:
            sujets[adresse]["author"] = [a[2:] if a[0] == "-" else a for a in metas["author"] if a]
            if len(sujets[adresse]["author"]) == 1:
                a = sujets[adresse]["author"][0]
                auteurs[a] = auteurs.get(a, 0) + 1
                seul_auteur[a] = seul_auteur.get(a, 0) + 1
            else:
                a = sujets[adresse]["author"][0]
                premier_auteur[a] = premier_auteur.get(a, 0) + 1
                auteurs[a] = auteurs.get(a, 0) + 1
                for i in range(1, len(sujets[adresse]["author"])):
                    a = sujets[adresse]["author"][i]
                    auteurs[a] = auteurs.get(a, 0) + 1
    except FileNotFoundError:
        pass

    
print("Nombre total d'exercices (en travaux et validés)\n")
print(len(sujets))

print("\nListe des auteurs des sujets\n")
for k, v in sujets.items():
    titre = k.split("\\")[-1][:-9]
    adresse = k.split("\\")
    statut = adresse[1] if len(adresse) > 1 else "autre"
    print(f"{titre} ({statut}): {v.get('author', [])}")
    # print(f"{titre} : {v.get('author', [])}")

print("\nDécompte des exercices signés par un auteur (seul ou parmi d'autres)\n")
for n, a in sorted(auteurs.items(), key=lambda t: t[1], reverse=True):
    print(f"{a} : {n}")

print("\nDécompte des exercices dans lequel l'auteur est le 1er auteur parmi d'autres\n")
for n, a in sorted(premier_auteur.items(), key=lambda t: t[1], reverse=True):
    print(f"{a} : {n}")

print("\nDécompte des exercices dans lequel l'auteur est seul auteur\n")
for n, a in sorted(seul_auteur.items(), key=lambda t: t[1], reverse=True):
    print(f"{a} : {n}")
    



print("\nExercice dans lesquels le nom de Franck apparaît\n")
total = 0
for k, v in sujets.items():
    titre = k.split("\\")[-1][:-9]
    adresse = k.split("\\")
    statut = adresse[1] if len(adresse) > 1 else "autre"
    if "Franck Chambon" in v.get('author', []):
        # print(f"{titre} ({statut})")
        total += 1
print(f"\n-> {total}\n")

print("\nExercice dans lesquels Franck est le premier auteur\n")
total = 0
for k, v in sujets.items():
    titre = k.split("\\")[-1][:-9]
    adresse = k.split("\\")
    statut = adresse[1] if len(adresse) > 1 else "autre"
    if 'author' in v and "Franck Chambon" == v['author'][0]:
        # print(f"{titre} ({statut})")
        total += 1
print(f"\n-> {total}\n")
    
print("\nExercice dans lesquels Franck est le seul auteur\n")
total = 0
for k, v in sujets.items():
    titre = k.split("\\")[-1][:-9]
    adresse = k.split("\\")
    statut = adresse[1] if len(adresse) > 1 else "autre"
    if 'author' in v and len(v['author']) == 1 and "Franck Chambon" == v['author'][0]:
        # print(f"{titre} ({statut})")
        total += 1
print(f"\n-> {total}\n")
    
