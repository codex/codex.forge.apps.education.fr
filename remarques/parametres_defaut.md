??? abstract "Paramètres par défaut"
    Il est possible de définir des valeurs par défaut pour les paramètres d'une fonction :
    ```python
    def ma_fonction(a, b = 0, c = 5):
        ...
    ```

    Lors de l'appel de la fonction, on peut mettre autant d'attributs que de paramètres :
    ```python
    >>> ma_fonction(5, 3, 2):
        ...
    ```
    mais on peut aussi ne préciser que le paramètre :
    ```python
    >>> ma_fonction(5):
        ...
    ```
    Dans ce cas, les paramètres `b` et `c` prendront leur valeur par défaut.