??? tip "Rappels sur la :turtle:"

    On rappelle ci-dessous les instructions utiles du module [`turtle`](https://docs.python.org/3/library/turtle.html).
    
    <center>
    
    | Appel           | Rôle                                                                                                      |
    | :-------------- | :-------------------------------------------------------------------------------------------------------- |
    | `hideturtle()`  | Cache la tortue.                                                                                          |
    | `speed(n)`      | Définit la vitesse de l'animation.<br>`n` est un entier entre `#!py 1` (lent) et `#!py 10` (rapide).      |
    | `animation(s)`  | Autorise ou non les animations.<br>`s` est soit `#!py 'on'` (avec animations, valeur par défaut)<br> soit `#!py 'off'` (sans animations).      |
    | `penup()`       | Lève le crayon : les déplacements de la tortue ne sont plus dessinés.                                     |
    | `pendown()`     | Baisse le crayon : les déplacements de la tortue sont dessinés.                                           |
    | `heading()`     | Renvoie la direction vers laquelle pointe la tortue sous la forme d'une mesure d'angle en degrés.         |
    | `setheading(d)` | Définit la direction vers laquelle pointe la tortue.<br>`#!py d` est une mesure d'angle en degrés.        |
    | `position()`    | Renvoie la position de la tortue sous la forme d'un couple de nombres `#!py (x, y)`.                      |
    | `goto(x, y)`    | La tortue se déplace à la position `#!py (x, y)`.<br>`#!py x` et `#!py y` sont des nombres.               |
    | `left(a)`       | La tortue tourne sur elle-même vers la gauche de `#!py a` degrés.<br>`#!py a` est un nombre.              |
    | `right(a)`      | La tortue tourne sur elle-même vers la droite de `#!py a` degrés.<br>`#!py a` est un nombre.              |
    | `forward(p)`    | La tortue avance de `#!py p` pixels.<br>`#!py p` est un nombre.                                           |
    
    </center>
    
    :race_car: Dans CodEx, il est possible d'augmenter la vitesse jusqu'à `#!py speed(100)` ! :race_car: