??? warning "Définition des méthodes"
    Pour pouvoir fragmenter l'activité en plusieurs exercices, vous serez amenés à redéfinir une méthode en dehors de la définition de sa classe. Un mécanisme du site permet cette manipulation.

    En dehors de cette activité, les méthodes doivent être définies en même temps que le reste de la classe.