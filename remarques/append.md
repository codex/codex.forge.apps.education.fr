??? note "`#!py append` ?"

    La méthode `#!py append` est utilisée en Python afin d'ajouter un élément à la fin d'une liste.
    
    On a par exemple :
    
    ```pycon title=""
    >>> nombres = [3, 4]
    >>> nombres.append(5)
    >>> nombres
    [3, 4, 5]
    >>> nombres.append(6)
    >>> nombres.append(7)
    >>> nombres
    [3, 4, 5, 6, 7]
    ```