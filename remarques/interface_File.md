??? note "Interface de la classe `File`"

    * `f = File()` : crée une file vide et l'affecte à la variable `#!py f`;
    
    * `f.est_vide()` : renvoie le booléen indiquant si la file `#!py f` est vide ;
    * `f.enfile(x)` : enfile l'élément `#!py x` dans la file `#!py f`;
    * `f.defile()` : défile un élément de la file `#!py f` si elle n'est pas vide et le renvoie. Provoque une erreur si la file est vide.