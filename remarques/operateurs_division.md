??? note "Opérateurs de la division entière"

    L'opérateur `//` permet de calculer la division entière entre deux nombres entiers, à savoir le quotient de leur division euclidienne.

    ```pycon title=""
    >>> 10 // 4
    2
    ```

    L'opérateur `%` permet de trouver le reste de la division entière entre deux nombres.
    
    ```pycon title=""
    >>> 10 % 3
    1
    ```