??? note "Racine carrée d'un nombre"

    La fonction permettant de calculer la racine carrée d'un nombre positif fait partie du module `math`. Elle est importée au début de cette version de l'exercice : `from math import sqrt`.

    $\sqrt{5}$ s'obtient avec `sqrt(5)`

    La valeur renvoyée est bien souvent une valeur approchée du résultat. Regardons par exemple ${\sqrt{3}}^2$
    ```pycon
    >>> sqrt(3)**2
    2.9999999999999996
    ```

    La fonction `sqrt` renvoie un nombre flottant (type float) : 
    ```pycon
    >>> sqrt(16)
    4.0
    ```



