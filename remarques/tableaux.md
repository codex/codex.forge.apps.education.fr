??? abstract "Liste ou tableau"
    Un **tableau** est une structure de données représentant une séquence **finie** d'éléments auxquels on peut accéder efficacement par leur position, ou indice, dans la séquence.

    Le nombre d'éléments d'un tableau est fixe et déterminé lors de l'initialisation du tableau. Contrairement à un tableau, il est possible de rajouter des éléments à une **liste**.

    En Python, les tableaux et les listes sont implémentées par le type `list`.

    ```python title = ""
    # Initialisation par affectation
    tableau_1 = [9, 4, 3, 0, 7]

    # Initialisation par concaténation
    tableau_2 = [0] * 6
    ```

