??? note "`#!py range` ?"

    La fonction `#!py range` permet de générer une série de nombres entiers. On l'utilise régulièrement dans les boucles `#!py for`. Elle a trois syntaxes différentes :
    
    * `#!py range(fin)` : génère les entiers compris entre `#!py 0` (inclus) et `#!py fin` (exclu). **On répète donc les instructions `fin` fois** ;
    
    * `#!py range(debut, fin)` : génère les entiers compris entre `#!py debut` (inclus) et `#!py fin` (exclu) ;
    
    * `#!py range(debut, fin, pas)` : génère les entiers compris entre `#!py debut` (inclus) et `#!py fin` (exclu) en les parcourant de `pas` en `pas`.

    Donc :
    
    * `#!py for i in range(5)` : parcourt les entiers `#!py i` dans `#!py [0, 1, 2, 3, 4]` ;
    
    * `#!py for i in range(1, 5)` : parcourt les entiers `#!py i` dans `#!py [1, 2, 3, 4]` ;
    
    * `#!py for i in range(1, 5, 3)` : parcourt les entiers `#!py i` dans `#!py [1, 4]`.