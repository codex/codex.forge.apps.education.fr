??? note "`#!py assert` ?"

    Le mot clé `#!py assert` est utilisé en Python afin de vérifier que des propositions sont vraies.
    
    Ainsi, l'instruction `#!py assert 3 + 5*7 == 38` permet de vérifier que l'expression `#!py 3 + 5*7` est bien évaluée à `#!py 38`.
    
    Si c'est le cas, le programme continue de se dérouler normalement. Dans le cas contraire, le programme est interrompu et une erreur est signalée.

    <center>
    <iframe title="CodEx - assertions" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/a5af0a1d-7adc-4e5c-842f-3e622ba1ecac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </center>
    