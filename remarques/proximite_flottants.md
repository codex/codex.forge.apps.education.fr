??? warning "Comparaison de nombres flottants"

    Lorsqu'on écrit `a = x` ou `x` est un nombre réel, la valeur de `a` enregistrée en machine est une valeur approchée de `x` (quelques fois la valeur exacte).
    Cette valeur approchée a la forme d'un nombre flottant (le type `#!py float` en Python). En conséquence, alors que des calculs et des comparaisons peuvent être effectués de manière exacte sur des réels, ils ne le sont que de manière approchée sur leur représentation en machine.
    On peut donc obtenir par exemple, avec `a = x` et `b = y`, l'expression `a == b` évaluée à `True` alors que `x` et `y` sont différents.
    
    C'est pourquoi les tests ne vérifient pas l'égalité des résultats et des valeurs attendues mais leur proximité.
    
    Ainsi, on peut vérifier que $\sqrt{2} \approx 1,414214$ en faisant`#!py assert abs(1.414214 - sqrt(2)) < 1e-6`. Ce test vérifie que les deux valeurs sont proches à $10^{-6}$ près.
