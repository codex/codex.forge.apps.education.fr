??? note "La classe `File`"

    ```python title=""
    from collections import deque


    class File:
        """Classe définissant une structure de file"""
        
        def __init__(self):
            self.valeurs = deque([])

        def est_vide(self):
            """Renvoie le booléen True si la file est vide, False sinon"""
            return len(self.valeurs) == 0

        def enfile(self, x):
            """Place x à la queue de la file"""
            self.valeurs.appendleft(x)

        def defile(self):
            """Retire et renvoie l'élément placé à la tête de la file.
            Provoque une erreur si la file est vide
            """
            if self.est_vide():
                raise ValueError("La file est vide")
            return self.valeurs.pop()
        
        def __repr__(self):
            """Convertit la file en une chaîne de caractères"""
            if self.est_vide():
                return "∅"
            return f"(queue) {' -> '.join(str(x) for x in self.valeurs)} (tête)"
    ```