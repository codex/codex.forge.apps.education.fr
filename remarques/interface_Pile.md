??? note "Interface de la classe `Pile`"

    * `p = Pile()` : crée une pile vide et l'affecte à la variable `#!py p`;
    
    * `p.est_vide()` : renvoie le booléen indiquant si la pile `#!py p` est vide ;
    * `p.empile(x)` : empile l'élément `#!py x` dans la pile `#!py p`;
    * `p.depile()` : dépile un élément de la pile `#!py p` si elle n'est pas vide et le renvoie. Provoque une erreur si la pile est vide.