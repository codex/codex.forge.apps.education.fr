??? tip "Fonctions ou méthodes avec types"

    Python n'est pas un langage à typage fort, mais il est possible de renseigner les types des données manipulées, à titre informatif.

    Ils sont intéressants notamment dans les signatures de fonctions ou de méthodes, pour rappeler succinctement quels sont les types des données attendus en arguments, ainsi que le type de ce que la fonction renvoie.


    Les deux signatures suivantes sont strictement équivalentes, la seconde ajoutant les informations de typage :

    ```python
    def compteur(lst):
        ...

    def compteur_avec_type(lst: list[int]) -> dict[int,int] :
        ...
    ```

    * `lst: list[int]` indique que l'argument `lst` est une liste python d'entiers.
    * La flèche après les parenthèses définissant les arguments, `->`, permet d'indiquer le type des données renvoyées par la fonction. Ici, il s'agit de `dict[int,int]`, soit un dictionnaire avec des entiers en clés et en valeurs.

    <br>

    Il peut aussi arriver que l'on trouve des signatures avec `None` en type de renvoi :

    ```python
    def mélange_liste(lst: list) -> None:
        ...
    ```
    :warning: Cette annotation `-> None` indique en fait que la fonction **ne renvoie rien**.
    <br>Comme son nom le suggère, cette fonction a à priori pour but de mélanger une liste en place, par mutation. Elle ne comporte donc __pas__ de `return`.