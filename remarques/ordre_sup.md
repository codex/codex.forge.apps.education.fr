??? note "Fonction d'ordre supérieur"
    Une des caractéristique du paradigme de **programmation fonctionnelle** est de considérer une fonction comme n'importe quelle variable. 

    Une fonction peut ainsi être un paramètre d'une autre fonction.

    On appelle **fonction d'ordre supérieur** une fonction qui a comme paramètre une autre fonction.