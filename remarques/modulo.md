??? note "L'opérateur modulo : `#!py %`"

    L'opérateur `#!py %` permet de trouver le reste de la division euclidienne entre deux nombres.

    `#!py a % b` vaut le reste de la division euclidienne de `a` par `b`.

    Par exemple Le reste de la division euclidienne de $13$ par $3$ est $1$ car $13=3 \times 4 + 1$
    
    ```pycon title=""
    >>> 13 % 3
    1
    ```