??? tip "Fonction `#!py randint(a, b)`"
    La fonction `#!py randint(a, b)` du module `random` renvoie un entier aléatoire compris entre les nombres `a` et `b` compris l'un et l'autre.

    ```pycon
    >>> from random import randint
    >>> randint(0, 5)
    3
    ```