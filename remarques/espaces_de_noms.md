Le QCM vous permet de vérifier votre compréhension du fonctionnement des espaces de noms dans Python (c'est-à-dire le fonctionnement des définitions et utilisations des noms de variables ou de fonctions, en Python. En anglais, on parle de "scopes").

A chaque question, cocher la ou les réponses correctes.

<br>

??? help "Rappels (variables locales/globales)"

    * Les noms de variables ou de fonctions sont aussi appelés des "identifiants".

    * Les identifiants déclarés dans une fonction ou dans les arguments d'une fonctions sont dits "locaux" à cette fonction.

    * Le code d'une fonction peut accéder à tous les identifiants déjà définis dans le corps de cette fonction, ou dans les espaces de noms "supérieurs" (c'est-à-dire, là où la fonction elle-même a été définie).

        ```python
        def func(n):
            print(a)      # Erreur car `a` n'est pas encore définie
            a = 42
        ```

    * En python, il est interdit à une fonction de __réaffecter__ localement un nom de variable non local à la fonction (c'est-à-dire de faire `var += something`, si `var` n'existe pas dans déjà dans la fonction).

        ```python
        a = 42
        def func():
            a += 101      # Erreur !
        func()
        ```

    * Il est possible (même si déconseillé) de déclarer des variables locales avec les mêmes noms que des variables globales sans erreurs.

        ```python
        a = 42

        def func(a):
            a += 101      # Pas d'erreur !
            return a

        print(func(19))   # afficher 120
        ```
