??? note "`#!py round` ?"
    La fonction intégrée `round` permet d'arrondir un nombre donné.
    La fonction prend en paramètres un nombre à virgule ainsi qu'un nombre de décimales et renvoie la valeur arrondie à ce nombre de décimales près.

    ```pycon
    >>> round(65.1745, 2)
    65.17
    >>> round(65.1795, 1)
    65.2
    ```