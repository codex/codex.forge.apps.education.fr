# CodEx ⇨ Le code par les exercices

Propositions d'exercices de programmation **écrits**, **relus** et **corrigés** par des enseignants.

Accéder au site : https://codex.forge.apps.education.fr/

![logo](./docs/logo_robot_128x128.png)