# 🚶 Parcours

???+ info "Comment ça marche ?"
    On propose ici différents parcours permettant d'explorer un algorithme, une thématique ou une structure de données...

    Il s'agit de suggestions d'exercices. **Tous les exercices présents sur le site ne sont pas listés ici**.

    --------------------------

    Les parcours sont répartis en trois grandes catégories:

    - La section [Informatique](#informatique), dédiée à la NSI, avec éventuellement des exercices pour élargir vers le supérieur.
    - La section [SNT](#snt), couvrant des exercices destinés à un public de seconde SNT.
    - La section [Mathématiques](#maths), couvrant des exercices qui peuvent être utilisés en cours de mathématiques au lycée (de la seconde à la terminale)

--------------------------

## :computer: Informatique { #informatique }

??? info "Légende"

    {{ parcours_mermaid(
        PNode("Exercice"),
        PNode("Epreuve pratique Exercice 1", kls="ep1"),
        PNode("Epreuve pratique Exercice 2", kls="ep2"),
        PNode("Post-Bac", kls="sup"),
        chart="TB",
        allow_non_exos=True
    ) }}



??? tip "Calcul de moyennes"

    ### Calcul de moyennes

    On calcule ici les moyennes dans un tableau, ou dans des structures plus complexes.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("somme_entiers",'moyenne'),
            PNode("moyenne",['moy_olympic','d\'un sous ensemble'],['moy_ponderee', 'avec coefficients']),
            PNode("moy_olympic",'moyenne_mobile'),
            PNode("moy_ponderee",'moyenne_ponderee'),
            PNode("moyenne_ponderee",'poo_eleve'),
            PNode("moyenne_mobile"),
            PNode("poo_eleve"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Moyenne simple", "moyenne") }} : calculer une moyenne
        - {{ lien_parcours("Moyenne olympique", "moy_olympic") }} : calculer une moyenne en excluant la valeur maximale et la valeur minimale
        - {{ lien_parcours("Moyenne pondérée", "moyenne_ponderee") }} : calculer une moyenne pondérée

??? tip "Dictionnaires"

    ### Dictionnaires

    === "Utiliser les dictionnaires"

        {{ parcours_mermaid(
            PNode("couleurs",'mra_bs_clguba'),
            PNode("mra_bs_clguba",'contacts','scrabble_1'),
            PNode("contacts", 'top_like', 'requetes_dict','lsystem'),
            PNode("top_like", 'palmares'),
            PNode("requetes_dict"),
            PNode("lsystem"),
            PNode("scrabble_1", 'scrabble_2','romain_decimal'),
            PNode("scrabble_2"),
            PNode("palmares"),
            PNode("romain_decimal"),
        ) }}

    === "Construire des dictionnaires"

        {{ parcours_mermaid(
            PNode("dict_extremes",'dico_occurrences'),
            PNode("dico_occurrences",'antecedents', 'resultat_vote', ['union_dictionnaires', 'avec deux dictionnaires']),
            PNode("antecedents", 'dictionnaire_renverse'),
            PNode("dictionnaire_renverse"),
            PNode("union_dictionnaires", 'eurovision'),
            PNode("eurovision",'jointure_dictionnaires'),
            PNode("jointure_dictionnaires"),
            PNode("resultat_vote", 'dictionnaire_aime'),
            PNode("dictionnaire_aime",'tete_de_serie'),
            PNode("tete_de_serie"),
        ) }}

    === "Parcours linéaire"

        * Construction de dictionnaires
            - {{ lien_parcours("Valeurs extrêmes", "dict_extremes") }} : extraire le dictionnaire `{"mini": m, "maxi": M}` d'un tableau
            - {{ lien_parcours("Dictionnaire d'occurrences", "dico_occurrences") }} : compter les occurrences des caractères dans une chaine
            - {{ lien_parcours("Dictionnaire des antécédents", "antecedents") }} : déterminer le dictionnaire « réciproque » d'un dictionnaire : `{valeur: [clés associées]}`
        * Utiliser des dictionnaires
            - {{ lien_parcours("Anniversaires", "anniversaires") }} : déterminer les clés dont les valeurs associées vérifient une certaine condition
            - {{ lien_parcours("Couleurs", "couleurs") }} : convertir la représentation d'une couleur en hexadécimal à du RGB
            - {{ lien_parcours("L-système", "lsystem") }} : « calculer » une nouvelle chaine de caractères en respectant les règles contenues dans un dictionnaire
            - {{ lien_parcours("Top-like", "top_like") }} : déterminer la clé de valeur maximale

??? tip "Manipulation de chaines de caractères"

    ### Chaînes de caractères

    Tout est dans le titre !

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("cpt_occurrences", ["validation_mdp", "compter"]),
            PNode("validation_mdp", "correspond_mot"),
            PNode("correspond_mot"),
            PNode("cpt_occurrences", ["dentiste", "copier/coller"]),
            PNode("dentiste", "join"),
            PNode("join", "split"),
            PNode("split", "brut"),
            PNode("brut"),
            PNode("cpt_occurrences", ["code_cesar", "calculer"]),
            PNode("code_cesar", "scrabble_1"),
            PNode("scrabble_1"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Occurrences d'un caractère", "cpt_occurrences") }} : compter les occurrences d'un caractère
        - {{ lien_parcours("Validation de mot de passe", "validation_mdp") }} : un mot de passe est-il « sûr » ?
        - {{ lien_parcours("Dentiste", "dentiste") }} : supprimer les voyelles d'une chaine de caractères
        - {{ lien_parcours("Mots qui se correspondent", "correspond_mot") }} : comparer deux chaines de caractères
        - {{ lien_parcours("Renverser une chaine", "renverse_chaine") }} : comme son nom l'indique !
        - {{ lien_parcours("Collage", "join") }} : former une chaine à partir des éléments d'une liste... réécrire `" ".join(mots)` !
        - {{ lien_parcours("Découpe", "split") }} : découper une chaine à chaque espace... réécrire `chaine.split(' ')` !
        - {{ lien_parcours("Code de César", "code_cesar") }} : chiffrer une chaine de caractère à l'aide du code de César
        - {{ lien_parcours("Texte inclus", "est_inclus") }} : recherche d'un motif dans une chaine de caractères
        - {{ lien_parcours("Texte brut", "brut") }} : extraire le contenu textuel d'une source `HTML`

??? tip "Découverte des listes"

    ### Découverte des listes

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("fonctions", "range"),
            PNode("range", ["course_cycliste", "lire/modifier"]),
            PNode("course_cycliste", "recadrer"),
            PNode("recadrer"),
            PNode("range", ["indices_valeurs", "parcourir"]),
            PNode("indices_valeurs", "repetitions"),
            PNode("repetitions", "derniere_occurrence"),
            PNode("derniere_occurrence"),
            PNode("range", ["termes_d_une_suite", "créer en\n extension"]),
            PNode("termes_d_une_suite", "diviseurs"),
            PNode("diviseurs", "syracuse"),
            PNode("syracuse"),
            PNode("range", ["suite_arithm", "créer en\n compréhension"]),
            PNode("suite_arithm", "somme_suite"),
            PNode("somme_suite"),
            PNode("range", ["filtre_tableau", "approfondir"]),
            PNode("filtre_tableau"),
        ) }}

    === "Parcours linéaire"

        * Prérequis
            - {{ lien_parcours("Fonctions simples", "fonctions") }} : écrire des fonctions très simples
            - {{ lien_parcours("Autour de range", "range") }} : savoir utiliser range
        * Accéder aux éléments d'une liste
            - {{ lien_parcours("La course cycliste (I)", "course_cycliste") }} : liste des concurrents dans une course cycliste
        * Modifier une liste"
            - {{ lien_parcours("Recadrer", "recadrer") }} : Remplacer des valeurs plus petites qu'une certaine valeur par celle-ci. Idem pour les grandes.
        * Créer une liste en extension"
            - {{ lien_parcours("Liste des termes d'une suite mathématique", "termes_d_une_suite") }}
            - {{ lien_parcours("Diviseurs d'un entier positif", "diviseurs") }}
            - {{ lien_parcours("Suite de Syracuse", "syracuse") }}
        * Créer une liste en compréhension"
            - {{ lien_parcours("Liste des termes d'une suite arithmétique", "suite_arithm") }}
            - {{ lien_parcours("Somme des termes d'une suite mathématique", "somme_suite") }}
        * Parcourir une liste"
            - {{ lien_parcours("Indices ou valeurs", "indices_valeurs") }}
            - {{ lien_parcours("Nombre de répétitions", "repetitions") }}
            - {{ lien_parcours("Moyenne simple", "moyenne") }}
            - {{ lien_parcours("Dernière occurrence", "derniere_occurrence") }}
        * Approfondissement"
            - {{ lien_parcours("Filtrer un tableau", "filtre_tableau") }}

??? tip "Filtrer un tableau"

    ### Filtrer un tableau

    Ici l'on filtre des tableaux, ou l'on vérifie une propriété d'un tableau.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("ecretage", "requetes"),
            PNode("requetes", "requetes_dict"),
            PNode("requetes_dict"),
            PNode("soleil_couchant", "est_trie","gelee"),
            PNode("est_trie", ["nb_puis_double", "création de listes"]),
            PNode("liste_differences", "tous_differents"),
            PNode("tous_differents"),
            PNode("gelee"),
            PNode("nb_puis_double"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Écrêtage", "ecretage") }} : remplacer les valeurs extrêmes d'un tableau
        - {{ lien_parcours("Soleil couchant", "soleil_couchant") }} : déterminer tous les *maxima* relatifs d'un tableau
        - {{ lien_parcours("Nombres puis double", "nb_puis_double") }} : rechercher des couples de valeurs consécutives particulières dans un tableau
        - {{ lien_parcours("Est trié ?", "est_trie") }} : le tableau est-il trié ?
        - {{ lien_parcours("Liste des différences", "liste_differences") }} : comparer deux tableaux
        - {{ lien_parcours("Gelées", "gelee") }} : longueur d'un sous-tableau particulier
        - {{ lien_parcours("Tous différents", "tous_differents") }} : les éléments d'un tableau sont-ils tous différents ?


??? tip "Recherche dans un tableau"

    ### Recherche dans un tableau

    On recherche ici les _extrema_ dans un tableau (minimum ou maximum), une valeur ou un indice particulier.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("recadrer",['ind_min','recherche d\'un extremum'],['ind_prem_occ','recherche d\'une occurence'],),
            PNode("ind_min", 'maximum_nombres'),
            PNode("ind_prem_occ", 'derniere_occurrence'),
            PNode("maximum_nombres", 'val_ind_max','plus_proche'),
            PNode("derniere_occurrence",'repetitions'),
            PNode("repetitions"),
            PNode("val_ind_max"),
            PNode("plus_proche"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Maximum", "maximum_nombres") }} : déterminer le maximum dans un tableau
        - {{ lien_parcours("Indice du minimum", "ind_min") }} : déterminer l'indice du minimum dans un tableau
        - {{ lien_parcours("Premier minimum local", "premier_minimum") }} : déterminer l'indice du premier minimum dans un tableau
        - {{ lien_parcours("Occurrences du minimum", "occurrences_du_mini") }} : déterminer la valeur et les indices du minimum
        - {{ lien_parcours("Valeur et indices du max", "val_ind_max") }} : déterminer la valeur et les indices du maximum

??? tip "Récursivité"

    ### Récursivité

    Voir « Récursivité » !

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("suite","pgcd"),
            PNode("pgcd", ['harshad_recursif', "calculer"],['dessin_arbres','dessiner des fractales'],['arbre_dom','structures arborescentes'],["hanoi_resolution","diviser pour régner"]),
            PNode("harshad_recursif","fib_1"),
            PNode("fib_1"),
            PNode("hanoi_resolution"),
            PNode("dessin_arbres", 'dessin_koch'),
            PNode("dessin_koch",'dessin_cesaro'),
            PNode("dessin_cesaro"),
            PNode("arbre_dom","arbre_genealogique"),
            PNode("arbre_genealogique",["ABR","ABR"],"expr_parenthesee"),
            PNode("expr_parenthesee"),
            PNode("ABR"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Suite de Fibonacci (1)", "fib_1") }} : une fonction très élémentaire, pour des indices inférieurs à 25
        - {{ lien_parcours("Anagrammes", "anagrammes") }} : deux chaines sont-elles des anagrammes ?
        - {{ lien_parcours("Percolation", "percolation") }} : écoulement d'eau dans un sol, ou plutôt parcours en profondeur dans une grille
        - {{ lien_parcours("Pavage possible avec triominos (2)", "pavage_triomino_2") }} : Construire un pavage, si possible, d'un carré troué avec des triominos
        - {{ lien_parcours("Énumération des permutations", "nb_factoriel") }} : les nombres factoriels
        - {{ lien_parcours("Énumération des chemins à Manhattan", "nb_chemins_grille") }} : relier deux points en ne se déplaçant que vers la droite ou vers le haut
        - {{ lien_parcours("Énumération des chemins de Schröder", "nb_schroder") }} : mouvements ↗ , →→, ou ↘
        - {{ lien_parcours("Énumération des chemins de Delannoy", "nb_delannoy") }} : mouvements ↗, →, ou ↘
        - {{ lien_parcours("Énumération des arbres binaires de taille n", "nb_catalan_2") }} : les nombres de Catalan
        - {{ lien_parcours("Énumération des arbres unaires-binaires à n+1 nœuds", "nb_motzkin") }} : les nombres de Motzkin
        - {{ lien_parcours("Énumération des subdivisions de polygone", "nb_hipparque") }} : Les nombres d'Hipparque
        - {{ lien_parcours("Suite de Fibonacci (2)", "fib_2") }}  : Calcul possible du millionième terme
        - {{ lien_parcours("Nombre de partitions d'un entier", "nb_partitions") }}

??? tip "Structures conditionnelles"

    ### Structures conditionnelles

    Si c'est vrai il faut faire ceci, sinon cela...

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("autour_des_booleens", "operateurs_booleens"),
            PNode("operateurs_booleens", "syracuse"),
            PNode("syracuse", "tous_differents"),
            PNode("tous_differents", "anonymat_1"),
            PNode("anonymat_1", "anonymat_2"),
            PNode("anonymat_2"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Autour des booléens", "autour_des_booleens") }} : écrire des tests tels que « $15$ est-il dans la table de $3$ et de $5$ ? »
        - {{ lien_parcours("Années bissextiles", "bissextile") }} : déterminer si une année est bissextile ou non
        - {{ lien_parcours("Opérateurs booléens", "operateurs_booleens") }} : écrire les opérateurs booléens sans utiliser les opérateurs booléens !
        - {{ lien_parcours("Multiplier sans *", "multiplication") }} : multiplier deux entiers relatifs sans utiliser le symbole `*`
        - {{ lien_parcours("Suite de Syracuse", "syracuse") }} : la fameuse suite $3n+1$
        - {{ lien_parcours("Tous différents", "tous_differents") }} : les éléments d'un tableau sont-ils tous distincts (solution en temps quadratique)
        - {{ lien_parcours("Noircir un texte", "anonymat_1") }} et {{ lien_parcours("Caviarder un texte", "anonymat_2") }} : effacer les caractères alphabétiques dans un texte

??? tip "Structures linéaires"

    ### Structures linéaires

    On y utilise ou met en œuvre des listes chaînées, piles, files.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("liste_longueurs",['autour_des_files','Les files'],['autour_des_piles','les piles'],['poo_train','Les listes chaînées'],),
            PNode("autour_des_files",'file_deux_piles'),
            PNode("autour_des_piles", 'diverses_piles','diverses_piles_alt'),
            PNode("diverses_piles", 'filtre_pile',),
            PNode("diverses_piles_alt",'file_deux_piles'),
            PNode("filtre_pile",'eval_postfixe'),
            PNode("poo_train",'file_avec_liste', 'pile_liste_chainee'),
            PNode('file_avec_liste',"file_liste_circulaire",),
            PNode("file_liste_circulaire"),
            PNode("pile_liste_chainee"),
            PNode("eval_postfixe", 'parentheses'),
            PNode("parentheses", 'expression_bien_parenthesee_2'),
            PNode("expression_bien_parenthesee_2",),
            PNode("file_deux_piles",),
        ) }}


    === "Parcours linéaire"

        * {{ lien_parcours("Train en POO", "poo_train") }} : une liste chainée en réalité
        * {{ lien_parcours("Bien parenthésée 2", "expression_bien_parenthesee_2") }} : une expression est-elle bien parenthésée ?
        * {{ lien_parcours("Hauteur et taille d'un arbre", "arbre_enracine") }} : on y représente les arbres sous forme de liste de listes Python
        * {{ lien_parcours("Hauteur et taille d'un arbre binaire", "arbre_bin") }} : on y représente les arbres à l'aide de la programmation orientée objet

??? tip "Tris"

    ### Tris

    Les classiques !

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("est_trie",'ind_min','insertion_liste_triee','tableau_0_1'),
            PNode("ind_min",'tri_selection'),
            PNode('tri_selection'),
            PNode("insertion_liste_triee",'tri_insertion','fusion_listes_triees'),
            PNode("tri_insertion",'tri_bulles'),
            PNode("tableau_0_1",'tri_trois_valeurs', ),
            PNode("tri_trois_valeurs",'partition_tableau'),
            PNode("partition_tableau",'tri_pivot'),
            PNode("tri_pivot",'tri_denombrement'),
            PNode("tri_denombrement"),
            PNode("tri_bulles"),
            PNode("fusion_listes_triees"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Tri par sélection", "tri_selection") }}
        - {{ lien_parcours("Tri à bulles", "tri_bulles") }}
        - {{ lien_parcours("Insertion dans une liste triée", "insertion_liste_triee") }}
        - {{ lien_parcours("Fusion de listes triées", "fusion_listes_triees") }}
        - {{ lien_parcours("Tas min", "tas_min") }} : vérifier qu'un tableau représente un tas minimal

??? tip "Algorithmes gloutons"

    ### Algorithmes gloutons

    On utilise des méthodes gloutonnes.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("rendu_monnaie_3p",'premier_minimum',["rendu_monnaie","avec récursivité"]),
            PNode("premier_minimum","manhattan",'conversion_gloutonne'),
            PNode("rendu_monnaie"),
            PNode("manhattan","partition_equilibree_1"),
            PNode("partition_equilibree_1"),
            PNode("conversion_gloutonne","numeration_shadock"),
            PNode("numeration_shadock"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Premier minimum local", "premier_minimum") }} : déterminer l'indice du premier minimum dans un tableau

??? tip "Programmation orientée objet"

    ### Programmation orientée objet

    Il s'agit ici d'utiliser des classes proposées ou de les écrire.

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("syntaxe_poo", ['robot_1', 'utiliser de objets'], ['geometrie_vectorielle_POO', 'compléter une classe']),
            PNode("robot_1", 'course_rentree'),
            PNode("geometrie_vectorielle_POO", 'domotique'),
            PNode("domotique", 'poo_chien'),
            PNode("poo_chien", ['livret', 'attribut privé'], 'belote', ['entiers_sans_chiffres_POO', 'méthodes spéciales'], ['etat_processus', 'état d\'un objet']),
            PNode("course_rentree", 'domotique_II'),
            PNode("livret", 'yahourt'),
            PNode("belote", 'paquet_de_cartes'),
            PNode("entiers_sans_chiffres_POO", 'les-durees-POO-II'),
            PNode("etat_processus", ['paquet', 'états d\'un objet']),
        ) }}


??? tip "Programmation dynamique"

    ### Programmation dynamique

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("nb_factoriel",'k_consecutifs_1'),
            PNode('k_consecutifs_1',"somme_sous_ensemble"),
            PNode("fib_1","fib_3"),
            PNode("fib_3",'nb_factoriel',["nb_chemins_grille","chemin"]),
            PNode("nb_chemins_grille",'nb_delannoy'),
            PNode("nb_delannoy",'mineur'),
            PNode('mineur'),
            PNode('somme_sous_ensemble'),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Communication des acacias", "acacias") }} : somme maximale dans un tableau sous contrainte
        - {{ lien_parcours("Plus longues sous-chaines", "lplssc") }} : plus longues sous-chaines communes à deux chaines

??? tip "Arbres"

    ### Arbres

    Il s'agit ici de traiter des arbres enracinés, et des arbres binaires.

    === "Arbres enracinés"

        {{ parcours_mermaid(
            PNode("arbre_enracine","arbre_enracine_2"),
            PNode("arbre_enracine_2","arbre_enracine_3"),
            PNode("arbre_enracine_3","systeme_fichiers","expr_parenthesee"),
            PNode("expr_parenthesee","ancetre_commun"),
            PNode("systeme_fichiers"),
            PNode("ancetre_commun"),
        ) }}
    
    === "Arbres binaires"

        {{ parcours_mermaid(
            PNode("arbre_genealogique","arbre_bin","arbre_additif"),
            PNode("arbre_bin","ABR"),
            PNode("arbre_additif"),
            PNode("ABR"),
        ) }}


??? tip "Graphes"

    ### Graphes

    ```mermaid
    flowchart LR
        G ---|5| R
        G ---|2| A
        R ---|3| A
        A --- |1| P
        P --- |6| H
        P --- |6| E
        H --- |1| E
    ```

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("liste_adjacence"),
            PNode("permutation_circulaire","aimer_etre_aime",["tourner_en_rond","recherche de cycles"]),
            PNode("tourner_en_rond","lievre_tortue"),
            PNode("lievre_tortue"),
            PNode("aimer_etre_aime", "dico_amis", ["liste_adjacence","changement d'implémentations"]),
            PNode("dico_amis"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("La rumeur qui court", "rumeur") }} : parcours en largeur
        - {{ lien_parcours("Degré de séparation", "degres_separation") }} : illustration des petits mondes
        - {{ lien_parcours("Aimer et être aimé", "aimer_etre_aime") }} : renverser un graphe orienté
        - {{ lien_parcours("Cadeaux circulaires", "permutation_circulaire") }} : un graphe est-il cyclique ?
        - {{ lien_parcours("Liste et matrice d'adjacence", "liste_adjacence") }} : passer d'une représentation d'un graphe à l'autre
        - {{ lien_parcours("Matrice d'adjacence", "matrice_adjacence") }} : deux exercices autour des matrices d'adjacence
        - {{ lien_parcours("Mme Tortue et M. Lièvre", "lievre_tortue") }} : algorithme de Floyd

--------------------------

## :space_invader: SNT { #snt }

??? tip "Bases de Python"

    ### Bases de Python

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("alien_1",'alien2'),
            PNode("alien2",'alien3'),
            PNode("alien3",'alien4'),
            PNode("alien4",'alien5','creer_image'),
            PNode("alien5",'alien6','polygones','triangle_mystere'),
            PNode("alien6"),
            PNode("creer_image"),
            PNode("polygones"),
            PNode("triangle_mystere"),
        ) }}

--------------------------

## :pencil: Maths { #maths }

??? tip "Seconde"

    ### Seconde

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("moy_ponderee"),
            PNode("fonctions", ["fonctions_affines", "fonctions"]),
            PNode("fonctions_affines", 'second_degre_1'),
            PNode("fonctions_affines", 'balayage'),
            PNode("second_degre_1"),
            PNode("equation_cartesienne",),
            PNode("balayage"),
            PNode("fonctions", ["geometrie_vectorielle", "géométrie"]),
            PNode("geometrie_vectorielle", 'equation_cartesienne'),
            PNode('equation_cartesienne'),
            PNode("fonctions", ["somme_entiers", "calculs"]),
            PNode("somme_entiers",'moyenne'),
            PNode("somme_entiers",'multiple'),
            PNode("somme_entiers",'seuil_puissances'),
            PNode("moyenne",'moy_ponderee'),
            PNode("multiple", 'diviseurs'),
            PNode("seuil_puissances"),
            PNode("diviseurs",'nombre_premier'),
            PNode("nombre_premier"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Fonctions affines", "fonctions_affines") }} : plusieurs exercices autour des fonctions affines
        - {{ lien_parcours("Fonctions du 2nd degré", "second_degre_1") }} : plusieurs exercices autour des fonctions polynômes du seconde degré
        - {{ lien_parcours("Géométrie vectorielle", "geometrie_vectorielle") }} : plusieurs exercices de géométrie vectorielle
        - {{ lien_parcours("Équation cartésienne d'une droite", "equation_cartesienne") }} : déterminer une équation cartésienne de la droite passant par deux points distincts du plan
        - {{ lien_parcours("Résolution d'équation par balayage", "balayage") }} : résolution approchée d'une équation
        - {{ lien_parcours("Moyenne simple", "moyenne") }} : calculer une moyenne
        - {{ lien_parcours("Moyennes pondérée", "moy_ponderee") }} : calculer une moyenne pondérée
        - {{ lien_parcours("Puissances d'un nombre", "seuil_puissances") }} : déterminer la première puissance d'un nombre dépassant ou passant sous un seuil
        - {{ lien_parcours("Multiple", "multiple") }} : déterminer si un entier est un multiple d'un autre
        - {{ lien_parcours("Diviseurs d'un entier", "diviseurs") }} : déterminer les diviseurs d'un entier positif
        - {{ lien_parcours("Divisibilité par 3", "divisible_par_3") }} : trois méthodes pour étudier la divisibilité par trois
        - {{ lien_parcours("Nombre Premier", "nombre_premier") }} : déterminer si un (petit) nombre entier est premier

??? tip "Première"

    ### Première

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("second_degre_1",'second_degre_2'),
            PNode("second_degre_2"),
            PNode("suite",'polonium','hanoi_nb_coups'),
            PNode("hanoi_nb_coups",'syracuse','rendement_livret'),
            PNode("polonium",'termes_d_une_suite'),
            PNode("syracuse"),
            PNode("rendement_livret",'somme_suite'),
            PNode("termes_d_une_suite"),
            PNode("somme_suite"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Équations du 2nd degré", "second_degre_1") }} : plusieurs exercices autour des équations du seconde degré
        - {{ lien_parcours( "Termes d'une suite", "suite") }} : calculer $u_n$
        - {{ lien_parcours( "Nombre de coups à la tour de Hanoï", "hanoi_nb_coups") }} : calculer le nombre minimal de coups permettant de résoudre la tour de Hanoï de deux façons
        - {{ lien_parcours( "Désintégration", "polonium") }} : modéliser la désintégration d'atomes par une suite
        - {{ lien_parcours( "Suite de Syracuse", "syracuse") }} : la fameuse suite
        - {{ lien_parcours( "Rendement d'un livret", "rendement_livret") }} : utiliser les suites dans le cadre d'un compte bancaire
        - {{ lien_parcours( "Suite arithmétique", "suite_arithm") }} : utiliser une suite arithmétique
        - {{ lien_parcours( "Suite doublement récurrente", "termes_d_une_suite") }} : utiliser une suite récurrente
        - {{ lien_parcours( "Somme des termes d'une suite", "somme_suite") }} : sommer des termes consécutifs d'une suite
        - {{ lien_parcours( "Terme d'une suite constante", "suite_flottants") }} : où il est bon de prendre un crayon avant de taper du code

??? tip "Terminale"

    ### Terminale

    === "Parcours arborescent"

        {{ parcours_mermaid(
            PNode("seuil_explicite", 'seuil_recurrence'),
            PNode("seuil_recurrence"),
            PNode("integrale_1",'integrale_2'),
            PNode("integrale_2"),
            PNode("dicho_maths"),
        ) }}

    === "Parcours linéaire"

        - {{ lien_parcours("Seuil et suite explicite" , "seuil_explicite") }} : limite d'un suite définie explicitement
        - {{ lien_parcours("Seuil et suite récurrente" , "seuil_recurrence") }} : limite d'un suite définie par récurrence
        - {{ lien_parcours("Intégrale entre 0 et 1", "integrale_1") }} : calcul approché de $\int_{0}^{1}x^2\text{d}x$ par la méthode des rectangles
        - {{ lien_parcours("Intégrale quelconque", "integrale_2") }} : calcul appoché de  $\int_{a}^{b}f(x)\text{d}x$ par la méthode des rectangles
        - {{ lien_parcours("Résolution approchée d'équation par dichotomie" , "dicho_maths") }}
