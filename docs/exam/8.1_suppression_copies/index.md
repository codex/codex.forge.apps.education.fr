---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Suppression des copies consécutives
---

# Suppression des copies consécutives

## Objectif

Dans cet exercice, on cherche à supprimer d'une liste les éléments consécutifs identiques. La liste résultat ne contiendra donc aucune suite d'éléments identiques.

Certains éléments pourront néanmoins apparaitre plusieurs fois, mais pas consécutivement.

Les éléments de la liste de résultats devront apparaitre dans le même ordre que dans la liste initiale.

## Exemples

```pycon
>>> suppression([])
[]
>>> suppression([1, 2, 2, 1, 2, 2, 2])
[1, 2, 1, 2]
>>> suppression([1, 2, 3])
[1, 2, 3]
>>> suppression([-12, 17, 17, 1, 100, 100, 1])
[-12, 17, 1, 100, 1])
```
