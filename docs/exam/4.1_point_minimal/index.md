---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Point minimal
---

# Point minimal

On considère dans cet exercice une liste contenant les coordonnées de points de l'espace. Chaque point est représenté par un triplet des trois coordonnées $(x, y, z)$.

Par exemple :

```python
#         (x  y   z)  (x   y   z)                ...                  (x   y  z)
points = [(9, 2, -8), (-3, 9, -8), (1, 9, 7), (-3, 6, 7), (-7, 1, 3), (-4, 0, 5)]
```

On souhaite récupérer les coordonnées du point ayant la plus petite coordonnée selon $x$, $y$ ou $z$.


## Objectif

On demande donc d'écrire la fonction `minimum` qui prend en paramètre la liste `points` des coordonnées des points ainsi qu'une chaine de caractères `composante` valant soit `#!py "Ox"`, `#!py "Oy"` ou `#!py "Oz"` et renvoyant les coordonnées du point ayant la plus petite coordonnée pour la composante indiquée.

On garantit que la liste est non-vide et que le paramètre `composante` est toujours égal à `#!py "Ox"`, `#!py "Oy"` ou `#!py "Oz"`.

Si deux points ont la même valeur minimale pour la composante indiquée, la fonction renverra le point ayant le plus petit indice dans la liste `points`.

## Exemples

```pycon
>>> points = [(9, 2, -8), (-3, 9, -8), (1, 9, 7), (-3, 6, 7), (-7, 1, 3), (-4, 0, 5)]
>>> minimum(points, "Ox")
(-7, 1, 3)
>>> minimum(points, "Oy")
(-4, 0, 5)
>>> minimum(points, "Oz")
(9, 2, -8)
```
