class Pile:
    def __init__(self):
        self.valeurs = []

    def est_vide(self):
        """Détermine si la pile est vide"""
        return len(self.valeurs) == 0

    def empile(self, x):
        """Empile x dans la pile"""
        self.valeurs.append(x)

    def depile(self):
        """Dépile une valeur et la renvoie
        Lève une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.valeurs.pop()


def renverse(pile):
    resultat = Pile()
    while ...:
        x = pile.depile()
        ...
    return ...


def filtre(pile):
    positifs = ...
    negatifs = ...

    while ...:
        x = ...
        if ...:
            ...
        else:
            ...

    return ..., ...
