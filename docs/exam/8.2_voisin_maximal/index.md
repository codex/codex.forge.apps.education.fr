---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Voisin maximal
---

# Voisin maximal

On considère des grilles d'entiers représentées en Python par des listes de listes. Les entiers peuvent être positifs ou négatifs.
 
On fournit les coordonnées d'une valeur dans cette grille : indices `i` de sa ligne et `j` de sa colonne.

On souhaite récupérer la valeur maximale prise par un des _voisins_ de la case désignée. Une case peut posséder jusqu'à 8 voisins :

```text title="Les 8 voisins"
  A   B   C
    \ | /
  D - ? - E
    / | \
  F   G   F
```

## Objectif

Écrire la fonction `maximum` qui prend en paramètres la liste `grille` représentant la grille ainsi que `i` et `j`, les coordonnées d'une case dans la grille, et qui renvoie la valeur maximale prise par un voisin de la case. 

On garantit que la grille est non vide et que les coordonnées `i` et `j` désignent une case valide.

## Exemples

```pycon
>>> grille = [
       [3,   8,  7],
       [13, -5,  2],
       [3,  18, -7],
       [5,   3, 10],
    ]
>>> maximum(grille, 0, 0)
13
>>> maximum(grille, 0, 1)
13
>>> maximum(grille, 0, 2)
8
>>> maximum(grille, 2, 0)
18
```
