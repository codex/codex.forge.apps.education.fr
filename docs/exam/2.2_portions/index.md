---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Nombre de portions
---

# Nombre de portions

On fournit une recette sous forme d'un dictionnaire `{ingredient: dose}`. Ce dictionnaire contient tous les ingrédients nécessaires à la fabrication d'une portion d'un plat.

Par exemple `recette = {"farine": 200, "œuf": 3, "lait": 300}`. On lit que la fabrication d'une portion du plat nécessite 200 grammes de farine, 3 œufs et 300 millilitres de lait.

On fournit sous le même format le dictionnaire des ingrédients présents dans la réserve du cuisinier. Par exemple : `reserve = {"chou-fleur": 1, "œuf": 7, "farine": 1000, "lait": 1000}`. Ce dictionnaire peut contenir plus ou moins d'ingrédients que ceux cités dans la recette.

Compte-tenu de la recette, ces ingrédients permettent de préparer **2** portions du plat. En effet :

* pour la farine, $2 × 200 = 400 ⩽ 1000$ ;

* pour les œufs, $2 × 3 = 6 ⩽ 7$ ;

* pour le lait, $2 × 300 = 600 ⩽ 1000$.

## Objectif

On demande d'écrire la fonction `portions` prenant en paramètre les dictionnaires `recette` et `reserve` décrits ci-dessus et renvoyant le plus grand nombre de portions qu'il est possible de réaliser.

On garantit que :

* le dictionnaire `recette` n'est pas vide,

* les quantités sont toutes des nombres strictement positifs,

* le nombre de portions maximal sera inférieur ou égal à 1000.

## Exemples

```pycon
>>> recette = {"farine": 200, "œuf": 3, "lait": 300}
>>> reserve = {"chou-fleur": 1, "œuf": 7, "farine": 1000, "lait": 1000}
>>> portions(recette, reserve)
2
>>> recette = {"farine": 200, "œuf": 3, "lait": 300, "chocolat": 200}
>>> reserve = {"œuf": 7, "farine": 1000, "chocolat": 120, "lait": 1000}
>>> portions(recette, reserve)
0
```
