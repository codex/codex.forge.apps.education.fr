---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Serpent en chameau
---

# Transformer un serpent en chameau

En informatique, la *snake_case*, littéralement *casse du serpent* en français, est une façon d'écrire les identifiants (de variable, de fonction) dans un programme informatique. Si l'identifiant contient plusieurs _parties_ (lettres, mots ou nombres), elles sont toutes écrites séparées par des tirets bas (un seul entre chaque partie). Tous les caractères alphabétiques sont écrits en minuscules.

Par exemple, `#!py "casse_du_chameau"`, `#!py "serpent"`, `#!py "ordinateur_2_bureau"` et `#!py "variable_bis_28"` sont écrits en *casse du serpent*.

La *camelCase*, littéralement *casse du chameau*, est un autre formatage dans lequel on écrit les différentes _parties_ d'un identifiant à la suite les unes des autres en prenant soin d'écrire la première lettre de chaque partie alphabétique en majuscule (sauf pour la toute première partie). Les tirets bas sont supprimés.

Par exemple, `#!py "casseDuChameau"`, `#!py "serpent"`, `#!py "ordinateur2Bureau"` et `#!py "variableBis28"` sont écrits en *casse du chameau*.

## Objectif

On demande d'écrire la fonction `en_chameau` prenant en paramètre une chaine de caractères `identifiant` contenant un identifiant écrit en *casse du serpent* et renvoyant le même identifiant écrit en *casse du chameau*.

On rappelle que si `c` est un caractère alors :

* `c.lower()` renvoie le même caractère passé en casse minuscule,

* `c.upper()` renvoie le même caractère passé en casse majuscule.

Les caractères numériques sont renvoyés tels quels par ces deux méthodes.

On garantit que les identifiants :

* ne sont pas vides,

* répondent exactement au format *casse du serpent*,

* ne contiennent que des caractères alphabétiques ou numériques.

## Exemples

```pycon
>>> en_chameau("casse_du_chameau")
'casseDuChameau'
>>> en_chameau("serpent")
'serpent'
>>> en_chameau("x_y_z")
'xYZ'
>>> en_chameau("une_variable_hyper_importante")
'uneVariableHyperImportante'
>>> en_chameau("variable_bis_28")
'variableBis28'
>>> en_chameau("x_123456_y")
'x123456Y'
```