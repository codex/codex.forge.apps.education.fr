from collections import deque


class File:
    def __init__(self):
        self.queue = deque()

    def est_vide(self):
        """Indique si la file est vide"""
        return len(self.queue) == 0

    def enfile(self, x):
        """Enfile x dans la file"""
        self.queue.append(x)

    def defile(self):
        """Défile et renvoie une valeur de la file
        Lève une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.queue.popleft()


def parcours_largeur(arbre):
    pass
