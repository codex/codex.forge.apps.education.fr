---
hide:
    - navigation
    - toc
search:
    exclude: true
---

# ✅ Aide

Le site propose des exercices de programmation en Python écrits, relus et discutés par des professeurs d'informatique.

La majorité des exercices sont construits sur le même modèle :

* un énoncé qui décrit le problème à résoudre ;

* des exemples permettant d'illustrer ce qui est attendu et certains cas particuliers ;

* un éditeur permettant de saisir l'exercice. Dans certains cas plusieurs versions de l'exercice sont proposées à l'aide de panneaux coulissants ({{ lien_exo("par exemple ici","diagramme_barre") }}).

---------------


## Tutoriels vidéos

??? tip "Chercher un exercice"
    <center>
        <iframe title="Codex - recherche" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/2bbdc217-1e5e-4e24-bb10-52cc7b25bf4b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </center>
??? tip "Résoudre un exercice"
    <center>
        <iframe title="CodEx - exercice" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/7f7ee348-b1d3-4943-a08e-015eb887db8b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </center>
??? tip "Tester son code : les assertions"
    <center>
        <iframe title="CodEx - assertions" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/a5af0a1d-7adc-4e5c-842f-3e622ba1ecac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </center>

## L'éditeur

L'éditeur comporte trois zones (dépliez le bloc ci-dessous pour voir un exemple):

* une zone de saisie (partie supérieure);

* un terminal (partie centrale, vide initialement);

* des boutons (en bas).

??? tip "Un éditeur"

    {{ IDE('exercices/somme/exo', ID=2)}}

### Tests publics

Vous devez compléter le code dans la zone de saisie. Les assertions constituent les **tests publics**. Il reprennent le plus souvent les exemples de l'énoncé.

Une ligne du type `#!py assert somme(10, 32) == 42` vérifie que la fonction `somme` renvoie bien `#!py 42` lorsqu'on lui propose `#!py 10` et `#!py 32` comme arguments.

Vous pouvez vérifier que votre fonction passe ces tests publics en cliquant sur le bouton **Exécuter** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-play-64.png" alt="Exécuter les tests"></button><span>.

Rédiger une fonction qui passe les tests publics est la première étape de résolution d'un exercice mais ce n'est pas la seule. Votre fonction doit aussi passer les **tests privés**.

### Tests privés

Une fois les tests publics passés, vous pouvez passer les **tests privés** en cliquant sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-check-64.png" alt="Valider"></button><span>.

Ceux-ci sont plus nombreux et, comme leur nom l'indique, ne vous sont pas connus. Seul leur résultat vous est indiqué avec, parfois, un commentaire sur la donnée ayant mis en défaut votre code.

L'exercice est considéré comme résolu et le corrigé et les commentaires sont affichés lorsque les tests privés sont passés.

Dans la plupart des exercices, un compteur permet de suivre vos essais. Ce compteur est décrémenté à chaque fois que vous cliquez sur le bouton **Valider** <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-check-64.png" alt="Valider"></button><span> effectuant les tests privés. Lorsqu'il atteint 0, la solution de l'exercice vous est proposée.

### Autres boutons

Il est aussi possible de :

* <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-download-64.png" alt="Télécharger"></button><span> : télécharger le contenu de l'éditeur si vous souhaitez le conserver ou travailler en local ;

* <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-upload-64.png" alt="Téléverser"></button><span> : téléverser un fichier Python dans l'éditeur afin de rapatrier votre travail local ;

* <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-restart-64.png" alt="Réinitialiser l'IDE"></button><span> : recharger l'éditeur dans son état initial ;

* <span class="py_mk_ide"><button  style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="../assets/images/icons8-save-64.png" alt="Sauvegarder"></button><span> : sauvegarder le contenu de l'éditeur dans la mémoire de votre navigateur ;

---------------

## Aspects techniques

L'exécution de Python par le navigateur a quelques incidences. En effet, si un exercice comporte plusieurs éditeurs, **ceux-ci partagent tous le même espace de nom**. Ce qui est tapé dans l'un est accessible dans les autres (exactement comme pour les différentes cellules d'un carnet Jupyter).
