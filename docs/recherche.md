---
hide:
    - navigation
    - toc
search:
    exclude: true
---

# 🧭 Recherche

???+ tip "Sélection des exercices"

    Vous pouvez sur cette page filtrer les exercices selon différents critères :

    * en précisant le mode de sélection (`ET` / `OU`) et en filtrant les exercices « en travaux » ;

    * en sélectionnant les thématiques abordées par les exercices.

    <div class="et-ou-travaux-container">
        <fieldset class="et-ou-container choix-tags"><legend>Mode de combinaison des tags</legend>
            <span id="span-ou"><span class="md-tag">les uns</span> <code>OU</code> <span class="md-tag">les autres</span></span>
            <input type="checkbox" id="switch-et-ou"/>
            <label for="switch-et-ou" id="label-et-ou"></label>
            <span id="span-et"><span class="md-tag">les uns</span> <code>ET</code> <span class="md-tag">les autres</span></span>
        </fieldset>
        <fieldset class="travaux-container choix-tags"><legend>Exercices « en travaux »</legend>
            <input type="checkbox" id="switch-travaux" checked=""/>
            <label for="switch-travaux" id="label-travaux"></label>
            <span for="affiche-travaux" id="span-travaux">Afficher les exercices « en travaux »</span>
        </fieldset>
        <fieldset class="tracking-container choix-tracking"><legend>Suivi des exercices</legend>
            <input type="checkbox" id="switch-tracking" checked="" />
            <label for="switch-tracking" id="label-tracking"></label>
            <span for="affiche-tracking" id="span-tracking">Afficher les informations</span>
        </fieldset>
    </div>

    <fieldset class="choix-tags"><legend>Tags sélectionnés</legend>
        {% for tag in config.plugins.pyodide_macros.variables.tagsValides -%}
            {% if tag != "brouillon" and tag != "en travaux" -%}
                <span class='tag-selection tag-selection-hover' data-hover="{{ config.plugins.pyodide_macros.variables.tagsValides[tag]['description'] }}">{{ tag }}</span>
            {%- endif %}
        {%- endfor %}
    </fieldset>
