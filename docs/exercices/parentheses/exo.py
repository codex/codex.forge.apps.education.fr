# --------- PYODIDE:env --------- #


class Pile:
    "Classe définissant une structure de pile"

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        "Renvoie le booléen True si la pile est vide, False sinon"
        return self.contenu == []

    def empile(self, v):
        "Place l'élément v au sommet de la pile"
        self.contenu.append(v)

    def depile(self):
        """
        Retire et renvoie l'élément placé au sommet de la pile,
        si la pile n'est pas vide
        """
        if not self.est_vide():
            return self.contenu.pop()


# --------- PYODIDE:code --------- #

def est_bien_delimitee(expression):
    ...

# --------- PYODIDE:corr --------- #

def est_bien_delimitee(expression):
    ouvrants = {")": "(", "]": "[", "}": "{"}

    pile = Pile()
    for delimiteur in expression:
        if delimiteur in "([{":
            pile.empile(delimiteur)
        else:
            if pile.est_vide():
                return False
            elif pile.depile() != ouvrants[delimiteur]:
                return False

    return pile.est_vide()
# --------- PYODIDE:tests --------- #

assert est_bien_delimitee('([()[]]{()})')
assert est_bien_delimitee('{}[(])') is False
assert est_bien_delimitee('[][') is False
assert est_bien_delimitee('[]]') is False

# --------- PYODIDE:secrets --------- #

# Tests supplémentaires
assert est_bien_delimitee('')
assert est_bien_delimitee('<<>>')
assert est_bien_delimitee("(" * 10 + ")" * 10)
assert est_bien_delimitee("([" * 10 + "])" * 10)
assert est_bien_delimitee("([{" * 10 + "}])" * 10)
assert est_bien_delimitee("{" * 10 + "}" * 10 + ")") is False
assert est_bien_delimitee("{" * 10 + ")" * 10) is False
