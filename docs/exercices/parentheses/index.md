---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Parenthésage correct
tags:
    - à trous
    - pile
    - ep2
difficulty: 250
---

{{ version_ep() }}

Dans cet exercice, on étend le problème de parenthésage à l'ensemble des **délimiteurs**, à savoir parenthèses, crochets et accolades.

On dispose de chaînes de caractères contenant **uniquement** : 

* des parenthèses ouvrantes `'('` et fermantes `')'`,
  
* des crochets ouvrants `'['` et fermants `']'`,

* des accolades ouvrantes `'{'` et fermantes `'}'`.

Un parenthésage est correct si :

* pour chaque délimiteur, le nombre de fermants coïncide avec le nombre d'ouvrants,
* en parcourant la chaîne de gauche à droite, pour chaque délimiteur, le nombre d'ouvrants est à tout moment supérieur ou égal au nombre de fermants,
* les différents délimiteurs ne s'entre-croisent pas, par exemple `'[(])'` est incorrect.

!!! success "Bien parenthésées"
    - `(2 + 4)*7`
    - `tableau[f(i) - g(i)]`
    - `#include <stdio.h> int main(){int liste[2] = {4, 2}; return (10*liste[0] + liste[1]);}`

!!! bug "Mauvais parenthésage"

    - `(une parenthèse laissée ouverte` ; pas de fermante associée à `(`.
    - `{<(}>)` ; mauvaise imbrication.
    - `c'est trop tard ;-)` ; pas d'ouvrante associée à `)`.


On souhaite programmer une fonction `est_bien_delimitee` qui prend en paramètre une chaîne de délimiteurs `expression` et renvoie `True` ou `False` selon qu'elle est correctement délimitée ou non.

On utilise un dictionnaire pour associer facilement délimiteurs fermants et ouvrants.

???+ example "Exemples"

    ```pycon title=""
    >>> est_bien_delimitee('([()[]]{()})')
    True
    >>> est_bien_delimitee('{}[(])')
    False
    >>> est_bien_delimitee('[][')
    False
    >>> est_bien_delimitee('[]]')
    False
    ```

=== "Version vide" 
    {{ IDE('exo') }}

=== "Version à compléter"
    {{ IDE('exo_b') }}
    

??? tip "Indice 1"
    On utilisera une pile qui empile les ouvrants, dépile un ouvrant dès qu'un fermant est rencontré en vérifiant la correspondance, et qui ignore les autres caractères.

    La classe `Pile` est fournie ci-dessous. Elle est déjà "chargée" en mémoire et donc utilisable sans import dans la zone de saisie.


    {{ remarque('classe_Pile' )}}


??? tip "Indice 2"
    On adopte la démarche suivante utilisant une *Pile* :

    * on parcourt les caractères de la chaîne,
    * si le caractère lu est un délimiteur ouvrant on l'empile,
    * si c'est un délimiteur fermant :
        * si la pile est vide, on renvoie `False`,
        * sinon, si le caractère dépilé n'est pas le délimiteur ouvrant du caractère lu, on renvoie `False`,
    * à la fin du parcours, si la pile est vide on renvoie `True`, sinon `False`.
