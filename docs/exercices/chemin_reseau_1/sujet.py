# --- vide --- #
def trouver_chemin(reseau, depart, arrivee):
    ...


# --- exo --- #
def trouver_chemin(reseau, depart, arrivee):
    position = depart
    chemin = [...]
    while not position == arrivee:
        table_routage = reseau[...]
        if arrivee in ...:
            position = table_routage[...]
        else:
            position = table_routage[...]
        chemin.append(...)
    return ...


# --- corr --- #
def trouver_chemin(reseau, depart, arrivee):
    position = depart
    chemin = [depart]
    while not position == arrivee:
        table_routage = reseau[position]
        if arrivee in table_routage:
            position = table_routage[arrivee]
        else:
            position = table_routage["Autres"]
        chemin.append(position)
    return chemin


# --- tests --- #
mon_reseau = {
    "Lycee": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Webcam": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Routeur1": {"Lycee": "Lycee", "Webcam": "Webcam", "Autres": "Routeur2"},
    "Routeur2": {"Lycee": "Routeur1", "Webcam": "Routeur1", "Autres": "Routeur4"},
    "Routeur4": {"Autres": "Routeur3"},
    "Routeur3": {"Maison": "Maison", "Autres": "Routeur2"},
    "Maison": {"Routeur3": "Routeur3", "Autres": "Routeur3"},
}

assert trouver_chemin(mon_reseau, "Webcam", "Lycee") == ["Webcam", "Routeur1", "Lycee"]
assert trouver_chemin(mon_reseau, "Webcam", "Maison") == [
    "Webcam",
    "Routeur1",
    "Routeur2",
    "Routeur4",
    "Routeur3",
    "Maison",
]

# --- secrets --- #
mon_reseau = {
    "Lycee": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Webcam": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Routeur1": {"Lycee": "Lycee", "Webcam": "Webcam", "Autres": "Routeur2"},
    "Routeur2": {"Lycee": "Routeur1", "Webcam": "Routeur1", "Autres": "Routeur4"},
    "Routeur4": {"Autres": "Routeur3"},
    "Routeur3": {"Maison": "Maison", "Autres": "Routeur2"},
    "Maison": {"Routeur3": "Routeur3", "Autres": "Routeur3"},
}

assert trouver_chemin(mon_reseau, "Maison", "Lycee") == [
    "Maison",
    "Routeur3",
    "Routeur2",
    "Routeur1",
    "Lycee",
], f"Erreur avec {mon_reseau}"

mon_reseau = {
    "Lycee": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Webcam": {"Routeur1": "Routeur1", "Autres": "Routeur1"},
    "Routeur1": {"Lycee": "Lycee", "Webcam": "Webcam", "Autres": "Routeur2"},
    "Routeur2": {"Lycee": "Routeur1", "Webcam": "Routeur1", "Autres": "Routeur3"},
    "Routeur4": {"Autres": "Routeur2"},
    "Routeur3": {"Maison": "Maison", "Autres": "Routeur4"},
    "Maison": {"Routeur3": "Routeur3", "Autres": "Routeur3"},
}
assert trouver_chemin(mon_reseau, "Maison", "Lycee") == [
    "Maison",
    "Routeur3",
    "Routeur4",
    "Routeur2",
    "Routeur1",
    "Lycee",
], f"Erreur avec {mon_reseau}"
assert trouver_chemin(mon_reseau, "Webcam", "Lycee") == [
    "Webcam",
    "Routeur1",
    "Lycee",
], f"Erreur avec {mon_reseau}"
assert trouver_chemin(mon_reseau, "Webcam", "Maison") == [
    "Webcam",
    "Routeur1",
    "Routeur2",
    "Routeur3",
    "Maison",
], f"Erreur avec {mon_reseau}"
