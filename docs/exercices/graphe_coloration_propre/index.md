---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Coloration propre d'un graphe
tags:
    - graphe
difficulty: 250
maj: 11/04/2024
---


La coloration d'un graphe est dit *propre* lorsque deux sommets voisins quelconques n'ont pas la même couleur.

La coloration du graphe ci-dessous est propre.


??? info "Coloration"

	Les sommets sont colorés avec quatre couleurs. Aucun sommet n'a un voisin de la même couleur.

	![Coloration propre](./coloration_propre.svg){ width=35%; : .center }

<!--
	<center>
	```mermaid
	graph LR
		classDef jaune fill:#ff0, color:#38474f;
		classDef bleu fill:#0ff, color:#38474f;
		classDef rouge fill:#f00, color:#38474f;
		classDef vert fill:#0f0, color:#38474f;
		n0((0)) --- n1((1))
		class n0 jaune
		class n1 bleu
		n0 --- n2((2))
		class n2 vert
		n0 --- n3((3))
		class n3 bleu
		n1 --- n4((4))
		class n4 rouge
		n3 --- n5((5))
		class n5 jaune
		n4 --- n5
		n3 --- n6((6))
		class n6 rouge
		n3 --- n7((7))
		class n7 jaune
		n7 --- n1
		n4 --- n2
		n6 --- n1
	```
	</center>
-->
	

Un graphe est implémenté à l'aide d'un dictionnaire. Un sommet est représenté par son numéro. Les clés du dictionnaire sont les sommets. La valeur associée à une clé `s` est la liste des voisins du sommet `s`. 
Par exemple, la graphe plus haut est représenté par: `#!py {0: [1, 2, 3], 1: [0, 4, 6, 7], 2: [0, 4], ...}`.

La coloration du graphe est créée par un autre dictionnaire qui associe à chaque numéro de sommet une chaîne de caractères qui désigne sa couleur.
Par exemple, la coloration du graphe plus haut est représentée par: `#!py {0: "jaune", 1: "bleu"", 2: "vert", ...}`.


Compléter la fonction `coloration_propre` qui prend en paramètres un graphe `graphe` supposé connexe et un dictionnaire `couleurs` qui stocke les couleurs associées et renvoie `True` si la coloration du graphe est propre et `False` sinon.

???+ example "Exemples"

    ```pycon title=""
    >>> g = {0: [1, 2], 1: [0, 2], 2: [0, 1]}
	>>> couleurs = {0:"rouge", 1:"bleu", 2:"vert", 3:"jaune"}
	>>> coloration_propre(g, couleurs)
    True
    >>> g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
	>>> couleurs = {0:"rouge", 1:"rouge", 2:"vert", 3:"jaune"}
	>>> coloration_propre(g, couleurs)
    False
    ```

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
