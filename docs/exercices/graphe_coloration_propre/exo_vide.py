

# --------- PYODIDE:code --------- #


def coloration_propre(graphe, couleurs):
    ...
            

# --------- PYODIDE:corr --------- #


def coloration_propre(graphe, couleurs):
    for sommet in graphe:
        for voisin in graphe[sommet]:
            if couleurs[voisin] == couleurs[sommet]:
                return False
    return True


# --------- PYODIDE:tests --------- #

couleurs = {0:"rouge", 1:"bleu", 2:"vert", 3:"jaune"}
g = {0: [1, 2], 1: [0, 2], 2: [0, 1]}
assert coloration_propre(g, couleurs)
couleurs = {0: "rouge", 1: "rouge", 2: "vert", 3: "jaune"}
g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
assert not coloration_propre(g, couleurs)

# --------- PYODIDE:secrets --------- #


# tests secrets
couleurs_sommets = {0: "rouge", 1: "bleu", 2: "vert", 3: "jaune"}
g = {0: [1, 2], 1: [0, 2], 2: [0, 1]}
assert coloration_propre(g, couleurs_sommets) is True
couleurs_sommets = {0: "rouge", 1: "rouge", 2: "vert", 3: "jaune"}
g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
assert coloration_propre(g, couleurs_sommets) is False


couleurs_sommets = {0: "rouge", 1: "bleu", 2: "vert", 3: "jaune",4: "rouge",
                    5: "bleu", 6: "vert", 7: "jaune", 8: "rouge", 9: "bleu"}
g1 = {0: [1, 4],
    1: [0, 2, 3, 4, 5],
    2: [1],
    3: [1],
    4: [0, 1, 5],
    5: [1, 4]}

assert coloration_propre(g, couleurs_sommets) is True

g2 = {0: [1],
    1: [0, 2, 3, 4],
    2: [1],
    3: [1],
    4: [1, 5],
    5: [4]}

assert coloration_propre(g2, couleurs_sommets) is True

g3 = {0: [1, 4],
    1: [0, 2, 3, 5],
    2: [1],
    3: [1],
    4: [0, 5],
    5: [1, 4]}

assert coloration_propre(g3, couleurs_sommets) is False

g4 = {0: [1, 4],
    1: [0, 2, 3],
    2: [1],
    3: [1],
    4: [0, 5],
    5: [4]}

assert coloration_propre(g4, couleurs_sommets) is False

g5 = {0: [1, 8, 9], 1: [0, 3, 4, 7], 2: [3, 5, 7], 3: [1, 2, 6, 9],
     4: [1, 6, 8, 9], 5: [2, 8, 9], 6: [3, 4, 7], 7: [1, 2, 6, 8],
     8: [0, 4, 5, 7], 9: [0, 3, 4, 5]}

assert coloration_propre(g5, couleurs_sommets) is False
