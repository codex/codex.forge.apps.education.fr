---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Les durées (I)
tags:
    - tuple
difficulty: 80
maj: 01/03/2024
---


Les durées peuvent être exprimées en secondes, en minutes-secondes, ou en heures-minutes-secondes.

Ainsi, la durée « 6 h 34 min et 12 s » peut être exprimée par :

```python
duree_h_min_sec = (6, 34, 12)  # en heures-minutes-secondes
duree_sec = 23652  # en secondes
```

On souhaite pouvoir passer d'une expression à l'autre.

Écrire la fonction `convertir_en_secondes` qui prend en paramètre `duree` (un tuple de trois entiers représentant la durée en heures-minutes-secondes) et renvoie le nombre total de secondes.

???+ example "Exemple"

    ```pycon title=""
    >>> duree_a_convertir = (1, 25, 50)
    >>> convertir_en_secondes(duree_a_convertir)
    5150
    >>> convertir_en_secondes((6, 34, 12))
    23652
    ```

{{ IDE('exo') }}
