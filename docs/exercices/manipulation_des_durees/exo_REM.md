La correction proposée repose sur le procédé de décompactage, *unpacking* en anglais, qui consiste à affecter les valeurs d'un p-uplet dans différentes variables et ce, en une seule instruction.

Ainsi, les instructions suivantes :

```python
heures = duree[0]
minutes = duree[1]
secondes = duree[2]
```

sont équivalentes à celle-ci :

```python
heures, minutes, secondes = duree
```

Dans ce cas, il faut naturellement qu'il y ait autant de variables dans la partie gauche de l'affectation que d'éléments dans le p-uplet.