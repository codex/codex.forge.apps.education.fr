

# --------- PYODIDE:code --------- #

def convertir_en_secondes(duree):
    ...

# --------- PYODIDE:corr --------- #

def convertir_en_secondes(duree):
    heures, minutes, secondes = duree
    return heures * 3600 + minutes * 60 + secondes

# --------- PYODIDE:tests --------- #

duree_a_convertir = (1, 25, 50)
assert convertir_en_secondes(duree_a_convertir) == 5150
assert convertir_en_secondes((6, 34, 12)) == 23652

# --------- PYODIDE:secrets --------- #


# Tests secrets
assert convertir_en_secondes((0, 0, 35)) == 35
assert convertir_en_secondes((0, 1, 1)) == 61
assert convertir_en_secondes((2, 0, 0)) == 7200
assert convertir_en_secondes((1, 1, 1)) == 3661
assert convertir_en_secondes((0, 2, 0)) == 120
assert convertir_en_secondes((1, 0, 0)) == 3600
assert convertir_en_secondes((0, 1, 0)) == 60
assert convertir_en_secondes((0, 0, 1)) == 1
assert convertir_en_secondes((0, 0, 0)) == 0