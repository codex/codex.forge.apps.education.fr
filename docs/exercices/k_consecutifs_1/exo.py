

# --------- PYODIDE:code --------- #

def somme_maxi(valeurs, k):
    ...

# --------- PYODIDE:corr --------- #

def somme_maxi(valeurs, k):
    k_somme = 0
    for i in range(k):
        k_somme += valeurs[i]
    maxi = k_somme
    for i in range(k, len(valeurs)):
        k_somme += valeurs[i] - valeurs[i - k]
        if k_somme > maxi:
            maxi = k_somme
    return maxi

# --------- PYODIDE:tests --------- #

assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 7
assert somme_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3

# --------- PYODIDE:secrets --------- #


# autres tests

valeurs = [-42] * 1000
assert somme_maxi(valeurs, 100) == -42 * 100, "Erreur avec tableau constant"

assert somme_maxi(list(range(1000)), 100) == sum(range(900, 1000))
assert somme_maxi(list(range(-1000, 0)), 100) == -sum(range(101))
assert somme_maxi(list(range(1000, 0, -1)), 100) == sum(range(901, 1001))
assert somme_maxi(list(range(0, -1000, -1)), 100) == -sum(range(100))