---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 250
hide:
    - navigation
    - toc
title: Somme maximale de k termes consécutifs
tags:
    - programmation dynamique
---



Écrire une fonction `somme_maxi` qui prend en paramètres un tableau d'entiers `valeurs`, et un entier strictement positif `k`. Cette fonction doit renvoyer la somme maximale de `k` entiers consécutifs du tableau `valeurs`.

On garantit que le tableau `valeurs` est de taille au moins égale à `k` et que `k` est un entier strictement positif.

???+ example "Exemple"

    ```pycon title=""
    >>> somme_maxi([0, 1, 2, 3, 2, 1, 0], 3)  # pour les termes consécutifs 2, 3, 2
    7
    >>> somme_maxi([0, 1, 2, 3, 2, 1, 0], 1)  # pour le terme 3
    3
    ```

{{ IDE('exo') }}


??? tip "Indice 1"
    On pourra commencer par faire le cumul des `k` premières valeurs pour initialiser une variable `maxi`.

    On pourra ensuite faire une boucle qui ajoute la valeur suivante et retranche la première valeur.

??? tip "Indice 2"
    On pourra s'aider du code à trou

    ```python
    def somme_maxi(valeurs, k):
        k_somme = ...
        for i in range(k):
            k_somme += ...
        maxi = k_somme
        for i in range(..., ...):
            k_somme += valeurs[...] - valeurs[...]
            if k_somme > maxi:
                maxi = ...
        return ...
    ```
