

# --------- PYODIDE:code --------- #

schroder_mem = [...]


def schroder(n):
    if n >= len(schroder_mem):
        resultat = ...  # ... schroder(n - 1) ...
        # ici schroder_mem est de longueur au moins n, garanti
        schroder_mem.append(...)
    return schroder_mem[...]

# --------- PYODIDE:corr --------- #

schroder_mem = [1, 2]


def schroder(n):
    if n >= len(schroder_mem):
        resultat = ((6 * n - 3) * schroder(n - 1) - (n - 2) * schroder(n - 2)) // (
            n + 1
        )
        # ici schroder_mem est de longueur n garanti
        schroder_mem.append(resultat)
    return schroder_mem[n]

# --------- PYODIDE:tests --------- #

assert schroder(2) == 6
assert schroder(3) == 22
assert schroder(4) == 90

# --------- PYODIDE:secrets --------- #


# autres tests

A006318 = [
    1,
    2,
    6,
    22,
    90,
    394,
    1806,
    8558,
    41586,
    206098,
    1037718,
    5293446,
    27297738,
    142078746,
    745387038,
    3937603038,
    20927156706,
    111818026018,
    600318853926,
    3236724317174,
    17518619320890,
    95149655201962,
    518431875418926,
    2832923350929742,
    15521467648875090,
]

for n, attendu in enumerate(A006318):
    resultat = schroder(n)
    assert isinstance(resultat, int), "Erreur, le résultat doit être entier"
    assert attendu == resultat, f"Erreur avec n = {n}"