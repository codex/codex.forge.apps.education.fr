---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Nombre de bits
tags:
    - int
    - binaire
---



Un ordinateur manipule des nombres écrits en binaire : uniquement avec les chiffres « $0$ » et « $1$ ». Par exemple le nombre $26$ s'écrit $11010$ en binaire.

 Chaque « $0$ » et « $1$ » d'une écriture binaire est appelé *bit*. Ainsi, le nombre $26$ s'écrit en binaire sur $5$ bits.

 Pour savoir combien de bits sont nécessaires pour écrire en binaire un nombre entier strictement positif **on compte le nombre de divisions euclidiennes par $2$ nécessaires pour obtenir un quotient nul**.

En partant de $26$ on a :

* $26 = 2 \times \mathbf{13} + 0$ ;
* $13 = 2 \times \mathbf{6} + 1$ ;
* $6 = 2 \times \mathbf{3} + 0$ ;
* $3 = 2 \times \mathbf{1} + 1$ ;
* $1 = 2 \times \mathbf{0} + 1$.

Comme on peut le voir, $5$ divisions euclidiennes ont été nécessaires pour passer de $26$ à $0$ : $26$ s'écrit donc sur $5$ bits en binaire.

On rappelle que l'opérateur `//` permet d'obtenir le quotient de deux nombres : `#!py 13 // 2` est évalué à `#!py 6`.

Écrire la fonction `nb_bits` qui prend en argument un nombre entier **strictement positif** et renvoie le nombre de bits nécessaires à son écriture en binaire.

!!! danger "Contrainte"

    On interdit d'utiliser la fonction `bin` !

???+ example "Exemples"

    ```pycon title=""
    >>> nb_bits(1)
    1
    >>> nb_bits(2)
    2
    >>> nb_bits(3)
    2
    >>> nb_bits(4)
    3
    ```

{{ IDE('exo', SANS='bin') }}
