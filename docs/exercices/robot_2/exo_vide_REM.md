Il est aussi possible d'unifier les formats des instructions en procédant ainsi :

```python
def execute(robot, instructions):
    paires = []
    i = 0
    while i < len(instructions):
        instru = instructions[i]
        if i < len(instructions) - 1 and isinstance(instructions[i + 1], int):
            paires.append((instru, instructions[i + 1]))
            i += 2
        else:
            paires.append((instru, 1))
            i += 1
            
    fonction = {"A": robot.avance, "D": robot.droite, "G": robot.gauche}
    for action, n in paires:
        for _ in range(n):
            fonction[action]()
```