# --------- PYODIDE:env --------- #
MOUVEMENTS = ((0, 1), (1, 0), (0, -1), (-1, 0))


def decoupe(instructions):
    return [int(c) if c.isnumeric() else c for c in instructions]


class Robot:
    def __init__(self, hauteur, largeur):
        self.hauteur = hauteur
        self.largeur = largeur
        self.grille = [[" " for _ in range(largeur)] for _ in range(hauteur)]
        self.i = 0
        self.j = 0
        self.grille[self.i][self.j] = "*"
        self.direction = 0

    def avance(self):
        """Fait avancer le robot d'une case (seulement si possible)"""
        di, dj = MOUVEMENTS[self.direction]
        if 0 <= self.i + di < self.hauteur and 0 <= self.j + dj < self.largeur:
            self.i += di
            self.j += dj
            self.grille[self.i][self.j] = "*"

    def droite(self):
        """Fait tourner le robot d'un quart de tour vers la droite"""
        self.direction = (self.direction + 1) % 4

    def gauche(self):
        """Fait tourner le robot d'un quart de tour vers la gauche"""
        self.direction = (self.direction - 1) % 4

    def dessine_parcours(self):
        """Affiche les cases parcourues et la position actuelle du robot"""
        affichage = [
            ["" for _ in range(self.largeur + 2)] for _ in range(self.hauteur + 2)
        ]
        for j in range(1, self.largeur + 1):
            affichage[0][j] = "─"
            affichage[-1][j] = "─"
        for i in range(1, self.hauteur + 1):
            affichage[i][0] = "│"
            affichage[i][-1] = "│"
        affichage[0][0] = "┌"
        affichage[-1][0] = "└"
        affichage[0][-1] = "┐"
        affichage[-1][-1] = "┘"
        for i in range(self.hauteur):
            for j in range(self.largeur):
                affichage[1 + i][1 + j] = self.grille[i][j]
        affichage[self.i + 1][self.j + 1] = [">", "v", "<", "^"][self.direction]
        print("\n".join("".join(ligne) for ligne in affichage))


# --------- PYODIDE:code --------- #
def execute(robot, instructions):
    ...


# --------- PYODIDE:corr --------- #
def execute(robot, instructions):
    i = 0
    while i < len(instructions):
        # lecture de l'instruction
        instru = instructions[i]
        if i < len(instructions) - 1 and isinstance(instructions[i + 1], int):
            n = instructions[i + 1]
            i += 2
        else:
            n = 1
            i += 1

        # exécution de l'instruction
        for _ in range(n):
            if instru == "D":
                robot.droite()
            elif instru == "G":
                robot.gauche()
            else:
                robot.avance()


# --------- PYODIDE:tests --------- #
robot = Robot(4, 5)
instructions = ["A", 8, "D", 41, "A"]
attendu = [
    ["*", "*", "*", "*", "*"],
    [" ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
]
execute(robot, instructions)
assert robot.grille == attendu


robot = Robot(4, 5)
code = ["A", 4, "G", 1, "A", "A", "D", 2, "A", 0]
attendu = [
    ["*", "*", "*", "*", "*"],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
]
execute(robot, code)
assert robot.grille == attendu
# --------- PYODIDE:secrets --------- #
hauteur = 3
largeur = 3
instructions = list("AADAADAADADA")
attendu = [["*", "*", "*"], ["*", "*", "*"], ["*", "*", "*"]]
robot = Robot(hauteur, largeur)
execute(robot, instructions)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 4
largeur = 10
robot = Robot(hauteur, largeur)
code = ["A", 100, "D", 41, "A", 35, "D", 40, "A"]
attendu = [
    ["*", "*", "*", "*", "*", "*", "*", "*", "*", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"
