

# --------- PYODIDE:env --------- #

def trier_valeurs(dico):
    for cle, valeur in dico.items():
        dico[cle] = sorted(valeur)
    return dico

# --------- PYODIDE:code --------- #

def renverser(dico):
    ...

# --------- PYODIDE:corr --------- #

def renverser(dico):
    dico_renverse = {}
    for cle, valeur in dico.items():
        if valeur not in dico_renverse:
            dico_renverse[valeur] = [cle]
        else:
            dico_renverse[valeur].append(cle)
    return dico_renverse

# --------- PYODIDE:tests --------- #

dico = {"a": 5, "b": 7}
assert trier_valeurs(renverser(dico)) == {5: ["a"], 7: ["b"]}

dico = {"a": 5, "b": 7, "c": 5}
assert trier_valeurs(renverser(dico)) == {5: ["a", "c"], 7: ["b"]}

dico = {
    "Paris": "Tour Eiffel",
    "Rome": "Colisée",
    "Berlin": "Reichtag",
    "Londres": "Big Ben",
}
assert trier_valeurs(renverser(dico)) == {
    "Tour Eiffel": ["Paris"],
    "Colisée": ["Rome"],
    "Reichtag": ["Berlin"],
    "Big Ben": ["Londres"],
}

dico = {"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}
assert trier_valeurs(renverser(dico)) == {
    "P": ["Paris"],
    "L": ["Lille", "Lyon"],
    "N": ["Nantes"],
}

# --------- PYODIDE:secrets --------- #


# autres tests

assert renverser({}) == {}
assert renverser({"a": "a"}) == {"a": ["a"]}
assert trier_valeurs(renverser({1: 2, 2: 2, 3: 2})) == {2: [1, 2, 3]}