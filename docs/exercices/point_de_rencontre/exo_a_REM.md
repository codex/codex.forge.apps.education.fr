
La variable `rencontre` n'est pas forcément nécessaire :

```python
def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = len(trajet2) - 1
    while True:
        if i1 == 0 or i2 == 0 or trajet1[i1-1] != trajet1[i2-1]:
            return trajet1[i1]
        i1 -= 1
        i2 -= 1
```

La boucle va forcément se terminer puisque dans le pire des cas, on fera autant de tours de boucles que la longueur du plus petit trajet.


On peut également faire une version naïve :

```python
def point_de_rencontre(trajet1, trajet2):
    for ville in trajet1:
        if ville in trajet2:
            return ville
```

Cette fonction est correcte mais le nombre de tests à effectuer peut devenir très grand. Si les deux trajets sont de longueur $n$, le nombre de comparaisons à faire est de l'ordre de $n^2$. La complexité de la fonction est donc quadratique, alors que la solution proposée est linéaire.

