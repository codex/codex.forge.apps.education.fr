---
author: Romain Janvier
hide:
    - navigation
    - toc
difficulty: 250
title: Point de rencontre
tags:
    - liste/tableau
    - à trous
---

Un site de co-voiturage propose un nouveau service. Lorsque deux utilisateurs du sites souhaitent aller au même endroit le même jour, il suffit qu'ils rentrent leur trajet pour savoir dans quelle ville ils doivent se retrouver pour ensuite co-voiturer.

Les trajets sont représentés par des listes de noms de villes. Par exemple pour aller à Paris en partant de Valence, le trajet peut-être :

`#!py ["Valence", "Vienne", "Lyon", "Beaune", "Auxerre", "Paris"]`

Si un autre utilisateur veut aller de Dijon à Paris il peut prendre le trajet :

`#!py ["Dijon", "Pouilly-en-Auxois", "Auxerre", "Paris"]`

La ville de Auxerre peut servir de point de rencontre sur les deux trajets.

Écrire le code de la fonction `point_de_rencontre` qui prend deux listes de chaînes de caractères `trajet1` et `trajet2`, et qui renvoie la première ville commune entre les deux trajets.

On suppose que :

* les trajets donnés en paramètres ont forcément au moins une ville en commun ;
* À partir du moment où une ville se trouve dans les deux listes, toutes les villes suivantes sont identiques.

```pycon
>>> trajet1 = ["Valence", "Vienne", "Lyon", "Beaune", "Auxerre", "Paris"]
>>> trajet2 = ["Dijon", "Pouilly-en-Auxois", "Auxerre", "Paris"]
>>> trajet3 = ["Rouen", "Mantes-la-Jolie", "Paris"]
>>> point_de_rencontre(trajet1, trajet2)
'Auxerre'
>>> point_de_rencontre(trajet1, trajet1)
'Valence'
>>> point_de_rencontre(trajet1, trajet3)
'Paris'
```

??? tip "Indice"
    Il peut être judicieux de partir de la fin et de remonter les deux listes tant qu'aucune n'est vide et qu'on a des villes communes.

=== "Version vide"
    {{ IDE('exo_a') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}



