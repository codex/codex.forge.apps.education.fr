

# --------- PYODIDE:code --------- #

def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = ...
    rencontre = ""
    while ...:
        rencontre = ...
        i1 = ...
        i2 = ...
    return rencontre

# --------- PYODIDE:corr --------- #

def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = len(trajet2) - 1
    rencontre = ""
    while i1 >= 0 and i2 >= 0 and trajet1[i1] == trajet2[i2]:
        rencontre = trajet1[i1]
        i1 = i1 - 1
        i2 = i2 - 1
    return rencontre

# --------- PYODIDE:tests --------- #

trajet1 = ["Valence", "Vienne", "Lyon", "Beaune", "Auxerre", "Paris"]
trajet2 = ["Dijon", "Pouilly-en-Auxois", "Auxerre", "Paris"]
trajet3 = ["Rouen", "Mantes-la-Jolie", "Paris"]
assert point_de_rencontre(trajet1, trajet2) == "Auxerre"
assert point_de_rencontre(trajet1, trajet1) == "Valence"
assert point_de_rencontre(trajet1, trajet3) == "Paris"

# --------- PYODIDE:secrets --------- #


# tests secrets
tj1 = ["A"]
tj2 = ["B", "C", "A"]
tj3 = ["D"] + tj2
assert point_de_rencontre(tj1, tj1) == "A"
assert point_de_rencontre(tj1, tj2) == "A"
assert point_de_rencontre(tj2, tj1) == "A"
assert point_de_rencontre(tj2, tj3) == "B"
assert point_de_rencontre(tj3, tj2) == "B"