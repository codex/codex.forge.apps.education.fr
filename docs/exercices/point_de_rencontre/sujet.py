# --- exo --- #
def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = ...
    rencontre = ""
    while ...:
        rencontre = ...
        i1 = ...
        i2 = ...
    return rencontre

# --- vide --- #
def point_de_rencontre(trajet1, trajet2):
    ...

# --- corr --- #
def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = len(trajet2) - 1
    rencontre = ""
    while i1 >= 0 and i2 >= 0 and trajet1[i1] == trajet2[i2]:
        rencontre = trajet1[i1]
        i1 = i1 - 1
        i2 = i2 - 1
    return rencontre

# --- tests --- #
trajet1 = ["Valence", "Vienne", "Lyon", "Beaune", "Auxerre", "Paris"]
trajet2 = ["Dijon", "Pouilly-en-Auxois", "Auxerre", "Paris"]
trajet3 = ["Rouen", "Mantes-la-Jolie", "Paris"]
assert point_de_rencontre(trajet1, trajet2) == "Auxerre"
assert point_de_rencontre(trajet1, trajet1) == "Valence"
assert point_de_rencontre(trajet1, trajet3) == "Paris"
# --- secrets --- #
tj1 = ["A"]
tj2 = ["B", "C", "A"]
tj3 = ["D"] + tj2
assert point_de_rencontre(tj1, tj1) == "A"
assert point_de_rencontre(tj1, tj2) == "A"
assert point_de_rencontre(tj2, tj1) == "A"
assert point_de_rencontre(tj2, tj3) == "B"
assert point_de_rencontre(tj3, tj2) == "B"
# --- rem --- #
''' # skip

La variable `rencontre` n'est pas forcément nécessaire :

```python
def point_de_rencontre(trajet1, trajet2):
    i1 = len(trajet1) - 1
    i2 = len(trajet2) - 1
    while True:
        if i1 == 0 or i2 == 0 or trajet1[i1-1] != trajet1[i2-1]:
            return trajet1[i1]
        i1 -= 1
        i2 -= 1
```

La boucle va forcément se terminer puisque dans le pire des cas, on fera autant de tours de boucles que la longueur du plus petit trajet.


On peut également faire une version naïve :

```python
def point_de_rencontre(trajet1, trajet2):
    for ville in trajet1:
        if ville in trajet2:
            return ville
```

Cette fonction est correcte mais le nombre de tests à effectuer peut devenir très grand. Si les deux trajets sont de longueur $n$, le nombre de comparaisons à faire est de l'ordre de $n^2$. La complexité de la fonction est donc quadratique, alors que la solution proposée est linéaire.

''' # skip
