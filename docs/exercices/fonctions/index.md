---
license: "by-nc-sa"
author:
    - Jean Diraison
    - Franck Chambon
hide:
    - navigation
    - toc
difficulty: 50
title: Fonctions simples
tags:
    - fonctions
---

Les questions ci-dessous sont **indépendantes**. Vous devez dans chacune rédiger une courte fonction en **Python**.

{{ remarque('assertion') }}

??? question "Fonction 0"

    Complétez la fonction suivante, sachant qu'elle traduit l'expression mathématique

    $$f(x) = 4x + 7$$

    {{ remarque('assertion') }}
    
    {{ IDE('exo_0') }}



??? question "Fonction 1"

    Vous devez créer une fonction qui prend en paramètre un nombre $x$ et qui renvoie $5x-3$.

    {{ IDE('exo_1') }}



??? question "Fonction 2"

    Complétez l'écriture de la fonction $g$ qui prend en paramètre un nombre $x$ et qui renvoie $(x+1)^2$.

    {{ IDE('exo_2') }}


??? question "Fonction 3"

    Complétez la fonction suivante, sachant qu'elle traduit l'expression mathématique

    $$f(x) = 5x^2 -3x + 1$$

    {{ IDE('exo_3') }}



??? question "Fonction 4"

    Corrigez la fonction `produit` pour qu'elle prenne deux paramètres `k` et `n`, et renvoie leur produit.

    {{ IDE('exo_4') }}



??? question "Fonction 5"

    !!! info "Aire d'un trapèze"
        ![](./exo_5_trapeze_0.svg){ .autolight align=left }
        Un trapèze est un quadrilatère non croisé ayant deux côtés parallèles ; ses bases. Son aire est égale à la moyenne des bases, multipliée par la hauteur associée.

        $$\mathscr A_\text{trapèze} = \frac{b_1 + b_2}2 × h$$

    Complétez la fonction `aire_trapeze` qui renvoie l'aire d'un trapèze.

    Elle a pour paramètres :

    - `base_1` : la longueur d'une base
    - `base_2` : la longueur de l'autre base
    - `hauteur` : la hauteur associée

    {{ IDE('exo_5') }}


    ???+ example "Exemples de trapèzes"
        ![](./exo_5_trapeze_1.svg){ .autolight } ![](./exo_5_trapeze_2.svg){ .autolight } ![](./exo_5_trapeze_3.svg){ .autolight }


??? question "Fonction 6"

    Créez la fonction `perimetre`. Cette fonction prend en paramètres deux nombres `L1` et `L2` qui sont les longueurs des deux côtés du rectangle. Elle renvoie le périmètre du rectangle correspondant.

    {{ IDE('exo_6') }}



??? question "Fonction 7"

    Créez la fonction `circonference`. Cette fonction prend pour paramètre le nombre`rayon` qui est le rayon d'un cercle. Elle renvoie le périmètre du cercle correspondant.

    > On définira la constante `PI` de valeur `#!py 3.1415926` dans le script.

    {{ IDE('exo_7') }}



??? question "Fonction 8"

    Complétez la fonction `Celsius_depuis_Fahrenheit` qui permet de convertir une température donnée en degré Fahrenheit en une température en degré Celsius.

    Aujourd'hui, l'échelle Fahrenheit est calée sur l'échelle Celsius par la relation : 

    $$T(°F) = \frac95 T(°C) + 32$$

    {{ IDE('exo_8') }}



??? question "Fonction 9"

    Un parc d'attractions affiche les tarifs suivants :

    - 8,50 € par enfant
    - 12,00 € par adulte

    Vous devez compléter la fonction `prix` qui renvoie le prix total à payer, à partir du nombre `n` d'enfants et du nombre `p` d'adultes.

    {{ IDE('exo_9') }}

