

# --------- PYODIDE:code --------- #

def f(x):
    y = ...  # ligne à modifier
    return y

# --------- PYODIDE:corr --------- #

def f(x):
    y = 5 * x * x - 3 * x + 1
    return y

# --------- PYODIDE:tests --------- #

assert f(0) == 1
assert f(1) == 3
assert f(-1) == 9

# --------- PYODIDE:secrets --------- #


# autres tests

for x in range(-10, 20):
    attendu = (5 * x - 3) * x + 1
    assert f(x) == attendu, f"Erreur avec {x=}"