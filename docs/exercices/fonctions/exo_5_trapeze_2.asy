// © 2022, Franck Chambon

import math;
unitsize(1cm);

add(grid(7, 4, black+dotted));
pair A = (1, 1), B = (4, 1), C = (6, 3), D = (1, 3);
pair H = (1, 1);

draw(A--B--C--D--cycle, 1bp+black);
draw(H--D, 0.5bp+black);

label("$3$", (A + B)/2, S);
label("$5$", (C + D)/2, N);
label("$2$", (H + D)/2, NE);


label("L'aire est de 8.", (3.5, -0.5));

shipout(bbox(2.5mm, nullpen));
