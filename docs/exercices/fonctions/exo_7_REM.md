:warning: La réponse est simple, mais il faut évoquer un point

!!! failure "Pas d'égalité entre flottant"

    Dans les tests, nous avons écrit

    ```python
    assert circonference(0) == 0
    assert circonference(1) == 6.2831852
    ```

    Cette méthode peut être source d'erreurs, il ne faut pas faire de comparaisons entre flottants.

!!! success "On remplace le test d'égalité de deux flottants par un test de proximité des deux flottants"

    ```python
    def sont_proches(x, y):
        return abs(x - y) < 10**-9

    assert sont_proches(circonference(0), 0)
    assert sont_proches(circonference(1), 6.2831852)
    ```

    `abs(x - y) < 10**-9` permet de tester si la distance entre les nombres `x` et `y` est plus petite que 0,000000001.

    Voilà une meilleure méthode. On utilise parfois une proximité relative.

Les tests cachés sur cet exercice utilisaient la fonction `sont_proches` !

Comme dans tous les exercices où interviennent des tests d'égalité entre flottants.
