

# --------- PYODIDE:code --------- #

def f(...):
    ...

# --------- PYODIDE:corr --------- #

def f(x):
    return 5 * x - 3

# --------- PYODIDE:tests --------- #

assert f(0) == -3
assert f(1) == 2

# --------- PYODIDE:secrets --------- #


# autres tests

for x in range(-10, 20):
    attendu = 5 * x - 3
    assert f(x) == attendu, f"Erreur avec {x=}"