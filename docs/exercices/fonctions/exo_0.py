

# --------- PYODIDE:code --------- #

def f(x):
    y = ...  # ligne à modifier
    return y

# --------- PYODIDE:corr --------- #

def f(x):
    y = 4 * x + 7
    return y

# --------- PYODIDE:tests --------- #

assert f(0) == 7
assert f(1) == 11

# --------- PYODIDE:secrets --------- #


# autres tests

for x in range(-10, 20):
    attendu = 4 * x + 7
    assert f(x) == attendu, f"Erreur avec {x=}"