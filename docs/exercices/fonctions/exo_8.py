

# --------- PYODIDE:code --------- #

def Celsius_depuis_Fahrenheit(t_fahrenheit):
    t_celcius = ...  # ligne à modifier
    return t_celcius

# --------- PYODIDE:corr --------- #

def Celsius_depuis_Fahrenheit(t_fahrenheit):
    t_celcius = (t_fahrenheit - 32) * 5 / 9
    return t_celcius

# --------- PYODIDE:tests --------- #

def sont_proches(x, y):
    return abs(x - y) < 10**-9


assert sont_proches(Celsius_depuis_Fahrenheit(-40), -40)
assert sont_proches(Celsius_depuis_Fahrenheit(+32), 0)
assert sont_proches(Celsius_depuis_Fahrenheit(+50), 10)
assert sont_proches(Celsius_depuis_Fahrenheit(212), 100)

# --------- PYODIDE:secrets --------- #


# autres tests

for t_fahrenheit in range(-10, 20):
    attendu = (t_fahrenheit - 32) / 1.8
    assert sont_proches(
        Celsius_depuis_Fahrenheit(t_fahrenheit), attendu
    ), f"Erreur avec {t_fahrenheit=}"