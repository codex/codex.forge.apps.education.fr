Version recommandée !

# Variantes

On peut aussi écrire de plusieurs autres façons


```python
def g(x):
    return  (x + 1) ** 2
```

> Avec l'opérateur puissance, c'est un peu plus lent


```python
def g(x):
    return  (x + 1) * (x + 1)
```

> Avec le facteur calculé deux fois, puis multiplié, également plus lent

