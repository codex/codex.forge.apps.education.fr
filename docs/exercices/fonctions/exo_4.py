

# --------- PYODIDE:code --------- #

# fonction à corriger
def produit(k):
    return k + k

# --------- PYODIDE:corr --------- #

def produit(k, n):
    return k * n

# --------- PYODIDE:tests --------- #

assert produit(2, 3) == 6
assert produit(-5, 2) == -10

# --------- PYODIDE:secrets --------- #


# autres tests

for k in range(-10, 20):
    for n in range(-10, 20):
        attendu = k * n
        assert produit(k, n) == attendu, f"Erreur avec {k=} et {n=}"