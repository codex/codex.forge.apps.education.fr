

# --------- PYODIDE:code --------- #

# À créer ici

# --------- PYODIDE:corr --------- #

PI = 3.1415926


def circonference(r):
    y = 2 * PI * r
    return y

# --------- PYODIDE:tests --------- #

assert circonference(0) == 0
assert circonference(1) == 6.2831852

# --------- PYODIDE:secrets --------- #


# autres tests


def sont_proches(x, y):
    return abs(x - y) < 10**-9


for r in range(20):
    attendu = 2 * 3.1415926 * r
    assert sont_proches(circonference(r), attendu), f"Erreur avec {r=}"