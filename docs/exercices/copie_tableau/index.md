---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Copie d'un tableau
tags:
    - liste/tableau
difficulty: 130
maj: 01/02/2024
---

Lorsque l'on souhaite réaliser une copie d'un tableau, le premier réflexe est de procéder par affectation :

```pycon
>>> valeurs = [10, 11, 12]
>>> copie = tableau
>>> copie
[10, 11, 12]
```

Par cette méthode, on ne crée pas réellement une copie du tableau. En réalité on obtient deux variables qui désignent le même tableau : on parle alors de **copie de référence** ou **par alias**.

![affectation](affectation.svg){ width=25% .autolight .center}

Ainsi la modification d'un élément de la variable `copie` entraînera la modification de ce même élément dans `valeurs`.

```pycon
>>> copie[1] = 0
>>> copie
[10, 0, 12]
>>> valeurs
[10, 0, 12]
```

On demande d'écrire une fonction `copie_superficielle` qui crée une *vraie* copie d'un tableau passé en attribut. Pour ce faire, on créera un nouveau tableau et on y affectera un à un les éléments du tableau initial.

![copie](copie.svg){ width=25% .autolight .center}

???+ example "Exemples"

    ```pycon title=""
    >>> tableau_1 = [1, 2, 3]
    >>> copie = copie_superficielle(tableau_1)
    >>> copie == tableau_1  # les deux tableaux sont identiques
    True
    >>> tableau_1[0] = 0  # on modifie l'un...
    >>> copie             # ... sans modifier l'autre
    [1, 2, 3]
    ```


{{ IDE('exo') }}
