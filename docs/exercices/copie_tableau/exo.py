

# --------- PYODIDE:code --------- #

def copie_superficielle(tableau):
    ...
    return nouveau

# --------- PYODIDE:corr --------- #

def copie_superficielle(tableau: list):
    nouveau = [element for element in tableau]
    return nouveau

# --------- PYODIDE:tests --------- #

tableau_1 = [1, 2, 3]
copie = copie_superficielle(tableau_1)
assert copie == tableau_1
tableau_1[0] = 0
assert copie == [1, 2, 3]

# --------- PYODIDE:secrets --------- #

# tests
tableau_1 = [1, 2, 3]
copie = copie_superficielle(tableau_1)
tableau_1[0] = 0
assert copie == [1, 2, 3]
# tests secrets
copie[1] = 15
assert tableau_1 == [0, 2, 3]

tableau_1 = []
copie = copie_superficielle(tableau_1)
assert copie == []
tableau_1.append(1)
assert copie == [], "Erreur après un ajout dans le tableau initial"

tableau_1 = [1]
copie = copie_superficielle(tableau_1)
assert copie == [1]
copie[0] = 2
assert tableau_1 == [1], "La modification de la copier modifie le tableau initial"

tableau_1 = ["papa", "maman", "la bonne", "moi"]
copie = copie_superficielle(tableau_1)
assert copie == ["papa", "maman", "la bonne", "moi"]
