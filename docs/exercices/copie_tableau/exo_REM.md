Une autre solution possible en utilisant une boucle :

```python
def copie_superficielle(tableau):
    taille = len(tableau)
    nouveau = [None] * taille  
    for i in range(taille):
        nouveau[i] = tableau[i]
    return nouveau
```

Attention, la copie réalisée ici est bien **superficielle** : on a bien créé un nouveau tableau, dans une nouvelle zone mémoire, mais on a seulement copié les références des éléments contenus dans le tableau initial.

Cette solution est donc insuffisante lorsqu'il s'agira de copier un tableau de tableaux :

![copie_tab](copie_tab.svg){ width=25% .autolight .center}

```pycon
>>> valeurs = [[10, 11, 12], [14, 15, 16]]
>>> copie = copie_superficielle(valeurs)
>>> valeurs[1][1] = 0
>>> copie
[[10, 11, 12], [14, 0, 16]]
```

La fonction a bien créé deux tableaux distincts, mais les tableaux qu'ils contiennent sont communs aux deux. Ainsi quand on modifie un élément de `valeurs`, il est également changé dans `copie`.

