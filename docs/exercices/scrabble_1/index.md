---
author:
    - Sébastien Hoarau
    - Fabrice Nativel
hide:
    - navigation
    - toc
title: Score d'un mot au Scrabble (I)
tags:
    - dictionnaire
    - string
difficulty: 180
---



Au Scrabble, chaque lettre possède une valeur et le score d'un mot est la somme des valeurs des lettres qui le compose. Par exemple, la valeur du mot :

<!--
$$\boxed{\mathrm{\tt G}_2} \; \boxed{\mathrm{\tt I}_1} \; \boxed{\mathrm{\tt R}_1} \; \boxed{\mathrm{\tt A}_1} \; \boxed{\mathrm{\tt F}_4} \; \boxed{\mathrm{\tt E}_1} $$
-->

![girage](girafe.svg)

est : $2 + 1 + 1 + 1 + 4 + 1 = 10$. Écrire une fonction `score_scrabble` qui prend en paramètre une chaine de caractères `mot` et renvoie sa valeur au Scrabble. Le `mot` ne doit comporter que les lettres de l'alphabet en majuscules et il peut être vide. La valeur des lettres de l'alphabet est donné sous la forme d'un dictionnaire. 

{{ IDE('exo') }}
