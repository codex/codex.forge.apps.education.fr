---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Puissances d'un nombre
tags:
    - maths
difficulty: 110
maj: 22/06/2024
---

On s'intéresse dans cette page à deux problèmes traitant du calcul des puissances d'un nombre strictement positif.

On rappelle que si $a$ est un nombre réel strictement positif et $n$ un entier naturel, alors on a :

$$
a^n =\begin{cases}
    1&\text{ si }n=0\\
    a\times \dots \times a \text{ avec }n\text{ facteurs}&\text{ sinon}
\end{cases}
$$

Par exemple : 

$$3^4=3\times3\times3\times3=81$$


{{ remarque('assertion') }}


??? question "Puissances croissantes"

    Soient $a$ et $b$ deux entiers strictement positifs. $a$ est différent de $1$.
    
    Si $a$ est strictement **supérieur** à $1$, les puissances de $a$ successives sont **croissantes**.
    
    Considérons par exemple les puissances de $2$ :
    ![Puissances de $2$](puissances_de_2.svg){.autolight align=right width=30%}
    
    * $2^0=1$
    
    * $2^1=2$
    
    * $2^2=4$
    
    * $2^3=8$
    
    * ...
    
    * $2^{8}=256$

    On peut démontrer que, quelle que soit la valeur de $b$ positif, il existe une certaine puissance de $a$ supérieure ou égale à $b$.
    
    Par exemple, en prenant $a=2$ et $b = 999$, il faut attendre $a^{10}=1~024$ pour dépasser $b$.

    On demande d'écrire la fonction `puissance_superieure` qui :
    
    * prend en paramètres les entiers `a` et `b` ;

    * renvoie la première valeur de l'entier `n` telle que `a` puissance `n` est supérieure ou égale à `b`.

    ??? example "Exemples"

        ```pycon title=""
        >>> puissance_superieure(2, 1)  # on a 2^0 = 1
        0
        >>> puissance_superieure(2, 8)  # on a 2^3 = 8
        3
        >>> puissance_superieure(5, 130)  # on a 5^3 = 125 et 5^4 = 625
        4
        ```
    === "Version vide"
        {{ IDE('exo_sup_vide') }}
    === "Version à compléter"
        {{ IDE('exo_sup_trous') }}


??? question "Puissances décroissantes "

    Soient $a$ et $b$ deux entiers strictement positifs. $a$ est différent de $1$.
    
    Si $a$ est strictement **inférieur** à $1$, les puissances de $a$ successives sont **décroissantes**.
    
    Considérons par exemple les puissances de $\dfrac12$ :
    ![Puissances de $2$](puissances_de_1_sur_2.svg){.autolight align=right width=30%}
    
    * $\left(\dfrac12\right)^0=1$
    
    * $\left(\dfrac12\right)^1=\dfrac{1}{2}=0,5$
    
    * $\left(\dfrac12\right)^2=\dfrac{1}{4}=0,25$
    
    * $\left(\dfrac12\right)^3=\dfrac{1}{8}=0,125$
    
    * ...
    
    * $\left(\dfrac12\right)^{8}=\dfrac{1}{256}=0,00390625$

    On peut démontrer que, quelle que soit la valeur de $b$ positif, il existe une certaine puissance de $a$ inférieure ou égale à $b$.
    
    Par exemple, en prenant $a=\dfrac{1}{10}$ et $b = 0,0002$, il faut attendre $a^{4}=0,0001$ pour être inférieur ou égal à $b$.

    On demande d'écrire la fonction `puissance_inferieure` qui :
    
    * prend en paramètres les entiers `a` et `b` ;

    * renvoie la première valeur de l'entier `n` telle que `a` puissance `n` est inférieure ou égale à `b`.

    ??? example "Exemples"

        ```pycon title=""
        >>> puissance_inferieure(1/2, 1)  # (1/2)^0 = 1
        0
        >>> puissance_inferieure(1/2, 1/8)  # (1/2)^3 = 1/8
        3
        >>> puissance_inferieure(1/10, 0.0002)  # (1/10)^3 = 0,001 et (1/10)^4 = 0,0001
        4
        ```
            
    {{ IDE('exo_inf_vide') }}

