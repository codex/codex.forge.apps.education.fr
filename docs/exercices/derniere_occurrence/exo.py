

# --------- PYODIDE:code --------- #

def derniere_occurrence(tableau, cible):
    ...

# --------- PYODIDE:corr --------- #

def derniere_occurrence(tableau, cible):
    indice_dernier = len(tableau)
    for i in range(len(tableau)):
        if tableau[i] == cible:
            indice_dernier = i
    return indice_dernier

# --------- PYODIDE:tests --------- #

assert derniere_occurrence([5, 3], 1) == 2
assert derniere_occurrence([2, 4], 2) == 0
assert derniere_occurrence([2, 3, 5, 2, 4], 2) == 3

# --------- PYODIDE:secrets --------- #


# Autres tests

assert derniere_occurrence([1, 2, 1, 2, 1], 1) == 4
assert derniere_occurrence([1, 2, 3, 4, 5], 0) == 5
assert derniere_occurrence([1, 2, 3], 1) == 0