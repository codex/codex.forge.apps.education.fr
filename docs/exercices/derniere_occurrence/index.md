---
author: Sébastien Hoarau
hide:
    - navigation
    - toc
title: Dernière occurrence
tags:
    - liste/tableau
    - ep1
difficulty: 150
---

Programmer la fonction `derniere_occurrence`, prenant en paramètre un tableau non vide d'entiers et un entier `cible`, et qui renvoie l'indice de la **dernière** occurrence de `cible`.

Si l'élément n'est pas présent, la fonction renvoie la longueur du tableau.

??? warning "Méthode `#!py index` interdite"

    La méthode `#!py index` qui renvoie l'indice d'un élément dans une liste est **interdite**.

???+ example "Exemples"

    ```python
    >>> derniere_occurrence([5, 3], 1)
    2
    ```
    `#!py 1` est absent, on renvoie la longueur du tableau : `#!py 2`.

    ```pycon
    >>> derniere_occurrence([2, 4], 2)
    0
    ```
    `#!py 2` est présent à l'indice `#!py 0`, on renvoie `#!py 0`.

    ```pycon
    >>> derniere_occurrence([2, 3, 5, 2, 4], 2)
    3
    ```
    `#!py 2` est présent aux indices `#!py 0` et `#!py 3`, on renvoie le dernier : `#!py 3`.


{{ remarque('assertion') }}


{{ IDE('exo', SANS=".index") }}
