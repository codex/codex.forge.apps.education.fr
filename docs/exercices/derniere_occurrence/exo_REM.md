Le fait de renvoyer la longueur du tableau pour une cible non trouvée est discutable. Une bonne pratique est plutôt de renvoyer `#!py None`.

# En commençant par la fin

On s'arrête dès qu'on trouve la cible :

```python
def derniere_occurrence(tableau, cible):
    for i in range(len(tableau)-1, -1, -1):
        if tableau[i] == cible:
            return i
    return len(tableau)
```

# Autre façon de partir par la fin

On incrémente la variable de boucle, mais on décrémente la position considérée.

```python
def derniere_occurrence(tableau, cible):
    n = len(tableau)
    for indice in range(1, n + 1):
        if tableau[n - indice] == cible:
            return n - indice
    return n
```


# En renvoyant `None`

Ce code ne correspond pas à ce qui est demandé, mais il est intéressant.

```python
def derniere_occurrence(tableau, cible):
    for i in range(len(tableau)-1, -1, -1):
        if tableau[i] == cible:
            return i
```

- Si la cible n'est pas trouvée, on renvoie `#!py None`. C'est une bonne idée !
- Commencer par la fin est plus logique pour obtenir une réponse rapide.
