---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Conversion binaire
tags:
    - à trous
    - maths
    - binaire
    - ep2
difficulty: 180
---

{{ version_ep() }}

Écrire une fonction `conversion_binaire` qui prend en paramètre un entier positif `n` et renvoie une liste `bits` d'entiers égaux à `#!py 0` ou `#!py 1`, la représentation en binaire de `n`, avec les bits de poids forts en premier.

!!! tip "Représentation binaire de $13$"
    - $13$ divisé par $2$ donne $6$ et reste $1$.
    - $6$ divisé par $2$ donne $3$ et reste $0$.
    - $3$ divisé par $2$ donne $1$ et reste $1$.
    - $1$ divisé par $2$ donne $0$ et reste $1$.
    - On s'arrête.
    - La liste renversée des restes est `[1, 1, 0, 1]`

!!! tip "Représentation binaire de $4$"
    - $4$ divisé par $2$ donne $2$ et reste $0$.
    - $2$ divisé par $2$ donne $1$ et reste $0$.
    - $1$ divisé par $2$ donne $0$ et reste $1$.
    - On s'arrête.
    - La liste renversée des restes est `[1, 0, 0]`

    Le bit de poids fort est en premier.


???+ example "Exemple"

    ```pycon title=""
    >>> conversion_binaire(13)
    [1, 1, 0, 1]
    >>> conversion_binaire(4)
    [1, 0, 0]
    >>> conversion_binaire(0)
    [0]
    ```

!!! tip "Consignes et aide"

    - On n'utilisera pas la fonction _built-in_ `bin`
    - On n'utilisera pas la fonction _built-in_ `reverse`
    - `n % 2` est le reste dans la division de `n` par `#!py 2`
    - `n // 2` est le quotient dans la division de `n` par `#!py 2`

=== "Version vide"
    {{ IDE('exo') }}

=== "Version à compléter"
    {{ IDE('exo_trous') }}
