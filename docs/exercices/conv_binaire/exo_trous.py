

# --------- PYODIDE:code --------- #

def conversion_binaire(n):
    restes = [n % 2]
    n = n // 2
    while ...:
        restes.append(...)
        n = ...
    # renverser la liste
    nb_bits = len(...)
    return [restes[...] for i in range(nb_bits)]

# --------- PYODIDE:corr --------- #

def conversion_binaire(n):
    restes = [n % 2]
    n = n // 2
    while n != 0:
        restes.append(n % 2)
        n = n // 2
    # renverser la liste
    nb_bits = len(restes)
    return [restes[nb_bits - 1 - i] for i in range(nb_bits)]

# --------- PYODIDE:tests --------- #

assert conversion_binaire(13) == [1, 1, 0, 1]
assert conversion_binaire(4) == [1, 0, 0]
assert conversion_binaire(0) == [0]

# --------- PYODIDE:secrets --------- #


# autres tests

for n in range(100):
    attendu = list(map(int, bin(n)[2:]))
    assert conversion_binaire(n) == attendu, f"Erreur avec {n}"