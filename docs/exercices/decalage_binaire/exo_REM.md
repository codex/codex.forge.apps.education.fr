L'instruction `#!py valeurs[0], valeurs[1], valeurs[2] = valeurs[1], valeurs[2], valeurs[3]` revient à faire en une seule ligne les instructions suivantes :

```python
valeurs[0] = valeurs[1]
valeurs[1] = valeurs[2]
valeurs[2] = valeurs[3]
```

Il aurait également été possible d'utiliser une structure itérative :

```python  title=""
def decalage_droite(nombre):
    for i in range(3, 0, -1):
        nombre[i] = nombre[i - 1]
    nombre[0] = 0


def decalage_gauche(nombre):
    for i in range(3):
        nombre[i] = nombre[i + 1]
    nombre[3] = 0
```