---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Décalage binaire
tags:
    - binaire
    - à trous
    - tuple
difficulty: 120
---



Un décalage binaire est une opération qui peut être directement réalisée par le processeur et qui consiste à décaler d'un ou de plusieurs rangs les bits d'un nombre vers la gauche ou vers la droite.

??? note "Multiplication et division"
    
    Tout comme en base décimale un décalage à gauche représente une multiplication par $10$, en base binaire, un décalage à gauche correspond à une multiplication par $2$.
    
    *A contrario*, le décalage à droite représente une division par $2$.

    En python, l'opérateur décalage à droite s'écrit `>>` et l'opérateur décalage à gauche `<<`.

    ```pycon
    >>> 10 >> 1  # décalage à droite de 1 bit (division par 2)
    5
    >>> 5 << 3  # décalage à gauche de 3 bits (multiplication par 2*2*2=8)
    40
    ```
    
On se propose de représenter des quartets (groupes de $4$ bits) par des tableaux de quatre entiers. Le nombre ci-dessous représente donc le nombre binaire $1110_2$.

![image](quartet.svg){ .center width=20%}

Un décalage à droite va donc décaler tous les bits d'un rang vers la droite, puis mettre la valeur $0$ pour le bit de poids fort. La valeur du bit de poids faible est perdue.

![image](décalage_droite.svg){.center width=25%}

Un décalage à gauche va décaler tous les bits d'un rang vers la gauche, puis mettre la valeur $0$ pour le bit de poids faible. La valeur du bit de poids fort est perdue.

![image](décalage_gauche.svg){.center width=25%}

On demande d'écrire les fonctions `decalage_droite` et  `decalage_gauche` qui prennent chacune en paramètre un tableau `nombre` de $4$ bits (entiers égaux à $0$ ou $1$) et effectuent le décalage correspondant d'un bit vers la droite ou vers la gauche.

Ces fonctions modifient le tableau `nombre` **en place** et ne renvoient rien.

```pycon
>>> nombre_binaire = [1, 1, 1, 0]
>>> decalage_droite(nombre_binaire)
>>> nombre_binaire
[0, 1, 1, 1]
>>> decalage_gauche(nombre_binaire)
>>> nombre_binaire 
[1, 1, 1, 0]
```

???+ abstract "Decompactage"

    Le procédé de décompactage, *unpacking* en anglais, consiste à affecter les valeurs d'un p-uplet dans différentes variables et ce, en une seule instruction.

    Ainsi, les instructions suivantes :

    ```python
    a = 5
    b = 4
    c, d = a, b
    ```

    permettent de placer la valeur `#!py 5` dans la variable `c` et la valeur `#!py 4` dans la variable `d`.

    Dans ce cas, il faut naturellement qu'il y ait autant de variables dans la partie gauche de l'affectation que d'éléments dans le p-uplet.

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}
