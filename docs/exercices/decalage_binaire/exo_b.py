

# --------- PYODIDE:code --------- #

def decalage_droite(nombre):
    nombre[1], nombre[2], nombre[3] = ...
    nombre[...] = ...

def decalage_gauche(nombre):
    ...

# --------- PYODIDE:corr --------- #

def decalage_droite(nombre):
    nombre[1], nombre[2], nombre[3] = nombre[0], nombre[1], nombre[2]
    nombre[0] = 0

def decalage_gauche(nombre):
    nombre[0], nombre[1], nombre[2] = nombre[1], nombre[2], nombre[3]
    nombre[3] = 0
    

# --------- PYODIDE:tests --------- #

nombre = [1, 1, 1, 0]
decalage_droite(nombre)
assert nombre == [0, 1, 1, 1]
nombre = [1, 1, 1, 0]
decalage_gauche(nombre)
assert nombre == [1, 1, 0, 0]

# --------- PYODIDE:secrets --------- #


# autres tests
nombre = [1, 1, 1, 1]
decalage_droite(nombre)
assert nombre == [0, 1, 1, 1], f"Erreur avec {nombre = }"
decalage_gauche(nombre)
assert nombre == [1, 1, 1, 0], f"Erreur avec {nombre = }"
nombre = [0, 0, 0, 0]
decalage_droite(nombre)
assert nombre == [0, 0, 0, 0], f"Erreur avec {nombre = }"
decalage_gauche(nombre)
assert nombre == [0, 0, 0, 0], f"Erreur avec {nombre = }"
nombre = [0, 1, 0, 0]
decalage_droite(nombre)
assert nombre == [0, 0, 1, 0], f"Erreur avec {nombre = }"
decalage_gauche(nombre)
assert nombre == [0, 1, 0, 0], f"Erreur avec {nombre = }"
