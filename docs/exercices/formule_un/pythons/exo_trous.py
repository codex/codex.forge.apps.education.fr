# --------- PYODIDE:env --------- #

PILOTES = [
    "Leclerc",
    "Perez",
    "Hamilton",
    "Norris",
    "Piastri",
    "Russell",
    "Sainz",
    "Alonso",
    "Ocon",
    "Albon",
    "Verstappen",
    "Gasly",
    "Ricciardo",
    "Bottas",
    "Stroll",
    "Hulkenberg",
    "Magnussen",
    "Sargeant",
    "Zhou",
    "Tsunoda",
]


# --------- PYODIDE:code --------- #
def classement(depart, changements):
    positions = [None] * ...
    for i in range(...):
        di = ...
        positions[...] = ...
    return ...


# --------- PYODIDE:corr --------- #
def classement(depart, changements):
    positions = [None] * 20
    for i in range(20):
        di = changements[i]
        positions[i - di] = depart[i]
    return positions


# --------- PYODIDE:tests --------- #
# PILOTES contient les noms des 20 pilotes du championnat 2024

depart = [p for p in PILOTES]
changements = [0] * 20  # aucun changement
assert classement(depart, changements) == depart

depart = [p for p in PILOTES]
changements = [-19] + [+1] * 19  # le premier passe en dernier
positions = [PILOTES[i] for i in range(1, 20)] + [PILOTES[0]]
assert classement(depart, changements) == positions

depart = [p for p in PILOTES]
changements = [-1, -1, +2] + [0] * 17  # le troisième double les deux premiers
positions = [PILOTES[2]] + [PILOTES[i] for i in range(20) if i != 2]
assert classement(depart, changements) == positions
# --------- PYODIDE:secrets --------- #
from random import shuffle


def _classement_(depart, changements):
    positions = [None] * 20
    for i in range(20):
        di = changements[i]
        positions[i - di] = depart[i]
    return positions


depart = PILOTES.copy()
shuffle(depart)
changements = [-1, +1] * 10
attendu = _classement_(depart, changements)
assert (
    classement(depart, changements) == attendu
), f"Erreur avec {depart = } et {changements = }"

for _ in range(10):
    depart = PILOTES.copy()
    attendu = depart.copy()
    shuffle(attendu)
    changements = [i - attendu.index(depart[i]) for i in range(20)]
    assert classement(depart, changements) == attendu, f"Erreur avec {changements = }"
    assert depart == PILOTES.copy(), f"La liste 'depart' ne doit pas être modifiée"
