---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Départ du grand prix
tags:
    - à trous
    - liste/tableau
difficulty: 190
maj: 22/08/2024
---

Lors du championnat 2024 de Formule 1, 20 pilotes prennent les départ des grands prix. Après les phases de qualifications ceux-ci sont placés selon un certain ordre sur la grille de départ.

Considérons par exemple la liste Python `#!py depart` représentant l'ordre des pilotes à l'issue des qualifications, **avant** le départ.

??? info "Classement avant le **départ**"

    ```python
    depart = [
        "Leclerc",
        "Perez",
        "Hamilton",
        "Norris",
        "Piastri",
        "Russell",
        "Sainz",
        "Alonso",
        "Ocon",
        "Albon",
        "Verstappen",
        "Gasly",
        "Ricciardo",
        "Bottas",
        "Stroll",
        "Hulkenberg",
        "Magnussen",
        "Sargeant",
        "Zhou",
        "Tsunoda"
    ]
    ```

`#!py "Leclerc"` est le premier élément de la liste. Le pilote correspondant, Charles Leclerc, a terminé premier aux qualifications et s'élance donc en pole position.

Lors de la course, ce classement initial peut évoluer : certains pilotes gagnent une ou plusieurs places, certains en perdent et d'autres restent à la même place.

Les différences observées entre les positions des coureurs avant le départ et à un instant donné de la course sont fournies sous forme d'une liste d'entiers. Une valeur positive à l'indice `#!py i` indique que le coureur dont le nom est à l'indice `#!py i` dans la grille de départ a progressé dans le classement. Une valeur négative indique qu'il a régressé et une valeur nulle qu'il est resté à la même position.

Considérons par exemple la liste `#!py changements = [-19] + [+1] * 19`. Cette liste contient la valeur `#!py -19` suivie de la valeur `#!py +1` répétée 19 fois. Dans cet exemple, le pilote en pole position a *perdu* 19 places, les autres pilotes ont tous *gagné* une place. Le nouveau classement est donné ci-dessous.

??? info "Classement pendant la **course**"

    ```python
    course = [
        "Perez",        # ↑ +1 place
        "Hamilton",     # ↑ +1 place
        "Norris",       # ↑ +1 place
        "Piastri",      # ↑ +1 place
        "Russell",      # ↑ +1 place
        "Sainz",        # ↑ +1 place
        "Alonso",       # ↑ +1 place
        "Ocon",         # ↑ +1 place
        "Albon",        # ↑ +1 place
        "Verstappen",   # ↑ +1 place
        "Gasly",        # ↑ +1 place
        "Ricciardo",    # ↑ +1 place
        "Bottas",       # ↑ +1 place
        "Stroll",       # ↑ +1 place
        "Hulkenberg",   # ↑ +1 place
        "Magnussen",    # ↑ +1 place
        "Sargeant",     # ↑ +1 place
        "Zhou",         # ↑ +1 place
        "Tsunoda",      # ↑ +1 place
        "Leclerc"       # ↓ -19 places
    ]
    ```

Écrire la fonction `#!py classement` qui prend en paramètres la liste de chaînes de caractères `#!py depart` contenant le classement au **départ** du grand prix et la liste d'entiers `#!py changements` décrivant l'évolution des positions entre la grille de départ et un instant donné de la course. Cette fonction renvoie une nouvelle liste de chaînes de caractères représentant le classement à l'instant décrit par la liste `#!py changements`. La liste `#!py depart` ne doit pas être modifiée.

On garantit que les deux listes contiennent chacune 20 éléments et que la liste `#!py changements` est valide (les valeurs indiquées renvoient les différents pilotes à des positions distinctes valides).

???+ example "Exemples"

    La liste `#!py avant` est celle fournie plus haut. Elle n'est pas modifiée durant les exemples.

    ```pycon title=""
    >>> depart
    ['Leclerc', 'Perez', 'Hamilton', 'Norris', 'Piastri', 'Russell', 'Sainz', 'Alonso', 'Ocon', 'Albon', 'Verstappen', 'Gasly', 'Ricciardo', 'Bottas', 'Stroll', 'Hulkenberg', 'Magnussen', 'Sargeant', 'Zhou', 'Tsunoda']
    ```
    
    * Aucun changement par rapport à la grille de départ :

        ```pycon title=""
        >>> changements = [0] * 20
        >>> classement(depart, changements)
        ['Leclerc', 'Perez', 'Hamilton', 'Norris', 'Piastri', 'Russell', 'Sainz', 'Alonso', 'Ocon', 'Albon', 'Verstappen', 'Gasly', 'Ricciardo', 'Bottas', 'Stroll', 'Hulkenberg', 'Magnussen', 'Sargeant', 'Zhou', 'Tsunoda']
        ```
    
    * Le premier est passé en dernier :

        ```pycon title=""
        >>> changements = [-19] + [+1] * 19
        >>> classement(depart, changements)
        ['Perez', 'Hamilton', 'Norris', 'Piastri', 'Russell', 'Sainz', 'Alonso', 'Ocon', 'Albon', 'Verstappen', 'Gasly', 'Ricciardo', 'Bottas', 'Stroll', 'Hulkenberg', 'Magnussen', 'Sargeant', 'Zhou', 'Tsunoda', 'Leclerc']
        ```

    * Le troisième a doublé les deux premiers :
    
        ```pycon title=""
        >>> changements = [-1, -1, +2] + [0] * 17
        >>> classement(avant, changements)
        ['Hamilton', 'Leclerc', 'Perez', 'Norris', 'Piastri', 'Russell', 'Sainz', 'Alonso', 'Ocon', 'Albon', 'Verstappen', 'Gasly', 'Ricciardo', 'Bottas', 'Stroll', 'Hulkenberg', 'Magnussen', 'Sargeant', 'Zhou', 'Tsunoda']
        ```

=== "Version vide"
    {{ IDE('./pythons/exo_vide') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_trous') }}
