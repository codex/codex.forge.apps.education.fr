# --- exo --- #
def ajoute_colonne(tab):
    ...


# --- vide --- #
""" # skip
def ajoute_colonne(tab):
    hauteur = len(...)
    largeur = len(...)
    for i in range(...):
        somme = ...
        for j in range(...):
            somme = ...
        tab[...].append(...)


"""  # skip


# --- corr --- #
def ajoute_colonne(tab):
    hauteur = len(tab)
    largeur = len(tab[0])
    for i in range(hauteur):
        somme = 0
        for j in range(largeur):
            somme += tab[i][j]
        tab[i].append(somme)
# --- tests --- #
tab_1 = [[0, 1, 1, 1, 0, 1], [0, 0, 1, 0, 0, 1], [1, 0, 0, 1, 1, 0], [1, 0, 0, 1, 1, 1]]
ajoute_colonne(tab_1)
assert tab_1 == [
    [0, 1, 1, 1, 0, 1, 4],
    [0, 0, 1, 0, 0, 1, 2],
    [1, 0, 0, 1, 1, 0, 3],
    [1, 0, 0, 1, 1, 1, 4],
]

tab_2 = [[0, 0, 0], [0, 0, 0]]
ajoute_colonne(tab_2)
assert tab_2 == [[0, 0, 0, 0], [0, 0, 0, 0]]


# --- secrets --- #
from random import randrange, sample
tab = [[0], [1]]
ajoute_colonne(tab)
assert tab == [[0, 0], [1, 1]], "Erreur avec tab = [[0], [1]]"

for _ in range(10):
    hauteur = randrange(1, 10)
    largeur = randrange(1, 10)
    sommes_lignes = [randrange(0, largeur + 1) for _ in range(hauteur)]
    tab = []
    for i in range(hauteur):
        positions = sample(list(range(largeur)), sommes_lignes[i])
        tab.append([1 if j in positions else 0 for j in range(largeur)])
    tab_base = [ligne[:] for ligne in tab]
    attendu = [tab[i][:] + [sommes_lignes[i]] for i in range(hauteur)]
    ajoute_colonne(tab)
    assert tab == attendu, f"Erreur avec tab = {tab_base}"

# --- rem --- #
"""  # skip
Il serait aussi possible de se passer de l'indice `j` en parcourant directement les valeurs de chaque ligne :

```python
def ajoute_colonne(tab):
    hauteur = len(tab)
    for i in range(hauteur):
        somme = 0
        for x in tab[i]:
            somme += x
        tab[i].append(somme)
```

En poursuivant cette idée et en profitant du caractère mutable des listes **Python**, on peut aussi se passer complètement
d'indices :

```python
def ajoute_colonne(tab):
    for ligne in tab:
        somme = 0
        for x in ligne:
            somme += x
        ligne.append(somme)
```
"""  # skip
