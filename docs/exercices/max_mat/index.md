---
author: 
    - Guillaume Connan
    - Pierre Marquestaut
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Ajout d'une colonne
tags:
    - liste/tableau
    - boucle
    - à trous
difficulty: 201
maj: 01/02/2024
---

On dispose d'un tableau non-vide ne contenant que des `#!py 0` et des `#!py 1` et on
voudrait lui rajouter une colonne à droite ou figurent les totaux par ligne.

Par exemple, le tableau :

$$
\begin{array}{*{6}{|c|}}
\hline
0&1&1&1&0&1\\
\hline
0&0&1&0&0&1\\
\hline
1&0&0&1&1&0\\
\hline
1&0&0&1&1&1\\
\hline
\end{array}
$$


sera représenté en machine par la liste de listes suivante : 


```python
tab1 = [
    [0, 1, 1, 1, 0, 1],
    [0, 0, 1, 0, 0, 1],
    [1, 0, 0, 1, 1, 0],
    [1, 0, 0, 1, 1, 1],
]
```

et deviendra :

$$
\begin{array}{*{7}{|c|}}
\hline
0&1&1&1&0&1&4\\
\hline
0&0&1&0&0&1&2\\
\hline
1&0&0&1&1&0&3\\
\hline
1&0&0&1&1&1&4\\
\hline
\end{array}
$$


Écrire la fonction `ajoute_colonne` qui prend en paramètre une liste **non vide** `tab` et la modifie en
lui ajoutant la colonne décrite précédemment.

??? warning "Modification « en place »"

    La liste `tab` passée en argument est directement modifiée :
    
    - il **n'est pas demandé** de créer une nouvelle liste modifiée,
    - la fonction `ajoute_colonne` ne doit rien renvoyer.

??? warning "Fonction `#!py sum` interdite"

    Pour résoudre cet exercice, il est interdit d'utiliser la fonction `#!py sum`.

???+ example "Exemples"

    ```pycon title=""
    >>> tab_1 = [[0, 1, 1, 1, 0, 1], [0, 0, 1, 0, 0, 1], [1, 0, 0, 1, 1, 0], [1, 0, 0, 1, 1, 1]]
    >>> ajoute_colonne(tab_1)
    >>> tab_1
    [[0, 1, 1, 1, 0, 1, 4], [0, 0, 1, 0, 0, 1, 2], [1, 0, 0, 1, 1, 0, 3], [1, 0, 0, 1, 1, 1, 4]]
    >>> tab_2 = [[0, 0, 0], [0, 0, 0]]
    >>> ajoute_colonne(tab_2)
    >>> tab_2
    [[0, 0, 0, 0], [0, 0, 0, 0]]
    ```    

=== "Version vide"
    {{ IDE('./pythons/exo_b', SANS="sum" ) }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a', SANS="sum" ) }}
