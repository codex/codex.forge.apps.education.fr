

# --------- PYODIDE:code --------- #

def tri_insertion(tableau):
    n = len(tableau)
    for i in range(1, n):
        j = ...
        valeur_a_inserer = tableau[...]
        while j > ... and valeur_a_inserer < tableau[...]:
            tableau[j] = tableau[j - 1]
            j = ...
        tableau[j] = ...

# --------- PYODIDE:corr --------- #

def tri_insertion(tableau):
    n = len(tableau)
    for i in range(1, n):
        j = i
        valeur_a_inserer = tableau[i]
        while j > 0 and valeur_a_inserer < tableau[j - 1]:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = valeur_a_inserer

# --------- PYODIDE:tests --------- #

tableau_0 = [9, 5, 8, 7, 6]
tri_insertion(tableau_0)
assert tableau_0 == [5, 6, 7, 8, 9]

tableau_1 = [2, 5, -1, 7, 0, 28]
tri_insertion(tableau_1)
assert tableau_1 == [-1, 0, 2, 5, 7, 28]

un_seul = [9]
tri_insertion(un_seul)
assert un_seul == [9]

tableau_vide = []
tri_insertion(tableau_vide)
assert tableau_vide == []

# --------- PYODIDE:secrets --------- #

from random import sample
# Autres tests
for i in range(10):
    nombres = list(sample(range(10**9), 100 + i))
    attendu = sorted(nombres)
    tri_insertion(nombres)
    assert len(nombres) == len(
        attendu
    ), "Erreur, le tableau ne doit pas changer de taille"
    assert nombres == attendu, "Erreur lors du tri"