---
author: 
    - Charles Poulmaire
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Le bon enclos
tags:
    - dictionnaire
    - à trous
difficulty: 170
maj: 01/02/2024
---

On considère une table (une liste de dictionnaires Python) qui contient des enregistrements relatifs à des animaux hébergés dans un refuge.

Les attributs des enregistrements sont :

* `nom`,
* `espece`,
* `age`,
* `enclos`.
 
Les valeurs associées à `nom` et `espece` sont des chaines de caractères, celles associées à `age` et `enclos` des entiers.

 Voici un exemple d'une telle table :

```python
animaux = [
  {'nom': 'Medor', 'espece': 'chien', 'age': 5, 'enclos': 2},
  {'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
  {'nom': 'Tom', 'espece': 'chat', 'age': 7, 'enclos': 4},
  {'nom': 'Belle', 'espece': 'chien', 'age': 6, 'enclos': 3},
  {'nom': 'Mirza', 'espece': 'chat', 'age': 6, 'enclos': 5}
]
```

On garantit que chaque enregistrement (un dictionnaire Python) contient l'ensemble des informations (aucune clé ne manque dans un dictionnaire).

??? question "Question 1"
    Programmer une fonction `selection_enclos` qui :

    * prend en paramètres :
        * une liste `table_animaux` contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
        * un numéro d'enclos `num_enclos` ;
    * renvoie une liste contenant les enregistrements de `table_animaux` dont l'attribut `enclos` est égal à `num_enclos`. Dans cette liste, les enregistrements seront donnés dans le même ordre que dans la liste d'entrée.


    Exemples avec la liste `animaux` ci-dessus :

    ???+ example "Exemples"

        ```pycon
        >>> selection_enclos(animaux, 5)
        [{'nom': 'Titine', 'espece': 'chat', 'age': 2, 'enclos': 5},
        {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
        >>> selection_enclos(animaux, 2)
        [{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
        >>> selection_enclos(animaux, 7)
        []
        ```

    === "Version vide"
        {{ IDE('exo_1a') }}
    === "Version à compléter"
        {{ IDE('exo_1b') }}

??? question "Question 2"
    Programmer une fonction `selection_nom_dans_enclos` qui :

    * prend en paramètres :
        * une liste `table_animaux` contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
        * un numéro d'enclos `num_enclos` ;
    * renvoie une liste contenant les noms des animaux contenus dans `table_animaux` dont l'attribut `enclos` est égal à `num_enclos`. Dans cette liste, les noms seront donnés dans le même ordre que dans la liste d'entrée.

    Exemples avec la liste `animaux` ci-dessus :

    ???+ example "Exemples"

        ```pycon
        >>> selection_nom_dans_enclos(animaux, 5)
        ['Titine', 'Mirza']
        >>> selection_nom_dans_enclos(animaux, 2)
        ['Medor']
        >>> selection_dans_enclos(animaux, 7)
        []
        ```

    === "Version vide"
        {{ IDE('exo_2a') }}
    === "Version à compléter"
        {{ IDE('exo_2b') }}

??? question "Question 3"
    Programmer une fonction `selection_espece` qui :

    * prend en paramètres :
        * une liste `table_animaux` contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
        * un texte `espece` ;
    * renvoie une liste contenant de nouveaux enregistrements ne contenant que le nom et le numéro d'enclos des animaux contenus dans `table_animaux` qui sont de l'espèce `espece`. Dans cette liste, les enregistrements seront donnés dans le même ordre que dans la liste d'entrée.

    La table des animaux ne doit pas être modifiée par la fonction `selection_espece`.

    Exemples avec la liste `animaux` ci-dessus :

    ???+ example "Exemples"

        ```pycon
        >>> selection_espece(animaux, "chien")
        [{'nom': 'Medor', 'enclos': 2}, {'nom': 'Belle', 'enclos': 3}]
        >>> selection_espece(animaux, "chat")
        [{'nom': 'Titine', 'enclos': 5}, {'nom': 'Tom', 'enclos': 4}, {'nom': 'Mirza', 'enclos': 5}]
        >>> selection_espece(animaux, "dragon")
        []
        ```

    === "Version vide"
        {{ IDE('exo_3a') }}
    === "Version à compléter"
        {{ IDE('exo_3b') }}
