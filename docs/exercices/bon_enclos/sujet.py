# --- exo, 1 --- #
def selection_enclos(table_animaux, num_enclos):
    resultat = []
    for animal in table_animaux:
        if animal[...] == ...:
            resultat.append(...)
    return resultat

# --- vide, 1 --- #
def selection_enclos(table_animaux, num_enclos):
    ...

# --- corr, 1 --- #
def selection_enclos(table_animaux, num_enclos):
    resultat = []
    for animal in table_animaux:
        if animal["enclos"] == num_enclos:
            resultat.append(animal)
    return resultat

# --- tests, 1 --- #
animaux = [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2},
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Tom", "espece": "chat", "age": 7, "enclos": 4},
    {"nom": "Belle", "espece": "chien", "age": 6, "enclos": 3},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]
assert selection_enclos(animaux, 5) == [
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]
assert selection_enclos(animaux, 2) == [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2}
]
assert selection_enclos(animaux, 7) == []
# --- secrets, 1 --- #
enclos = {animal["enclos"] for animal in animaux}
for num_enclos in enclos:
    reponse = [ani for ani in animaux if ani["enclos"] == num_enclos]
    assert selection_enclos(animaux, num_enclos) == reponse

assert selection_enclos(animaux, -1) == []

animaux = [
    {"nom": "Moder", "espece": "lapin", "age": 5, "enclos": 0},
    {"nom": "Ollie", "espece": "furet", "age": 6, "enclos": 0},
]
assert selection_enclos(animaux, 0) == animaux
assert selection_enclos(animaux, 1) == []
# --- rem, 1 --- #
''' # skip
Le problème revient à filtrer les éléments d'une liste de dictionnaires selon un critère portant sur les valeurs des dictionnaires.

On crée une liste `resultat` qui contiendra les enregistrements souhaités.

On parcourt ensuite la liste des enregistrements avec la variable nommée `animal`. Pour chaque `animal` on teste son enclos. S'il correspond à celui cherché, on ajoute l'enregistrement à la fin de la liste.

Il faut réaliser un filtre sur une liste, on peut donc facilement écrire la liste renvoyée en compréhesion :

```python
def selection_enclos(table_animaux, num_enclos):
    return [animal for animal in table_animaux if animal["enclos"] == num_enclos]
```
''' # skip
# --- exo, 2 --- #
def selection_nom_dans_enclos(table_animaux, num_enclos):
    resultat = []
    for animal in table_animaux:
        if animal[...] == ...:
            resultat.append(animal[...])
    return resultat

# --- vide, 2 --- #
def selection_nom_dans_enclos(table_animaux, num_enclos):
    ...

# --- corr, 2 --- #
def selection_nom_dans_enclos(table_animaux, num_enclos):
    resultat = []
    for animal in table_animaux:
        if animal["enclos"] == num_enclos:
            resultat.append(animal["nom"])
    return resultat

# --- tests, 2 --- #
animaux = [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2},
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Tom", "espece": "chat", "age": 7, "enclos": 4},
    {"nom": "Belle", "espece": "chien", "age": 6, "enclos": 3},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]
assert selection_nom_dans_enclos(animaux, 5) == ["Titine", "Mirza"]
assert selection_nom_dans_enclos(animaux, 2) == ["Medor"]
assert selection_nom_dans_enclos(animaux, 7) == []
# --- secrets, 2 --- #
enclos = {animal["enclos"] for animal in animaux}
for num_enclos in enclos:
    reponse = [ani["nom"] for ani in animaux if ani["enclos"] == num_enclos]
    assert selection_nom_dans_enclos(animaux, num_enclos) == reponse

assert selection_nom_dans_enclos(animaux, -1) == []

animaux = [
    {"nom": "Moder", "espece": "lapin", "age": 5, "enclos": 0},
    {"nom": "Ollie", "espece": "furet", "age": 6, "enclos": 0},
]
assert selection_nom_dans_enclos(animaux, 0) == ["Moder", "Ollie"]
assert selection_nom_dans_enclos(animaux, 1) == []
# --- rem, 2 --- #
''' # skip
Le problème est quasiment identique à celui de la question 1. Par contre lors de l'ajout dans la liste des résultats, on ne met que le nom de l'animal et pas l'intégralité de l'enregistrement.

On peut également utiliser une liste en compréhesion :

```python
def selection_nom_dans_enclos(table_animaux, num_enclos):
    return [animal["nom"] for animal in table_animaux if animal["enclos"] == num_enclos]
```
''' # skip
# --- exo, 3 --- #
def selection_espece(table_animaux, espece):
    resultats = ...
    for animal in table_animaux:
        if ...:
            enregistrement = {"nom": ..., "enclos": ...}
            resultats.append(...)
    return ...

# --- vide, 3 --- #
def selection_espece(table_animaux, espece):
    ...

# --- corr, 3 --- #
def selection_espece(table_animaux, espece):
    resultats = []
    for animal in table_animaux:
        if animal["espece"] == espece:
            enregistrement = {"nom": animal["nom"], "enclos": animal["enclos"]}
            resultats.append(enregistrement)
    return resultats

# --- tests, 3 --- #
animaux = [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2},
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Tom", "espece": "chat", "age": 7, "enclos": 4},
    {"nom": "Belle", "espece": "chien", "age": 6, "enclos": 3},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]

assert selection_espece(animaux, "chien") == [
    {'nom': 'Medor', 'enclos': 2},
    {'nom': 'Belle', 'enclos': 3}
]

assert selection_espece(animaux, "chat") == [
    {'nom': 'Titine', 'enclos': 5},
    {'nom': 'Tom', 'enclos': 4},
    {'nom': 'Mirza', 'enclos': 5}
]

assert selection_espece(animaux, "dragon") == []
# --- secrets, 3 --- #
especes = {animal["espece"] for animal in animaux}
for espece in especes:
    reponse = [{"nom": ani["nom"], "enclos": ani["enclos"]} for ani in animaux if ani["espece"] == espece]
    assert selection_espece(animaux, espece) == reponse

assert selection_espece(animaux, "WWWWWWWWCZCZEFZEFFZVZEVVQS") == []

animaux = [
    {"nom": "Moder", "espece": "lapin", "age": 5, "enclos": 0},
    {"nom": "Ollie", "espece": "furet", "age": 6, "enclos": 0}
]
assert selection_espece(animaux, "lapin") == [{"nom": "Moder", "enclos": 0}]
assert selection_espece(animaux, "furet") == [{"nom": "Ollie", "enclos": 0}]
assert selection_espece(animaux, "serpent") == []
# --- rem, 3 --- #
''' # skip
Cette fois, il faut regarder l'attribut `espece` de chacun des animaux et bien reconstruire un nouvel enregistrement avec uniquement les attributs `nom` et `enclos`.

On peut également utiliser une liste en compréhesion :

```python
def selection_espece(table_animaux, espece):
    return [{"nom": animal["nom"], "enclos": animal["enclos"]} for animal in table_animaux if animal["espece"] == espece]
```
''' # skip

