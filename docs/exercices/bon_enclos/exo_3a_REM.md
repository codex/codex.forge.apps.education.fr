Cette fois, il faut regarder l'attribut `espece` de chacun des animaux et bien reconstruire un nouvel enregistrement avec uniquement les attributs `nom` et `enclos`.

On peut également utiliser une liste en compréhesion :

```python
def selection_espece(table_animaux, espece):
    return [{"nom": animal["nom"], "enclos": animal["enclos"]} for animal in table_animaux if animal["espece"] == espece]
```

