Le problème est quasiment identique à celui de la question 1. Par contre lors de l'ajout dans la liste des résultats, on ne met que le nom de l'animal et pas l'intégralité de l'enregistrement.

On peut également utiliser une liste en compréhesion :

```python
def selection_nom_dans_enclos(table_animaux, num_enclos):
    return [animal["nom"] for animal in table_animaux if animal["enclos"] == num_enclos]
```
