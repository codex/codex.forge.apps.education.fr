

# --------- PYODIDE:code --------- #

def selection_espece(table_animaux, espece):
    resultats = ...
    for animal in table_animaux:
        if ...:
            enregistrement = {"nom": ..., "enclos": ...}
            resultats.append(...)
    return ...

# --------- PYODIDE:corr --------- #

def selection_espece(table_animaux, espece):
    resultats = []
    for animal in table_animaux:
        if animal["espece"] == espece:
            enregistrement = {"nom": animal["nom"], "enclos": animal["enclos"]}
            resultats.append(enregistrement)
    return resultats

# --------- PYODIDE:tests --------- #

animaux = [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2},
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Tom", "espece": "chat", "age": 7, "enclos": 4},
    {"nom": "Belle", "espece": "chien", "age": 6, "enclos": 3},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]

assert selection_espece(animaux, "chien") == [
    {'nom': 'Medor', 'enclos': 2},
    {'nom': 'Belle', 'enclos': 3}
]

assert selection_espece(animaux, "chat") == [
    {'nom': 'Titine', 'enclos': 5},
    {'nom': 'Tom', 'enclos': 4},
    {'nom': 'Mirza', 'enclos': 5}
]

assert selection_espece(animaux, "dragon") == []

# --------- PYODIDE:secrets --------- #


# tests secrets
especes = {animal["espece"] for animal in animaux}
for espece in especes:
    reponse = [{"nom": ani["nom"], "enclos": ani["enclos"]} for ani in animaux if ani["espece"] == espece]
    assert selection_espece(animaux, espece) == reponse

assert selection_espece(animaux, "WWWWWWWWCZCZEFZEFFZVZEVVQS") == []

animaux = [
    {"nom": "Moder", "espece": "lapin", "age": 5, "enclos": 0},
    {"nom": "Ollie", "espece": "furet", "age": 6, "enclos": 0}
]
assert selection_espece(animaux, "lapin") == [{"nom": "Moder", "enclos": 0}]
assert selection_espece(animaux, "furet") == [{"nom": "Ollie", "enclos": 0}]
assert selection_espece(animaux, "serpent") == []