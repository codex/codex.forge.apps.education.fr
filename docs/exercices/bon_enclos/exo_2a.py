

# --------- PYODIDE:code --------- #

def selection_nom_dans_enclos(table_animaux, num_enclos):
    ...

# --------- PYODIDE:corr --------- #

def selection_nom_dans_enclos(table_animaux, num_enclos):
    resultat = []
    for animal in table_animaux:
        if animal["enclos"] == num_enclos:
            resultat.append(animal["nom"])
    return resultat

# --------- PYODIDE:tests --------- #

animaux = [
    {"nom": "Medor", "espece": "chien", "age": 5, "enclos": 2},
    {"nom": "Titine", "espece": "chat", "age": 2, "enclos": 5},
    {"nom": "Tom", "espece": "chat", "age": 7, "enclos": 4},
    {"nom": "Belle", "espece": "chien", "age": 6, "enclos": 3},
    {"nom": "Mirza", "espece": "chat", "age": 6, "enclos": 5},
]
assert selection_nom_dans_enclos(animaux, 5) == ["Titine", "Mirza"]
assert selection_nom_dans_enclos(animaux, 2) == ["Medor"]
assert selection_nom_dans_enclos(animaux, 7) == []

# --------- PYODIDE:secrets --------- #


# tests secrets
enclos = {animal["enclos"] for animal in animaux}
for num_enclos in enclos:
    reponse = [ani["nom"] for ani in animaux if ani["enclos"] == num_enclos]
    assert selection_nom_dans_enclos(animaux, num_enclos) == reponse

assert selection_nom_dans_enclos(animaux, -1) == []

animaux = [
    {"nom": "Moder", "espece": "lapin", "age": 5, "enclos": 0},
    {"nom": "Ollie", "espece": "furet", "age": 6, "enclos": 0},
]
assert selection_nom_dans_enclos(animaux, 0) == ["Moder", "Ollie"]
assert selection_nom_dans_enclos(animaux, 1) == []