---
author: 
    - Romain Janvier
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Tableau avec des éléments tous différents (optimisé)
tags:
    - boucle
    - booléen
    - liste/tableau
    - dictionnaire
    - défi
difficulty: 299
---

??? "Version optimisée"

    Cet exercice propose une version optimisée de {{ lien_exo('celui-ci', 'tous_differents') }}.
    
    En effet, les tableaux considérés contiennent ici **un grand nombre d'éléments**.

Un tableau peut contenir plusieurs fois le même élément. C'est le cas du tableau `tableau_1` ci-dessous :
```python title=""
tableau_1 = [1, 2, 3, 6, 2, 4, 5]
```
La valeur `#!py 2` est deux fois dans ce tableau.

Au contraire, dans le tableau `tableau_2`, toutes les valeurs sont uniques :
```python title=""
tableau_2 = ['chien', 'chat', 'lion', 'poisson']
```

Écrire une fonction `tous_differents` qui prend un tableau `tableau` et renvoie un booléen indiquant si toutes les valeurs de `tableau` sont différentes ou non.

???+ warning "**Grands** tableaux"

    Les tableaux utilisés dans les tests peuvent contenir un grand nombre d'éléments.
    
    Une approche comparant chaque élément à tous les autres prendra bien trop de temps à s'exécuter...

???+ example "Exemples"

    ```pycon title=""
    >>> tableau_1 = [1, 2, 3, 6, 2, 4, 5]
    >>> tous_differents(tableau_1)
    False
    >>> tableau_2 = ['chien', 'chat', 'lion', 'poisson']
    >>> tous_differents(tableau_2)
    True
    ```
    
{{ IDE('exo') }}

