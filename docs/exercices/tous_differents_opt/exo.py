# --------- PYODIDE:code --------- #
def tous_differents(tableau):
    ...

# --------- PYODIDE:corr --------- #
def tous_differents(tableau):
    vus = dict()
    for element in tableau:
        if element in vus:
            return False
        else:
            vus[element] = True
    return True

# --------- PYODIDE:tests --------- #

tableau1 = [1, 2, 3, 6, 2, 4, 5]
assert tous_differents(tableau1) is False

tableau2 = ["chien", "chat", "lion", "poisson"]
assert tous_differents(tableau2) is True

# --------- PYODIDE:secrets --------- #


# autres tests
assert tous_differents([]) is True
assert tous_differents(list(range(10**6))) is True
assert tous_differents(list(range(10**6)) + [1]) is False