# --------- PYODIDE:code --------- #
termes = [...]
somme = ...

# --------- PYODIDE:corr --------- #
termes = [(1/2)**n for n in range(0, 16)]
somme = sum(termes)
# --------- PYODIDE:secrets --------- #
termes_attendus = [(1/2)**n for n in range(0, 16)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
