# --- exo, 1 --- #
termes = [5 + 3 * n for n in range(..., ...)]
somme = sum(...)

# --- corr, 1 --- #
termes = [5 + 3 * n for n in range(0, 431)]
somme = sum(termes)
# --- secrets, 1 --- #
termes_attendus = [5 + 3 * n for n in range(0, 431)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 2 --- #
termes = [... for n in range(..., ...)]
somme = sum(...)

# --- corr, 2 --- #
termes = [5 + 3 * n for n in range(28, 1000)]
somme = sum(termes)
# --- secrets, 2 --- #
termes_attendus = [5 + 3 * n for n in range(28, 1000)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 3 --- #
termes = [... for ... in range(..., ...)]
somme = sum(...)

# --- corr, 3 --- #
termes = [n for n in range(1, 1001)]
somme = sum(termes)
# --- secrets, 3 --- #
termes_attendus = [n for n in range(1, 1001)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 4 --- #
termes = [...]
somme = sum(...)

# --- corr, 4 --- #
termes = [n for n in range(-79, 80)]
somme = sum(termes)
# --- secrets, 4 --- #
termes_attendus = [n for n in range(-79, 80)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 5 --- #
termes = [...**... for n in range(..., ...)]
somme = ...

# --- corr, 5 --- #
termes = [n**2 for n in range(1, 21)]
somme = sum(termes)
# --- secrets, 5 --- #
termes_attendus = [n**2 for n in range(1, 21)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 6 --- #
termes = [...]
somme = ...

# --- corr, 6 --- #
termes = [(1/2)**n for n in range(0, 16)]
somme = sum(termes)
# --- secrets, 6 --- #
termes_attendus = [(1/2)**n for n in range(0, 16)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
# --- exo, 7 --- #
termes = ...
somme = ...

# --- corr, 7 --- #
termes = [2*n for n in range(1, 101)]
somme = sum(termes)
# --- secrets, 7 --- #
termes_attendus = [2*n for n in range(1, 101)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"

# --- exo, 8 --- #
termes = ...
somme = ...

# --- corr, 8 --- #
termes = [2**n for n in range(0, 31)]
somme = sum(termes)
# --- secrets, 8 --- #
termes_attendus = [2**n for n in range(0, 31)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"