# --------- PYODIDE:code --------- #
termes = [... for n in range(..., ...)]
somme = sum(...)

# --------- PYODIDE:corr --------- #
termes = [5 + 3 * n for n in range(28, 1000)]
somme = sum(termes)
# --------- PYODIDE:secrets --------- #
termes_attendus = [5 + 3 * n for n in range(28, 1000)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
