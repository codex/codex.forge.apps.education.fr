# --------- PYODIDE:code --------- #
termes = [5 + 3 * n for n in range(..., ...)]
somme = sum(...)

# --------- PYODIDE:corr --------- #
termes = [5 + 3 * n for n in range(0, 431)]
somme = sum(termes)
# --------- PYODIDE:secrets --------- #
termes_attendus = [5 + 3 * n for n in range(0, 431)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
