# --------- PYODIDE:code --------- #
termes = [...]
somme = sum(...)

# --------- PYODIDE:corr --------- #
termes = [n for n in range(-79, 80)]
somme = sum(termes)
# --------- PYODIDE:secrets --------- #
termes_attendus = [n for n in range(-79, 80)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
