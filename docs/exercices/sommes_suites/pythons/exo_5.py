# --------- PYODIDE:code --------- #
termes = [...**... for n in range(..., ...)]
somme = ...

# --------- PYODIDE:corr --------- #
termes = [n**2 for n in range(1, 21)]
somme = sum(termes)
# --------- PYODIDE:secrets --------- #
termes_attendus = [n**2 for n in range(1, 21)]
somme_attendue = sum(termes_attendus)
assert termes == termes_attendus, "Erreur sur les termes"
assert somme == somme_attendue, "Erreur sur la somme totale"
