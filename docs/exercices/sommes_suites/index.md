---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Somme des termes d'une suite
tags:
    - maths
    - liste/tableau
difficulty: 90
maj: 18/10/2024
---

Antoine est un élève pressé : il souhaite faire **rapidement** son exercice de Maths pour demain.

Tellement rapidement que, plutôt que de simplement *relire son cours afin de trouver la formule adaptée*, il souhaite utiliser Python afin de faire les calculs à sa place !

Son travail à faire est le suivant :

!!! note "Somme des termes d'une suite arithmétique"

    Soit $u$ la suite arithmétique de premier terme $u_0=5$ et de raison $3$.
    
    1. Justifier que pour tout entier naturel $n$ on a : $u_n=5+3n$.
    2. Calculer la valeur des termes $u_0$, $u_1$, ..., $u_{10}$.
    3. Calculer la somme $S = u_0 + u_1 + \dots + u_{10}$.

Féru de programmation, Antoine sait qu'il peut résoudre le problème en tapant le script ci-dessous :

```python title="En construisant la liste au fur et à mesure"
termes = []
somme = 0
for n in range(0, 11):
    u = 5 + 3*n
    termes.append(u)
    somme = somme + u
```
 
À l'issue de ce script, la variable `#!py termes` contient toutes les valeurs souhaitées et `#!py somme` contient la valeur attendue : `#!py 220` !

Antoine est satisfait mais... c'est encore trop long ! Il souhaite ne pas écrire plus de deux lignes ! En demandant à son agent conversationnel préféré (ce qui lui prend un peu de temps), il trouve la construction suivante :

```python title="Avec une liste en compréhension"
termes = [5 + 3*n for n in range(0, 11)]
somme = sum(termes)
```
 
 Il se gratte la tête, cherche à nouveau des explications sur le net et finit par comprendre :
 
 * on utilise une **liste en compréhension** dont la syntaxe générale est `#!py [calcul for variable in ensemble_à_parcourir]` ;
 * cette liste contient tous les termes de la suite souhaités et est affectée à la variable `#!py termes` ;
 * la fonction `#!py sum` de Python additionne tous les éléments d'une liste.

Il trouve donc son résultat. En deux lignes ![^1]

[^1]: après avoir passé beaucoup de temps à saisir un *prompt* correct dans son agent conversationnel et encore plus à comprendre ce qu'il lui a répondu...

-------------------------

Utiliser la même démarche (une liste en compréhension puis la fonction `#!py sum`) afin de calculer les termes décrits ci-dessous et leur somme.

Dans chaque cas, les tests vérifieront :

* que la liste `#!py termes` contient bien tous les termes demandés (dans le bon ordre) ;
* que la variable `#!py somme` contient bien la somme attendue.

{{ remarque('assertion') }}

??? question "1. La même, mais plus loin"

    Soit $u$ la suite pour tout $n\in\mathbb{N}$ par $u_n=5+3n$.
    
    Calculer la somme $S = u_0 + u_1 + u_2 + \dots + u_{430}$.
    
    {{ IDE("pythons/exo_1") }}

??? question "2. Encore la même, mais on change le début et la fin"

    Soit $u$ la suite pour tout $n\in\mathbb{N}$ par $u_n=5+3n$.
    
    Calculer la somme $S = u_{28} + u_{29} + u_{30} + \dots + u_{999}$.
    
    {{ IDE("pythons/exo_2") }}

??? question "3. Tous ensembles"

    Calculer la somme $S = 1 + 2 + 3 + \dots + 1~000$.
    
    Cette somme se note aussi : 
    
    $$S = \sum_{n=1}^{1~000} n$$
    
    {{ IDE("pythons/exo_3") }}

??? question "4. Normalement çà fait $0$..."

    Calculer la somme :
    
    $$\begin{aligned}S &= -79 - 78 - 77 - \dots + 0 + \dots + 77 + 78 + 79\\ &=\sum_{n=-79}^{79} n\end{aligned}$$
    
    {{ IDE("pythons/exo_4") }}

??? question "5. :white_square_button: $\rightarrow$ :white_square_button: :white_square_button: $\rightarrow$ :white_square_button: :white_square_button: :white_square_button: $\rightarrow$ ..."

    Calculer la somme :
    
    $$S = \sum_{n=1}^{20} n^2$$
    
    ??? tip "Aide"
    
        Il s'agit de calculer $S = 1^2 + 2^2+2^2+\dots+20^{2}$.
    
    {{ IDE("pythons/exo_5") }}
    
??? question "6. Une histoire de flèche et d'arbre..."

    Calculer la somme $S = 1 + \dfrac12 + \dfrac14 + \dots+\left(\dfrac12\right)^{15}$.
    
    Cette somme intervient dans un célèbre [paradoxe de Zénon](https://fr.wikipedia.org/wiki/Paradoxe_de_la_fl%C3%A8che){ target="_blank}.
    
    {{ IDE("pythons/exo_6") }}
    
??? question "7. Une énigme de [Jacques Ozanam](https://fr.wikipedia.org/wiki/Jacques_Ozanam){ target="_blank}"

    <italic>
    Il y a un panier et cent cailloux rangés en ligne droite et à des espaces égaux d'une toise[^2].
    
    On propose de les ramasser et les rapporter dans le panier un à un, en allant d'abord chercher le premier, ensuite le second, et ainsi de suite jusqu'au dernier.
    
    Combien de toises doit faire celui qui entreprend cet ouvrage ?
    </italic>
    
    ![Des cailloux et un panier](ozanam.png){width=50% .center .autolight}
    
    La liste `#!py termes` contiendra les différentes longueurs parcourues pour ramasser le premier caillou, le deuxième, ..., le centième.
    
    {{ IDE("pythons/exo_7") }}

[^2]: la toise est une unité de longueur mesurant environ $2$ mètres.
        

??? question "8. L'astuce qui court"

    Antoine est tellement content de sa découverte qu'il la raconte à deux amis.
    
    Le lendemain, chacun d'eux partage l'astuce avec deux autres personnes.
    
    Le surlendemain, cela recommence et encore et encore : chaque jour, les personnes ayant découvert l'astuce la veille la partagent avec deux nouvelles personnes.
    
    Par un hasard extraordinaire, chacun s'adresse à une personne qui n'a jamais entendu l'astuce.
    
    Si on considère qu'Antoine a raconté son histoire le premier jour et que le processus est effectué $30$ jours en tout, combien de personnes, Antoine compris, connaîtront l'astuce au total ?
    
    ??? tip "Aide"
    
        Il s'agit de calculer $S = 1 + 2^1+2^2+\dots+2^{30}$.
        
    {{ IDE("pythons/exo_8") }}
