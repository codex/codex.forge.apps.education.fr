

# --------- PYODIDE:env --------- #



class Pile:
    "Classe à ne pas modifier"
    def __init__(self, valeurs=[]):
        """Initialise une pile
        - vide, si `valeurs` n'est pas fourni ;
        - remplie avec `valeurs` sinon.
            - Le sommet de la pile est à la fin de la liste.
        """
        self.valeurs = list(valeurs)

    def est_vide(self):
        "Renvoie un booléen : la pile est-elle vide ?"
        return self.valeurs == list()

    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + ' <- sommet'

    def depile(self):
        """
        - Si la pile est vide, provoque une erreur.
        - Sinon, dépile un élément au sommet et le renvoie.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        "Empile un élément au sommet de la pile"
        self.valeurs.append(element)



# --------- PYODIDE:code --------- #

# la classe Pile est utilisable directement pour cet exercice

def filtre_positifs(donnees):
    "fonction à compléter"
    auxiliaire = Pile()
    while not ... .est_vide():
        element = donnees. ... ()
        auxiliaire. ...(...)

    resultat = Pile()
    while ...:
        element = ...
        donnees.empile(element)
        if element >= 0:
            ...

    return ...

# --------- PYODIDE:corr --------- #

class Pile:
    def __init__(self, valeurs=[]):
        self.valeurs = list(valeurs)

    def est_vide(self):
        return self.valeurs == list()

    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + " <- sommet"

    def depile(self):
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        self.valeurs.append(element)


def filtre_positifs(donnees):
    auxiliaire = Pile()
    while not donnees.est_vide():
        element = donnees.depile()
        auxiliaire.empile(element)

    resultat = Pile()
    while not auxiliaire.est_vide():
        element = auxiliaire.depile()
        donnees.empile(element)
        if element >= 0:
            resultat.empile(element)

    return resultat

# --------- PYODIDE:tests --------- #

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == '| 4 | 7 | 0 | 6 <- sommet'
assert donnees.est_vide() is False
assert str(donnees) == '| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet'

donnees = Pile([1, 2, 3, 4])
assert str(filtre_positifs(donnees)) == '| 1 | 2 | 3 | 4 <- sommet'
assert donnees.est_vide() is False
assert str(donnees) == '| 1 | 2 | 3 | 4 <- sommet'

donnees = Pile([-4, -3, -2, -1])
assert str(filtre_positifs(donnees)) == '|  <- sommet'
assert donnees.est_vide() is False
assert str(donnees) == '| -4 | -3 | -2 | -1 <- sommet'

# --------- PYODIDE:secrets --------- #

# tests

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == "| 4 | 7 | 0 | 6 <- sommet"
assert donnees.est_vide() is False
assert str(donnees) == "| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet"

donnees = Pile([1, 2, 3, 4])
assert str(filtre_positifs(donnees)) == "| 1 | 2 | 3 | 4 <- sommet"
assert donnees.est_vide() is False
assert str(donnees) == "| 1 | 2 | 3 | 4 <- sommet"

donnees = Pile([-4, -3, -2, -1])
assert str(filtre_positifs(donnees)) == "|  <- sommet"
assert donnees.est_vide() is False
assert str(donnees) == "| -4 | -3 | -2 | -1 <- sommet"

# autres tests

donnees = Pile([])
assert str(filtre_positifs(donnees)) == "|  <- sommet", "Erreur avec donnees = []"
assert donnees.est_vide() is True
assert str(donnees) == "|  <- sommet"

donnees = Pile([6])
assert str(filtre_positifs(donnees)) == "| 6 <- sommet", "Erreur avec donnees = [6]"
assert donnees.est_vide() is False
assert str(donnees) == "| 6 <- sommet"

donnees = Pile([-6])
assert str(filtre_positifs(donnees)) == "|  <- sommet", "Erreur avec donnees = [-6]"
assert donnees.est_vide() is False
assert str(donnees) == "| -6 <- sommet"

donnees = Pile([-7, 6, -5, 4, -3, 2, -1])
assert (
    str(filtre_positifs(donnees)) == "| 6 | 4 | 2 <- sommet"
), "Erreur avec donnees = [-7, 6, -5, 4, -3, 2, -1]"
assert donnees.est_vide() is False
assert str(donnees) == "| -7 | 6 | -5 | 4 | -3 | 2 | -1 <- sommet"
