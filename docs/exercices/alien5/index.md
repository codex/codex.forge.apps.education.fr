---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Alien (5) - Fonctions
tags:
    - SNT
difficulty : 050
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :

    
    * « {{ lien_exo("Alien (1) : Appels de fonctions", "alien_1") }} »,

    * « {{ lien_exo("Alien (2) : Variables et affectations", "alien2") }} »,
    
    * « {{ lien_exo("Alien (3) : Instructions conditionnelles", "alien3") }} »,
    
    * « {{ lien_exo("Alien (4) : Boucles bornées", "alien4") }} »,
    
    * « {{ lien_exo("Alien (5) : Fonctions", "alien5") }} »,
    
    * « {{ lien_exo("Alien (6) : Tableaux", "alien6") }} ».


Les règles sont simples : l'alien :alien: se situe au départ au centre de la grille et peut être déplacé avec les fonctions `haut`, `bas`, `gauche` et `droite`.

L'objectif est de trouver la case finale de l'alien (et donc son parcours) après exécution du programme donné.

??? example "Rappels des différentes instructions"

    * `haut(4)` déplace l'alien de `#!py 4` cases vers le haut ;
    * `haut(-2)` déplace l'alien de `#!py 2` cases vers le bas ;
    * `haut()` déplace l'alien de `#!py 1` case vers le haut (c'est donc équivalent à `#!py haut(1)`).

    Le principe est similaire avec les trois autres fonctions.

Pour les questions suivantes, dessinez le parcours de l'alien en cliquant sur **la case d'arrivée** de chaque instruction exécutée. Vous pourrez ensuite valider votre parcours pour vérifier s'il est correct.

???+ abstract "Les fonctions"

    Jusqu'à présent, nous avons utilisé des fonctions prédéfinies : `haut`, `bas`...

    Il est également possible de créer ses propres fonctions.


??? question "Question 1 : Dessinez le parcours"
    
    ```python { .inline .end .w45 }
    def deplacement():
        gauche(3)
        haut(4)

    deplacement()
    ```
    
    {{ run('scripts/exo1') }}
    {{ figure("figure1") }}
 
???+ abstract "Appel d'une fonction"

    Une fois créée, une fonction peut être **appelée** autant de fois que l'on souhaite.

??? question "Question 2 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    def deplacement():
        gauche()
        haut()

    deplacement()
    deplacement()
    ```
    {{ run('scripts/exo2') }}
    {{ figure("figure2") }}

???+ abstract "Paramètres"

    Il est possible de rajouter des paramètres à la fonction.

    La valeur de chaque paramètre est fournie lors de l'appel de la fonction.
 
??? question "Question 3 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    def deplacement(x):
        gauche(x)
        haut(x)

    deplacement(2)
    deplacement(3)
    ```
    {{ run('scripts/exo3') }}
    {{ figure("figure3") }}
 

Pour les questions suivantes écrire le code nécessaire pour obtenir le déplacement souhaité (les numéros correspondent aux différentes étapes).

??? question "Question 4 : Codez le parcours"
    {{ run('scripts/grille1') }}
    {{ figure("grille1",admo_title="Figure attendue") }}

    {{ IDE('scripts/exo4') }}
    {{ figure("figure4",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 5 : Codez le parcours"
    {{ run('scripts/grille2') }}
    {{ figure("grille2",admo_title="Figure attendue") }}

    {{ IDE('scripts/exo5') }}
    {{ figure("figure5",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 6 : Codez le parcours"
    {{ run('scripts/grille3') }}
    {{ figure("grille3",admo_title="Figure attendue") }}

    {{ IDE('scripts/exo6') }}
    {{ figure("figure6",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

