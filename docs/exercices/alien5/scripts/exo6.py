# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = "deplacement(" in __USER_CODE__
assert valide is True, "On doit créer puis appeler la fonction deplacement"
app.centrer("figure6") 


# --------- PYODIDE:code --------- #
def deplacement(a, b):
    gauche(a)
    bas(b)

deplacement(...,...)
droite(...)
deplacement(...,...)
haut(...)
deplacement(...,...)


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H4', 'M4', 'M11', 'M10', 'O10', 'G10', 'G5', 'I5'])
    