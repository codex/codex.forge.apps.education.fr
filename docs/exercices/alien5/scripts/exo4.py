# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = "deplacement()" in __USER_CODE__
assert valide is True, "On doit créer puis appeler la fonction deplacement"
app.centrer("figure4") 


# --------- PYODIDE:code --------- #
def deplacement():
    gauche(...)
    haut(...)

deplacement()
deplacement()


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H6', 'E6', 'E4', 'B4'])