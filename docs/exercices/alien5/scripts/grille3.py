# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app7 = App("grille3")

def deplacement(a, b):
    app7.gauche(a)
    app7.bas(b)

deplacement(4,5)
app7.droite(7)
deplacement(1,2)
app7.haut(8)
deplacement(5,2)

app7.afficher_deplacement(etapes=True, num_lignes=False)

