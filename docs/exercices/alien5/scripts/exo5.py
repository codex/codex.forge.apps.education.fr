# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = "deplacement(" in __USER_CODE__
assert valide is True, "On doit créer puis appeler la fonction deplacement"
app.centrer("figure5") 


# --------- PYODIDE:code --------- #
def deplacement(x):
    gauche(x)
    bas(x)

deplacement(...)
deplacement(...)
deplacement(...)


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H7', 'I7', 'I5', 'K5', 'K1', 'O1'])