# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app6 = App("grille2")

def deplacement(x):
    app6.gauche(x)
    app6.bas(x)

deplacement(1)
deplacement(2)
deplacement(4)

app6.afficher_deplacement(etapes=True, num_lignes=False)