# --------- PYODIDE:env ---------- #

from collections import deque

def est_couvrant(a, g):
    n = len(g)
    assert set(a)==set(g), f"L'arbre {a} n'a pas les mêmes sommets que le graphe {g}"
    aretes_a = {(x, y) for x in a for y in a[x]}
    aretes_g = {(x, y) for x in g for y in g[x]}
    for (x, y) in aretes_a:
        assert (x, y) in aretes_g, f"L'arête {x}-{y} de l'arbre {a} n'est pas une arête du graphe {g}"
        assert (y, x) in aretes_a, f"Dans l'arbre {a}, {x} est voisin de {y} mais {y} n'est pas voisin de {x}"
    assert len(aretes_a) == 2*(n-1), f"Le nombre d'arêtes n'est pas correct: {len(aretes_a)} != {2*(n-1)}"
    connectes = {0}
    file = deque()
    file.append(0)
    while len(file) > 0:
        sommet = file.popleft()
        for voisin in a[sommet]:
            if voisin not in connectes:
                file.append(voisin)
                connectes.add(voisin)
    assert len(connectes) == n, f"Tous les sommets de l'arbre {a} ne sont pas connectés"
    return True

# --------- PYODIDE:code --------- #

def arbre_couvrant(graphe):
    arbre = {0: []}  # on choisit le sommet 0 comme racine
    ...

# --------- PYODIDE:corr --------- #

def arbre_couvrant(graphe):
    arbre = {0: []}  # on choisit le sommet 0 comme racine
    file = deque()
    file.append(0)
    while len(file) > 0:
        sommet = file.popleft()
        for voisin in graphe[sommet]:
            if voisin not in arbre:
                file.append(voisin)
                arbre[sommet].append(voisin)
                arbre[voisin] = [sommet]
    return arbre

# --------- PYODIDE:tests --------- #

g = {0: [1], 1: [0]}
assert arbre_couvrant(g) == g
g = {0: [1], 1: [0, 2], 2: [1, 3], 3: [2]}
assert arbre_couvrant(g) == g
g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
a = {0: [1, 3], 1: [0, 2], 3: [0], 2: [1]}  # un exemple d'arbre couvrant
# La fonction est_couvrant vérifie si a est un arbre couvrant de g
assert est_couvrant(a, g)
assert est_couvrant(arbre_couvrant(g), g)
g = {0: [1, 2, 3], 1: [0, 4, 5], 2: [0, 3, 4], 3: [0, 2, 4], 4: [1, 2, 3, 5], 5: [1, 4, 6], 6: [5]}
a = {0: [1, 2, 3], 1: [0, 4, 5], 2: [0], 3: [0], 4: [1], 5: [1, 6], 6: [5]}  # un exemple d'arbre couvrant
assert est_couvrant(a, g)
assert est_couvrant(arbre_couvrant(g), g)

# --------- PYODIDE:secrets --------- #

g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
assert est_couvrant(arbre_couvrant(g), g)
g = {i: [j for j in range(10) if i != j] for i in range(10)}
assert est_couvrant(arbre_couvrant(g), g)
g = {0: [1, 2, 3, 4, 5, 6], 1: [0], 2: [0], 3: [0], 4: [0], 5: [0], 6: [0]}
assert est_couvrant(arbre_couvrant(g), g)
