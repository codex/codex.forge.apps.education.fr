Il est aussi possible d'utiliser un parcours en profondeur, donc à l'aide d'une pile.

Mais l'avantage avec un parcours en largeur est qu'on obtient un arbre de hauteur minimale et donc les plus court chemins entre le sommet choisi comme racine et chacun des autres sommets du graphe.