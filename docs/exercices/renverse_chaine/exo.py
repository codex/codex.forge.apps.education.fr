

# --------- PYODIDE:code --------- #

def renverser(mot):
    ...

# --------- PYODIDE:corr --------- #

def renverser(mot):
    tom = ""
    for caractere in mot:
        tom = caractere + tom
    return tom

# --------- PYODIDE:tests --------- #

assert renverser("informatique") == "euqitamrofni"
assert renverser("nsi") == "isn"
assert renverser("") == ""

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert renverser("abcde") == "edcba"
assert renverser("159753") == "357951"
assert renverser("a6b-[]") == "][-b6a"
assert renverser("abc" * 20) == "cba" * 20