

# --------- PYODIDE:env --------- #

def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

# --------- PYODIDE:code --------- #

# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2 = ...
debut_2 = ...
fin_2 = ...

# --------- PYODIDE:corr --------- #

# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2 = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2 = 0
fin_2 = 6

# --------- PYODIDE:tests --------- #

assert correspond("ruche", automate_2, debut_2, fin_2) is True
assert correspond("riche", automate_2, debut_2, fin_2) is True
assert correspond("reche", automate_2, debut_2, fin_2) is False
assert correspond("rche", automate_2, debut_2, fin_2) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

automate_2_corr = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2_corr = 0
fin_2_corr = 6
assert automate_2 == automate_2_corr
assert debut_2 == debut_2_corr
assert fin_2 == fin_2_corr

assert correspond("aruche", automate_2, debut_2, fin_2) is False
assert correspond("briche", automate_2, debut_2, fin_2) is False
assert correspond("rich", automate_2, debut_2, fin_2) is False
assert correspond("ruch", automate_2, debut_2, fin_2) is False