

# --------- PYODIDE:env --------- #

def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

# --------- PYODIDE:code --------- #

# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3 = ...
debut_3 = ...
fin_3 = ...

# --------- PYODIDE:corr --------- #

# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3 = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3 = 0
fin_3 = 6

# --------- PYODIDE:tests --------- #

assert correspond("vroum", automate_3, debut_3, fin_3) is True
assert correspond("broum", automate_3, debut_3, fin_3) is True
assert correspond("vrououm", automate_3, debut_3, fin_3) is True
assert correspond("brou", automate_3, debut_3, fin_3) is False
assert correspond("vrouom", automate_3, debut_3, fin_3) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

automate_3_corr = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3_corr = 0
fin_3_corr = 6
assert automate_3 == automate_3_corr
assert debut_3 == debut_3_corr
assert fin_3 == fin_3_corr

assert correspond("brouououououououm", automate_3, debut_3, fin_3) is True
assert correspond("vrououououououououm", automate_3, debut_3, fin_3) is True
assert correspond("brououououououom", automate_3, debut_3, fin_3) is False
assert correspond("vrouououououououom", automate_3, debut_3, fin_3) is False
assert correspond("vrouououououououou", automate_3, debut_3, fin_3) is False