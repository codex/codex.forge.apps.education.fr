# --- hdr, 0, 1, 2, 3 | secrets, 1, 2, 3 --- #
def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

# --- exo, 0 --- #
# Cet automate reconnaît "bato" et "baato" mais pas "bota" (exemple du sujet)
automate_0 = {
    (0, "b"): 1,
    (1, "a"): 2,
    (2, "a"): 2,
    (2, "t"): 3,
    (3, "o"): 4,
    (4, "o"): 4,
}
debut_0 = 0
fin_0 = 4

# tests
assert correspond("bato", automate_0, debut_0, fin_0) is True
assert correspond("baato", automate_0, debut_0, fin_0) is True
assert correspond("bota", automate_0, debut_0, fin_0) is False

# --- exo, 1 --- #
# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1 = ...
debut_1 = ...
fin_1 = ...

# --- corr, 1 --- #
# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1 = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 4, (4, "a"): 4}
debut_1 = 0
fin_1 = 4

# --- tests, 1 --- #
assert correspond("tada", automate_1, debut_1, fin_1) is True
assert correspond("tadaaa", automate_1, debut_1, fin_1) is True
assert correspond("tad", automate_1, debut_1, fin_1) is False
assert correspond("taada", automate_1, debut_1, fin_1) is False

# --- secrets, 1 --- #
automate_1_corr = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 4, (4, "a"): 4}
debut_1_corr = 0
fin_1_corr = 4
assert automate_1 == automate_1_corr
assert debut_1 == debut_1_corr
assert fin_1 == fin_1_corr

assert correspond("tadaaaaaaaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is True
assert correspond("tadaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is True
assert correspond("atada", automate_1, debut_1, fin_1) is False
assert correspond("bbbaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is False
assert correspond("taaaaaaaaaaaada", automate_1, debut_1, fin_1) is False



# --- exo, 2 --- #
# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2 = ...
debut_2 = ...
fin_2 = ...

# --- corr, 2 --- #
# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2 = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2 = 0
fin_2 = 6

# --- tests, 2 --- #
assert correspond("ruche", automate_2, debut_2, fin_2) is True
assert correspond("riche", automate_2, debut_2, fin_2) is True
assert correspond("reche", automate_2, debut_2, fin_2) is False
assert correspond("rche", automate_2, debut_2, fin_2) is False

# --- secrets, 2 --- #
automate_2_corr = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2_corr = 0
fin_2_corr = 6
assert automate_2 == automate_2_corr
assert debut_2 == debut_2_corr
assert fin_2 == fin_2_corr

assert correspond("aruche", automate_2, debut_2, fin_2) is False
assert correspond("briche", automate_2, debut_2, fin_2) is False
assert correspond("rich", automate_2, debut_2, fin_2) is False
assert correspond("ruch", automate_2, debut_2, fin_2) is False


# --- exo, 3 --- #
# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3 = ...
debut_3 = ...
fin_3 = ...

# --- corr, 3 --- #
# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3 = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3 = 0
fin_3 = 6

# --- tests, 3 --- #
assert correspond("vroum", automate_3, debut_3, fin_3) is True
assert correspond("broum", automate_3, debut_3, fin_3) is True
assert correspond("vrououm", automate_3, debut_3, fin_3) is True
assert correspond("brou", automate_3, debut_3, fin_3) is False
assert correspond("vrouom", automate_3, debut_3, fin_3) is False

# --- secrets, 3 --- #
automate_3_corr = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3_corr = 0
fin_3_corr = 6
assert automate_3 == automate_3_corr
assert debut_3 == debut_3_corr
assert fin_3 == fin_3_corr

assert correspond("brouououououououm", automate_3, debut_3, fin_3) is True
assert correspond("vrououououououououm", automate_3, debut_3, fin_3) is True
assert correspond("brououououououom", automate_3, debut_3, fin_3) is False
assert correspond("vrouououououououom", automate_3, debut_3, fin_3) is False
assert correspond("vrouououououououou", automate_3, debut_3, fin_3) is False

