---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Automate en dictionnaire
tags:
    - tuple
    - dictionnaire
maj: 01/02/2024
---

Une *expression régulière* est une chaine de caractères permettant de décrire d'autres chaines de caractères.

Par exemple, la chaine `#!py "ba+to+"` permet de décrire les « mots » `#!py "bato"`, `#!py "baato"`, `#!py "batoo"` et `#!py "baatooooo"` : un caractère `#!py "b"` suivi d'au moins un caractère `#!py "a"`, puis un `#!py "t"` et enfin un ou plusieurs caractères `#!py "o"`.

Les expressions régulières peuvent être aussi représentées sous forme d'*automates*. Ainsi, la chaine `#!py "ba+to+"` est équivalente à l'automate ci-dessous :

![Automate associé à "ba+to+"](ba+to+.svg){ .center .autolight}

Dans ce graphe, chaque nœud représente un « état » de l'automate. L'état 0 est l'état de **début** et 4 celui de **fin**.

On parcourt l'automate de la façon suivante :

* on débute dans l'état de départ en lisant le premier caractère de la chaine,
* si ce caractère est un `#!py "b"`, on passe dans l'état 1 et on lit le caractère suivant,
* dans l'état 1, si le caractère lu est un `#!py "a"`, on passe dans l'état 2 et on lit le caractère suivant,
* dans l'état 2, si le caractère lu est un `#!py "a"`, on reste dans l'état 2 et on lit le caractère suivant,
* *etc*

Si on se trouve dans l'état final après avoir lu le dernier caractère de la chaine, alors celle-ci correspond au motif cherché.

La chaine `#!py "baato"` est reconnue car elle correspond au parcours 0 → 1 → 2 → 2 → 3 → 4.

On représente un automate par une série de règles stockées dans un dictionnaire Python. Chaque règle associe à un couple  `(état, caractère lu)` la valeur du prochain état.

Ainsi l'automate ci-dessus est représenté par le dictionnaire `#!py automate_0 = {(0, "b"): 1, (1, "a"): 2, (2, "a"): 2, (2, "t"): 3, (3, "o"): 4, (4, "o"): 4}`. L'état initial est le `#!py 0`, le final `#!py 4`.

On demande d'écrire les dictionnaires représentant les automates suivants. On identifiera aussi les états de départ et de fin.

Afin de vérifier si de tester si un mot correspond à l'expression régulière représentée par un automate, vous pourrez utiliser la fonction `correspond` qui prend en paramètres :

*   `mot`, un texte correspondant au texte à vérifier ;
*   `automate`, un dictionnaire correspondant à un automate ;
*   `debut`, un entier correspondant à l'état initial de l'automate ;
*   `fin`, un entier correspondant à l'état final de l'automate.

??? abstract "Exemple avec l'automate de l'énoncé"
    Vous pouvez tester différents mots avec l'automate de l'exemple et la fonction `correspond`.

    Cet automate reconnaît `#!py "bato"` et `#!py "baato"` mais pas "bota" (exemple du sujet) :    

    ![Automate associé à ba+to+"](ba+to+.svg){ .center .autolight}  

    {{ IDE('exo_0') }}

??? question "Question 1"

    Cet automate reconnaît `#!py "tada"` et `#!py "tadaaa"` mais pas `#!py "tad"` ni `#!py "taada"` :

    ![Automate associé à "tada+"](tada+.svg){ .center .autolight}

    Compléter la définition de cet automate ci-dessous.

    {{ IDE('exo_1') }}


??? question "Question 2"

    Cet automate reconnaît `#!py "ruche"` et `#!py "riche"` mais pas `#!py "reche"` ni `#!py "rche"` :

    ![Automate associé à "r(u|i)che"](ruiche.svg){ .center .autolight}

    Compléter la définition de cet automate ci-dessous.

    {{ IDE('exo_2') }}


??? question "Question 3"

    Cet automate reconnaît `#!py "vroum"`, `#!py "broum"` et `#!py "vrououm"` mais pas `#!py "brou"` ni `#!py "vrouom"`.

    ![Automate associé à "(v|b)r(ou)+m"](vbrououm.svg){ .center .autolight}

    Compléter la définition de cet automate ci-dessous.

    {{ IDE('exo_3') }}


