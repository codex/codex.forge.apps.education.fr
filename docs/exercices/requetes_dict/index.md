---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Requêtes dans une liste de dictionnaires
tags:
    - booléen
    - grille
    - dictionnaire
maj: 30/10/2024
difficulty: 180
---

On s'intéresse dans cet exercice aux données décrivant différents animaux ([source](https://www.kaggle.com/datasets/agajorte/zoo-animals-extended-dataset?resource=download&select=zoo2.csv)).

Chaque animal est décrit par les attributs suivants :

|   Attributs   |           Description           | Type Python |
| :-----------: | :-----------------------------: | :---------: |
|     `air`     |    Respire-t-il dans l'air ?    | `#!py bool` |
|  `aquatique`  |       Vit-il dans l'eau ?       | `#!py bool` |
|    `dents`    |       A-t-il des dents ?        | `#!py bool` |
| `domestique`  |  Est-ce un animal domestique ?  | `#!py bool` |
|    `lait`     |    Allaite-t-il ses petits ?    | `#!py bool` |
|     `nom`     |             Son nom             | `#!py str`  |
|  `nageoires`  |     A-t-il des nageoires ?      | `#!py bool` |
|    `oeufs`     |       Pond-il des œufs ?        | `#!py bool` |
|   `pattes`    |   Combien a-t-il de pattes ?    | `#!py int`  |
|    `poils`     |       A-t-il des poils ?        | `#!py bool` |
|    `plumes`    |       A-t-il des plumes ?       | `#!py bool` |
|  `prédateur`  |      Est-ce un prédateur ?      | `#!py bool` |
|    `queue`    |       A-t-il une queue ?        | `#!py bool` |
| `taille_chat` | Est-il de la taille d'un chat ? | `#!py bool` |
|  `venimeux`   |        Est-il venimeux ?        | `#!py bool` |
|  `vertébré`   |      Est-ce un vertébré ?       | `#!py bool` |
|   `volant`    |         Peut-il voler ?         | `#!py bool` |

Ces données ont été importées dans une liste Python nommée `animaux`. Chacun des éléments de cette liste est un dictionnaire contenant les informations décrivant un animal. On affiche ci-dessous le premier élément de la liste `animaux` :

```pycon
>>> animaux[0]  # l'ensemble des informations du 1er animal
{'air': True, 'aquatique': True, 'dents': False, 'domestique': True, 'lait': False, 'nom': 'tortue', 'nageoires': False, 'oeufs': True, 'pattes': 4, 'po
ils': False, 'plumes': False, 'prédateur': False, 'queue': True, 'taille_chat': True, 'venimeux': False, 'vertébré': True, 'volant': False}
>>> animaux[0]["nom"]  # le nom du 1er animal
'tortue'
>>> animaux[0]["poils"]  # le 1er animal a-t-il des poils ?
False
>>> animaux[0]["aquatique"]  # le 1er animal vit-il dans l'eau ?
True
```

Comme on peut le voir ci-dessus, cet élément décrit la tortue :turtle: qui, comme chacun sait, n'a pas de poils et vit dans l'eau ! 

Le but de l'exercice est d'effectuer des requêtes sur cette liste afin de sélectionner certains éléments (certains animaux). On utilise pour ce faire les listes en compréhension.

L'instruction ci-dessous permet par exemple de sélectionner les noms des animaux volants :

```pycon
>>> [animal["nom"] for animal in animaux if animal["volant"]]
['moustique', 'frelon', 'papillon', 'blatte', 'libellule', 'cigale', 'fourmi']
```

Il est aussi possible de compter les résultats en faisant par exemple (nombre d'animaux aquatiques sans pattes) :

```pycon
>>> len([animal for animal in animaux if animal["aquatique"] and animal["pattes"] == 0])
10
```

Compléter le code ci-dessous en effectuant les requêtes proposées.

{{ IDE('exo', MAX=20, STD_KEY="bk4fg1") }}
