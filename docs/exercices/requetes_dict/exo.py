# --------- PYODIDE:env --------- #
from js import fetch

url_fichier = "zoo_traduction.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()
lignes = contenu.splitlines()

animaux = []

attributs = {
    "air": lambda x: bool(int(x)),
    "aquatique": lambda x: bool(int(x)),
    "dents": lambda x: bool(int(x)),
    "domestique": lambda x: bool(int(x)),
    "lait": lambda x: bool(int(x)),
    "nom": lambda x: str(x),
    "nageoires": lambda x: bool(int(x)),
    "oeufs": lambda x: bool(int(x)),
    "pattes": lambda x: int(x),
    "poils": lambda x: bool(int(x)),
    "plumes": lambda x: bool(int(x)),
    "prédateur": lambda x: bool(int(x)),
    "queue": lambda x: bool(int(x)),
    "taille_chat": lambda x: bool(int(x)),
    "venimeux": lambda x: bool(int(x)),
    "vertébré": lambda x: bool(int(x)),
    "volant": lambda x: bool(int(x)),
}

for i in range(1, len(lignes)):
    valeurs = lignes[i].split(",")
    dico = {}
    for x, (att, f) in zip(valeurs, attributs.items()):
        dico[att] = f(x)
    animaux.append(dico)
# --------- PYODIDE:code --------- #
# (exemple) Quels sont les noms des animaux volants ?
requete_1 = [animal["nom"] for animal in animaux if animal["volant"]]

# (exemple) Combien y-a-t'il d'animaux aquatiques sans pattes ?
requete_2 = len([animal["nom"] for animal in animaux if animal["aquatique"] and animal["pattes"] == 0])

# Quels sont les noms de tous les animaux ?
requete_3 = ...

# Quels sont les noms des prédateurs ?
requete_4 = ...

# Quels sont les noms des animaux invertébrés ?
requete_5 = ...

# Quels sont les noms des animaux ne vivant pas dans l'eau ?
requete_6 = ...

# Combien y-a-t'il de prédateurs venimeux ?
requete_7 = ...

# Combien y-a-t'il d'animaux aquatiques ou volants ?
requete_8 = ...

# Combien y-a-t'il d'animaux ayant strictement plus de quatre pattes ?
requete_9 = ...

# Combien y-a-t'il d'animaux sans poils ?
requete_10 = ...

# Quels sont les noms des animaux aquatiques sans nageoires ?
requete_11 = ...

# Combien y-a-t'il de prédateurs de la taille d'un chat ?
requete_12 = ...

# Combien y-a-t'il d'animaux ayant des dents et des plumes ?
requete_13 = ...

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et pondant des oeufs ?
requete_14 = ...


# --------- PYODIDE:corr --------- #
# Quels sont les noms des animaux volants ?
requete_1 = [animal["nom"] for animal in animaux if animal["volant"]]

# Combien y-a-t'il d'animaux aquatiques sans pattes ?
requete_2 = len([animal["nom"] for animal in animaux if animal["aquatique"] and animal["pattes"] == 0])

# Quels sont les noms de tous les animaux ?
requete_3 = [animal["nom"] for animal in animaux]

# Quels sont les noms des prédateurs ?
requete_4 = [animal["nom"] for animal in animaux if animal["prédateur"]]

# Quels sont les noms des animaux invertébrés ?
requete_5 = [animal["nom"] for animal in animaux if not animal["vertébré"]]

# Quels sont les noms des animaux ne vivant pas dans l'eau ?
requete_6 = [animal["nom"] for animal in animaux if not animal["aquatique"]]

# Combien y-a-t'il de prédateurs venimeux ?
requete_7 = len([animal for animal in animaux if animal["prédateur"] and animal["venimeux"]])

# Combien y-a-t'il d'animaux aquatiques ou volants ?
requete_8 = len([animal for animal in animaux if animal["aquatique"] or animal["volant"]])

# Combien y-a-t'il d'animaux ayant strictement plus de quatre pattes ?
requete_9 = len([animal for animal in animaux if animal["pattes"] > 4])

# Combien y-a-t'il d'animaux sans poils ?
requete_10 = len([animal["nom"] for animal in animaux if not animal["poils"]])

# Quels sont les noms des animaux aquatiques sans nageoires ?
requete_11 = [animal["nom"] for animal in animaux if animal["aquatique"] and not animal["nageoires"]]

# Combien y-a-t'il de prédateurs de la taille d'un chat ?
requete_12 = len([animal for animal in animaux if animal["prédateur"] and animal["taille_chat"]])

# Combien y-a-t'il d'animaux ayant des dents et des plumes ?
requete_13 = len([animal for animal in animaux if animal["dents"] and animal["plumes"]])

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et pondant des oeufs ?
requete_14 = len([animal for animal in animaux if animal["domestique"] and animal["taille_chat"] and animal["oeufs"]])

# --------- PYODIDE:secrets --------- #
ok = True
# Quels sont les noms des animaux volants ?
attendu_1 = [animal["nom"] for animal in animaux if animal["volant"]]
assert requete_1 == attendu_1, f"Il ne faut pas modifier la requête 1"

# Combien y-a-t'il d'animaux aquatiques sans pattes ?
attendu_2 = len([animal["nom"] for animal in animaux if animal["aquatique"] and animal["pattes"] == 0])
assert requete_2 == attendu_2, f"Il ne faut pas modifier la requête 2"

# Quels sont les noms de tous les animaux ?
attendu_3 = [animal["nom"] for animal in animaux]

# Quels sont les noms des prédateurs ?
attendu_4 = [animal["nom"] for animal in animaux if animal["prédateur"]]

# Quels sont les noms des animaux invertébrés ?
attendu_5 = [animal["nom"] for animal in animaux if not animal["vertébré"]]

# Quels sont les noms des animaux ne vivant pas dans l'eau ?
attendu_6 = [animal["nom"] for animal in animaux if not animal["aquatique"]]

# Combien y-a-t'il de prédateurs venimeux ?
attendu_7 = len([animal for animal in animaux if animal["prédateur"] and animal["venimeux"]])

# Combien y-a-t'il d'animaux aquatiques ou volants ?
attendu_8 = len([animal for animal in animaux if animal["aquatique"] or animal["volant"]])

# Combien y-a-t'il d'animaux ayant strictement plus de quatre pattes ?
attendu_9 = len([animal for animal in animaux if animal["pattes"] > 4])

# Combien y-a-t'il d'animaux sans poils ?
attendu_10 = len([animal["nom"] for animal in animaux if not animal["poils"]])

# Quels sont les noms des animaux aquatiques sans nageoires ?
attendu_11 = [animal["nom"] for animal in animaux if animal["aquatique"] and not animal["nageoires"]]

# Combien y-a-t'il de prédateurs de la taille d'un chat ?
attendu_12 = len([animal for animal in animaux if animal["prédateur"] and animal["taille_chat"]])

# Combien y-a-t'il d'animaux ayant des dents et des plumes ?
attendu_13 = len([animal for animal in animaux if animal["dents"] and animal["plumes"]])

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et pondant des oeufs ?
attendu_14 = len([animal for animal in animaux if animal["domestique"] and animal["taille_chat"] and animal["oeufs"]])

requetes = [requete_3, requete_4, requete_5, requete_6, requete_7, requete_8, requete_9, requete_10, requete_11, requete_12, requete_13, requete_14]
attendus = [attendu_3, attendu_4, attendu_5, attendu_6, attendu_7, attendu_8, attendu_9, attendu_10, attendu_11, attendu_12, attendu_13, attendu_14]

for i in range(len(requetes)):
    n = i+3
    terminal_message("bk4fg1", f"Requête {n} : ",new_line=False)
    if requetes[i] == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif requetes[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"
