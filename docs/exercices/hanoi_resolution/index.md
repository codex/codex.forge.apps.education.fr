---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Résolution de la tour de Hanoï
tags:
    - récursivité
    - pile
difficulty: 190
maj: 03/11/2024
---

<script>
function isDark(){
  return !$('label[for=__palette_0]').attr('hidden');
}
</script>


![Tour de Hanoï](tour_hanoi.png){ width=30%; align=right }

La tour de Hanoï est un jeu inventé par le mathématicien français [Édouard Lucas](https://fr.wikipedia.org/wiki/%C3%89douard_Lucas){target="_blank"}.

Les règles sont simples :

* le jeu est organisé autour de trois bâtons de bois que l'on nommera $a$, $b$ et $c$ ;
* sur ces bâtons, on glisse des pièces circulaires de diamètres différents. Le nombre de pièces est appelé $n$ ;
* les pièces doivent toujours être empilées dans un ordre décroissant : il est interdit de poser une grande pièce sur une petite ;
* à chaque étape on déplace une pièce d'un bâton à l'autre ;
* initialement, les pièces sont sur le bâton $a$. Le but du jeu est de les amener sur le bâton $c$.



{{ figure(
    "anim_jeu",
    admo_kind="???+",
    admo_title="A vous de jouer",
    inner_text="La partie va bientôt commencer...",
    admo_class="autolight")
}}

{{ run("anim_jeu") }}

Les trois bâtons sont modélisés par des instances de la classe `Pile` : `a`, `b` et `c`.
    
{{ remarque('interface_Pile') }}

???+ info "Remarques"

    * La classe `Pile` dispose d'une méthode qui permet de dessiner la pile.
    
    * La classe empêchera d'empiler une pièce sur une pièce plus petite.

Des pièces de diamètres $140$, $120$, $100$, $80$, $60$ et $40$ sont empilés sur le bâton `a`. Les bâtons `b` et `c` sont initialement vides.

```pycon title="L'état initial"
>>> a = Pile()
>>> b = Pile()
>>> c = Pile()
>>> a.empile(140)
>>> a.empile(120)
>>> a.empile(100)
>>> a.empile(80)
>>> a.empile(60)
>>> a.empile(40)
>>> a
'140|120|100|80|60|40|'
>>> b
''
>>> c
''
```

Les différentes questions sont indépendantes.

??? question "Déplacer `#!py 1` pièce"

    Compléter le code de la procédure `deplace_1` qui prend en paramètres la pile de `depart` et la pile d'`arrivee`, et qui permet de transférer une pièce de l'une vers l'autre.
    
    ???+ example "Exemples"
        On se place dans l'état initial décrit plus haut. L'exemple ci-dessous permet de déplacer la pièce au sommet de la `a` sur la `b`.

        ```pycon title=""
        >>> deplace_1(a, b)
        >>> a
        `140|120|100|80|60|`
        >>> b
        '40|'
        ```
    

    {{ IDE('exo_1')}}
    
    {{ figure(
        "figure1", 
        div_class = "py_mk_figure orange", 
        admo_kind = "!!!", 
        admo_title = "L'animation se lancera ici", 
        inner_text = "Animation", 
    ) }}

??? question "Déplacer `#!py 2` pièces"

    Écrire une procédure `deplace_2` qui prend en paramètres $3$ piles `depart`, `arrivee` et `intermediaire` et qui déplace les $2$ pièces au sommet du bâton de `depart` vers le bâton d'`arrivee` en utilisant le bâton `intermediaire`.
    
    ???+ example "Exemple"

        On se place dans l'état initial décrit plus haut. L'appel ci-dessous permet de déplacer les $2$ pièces au sommet de la pile `a` vers la pile `c`, en s'aidant de la pile `b` comme intermédiaire :

        ```pycon title=""
        >>> deplace_2(a, c, b)
        >>> a
        `140|120|100|80|`
        >>> b
        ''
        >>> c
        '60|40|'
        ```

    !!! tips "Indication"
        
        On pourra réutiliser la procédure `deplace_1` qui est déjà chargée en mémoire.

    {{ IDE('exo_2')}}
    
    {{ figure(
        "figure2", 
        div_class = "py_mk_figure orange", 
        admo_kind = "!!!", 
        admo_title = "L'animation se lancera ici", 
        inner_text = "Animation", 
    ) }}

??? question "Déplacer `#!py n` pièces"

    Un stratégie de résolution optimale, qui résout le problème en un nombre *minimal* de déplacements, repose sur l'observation suivante. Transporter $n$ pièces du bâton $a$ vers le $c$ nécessite de :

    * transporter les $n - 1$ pièces du dessus du bâton $a$ vers le $b$ ;
    
    * déplacer la plus grande pièce du bâton $a$ vers le $c$ ;
    
    * transporter les $n - 1$ pièces du bâton $b$ vers le $c$.

    L'illustration ci-dessous[^1] illustre la résolution du problème dans le cas $n = 4$ :

    [^1]: Les illustrations de cette page proviennent d'<a href="https://commons.wikimedia.org/w/index.php?curid=228623" target="_blank">ici</a> et <a href="https://commons.wikimedia.org/w/index.php?curid=85401" target="_blank">là</a>.

    ![Animation](Tower_of_Hanoi_4.gif){.center}

    Écrire une procédure récursive `deplace_n` qui prend en paramètres le nombre `n` de pièces à déplacer et $3$ piles `depart`, `arrivee`, `intermediaire` et qui déplace les `n` pièces du sommet du bâton de `depart` vers le bâton d'`arrivee` en utilisant le bâton `intermediaire`.

    On pourra réutiliser les procédures `deplace_1` et `deplace_2` qui sont déjà chargées en mémoire.

    ??? tips "Aide"
        Les solutions pour $n = 1$ et $n = 2$ sont triviales : voir questions précédentes.
    
    ???+ example "Exemples"

        On se place dans l'état initial décrit plus haut. L'appel ci-dessous permet de déplacer les $4$ pièces au sommet du bâton `a` vers le bâton `c`, en s'aidant du bâton `b` comme intermédiaire :

        ```pycon title=""
        >>> deplace_n(4, a, c, b)
        >>> a
        `140|120|`
        >>> c
        '100|80|60|40|'
        ```
    
    {{ IDE('exo_3')}}
    
    {{ figure(
        "figure3", 
        div_class = "py_mk_figure orange", 
        admo_kind = "!!!", 
        admo_title = "L'animation se lancera ici", 
        inner_text = "Animation", 
    ) }}

