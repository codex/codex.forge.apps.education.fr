# --------- PYODIDE:env --------- #
import p5

COULEUR_PIECES = (126, 86, 194)


class Animation(p5.Sketch):
    def __init__(self, etats):
        self.infos = [(120, 30, "a"), (325, 30, "b"), (545, 30, "c")]
        self.etats = etats
        self.num_etat = 0

    @p5.hook
    def setup(self):
        self.p5.createCanvas(600, 175)
        self.p5.textFont("Calibri")
        self.p5.textSize(40)
        self.p5.frameRate(1)

    @p5.hook
    def draw(self):
        if self.num_etat < len(self.etats):
            etat = self.etats[self.num_etat]
            self.p5.fill(0)
            self.p5.stroke(0)
            self.p5.background(255)
            self.p5.strokeWeight(1)
            self.p5.textSize(22)
            self.p5.text(f"Nombre de coups : {str(self.num_etat)}", 0, 20)

            for i in range(3):
                self.p5.fill(0)
                x, y, nom = self.infos[i]
                self.p5.stroke(50)
                self.p5.strokeWeight(3)
                self.p5.line(x, y, x, y + 100)
                self.p5.textSize(22)
                self.p5.stroke(0)
                self.p5.strokeWeight(1)
                self.p5.text(nom, x - 5, y + 129)
                etat_pile = etat[i]
                self.p5.strokeWeight(3)
                p5.stroke(50)
                for j, diametre in enumerate(etat_pile):
                    self.p5.fill(*COULEUR_PIECES)
                    self.p5.rect(x - diametre / 2, y + 100 - j * 10, diametre, 10)

            self.num_etat += 1
        else:
            self.p5.noLoop()


class Partie:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.etats = [
            (self.a.contenu.copy(), self.b.contenu.copy(), self.c.contenu.copy())
        ]

    def ajoute_etat(self):
        self.etats.append(
            (self.a.contenu.copy(), self.b.contenu.copy(), self.c.contenu.copy())
        )


class Pile:
    """Classe définissant une structure de pile"""

    empilement = 0

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def empile(self, x):
        """Place x au sommet de la pile"""
        Pile.empilement += 1
        if not self.est_vide() and not self.contenu[-1] > x:
            raise ValueError(
                "Il est interdit d'empiler une pièce de diamètre inférieur à celle du dessus de la pile"
            )
        self.contenu.append(x)
        if "partie" in globals():
            partie.ajoute_etat()

    def depile(self):
        """Retire et renvoie l'élément placé au sommet de la pile.
        Provoque une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.contenu.pop()

    def __repr__(self):
        """Affichage de la pile"""
        if self.est_vide():
            return ""
        return f"{'|'.join(str(x) for x in self.contenu)}|"


def nouvelle_partie():
    # définition des piles
    a = Pile()
    b = Pile()
    c = Pile()
    a.empile(140)
    a.empile(120)
    a.empile(100)
    a.empile(80)
    a.empile(60)
    a.empile(40)
    Pile.empilement = 0
    return a, b, c


def deplace_1(depart, arrivee):
    pass


# --------- PYODIDE:code --------- #
def deplace_1(depart, arrivee):
    ...


# --------- PYODIDE:corr --------- #
def deplace_1(depart, arrivee):
    valeur = depart.depile()
    arrivee.empile(valeur)


# --------- PYODIDE:tests --------- #
a, b, c = nouvelle_partie()
deplace_1(a, b)
deplace_1(a, c)

assert str(a) == "140|120|100|80|"
assert str(b) == "40|"
assert str(c) == "60|"

# --------- PYODIDE:secrets --------- #
a, b, c = Pile(), Pile(), Pile()
print(a)
for i in range(6):
    a.empile(140 - i * 20)


deplace_1(a, b)
assert str(a) == "140|120|100|80|60|"
deplace_1(a, c)
assert str(a) == "140|120|100|80|"
deplace_1(b, c)
assert str(c) == "60|40|"
deplace_1(a, b)
deplace_1(c, b)
deplace_1(c, a)
assert str(a) == "140|120|100|60|"
assert str(b) == "80|40|"
assert str(c) == ""
succes_tests = True

# --------- PYODIDE:post --------- #
a, b, c = nouvelle_partie()
partie = Partie(a, b, c)
deplace_1(a, b)
deplace_1(a, c)

anim1 = Animation(partie.etats)
anim1.run("figure1", stop_others=False)
