# --- PYODIDE:env --- #
import p5
import js

ATTENTE = False
CHOIX = True
AUCUNE = -1

class Pile:
    """Classe définissant une structure de pile"""

    empilement = 0

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def dessus(self):
        """Renvoie la valeur du dessus"""
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.contenu[-1]

    def empile(self, x):
        """Place x au sommet de la pile"""
        Pile.empilement += 1
        if not self.est_vide() and not self.contenu[-1] > x:
            raise ValueError(
                "Il est interdit d'empiler une pièce de diamètre inférieur à celle du dessus de la pile"
            )
        self.contenu.append(x)
        if "partie" in globals():
            partie.ajoute_etat()

    def depile(self):
        """Retire et renvoie l'élément placé au sommet de la pile.
        Provoque une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.contenu.pop()

    def __repr__(self):
        """Affichage de la pile"""
        if self.est_vide():
            return ""
        return f"{'|'.join(str(x) for x in self.contenu)}|"



class Animation(p5.Sketch):
    def __init__(self):
        self.infos = [(125, 40, "p0"), (325, 40, "p1"), (525, 40, "p2")]
        self.zones = [
            (125 - 80, 40 - 10, 2*80, 110),
            (325 - 80, 40 - 10, 2*80, 110),
            (525 - 80, 40 - 10, 2*80, 110),
            ]
        self.nb_pieces = 5
        self.nouvelle_partie()

    def nouvelle_partie(self):
        # définition des piles
        a = Pile()
        b = Pile()
        c = Pile()
        a.contenu = [140 - 20*i for i in range(self.nb_pieces)]
        Pile.empilement = 0
        self.piles = [a, b, c]
        self.etat = ATTENTE
        self.zone_selectionnee = AUCUNE
        self.piece_selectionnee = 0
        self.historique = []
        self.redo = []

    @p5.hook
    def setup(self):
        self.p5.createCanvas(665, 200)
        self.p5.textFont("Calibri")
        self.p5.textSize(40)

    @p5.hook
    def draw(self):
        # Remise à zéro
        if js.isDark():
            crayon = 255
            fond = 255
            crayongris = 100
            fondgris = (250, 250, 250, 30)
            background = (43, 42, 51)
            COULEUR_PIECES = (255, 175, 0)
        else:
            crayon = 0
            fond = 0
            crayongris = 50
            fondgris = (220,)
            background = (255,)
            COULEUR_PIECES = (126, 86, 194)
        self.p5.stroke(crayon)
        self.p5.fill(fond)
        self.p5.background(*background)


        # Le socle
        x, y, _ = self.infos[0]
        self.p5.strokeWeight(3)
        self.p5.stroke(crayongris)
        self.p5.fill(crayongris)
        self.p5.rect(x-100, y+100, 2*200*2*100, 10)

        sur_zone = None
        # La zone de survol
        for i, (x, y, l, h) in enumerate(self.zones):
            if x <= self.p5.mouseX <= x + l and y <= self.p5.mouseY <= y + h:
                sur_zone = i
                ok = self.etat != CHOIX or self.piles[i].est_vide() or self.piece_selectionnee < self.piles[i].dessus()
                if i != self.zone_selectionnee:
                    if ok:
                        self.p5.stroke(crayongris)
                        self.p5.fill(*fondgris)
                    else:
                        self.p5.stroke(*(150, 0, 0))
                        self.p5.fill(200, 0, 0, 50)
                    self.p5.rect(x, y, l, h)
                break

        # Le nombre de coups
        self.p5.strokeWeight(1)
        self.p5.stroke(crayon)
        self.p5.fill(fond)
        self.p5.textSize(22)
        self.p5.text(f"Nombre de coups : {Pile.empilement}", 0, 20)

        if Pile.empilement == 0:
            self.p5.stroke(crayongris)
            self.p5.fill(crayongris)
        # Le bouton "Recommencer"
        self.p5.text("Recommencer", 0, 195)
        # Le bouton "Annuler"
        self.p5.text("Annuler", 170, 195)

        if len(self.redo) > 0:
            self.p5.stroke(crayon)
            self.p5.fill(fond)
        else:
            self.p5.stroke(crayongris)
            self.p5.fill(crayongris)
        # Le bouton "Refaire"
        self.p5.text("Refaire", 280, 195)

        self.p5.stroke(crayon)
        self.p5.fill(fond)
        # Le bouton "+"
        self.p5.text("\u2295", 0, 95)
        # Le bouton "-"
        self.p5.text("\u2296", 0, 115)

        # Les piles
        for i, p in enumerate(self.piles):
            # Les barres
            self.p5.fill(fond)
            x, y, nom = self.infos[i]
            self.p5.stroke(crayongris)
            self.p5.strokeWeight(3)
            self.p5.line(x, y, x, y + 100)
            self.p5.textSize(22)
            self.p5.stroke(crayon)
            self.p5.strokeWeight(1)
            self.p5.text(nom, x - 5, y + 129)

            # Les piles
            self.p5.strokeWeight(3)
            self.p5.stroke(crayongris)
            for j, diametre in enumerate(p.contenu):
                self.p5.fill(*COULEUR_PIECES)
                self.p5.rect(x - diametre / 2, y + 90 - j * 10, diametre, 10)

        # La zone sélectionnée
        if self.zone_selectionnee != AUCUNE:
            x, y, l, h = self.zones[self.zone_selectionnee]
            self.p5.stroke(*(0, 150, 0))
            self.p5.fill(*(0, 200, 0, 50))
            self.p5.rect(x, y, l, h)

        # La piece selectionnee
        if self.piece_selectionnee != 0:
            h = self.p5.mouseY - 5
            self.p5.strokeWeight(3)
            self.p5.stroke(crayongris)
            self.p5.fill(*COULEUR_PIECES)
            if sur_zone is not None:
                x, y, nom = self.infos[sur_zone]
                h = min(h, y + 90 - len(self.piles[sur_zone].contenu) * 10)
                self.p5.rect(x - self.piece_selectionnee / 2, h, self.piece_selectionnee, 10)
            else:
                self.p5.rect(self.p5.mouseX - self.piece_selectionnee / 2, self.p5.mouseY - 5, self.piece_selectionnee, 10)

    def reposer(self):
        self.piles[self.zone_selectionnee].contenu.append(self.piece_selectionnee)
        self.etat = ATTENTE
        self.zone_selectionnee = AUCUNE
        self.piece_selectionnee = 0

    @p5.hook
    def mousePressed(self, _event):
        # La zone de sélection
        if 0 <= self.p5.mouseX <= self.p5.textWidth("Recommencer") and 180 <= self.p5.mouseY <= 200:
            self.nouvelle_partie()
            return None
        elif 0 <= self.p5.mouseX-170 <= self.p5.textWidth("Annuler") and 180 <= self.p5.mouseY <= 200:
            if self.etat == CHOIX:
                # On repose juste
                self.reposer()
            elif len(self.historique) > 0:
                de, vers = self.historique.pop()
                self.redo.append((de, vers))
                piece = self.piles[vers].depile()
                self.piles[de].contenu.append(piece)
                Pile.empilement -= 1
            return None
        elif 0 <= self.p5.mouseX-280 <= self.p5.textWidth("Refaire") and 180 <= self.p5.mouseY <= 200:
            if self.etat == CHOIX:
                # On repose juste
                self.reposer()
            elif len(self.redo) > 0:
                de, vers = self.redo.pop()
                self.historique.append((de, vers))
                piece = self.piles[de].depile()
                self.piles[vers].empile(piece)
            return None
        elif 0 <= self.p5.mouseX <= self.p5.textWidth("\u2295") and 80 <= self.p5.mouseY < 100:
            if self.nb_pieces < 7:
                self.nb_pieces += 1
                self.nouvelle_partie()
            return None
        elif 0 <= self.p5.mouseX <= self.p5.textWidth("\u2295") and 100 <= self.p5.mouseY <= 120:
            if self.nb_pieces > 1:
                self.nb_pieces -= 1
                self.nouvelle_partie()
            return None


        for i, (x, y, l, h) in enumerate(self.zones):
            if x <= self.p5.mouseX <= x + l and y <= self.p5.mouseY <= y + h:
                if self.etat == ATTENTE and not self.piles[i].est_vide():
                    self.etat = CHOIX
                    self.zone_selectionnee = i
                    self.piece_selectionnee = self.piles[i].depile()
                elif self.etat == CHOIX:
                    if i == self.zone_selectionnee:
                        # On remet la pièce en place
                        self.reposer()
                    elif self.piles[i].est_vide() or self.piles[i].dessus() > self.piece_selectionnee:
                        # On a déplacé la pièce
                        self.piles[i].empile(self.piece_selectionnee)
                        self.historique.append((self.zone_selectionnee, i));
                        self.zone_selectionnee = AUCUNE
                        self.etat = ATTENTE
                        self.piece_selectionnee = 0
                        self.redo = []
                break
        else:
            if self.etat == CHOIX:
                self.reposer()

Animation().run("anim_jeu")





