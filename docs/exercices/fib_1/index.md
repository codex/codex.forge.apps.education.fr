---
license: "by-nc-sa"
author:
    - Mireille Coilhac
    - Franck Chambon
hide:
    - navigation
    - toc
title: Suite de Fibonacci (1)
tags:
    - récursivité
    - ep1
difficulty: 290
---


Les premiers termes de la suite de Fibonacci sont :

$$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, \cdots$$

- Les deux premiers termes sont $0$ et $1$.
- À partir du troisième, un terme est la somme des deux précédents.

Écrire une fonction telle que `fibonacci(n)` renvoie le terme d'indice $n$ de la suite de Fibonacci.

On demande ici de programmer une fonction **très** élémentaire, pour des valeurs de $n$ inférieures à $25$.

???+ example "Exemples :"

    ```pycon title=""
    >>> fibonacci(0)
    0
    >>> fibonacci(3)
    2
    >>> fibonacci(7)
    13
    >>> fibonacci(8)
    21
    >>> fibonacci(9)
    34
    >>> fibonacci(4)
    3
    ```

    On a bien `#!py fibonacci(9)` égal à `#!py fibonacci(7) + fibonacci(8)`

{{ IDE('exo') }}
