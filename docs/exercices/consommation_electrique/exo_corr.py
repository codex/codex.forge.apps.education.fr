def puissance_appareil(tension, intensite):
    return tension * intensite

def energie_consommee(puissance, temps):
    return puissance * temps

def cout_utilisation(energie, prix_base):
    return round(energie * prix_base, 2)

def cout_consommation(intensite, temps):
    tension = 230
    prix = 0.1582

    puissance = puissance_appareil(tension, intensite)

    energie_en_Wh = energie_consommee(puissance, temps)
    energie_en_kWh = energie_en_Wh / 1000
    
    cout = cout_utilisation(energie_en_kWh, prix)

    return cout
