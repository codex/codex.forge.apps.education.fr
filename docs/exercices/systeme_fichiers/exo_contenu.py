# --- PYODIDE:env --- #
def est_fichier(elt):
    return elt[1] is None


def est_dossier(elt):
    return type(elt[1]) is list


# --- PYODIDE:code --- #
def contenu(dossier, chemin):
    enfants = dossier[...]
    for ... in chemin:
        i = ...
        while enfants[i][...] != ...:
            i = ...
        enfants = enfants[...][...]
    return ...


# --- PYODIDE:corr --- #
def contenu(dossier, chemin):
    enfants = dossier[1]
    for etape in chemin:
        i = 0
        while enfants[i][0] != etape:
            i += 1
        enfants = enfants[i][1]
    return enfants


# --- PYODIDE:tests --- #
dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
assert contenu(dossier_nsi, []) == [("tp1.py", None), ("tp1.pdf", None)]
dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
assert contenu(dossier_doc, ["nsi"]) == dossier_nsi[1]
# --- PYODIDE:secrets --- #

# Dossier racine vide
dossier = ("c:", [])
chemin = []
attendu = []
assert contenu(dossier, chemin) == attendu, f"Erreur avec {dossier} et {chemin}"
# Dossier racine en profondeur
dossier = ("c:", [("d:", [("e:", [])])])
chemin = ["d:"]
attendu = [("e:", [])]
assert contenu(dossier, chemin) == attendu, f"Erreur avec {dossier} et {chemin}"
# Plusieurs dossiers vides
dossier = ("c:", [("d:", [("e:", []), ("f:", [])]), ("g:", [("h:", [])])])
chemin = ["d:", "f:"]
attendu = []
assert contenu(dossier, chemin) == attendu, f"Erreur avec {dossier} et {chemin}"
# 1 fichier tout au fond
dossier = ("c:", [["d:", [("e:", []), ("f:", []), ("g:", [("h:", [("seul.py", None)])])]]])
chemin = ["d:", "g:"]
attendu = [("h:", [("seul.py", None)])]
assert contenu(dossier, chemin) == attendu, f"Erreur avec {dossier} et {chemin}"
# 1 fichier tout au fond
dossier = ("c:", [["d:", [("e:", []), ("f:", []), ("g:", [("h:", [("seul.py", None)])])]]])
chemin = ["d:", "g:", "h:"]
attendu = [("seul.py", None)]
assert contenu(dossier, chemin) == attendu, f"Erreur avec {dossier} et {chemin}"
