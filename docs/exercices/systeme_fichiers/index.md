---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Système de fichiers
tags:
    - arbre
    - récursivité
difficulty: 250
maj: 10/01/2025
---

On s'intéresse dans cet exercice au système de fichiers sur un ordinateur. On considère dans ce cadre les deux éléments suivants :

* des fichiers, représentés en machine par des tuples au format `#!py (nom_du fichier, None)`. `#!py nom_du_fichier` est une chaîne de caractères ;

* des dossiers, représentés en machine par des tuples au format `#!py (nom_du_dossier, liste_des_enfants)`. `#!py nom_du_dossier` est une chaîne de caractères, `#!py liste_des_enfants` est une liste contenant de nouveaux éléments (fichiers ou dossiers).

L'exemple ci-dessous est constitué d'un dossier nommé `#!py racine` contenant un fichier (`#!py un.py`) et un sous-dossier (nommé `#!py sous_dossier`) contenant lui-même deux fichiers (`#!py deux.pdf` et `#!py trois.jpg`) :

```text title="Système de fichier"
racine
├───un.py
└───sous_dossier
    ├───deux.pdf
    └───trois_jpg
```

Ce dossier est représenté en machine par le tuple :

```python title="Représentation en Python"
("racine", [("un.py", None), ("sous_dossier", [("deux.pdf", None), ("trois.jpg", None)])])
```

On fournit deux fonctions, `#!py est_fichier` et `#!py est_dossier`, indiquant, pour un élément quelconque, s'il s'agit d'un fichier ou d'un dossier :

```pycon
>>> elt = ("un.py", None)
>>> est_fichier(elt)
True
>>> est_dossier(elt)
False
>>> elt = ("dossier_vide", [])
>>> est_fichier(elt)
False
>>> est_dossier(elt)
True
```

??? tip "Code des deux fonctions"

    ```python
    def est_fichier(elt):
        return elt[1] is None

    def est_dossier(elt):
        return type(elt[1]) is list
    ```

On cherche dans cet exercice, étant donné **un dossier**, à calculer le nombre de dossiers et de fichiers qu'il contient ainsi que sa profondeur et le contenu d'un de ses sous-dossiers descendant.

??? question "1. Nombre de dossiers"

    Écrire la fonction `#!py nb_dossiers` qui prend en paramètre un tuple **représentant un dossier** et renvoie le nombre de dossiers que celui-ci contient au total : lui-même et ses sous-dossiers ainsi que les sous-dossiers de ceux-ci...

    ???+ example "Exemples"

        ```pycon title=""
        >>> dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
        >>> nb_dossiers(dossier_nsi)
        1
        >>> dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
        >>> nb_dossiers(dossier_doc)
        2
        ```

    {{ IDE('exo_dossiers') }}

??? question "2. Nombre de fichiers"

    Écrire la fonction `#!py nb_fichiers` qui prend en paramètre un tuple **représentant un dossier** et renvoie le nombre de fichiers que celui-ci contient au total : ses fichiers mais aussi ceux présents dans ses sous-dossiers et les sous-dossiers de ceux-ci...

    ???+ example "Exemples"

        ```pycon title=""
        >>> dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
        >>> nb_fichiers(dossier_nsi)
        2
        >>> dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
        >>> nb_fichiers(dossier_doc)
        3
        ```

    {{ IDE('exo_fichiers') }}


??? question "3. Profondeur"

    On définit la *profondeur* d'un dossier comme la longueur maximale d'un chemin menant de ce dossier à un dossier ne contenant pas de sous-dossiers.
    
    La profondeur d'un dossier vide ou ne contenant que des fichiers est donc égale à `#!py 0`.

    Écrire la fonction `#!py profondeur` qui prend en paramètre un tuple **représentant un dossier** et renvoie la profondeur de celui-ci.

    ???+ example "Exemples"

        ```pycon title=""
        >>> dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
        >>> profondeur(dossier_nsi)
        0
        >>> dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
        >>> profondeur(dossier_doc)
        1
        ```

    {{ IDE('exo_profondeur') }}

??? question "4. Contenu"

    On appelle *contenu* d'un dossier l'ensemble des sous-dossiers et des fichiers qu'il contient.
    
    De même on appelle *chemin* entre deux dossiers, le nom des sous-dossiers traversés afin d'accéder du premier au dernier. Le nom du dossier de départ ne fait pas partie du chemin, par contre celui du dossier d'arrivée en fait partie.
    
    Écrire la fonction `#!py contenu` qui prend en paramètre un tuple **représentant un dossier** et un chemin menant de ce dossier à un de ses descendants. Cette fonction le contenu du sous-dossier visé par le chemin.
    
    On garantit que le chemin passé en paramètre mène à un sous-dossier existant.

    ???+ example "Exemples"

        ```pycon title=""
        >>> dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
        >>> contenu(dossier_nsi, [])
        [("tp1.py", None), ("tp1.pdf", None)]
        >>> dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
        >>> contenu(dossier_doc, ["nsi"])
        [("tp1.py", None), ("tp1.pdf", None)]
        ```

    {{ IDE('exo_contenu') }}
