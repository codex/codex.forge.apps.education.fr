# --- PYODIDE:env --- #
def est_fichier(elt):
    return elt[1] is None


def est_dossier(elt):
    return type(elt[1]) is list


# --- PYODIDE:code --- #
def nb_fichiers(dossier):
    ...


# --- PYODIDE:corr --- #
def nb_fichiers(dossier):
    t = 0
    for elt in dossier[1]:
        if est_fichier(elt):
            t += 1
        else:
            t += nb_fichiers(elt)
    return t


# --- PYODIDE:tests --- #
dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
assert nb_fichiers(dossier_nsi) == 2
dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
assert nb_fichiers(dossier_doc) == 3
# --- PYODIDE:secrets --- #

# Dossier racine vide
dossier = ("c:", [])
attendu = 0
assert nb_fichiers(dossier) == attendu, f"Erreur avec {dossier}"
# Dossier racine en profondeur
dossier = ("c:", [("d:", [("e:", [])])])
attendu = 0
assert nb_fichiers(dossier) == attendu, f"Erreur avec {dossier}"
# Plusieurs dossiers vides
dossier = ("c:", [("d:", [("e:", []), ("f:", [])]), ("g:", [("h:", [])])])
attendu = 0
assert nb_fichiers(dossier) == attendu, f"Erreur avec {dossier}"
# 1 fichier tout au fond
dossier = ("c:", [["d:", [("e:", []), ("f:", []), ("g:", [("h:", [("seul.py", None)])])]]])
attendu = 1
assert nb_fichiers(dossier) == attendu, f"Erreur avec {dossier}"
# 2 fichiers
dossier = (
    "c:",
    [("d:", [("e:", []), ("un.py", None), ("f:", [])]), ("g:", [("h:", [("deux.js", None)])])],
)
attendu = 2
assert nb_fichiers(dossier) == attendu, f"Erreur avec {dossier}"
