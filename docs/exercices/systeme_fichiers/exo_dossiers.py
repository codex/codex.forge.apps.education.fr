# --- PYODIDE:env --- #
def est_fichier(elt):
    return elt[1] is None


def est_dossier(elt):
    return type(elt[1]) is list


# --- PYODIDE:code --- #
def nb_dossiers(dossier): 
    ...


# --- PYODIDE:corr --- #
def nb_dossiers(dossier):
    t = 1
    for elt in dossier[1]:
        if est_dossier(elt):
            t += nb_dossiers(elt)
    return t


# --- PYODIDE:tests --- #
dossier_nsi = ("nsi", [("tp1.py", None), ("tp1.pdf", None)])
assert nb_dossiers(dossier_nsi) == 1
dossier_doc = ("documents", [("photo.jpg", None), dossier_nsi])
assert nb_dossiers(dossier_doc) == 2
# --- PYODIDE:secrets --- #

# Dossier racine vide
dossier = ("c:", [])
attendu = 1
assert nb_dossiers(dossier) == attendu, f"Erreur avec {dossier}"
# Dossier racine en profondeur
dossier = ("c:", [("d:", [("e:", [])])])
attendu = 3
assert nb_dossiers(dossier) == attendu, f"Erreur avec {dossier}"
# Plusieurs dossiers vides
dossier = ("c:", [("d:", [("e:", []), ("f:", [])]), ("g:", [("h:", [])])])
attendu = 6
assert nb_dossiers(dossier) == attendu, f"Erreur avec {dossier}"
# 1 fichier tout au fond
dossier = ("c:", [["d:", [("e:", []), ("f:", []), ("g:", [("h:", [("seul.py", None)])])]]])
attendu = 6
assert nb_dossiers(dossier) == attendu, f"Erreur avec {dossier}"
# 2 fichiers
dossier = (
    "c:",
    [("d:", [("e:", []), ("un.py", None), ("f:", [])]), ("g:", [("h:", [("deux.js", None)])])],
)
attendu = 6
assert nb_dossiers(dossier) == attendu, f"Erreur avec {dossier}"
