# --------- PYODIDE:code --------- #

SYMBOLES = "+-*/()"


def analyse(expression):
    elements = ...
    temp = ""
    for caractere in expression:
        if ... in SYMBOLES:
            if ... > 0:
                elements.append(...)
                temp = ...
            elements.append(...)
        elif ... != " ":
            temp = ...
    if ...:
        elements.append(...)
    return ...

# --------- PYODIDE:corr --------- #

SYMBOLES = "+-*/()"


def analyse(expression):
    elements = []
    temp = ""
    for caractere in expression:
        if caractere in SYMBOLES:
            if len(temp) > 0:
                elements.append(temp)
                temp = ""
            elements.append(caractere)
        elif caractere != " ":
            temp += caractere
    if len(temp) > 0:
        elements.append(temp)
    return elements

# --------- PYODIDE:tests --------- #

assert analyse("35.896 ") == ["35.896"]
assert analyse("3*5   +8") == ["3", "*", "5", "+", "8"]
assert analyse("3.9  * (5+8.6)") == ["3.9", "*", "(", "5", "+", "8.6", ")"]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert analyse("") == []
assert analyse("1.986") == ["1.986"]
assert analyse("((3))") == list("((3))")
from random import choice, random, randrange

for num_test in range(20):
    jetons = []
    for _ in range(randrange(10, 100)):
        jetons.extend(
            [
                "(",
                str(randrange(0, 2000)),
                choice("+-*/"),
                " " * randrange(1, 3),
                str(100 * random()),
                ")",
                choice("+-*/"),
            ]
        )
    assert analyse("".join(jetons)) == [j for j in jetons if j[0] != " "]