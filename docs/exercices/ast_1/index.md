---
author: Nicolas Revéret
difficulty: 290
hide:
    - navigation
    - toc
title: Éléments d'une expression arithmétique
tags:
    - string
maj: 23/01/2025
---

Dans un terminal Python il est possible de saisir des expressions arithmétiques, des « calculs », afin de les exécuter.

```pycon
>>> 2 +3* 4.5
15.5
```

Cette opération a l'air anodine, mais elle n'est pas si immédiate à effectuer pour Python.

L'une des étapes indispensables est d'analyser l'expression `#!py 2 +3* 4.5` afin d'en extraire les différents **jetons** (les anglo-saxons parlent de *tokens* comme dans [cette page](https://en.wikipedia.org/wiki/Parsing)) : `#!py 2`, `+`, `#!py 3`, `*` et `#!py 4.5`. Le but de cet exercice est d'effectuer cette analyse.

!!! note "Remarque"

    On ne va pas procéder exactement comme Python mais l'idée générale reste la même.

On se donne donc des expressions arithmétiques, chaines de caractères Python, composées de :

* nombres entiers ou décimaux (le séparateur décimal est le point `#!py '.'`) ;
* de symboles d'opérations : `#!py '+'`, `#!py '-'`, `#!py '*'` et `#!py '/'`;
* de parenthèses `#!py '('` et `#!py ')'` ;
* d'espaces `#!py ' '` placées entre les jetons, mais pas toujours (voir remarque ci-dessous).

!!! note "Remarque"

    On considère que les expressions arithmétiques proposées sont bien formées et correspondent donc à des calculs valides.

    Par contre, les différents jetons (nombres et symboles) peuvent être séparés **par zéro, un ou plusieurs espaces**.

On demande d'écrire la fonction `analyse` qui prend en argument une telle expression et renvoie la liste des jetons, chacun étant une chaine de caractères. Les espaces seront ignorées.

???+ example "Exemples"

    ```pycon title=""
    >>> analyse("35.896 ")
    ["35.896"]
    >>> analyse("3*   5+8")
    ["3", "*", "5", "+", "8"]
    >>> analyse("3.9  * (5+8.6)")
    ["3.9", "*", "(", "5", "+", "8.6", ")"]
    ```

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à trous"
    {{ IDE('exo_trous') }}

??? tip "Conseil (1)"

    On pourra utiliser une variable temporaire afin de stocker les différents caractères composant un nombre.

??? tip "Conseil (2)"

    Avant d'ajouter un symbole (opérateur ou parenthèse), on vérifiera que la variable temporaire est non vide. Si c'est le cas, on pourra ajouter un nombre **et** le symbole à la liste des jetons.

??? tip "Conseil (3)"

    Un nombre n'est ni un symbole (opérateur ou parenthèse), ni une espace.
