L'algorithme est classique et ne présente pas de difficultés majeures. On le retrouve de façon très proche dans le calcul des termes de la suite audio-active de Conway et dans la compression *Run-Length Encoding*.

Enfin, les [expressions régulières](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re) permettent de résoudre rapidement ce problème. Une expression régulière est une chaine de caractères décrivant d'autres chaines de caractères. Par exemple `chat(on)?s?` désigne les mots `chat`, `chaton`, `chats` et `chatons`.

On utilise alors l'expression régulière `\d+(?:\.\d+)?|[+\-*\/()]`. Il est possible de tester cette expression régulière sur le site de [regex101](https://regex101.com/r/9yHF3v/1).

Le code Python devient alors :

```python
import re
def analyse(expression):
    return re.findall("\d+(?:\.\d+)?|[+\-*\/()]", expression)
```
