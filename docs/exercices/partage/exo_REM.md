On pourrait aussi utiliser les listes en compréhension :

```python
def partage(valeurs, n):
    taille = len(valeurs)
    
    partie_gauche = [valeurs[i] for i in range(n)]
    partie_droite = [valeurs[i] for i in range(n, taille)]

    return (partie_gauche, partie_droite)
```

Une version encore plus concise utilisant des tranches est :

```python
def partage(valeurs, n):
    return valeurs[:n], valeurs[n:]
```

En effet :

* `#!py valeurs[:n]` crée une nouvelle liste contenant tous les éléments d'indices entre `#!py 0` et `#!py n - 1` (inclus) de `#!py valeurs`;
* `#!py valeurs[n:]` crée une nouvelle liste contenant tous les éléments d'indices entre `#!py n` et `#!py len(valeurs) - 1` (inclus) de `#!py valeurs`.

**Attention toutefois, les solutions concises sont parfois plus difficiles à lire.**
