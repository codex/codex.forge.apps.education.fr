

# --------- PYODIDE:code --------- #

def partage(valeurs, n):
    taille = len(valeurs)

    partie_gauche = ...
    partie_droite = ...

    ...  # lignes supplémentaires, pas indispensables

    return (..., ...)

# --------- PYODIDE:corr --------- #

def partage(valeurs, n):
    taille = len(valeurs)

    partie_gauche = []
    for i in range(n):
        partie_gauche.append(valeurs[i])

    partie_droite = []
    for i in range(n, taille):
        partie_droite.append(valeurs[i])

    return (partie_gauche, partie_droite)

# --------- PYODIDE:tests --------- #

assert partage(["pim", "pam", "poum"], 2) == (["pim", "pam"], ["poum"])
assert partage([7, 12, 5, 6, 8], 0) == ([], [7, 12, 5, 6, 8])
assert partage([7, 12, 5, 6, 8], 5) == ([7, 12, 5, 6, 8], [])

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert partage(list(range(-10, 10)), 0) == ([], list(range(-10, 10)))
assert partage(list(range(-10, 10)), 3) == (list(range(-10, -7)), list(range(-7, 10)))
assert partage(list(range(-10, 10)), 5) == (list(range(-10, -5)), list(range(-5, 10)))
assert partage(list(range(-10, 10)), 20) == (list(range(-10, 10)), [])