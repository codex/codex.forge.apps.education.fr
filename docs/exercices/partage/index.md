---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Partage d'une liste
tags:
    - liste/tableau
maj: 01/03/2024
---

On donne une liste `valeurs` et un entier `n`.

On garantit que `n` est un entier compris entre 0 et la longueur de `valeurs` (inclus l'un et l'autre).

Compléter le code de la fonction `partage` qui :

* prend `valeurs` et `n` en arguments,

* renvoie le couple formé :
    * de la liste comprenant les `n` premiers éléments de `valeurs` (situés à gauche),
    * de la liste comprenant les éléments restants (situés à droite).

???+ example "Exemples"

    ```pycon title=""
    >>> partage(['pim', 'pam', 'poum'], 2)
    (['pim', 'pam'], ['poum'])
    >>> partage([7, 12, 5, 6, 8], 0)
    ([], [7, 12, 5, 6, 8])
    >>> partage([7, 12, 5, 6, 8], 5)
    ([7, 12, 5, 6, 8], [])
    ```

{{ IDE('exo') }}
