---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Coefficients directeurs de cordes
difficulty: 250
tags:
    - maths
    - liste/tableau
maj: 11/11/2024
---

![Trois cordes](images/cordes.svg){width=30% .autolight align=right}

En géométrie on appelle « corde » un segment reliant deux points distincts d'une courbe.


On considère dans cet exercice la courbe $C_f$ représentant une fonction $f$ définie[^1] sur $\mathbb{R}$. Soit $a$ un nombre réel et $A$ le point de $C_f$ d'abscisse $a$.

On souhaite calculer les coefficients directeurs de plusieurs cordes, toutes issues du point $A$. Pour une corde passant par les points de coordonnées $\left(~a~;~f(a)~\right)$ et $\left(~a+h~;~f(a+h)~\right)$, ce coefficient directeur vaut :

$$\tau_a(h)=\frac{f(a+h)-f(a)}{h}$$

Afin de pouvoir observer l'évolution de ces coefficients directeurs, on les calculera pour les valeurs de $h$ successivement égales à $\dfrac{1}{2^0}$, $\dfrac{1}{2^1}$, ..., $\dfrac{1}{2^{e_{max}}}$ (inclus).

??? question "Puissances de $2$ ?"
    
    Python représente les nombres en binaire. Sans rentrer dans les détails, le fait d'utiliser des valeurs de $h$ pouvant s'écrire sous la forme $2^k$ permet, bien souvent, d'éviter des erreurs d'arrondis.

Sur la figure ci-contre, on a pris :

* $f:x\mapsto x^3$ ;
* $a=0$ ; le point $A$ a donc pour coordonnées $(0~;~f(0))$ ;
* $e_{max}={2}$ et on a donc calculé les $3$ coefficients directeurs pour $h \in \left\{\dfrac{1}{2^0}~;~\dfrac{1}{2^{1}}~;~\dfrac{1}{2^2}\right\}$.


Les coefficients directeurs calculés sont :

<center>

| $a$  | $h$    | $a+h$  | $\tau_h(0)$ |
| :--- | :----- | :----- | :---------- |
| $0$  | $1$    | $1$    | $1$         |
| $0$  | $0,5$  | $0,5$  | $0,25$      |
| $0$  | $0,25$ | $0,25$ | $0,0625$    |

</center>

On demande d'écrire la fonction `coeffs_cordes` qui prend en paramètres un nombre `#!py a` et un entier `#!py e_max`.

Cette fonction renvoie la liste des coefficients directeurs des cordes issues du point $A~\left(~a~;~f(a)~\right)$ et passant par les points de coordonnées $\left(~a+h~;~f(a+h)~\right)$ pour les valeurs de $h$ comprises entre $\dfrac{1}{2^0}$ et $\dfrac{1}{2^{e_{max}}}$ (inclus l'un et l'autre).


???+ example "Exemples"

    ```pycon title=""
    >>> # cordes issues de (0, f(0)) pour h = 1 ; h = 0,5 et h = 0,25 = 1/2^2
    >>> coeffs_cordes(0, 2)
    [1.0, 0.25, 0.0625]
    ```
    ```pycon title=""
    >>> # cordes issues de (0, f(0)) pour h = 1 ; h = 0,5 ; h = 0,25 et h = 0,125 = 1/2^3
    >>> coeffs_cordes(0, 3)
    [1.0, 0.25, 0.0625, 0.015625]
    ```
    ```pycon title=""
    >>> # cordes issues de (2, f(2)) pour h = 1 ; ... et h = 0,015625 = 1/2^6
    >>> coeffs_cordes(2, 6)
    [19.0, 15.25, 13.5625, 12.765625, 12.37890625, 12.1884765625, 12.093994140625]
    ```
On propose deux méthodes produisant l'une et l'autre le résultat attendu :

??? question "En construisant une liste au fur et à mesure"

    Dans cette version :
    
    * on crée dans un premier temps une liste vide qui contiendra les différents coefficients ;
    * on parcourt l'ensemble des exposants `#!py e` compris entre `#!py 0` et `#!py e_max` (inclus) ;
    * pour chacun, on calcule le coefficient directeur de la corde ;
    * on ajoute cette valeur à la liste des coefficients.

    On rappelle les instructions suivantes :
    
    ```pycon
    >>> coeffs = []       # création d'une liste vide
    >>> coeffs.append(1)  # ajout de la valeur 1 (à la fin)
    >>> coeffs.append(2)  # ajout de la valeur 2 (à la fin)
    >>> coeffs
    [1, 2]
    ```

    === "Version vide"
        {{ IDE('pythons/exo_construction_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_construction_trous') }}

??? question "En utilisant une liste en compréhension"

    Dans cette version, on utilise une liste en compréhension qui permet de générer le résultat attendu.
    
    La syntaxe Python des listes en compréhension est `#!py [action for variable in ensemble_a_parcourir]`
    
    On fournit ci-dessous quelques exemples :
    
    ```pycon
    >>> [x for x in range(5)]  # les nombres entre 0 (inclus) et 5 (exclu)
    [0, 1, 2, 3, 4]
    >>> [2*x for x in range(1, 6)]  # les doubles des nombres entre 1 (inclus) et 6 (exclu)
    [2, 4, 6, 8, 10]
    >>> [x**2 for x in (2, 3, 5, 7)]  # les carrés des nombres 2, 3, 5 et 7 (dans cet ordre)
    [4, 9, 25, 49]
    ```

    === "Version vide"
        {{ IDE('pythons/exo_comprehension_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_comprehension_trous') }}

[^1]: définir la fonction sur $\mathbb{R}$ assure que $f$ est définie pour toutes les valeurs de $a$ et $a+h$.