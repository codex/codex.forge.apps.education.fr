# --------- PYODIDE:env --------- #
def f(x):
    return x**3


def coeffs_cordes(a, e_max):
    pass


# --------- PYODIDE:code --------- #
def f(x):
    return x**3


def coeffs_cordes(a, e_max): 
    ...


# --------- PYODIDE:corr --------- #
def f(x):
    return x**3


def coeffs_cordes(a, e_max):
    coeffs = []
    f_de_a = f(a)
    for e in range(e_max + 1):
        h = 1 / 2**e
        coeff_dir = (f(a + h) - f_de_a) / h
        coeffs.append(coeff_dir)
    return coeffs


# --------- PYODIDE:tests --------- #
assert coeffs_cordes(0, 2) == [1.0, 0.25, 0.0625]
assert coeffs_cordes(0, 4) == [1.0, 0.25, 0.0625, 0.015625, 0.00390625]
assert coeffs_cordes(2, 6) == [
    19.0,
    15.25,
    13.5625,
    12.765625,
    12.37890625,
    12.1884765625,
    12.093994140625,
]


# --------- PYODIDE:secrets --------- #
def _coeffs_secantes_(a, e_max):
    coeffs = []
    for e in range(e_max + 1):
        h = 1 / 2**e
        coeff_dir = (f(a + h) - f(a)) / h
        coeffs.append(coeff_dir)
    return coeffs


for a in range(-5, 5):
    for e_max in range(6):
        attendu = _coeffs_secantes_(a, e_max)
        assert coeffs_cordes(a, e_max) == attendu, f"Erreur avec {a = } et {e_max = }"
