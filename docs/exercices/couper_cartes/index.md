---
author: Mireille Coilhac
hide:
    - navigation
    - toc
title: Couper un jeu de cartes
tags:
    - liste/tableau
difficulty: 220
maj: 01/02/2024
---

Couper un jeu de cartes consiste à prendre une partie des cartes depuis le dessus du paquet pour les passer sous le paquet, sans en modifier l'ordre. Ceci permet d'éviter une triche lors de la distribution des cartes. De façon analogue, nous allons couper un tableau.

Vous devez écrire la fonction `coupe` qui prend en paramètres `paquet` qui est un tableau de taille supérieure ou égale à 2 et `nombre` qui est un entier représentant le nombre de cartes à mettre en dessous.

La fonction `coupe` renvoie un nouveau tableau contenant les mêmes éléments que `paquet`, dans le même ordre, mis à part les `nombre` derniers éléments qui ont été transférés de la fin au début sans changer leur ordre relatif.

On garantit que `paquet` est de longueur `n` supérieure ou égale à 2, et que `nombre` est un entier pouvant prendre toutes les valeurs de 1 à `n`- 1

???+ example "Exemples"

    ```pycon title=""
    >>> coupe(['As ♠', '5 ♥', '2 ♠', '5 ♣', 'Dame ♠', '8 ♦', 'As ♣', 'Valet ♣'], 3)
	['8 ♦', 'As ♣', 'Valet ♣', 'As ♠', '5 ♥', '2 ♠', '5 ♣', 'Dame ♠']
    >>> coupe([5, 1, 8, 3, 7], 1)
	[7, 5, 1, 8, 3]
    >>> coupe([5, 1, 8, 3, 7], 2)
	[3, 7, 5, 1, 8]
	>>> coupe([5, 1, 8, 3, 7], 4)
	[1, 8, 3, 7, 5]
	>>> coupe([5, 3], 1)
	[3, 5]
	```

???+ danger "Tranches interdites"

	L'utilisation des tranches (*slices* en anglais) est interdite.


{{IDE('exo')}}


