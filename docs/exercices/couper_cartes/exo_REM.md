Autre correction possible, en utilisant des indices négatifs pour le tableau : 

```python
def coupe(paquet, nombre):
    n = len(tableau)
    return [paquet[i-nombre] for i in range(n)]
```
