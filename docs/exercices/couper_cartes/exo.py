

# --------- PYODIDE:code --------- #

def coupe(paquet, nombre):
    ...

# --------- PYODIDE:corr --------- #

def coupe(paquet, nombre):
    n = len(paquet)
    nouveau = [None] * n
    for i in range(nombre):
        nouveau[i] = paquet[n - nombre + i]
    for i in range(nombre, n):
        nouveau[i] = paquet[i - nombre]
    return nouveau

# --------- PYODIDE:tests --------- #

paquet_1 = ['As ♠', '5 ♥', '2 ♠', '5 ♣', 'Dame ♠', '8 ♦', 'As ♣', 'Valet ♣']
paquet_2 = ['8 ♦', 'As ♣', 'Valet ♣', 'As ♠', '5 ♥', '2 ♠', '5 ♣', 'Dame ♠']
assert coupe(paquet_1, 3) == paquet_2
assert coupe([5, 1, 8, 3, 7], 1) == [7, 5, 1, 8, 3]
assert coupe([5, 1, 8, 3, 7], 2) == [3, 7, 5, 1, 8]
assert coupe([5, 1, 8, 3, 7], 4) == [1, 8, 3, 7, 5]
assert coupe([5, 3], 1) == [3, 5]

# --------- PYODIDE:secrets --------- #


# Autres tests
from random import randint
nombre = randint(0, 99)
nombres = [randint(1, 2**10) for _ in range(100)]
attendu = nombres[len(nombres)-nombre:] + nombres[:len(nombres)-nombre]
assert coupe(nombres, nombre) == attendu