---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Compression RLE
difficulty: 160
tags:
    - à trous
    - string
---

Certains formats de fichiers informatiques utilisent la compression *Run-Length Encoding*.

Le principe est de remplacer les suites de valeurs identiques par des couples `(valeur, répétition)`.

On propose ci-dessous un exemple d'application à un fichier texte :

???+ example "Exemple"

    ```pycon title=""
    >>> compression_RLE("aabbbbcaa")
    "2a4b1c2a"
    ```

Le code `#!py "2a4b1c2a"` signifie que la chaîne `#!py "aabbbbcaa"` comporte dans cet ordre :

* deux `#!py "a"` ;
* quatre `#!py "b"` ;
* un `#!py "c"` ;
* deux `#!py "a"`.
 
On garantit que le texte proposé est non-vide.

??? note "Remarques"

    Cette méthode de compression est particulièrement efficace dans le cas d'un texte comportant de nombreuses répétitions.
    
    Par exemple, le texte `#!py "aaaa...a"` comportant 10 000 fois le caractère `#!py "a"` sera compressé en `#!py "10000a"`. On passe ainsi de 10 000 caractères à 6 !
    
    À l'inverse, si le texte comporte beaucoup de caractères uniques, la compression n'est pas avantageuse : `#!py "abcd"` devient `#!py "1a1b1c1d"` et la taille a doublé !

Afin de simplifier la démarche, on ne considérera que des chaînes de caractères ne comportant aucun chiffre. Ainsi, toutes les chaînes étudiées ne comporteront que des lettres ou de la ponctuation.

Afin de calculer la compression d'une chaîne de caractères par cette méthode, on doit la parcourir en comptant le nombre de répétitions de chaque caractère lu. Lorsque l'on change de caractère, on complète la chaîne compressée.

???+ example "Exemples"

    ```pycon title=""
    >>> compression_RLE("aabbbbcaa")
    '2a4b1c2a'
    >>> compression_RLE("aa aa.")
    '2a1 2a1.'
    >>> compression_RLE("a" * 1000)
    '1000a'
    >>> compression_RLE("aA")
    '1a1A'
    ```

Vous devez écrire la fonction `compression(texte)` décrite.

??? tip "Coup de pouce"

    Python ne supporte pas « l'addition » d'un entier et d'un caractère. On pourra utiliser la méthode suivante :

    ```pycon
    >>> str(5) + "a"
    '5a'
    ```

{{ IDE('exo') }}

