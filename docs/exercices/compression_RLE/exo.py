

# --------- PYODIDE:code --------- #

def compression_RLE(texte):
    texte_compresse = ""

    caractere_repete = texte[0]
    nb_repetitions = ...

    for caractere in texte:
        if caractere == ...:
            nb_repetitions = ...
        else:
            texte_compresse = ... + ... + ...
            caractere_repete = ...
            nb_repetitions = ...

    texte_compresse = ... + ...

    return ...

# --------- PYODIDE:corr --------- #

def compression_RLE(texte):
    texte_compresse = ""

    caractere_repete = texte[0]
    nb_repetitions = 0

    for caractere in texte:
        if caractere == caractere_repete:
            nb_repetitions += 1
        else:
            texte_compresse += str(nb_repetitions) + caractere_repete
            caractere_repete = caractere
            nb_repetitions = 1

    texte_compresse += str(nb_repetitions) + caractere_repete

    return texte_compresse

# --------- PYODIDE:tests --------- #

assert compression_RLE("aabbbbcaa") == "2a4b1c2a"
assert compression_RLE("aa aa.") == "2a1 2a1."
assert compression_RLE("a" * 1000) == "1000a"
assert compression_RLE("aA") == "1a1A"

# --------- PYODIDE:secrets --------- #


# # Tests supplémentaires
from string import ascii_letters as caracteres
from random import sample, randrange

caracteres += "?.,;:!"
for _ in range(20):
    taille = randrange(5, 20)
    caracteres_choisis = sample(caracteres, taille)
    repetitions = [randrange(1, 10) for _ in range(taille)]
    texte = "".join([c * n for n, c in zip(repetitions, caracteres_choisis)])
    attendu = "".join([str(n) + c for n, c in zip(repetitions, caracteres_choisis)])
    assert compression_RLE(texte) == attendu, f"Erreur avec {texte}"