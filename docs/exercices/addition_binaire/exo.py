

# --------- PYODIDE:code --------- #

def additionneur(a, b, retenue):
    return (a & b) | (a & retenue) | (b & retenue), a ^ b ^ retenue


def addition_binaire(a, b):
    retenue = ...
    resultat = [None] * ...
    for i in range(..., ..., ...):
        bit_a = a[i]
        bit_b = b[i]
        ..., ... = additionneur(..., ..., ...)
        resultat[i] = ...
    return resultat


a = [0, 0, 0, 0, 1, 0, 1, 0]
b = [0, 0, 0, 0, 0, 1, 0, 1]
assert addition_binaire(a, b) == [0, 0, 0, 0, 1, 1, 1, 1]
a = [0, 1, 1, 1, 1, 1, 1, 1]
b = [0, 0, 0, 0, 0, 0, 0, 1]
assert addition_binaire(a, b) == [1, 0, 0, 0, 0, 0, 0, 0]
a = [1, 1, 1, 1, 1, 1, 1, 1]
b = [0, 0, 0, 0, 0, 0, 0, 1]
assert addition_binaire(a, b) == [0, 0, 0, 0, 0, 0, 0, 0]

# --------- PYODIDE:corr --------- #

def addition_binaire(a, b):
    retenue = 0
    resultat = [None] * 8
    for i in range(7, -1, -1):
        bit_a = a[i]
        bit_b = b[i]
        retenue, bit = additionneur(bit_a, bit_b, retenue)
        resultat[i] = bit
    return resultat

# --------- PYODIDE:secrets --------- #

# Tests
a = [0, 0, 0, 0, 1, 0, 1, 0]
b = [0, 0, 0, 0, 0, 1, 0, 1]
assert addition_binaire(a, b) == [0, 0, 0, 0, 1, 1, 1, 1]
a = [0, 1, 1, 1, 1, 1, 1, 1]
b = [0, 0, 0, 0, 0, 0, 0, 1]
assert addition_binaire(a, b) == [1, 0, 0, 0, 0, 0, 0, 0]
a = [1, 1, 1, 1, 1, 1, 1, 1]
b = [0, 0, 0, 0, 0, 0, 0, 1]
assert addition_binaire(a, b) == [0, 0, 0, 0, 0, 0, 0, 0]

# Tests supplémentaires
from random import randrange

for _ in range(50):
    entier_a = randrange(0, 256)
    entier_b = randrange(0, 256)
    a_plus_b = entier_a + entier_b
    a = list(map(int, bin(entier_a)[2:].zfill(8)))
    b = list(map(int, bin(entier_b)[2:].zfill(8)))
    attendu = list(map(int, bin(a_plus_b)[2:].zfill(8)[-8:]))
    assert addition_binaire(a, b) == attendu, f"Erreur avec {a = } e {b = }"
