---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Indice première occurrence
difficulty: 115
tags:
    - liste/tableau
    - ep1
---



Écrire une fonction `indice` qui prend en paramètres `element` un nombre entier, `tableau` un tableau de nombres entiers, et qui renvoie l'indice de la **première** occurrence de `element` dans `tableau`.

La fonction devra renvoyer `None` si `element` est absent de `tableau`.

??? warning "Contraintes"

    On n'utilisera pas ni la fonction `#!py index`, ni la fonction `#!py find`.

???+ example "Exemples"

    ```pycon title=""
    >>> indice(1, [10, 12, 1, 56])
    2
    >>> indice(1, [1, 50, 1])
    0
    >>> indice(15, [8, 9, 10, 15])
    3
    >>> indice(1, [2, 3, 4]) is None
    True
    ```

{{ IDE('exo', SANS=".index, .find") }}
