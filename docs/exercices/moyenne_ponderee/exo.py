

# --------- PYODIDE:code --------- #

def moyenne(nom, resultats):
    if nom ... resultats:
        notes_et_coeff = ...
        somme_points = 0
        somme_coefficients = 0
        for matiere in notes_et_coeff:
            note, coefficient = notes_et_coeff[...]
            somme_points = somme_points + ... * ...
            somme_coefficients = somme_coefficients + ...
        return round(... / ..., 1)
    else:
        return ...


def sont_proches(x, y):
    """
    Vérifie que deux nombres sont proches,
    c'est-à-dire égaux au millionième près
    """
    return abs(x - y) < 10**-6

# --------- PYODIDE:corr --------- #

def sont_proches(a, b):
    return abs(a - b) < 0.000001

def moyenne(nom, resultats):
    if nom in resultats:
        notes_et_coeff = resultats[nom]
        somme_points = 0
        somme_coefficients = 0
        for matiere in notes_et_coeff:
            note, coefficient = notes_et_coeff[matiere]
            somme_points = somme_points + coefficient*note
            somme_coefficients = somme_coefficients + coefficient
        return round(somme_points / somme_coefficients, 1)
    else:
        return -1

# --------- PYODIDE:tests --------- #

resultats_trimestre = {'Dupont':{'DS1': [15.5, 4],
                                 'DM1': [14.5, 1],
                                 'DS2': [13, 4],
                                 'PROJET1': [16, 3],
                                 'DS3': [14, 4]},
                       'Durand':{'DS1': [6 , 4],
                                 'DM1': [14.5, 1],
                                 'DS2': [8, 4],
                                 'PROJET1': [9, 3],
                                 'IE1': [7, 2],
                                 'DS3': [8, 4],
                                 'DS4': [15, 4]}}

assert sont_proches(moyenne('Durand', resultats_trimestre), 9.2)
assert moyenne('Martin', resultats_trimestre) == -1

# --------- PYODIDE:secrets --------- #

# tests

resultats = {'Dupont':{ 'DS1' : [15.5, 4],
                        'DM1' : [14.5, 1],
                        'DS2' : [13, 4],
                        'PROJET1' : [16, 3],
                        'DS3' : [14, 4]},
            'Durand':{  'DS1' : [6 , 4],
                        'DM1' : [14.5, 1],
                        'DS2' : [8, 4],
                        'PROJET1' : [9, 3],
                        'IE1' : [7, 2],
                        'DS3' : [8, 4],
                        'DS4' :[15, 4]}}

assert sont_proches(moyenne('Durand', resultats), 9.2)
assert moyenne('Martin', resultats) == -1

# Autres tests

assert sont_proches(moyenne('Dupont', resultats), 14.5)

terminale_T04 = {'Ada': {'EP1': [8.9, 2],
  'EP2': [17.7, 2],
  'EP3': [4.4, 2],
  'PROJET1': [1.5, 1],
  'PROJET2': [12.7, 3],
  'BAC_BLANC': [15.2, 4],
  'ECRIT1': [14.8, 3],
  'ECRIT2': [12.5, 3]},
 'Alan': {'EP1': [11.1, 2],
  'EP2': [10.5, 2],
  'EP3': [2.8, 2],
  'PROJET1': [4.5, 1],
  'PROJET2': [4.1, 3],
  'BAC_BLANC': [9.6, 4],
  'ECRIT1': [9.4, 3],
  'ECRIT2': [10.5, 3]},
 'Liz': {'EP1': [13.8, 2],
  'EP2': [16.2, 2],
  'EP3': [1.2, 2],
  'PROJET1': [20.0, 1],
  'PROJET2': [18.8, 3],
  'BAC_BLANC': [2.6, 4],
  'ECRIT1': [5.9, 3],
  'ECRIT2': [3.8, 3]},
 'Margaret': {'EP1': [10.0, 2],
  'EP2': [8.1, 2],
  'EP3': [18.6, 2],
  'PROJET1': [17.0, 1],
  'PROJET2': [4.6, 3],
  'BAC_BLANC': [17.6, 4],
  'ECRIT1': [3.8, 3],
  'ECRIT2': [12.0, 3]},
 'Grace': {'EP1': [19.5, 2],
  'EP2': [19.4, 2],
  'EP3': [13.1, 2],
  'PROJET1': [15.2, 1],
  'PROJET2': [5.9, 3],
  'BAC_BLANC': [18.3, 4],
  'ECRIT1': [8.7, 3],
  'ECRIT2': [1.8, 3]},
 'Dennis': {'EP1': [5.9, 2],
  'EP2': [18.2, 2],
  'EP3': [16.7, 2],
  'PROJET1': [19.7, 1],
  'PROJET2': [19.2, 3],
  'BAC_BLANC': [3.6, 4],
  'ECRIT1': [11.2, 3],
  'ECRIT2': [14.0, 3]},
 'Claude': {'EP1': [10.2, 2],
  'EP2': [18.5, 2],
  'EP3': [9.9, 2],
  'PROJET1': [5.1, 1],
  'PROJET2': [10.1, 3],
  'BAC_BLANC': [8.4, 4],
  'ECRIT1': [16.3, 3],
  'ECRIT2': [10.4, 3]},
 'Donald': {'EP1': [5.8, 2],
  'EP2': [12.2, 2],
  'EP3': [17.8, 2],
  'PROJET1': [15.3, 1],
  'PROJET2': [10.0, 3],
  'BAC_BLANC': [9.5, 4],
  'ECRIT1': [19.4, 3],
  'ECRIT2': [12.3, 3]}}

assert sont_proches(moyenne('Ada', terminale_T04), 12.2)
assert sont_proches(moyenne('Alan', terminale_T04), 8.2)
assert sont_proches(moyenne('Liz', terminale_T04), 8.9)
assert sont_proches(moyenne('Claude', terminale_T04), 11.3)
assert moyenne('Steve', terminale_T04) == -1
