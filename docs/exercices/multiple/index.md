---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Multiple
tags:
    - maths
    - booléen
difficulty: 120
maj: 20/06/2024
---

Soient $a$ et $b$ deux entiers positifs.

On dit que $a$ est un multiple de $b$ s'il existe un autre entier $k$ tel que $a =k \times b$.

On demande d'écrire une fonction `est_multiple` qui :

* prend en paramètres deux entiers strictement positifs `a` et `b` ;

* renvoie `True` si le premier est un multiple du second, `False` dans le cas contraire.

Python propose une solution native détaillée ci-dessous pour répondre à cette question.

??? note "Solution native"

    Il suffit en effet de vérifier que le reste de la division euclidienne de $a$ par $b$ vaut $0$.
    
    L'expression Python renvoyant le reste de la division de `a` par `b` est `#!py a % b`.
    
    Une solution native serait donc :
    
    ```python
    def est_multiple(a, b):
        return a % b == 0
    ```

    Cette solution ne permet toutefois pas de répondre au cas $b = 0$ (il est impossible de diviser par $0$).

On propose de raisonner différemment. Pour ce faire, on gère immédiatement le cas $b = 0$ en observant que le seul multiple de $b = 0$ est $a = 0$. Ce cas particulier étant traité : 

* on crée une variable `multiple` prenant initialement la valeur `#!py 0` ;

* tant que `multiple` est strictement inférieur à `a`, on ajoute `b` à la valeur de `multiple` ;

* en fin de boucle on teste l'égalité de `multiple` et `a` : s'ils sont égaux, `a` est un multiple de `b`. Sinon, `a` n'est pas un multiple de `b`.

??? example "Exemples"

    ```pycon title=""
    >>> est_multiple(0, 0)
    True
    >>> est_multiple(5, 0)
    False
    >>> est_multiple(10, 2)
    True
    >>> est_multiple(10, 10)
    True
    >>> est_multiple(2, 10)
    False
    >>> est_multiple(7, 3)
    False
    ```

{{ remarque('assertion') }}

        
=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}

