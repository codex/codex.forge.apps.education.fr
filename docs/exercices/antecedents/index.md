---
author: Jean Diraison
hide:
    - navigation
    - toc
title: Dictionnaire des antécédents
tags:
    - dictionnaire
difficulty: 210
---

Un dictionnaire associe des valeurs à des clés, comme `#!py {"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}` qui associe `#!py "P"` à la clé `#!py "Paris"`. 

Suivant les cas, une même valeur peut être associée à une ou plusieurs clés. Dans l'exemple précédent, la valeur `#!py "L"` est associée aux clés `#!py "Lyon"` et `#!py "Lille"`, on les appelle les **antécédents** de `#!py "L"`, tandis que `#!py "P"` a la clé `#!py "Paris"` pour seul et unique antécédent.

On peut ainsi construire le dictionnaire des antécédents `{"P": ["Paris"], "L": ["Lyon", "Lille"], "N": ["Nantes"]}`.

Vous devez écrire une fonction `antecedents`, de paramètre `dico`, qui renvoie le dictionnaire associant les valeurs de `dico` à la liste de leurs antécédents dans `dico`.


!!! info "Notes"
    Puisqu'aucun ordre ne vous est imposé dans la construction des listes, une étape supplémentaire de tri est réalisée lors des tests de validation.
    
    Par ailleurs, nous garantissons que le type utilisé autorise bien les valeurs à servir de clé.

???+ example "Exemples"

    ```pycon title=""
    >>> antecedents({'a': 5, 'b': 7})
    {5: ['a'], 7: ['b']}
    >>> antecedents({'a': 5, 'b': 7, 'c': 5})
    {5: ['a', 'c'], 7: ['b']}
    >>> antecedents({"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"})
    {"P": ["Paris"], "L": ["Lyon", "Lille"], "N": ["Nantes"]}
    ```

{{ IDE('exo') }}
