---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Nombre de partitions d'un entier
tags:
    - programmation dynamique
    - mémoïsation
    - récursivité
---

On considère qu'une somme doit comporter au moins un terme et que l'ordre des termes ne compte pas. Ainsi :

- il y a $3$ façons d'écrire $3$ comme une somme :
    - $3 = 3$,
    - $3 = 2 + 1$ ;
    - $3 = 1 + 1 + 1$ ;

- il y a $7$ façons d'écrire $5$ comme une somme :
    - $5 = 5$ ;
    - $5 = 4 + 1$ ;
    - $5 = 3 + 2$ ;
    - $5 = 3 + 1 + 1$ ;
    - $5 = 2 + 2 + 1$ ;
    - $5 = 2 + 1 + 1 + 1$ ;
    - $5 = 1 + 1 + 1 + 1 + 1$.

Écrire une fonction `nb_sommes` qui prend un paramètre entier `#!py 0 < n <= 42` et qui renvoie le nombre de façons d'écrire `n` comme une somme, sans compter l'ordre.

???+ example "Exemples"

    ```pycon title=""
    >>> nb_sommes(3)
    3
    >>> nb_sommes(5)
    7
    ```

??? tip "Indice 1"

    - Toujours commencer par dénombrer à la main les premiers cas.
  
    - Bien ranger les cas ; faire des figures.

??? tip "Indice 2"

    Il est recommandé de construire une fonction **auxiliaire** `nb_k_sommes` qui prend en paramètres deux entiers `n` et `k` et renvoie le nombre de façons d'écrire `n` comme une somme de `k` termes.

    Ainsi, `nb_sommes(n)` sera la somme pour `k` allant de `#!py 1` à `n` de `nb_k_sommes(n, k)`.

    On remarquera que :

    - `#!py n < k` ne donne aucune somme ;
    - `#!py k == 1` donne une unique somme.

??? tip "Indice 3"

    Pour dénombrer `nb_k_sommes(n, k)`, on fera deux cas :

    - Les sommes dont le plus petit terme est `#!py 1`. Combien ont-elles de termes restants et pour quelle somme ? Un calcul récursif est alors possible...
  
    - Les sommes dont tous les termes sont supérieurs à `#!py 1`. On pourra enlever `#!py 1` à chaque terme, ce qui donne une somme égale à ??? en ??? parties. Et réciproquement. Ce qui permet de dénombrer ce cas de manière récursive également.

    Par exemple, parmi les sommes de $10$ termes égales à $50$, il y a :
    
    * des sommes de $9$ termes valant toutes $49$ auxquelles on ajoute $1$ ;
    * des sommes de $10$ termes valant toutes $40$ auxquelles on ajoute $10$ fois $1$.

    On en déduit : `#!py nb_k_sommes(50, 10) = nb_k_sommes(49, 9) + nb_k_sommes(40, 10)`.

??? tip "Indice 4"

    Il faudra utiliser de la mémoïsation pour que le code soit assez rapide.

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
