---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Inversions dans un tableau (1)
tags:
    - boucle
    - tri
difficulty: 310
---

On considère dans cet exercice des tableaux d'entiers.

Soit `tab` un tel tableau. On appelle **inversion** un couple d'indices distincts `i` et `j` tel que :

* `i` est plus **petit** que `j` ;
* `tab[i]` est strictement plus **grand** que `tab[j]`.

Considérons par exemple dans le tableau :

 ```py
 # indices  0  1  2  3
 tab     = [7, 5, 9, 6]
 ```
 
 Ce tableau compte 3 inversions :

* pour les indices `#!py 0` et `#!py 1`, car `tab[0]` (qui vaut `#!py 7`) est strictement supérieur à `tab[1]` (qui vaut `#!py 5`),
* pour les indices `#!py 0` et `#!py 3`, car `tab[0]` (qui vaut `#!py 7`) est strictement supérieur à `tab[3]` (qui vaut `#!py 6`),
* pour les indices `#!py 2` et `#!py 3`, car `tab[2]` (qui vaut `#!py 9`) est strictement supérieur à `tab[3]` (qui vaut `#!py 6`).

!!! note "Remarque"

    Compter les inversions dans un tableau permet de mesurer son « désordre » : si un tableau ne comporte aucune inversion, il est trié dans l'ordre croissant !

On demande d'écrire la fonction `inversions` qui prend en argument un tableau d'entiers et renvoie son nombre d'inversions.

On convient qu'un tableau vide ne compte aucune inversion.

Les tableaux utilisés dans les tests seront de petite taille (100 éléments au maximum).


???+ example "Exemples"

    ```pycon title=""
    >>> inversions([])
    0
    >>> inversions([5, 6, 7, 9])
    0
    >>> inversions([7, 5, 9, 6])
    3
    ```

{{ IDE('exo') }}
