On utilise une double boucle : on compare chaque élément d'indice `i` avec tous les éléments suivants d'indice `j`. Si `tab[j]` est strictement inférieur à `tab[i]` on est face à une inversion : on incrémente alors le compteur.

Facile à concevoir cette méthode a un désavantage : elle effectue beaucoup de comparaisons. Par exemple, pour un tableau de dix éléments, le premier sera comparé aux 9 suivants, le deuxième au 8 suivants, le troisième au 7 suivants *etc*.
Il y aura au total 55 comparaisons.

Le cout de cette méthode est quadratique ce qui est pénalisant si l'on souhaite l'utiliser pour de grands tableaux.
