---
author:
    - Fabrice Nativel
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Texte brut issu d'un document HTML
difficulty: 250
tags:
    - à trous
    - booléen
    - string
---

On dispose d'une chaine de caractères représentant une portion de code `HTML` et on souhaite écrire une fonction `texte_brut` qui supprime toutes les balises `HTML` de cette chaîne et renvoie le texte brut qu'il contient.

On rappelle qu'une balise `HTML` s'écrit sous la forme `<nom_balise>` ou `</nom_balise>`.

Si l'on considère par exemple le code `html = "<p>un <strong>exemple</strong> de chaine</p>"` alors l'appel `texte_brut(html)` renverra `'un exemple de chaine'`.

On admet :

* qu'un caractère `<` déclarant une ouverture de balise est *toujours* suivi plus loin dans la chaîne d'un caractère `>` ;
* qu'il n'y a **jamais** de balises imbriquées.

???+ example "Exemples"

    ```pycon title=""
    >>> html = "<p>un <strong>exemple</strong> de chaine</p>"
    >>> texte_brut(html)
    'un exemple de chaine'
    >>> html = "<h1>un grand titre en <span style='color: red'>rouge</strong> !</h1>"
    >>> texte_brut(html)
    'un grand titre en rouge !'
    ```

??? info "Sur la présentation des tests"
    Afin d'éviter des problèmes d'affichage de la page, les balises ont été découpées dans le fichier Python.

    Ainsi `#!python "<p>"` devient `#!python "<"+"p"+">"`.

{{ IDE('exo') }}
