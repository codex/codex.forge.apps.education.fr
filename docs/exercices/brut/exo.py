

# --------- PYODIDE:code --------- #

def texte_brut(html):
    resultat = ""
    balise = False
    for c in html:
        if c == "<":
            balise = ...
        elif c == ...:
            balise = False
        elif not ...:
            resultat += ...
    return ...

# --------- PYODIDE:corr --------- #

def texte_brut(html):
    resultat = ""
    balise = False
    for c in html:
        if c == "<":
            balise = True
        elif c == ">":
            balise = False
        elif not balise:
            resultat += c
    return resultat

# --------- PYODIDE:tests --------- #

html = "<"+"p"+">un <"+"strong"+">exemple<"+"/strong"+"> de chaine<"+"/p"+">"
assert texte_brut(html) == "un exemple de chaine"
html = "<"+"h1"+">un grand titre en <"+"span style='color: red'"+">rouge<"+"/strong"+"> !<"+"/h1"+">"
assert texte_brut(html) == "un grand titre en rouge !"

# --------- PYODIDE:secrets --------- #


# Tests
html = "<p>un <strong>exemple</strong> de chaine</p>"
assert texte_brut(html) == "un exemple de chaine"
html = "<h1>un grand titre en <span style='color: red'>rouge</strong> !</h1>"
assert texte_brut(html) == "un grand titre en rouge !"

# Tests supplémentaires
from random import choice, randrange
from string import ascii_lowercase
balises_possibles = ["h1", "h2", "h3", "h4", "p", "div", "span", "section", "nav", "header", "footer"]
for _ in range(10):
    nb_balises = 2 * randrange(1, 10)
    balises = [choice(balises_possibles) for _ in range(nb_balises)]
    aleatoire = "".join([choice(ascii_lowercase) for _ in range(randrange(1, 10))])
    attendu = aleatoire
    html = aleatoire
    for b in balises:
        aleatoire = "".join([choice(ascii_lowercase) for _ in range(randrange(1, 10))])
        attendu += aleatoire
        html += f"<{b}>" + aleatoire + f"</{b}>"
        aleatoire = "".join([choice(ascii_lowercase) for _ in range(randrange(1, 10))])
        attendu += aleatoire
        html += aleatoire
    assert texte_brut(html) == attendu, f"Erreur avec {html}"
