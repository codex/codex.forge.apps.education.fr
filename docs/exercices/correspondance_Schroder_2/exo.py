

# --------- PYODIDE:code --------- #

def chemin_vers_arbre(chemin):
    ...

# --------- PYODIDE:corr --------- #

def chemin_vers_arbre(chemin):
    arbre = []
    noeud = arbre
    pile = []
    for dx, dy in chemin:
        if dy == +1:
            pile.append(noeud)
        else:
            noeud = pile[-1]
            if dy == -1:
                pile.pop()
        noeud.append([])
        noeud = noeud[-1]
    return arbre

# --------- PYODIDE:tests --------- #

assert chemin_vers_arbre([(1, 1), (1, 1), (1, -1), (2, 0), (1, -1)]) == [
    [[], []],
    [],
    [],
]

assert chemin_vers_arbre([(1, 1), (2, 0), (1, 1), (1, -1), (1, -1)]) == [
    [],
    [[], []],
    [],
]

assert chemin_vers_arbre(
    [(1, 1), (2, 0), (1, 1), (2, 0), (1, 1), (1, -1), (2, 0), (2, 0), (1, -1), (1, -1)]
) == [[], [[], [[], []], [], [], []], []]

# --------- PYODIDE:secrets --------- #


# autres tests


def arbre_vers_chemin(arbre):
    def parcours_prefixe(arbre, chemin):
        i_premier = 0
        i_dernier = len(arbre) - 1
        for i, sous_arbre in enumerate(arbre):
            if i == i_premier:
                chemin.append((1, 1))
            elif i == i_dernier:
                chemin.append((1, -1))
            else:
                chemin.append((2, 0))
            parcours_prefixe(sous_arbre, chemin)

    chemin = []
    parcours_prefixe(arbre, chemin)
    return chemin


def gen(n):
    if n == 1:
        yield []
        return
    for i in range(1, n - 1):
        for gauche in gen(i):
            for droite in gen(n - i):
                arbre = [gauche]
                arbre.extend(droite)
                yield arbre
            for droite in gen(n - i - 1):
                arbre = [gauche, droite]
                yield arbre


for n in range(15):
    for arbre in gen(n):
        chemin = arbre_vers_chemin(arbre)
        assert chemin_vers_arbre(chemin) == arbre, str(arbre)