# --------- PYODIDE:code --------- #
def taille(arbre):
    ...

# --------- PYODIDE:corr --------- #
def taille(arbre):
    t = 1
    for sous_arbre in arbre[1]:
        t = t + taille(sous_arbre)
    return t

# --------- PYODIDE:tests --------- #
assert taille(("a", [])) == 1
assert taille(("a", [("b", []), ("c", []), ("d", [("e", [])])])) == 5
# --------- PYODIDE:secrets --------- #
arbre = ("a", [("b", [])])
attendu = 2
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", [])])
attendu = 3
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", []), ("d", [])])
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", [("d", [])]), ("c", [])])
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", [("d", [])])])
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", [("c", [("d", [("e", [("f", [])])])])]), ("g", [("h", [])])])
attendu = 8
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
