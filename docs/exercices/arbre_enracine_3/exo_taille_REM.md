La fonction `#!py sum` de Python permet d'alléger le code :

```python
def taille(arbre):
    return 1 + sum(taille(sous_arbre) for sous_arbre in arbre[1])
```

Cette approche est aussi valide dans le cas d'arbres réduits à une racine car `#!py sum([])` est évalué à `#!py 0`.