# --------- PYODIDE:code --------- #
def hauteur(arbre):
    ...
# --------- PYODIDE:corr --------- #
def hauteur(arbre):
    if arbre[1] == []:
        return 0
    h = 0
    for sous_arbre in arbre[1]:
        h = max(h, hauteur(sous_arbre))
    return 1 + h
# --------- PYODIDE:tests --------- #

assert hauteur(("a", [])) == 0
assert hauteur(("a", [("b", []), ("c", []), ("d", [("e", [])])])) == 2
# --------- PYODIDE:secrets --------- #
arbre = ("a", [("b", [])])
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", [])])
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", []), ("d", [])])
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", [("d", [])]), ("c", [])])
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", []), ("c", [("d", [])])])
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ("a", [("b", [("c", [("d", [("e", [("f", [])])])])]), ("g", [("h", [])])])
attendu = 5
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
