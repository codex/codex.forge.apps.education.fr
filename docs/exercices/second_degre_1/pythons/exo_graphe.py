# --------- PYODIDE:env --------- #
# Import de matplotlib (installation lors du 1er lancement)
import matplotlib

# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécicfiqué (id="cible")
from js import document

document.pyodideMplTarget = document.getElementById("graphe")

import matplotlib.pyplot as plt


def dessine(points):
    document.getElementById("graphe").textContent = ""

    _, ax = plt.subplots()
    xs = [x for x, _ in points]
    ys = [y for _, y in points]
    ax.plot(xs, ys, "r-")
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.show()


# --------- PYODIDE:code --------- #
def graphique(fonction, a, b, n):
    points = []
    pas = (... - ...) / ...
    x = ...
    for k in range(...):
        y = ...
        points.append((..., ...))
        x = ... + ...

    dessine(...)


# Modifiez la définition et l'appel ci-dessous 
# afin de représenter la fonction de votre choix
def f(x):
    return x**2

graphique(f, -3, 3, 500)
