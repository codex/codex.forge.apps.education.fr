---
author: Serge Bays
hide:
    - navigation
    - toc
title: Matrice d'adjacence
tags:
    - graphe
    - maths
difficulty: 270
maj: 12/04/2024
---

Soit $n$ un entier strictement positif.
  
On rappelle que la matrice d'adjacence $A$ associée à un graphe fini et simple dont les sommets sont numérotés de $1$ à $n$ est une matrice carrée à $n$ lignes et $n$ colonnes si $n$ est le nombre de sommets du graphe. Pour tout couple $(i, j)$, le coefficient $A_{i, j}$ vaut $1$ si les sommets numéro $i$ et numéro $j$ sont adjacents et $0$ sinon.

??? info "Un graphe et sa matrice d'adjacence"

    ![pour matrice d'adjacence](./pour_matrice_adjacence.svg){ width=20%; : .center }

    $$\begin{pmatrix}0&1&1&1&0\\1&0&0&0&1\\1&0&0&1&1\\1&0&1&0&0\\0&1&1&0&0\end{pmatrix}$$

<!--
    <center>
    ```mermaid
    flowchart LR
        D((1)) --- G((4))
        D --- F((3))
        E --- H((5))
        D --- E((2))
        F --- G
        F --- H
    ```
    </center>
-->

Cette matrice est représentée en Python par une liste contenant $n$ listes de longueur $n$, représentant les $n$ lignes.

??? question "Question 1 : Produit de matrices"

    Le produit de deux matrices carrées $A$ et $B$ à $n$ lignes et $n$ colonnes est la matrice $P=A\times B$ telle que pour tout couple $(i, j)$ : 

    $$p_{i, j} = \sum_{k=1}^n a_{i, k} b_{k, j}$$

    ![Produit](produit_3_3.svg){ .center .autolight width=40%}

    Dans la figure ci-dessus on a donc :

    $$p_{2, 3} = a_{2, 1} b_{1, 3} + a_{2, 2} b_{2, 3} + a_{2, 3} b_{3, 3}$$

    Si les matrices $A$ et $B$ sont représentées respectivement par les listes `a` et `b`, alors la matrice $P$ est représentée par la liste `p` telle que pour tout couple $(i, j)$ valide :

    <center>`#!py p[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + ... + a[i][n-1] * b[n-1][j]`</center>


    Compléter le code de la fonction `produit` qui prend en paramètres deux matrices carrées et renvoie le produit de ces deux matrices.

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> a = [[0, 1], [1, 0]]
        >>> b = [[1, 0], [0, 1]]
        >>> produit(a, b)
        [[0, 1], [1, 0]]
        >>> produit(a, a) == b
        [[1, 0], [0, 1]]
        ```

    {{ IDE('exo_1') }}

??? question "Question 2 : Recherche d'un triangle" 

    On considère un graphe, fini, simple et **non orienté**, la matrice d'adjacence associée $A$ et la matrice $A^2= A \times A$. Les deux propriétés suivantes sont équivalentes :

    - Il existe un couple $(i, j)$, avec $i \neq j$, tel que les coefficients $A_{i, j}$ et $A^2_{i, j}$ sont non nuls ;

    - Le graphe possède un cycle de longueur $3$, qu'on appelle un *triangle*.

    Par exemple, les deux graphes ci-dessous possèdent un *triangle*.

    ![triangles](./triangles.svg){ width=20%; : .center }


    Par contre, ce graphe ne possède pas de triangle :

    ![pas triangle](./pas_triangle.svg){ width=10%; : .center }


    Écrire une fonction `triangle` qui prend en paramètre une matrice d'adjacence associée à un graphe et renvoie `True` si le graphe possède un triangle et `False` sinon.
    
    Une version valide de la fonction `produit` demandée à la question précédente est déjà incluse dans cet éditeur. Vous pouvez l'utiliser **sans l'importer**.


    ???+ example "Exemples"

        ```pycon title=""
        >>> a = [[0, 1, 1], [1, 0, 1], [1, 1, 0]]
        >>> triangle(a)
        True
        >>> b = [
        ...    [0, 1, 1, 1, 0],
        ...    [1, 0, 0, 0, 1],
        ...    [1, 0, 0, 1, 1],
        ...    [1, 0, 1, 0, 0],
        ...    [0, 1, 1, 0, 0],
        ... ]
        >>> triangle(b)
        True
        >>> c = [[0, 1], [1, 0]]
        >>> triangle(c)
        False
        ```

    {{ IDE('exo_2') }}


<!--
    <center>
    ```mermaid
    flowchart LR
        A((1)) --- B((2))
        A --- C((3))
        B --- C
        
        D((1)) --- G((4))
        D --- F((3))
        E --- H((5))
        D --- E((2))
        F --- G
        F --- H
    ```
    </center>


    <center>
    ```mermaid
    flowchart LR
        A((1)) --- B((2))
    ```
    </center>
-->

