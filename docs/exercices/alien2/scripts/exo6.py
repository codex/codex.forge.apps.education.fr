# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

app.centrer("figure6") 


# --------- PYODIDE:code --------- #
haut(...)
...
x = ...
gauche(x + 1)
y = ...
bas(x - y)
z = ...
droite(x * z)



# --------- PYODIDE:post --------- #
app.verifier_programme(['H8', 'D8', 'D7', 'D2', 'F2', 'F14'])