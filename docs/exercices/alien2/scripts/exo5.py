# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

app.centrer("figure5") 


# --------- PYODIDE:code --------- #
bas(...)
gauche(...)
...
x = ...
droite(x - 1)



# --------- PYODIDE:post --------- #
app.verifier_programme(['H8', 'K8', 'K4', 'D4', 'D8'])

