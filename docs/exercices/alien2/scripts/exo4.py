# --------- PYODIDE:env --------- #
from alien_python import *
import p5


app4 = App("figure4")

x = 5
y = x - 2
app4.gauche(x)
app4.bas(y)
app4.droite(x - 1)
z = x - y
app4.bas(2 * z)

app4.dessiner_parcours()