---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Alien (2) - Variables et affectations
tags:
    - SNT
difficulty : 050
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :

    
    * « {{ lien_exo("Alien (1) : Appels de fonctions", "alien_1") }} »,

    * « {{ lien_exo("Alien (2) : Variables et affectations", "alien2") }} »,
    
    * « {{ lien_exo("Alien (3) : Instructions conditionnelles", "alien3") }} »,
    
    * « {{ lien_exo("Alien (4) : Boucles bornées", "alien4") }} »,
    
    * « {{ lien_exo("Alien (5) : Fonctions", "alien5") }} »,
    
    * « {{ lien_exo("Alien (6) : Tableaux", "alien6") }} ».


Les règles sont simples : l'alien :alien: se situe au départ au centre de la grille et peut être déplacé avec les fonctions `haut`, `bas`, `gauche` et `droite`.

L'objectif est de trouver la case finale de l'alien (et donc son parcours) après exécution du programme donné.

??? example "Rappels des différentes instructions"

    * `haut(4)` déplace l'alien de `#!py 4` cases vers le haut ;
    * `haut(-2)` déplace l'alien de `#!py 2` cases vers le bas ;
    * `haut()` déplace l'alien de `#!py 1` case vers le haut (c'est donc équivalent à `#!py haut(1)`).

    Le principe est similaire avec les trois autres fonctions.

Pour les questions suivantes, dessinez le parcours de l'alien en cliquant sur **la case d'arrivée** de chaque instruction exécutée. Vous pourrez ensuite valider votre parcours pour vérifier s'il est correct.

??? question "Question 1"
    ```python { .inline .end .w45 }
    a = 3
    haut(a)
    gauche(a)
    ```
    {{ run('scripts/exo1') }}
    {{ figure("figure1") }}


??? question "Question 2"
    ```python { .inline .end .w45 }
    a = 3
    haut(a)
    a = a - 1
    droite(a)
    a = a + 2
    haut(-a)
    ```
    {{ run('scripts/exo2') }}
    {{ figure("figure2") }}

??? question "Question 3"
    ```python { .inline .end .w45 }
    a = 3
    bas(a)
    droite(2 * a)
    a = a - 5
    bas(a)
    a = a + 4
    gauche(a)
    a = 2 * a
    haut(a)
    ```
    {{ run('scripts/exo3') }}
    {{ figure("figure3") }}   

??? question "Question 4"
    ```python { .inline .end .w45 }
    x = 5
    y = x - 2
    gauche(x)
    bas(y)
    droite(x - 1)
    z = x - y
    bas(2 * z)
    ```

    {{ run('scripts/exo4') }}
    {{ figure("figure4") }}

Pour les questions suivantes écrire le code nécessaire pour obtenir le déplacement souhaité (les numéros correspondent aux différentes étapes).

??? question "Question 5"
    ![alien](images/alien21.png)

    {{ IDE('scripts/exo5') }}
    {{ figure("figure5",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 6"
    ![alien](images/alien22.png)

    {{ IDE('scripts/exo6') }}
    {{ figure("figure6",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}
