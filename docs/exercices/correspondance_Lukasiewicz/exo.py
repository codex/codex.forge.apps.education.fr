

# --------- PYODIDE:code --------- #

def arbre_vers_chemin(arbre):
    ...

# --------- PYODIDE:corr --------- #

def arbre_vers_chemin(arbre):
    def etape(arbre):
        "Fonction récursive interne"
        resultat = [len(arbre)]
        for sous_arbre in arbre:
            resultat.extend(etape(sous_arbre))
        return resultat

    chemin = [y - 1 for y in etape(arbre)]
    chemin.pop()
    return chemin

# --------- PYODIDE:tests --------- #

assert arbre_vers_chemin([[], []]) == [1, -1]
assert arbre_vers_chemin([[], [], [[], []]]) == [2, -1, -1, 1, -1]

# --------- PYODIDE:secrets --------- #

# tests

assert arbre_vers_chemin([[], []]) == [1, -1]

assert arbre_vers_chemin([[], [], [[], []]]) == [2, -1, -1, 1, -1]

# autres tests

assert arbre_vers_chemin([]) == []
assert arbre_vers_chemin([[]]) == [0]
assert arbre_vers_chemin([[[[[]]]]]) == [0, 0, 0, 0]
assert arbre_vers_chemin([[], [], []]) == [2, -1, -1]
assert arbre_vers_chemin([[], [], [], [], []]) == [4, -1, -1, -1, -1]
assert arbre_vers_chemin([[[]], [[], [], [], [[]], []], []]) == [
    2,
    0,
    -1,
    4,
    -1,
    -1,
    -1,
    0,
    -1,
    -1,
]
