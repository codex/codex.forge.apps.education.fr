---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Tourner en rond
difficulty: 220
tags:
    - dictionnaire
    - graphe
    - pile
    - file
   
---

On souhaite se diriger à l'intérieur d'une ville, et notamment savoir s'il est possible de tourner en rond.

On représente la circulation dans une ville par un graphe orienté, où chaque sommet correspond à un lieu de la ville et chaque arête au sens de circulation entre deux lieux.

???+ example "Exemple de plan de circulation"

    <center>
    
    ```mermaid
    flowchart LR
        A([Lycée]) --> B([Mairie])
        B --> A
        B --> E([Médiathèque])
        C([Port]) --> B
        B --> C
        D([Ecole]) --> G([Maison])
        E --> B
        E --> D
        G --> E
        E --> F([Stade])
        F --> G
    ```
    </center>

    Pour aller au *Stade* depuis le *Lycée*, on pourra emprunter le chemin *Lycée* - *Mairie* - *Médiathèque* - *Stade*.
 
    Il est à noter que depuis la *Médiathèque*, on peut revenir à la *Médiathèque* en passant par l'*Ecole* puis la *Maison*.

    Imaginons maintenant qu'il y a des travaux sur certaines routes qui sont désormais fermées. La circulation devient alors :

    <center>
    
    ```mermaid
    flowchart LR
        A([Lycée]) --> B([Mairie])
        B --> E([Médiathèque])
        C([Port]) --> B
        D([Ecole]) --> G([Maison])
        E --> D
        E --> F([Stade])
        F --> G
    ```
    </center>
    
    Dans ce nouveau graphe, il n'y a plus de chemin possible depuis la *Maison* vers le *Port*.

    De plus, quel que soit le lieu, il n'existe pas de chemin qui part de ce lieu et qui y revient.

On représente ce graphe par un dictionnaire dans lequel :

- les clés sont les chaînes de caractères correspondant aux noms des lieux, 
  
- les valeurs associées sont des listes de chaînes de caractères représentant les lieux vers lesquels on peut se diriger.

??? question "1. Fonction `existe_chemin`"
    
    On se demande si, connaissant un plan de circulation, il est possible de se rendre d'un point de départ à un point d'arrivée.
    
    Vous devez donc d'écrire une fonction `existe_chemin` qui :

    - prend en paramètre un dictionnaire `graphe` représentant une telle circulation, une chaîne de caractères
    `depart` qui représente le lieu de départ et une chaîne de caractères `arrivee` qui représente le lieu d'arrivée ;

    - renvoie `True` si un chemin existe entre le lieu de départ et le lieu d'arrivée, `False` sinon.


    ???+ example "Exemples"

        ```pycon title=""
        >>> circulation = {
        ...   "Lycee": ["Mairie"],
        ...   "Port": ["Mairie"],
        ...   "Mairie": ["Lycee", "Mediatheque", "Port"],
        ...   "Mediatheque": ["Mairie", "Ecole", "Stade"],
        ...   "Ecole": ["Maison"],
        ...   "Stade": ["Maison"],
        ...   "Maison": ["Mediatheque"],
        ... }
        >>> existe_chemin(circulation, "Lycee", "Mediatheque")
        True
        ```
        ```pycon title=""
        >>> circulation_en_travaux = {
        ...   "Lycee": ["Mairie"],
        ...   "Port": ["Mairie"],
        ...   "Mairie": ["Mediatheque"],
        ...   "Mediatheque": ["Ecole", "Stade"],
        ...   "Ecole": ["Maison"],
        ...   "Stade": ["Maison"],
        ...   "Maison": [],
        ... }
        >>> existe_chemin(circulation_en_travaux, "Maison", "Port")
        False
        ```

    ??? tip "Aide"

        On pourra utiliser un parcours du graphe. 
        
        À ce titre, on fournit les classes `Pile` et `File` dont on donne les interfaces ci-dessous.
        
        Ces deux classes sont déjà importées dans les différents éditeurs.

        {{ remarque('interface_Pile')}}
        {{ remarque('classe_Pile')}}
        
        {{ remarque('interface_File')}}
        {{ remarque('classe_File')}}

    === "Version vide"
        {{ IDE('exo_vide', MAX_SIZE=45) }}
    === "Version à compléter (profondeur)"
        {{ IDE('exo_profondeur', MAX_SIZE=45) }}
    === "Version à compléter (largeur)"
        {{ IDE('exo_largeur', MAX_SIZE=45) }}

??? question "2. Fonction `contient_cycle`"

    Un promeneur se demande s'il est possible, connaissant le plan de circulation, de faire au moins une promenade débutant et se terminant au même endroit de la ville : une promenade allant du *Lycée* au *Lycée* ou une autre allant du *Port* au *Port*, *etc*.
    
    Si au moins une telle promenade existe, on dit que le graphe représentant le plan de circulation contient un *cycle*.
    
    Vous devez donc d'écrire une fonction `contient_cycle` qui :

    - prend en paramètre un dictionnaire `graphe` représentant une telle circulation ;

    - renvoie `True` si le graphe contient un cycle, `False` sinon.

    La fonction `existe_chemin` de la question précédente est déjà importée dans cet éditeur.


    ???+ example "Exemple"

        ```pycon title=""
        >>> circulation = {
        ...   "Lycee": ["Mairie"],
        ...   "Port": ["Mairie"],
        ...   "Mairie": ["Lycee", "Mediatheque", "Port"],
        ...   "Mediatheque": ["Mairie", "Ecole", "Stade"],
        ...   "Ecole": ["Maison"],
        ...   "Stade": ["Maison"],
        ...   "Maison": ["Mediatheque"],
        ... }
        >>> contient_cycle(circulation)
        True
        ```
        
        ```pycon title=""
        >>> circulation_en_travaux = {
        ...   "Lycee": ["Mairie"],
        ...   "Port": ["Mairie"],
        ...   "Mairie": ["Mediatheque"],
        ...   "Mediatheque": ["Ecole", "Stade"],
        ...   "Ecole": ["Maison"],
        ...   "Stade": ["Maison"],
        ...   "Maison": [],
        ... }
        >>> contient_cyle(circulation_en_travaux)
        False
        ```

    {{ IDE('exo2') }}