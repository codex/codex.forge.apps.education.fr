# --------- PYODIDE:env --------- #
from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.append(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.popleft()


class Pile:
    """Classe définissant une structure de pile"""

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def empile(self, x):
        """Place x au sommet de la pile"""
        self.contenu.append(x)

    def depile(self):
        """Retire et renvoie l'élément placé au sommet de la pile.
        Provoque une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.contenu.pop()


# --------- PYODIDE:code --------- #
def existe_chemin(graphe, depart, arrivee):
    ...

# --------- PYODIDE:corr --------- #

# Version avec un parcours en profondeur et donc une pile
def existe_chemin(graphe, depart, arrivee):
    pile = Pile()
    visites = {}
    pile.empile(depart)
    while not pile.est_vide():
        courant = pile.depile()
        visites[courant] = True
        if arrivee in graphe[courant]:
            return True
        for etape in graphe[courant]:
            if not etape in visites:
                pile.empile(etape)
                visites[etape] = True
    return False

# Version avec un parcours en largeur et donc une file
def existe_chemin(graphe, depart, arrivee):
    file = File()
    visites = {}
    file.enfile(depart)
    while not file.est_vide():
        courant = file.defile()
        visites[courant] = True
        if arrivee in graphe[courant]:
            return True
        for etape in graphe[courant]:
            if not etape in visites:
                file.enfile(etape)
                visites[etape] = True
    return False


# --------- PYODIDE:tests --------- #

circulation = {
    "Lycee": ["Mairie"],
    "Port": ["Mairie"],
    "Mairie": ["Lycee", "Mediatheque", "Port"],
    "Mediatheque": ["Mairie", "Ecole", "Stade"],
    "Ecole": ["Maison"],
    "Stade": ["Maison"],
    "Maison": ["Mediatheque"],
}

circulation_en_travaux = {
    "Lycee": ["Mairie"],
    "Port": ["Mairie"],
    "Mairie": ["Mediatheque"],
    "Mediatheque": ["Ecole", "Stade"],
    "Ecole": ["Maison"],
    "Stade": ["Maison"],
    "Maison": [],
}


assert existe_chemin(circulation, "Lycee", "Mediatheque")
assert existe_chemin(circulation, "Maison", "Maison")
assert not existe_chemin(circulation_en_travaux, "Stade", "Port")
assert not existe_chemin(circulation_en_travaux, "Maison", "Maison")

# --------- PYODIDE:secrets --------- #


# Tests supplémentairescirculation = {
circulation = {
    "Lycee": ["Mairie"],
    "Port": ["Mairie"],
    "Mairie": ["Mediatheque"],
    "Mediatheque": ["Ecole", "Stade"],
    "Ecole": ["Maison"],
    "Stade": ["Maison"],
    "Maison": [],
}
assert not existe_chemin(circulation, "Maison", "Stade")
assert existe_chemin(circulation, "Lycee", "Maison")
assert not existe_chemin(circulation, "Maison", "Maison")
