La construction de `resultat` sous forme d'une liste amène à effectuer le test `#!py ami_de_ami not in resultat` qui est de coût linéaire (plus la liste `resultat` est longue, plus la recherche peut être longue).

On peut être plus efficace en utilisant un dictionnaire. Il faut par contre effectuer une conversion sous forme de liste avant de renvoyer le résultat :

```python
def amis_d_amis(reseau, membre):
    vus = dict()
    for ami in reseau[membre]:
        for ami_de_ami in reseau[ami]:
            vus[ami_de_ami] = True
    return [cle for cle in vus if cle != membre]  # conversion en liste
```

Avec un dictionnaire, nous pouvons également utiliser la méthode `dict.keys()` : 

```python
def amis_d_amis(reseau, membre):
    vus = dict()
    for ami in reseau[membre]:
        for ami_de_ami in reseau[ami]:
            if ami_de_ami != membre:  # test efficace
                vus[ami_de_ami] = True
    return list(vus.keys())
```

👉 Notons que `return vus.keys()` ne renvoie pas une liste mais on objet du type `#!py dict_keys`. On effectue donc une conversion au moment de renvoyer le résultat : `return list(vus.keys())`
