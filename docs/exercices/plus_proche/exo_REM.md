Une autre possibilité en initialisant la distance minimale à $+\infty$ et en faisant une boucle sur les éléments du tableau et non sur les indices.

D'autre part, comme ce qui nous intéresse ici est de comparer les distances, nous pouvons nous contenter de calculer le carré de la distance euclidienne. Nous éviterons ainsi la manipulation des nombres flottants.

```python
def distance_carree(point_A, point_B):
    x_A, y_A = point_A
    x_B, y_B = point_B
    return (x_B - x_A) ** 2 + (y_B - y_A) ** 2

def plus_proche(points, depart):
    point_proche = None
    dist_minimale = float('inf')
    for point in points:
        dist_courante = distance_carree(point, depart)
        if dist_courante < dist_minimale:
            point_proche = point
            dist_minimale = dist_courante
    return point_proche
```
