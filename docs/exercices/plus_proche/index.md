---
author:
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Le plus proche
difficulty: 230
tags:
    - à trous
    - float
    - tuple
    - liste/tableau
    - ep2
maj: 01/02/2024
--- 

{{ version_ep() }}

Nous souhaitons programmer une fonction qui, étant donnée une liste de points du plan, donne celui qui est le plus proche d'un point de départ.

Chaque point est donné sous la forme d'un tuple de deux entiers.
La liste des points à traiter est donc un tableau de tuples.

La distance utilisée est la distance euclidienne. La distance euclidienne entre deux points du plan de coordonnées $(x_A;\,y_A)$ et $(x_B;\,y_B)$
est donnée par la formule :

$$d_{AB}=\sqrt{\left(x_B-x_A\right)^2+\left(y_B-y_A\right)^2}$$

??? tip "Décompactage"

    Si la variable `point` référence notre couple de coordonnées, alors le _décompactage_ ("unpacking" en anglais) permet de donner un nom simple aux composantes `point[0]` et `point[1]` :

    ```python
    x, y = point
    ```

Compléter les fonctions `distance` et `plus_proche`.

La fonction `distance` prend en paramètres deux tuples représentant deux points et renvoie leur distance.

La fonction `plus_proche` prend en paramètres une liste non vide de tuples représentant des points ainsi qu'un point de départ et renvoie le tuple correspondant au point le plus proche de celui de départ.

???+ example "Exemples"

    ```pycon title=""
    >>> distance((0, 3), (4, 0))
    5.0
    >>> plus_proche([(7, 9), (2, 5), (5, 2)], (0, 0))
    (2, 5)
    >>> plus_proche([(7, 9), (2, 5), (5, 2)], (7, 9))
    (7, 9)
    ```

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
