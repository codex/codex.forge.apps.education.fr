---
author:
    - Vincent-Xavier Jumel
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Moyenne Mobile
tags:
    - liste/tableau
difficulty: 210
maj: 09/05/2024
---



Un **accéléromètre** permet de déterminer les mouvements d'un objet. Lorsque l'on effectue une série de mesures issues de ce capteur, on récupère sur le  **micro-contrôleur** des valeurs qui varient rapidement, ou avec des imperfections, des variations parasites appelées **bruit**.

Il est ainsi difficile d'interpréter les informations brutes issues de ce capteur :

![signal parasité](brut.png)


Il convient alors d'appliquer un **filtre** à ce signal afin de le lisser et supprimer les variations rapides, comme représenté ci-dessous :

![signal lissé](signalfiltre1.png)


Un filtre simple à mettre en place est la **moyenne glissante** ou **mobile**. Une valeur filtrée est obtenue en faisant la moyenne des N dernières mesures.
Avec une moyenne glissante, on ne calcule pas la moyenne sur l'ensemble des valeurs de la série, mais sur un sous-ensemble de N éléments consécutifs. Ainsi ce sous-ensemble, parfois appelé **fenêtre**, utilisé pour calculer chaque moyenne **glisse** sur l'ensemble des données.

Imaginons une fenêtre de $4$ éléments :

![moyenne glissante](moyenne_glissante.svg)


Il est nécessaire d'avoir $4$ éléments pour calculer une moyenne : cette série de $6$ valeurs nous permet de calculer uniquement $3$ moyennes.

Écrire la fonction `moyenne_mobile`. Cette fonction prend en paramètres un tableau de nombres `serie` contenant les mesures et un entier `N` qui vaut la taille de la fenêtre. Cette fonction renvoie la liste des moyennes glissantes calculées sur `serie` avec une fenêtre de taille `N`.

On assure que le nombre d'éléments de la série est supérieur à  `N`.

??? danger "Contraintes"

    On n'utilisera ni `sum`, ni `mean` ni les tranches (comme par exemple `liste[i:i+7]`)

???+ example "Exemples"

    ```pycon title=""
    >>> moyenne_mobile([1, 2, 3, 4, 5, 6], 4)
    [2.5, 3.5, 4.5]
    ```

=== "Version vide"
    {{ IDE('exo_vide', SANS="sum, mean") }}

=== "Version à compléter"
    L'algorithme est ici le suivant :

    * on calcule une première fois la moyenne,
    * pour les moyennes suivantes, on supprime la valeur la plus ancienne du total, puis on y ajoute la valeur la plus récente.

    {{ IDE('exo_trous', SANS="sum, mean") }}

