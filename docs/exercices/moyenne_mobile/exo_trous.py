

# --------- PYODIDE:code --------- #



def moyenne_mobile(serie, N):
    nbre_moyennes = len(serie) - N + 1
    resultats = [0 for _ in range(nbre_moyennes)]
    somme = 0
    for i in range(...):
        somme = ...
    resultats[0] = somme/N
    indice_sortant = 0
    indice_entrant = N
    indice_moyenne = 1
    for indice in range(nbre_moyennes-1):
        somme = somme + serie[...] - serie[...]
        resultats[indice_moyenne] = ...
        indice_sortant += 1
        indice_entrant += 1
        indice_moyenne += 1
    return resultats

# --------- PYODIDE:corr --------- #

def moyenne_mobile(serie, N):
    nbre_moyennes = len(serie) - N + 1
    resultats = [0 for _ in range(nbre_moyennes)]
    somme = 0
    for i in range(N ):
        somme = somme + serie[i]
    resultats[0] = somme / N
    indice_sortant = 0
    indice_entrant = N
    indice_moyenne = 1
    for indice in range(nbre_moyennes-1):
        somme = somme + serie[indice_entrant] - serie[indice_sortant]
        resultats[indice_moyenne] = somme / N
        indice_sortant += 1
        indice_entrant += 1
        indice_moyenne += 1
    return resultats

# --------- PYODIDE:tests --------- #

assert moyenne_mobile([1, 2, 3, 4, 5, 6], 4) == [2.5, 3.5, 4.5]
assert moyenne_mobile([1, 1, 1, 1, 1, 1, 1, 1], 8) ==  [1.0]
assert moyenne_mobile([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 8) ==  [4.5, 5.5, 6.5, 7.5, 8.5]


# --------- PYODIDE:secrets --------- #

# autres tests

assert moyenne_mobile([1, -1, 1, -1, 1, -1, 1, -1], 8) ==  [0]


