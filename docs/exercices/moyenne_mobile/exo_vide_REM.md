Recalculer la somme pour chaque nouvelle fenêtre permet d'obtenir le résultat, mais est peu efficace car demande des calculs qui sont en fait inutiles.

```python
def moyenne_mobile(serie, N):
    nbre_moyennes = len(serie) - N + 1
    resultats = [0 for _ in range(nbre_moyennes)]
    
    for indice in range(nbre_moyennes):
        somme = 0
        for i in range(indice, indice+N):
            somme = somme + serie[i]
        resultats[indice] = somme / N

    return resultats
```

Un algorithme plus efficace, présenté en correction, est le suivant : 

* on calcule une première fois la moyenne, 
* pour les moyennes suivantes, on supprime la valeur la plus ancienne du total, puis on y ajoute la valeur la plus récente.

Cet algorithme repose sur le principe de la **programmation dynamique**.
