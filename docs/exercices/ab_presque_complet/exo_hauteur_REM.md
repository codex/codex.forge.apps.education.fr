L'idée est de partir du nœud le plus à droite du dernier niveau (nœud d'indice `#!py i = len(arbre) - 1`) et de remonter à la racine. Pour ce faire on divise cet indice par deux jusqu'à atteindre l'indice de la racine.

On pourrait aussi, à l'inverse, partir de la racine (à l'indice `#!py 1`) et descendre de sous-arbre gauche en sous-arbre gauche. Dans ce cas on aurait :

```python
def hauteur(arbre):
    t = taille(arbre)
    h = 0
    i = 1
    while i <= t:
        i = 2 * i
        h = h + 1
    return h
```

Les divisions successives par `#!py 2` évoquent les conversions en binaire. Ainsi, si on considère des arbres non-vides, la hauteur d'un arbre binaire presque complet est liée au nombre de bits permettant d'écrire la taille de celui-ci.

Les trois fonctions suivantes sont donc aussi valides :

* en utilisant `#!py bin`

    ```python
    def hauteur(arbre):
        t = taille(arbre)
        if t == 0:
            return 0
        return len(bin(t)) - 2  # bin(t) est préfixé par `0b` donc on soustrait 2
    ```

* en utilisant `#!py log2` :

    ```python
    from math import log2
    
    def hauteur(arbre):
        t = taille(arbre)
        if t == 0:
            return 0
        return int(log2(t)) + 1
    ```

* plus original, avec `#!py int.bit_length()` qui renvoie le nombre de bits d'un entier :

    ```python
    def hauteur(arbre):
        t = taille(arbre)
        if t == 0:
            return 0
        return t.bit_length()
    ```
