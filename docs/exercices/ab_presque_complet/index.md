---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Arbre binaire presque complet
difficulty: 280
tags:
    - arbre binaire
    - binaire
    - récursivité
    - en travaux
maj: 01/02/2025
---

On s'intéresse dans cet exercice aux *arbres binaires presque complets*. Un tel arbre binaire est soit :

* un arbre vide ;

* un arbre binaire dont tous les niveaux sont remplis à l'exception éventuelle du dernier qui, s'il n'est pas entièrement rempli, est « tassé » à gauche : tous les nœuds présents au dernier niveau se trouvent sur la gauche de l'arbre binaire.

???+ example "Un arbre binaire presque complet"

    <center>

    ```mermaid
    flowchart TD
        a((a)) --> b((b))
        a      --> c((c))
        b      --> d((d))
        b      --> e((e))
        c      --> f((f))
        c      ~~~ g(( ))
        style g fill:none,stroke:none,color:none
        classDef noeud font-family:consolas;
        class a,b,c,d,e,f noeud;
    ```
    </center>

    On n'a pas représenté les arbres binaires vides.

    Cet arbre binaire est presque complet : les deux premiers niveaux sont entièrement remplis, les nœuds du dernier niveau se trouvent sur la gauche.

Un arbre binaire presque complet de taille $n$ peut être représenté en machine par un tableau de $n+1$ valeurs. En indexant les valeurs à partir de $0$, on trouve :

* une valeur quelconque à l'indice $0$ ;
* la valeur portée par la racine à l'indice $1$.

Pour les autres nœuds, on considère les règles suivantes. Si la valeur d'un nœud est stockée à l'indice $k$ :

* si son sous-arbre gauche est non-vide, sa valeur est stockée à l'indice $2k$ ;
* si son sous-arbre droit est non-vide, sa valeur est stockée à l'indice $2k+1$.

Avec Python, on peut donc représenter les arbres binaires presque complets à l'aide de listes. On place la valeur `#!py None` à l'indice `#!py 0`.

L'arbre vide est donc représenté par la liste `#!py [None]`.

???+ example "Représentation de l'arbre binaire vu plus haut"

    L'arbre binaire dessiné plus haut a une taille de $6$. Il est donc représenté par une liste de longueur `#!py 7` :

    ```python title=""
    # indices   0   1    2    3    4    5    6
    arbre = [None, 'a', 'b', 'c', 'd', 'e', 'f']
    ```

    Le `#!py 'b'` se trouve a l'indice `#!py 2`. On trouve bien `#!py 'd'` à l'indice `#!py 2*2 = 4` et `#!py 'e'` à l'indice `#!py 2*2 + 1 = 5`.


??? question "1. Taille de l'arbre binaire"

    Écrire la fonction `#!py taille` qui prend en paramètre une liste représentant un arbre binaire presque complet et renvoie sa taille.

    ???+ example "Exemples"

        ```pycon title=""
        >>> taille([None])
        0
        >>> taille([None, 'a', 'b', 'c', 'd', 'e', 'f'])
        6
        ```

    {{ IDE('exo_taille') }}

??? question "2. Hauteur de l'arbre binaire"

    Écrire la fonction `#!py hauteur` qui prend en paramètre une liste représentant un arbre binaire presque complet et renvoie sa hauteur.

    Une version valide de la fonction `#!py taille` est disponible dans l'éditeur ci-dessous.
    Vous pouvez directement l'utiliser sans l'importer.

    ???+ example "Exemples"

        ```pycon title=""
        >>> hauteur([None])
        0
        >>> hauteur([None, 'a', 'b', 'c', 'd', 'e', 'f'])
        3
        ```

    {{ IDE('exo_hauteur') }}
