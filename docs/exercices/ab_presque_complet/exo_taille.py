# --------- PYODIDE:env --------- #
def taille(arbre):
    ...
# --------- PYODIDE:code --------- #
def taille(arbre):
    ...

# --------- PYODIDE:corr --------- #
def taille(arbre):
    return len(arbre) - 1

# --------- PYODIDE:tests --------- #
assert taille([None]) == 0
assert taille([None, 'a', 'b', 'c', 'd', 'e', 'f']) == 6
# --------- PYODIDE:secrets --------- #
from random import randrange
for t in range(20):
    arbre = [None] + [randrange(10, 50) for _ in range(t)]
    attendu = t
    assert taille(arbre) == attendu, f"Erreur avec {arbre = }"