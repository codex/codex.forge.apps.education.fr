On notera que, dans le cas précis de cette représentation des arbres binaires presque complets, on peut donc obtenir la taille de l'arbre en ne faisant qu'une soustraction : `#!py len(arbre) - 1`.

Ainsi le calcul de la taille d'un arbre comptant $10^{10}$ nœuds ne sera pas plus compliqué ni long que celui d'un arbre vide !