# --------- PYODIDE:env --------- #
def taille(arbre):
    return len(arbre) - 1

# --------- PYODIDE:code --------- #
def hauteur(arbre):
    ...


# --------- PYODIDE:corr --------- #
def hauteur(arbre):
    if len(arbre) == 1:
        return 0
    i = len(arbre) - 1
    t = 1
    while i != 1:
        i = i // 2
        t = t + 1
    return t

# --------- PYODIDE:tests --------- #
assert hauteur([None]) == 0
assert hauteur([None, 'a', 'b', 'c', 'd', 'e', 'f']) == 3
# --------- PYODIDE:secrets --------- #
from random import randrange
for t in range(1, 20):
    arbre = [None] + [randrange(10, 50) for _ in range(t)]
    attendu = (t).bit_length()
    assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"