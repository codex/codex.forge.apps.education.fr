from random import *
from time import *

n = 10
pos = [0, 1, 2]
while True:
    if randrange(2) == 0:
        # décalage
        i = randrange(3)
        dir = choice([-1, +1])
        if dir > 0:
            if (i == 2):
                if pos[2] < n - 1:
                    pos[2] += 1
                else:
                    continue
            elif pos[i] + 1 < pos[i + 1]:
                pos[i] += 1
            else:
                continue
        else:
            if (i == 0):
                if pos[0] > 0:
                    pos[0] -= 1
                else:
                    continue
            elif pos[i] - 1 > pos[i - 1]:
                pos[i] -= 1
            else:
                continue
    else:
        # saut
        i, j = choice([(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1)])
        arrivée = 2*pos[j] - pos[i]
        if (0 <= arrivée < n) and arrivée not in pos:
            pos[i] = arrivée
            pos.sort()
        else:
            continue
    
    msg = [" "] * n
    for p in pos: msg[p] = "#"
    print("".join(msg))
    sleep(0.1)
    if pos == [0, 1, 2]:
        break
    