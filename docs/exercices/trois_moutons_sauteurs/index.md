---
license: "by-nc-sa"
author: 
    - Franck Chambon
    - Pierre Marquestaut
    - Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Trois moutons sauteurs
tags:
    - graphe
    - file
maj: 26/11/2024
---

Trois moutons indifférenciés font une partie de « saute-moutons » sur un terrain comportant $n$ emplacements numérotés par leurs indices allant de `#!py 0` à `#!py n - 1`. Ils sont initialement placés tout à gauche du terrain. Deux moutons ne peuvent pas être sur le même emplacement.

À chaque instant, on représente la « configuration » des moutons par un triplet ordonné `#!py (a, b, c)` : le mouton le plus à gauche est sur l'emplacement d'indice `#!py a`, celui du milieu sur l'emplacement d'indice `#!py b` et le plus à droite sur celui d'indice `#!py c`. Toutes les configurations vérifient donc `#!py a < b < c`. La configuration initiale est représentée par le triplet `#!py (0, 1, 2)`.

À chaque tour, un mouton peut :

- se déplacer d'une case à droite ou à gauche **sur un emplacement libre** ;
- faire un saut sur un **emplacement libre**. Pour réaliser le saut, un mouton doit tout d'abord choisir n'importe lequel de ses deux camarades. Il saute alors par dessus celui-ci et atterrit sur l'emplacement symétrique de son emplacement actuel par rapport au mouton choisi. Si par exemple, le mouton situé sur l'emplacement d'indice `#!py 0` saute par dessus celui placé à l'indice `#!py 2`, il atterrit sur l'emplacement d'indice `#!py 4`.

La figure ci-dessous représente les configurations successives de trois moutons sur une ligne de $10$ emplacements.

{{ figure(
    "animation",
    admo_kind="???+",
    admo_title="Des moutons qui jouent à « saute-moutons »",
    inner_text="La partie va bientôt commencer 🐑 🐑 🐑",
    admo_class="autolight") }}

{{ run("animation") }}

En appliquant ces règles dans le cas d'une ligne comptant `#!py 5` emplacements, à la fin du premier tour les différentes configurations possibles sont :

- `#!py (0, 1, 3)` : le mouton en `#!py 2` s'est décalé de `#!py 2` à `#!py 3` ;
- `#!py (1, 2, 4)` : le mouton en `#!py 0` a sauté par-dessus de celui en `#!py 2` et se retrouve désormais en `#!py 4` ;
- `#!py (0, 2, 3)` : le mouton en `#!py 1` a sauté par-dessus de celui en `#!py 2` et se retrouve désormais en `#!py 3` ;

On imagine que les moutons jouent de façon à découvrir toutes les différentes configurations. On souhaite obtenir un dictionnaire qui associe à chaque configuration son **coût**, à savoir **le nombre minimum de tours pour l'atteindre à partir de la position initiale**.

??? question "1. Configurations voisines"
    
    La fonction `configurations_voisines` prend en paramètres un triplet d'entiers `#!py config` ainsi qu'un entier `#!py n`. Le triplet `#!py config` représente la configuration prise par les moutons à un certain instant et l'entier `#!py n` le nombre d'emplacements sur la ligne.
    
    Cette fonction renvoie la liste des triplets représentant les configurations possibles au tour suivant. Cette liste pourra être renvoyée dans n'importe quel ordre.

    On garantit que :

    - `#!py n` est un entier de `#!py 3` à `#!py 32` ;

    - `#!py config = (a, b, c)` est un triplet **bien formé**, c'est-à-dire avec `#!py 0 <= a < b < c < n`.

    **Attention**, la liste renvoyée ne devra contenir que des triplets *bien formés* à l'image du triplet `#!py config` : `#!py 0 <= a < b < c < n`.
    
    Écrire la fonction `configurations_voisines`.

    ???+ example "Exemples"

        ```pycon title=""
        >>> configurations_voisines((0, 1, 2), 10)
        [(0, 1, 3), (1, 2, 4), (0, 2, 3)]
        >>> configurations_voisines((7, 8, 9), 10)
        [(6, 8, 9), (6, 7, 9), (5, 7, 8)]
        >>> configurations_voisines((2, 4, 7), 10)
        [(1, 4, 7), (3, 4, 7), (2, 3, 7), (2, 5, 7), (2, 4, 6), (2, 4, 8), (4, 6, 7), (0, 2, 7), (1, 2, 4)]
        ```
    
    {{ IDE('exo') }}

??? question "2. Coût des positions"
    
    La fonction `#!py couts` en prend en paramètre un entier `n` donnant le nombre d'emplacements sur la ligne.
    
    Cette fonction renvoie le dictionnaire associant à chaque configuration que peuvent prendre les moutons, le nombre minimum de tours nécessaires pour l'atteindre à partir de la position initiale.
    
    Une version correcte de la fonction `configurations_voisines` de la question précédente est déjà chargée en mémoire.
    
    Écrire cette fonction.
    
    ???+ example "Exemples"

        ```pycon title=""
        >>> couts(4)
        {(0, 1, 2): 0, (0, 1, 3): 1, (0, 2, 3): 1, (1, 2, 3): 2}
        >>> couts(5)
        {(0, 1, 2): 0, (0, 1, 3): 1, (1, 2, 4): 1, (0, 2, 3): 1, (0, 1, 4): 2, (1, 2, 3): 2, (0, 2, 4): 2, (1, 3, 4): 2, (2, 3, 4): 2, (0, 3, 4): 2}
        ```
    
    ??? tip "Aide"
    
        On peut modéliser le problème à l'aide d'un graphe dans lequel les sommets sont les différentes configurations possibles. Deux configurations voisines sont reliées par une arête.
        
         On fournit ci-dessous deux représentations de la situation pour $n=5$.
        
        === "Parcours simplifié"
        
            Le graphe comportant toutes les arêtes est complexe. On en a représenté ici une version simplifiée comportant tous les sommets mais en supprimant de nombreuses arêtes de façon à ce qu'il n'existe qu'un unique chemin entre la configuration initiale et chaque configuration accessible.[^1]
            
            [^1]: il s'agit d'un *arbre couvrant* du graphe. Il en existe d'autres dans lesquels les chemins entre deux configurations peuvent différer.
            
            ![Arbre couvrant](graphe_radial.svg){.center .autolight width=45%}
            
        === "Construction du parcours"
        
            Afin de construire le graphe précédent, on a pris soin de ne pas repasser par un sommet correspondant à une configuration déjà rencontrée (en pointillés ci-dessous).
            
            Plusieurs configurations accessibles ne sont pas représentées afin d'alléger la figure.
            
            ![Graphe](graphe.svg){.center .autolight width=35%}
        
        Ces représentations fournissent une approche permettant de résoudre le problème : le dictionnaire souhaité peut être obtenu à l'aide d'un parcours en largeur du graphe des configurations.
    
        On pourra utiliser une file et enfiler, à chaque étape du parcours, le couple `#!py (configuration, distance)` précisant la configuration à envisager et sa distance à la configuration initiale.
        
        La classe `#!py File` décrite ci-dessous est à ce titre déjà chargée dans l'éditeur.
        
        {{ remarque('classe_File') }}
        
    {{ IDE('exo_2') }}
