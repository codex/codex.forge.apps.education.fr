Une autre solution utilise une succession de listes, chacune correspondant à un coût et donc à une étape du parcours en largeur.

```python
def couts(n):
    les_couts = dict()
    depart = (0, 1, 2)
    cout = 0

    actuel = [depart]
    les_couts[depart] = cout
    while actuel != []:
        suivant = []
        cout += 1
        for pos_abc in actuel:
            for pos_voisine in positions_voisines(pos_abc, n):
                if pos_voisine not in les_couts:
                    suivant.append(pos_voisine)
                    les_couts[pos_voisine] = cout
        actuel = suivant
    return les_couts
```

Cette solution ne nécessite pas de file, elle est, sous certains aspects, plus simple à écrire pour un parcours en largeur.
