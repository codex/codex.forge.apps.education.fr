# --------- PYODIDE:env --------- #

def configurations_voisines(config, n):
    a, b, c = config
    pass



# --------- PYODIDE:code --------- #

def configurations_voisines(config, n):
    a, b, c = config
    ...


# --------- PYODIDE:corr --------- #

def configurations_voisines(config, n):
    a, b, c = config
    voisines = []

    # les décalages
    if a != 0:
        voisines.append((a - 1, b, c))
    if a + 1 != b:
        voisines.append((a + 1, b, c))
        voisines.append((a, b - 1, c))
    if b + 1 != c:
        voisines.append((a, b + 1, c))
        voisines.append((a, b, c - 1))
    if c + 1 != n:
        voisines.append((a, b, c + 1))

    # les sauts

    ## a saute au dessus de b ?
    i = 2*b - a
    if i < c:
        voisines.append((b, i, c))
    elif c < i < n:
        voisines.append((b, c, i))

    ## a saute au dessus de c ?
    i = 2*c - a
    if i < n:
        voisines.append((b, c, i))

    ## b saute au dessus de a ?
    i = 2*a - b
    if 0 <= i:
        voisines.append((i, a, c))

    ## b saute au dessus de c ?
    i = 2*c - b
    if i < n:
        voisines.append((a, c, i))

    ## c saute au dessus de a ?
    i = 2*a - c
    if 0 <= i:
        voisines.append((i, a, b))

    ## c saute au dessus de b ?
    i = 2*b - c
    if a < i:
        voisines.append((a, i, b))
    elif 0 <= i < a:
        voisines.append((i, a, b))

    return voisines



# --------- PYODIDE:tests --------- #

assert sorted(configurations_voisines((0, 1, 2), 10)) == [(0, 1, 3), (0, 2, 3), (1, 2, 4)]
assert sorted(configurations_voisines((7, 8, 9), 10)) == [(5, 7, 8), (6, 7, 9), (6, 8, 9)]

exemple_3 = [(0, 2, 7), (1, 2, 4), (1, 4, 7), (2, 3, 7), (2, 4, 6),
             (2, 4, 8), (2, 5, 7), (3, 4, 7), (4, 6, 7)]
assert sorted(configurations_voisines((2, 4, 7), 10)) == exemple_3



# --------- PYODIDE:secrets --------- #

assert sorted(configurations_voisines((1,5,6),10)) == [(0, 5, 6), (1, 4, 5), (1, 4, 6), (1, 5, 7), (1, 6, 7), (2, 5, 6), (5, 6, 9)]
assert sorted(configurations_voisines((1,5,6),7)) == [(0, 5, 6), (1, 4, 5), (1, 4, 6), (2, 5, 6)]
