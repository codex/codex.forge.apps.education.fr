# --------- PYODIDE:env --------- #


def configurations_voisines(config, n):
    a, b, c = config
    voisines = []

    # les décalages
    if a != 0:
        voisines.append((a - 1, b, c))
    if a + 1 != b:
        voisines.append((a + 1, b, c))
        voisines.append((a, b - 1, c))
    if b + 1 != c:
        voisines.append((a, b + 1, c))
        voisines.append((a, b, c - 1))
    if c + 1 != n:
        voisines.append((a, b, c + 1))

    # les sauts

    ## a saute au-dessus de b ?
    i = 2 * b - a
    if i < c:
        voisines.append((b, i, c))
    elif c < i < n:
        voisines.append((b, c, i))

    ## a saute au-dessus de c ?
    i = 2 * c - a
    if i < n:
        voisines.append((b, c, i))

    ## b saute au-dessus de a ?
    i = 2 * a - b
    if 0 <= i:
        voisines.append((i, a, c))

    ## b saute au-dessus de c ?
    i = 2 * c - b
    if i < n:
        voisines.append((a, c, i))

    ## c saute au-dessus de a ?
    i = 2 * a - c
    if 0 <= i:
        voisines.append((i, a, b))

    ## c saute au-dessus de b ?
    i = 2 * b - c
    if a < i:
        voisines.append((a, i, b))
    elif 0 <= i < a:
        voisines.append((i, a, b))

    return voisines


from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.appendleft(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.pop()

    def __str__(self):
        """Convertit la file en une chaîne de caractères"""
        return f"{list(self.valeurs)}"


# --------- PYODIDE:code --------- #


def couts(n):
    ...


# --------- PYODIDE:corr --------- #


def couts(n):
    les_couts = dict()
    a_traiter = File()
    depart = (0, 1, 2)
    cout = 0
    a_traiter.enfile((depart, cout))
    les_couts[depart] = cout
    while not a_traiter.est_vide():
        config, cout = a_traiter.defile()
        for config_voisine in configurations_voisines(config, n):
            if config_voisine not in les_couts:
                a_traiter.enfile((config_voisine, cout + 1))
                les_couts[config_voisine] = cout + 1
    return les_couts


# --------- PYODIDE:tests --------- #


couts_4 = {(0, 1, 2): 0, (0, 1, 3): 1, (0, 2, 3): 1, (1, 2, 3): 2}
assert couts(4) == couts_4

couts_5 = {
    (0, 1, 2): 0,
    (0, 1, 3): 1,
    (1, 2, 4): 1,
    (0, 2, 3): 1,
    (0, 1, 4): 2,
    (1, 2, 3): 2,
    (0, 2, 4): 2,
    (1, 3, 4): 2,
    (2, 3, 4): 2,
    (0, 3, 4): 2,
}
assert couts(5) == couts_5

# --------- PYODIDE:secrets --------- #


# Autres tests

ans_321 = [
    None,
    None,
    None,
    None,
    2,
    1,
    3,
    2,
    2,
    3,
    4,
    3,
    3,
    4,
    4,
    4,
    4,
    5,
    4,
    5,
    5,
    5,
    5,
    6,
    5,
    6,
    6,
    6,
    6,
    7,
    6,
    7,
]
ans_rev = [
    None,
    None,
    None,
    None,
    2,
    2,
    3,
    4,
    4,
    4,
    5,
    6,
    6,
    6,
    6,
    6,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    10,
    10,
    10,
    10,
    10,
    10,
]
ans_spe = [
    None,
    None,
    None,
    None,
    2,
    2,
    3,
    4,
    4,
    5,
    6,
    6,
    7,
    7,
    8,
    8,
    8,
    8,
    8,
    9,
    9,
    9,
    10,
    10,
    10,
    10,
    11,
    11,
    11,
    11,
    11,
    12,
]
ans_sgn = [
    None,
    None,
    None,
    None,
    172875785,
    555226122,
    377607429,
    278818256,
    493322622,
    387916690,
    96768486,
    154062349,
    962685022,
    449500256,
    881055518,
    778565674,
    483350325,
    491990007,
    488380183,
    926760179,
    49868489,
    428783480,
    807770126,
    426844936,
    301142405,
    16203712,
    858324645,
    282004027,
    803031692,
    692323159,
    554726008,
    544450587,
]

for n in range(4, 32):
    couts_n = couts(n)

    a, b, c = n // 3, n // 2, n - 1
    attendu = ans_321[n]
    assert couts_n[(a, b, c)] == attendu, f"Erreur avec {n=}, {a=}, {b=}, {c=}"

    a, b, c = n - 3, n - 2, n - 1
    attendu = ans_rev[n]
    assert couts_n[(a, b, c)] == attendu, f"Erreur avec {n=}, {a=}, {b=}, {c=}"

    a, b, c = n // 3, n - 2, n - 1
    attendu = ans_spe[n]
    assert couts_n[(a, b, c)] == attendu, f"Erreur avec {n=}, {a=}, {b=}, {c=}"

    Z = 1234567
    M = 10**9 + 7
    attendu = (
        sum(
            (Z + a) * (Z + 10 * b) * (Z + 100 * c) * (Z + 1000 * x)
            for ((a, b, c), x) in couts_n.items()
        )
        % M
    )
    assert ans_sgn[n] == attendu, f"Erreur avec {n=}"
