# --- PYODIDE:env --- #
import p5
from random import choice

N = 10
dim_case = 50
w, h = (N + 2) * dim_case, 4 * dim_case
config = [0, 1, 2]
TAILLE_MOUTON = 36
TAILLE_TEXTE = 26
FPS = 60
DELAI_CLIC = FPS // 10


def configurations_voisines(config, n):
    a, b, c = config
    voisines = []

    # les décalages
    if a != 0:
        voisines.append((a - 1, b, c))
    if a + 1 != b:
        voisines.append((a + 1, b, c))
        voisines.append((a, b - 1, c))
    if b + 1 != c:
        voisines.append((a, b + 1, c))
        voisines.append((a, b, c - 1))
    if c + 1 != n:
        voisines.append((a, b, c + 1))

    # les sauts

    ## a saute sur b ?
    i = 2 * b - a
    if i < c:
        voisines.append((b, i, c))
    elif c < i < n:
        voisines.append((b, c, i))

    ## a saute sur c ?
    i = 2 * c - a
    if i < n:
        voisines.append((b, c, i))

    ## b saute sur a ?
    i = 2 * a - b
    if 0 <= i:
        voisines.append((i, a, c))

    ## b saute sur c ?
    i = 2 * c - b
    if i < n:
        voisines.append((a, c, i))

    ## c saute sur a ?
    i = 2 * a - c
    if 0 <= i:
        voisines.append((i, a, b))

    ## c saute sur b ?
    i = 2 * b - c
    if a < i:
        voisines.append((a, i, b))
    elif 0 <= i < a:
        voisines.append((i, a, b))

    return voisines


class Animation(p5.Sketch):
    def __init__(self):
        self.configs = [(0, 1, 2)]
        self.premier = True
        self.indice = 0
        self.last_click = -1

    @p5.hook
    def setup(self):
        self.p5.createCanvas(w, h)
        self.p5.frameRate(FPS)
        self.p5.textFont("Courier New")
        self.p5.strokeWeight(4)
        self.p5.textSize(TAILLE_TEXTE)
        self.prec_x = dim_case - 0.05 * self.p5.textWidth("Précédent")
        self.prec_w = 1.1 * self.p5.textWidth("Précédent")
        self.suiv_x = (N + 1) * dim_case - 1.05 * self.p5.textWidth("Suivant")
        self.suiv_w = 1.1 * self.p5.textWidth("Suivant")
        self.btn_y = 3.9 * dim_case - TAILLE_TEXTE
        self.btn_h = 1.3 * TAILLE_TEXTE
        self.dessine()

    def dessine(self):
        self.p5.textAlign(p5.CENTER)
        self.p5.clear()
        self.p5.background(21, 149, 25, 128)

        # les cases
        self.p5.strokeWeight(4)
        self.p5.stroke(50)
        self.p5.fill(21, 149, 25, 128)
        for i in range(N):
            self.p5.square((i + 1) * dim_case, dim_case, dim_case)

        # les indices
        self.p5.fill(50)
        self.p5.strokeWeight(2)
        self.p5.textSize(0.9 * TAILLE_TEXTE)
        for i in range(N):
            self.p5.text(i, (i + 1.5) * dim_case, 0.9 * dim_case)

        # la configuration
        self.p5.textSize(TAILLE_TEXTE)
        self.p5.text(
            f"Configuration : {self.configs[self.indice]}",
            p5.width // 2,
            2.75 * dim_case,
        )

        # la flèche
        if not self.premier:
            precs = set(self.configs[self.indice - 1])
            suivs = set(self.configs[self.indice])
            communs = precs & suivs
            prec = (precs - communs).pop()
            suiv = (suivs - communs).pop()
            self.p5.strokeWeight(4)
            self.p5.stroke(50)
            self.p5.line(
                (prec + 1.5) * dim_case,
                0.5 * dim_case,
                (prec + 1.5) * dim_case,
                0.2 * dim_case,
            )
            self.p5.line(
                (prec + 1.5) * dim_case,
                0.2 * dim_case,
                (suiv + 1.5) * dim_case,
                0.2 * dim_case,
            )
            self.p5.line(
                (suiv + 1.5) * dim_case,
                0.5 * dim_case,
                (suiv + 1.5) * dim_case,
                0.2 * dim_case,
            )
            self.p5.line(
                (suiv + 1.5) * dim_case,
                0.5 * dim_case,
                (suiv + 1.4) * dim_case,
                0.45 * dim_case,
            )
            self.p5.line(
                (suiv + 1.5) * dim_case,
                0.5 * dim_case,
                (suiv + 1.6) * dim_case,
                0.45 * dim_case,
            )
            self.p5.textSize(TAILLE_MOUTON)
            self.p5.textAlign(self.p5.CENTER)
            self.p5.text("🐑", (prec + 1.5) * dim_case, 1.80 * dim_case)
            self.p5.strokeWeight(4)
            self.p5.stroke(50)
            self.p5.fill(21, 149, 25, 90)
            self.p5.square((prec + 1) * dim_case, dim_case, dim_case)

        # les boutons
        # le bouton "précédent"
        self.p5.textSize(TAILLE_TEXTE)
        if not self.premier:
            self.p5.stroke(50)
            self.p5.fill(50)
            self.p5.rect(
                self.prec_x,
                self.btn_y,
                self.prec_w,
                self.btn_h,
            )
            self.p5.fill(255)
            self.p5.textAlign(p5.LEFT)
            self.p5.text("Précédent", dim_case, 3.9 * dim_case)
        # le bouton "suivant"
        self.p5.stroke(50)
        self.p5.fill(50)
        self.p5.rect(
            self.suiv_x,
            self.btn_y,
            self.suiv_w,
            self.btn_h,
        )
        self.p5.fill(255)
        self.p5.textAlign(p5.RIGHT)
        self.p5.text("Suivant", (N + 1) * dim_case, 3.9 * dim_case)

        # les moutons
        self.p5.textSize(TAILLE_MOUTON)
        self.p5.textAlign(p5.LEFT)
        for i in self.configs[self.indice]:
            self.p5.text("🐑", (i + 1) * dim_case, 1.80 * dim_case)

    @p5.hook
    def draw(self):
        pass

    @p5.hook
    def mousePressed(self, event):
        if self.p5.frameCount - self.last_click > DELAI_CLIC:
            self.last_click = self.p5.frameCount
            x, y = self.p5.mouseX, self.p5.mouseY
            if (
                self.prec_x <= x <= self.prec_x + self.prec_w
                and self.btn_y <= y <= self.btn_y + self.btn_h
                and not self.premier
            ):
                self.indice -= 1
                self.premier = self.indice == 0
            elif (
                self.suiv_x <= x <= self.suiv_x + self.suiv_w
                and self.btn_y <= y <= self.btn_y + self.btn_h
            ):
                self.premier = False
                self.indice += 1
                if self.indice == len(self.configs):
                    self.configs.append(
                        choice(configurations_voisines(self.configs[-1], N))
                    )
            self.dessine()


Animation().run(target="animation")
