

# --------- PYODIDE:code --------- #

def pascal(n):
    triangle = [[1]]
    for k in range(1, ...):
        ligne_k = [...]
        for i in range(1, k):
            ligne_k.append(triangle[...][i - 1] + triangle[...][...])
        ligne_k.append(...)
        triangle.append(ligne_k)
    return triangle

# --------- PYODIDE:corr --------- #

def pascal(n):
    triangle = [[1]]
    for k in range(1, n + 1):
        ligne_k = [1]
        for i in range(1, k):
            ligne_k.append(triangle[k - 1][i - 1] + triangle[k - 1][i])
        ligne_k.append(1)
        triangle.append(ligne_k)
    return triangle

# --------- PYODIDE:tests --------- #

assert pascal(4) == [
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
]


assert pascal(5) == [
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
    [1, 5, 10, 10, 5, 1],
]

# --------- PYODIDE:secrets --------- #

# tests
assert pascal(4) == [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1]]
assert pascal(5) == [
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
    [1, 5, 10, 10, 5, 1],
]

# autres tests

assert pascal(0) == [[1]]
assert pascal(1) == [[1], [1, 1]]
assert pascal(10) == [
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
    [1, 5, 10, 10, 5, 1],
    [1, 6, 15, 20, 15, 6, 1],
    [1, 7, 21, 35, 35, 21, 7, 1],
    [1, 8, 28, 56, 70, 56, 28, 8, 1],
    [1, 9, 36, 84, 126, 126, 84, 36, 9, 1],
    [1, 10, 45, 120, 210, 252, 210, 120, 45, 10, 1],
]
