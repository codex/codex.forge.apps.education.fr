---
author: Mireille Coilhac
hide:
    - navigation
    - toc
title: Nombre de répétitions
tags:
    - liste/tableau
    - ep1
difficulty: 130
maj: 01/02/2024
--- 

Écrire une fonction python appelée `repetitions` qui prend en paramètres un élément `cible` et un tableau de `valeurs` et renvoie le nombre de fois où l'élément `cible` apparait dans le tableau `valeurs`. C'est le nombre d'occurences de l'élément `cible` dans le tableau de `valeurs`.

??? danger "Méthode `count` interdite"
    
    Il est **interdit** d'utiliser la méthode `count`.

???+ example "Exemples"

    ```pycon title=""
    >>> repetitions(5, [2, 5, 3, 5, 6, 9, 5])
    3
    >>> repetitions("A", ["B", "A", "B", "A", "R"])
    2
    >>> repetitions(12, [1, 7, 21, 36, 44])
    0
    >>> repetitions(12, [])
    0
    ```

{{ remarque('assertion') }}

{{ IDE('exo', SANS='count') }}
