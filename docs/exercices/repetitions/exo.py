

# --------- PYODIDE:code --------- #

def repetitions(cible, valeurs):
    ...

# --------- PYODIDE:corr --------- #

def repetitions(cible, valeurs):
    effectif = 0
    for x in valeurs:
        if x == cible:
            effectif = effectif + 1
    return effectif

# --------- PYODIDE:tests --------- #

assert repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert repetitions("A", ["B", "A", "B", "A", "R"]) == 2
assert repetitions(12, [1, 7, 21, 36, 44]) == 0
assert repetitions(12, []) == 0

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
import random

def secret_458_repetitions(cible, valeurs):
    nb = 0
    for elt in valeurs:
        if elt == cible:
            nb = nb + 1
    return nb

for test in range(6):
    cible = random.randint(-10, 10)
    valeurs = [random.randint(-10, 10) for _ in range(random.randint(800, 1000))]
    attendu = secret_458_repetitions(cible, valeurs)
    assert repetitions(cible, valeurs) == attendu, f"Erreur dans le test caché n°{test}"