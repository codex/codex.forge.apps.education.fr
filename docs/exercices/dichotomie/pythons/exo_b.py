

# --------- PYODIDE:code --------- #

def est_present(nombres, cible):
    ...

# --------- PYODIDE:corr --------- #

def est_present(nombres, cible):
    debut = 0
    fin = len(nombres) - 1
    while debut <= fin:
        milieu = (fin + debut) // 2
        if cible < nombres[milieu]:
            fin = milieu - 1
        elif cible > nombres[milieu]:
            debut = milieu + 1
        else:
            return True
    return False

# --------- PYODIDE:tests --------- #

assert est_present([1, 2, 3, 4], 2) is True
assert est_present([1, 2, 3, 4], 1) is True
assert est_present([1, 2, 3, 4], 4) is True
assert est_present([1, 2, 3, 4], 5) is False
assert est_present([1, 2, 3, 4], 0) is False
assert est_present([1, 2, 5, 6], 4) is False
assert est_present([1], 1) is True
assert est_present([1], 0) is False
assert est_present([], 1) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
nombres = list(range(0, 101, 2))
for cible in range(101):
    attendu = (cible % 2 == 0)
    assert est_present(nombres, cible) is attendu, f"Erreur dans la recherche de {cible} dans {nombres}"

nombres = list(range(-100, 0, 2))
for cible in range(-100, 0):
    attendu = (cible % 2 == 0)
    assert est_present(nombres, cible) is attendu, f"Erreur dans la recherche de {cible} dans {nombres}"

nombres = list(range(-12, 10, 3))
for cible in range(-12, 10):
    attendu = (cible % 3 == 0)
    assert est_present(nombres, cible) is attendu, f"Erreur dans la recherche de {cible} dans {nombres}"

class Liste(list):
    def __init__(self, valeurs):
        self.lectures = 0
        super().__init__(valeurs)

    def __getitem__(self, i):
        self.lectures += 1
        return super().__getitem__(i)

    def __iter__(self):
        return Iterateur(self)

    def __contains__(self, x):
        raise Exception("Il est interdit d'utiliser le mot clé in")


class Iterateur:
    def __init__(self, valeurs):
        self.valeurs = valeurs
        self.indice = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.indice += 1
        if self.indice < len(self.valeurs):
            return self.valeurs[self.indice]
        else:
            raise StopIteration

# Nombre de lecture de valeurs inférieur à 0.5 * len(Liste)
from random import randrange, choice, sample
taille = randrange(10**3, 10**5)
nombres_base = sorted(sample(range(-2*taille, 2*taille), taille + 1))
absent = nombres_base.pop(randrange(0, taille))
present = choice(nombres_base)
nombres = Liste(nombres_base)
assert est_present(nombres, present) is True, f"Erreur avec une liste de {taille} éléments"
lectures = nombres.lectures
assert lectures <= taille // 2, f"Dans le cas d'une liste de {taille} éléments, vous avez accédé à {lectures} valeurs. Vous devez faire moins!"
nombres = Liste(nombres_base)
assert est_present(nombres, absent) is False, f"Erreur avec une liste de {taille} éléments"
lectures = nombres.lectures
assert lectures <= taille // 2, f"Dans le cas d'une liste de {taille} éléments, vous avez accédé à {lectures} valeurs. Vous devez faire moins!"