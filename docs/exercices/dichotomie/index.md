---
author:
    - Mireille Coilhac
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Recherche dichotomique (booléen)
difficulty: 220
tags:
    - à trous
    - dichotomie
    - liste/tableau
    - ep2
maj: 01/03/2024
---


{{ version_ep() }}

Compléter la fonction `est_present` :

* prenant en paramètre un tableau de nombres entiers triés dans l'ordre croissant `nombres` et une valeur `cible`,
  
* renvoyant `#!py True` si `cible` est une valeur de `nombres`, `#!py False` dans le cas contraire.


??? warning "Recherche dichotomique obligatoire"

    Vous utiliserez obligatoirement un algorithme de **recherche dichotomique**.
    
    Il est donc interdit dans les tests privés :
    
    * d'accéder à plus de la moitié des valeurs présentes dans `nombres` ;
  
    * d'utiliser le test `cible in nombres`.

???+ example "Exemples"

    ```pycon title=""
    >>> est_present([1, 2, 3, 4], 2)
    True
    >>> est_present([1, 2, 3, 4], 1)
    True
    >>> est_present([1, 2, 3, 4], 4)
    True
    >>> est_present([1, 2, 3, 4], 5)
    False
    >>> est_present([1, 2, 3, 4], 0)
    False
    >>> est_present([1], 1)
    True
    >>> est_present([1], 0)
    False
    >>> est_present([], 1)
    False
    ```

=== "Version vide"
    {{ IDE('./pythons/exo_b') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a') }}
