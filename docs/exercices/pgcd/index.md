---
author: Serge Bays
hide:
    - navigation
    - toc
title: Plus Grand Commun Diviseur
tags:
    - maths
    - récursivité
    - int
difficulty: 140
maj: 01/03/2024
---



Le PGCD, (ou plus grand commun diviseur), de deux entiers strictement positifs $a$ et $b$ est le plus grand entier positif qui divise $a$ et $b$.

* Le PGCD de $18$ et $24$ est $6$ ;
* Le PGCD de $20$ et $10$ est $10$ ;
* Le PGCD de $28$ et $15$ est $1$.

??? "Division euclidienne"

    Si $a$ et $b$ sont deux entiers strictement positifs, effectuer la division euclidienne de $a$ par $b$ consiste à trouver deux entiers positifs $q$ et  $r$ (appelés respectivement le *quotient* et le *reste*) tels que $a = bq + r$ et $0\leq r < b$.
    
    Avec **Python** le reste $r$ est la valeur de l'expression `#!py a % b`.


On demande dans cet exercice d'écrire la fonction `pgcd` qui prend en paramètres deux entiers strictement positifs `#!py a` et `#!py b` et renvoie le PGCD de ces deux nombres.

Deux versions, l'une itérative et l'autre récursive, sont proposées. 

???+ example "Exemples"

    ```pycon title=""
    >>> pgcd(18, 24)
    6
    >>> pgcd(20, 10)
    10
    >>> pgcd(28, 15)
    1
    ```

??? warning "Fonction `gcd` interdite"

    Le module `#!py math` propose une fonction calculant le PGCD de deux entiers.
    
    On s'interdit d'utiliser cette fonction dans cet exercice.

??? question "Version itérative"

    On utilise l'algorithme d'Euclide avec $a$ et $b$ deux entiers strictement positifs.

    1. On détermine le reste $r$ dans la division euclidienne de $a$ par $b$.

    2. Tant que $r$ n'est pas nul, $a$ prend la valeur de $b$, $b$ prend la valeur de $r$ et on calcule le nouveau reste $r$ dans la division euclidienne de $a$ par $b$.

    3. L'algorithme est terminé, le reste $r$ est nul, et le PGCD est $b$.
    
    {{ IDE('./pythons/exo_it') }}

??? question "Version récursive"

    Une autre manière de décrire l'algorithme d'Euclide est de dire que si $r$ est le reste dans la division euclidienne de $a$ par $b$ alors:

    * si $r=0$ alors le PGCD de $a$ et $b$ est $b$;
    * si $r\neq 0$ alors le PGCD de $a$ et $b$ est le PGCD de $b$ et $r$.
	
    
    {{ IDE('./pythons/exo_rec') }}
