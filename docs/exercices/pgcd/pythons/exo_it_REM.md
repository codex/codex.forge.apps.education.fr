On a considéré les entiers $a$ et $b$ strictement positifs. Dans le cas où l'un des deux est nul (et seulement l'un des deux), le PGCD est celui des deux nombres qui n'est pas nul.
En effet l'entier $n$ non nul est le plus grand entier qui divise $n$ et $0$.

Le programme peut alors s'écrire sous cette forme :

```python
def pgcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a
```
