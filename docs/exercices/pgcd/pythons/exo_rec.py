

# --------- PYODIDE:code --------- #

def pgcd(a, b):
    ...

# --------- PYODIDE:corr --------- #

def pgcd(a, b):
    r = a % b
    if r == 0:
        return b
    return pgcd(b, r)

# --------- PYODIDE:tests --------- #

assert pgcd(18, 24) == 6
assert pgcd(20, 10) == 10
assert pgcd(28, 15) == 1
assert pgcd(12, 12) == 12
assert pgcd(1, 6) == 1

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange
from math import gcd

for _ in range(20):
    a = randrange(1, 1000)
    b = randrange(1, 1000)
    attendu = gcd(a, b)
    assert pgcd(a, b) == attendu, f"Erreur avec {a = } et {b = }"