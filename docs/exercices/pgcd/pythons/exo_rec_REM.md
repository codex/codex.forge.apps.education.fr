Comme dans le programme itératif, on peut considérer le cas où l'un des deux nombres (et seulement l'un des deux) est nul.

Le programme récursif peut alors s'écrire sous cette forme:

```python
def pgcd(a, b):
    if b == 0:
        return a
    return pgcd(b, a % b)
```
