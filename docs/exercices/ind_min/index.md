---
license: "by-nc-sa"
author:
    - Mireille Coilhac
    - Franck Chambon
hide:
    - navigation
    - toc
title: Indice du minimum
tags:
    - liste/tableau
    - ep1
difficulty: 150
---


{{ version_ep() }}

Écrire une fonction `indice_min` qui prend en paramètre un tableau **non vide** de nombres et qui renvoie l'indice de la première occurrence du minimum de ce tableau.

Les tableaux seront représentés sous forme de liste Python.

???+ warning "Contraintes"

    On n'utilisera pas les fonctions `#!py min` et `#!py index`.

???+ example "Exemples"

    ```pycon title=""
    >>> indice_min([5])
    0
    >>> indice_min([2, 4, 1, 1])
    2
    >>> indice_min([5, 3, 2, 5, 2])
    2
    ```

=== "Version vide"
    {{ IDE('exo', SANS="min, .index") }}

=== "Version à compléter"
    {{ IDE('exo_trous', SANS="min, .index") }}

