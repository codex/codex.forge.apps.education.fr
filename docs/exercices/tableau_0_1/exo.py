

# --------- PYODIDE:code --------- #

def separe(zeros_et_uns):
    """place tous les 0 de zeros_et_uns à gauche et tous les 1 à droite"""
    debut = ...  # indice de début
    fin = ...    # indice de fin
    while debut ... fin:
        if zeros_et_uns[debut] == 0:
            debut = ...
        else:
            zeros_et_uns[debut] = ...
            ... = 1
            fin = ...

# --------- PYODIDE:corr --------- #

def separe(zeros_et_uns):
    """place tous les 0 de zeros_et_uns à gauche et tous les 1 à droite"""
    debut = 0  # indice de début
    fin = len(zeros_et_uns) - 1  # indice de fin
    while debut < fin:
        if zeros_et_uns[debut] == 0:
            debut = debut + 1
        else:
            zeros_et_uns[debut] = zeros_et_uns[fin]
            zeros_et_uns[fin] = 1
            fin = fin - 1

# --------- PYODIDE:tests --------- #

tab_1 = [0, 1, 0, 1, 0, 1, 0]
separe(tab_1)
assert tab_1 == [0, 0, 0, 0, 1, 1, 1]

tab_2 = [1, 1, 1, 0, 0, 0]
separe(tab_2)
assert tab_2 == [0, 0, 0, 1, 1, 1]

# --------- PYODIDE:secrets --------- #


# autres tests

tableau_vide = []
separe(tableau_vide)
assert tableau_vide == []

que_0 = [0] * 100
separe(que_0)
assert que_0 == [0] * 100

que_1 = [1] * 100
separe(que_1)
assert que_1 == [1] * 100

mono_0 = [0]
separe(mono_0)
assert mono_0 == [0]

mono_1 = [1]
separe(mono_1)
assert mono_1 == [1]

duo = [1, 0]
separe(duo)
assert duo == [0, 1]

un_seul_1_a = [1, 0, 0, 0, 0, 0]
separe(un_seul_1_a)
assert un_seul_1_a == [0, 0, 0, 0, 0, 1]

un_seul_1_b = [0, 0, 1, 0, 0, 0]
separe(un_seul_1_b)
assert un_seul_1_b == [0, 0, 0, 0, 0, 1]

un_seul_1_c = [0, 0, 0, 0, 0, 1]
separe(un_seul_1_c)
assert un_seul_1_c == [0, 0, 0, 0, 0, 1]

un_seul_0_a = [0, 1, 1, 1, 1, 1]
separe(un_seul_0_a)
assert un_seul_0_a == [0, 1, 1, 1, 1, 1]

un_seul_0_c = [1, 1, 1, 1, 1, 0]
separe(un_seul_0_c)
assert un_seul_0_c == [0, 1, 1, 1, 1, 1]