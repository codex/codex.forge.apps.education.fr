---
license: "by-nc-sa"
author:
    - project Euler 081
    - Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Somme minimale (1)
tags:
    - grille
    - programmation dynamique
---



Une grille carrée étant donnée, remplie d'entiers positifs, on souhaite traverser le carré avec une somme minimale des entiers rencontrés.

Pour cet exercice,

- on souhaite aller du coin supérieur gauche au coin inférieur droit,
- les seuls déplacements autorisés sont →, ↓

$$
\begin{pmatrix}
\mathbf{131} & 673 & 234 & 103 & 18 \\
\mathbf{201} & \mathbf{96} & \mathbf{342} & 965 & 150 \\
630 & 803 & \mathbf{746} & \mathbf{422} & 111 \\
537 & 699 & 497 & \mathbf{121} & 956 \\
805 & 732 & 524 & \mathbf{37} & \mathbf{331}\\
\end{pmatrix}
$$

Le chemin marqué en gras donne une somme de $2427$ qui est ici le minimum possible pour cet exercice.

Écrire une fonction telle que `somme_minimale_1(grille)` renvoie la somme minimale spécifiée plus haut. On garantit que la `grille` est carrée et remplie d'entiers positifs.

???+ example "Exemples"
    $$
    \begin{pmatrix}
    \mathbf{1} & \mathbf{5} & 9 \\
    10 & \mathbf{3} & 5 \\
    10 & \mathbf{2} & \mathbf{3}
    \end{pmatrix}
    $$

    ```pycon
    >>> grille = [[1, 5, 9], [10, 3, 5], [10, 2, 3]]
    >>> somme_minimale_1(grille)
    14
    ```

    $$
    \begin{pmatrix}
    \mathbf{131} & 673 & 234 & 103 & 18 \\
    \mathbf{201} & \mathbf{96} & \mathbf{342} & 965 & 150 \\
    630 & 803 & \mathbf{746} & \mathbf{422} & 111 \\
    537 & 699 & 497 & \mathbf{121} & 956 \\
    805 & 732 & 524 & \mathbf{37} & \mathbf{331}\\
    \end{pmatrix}
    $$

    ```pycon
    >>> grille = [
    ...     [131, 673, 234, 103, 18],
    ...     [201, 96, 342, 965, 150],
    ...     [630, 803, 746, 422, 111],
    ...     [537, 699, 497, 121, 956],
    ...     [805, 732, 524, 37, 331],
    ... ]
    >>> somme_minimale_1(grille)
    2427
    ```

{{ IDE('exo') }}
