

# --------- PYODIDE:code --------- #

def somme_minimale_1(grille):
    ...

# --------- PYODIDE:corr --------- #

def somme_minimale_1(grille):
    n = len(grille)
    sommes = []
    cumul = 0
    for x in grille[0]:
        cumul += x
        sommes.append(cumul)
    for i in range(1, n):
        sommes[0] = sommes[0] + grille[i][0]
        for j in range(1, n):
            sommes[j] = grille[i][j] + min(sommes[j], sommes[j - 1])
    return sommes[-1]

# --------- PYODIDE:tests --------- #

grille = [[1, 5, 9], [10, 3, 5], [10, 2, 3]]
assert somme_minimale_1(grille) == 14


grille = [
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331],
]
assert somme_minimale_1(grille) == 2427

# --------- PYODIDE:secrets --------- #


# autres tests


def secret_somme_minimale_1(grille):
    n = len(grille)
    sommes = []
    cumul = 0
    for x in grille[0]:
        cumul += x
        sommes.append(cumul)
    for i in range(1, n):
        ligne = grille[i]
        nouvelles_sommes = [sommes[0] + ligne[0]]
        for j in range(1, n):
            nouvelles_sommes.append(ligne[j] + min(sommes[j], nouvelles_sommes[-1]))
        sommes = nouvelles_sommes
    return sommes[-1]


from random import randrange

for n in [1, 2, 3, 4, 5, 6, 7, 8, 16, 32, 64]:
    grille = [
        [randrange(10**6) * (i + 3) * (j + 3) for j in range(n)] for i in range(n)
    ]
    attendu = secret_somme_minimale_1(grille)
    resultat = somme_minimale_1(grille)
    assert attendu == resultat