# --- exo, 1 --- #
def somme_chiffres(n):
    somme = ...
    while ...:
        somme = ...
        n = ...
    return ...


# --- vide, 1 | hdr, 1--- #
def somme_chiffres(n):
    ...


# --- corr, 1 | hdr, 2 --- #
def somme_chiffres(n):
    somme = 0
    while n > 0:
        somme = somme + n % 10
        n = n // 10
    return somme
# --- tests, 1 --- #
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
# --- secrets, 1 --- #
assert somme_chiffres(0) == 0
assert somme_chiffres(1) == 1
assert somme_chiffres(9) == 9
assert somme_chiffres(100000000000000000000000000) == 1
assert somme_chiffres(500000000000000000000000000) == 5
assert somme_chiffres(12345678901234567890) == 90
# --- rem, 1 --- #
""" # skip
Prenons un exemple : afin de calculer la somme des chiffres de $409$ on doit additionner le chiffre des unités $9$, celui des dizaines $0$ et celui des centaines $4$.

Le chiffre des unités vaut $9$. C'est le reste de la division de $409$ par $10$. On l'obtient en Python en faisant `409 % 10`.

Ce calcul effectué, on calcule le quotient de $409$ par $10$ : on obtient $40$. On l'obtient en Python en faisant `409 // 10`.

On peut alors recommencer la démarche (reste de la division suivie de quotient) jusqu'à ce que le quotient soit nul.

| Etape | Valeur de $n$ | Reste de la division par $10$ | Quotient de la division par $10$ |
| :---: | :-----------: | :---------------------------: | :------------------------------: |
|   1   |     $409$     |              $9$              |               $40$               |
|   2   |     $40$      |              $0$              |               $4$                |
|   3   |      $4$      |              $4$              |               $0$                |

La fonction `somme_chiffres` est donc construite autour d'une boucle non-bornée (`while`) se répétant tant que `n` est non nul.

A chaque itération on ajoute le reste à la variable `somme` et l'on remplace la valeur de `n` par son quotient par `#!py 10`.

On renvoie la valeur de `somme`.
"""  # skip
# --- exo, 2 --- #
def harshad(n):
    ...


# --- corr, 2 --- #
def harshad(n):
    return n % somme_chiffres(n) == 0
# --- tests, 2 --- #
assert harshad(18) is True
assert harshad(72) is True
assert harshad(11) is False
# --- secrets, 2 --- #
HARSHAD = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    12,
    18,
    20,
    21,
    24,
    27,
    30,
    36,
    40,
    42,
    45,
    48,
    50,
    54,
    60,
    63,
    70,
    72,
    80,
    81,
    84,
    90,
    100,
    102,
    108,
    110,
    111,
    112,
    114,
    117,
    120,
    126,
    132,
    133,
    135,
    140,
    144,
    150,
    152,
    153,
    156,
    162,
    171,
    180,
    190,
    192,
    195,
    198,
    200,
    201,
    204,
]
for n in range(1, 1 + HARSHAD[-1]):
    assert harshad(n) == (n in HARSHAD), f"Erreur avec {n}"
# --- rem, 2 --- #
""" # skip
La fonction `harshad` est simple : on se contente de tester si le reste de la division de `n` par `somme_chiffres(n)` est égal à `#!py 0`.
Si oui, le nombre est *harshad*, si non il ne l'est pas.
"""  # skip
