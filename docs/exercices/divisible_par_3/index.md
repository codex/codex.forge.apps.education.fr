---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Divisibilité par 3
tags:
    - à trous
    - maths
    - string
    - int
difficulty: 190
---

Un nombre $n$ est divisible par $3$ si le reste de la division euclidienne de $n$ par $3$ vaut $0$.

On se propose dans cet exercice de tester de trois manières différentes si un entier $n$ est divisible par $3$.

On demande donc, dans chaque question, d'écrire une version de la fonction `est_divisible_par_3` qui prend en paramètre un entier positif ou nul `n` et renvoie `#!py True` si `n` est divisible par $3$, `#!py False` dans le cas contraire.

???+ example "Exemples"

    ```python
    >>> divisible_par_3(0)
    True
    >>> divisible_par_3(1)
    False
    >>> divisible_par_3(3)
    True
    >>> divisible_par_3(9230)
    False
    >>> divisible_par_3(9231)
    True
    >>> divisible_par_3(9232)
    False
    ```

{{ remarque('assertion') }}


??? question "Version native"

    L'opérateur `#!py %` de Python renvoie le reste de la division euclidienne des deux nombres proposés.
    
    Ainsi, `#!py 25 % 3` est évalué à `#!py 1` car $25 = 8 \times 3 + \mathbf{1}$.
    
    
    ???+ warning "~~Contrainte~~"
    
        Dans cette version, on autorise l'utilisation de  `#!py n % 3`. 
    
    ??? tip "Défi : du premier coup !"
        
        Pouvez-vous compléter cette fonction en ajoutant une seule ligne ?

    {{ IDE('./pythons/exo_natif_vide') }}

??? question "Version « en chaîne »"

    Dans cette version, on se propose d'utiliser la règle de divisibilité enseignée dans les petites classes :
    
    !!! quote "Somme des chiffres"
    
        Un nombre $n$ est divisible par $3$ si et seulement si la somme de ses chiffres est divisible par $3$.
    
    On peut répéter cette règle à plusieurs reprises jusqu'à ce que la somme des chiffres soit strictement inférieure à $10$. Dans ce cas, les seuls entiers divisibles par $3$ sont $0$, $3$, $6$ ou $9$.
    
    Dans cette version, on parcourt les chiffres d'un entier après l'avoir converti en une chaîne de caractères comme illustré ci-dessous :
    
    ```python
    >>> n = 1234
    >>> chaine = str(n)
    >>> for chiffre in chaine:
    ...     print(chiffre)
    ...
    1
    2
    3
    4
    ```

    La fonction `#!py int` permet de faire l'opération inverse : 
    
    ```python
    >>> '1' + '2'
    '12'
    >>> int('1') + int('2')
    3
    ```
    
    ???+ warning "Contrainte"
    
        Dans cette version, on interdit l'utilisation de  `#!py n % 3`.
    
    === "Version vide"
        {{ IDE('./pythons/exo_str_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_str_trous') }}

??? question "Version « matheuse »"

    Dans cette version, on se propose d'utiliser une version modifiée de la règle de divisibilité enseignée dans les petites classes.
    
    !!! quote "Somme des chiffres"
    
        Un nombre $n$ est divisible par $3$ si et seulement si la somme de son nombre de dizaine et de son chiffre des unités est divisible par $3$.

    On répète cette règle jusqu'à ce que cette somme soit strictement inférieure à $10$. Dans ce cas, les seuls entiers divisibles par $3$ sont $0$, $3$, $6$ ou $9$.

    Appliquée à $5\,832$ la règle donne ($n \equiv r \pmod 3$ est la notation mathématique pour « *le reste de la division de $n$ par $3$ vaut $r$* ») :
        
    $\begin{align*} 
        5\,832   &\equiv 583 + 2 &\pmod 3 \\
            &\equiv 585 &\pmod 3 \\
            &\equiv 58 + 5 &\pmod 3 \\
            &\equiv 63 &\pmod 3 \\
            &\equiv 6 + 3 &\pmod 3 \\
            &\equiv 9 &\pmod 3 \\
    \end{align*}$
    
    Comme $9$ est inférieur à $10$ et est divisible par $3$, on peut affirmer que $5\,832$ est divisible par $3$.

    Il est possible de récupérer la somme envisagée en faisant `#!py n // 10 + n % 10` mais aussi, sans utiliser l'opérateur `%`, en faisant `#!py n - 9 * (n // 10)`[^1].
    
    [^1]: en effet `#!py n % 10` est aussi égal à `#!py n - 10 * (n // 10)`. On a donc `#!py (n // 10) + n % 10 = (n // 10) + n - 10 * (n // 10) = n - 9 * (n // 10)`.
    
    
    ```python
    >>> 5832 % 10 + 5832 // 10
    585
    >>> 5832 - 9 * (5832 // 10)
    585
    ```
        
    ???+ warning "Contraintes"
    
        Dans cette version :
        
        * on interdit l'utilisation de l'opérateur `#!py %` ;
        
        * on interdit de convertir l'entier étudié en une chaîne de caractère.

    === "Version vide"
        {{ IDE('./pythons/exo_int_vide', SANS = "str .format AST: f_string") }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_int_trous', SANS = "str .format AST: f_string") }}
