

# --------- PYODIDE:code --------- #

DECALAGE = {"N": (-1, 0), "S": ...}
MARQUE_LIBRE = 0
MARQUE_BUISSON = 1
MARQUE_VU = 2


def verifie(labyrinthe, chemin):
    i_entree, j_entree = 1, 1
    i_sortie, j_sortie = ..., ...

    i, j = i_entree, j_entree
    for direction in chemin:
        labyrinthe[i][j] = MARQUE_VU
        di, dj = DECALAGE[...]
        i, j = ..., ...
        if labyrinthe[i][j] != ...:
            return ...
    return (i, j) == ...

# --------- PYODIDE:corr --------- #

DECALAGE = {"N": (-1, 0), "S": (1, 0), "E": (0, 1), "O": (0, -1)}
MARQUE_LIBRE = 0
MARQUE_BUISSON = 1
MARQUE_VU = 2


def verifie(labyrinthe, chemin):
    i_entree, j_entree = 1, 1
    i_sortie, j_sortie = len(labyrinthe) - 2, len(labyrinthe[0]) - 2

    i, j = i_entree, j_entree
    for direction in chemin:
        labyrinthe[i][j] = MARQUE_VU
        di, dj = DECALAGE[direction]
        i, j = i + di, j + dj
        if labyrinthe[i][j] != MARQUE_LIBRE:
            return False
    return (i, j) == (i_sortie, j_sortie)

# --------- PYODIDE:tests --------- #

labyrinthe = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
    [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

chemin_1 = "EEEEES"
chemin_2 = "SSSSSEENNNEEEEEEESSOOOOSSSEEEESS"

labyrinthe_1 = [ligne.copy() for ligne in labyrinthe]
labyrinthe_2 = [ligne.copy() for ligne in labyrinthe]

assert verifie(labyrinthe_1, chemin_1) is False
assert verifie(labyrinthe_2, chemin_2) is True

# --------- PYODIDE:secrets --------- #

# tests

labyrinthe = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
    [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

chemin_0 = "EEEEEO"
chemin_1 = "EEEEES"
chemin_2 = "SSSSSEENNNEEEEEEESSOOOOSSSEEEESS"

labyrinthe_0 = [ligne.copy() for ligne in labyrinthe]
labyrinthe_1 = [ligne.copy() for ligne in labyrinthe]
labyrinthe_2 = [ligne.copy() for ligne in labyrinthe]

assert verifie(labyrinthe_0, chemin_0) is False, "retour sur ses pas"
assert verifie(labyrinthe_1, chemin_1) is False, "entrée dans les buissons"
assert verifie(labyrinthe_2, chemin_2) is True

# autres tests
import copy

# test minimaliste

lab2 = [
    [1, 1, 1],
    [1, 0, 1],
    [1, 1, 1],
]

lab = copy.deepcopy(lab2)
assert verifie(lab, "")

lab = copy.deepcopy(lab2)
assert verifie(lab, "N") is False

lab = copy.deepcopy(lab2)
assert verifie(lab, "S") is False

lab = copy.deepcopy(lab2)
assert verifie(lab, "E") is False

lab = copy.deepcopy(lab2)
assert verifie(lab, "O") is False


# test 4×3

lab3 = [
    [1, 1, 1],
    [1, 0, 1],
    [1, 0, 1],
    [1, 1, 1],
]

lab = copy.deepcopy(lab3)
assert verifie(lab, "S")

lab = copy.deepcopy(lab3)
assert verifie(lab, "SNS") is False

lab = copy.deepcopy(lab3)
assert verifie(lab, "N") is False
lab = copy.deepcopy(lab3)
assert verifie(lab, "E") is False
lab = copy.deepcopy(lab3)
assert verifie(lab, "O") is False


# test 3×4

lab3 = [
    [1, 1, 1, 1],
    [1, 0, 0, 1],
    [1, 1, 1, 1],
]

lab = copy.deepcopy(lab3)
assert verifie(lab, "E")

lab = copy.deepcopy(lab3)
assert verifie(lab, "EOE") is False

lab = copy.deepcopy(lab3)
assert verifie(lab, "N") is False
lab = copy.deepcopy(lab3)
assert verifie(lab, "S") is False
lab = copy.deepcopy(lab3)
assert verifie(lab, "O") is False
