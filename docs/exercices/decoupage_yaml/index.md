---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: YAML en dictionnaire
tags:
    - string
    - dictionnaire
    - à trous
difficulty: 210
maj: 01/02/2024
---


Le [YAML](https://fr.wikipedia.org/wiki/YAML) est un format de représentation de données simple permettant d'associer des valeurs à des clés. Par exemple :

```YAML
auteur: Léo et Rafaël
titre: YAML en dictionnaire
difficulté: 210
```

Dans cet exemple on peut observer trois clés associées à trois valeurs :

| `#!py   auteur`      | `#!py titre`                | `#!py difficulté` |
| :------------------- | :-------------------------- | :---------------- |
| `#!py Léo et Rafaël` | `#!py YAML en dictionnaire` | `#!py 210`        |


L'objectif de cet exercice est de traiter des lignes extraites d'un fichier YAML afin de créer le dictionnaire **Python** correspondant.

On fournit donc une liste de chaînes de caractères correspondant toutes à une ligne extraite d'un fichier YAML. On garantit que, dans chaque ligne, la clé et la valeur sont séparées par la chaîne `#!py ": "` et que ce séparateur n'apparaît qu'une seule fois dans la ligne. Chaque clé présente n'apparaît qu'une seule fois dans la liste.

La liste correspondant à l'exemple précédent est donc :

```python
yaml = [
  "auteur: Léo et Rafaël",
  "titre: YAML en dictionnaire",
  "difficulté: 210"
]
```

On souhaite à l'issue du traitement obtenir un dictionnaire `#!py {clé: valeur}` contenant tous les couples `#!py (clé, valeur)` présents dans la liste. Avec la liste précédente, on obtient :

```python
{
    "auteur": "Léo et Rafaël",
    "titre": "YAML en dictionnaire",
    "difficulté": 210,
}
```

Comme on peut le voir, **certaines valeurs sont des nombres**. On demande de les convertir au format `#!py int`. On garantit que ces nombres sont des entiers positifs ou nuls.

Compléter la fonction `yaml_en_dictionnaire`.

???+ example "Exemples"

    ```pycon title=""
    >>> yaml = ["auteur: Léo et Rafaël", "titre: YAML en dictionnaire", "difficulté: 210"]
    >>> yaml_en_dictionnaire(yaml)
    {'auteur': 'Léo et Rafaël', 'titre': 'YAML en dictionnaire', 'difficulté': 210}
    >>> yaml = ["auteur: x", "difficulté: 143", "classement: 3"]
    >>> yaml_en_dictionnaire(yaml)
    {'auteur': 'x', 'difficulté': 143, 'classement': 3}
    >>> yaml = []
    >>> yaml_en_dictionnaire(yaml)
    {}
    ```
    
??? tip "Découper une chaîne"
  
    Si l'on considère `#!py texte = "Léo et Victor et Maxence et Inès et Rafaël"`, l'instruction `#!py texte.split(" et ")` renverra `#!py ["Léo", "Victor", "Maxence", "Inès", "Rafaël"]`.
  
??? tip "Repérer les entiers positifs"

    Si l'on considère `#!py texte = "toto"`, l'instruction `#!py texte.isnumeric()` renverra `#!py False`. Par contre avec `#!py texte = "167"`, on obtient `#!py True`.

=== "Version vide"
    {{ IDE('./pythons/exo_b') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a') }}
