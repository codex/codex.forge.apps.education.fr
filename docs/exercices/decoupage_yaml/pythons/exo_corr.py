def yaml_en_dictionnaire(yaml):
    dico = dict()
    for ligne in yaml:
        cle, valeur = ligne.split(": ")
        if valeur.isnumeric():
            valeur = int(valeur)
        dico[cle] = valeur
    return dico
