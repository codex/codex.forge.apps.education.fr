On utilise dans le corrigé le *décompactage* qui permet d'affecter directement les valeurs de deux variables.
Ainsi, `cle, valeur = ligne.split(": ")` est équivalent à :

```python
portions = ligne.split(": ")
cle = portions[0]
valeur = portions[1]
```

Il faut par contre être certain que `ligne.split(": ")` va bien générer exactement deux *portions*.
