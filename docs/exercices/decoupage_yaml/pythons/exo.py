def yaml_en_dictionnaire(yaml):
    ...
    
# Tests
yaml = [
    "auteur: Léo et Rafaël",
    "titre: Créer un dictionnaire",
    "difficulté: 167"
]
attendu = {
    "auteur": "Léo et Rafaël",
    "titre": "Créer un dictionnaire",
    "difficulté": 167
}
assert yaml_en_dictionnaire(yaml) == attendu

yaml = []
attendu = {}
assert yaml_en_dictionnaire(yaml) == attendu

yaml = [
    "auteur: x",
    "difficulté: 143",
    "classement: 3"
]
attendu = {
    "auteur": "x",
    "difficulté": 143,
    "classement" : 3
}
assert yaml_en_dictionnaire(yaml) == attendu