

# --------- PYODIDE:code --------- #

def yaml_en_dictionnaire(yaml):
    ...

# --------- PYODIDE:corr --------- #

def yaml_en_dictionnaire(yaml):
    dico = dict()
    for ligne in yaml:
        cle, valeur = ligne.split(": ")
        if valeur.isnumeric():
            valeur = int(valeur)
        dico[cle] = valeur
    return dico

# --------- PYODIDE:tests --------- #

yaml = ["auteur: Léo et Rafaël", "titre: YAML en dictionnaire", "difficulté: 210"]
attendu = {
    "auteur": "Léo et Rafaël",
    "titre": "YAML en dictionnaire",
    "difficulté": 210
}
assert yaml_en_dictionnaire(yaml) == attendu

yaml = ["auteur: x", "difficulté: 143", "classement: 3"]
attendu = {"auteur": "x", "difficulté": 143, "classement": 3}
assert yaml_en_dictionnaire(yaml) == attendu

yaml = []
attendu = {}
assert yaml_en_dictionnaire(yaml) == attendu

# --------- PYODIDE:secrets --------- #


# tests secrets
yaml = ["tag: en travaux", "auteur: X", "titre: YAML"]

attendu = {"tag": "en travaux", "auteur": "X", "titre": "YAML"}
assert yaml_en_dictionnaire(yaml) == attendu

yaml = ["auteur: x", "difficulté: 143"]
attendu = {"auteur": "x", "difficulté": 143}
assert yaml_en_dictionnaire(yaml) == attendu

from random import sample, randrange, choice

possibles = {
    "auteur": ["L & R", "Riri", "Fifi", "Loulou"],
    "classement": [
        27,
        346,
        150,
        42,
        370,
    ],
    "tag": ["en travaux", "terminé", "à trous", "string", "dictionnaire", "liste"],
    "difficulté": ["facile", "moyen", "difficile", "très difficile"],
    "brouillon": ["faux", "vrai", "oui", "non"],
    "type": ["dictionnaire", "liste"],
    "mode": ["A", "B", "C", "D"],
    "niveau": ["Seconde", "Première", "Terminale"],
    "ordre": [
        27,
        346,
        150,
        42,
        370,
    ],
    "nb_ligne": [
        27,
        346,
        150,
        42,
        370,
    ],
}

for test in range(5):
    taille = randrange(1, len(possibles) + 1)
    cles = sample(list(possibles.keys()), taille)
    valeurs = [choice(possibles[cle]) for cle in cles]
    yaml = []
    attendu = {}
    for i in range(taille):
        ligne = cles[i] + ": " + str(valeurs[i])
        yaml.append(ligne)
        attendu[cles[i]] = valeurs[i]
    assert yaml_en_dictionnaire(yaml) == attendu