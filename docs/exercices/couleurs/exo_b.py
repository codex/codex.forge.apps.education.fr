# --------- PYODIDE:env --------- #

HEX_DEC = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "A": 10,
    "B": 11,
    "C": 12,
    "D": 13,
    "E": 14,
    "F": 15,
}


def hex_int(seizaine, unite):
    return HEX_DEC[seizaine] * 16 + HEX_DEC[unite]


# --------- PYODIDE:code --------- #

def html_vers_rvb(html):
    ...



# --------- PYODIDE:corr --------- #


def html_vers_rvb(html):
    return (
        hex_int(html[1], html[2]),
        hex_int(html[3], html[4]),
        hex_int(html[5], html[6]),
    )

# --------- PYODIDE:tests --------- #

assert html_vers_rvb("#C0392B") == (192, 57, 43), "Échec html_vers_rvb exemple 1"
assert html_vers_rvb("#00FF00") == (0, 255, 0), "Échec html_vers_rvb exemple 2"
assert html_vers_rvb("#000000") == (0, 0, 0), "Échec html_vers_rvb exemple 3"


# --------- PYODIDE:secrets --------- #


# tests supplémentaires
assert html_vers_rvb("#FFFFFF") == (
    255,
    255,
    255,
), "Échec html_vers_rvb autre exemple 1"
assert html_vers_rvb("#888888") == (
    136,
    136,
    136,
), "Échec html_vers_rvb autre exemple 2"
assert html_vers_rvb("#760017") == (118, 0, 23), "Échec html_vers_rvb autre exemple 3"
