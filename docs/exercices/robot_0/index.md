---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Programmer un robot (0)
tags:
    - string
difficulty: 230
maj: 18/02/2025
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :

    * « {{ lien_exo("Programmer un robot (0)", "robot_0") }} »,
    
    * « {{ lien_exo("Programmer un robot (1)", "robot_1") }} »,

    * « {{ lien_exo("Programmer un robot (2)", "robot_2") }} »,
    
    * « {{ lien_exo("Programmer un robot (3)", "robot_3") }} »,
    
    * « {{ lien_exo("Programmer un robot (4)", "robot_4") }} ».


On considère un robot capable d'effectuer trois actions :

* avancer d'un pas,

* tourner à droite,

* tourner à gauche.

Ces actions sont fournies au robot sous forme d'une chaîne de caractères :

* `#!py "A"` pour **a**vancer d'un pas,

* `#!py "D"` pour tourner à **d**roite,

* `#!py "G"` pour tourner à **g**auche.

Il est possible de répéter une action en faisant suivre celle-ci d'un nombre entier positif ou nul. Ainsi `#!py ["A", 12]` permet de faire avancer le robot de 12 pas.

On demande d'écrire la fonction `decoupe` qui prend en paramètre la chaîne de caractères contenant les instructions à effectuer ainsi que des caractères invalides et renvoie la liste de ces instructions élémentaires : les lettres `#!py "A"`, `#!py "D"` ou `#!py "G"` ou des nombre entiers positifs ou nuls.

Attention : 

* les nombres entiers de plus de un chiffre doivent être « regroupés » : `#!py ["A", 12]` et pas `#!py ["A", 1, 2]` ;

* les caractères invalides doivent être ignorés : `#!py decoupe("A toto 1e2")` doit renvoyer `#!py ["A", 12]`.

On garantit que chaque nombre entier est précédé d'une instruction valide. Il est toutefois possible que ces deux instructions soient séparées par des caractères à ignorer et que les différents chiffres composant un nombre soient aussi séparés par des caractères invalides.

???+ example "Exemples"

    ```pycon title=""
    >>> decoupe("A5G")
    ["A", 5, "G"]
    >>> decoupe("A5GA4")
    ["A", 5, "G", "A", 4]
    >>> decoupe("A toto 1e2")
    ["A", 12]
    ```


=== "Version vide"
    {{ IDE('exo_vide')}}
=== "Version à compléter"
    {{ IDE('exo_trous')}}

