# --------- PYODIDE:code --------- #
def decoupe(expression):
    instructions = []
    nombre = ""
    for c in expression:
        if c in "0...":
            nombre = ... + ...
        elif c in "A..":
            if nombre != "":
                instructions.append(...)
                nombre = ...
            instructions.append(...)
    if nombre != "":
        instructions.append(...)
    return ...
# --------- PYODIDE:corr --------- #
def decoupe(expression):
    instructions = []
    nombre = ""
    for c in expression:
        if c in "0123456789":
            nombre = nombre + c
        elif c in "AGD":
            if nombre != "":
                instructions.append(int(nombre))
                nombre = ""
            instructions.append(c)
    if nombre != "":
        instructions.append(int(nombre))
    return instructions
# --------- PYODIDE:tests --------- #
assert decoupe("A5G") == ["A", 5, "G"]
assert decoupe("A5GA4") == ["A", 5, "G", "A", 4]
assert decoupe("A toto 1e2") == ["A", 12]
# --------- PYODIDE:secrets --------- #
expression = "G12a3"
assert decoupe(expression) == ["G", 123], f"Erreur avec {expression = }"
expression = "aA" * 10 + "3 2"
assert decoupe(expression) == ["A"] * 10 + [32], f"Erreur avec {expression = }"
expression = "CeciEstUnePhrase"
assert decoupe(expression) == [], f"Erreur avec {expression = }"
