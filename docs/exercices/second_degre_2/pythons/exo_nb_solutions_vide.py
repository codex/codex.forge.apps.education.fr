# --------- PYODIDE:env --------- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    pass


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --------- PYODIDE:code --------- #
def nb_solutions(a, b, c): 
    ...


# --------- PYODIDE:corr --------- #
def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return 0
    elif d == 0:
        return 1
    else:
        return 2


# --------- PYODIDE:tests --------- #
assert nb_solutions(1, 2, 1) == 1
assert nb_solutions(-1, 2, 1) == 2
assert nb_solutions(1, -3, 1) == 2
assert nb_solutions(-1, 3, -3) == 0

# --------- PYODIDE:secrets --------- #
from random import randrange

nb_sols_ = lambda a, b, c: (
    0 if b * b - 4 * a * c < 0 else 1 if b * b - 4 * a * c == 0 else 2
)

for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = nb_sols_(a, b, c)
    assert nb_solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


