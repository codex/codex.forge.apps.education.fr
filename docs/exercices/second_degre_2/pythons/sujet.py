# --- hdr, delta_--- #
def delta(a, b, c):
    pass


def nb_solutions(a, b, c):
    pass


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --- vide, delta_--- #
def delta(a, b, c): 
    ...


# --- corr, delta_--- #
def delta(a, b, c):
    return b**2 - 4 * a * c


# --- tests, delta_--- #
assert delta(1, 2, 1) == 0
assert delta(-1, 2, 1) == 8
assert delta(1, -3, 1) == 5
assert delta(-1, 3, -3) == -3

# --- secrets, delta_--- #
from random import randrange

delta_ = lambda a, b, c: b * b - 4 * a * c

for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = delta_(a, b, c)
    assert delta(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


# --- hdr, nb_solutions_--- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    pass


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --- vide, nb_solutions_--- #
def nb_solutions(a, b, c): 
    ...


# --- exo, nb_solutions_--- #
def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return ...
    ...  # plusieurs lignes possibles


# --- corr, nb_solutions_--- #
def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return 0
    elif d == 0:
        return 1
    else:
        return 2


# --- tests, nb_solutions_--- #
assert nb_solutions(1, 2, 1) == 1
assert nb_solutions(-1, 2, 1) == 2
assert nb_solutions(1, -3, 1) == 2
assert nb_solutions(-1, 3, -3) == 0

# --- secrets, nb_solutions_--- #
from random import randrange

nb_sols_ = lambda a, b, c: (
    0 if b * b - 4 * a * c < 0 else 1 if b * b - 4 * a * c == 0 else 2
)

for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = nb_sols_(a, b, c)
    assert nb_solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


# --- hdr, signe_--- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return 0
    elif d == 0:
        return 1
    else:
        return 2


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --- vide, signe_--- #
def signe(a, b, c): 
    ...


# --- exo, signe_--- #
def signe(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return ...
        else:
            return ...
    ...  # plusieurs lignes possibles


# --- corr, signe_--- #
def signe(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return "+"
        else:
            return "-"
    elif d == 0:
        if a > 0:
            return "+0+"
        else:
            return "-0-"
    else:
        if a > 0:
            return "+0-0+"
        else:
            return "-0+0-"


# --- tests, signe_--- #
assert signe(1, 2, 1) == "+0+"
assert signe(-1, 2, 1) == "-0+0-"
assert signe(1, -3, 1) == "+0-0+"
assert signe(-1, 3, -3) == "-"

# --- secrets, signe_--- #
from random import randrange


def signe_(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return "+"
        else:
            return "-"
    elif d == 0:
        if a > 0:
            return "+0+"
        else:
            return "-0-"
    else:
        if a > 0:
            return "+0-0+"
        else:
            return "-0+0-"


for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = signe_(a, b, c)
    assert signe(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


# --- hdr, solutions_--- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return 0
    elif d == 0:
        return 1
    else:
        return 2


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --- vide, solutions_--- #
from math import sqrt


def solutions(a, b, c): 
    ...


# --- exo, solutions_--- #
from math import sqrt


def solutions(a, b, c):
    d = delta(a, b, c)
    if ...:
        return ...
    ...  # plusieurs lignes possibles


# --- corr, solutions_--- #
from math import sqrt


def solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return []
    elif d == 0:
        return [-b / (2 * a)]
    else:
        racine_d = sqrt(d)
        return [(-b - racine_d) / (2 * a), (-b + racine_d) / (2 * a)]


# --- tests, solutions_--- #
assert solutions(1, -5, 7) == []
assert solutions(1, 2, 1) == [-1]
assert solutions(1, -7, -18) == [-2, 9]
assert solutions(-1, 7, 18) == [9, -2]

# --- secrets, solutions_--- #
from random import randrange


def solutions_(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return []
    elif d == 0:
        return [-b / (2 * a)]
    else:
        racine_d = sqrt(d)
        return [(-b - racine_d) / (2 * a), (-b + racine_d) / (2 * a)]


for _ in range(5):
    x_1 = randrange(-10, 0)
    x_2 = randrange(2, 11)
    k = randrange(1, 5)
    a = 1 * k
    b = -(x_1 + x_2) * k
    c = x_1 * x_2 * k
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = -a
    b = -b
    c = -c
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = k
    b = (-2 * x_1) * k
    c = (x_1 * x_1) * k
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = abs(x_1) * k
    c = abs(x_2) * 5 * k
    b = int((4 * a * c)**0.5) - 1
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
