# --------- PYODIDE:env --------- #
def delta(a, b, c):
    pass


def nb_solutions(a, b, c):
    pass


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --------- PYODIDE:corr --------- #
def delta(a, b, c):
    return b**2 - 4 * a * c


# --------- PYODIDE:tests --------- #
assert delta(1, 2, 1) == 0
assert delta(-1, 2, 1) == 8
assert delta(1, -3, 1) == 5
assert delta(-1, 3, -3) == -3

# --------- PYODIDE:secrets --------- #
from random import randrange

delta_ = lambda a, b, c: b * b - 4 * a * c

for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = delta_(a, b, c)
    assert delta(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


