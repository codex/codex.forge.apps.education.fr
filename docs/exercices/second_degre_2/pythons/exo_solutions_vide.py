# --------- PYODIDE:env --------- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return 0
    elif d == 0:
        return 1
    else:
        return 2


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --------- PYODIDE:code --------- #
from math import sqrt


def solutions(a, b, c): 
    ...


# --------- PYODIDE:corr --------- #
from math import sqrt


def solutions(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return []
    elif d == 0:
        return [-b / (2 * a)]
    else:
        racine_d = sqrt(d)
        return [(-b - racine_d) / (2 * a), (-b + racine_d) / (2 * a)]


# --------- PYODIDE:tests --------- #
assert solutions(1, -5, 7) == []
assert solutions(1, 2, 1) == [-1]
assert solutions(1, -7, -18) == [-2, 9]
assert solutions(-1, 7, 18) == [9, -2]

# --------- PYODIDE:secrets --------- #
from random import randrange


def solutions_(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        return []
    elif d == 0:
        return [-b / (2 * a)]
    else:
        racine_d = sqrt(d)
        return [(-b - racine_d) / (2 * a), (-b + racine_d) / (2 * a)]


for _ in range(5):
    x_1 = randrange(-10, 0)
    x_2 = randrange(2, 11)
    k = randrange(1, 5)
    a = 1 * k
    b = -(x_1 + x_2) * k
    c = x_1 * x_2 * k
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = -a
    b = -b
    c = -c
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = k
    b = (-2 * x_1) * k
    c = (x_1 * x_1) * k
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
    a = abs(x_1) * k
    c = abs(x_2) * 5 * k
    b = int((4 * a * c)**0.5) - 1
    attendu = solutions_(a, b, c)
    assert solutions(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"
