# --------- PYODIDE:env --------- #
def delta(a, b, c):
    return b**2 - 4 * a * c


def nb_solutions(a, b, c):
    pass


def signe(a, b, c):
    pass


def solutions(a, b, c):
    pass


# --------- PYODIDE:code --------- #
def signe(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return ...
        else:
            return ...
    ...  # plusieurs lignes possibles


# --------- PYODIDE:corr --------- #
def signe(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return "+"
        else:
            return "-"
    elif d == 0:
        if a > 0:
            return "+0+"
        else:
            return "-0-"
    else:
        if a > 0:
            return "+0-0+"
        else:
            return "-0+0-"


# --------- PYODIDE:tests --------- #
assert signe(1, 2, 1) == "+0+"
assert signe(-1, 2, 1) == "-0+0-"
assert signe(1, -3, 1) == "+0-0+"
assert signe(-1, 3, -3) == "-"

# --------- PYODIDE:secrets --------- #
from random import randrange


def signe_(a, b, c):
    d = delta(a, b, c)
    if d < 0:
        if a > 0:
            return "+"
        else:
            return "-"
    elif d == 0:
        if a > 0:
            return "+0+"
        else:
            return "-0-"
    else:
        if a > 0:
            return "+0-0+"
        else:
            return "-0+0-"


for _ in range(10):
    a = randrange(-99, 100)
    b = randrange(-99, 100)
    c = randrange(-99, 100)
    attendu = signe_(a, b, c)
    assert signe(a, b, c) == attendu, f"Erreur avec {a = }, {b = } et {c = }"


