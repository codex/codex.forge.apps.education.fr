---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Autour du second degré (2)
tags:
    - maths
    - float
    - fonctions
difficulty: 90
maj: 11/11/2024
---

!!! note "Second degré (1)"

    Cette page regroupe différents exercices sur la résolution d'équations du second degré et l'étude du signe des fonctions polynômes du second degré.
    
    Il fait suite à {{ lien_exo("cette série d'exercices", "second_degre_1")}} qui aborde les fonctions du second degré sans s'intéresser à leurs racines.


On s'intéresse dans cet exercice aux équations polynomiales du second degré. On rappelle que ces équations peuvent s'écrire sous la forme :

$$ax^2 + bx + c = 0$$

où $a$, $b$ et $c$ sont des nombres réels avec $a$ non nul.

![Graphes](graphe.svg){.center .autolight width=60%}

Les exercices sont tous indépendants et peuvent être traités dans un ordre quelconque.

??? tip "Coefficients et solutions entiers"

    Dans ces exercices, afin de simplifier les tests, les coefficients $a$, $b$ et $c$ considérés ainsi que les solutions des équations sont des nombres entiers.


??? question "Calcul du discriminant"

    Écrire la fonction `#!py delta` qui prend en paramètres trois nombres `#!py a`, `#!py b` et `#!py c` (`#!py a` non nul) et renvoie la valeur du discriminant de l'équation :
    
    $$ax^2+bx+c=0$$
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> delta(1, 2, 1)
        0
        >>> delta(-1, 2, 1)
        8
        >>> delta(1, -3, 1)
        5
        >>> delta(-1, 3, -3)
        -3
        ```
    
    {{ IDE('./pythons/exo_delta_vide') }}

??? question "Nombre de solutions"

    Écrire la fonction `#!py nb_solutions` qui prend en paramètres trois nombres `#!py a`, `#!py b` et `#!py c` (`#!py a` non nul) et renvoie le nombre de solutions réelles de l'équation :
    
    $$ax^2+bx+c=0$$
    
    Vous pouvez utiliser la fonction `delta` de l'exercice précédent dont une version valide est importée dans cet éditeur.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> nb_solutions(1, 2, 1)
        1
        >>> nb_solutions(-1, 2, 1)
        2
        >>> nb_solutions(1, -3, 1)
        2
        >>> nb_solutions(-1, 3, -3)
        0
        ```
    === "Version vide"
        {{ IDE('./pythons/exo_nb_solutions_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_nb_solutions_trous') }}

??? question "Solutions"

    Écrire la fonction `#!py solutions` qui prend en paramètres trois nombres `#!py a`, `#!py b` et `#!py c` (`#!py a` non nul) et renvoie la liste Python contenant les solutions réelles de l'équation :
    
    $$ax^2+bx+c=0$$
    
    Vous pouvez utiliser la fonction `delta` du premier exercice dont une version valide est importée dans cet éditeur.
    
    On rappelle qu'il est possible avec Python de calculer la racine carré d'un nombre positif `#!py d` en faisant `#!py sqrt(d)`. La fonction `#!py sqrt` est déjà importée dans cet éditeur.
    
    ???+ warning "Ordre des solutions"
    
        Dans le cas où l'équation possède deux solutions, il est demandé de les renvoyer dans l'ordre suivant :
        
        $$\left[~ \frac{-b \mathbf{-}\sqrt{\Delta}}{2a} ~;~ \frac{-b \mathbf{+}\sqrt{\Delta}}{2a} ~\right]$$
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> solutions(1, -5, 7)
        []
        >>> solutions(1, 2, 1)
        [-1]
        >>> solutions(1, -7, -18)
        [-2, 9]
        >>> solutions(-1, 7, 18)
        [9, -2]
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_solutions_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_solutions_trous') }}

??? question "Signe"

    Écrire la fonction `#!py signe` qui prend en paramètres trois nombres `#!py a`, `#!py b` et `#!py c` (`#!py a` non nul) et renvoie la chaîne de caractères indiquant le signe de la fonction $x \mapsto ax^2+bx+c$ sur $\mathbb{R}$.
    
    La chaîne de caractères renvoyée contiendra :
    
    * le caractère `#!py "+"` si la fonction est strictement positive sur un certain intervalle,
    * le caractère `#!py "-"` si la fonction est strictement négative sur un certain intervalle,
    * le caractère `#!py "0"` si la fonction s'annule en une certaine valeur.
    
    Ainsi une fonction s'annulant deux fois et positive à l'extérieur de ses racines sera associée à la chaîne de caractères `#!py "+0-0+"`.
    
    Vous pouvez utiliser la fonction `delta` du premier exercice dont une version valide est importée dans cet éditeur.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> signe(1, 2, 1)
        '+0+'
        >>> signe(-1, 2, 1)
        '-0+0-'
        >>> signe(1, -3, 1)
        '+0-0+'
        >>> signe(-1, 3, -3)
        '-'
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_signe_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_signe_trous') }}
    
