Cette solution peut aussi s'écrire ainsi :

```python
def en_secondes(duree):
    if len(duree) == 2:
        minutes, secondes = duree
        return minutes * 60 + secondes
    else:
        h, m, s = duree
        return (h * 60 + m) * 60 + s
```

# Autre solution

En généralisant la solution précédente, on peut même se passer du test sur la longueur du p-uplet :

```python
def en_secondes(duree):
    total = 0
    for val in duree:
        total = total * 60 + val
    return total
```

Voici ce qui se passe lors de l'évaluation de `en_secondes((1, 25, 50))` :


| valeur de `val` | valeur de `total` | explications |
|-----------------:|-------------------:|:--------------|
|                 |   0               | valeur initiale |
|   1             |  1                | nombre total d'heures |
|   25            |  85               | nombre total de minutes |
|  50             | 5150               | nombre total de secondes |


Dans le cas d'un triplet $(h, m, s)$, on a :

$$\text{total minutes} = h \times 60 + m$$

Et donc :

$$\text{total secondes} = \text{total minutes} \times 60 + s = (h \times 60 + m) \times 60 + s$$

On retrouve l'expression de la deuxième solution proposée.

Dans le cas d'un couple $(m, s)$, on a :

$$\text{total secondes} =  m \times 60 + s$$

Cette technique s'appelle la **méthode de Horner**. Elle est aussi utilisée, entre autres, pour convertir un nombre exprimé dans une base $b$ vers la base $10$.
