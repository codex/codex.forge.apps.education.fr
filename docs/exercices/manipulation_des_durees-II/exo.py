

# --------- PYODIDE:code --------- #

def en_secondes(...):
    ...

# --------- PYODIDE:corr --------- #

def en_secondes(duree):
    if len(duree) == 2:
        minutes, secondes = duree
        return minutes * 60 + secondes
    else:
        h, m, s = duree
        return h * 3600 + m * 60 + s  # il y a 3600 secondes dans 1 heure

# --------- PYODIDE:tests --------- #

assert en_secondes((1, 25, 50)) == 5150
assert en_secondes((2, 20)) == 140

# --------- PYODIDE:secrets --------- #


# autres tests
assert en_secondes((1, 0, 0)) == 3600
assert en_secondes((1, 1, 1)) == 3661
assert en_secondes((0, 1, 0)) == 60
assert en_secondes((0, 0, 1)) == 1
assert en_secondes((1, 1)) == 61
assert en_secondes((1, 0)) == 60
assert en_secondes((0, 1)) == 1

assert en_secondes((0, 0, 0)) == 0
assert en_secondes((0, 0)) == 0