---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
difficulty: 85
title: Les durées (II)
tags:
    - tuple
---



Les durées peuvent être exprimées en secondes, en minutes-secondes, ou en heures-minutes-secondes.

Ainsi, la durée 6 h 34 min et 12 s peut être exprimée par :

```python
duree_sec = 21612  # en secondes
duree_min_sec = (360, 12)  # en minutes-secondes
duree_h_min_sec = (6, 34, 12)  # en heures-minutes-secondes
```

On souhaite créer une fonction permettant de convertir une durée en secondes, alors qu'elle est exprimée initialement sous l'une des deux dernières formes.


Écrire la fonction `en_secondes` qui prend en paramètre un p-uplet représentant une `durée` exprimée soit en heures, minutes et secondes sous la forme `(h, m, s)`, soit en minutes et secondes `(m, s)`, et qui renvoie le nombre total de secondes.

```pycon
>>> en_secondes((1, 25, 50))
5150
>>> en_secondes((2, 20))
140
```

??? tips "Astuce" 

    La fonction `len` permet de connaître le nombre d'éléments contenus dans un type construit (tableau, dictionnaire, p-uplet...).
    
    ```python
    >>> n_uplet = (5, 7, -3, 1, 6, 42, 17)
    >>> len(n_uplet)
    7
    ```

{{ IDE('exo') }}
