---
author:
    - Pierre Marquestaut
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Autour des files
difficulty: 20
tags:
    - file
    - structure linéaire
---

Les files sont des structures linéaires de données dont les éléments ne peuvent être ajoutés qu'en **queue**, et retirés qu'à partir de la **tête**.

![files](files2.svg){width=35% .center .autolight}

Ce fonctionnement est appelé **PEPS** (*Premier Entré, Premier Sorti*),  **FIFO** en anglais (*First In, First Out*).

C'est le principe des files d'attente : les personnes entrent dans une file et en ressortent dans le même ordre suivant lequel elles sont entrées.

Il est possible d'effectuer les deux actions suivantes sur des files :

* **enfiler** : ajouter un élément à la queue de la file ;
* **défiler** : retirer un élément de la tête de la file et renvoyer sa valeur.

On s'intéresse ici à une implantation rudimentaire des files en Python à l'aide de listes.
Ce type dispose de méthodes pour ajouter ou supprimer des éléments.

???+ note "Enfiler un élément"

    La méthode `#!py append` permet de mettre en œuvre l'action d'enfiler une valeur :

    ```python
    >>> file = [1, 2, 3]
    >>> file.append(4)  # enfile la valeur 4
    >>> file
    [1, 2, 3, 4]
    ```

???+ note "Défiler un élément"

    La méthode `#!py pop` et en particulier l'instruction `#!py file.pop(0)` permet de mettre en œuvre l'action de défiler un élément de la tête de la file et de renvoyer sa valeur :

    ```python
    >>> file = ["un", "deux", "trois", "quatre"]
    >>> file.pop(0)  # défile un élément
    'un'
    >>> file
    ['deux', 'trois', 'quatre']
    >>> file.pop(0)  # défile un élément
    'deux'
    >>> file
    ['trois', 'quatre']
    ```

Compléter le code ci-dessous :

{{ IDE('exo') }}
