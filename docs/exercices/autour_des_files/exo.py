

# --------- PYODIDE:code --------- #

premiere_file = [42, 15, 64]
deuxieme_file = [22, 57, 36, 17]
troisieme_file = [78, 12]

# Enfiler l'élément 21 dans la première file
...

# Défiler un élément de la première file
element_1 = ...

# Défiler un nouvel élément de la première file
element_2 = ...

# Défiler un élément de la première file
# et l'enfiler dans la deuxième file
...

# Défiler un élément de la deuxième file
# et l'enfiler dans la troisième file
...

# Défiler un élément de la troisième file
# et l'enfiler dans la première file
...

# --------- PYODIDE:corr --------- #

premiere_file = [42, 15, 64]
deuxieme_file = [22, 57, 36, 17]
troisieme_file = [78, 12]

# Enfiler l'élément 21 dans la première file
premiere_file.append(21)

# Défiler un élément de la première file
element_1 = premiere_file.pop(0)

# Défiler un nouvel élément de la première file
element_2 = premiere_file.pop(0)

# Défiler un élément de la première file
# et l'enfiler dans la deuxième file
element = premiere_file.pop(0)
deuxieme_file.append(element)

# Défiler un élément de la deuxième file
# et l'enfiler dans la troisième file
element = deuxieme_file.pop(0)
troisieme_file.append(element)

# Défiler un élément de la troisième file
# et l'enfiler dans la première file
element = troisieme_file.pop(0)
premiere_file.append(element)

# --------- PYODIDE:secrets --------- #

# tests
assert premiere_file == [21, 78]
assert deuxieme_file == [57, 36, 17, 64]
assert troisieme_file == [12, 22]
assert element_1 == 42
assert element_2 == 15