Les variables de type `#!py list` sont gérées en mémoires comme des tableaux dynamiques :

* ajouter ou retirer à la fin d'un tableau (respectivement grâce aux méthodes `#!py append` et `#!py pop`) s'effectue sur une zone contiguë en mémoire et se fait donc à coût constant.
* ajouter ou retirer un élément d'un tableau à tout autre endroit entraîne la recopie en mémoire de l'ensemble du tableau : le coût est donc linéaire.

Le type `#!py list` permet donc de créer facilement des files, mais rend les algorithmes peu efficaces.
