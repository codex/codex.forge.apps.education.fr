---
author:
    - Gilles Lassus
    - équipe e-nsi
hide:
    - navigation
    - toc
tags:
    - booléen
    - liste/tableau
    - ep1
title: Ou exclusif entre deux listes
difficulty: 190
---

L'opérateur « ou exclusif » entre deux bits renvoie 1 si le premier **ou** le second est égal à 1, **mais** pas les deux (exclusion du cas où les deux sont égaux à 1). Il est symbolisé par le caractère ⊕.  
Ainsi :

- 0 ⊕ 0 = 0
- 0 ⊕ 1 = 1
- 1 ⊕ 0 = 1
- 1 ⊕ 1 = 0

On représente ici une suite de bits par une liste contenant des 0 et des 1.

Exemples :

```python
liste_a = [1, 0, 1, 0, 1, 1, 0, 1]
liste_b = [0, 1, 1, 1, 0, 1, 0, 0]
liste_c = [1, 1, 0, 1]
liste_d = [0, 0, 1, 1]
```

Écrire la fonction `ou_exclusif` qui prend en paramètres deux listes non vides de même longueur constituées uniquement de 0 et de 1.  Cette fonction renvoie
une liste où l’élément situé à la position `i` est le résultat, par l’opérateur « ou exclusif », des éléments à la position `i` des listes passées en paramètres.

En considérant les quatre exemples ci-dessus, on obtient :

```pycon
>>> ou_exclusif(liste_a, liste_b)
[1, 1, 0, 1, 1, 0, 0, 1]
>>> ou_exclusif(liste_c, liste_d)
[1, 1, 1, 0]
```

!!! danger "Contrainte"

    Vous ne devez pas utiliser l'opérateur `^`de Python.
    
???+ question "Compléter ci-dessous"

    {{ IDE('exo') }}
    

??? tip "Astuce"

    Vous pourrez écrire une fonction `ou_exclusif_bits` qui prend en paramètres deux entiers égaux à 0 ou 1, et renvoie l'entier obtenu avec le ou exclusif appliqué à ces deux entiers.
    
    
