

# --------- PYODIDE:code --------- #

def ou_exclusif(liste_1, liste_2):
    ...

# --------- PYODIDE:corr --------- #

def ou_exclusif_bits(a, b):
    if a == b:
        return 0
    return 1


def ou_exclusif(liste_1, liste_2):
    taille = len(liste_1)
    return [ou_exclusif_bits(liste_1[i], liste_2[i]) for i in range(taille)]

# --------- PYODIDE:tests --------- #

liste_a = [1, 0, 1, 0, 1, 1, 0, 1]
liste_b = [0, 1, 1, 1, 0, 1, 0, 0]
liste_c = [1, 1, 0, 1]
liste_d = [0, 0, 1, 1]
assert ou_exclusif(liste_a, liste_b) == [1, 1, 0, 1, 1, 0, 0, 1]
assert ou_exclusif(liste_c, liste_d) == [1, 1, 1, 0]

# --------- PYODIDE:secrets --------- #


# Autres tests
from random import randint

liste_e = [randint(0, 1) for i in range(100)]
liste_f = [randint(0, 1) for i in range(100)]
resultat_attendu = [(liste_e[i] + liste_f[i]) % 2 for i in range(100)]
assert ou_exclusif(liste_e, liste_f) == resultat_attendu