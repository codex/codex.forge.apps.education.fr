!!! info "Autres possibilités"

    👉 On n'est pas obligé, comme le suggère l'énoncé, de créer une fonction "ou exclusif" entre deux bits, que nous avons nommée dans cette correction `ou_exclusif_bits`.
    

!!! info "Version avec `==`"

    ```python
    def ou_exclusif(liste_1, liste_2):
        liste = []
        for i in range(len(liste_1)):
            if liste_1[i] == liste_2[i]:
                liste.append(0)
            else:
                liste.append(1)
        return liste
    ```

!!! info "Version arithmétique"

    On remarque que :

    * $0+0=0$
    * $0+1=1$
    * $1+0=1$
    * $1+1=2$ et donc $(1+1) \% 2 = 0$
    
    ```python
    def ou_exclusif(liste_1, liste_2):
        return [(liste_1[i] + liste_2[i]) % 2 for i in range(len(liste_1))]
    ```

!!! info "Version fonctionnelle"
    
    On peut utiliser la fonction `zip` qui prend deux listes de même taille et renvoie la liste des couples formés les éléments de deux listes de même indice.

    ```python
    def ou_exclusif(liste_1, liste_2):
        return [ou_exclusif_bits(n_1[i], n_2[i]) for n_1, n_2 in zip(liste_1, liste_2)]
    ```


!!! info "Version interdite"

    L'opérateur « ou exclusif » entre deux bits existe en Python : `^`.
    Il était interdit dans cet exercice.

    ```pycon
    >>> 0 ^ 0
    0
    >>> 0 ^ 1
    1
    >>> 1 ^ 0
    1
    >>> 1 ^ 1
    0
    ```

    On aurait donc pu écrire de façon plus concise : 

    ```python
    def ou_exclusif(liste_1, liste_2):
        return [(liste_1[i] ^ liste_2[i]) for i in range(len(liste_1))]
    ```

    ??? note pliée "Approfondissement : l'opérateur `^` de Python opère bit à bit"

        Par exemple :  
        
        * $7_{10}= 111_2$  
        * $9_{10}=1001_2$

        Réalisons le xor bit à bit : 

        | | 0|1|1|1|
        |:--:|:--:|:--:|:--:|:--:|
        |**xor**|**1**|**0**|**0**|**1**|
        |**=**|**1**|**1**|**1**|**0**|

        Or $1110_2=14_{10}$

        ```pycon
        >>> 7 ^ 9
        14
        ```
        
