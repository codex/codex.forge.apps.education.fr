---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Dessiner des arbres
difficulty: 250
tags:
    - récursivité
maj: 28/01/2025
---

{{ remarque('tortue') }}

On souhaite dessiner des arbres en utilisant le module `#!py turtle`.

![Arbre](images/arbre1.svg){ .autolight .center}

Cet arbre a une structure **récursive** : les sous-arbres de droite et de gauche ont, à un niveau de profondeur près, la même forme que l'arbre entier.

<center>
![Sous-arbre droit ](images/arbre1_sad.svg){ .autolight}
![Sous-arbre gauche](images/arbre1_sag.svg){ .autolight}
</center>

Cette observation étant faite, on peut définir un algorithme de construction :

1. Tracer un trait de la longueur souhaitée ;
2. Si cette longueur est suffisamment grande :
   
   * Prendre note de la position actuelle de la tortue et de la direction dans laquelle elle pointe ;
   * Tourner la tortue de l'angle souhaité vers la droite et dessiner un sous-arbre ;
   * Revenir au point et à la direction notés à l'étape 2 ;
   * Tourner la tortue de l'angle souhaité vers la gauche et dessiner un sous-arbre.

On précise quelques informations :

* la longueur de départ est de `#!py 100` pixels ;
* à chaque étape, la longueur est multipliée par `#!py 0.75` ;
* l'angle de rotation du départ est de `#!py 60` degrés ;
* à chaque étape, l'angle de rotation est multiplié par `#!py 0.7` ;
* on ne construit un sous-arbre que si la longueur est strictement supérieure à `#!py 20` pixels.

Vous devez donc compléter le script ci-dessous permettant de construire l'arbre souhaité. La construction sera lancée par l'appel incomplet `#!py arbre(..., ...)` à la ligne `#!py 15`.

La fonction `modele` dessine à l'écran l'arbre attendu. Vous pouvez l'exécuter ou non !


=== "Version vide"

    {{ IDE('pythons/exo', MODE='delayed_reveal', MAX=10)}}

    {{ figure("figure1", div_class="py_mk_figure center", admo_title='Votre arbre') }}
=== "Version à compléter"

    {{ IDE('pythons/exo_trous', MODE='delayed_reveal', MAX=10)}}

    {{ figure("figure2", div_class="py_mk_figure center", admo_title='Votre arbre') }}
        