# --------- PYODIDE:env --------- #
from js import document
_prefixes_possibles = ["turtle", "tt"]
if "restart" in globals():
    restart()
else:
    for pref in _prefixes_possibles:
        if pref in globals():
            p = eval(pref)
            p.restart()
            break

def m_a_j():
    cible = "figure2"
    if "done" in globals():
        done()
        document.getElementById(cible).innerHTML = Screen().html
    else:
        for pref in _prefixes_possibles:
            if pref in globals():
                p = eval(pref)
                p.done()
                document.getElementById(cible).innerHTML = p.Screen().html
                break



def modele():
    def __arbre__(longueur, angle):
        forward(longueur)
        if longueur > 20:
            x, y = position()
            d = heading()
            right(angle)
            __arbre__(longueur * 0.75, angle * 0.7)
            penup()
            setheading(d)
            goto(x, y)
            pendown()
            left(angle)
            __arbre__(longueur * 0.75, angle * 0.7)
    speed(10)       # la tortue se déplace rapidement
    penup()         # le crayon est levé
    goto(0, -200)   # la tortue se déplace en bas de la figure
    setheading(90)  # la tortue pointe vers le haut
    pendown()       # le crayon est baissé
    pencolor('green')
    __arbre__(100, 60)
    pencolor('var(--md-admonition-fg-color)')
    penup()         # le crayon est levé
    goto(0, -200)   # la tortue se déplace en bas de la figure
    setheading(90)  # la tortue pointe vers le haut
    pendown()       # le crayon est baissé
    
    
    
# --- PYODIDE:code --- #
from turtle import *

# Réglages initiaux
speed(10)       # la tortue se déplace rapidement
penup()         # le crayon est levé
goto(0, -200)   # la tortue se déplace en bas de la figure
setheading(90)  # la tortue pointe vers le haut
pendown()       # le crayon est baissé

def arbre(longueur, angle):
    forward(...)
    if longueur > ...:
        x, y = ...
        d = ...
        right(...)
        arbre(..., ...)
        penup()
        setheading(...)
        goto(..., ...)
        pendown()
        left(...)
        ...

# modele()  # Décommenter pour comparer votre figure avec le modèle

arbre(..., ...)
# --- PYODIDE:corr --- #
from turtle import *

# Réglages initiaux
speed(10)       # la tortue se déplace rapidement
penup()         # le crayon est levé
goto(0, -200)   # la tortue se déplace en bas de la figure
setheading(90)  # la tortue pointe vers le haut
pendown()       # le crayon est baissé

def arbre(longueur, angle):
    forward(longueur)
    if longueur > 20:
        x, y = position()
        d = heading()
        right(angle)
        arbre(longueur * 0.75, angle * 0.7)
        penup()
        setheading(d)
        goto(x, y)
        pendown()
        left(angle)
        arbre(longueur * 0.75, angle * 0.7)

arbre(100, 60)
# --- PYODIDE:post --- #
if 'post_async' in globals():
    await post_async()
if 'Screen' in globals():
    if Screen().html is None:
        forward(0)
    m_a_j()
elif "turtle" in globals():
    if turtle.Screen().html is None:
        turtle.forward(0)
    m_a_j()

# --------- PYODIDE:post_term --------- #
if any(pref in globals() for pref in _prefixes_possibles) and "m_a_j" in globals():
    m_a_j()

