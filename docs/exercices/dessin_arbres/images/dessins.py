import drawsvg as draw
from math import pi, cos, sin


def degres(x):
    return 180 * x / pi


def radians(x):
    return pi * x / 180


def forward(longueur, t=None):
    if t is None:
        t = tortue
    a = radians(t["h"])
    nv_x = t["x"] + longueur * cos(a)
    nv_y = t["y"] + longueur * sin(a)
    d.append(draw.Line(t["x"], t["y"], nv_x, nv_y, **ARGS))
    t["x"] = nv_x
    t["y"] = nv_y


def heading(t=None):
    if t is None:
        t = tortue
    return t["h"]


def position(t=None):
    if t is None:
        t = tortue
    return t["x"], t["y"]


def setheading(deg, t=None):
    if t is None:
        t = tortue
    t["h"] = deg


def penup():
    pass


def pendown():
    pass


def goto(x, y, t=None):
    if t is None:
        t = tortue
    t["x"] = x
    t["y"] = y


def arbre(longueur, angle):
    forward(longueur)
    if longueur > d_min:
        x, y = position()
        d = heading()
        setheading(d + angle)
        arbre(longueur * k, angle * ka)
        penup()
        goto(x, y)
        pendown()
        setheading(d - angle)
        arbre(longueur * k, angle * ka)


d = draw.Drawing(500, 400, origin="center")
ARGS = {
    "stroke": "black",
    "stroke_width": 1,
}
tortue = {"x": 0, "y": 150, "h": -90}
k = 0.75
ka = 0.7
d_min = 20

arbre(100, 60)
d.save_svg("arbre1.svg")
