# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app5 = App("grille1")

deplacement = [4, 3, 2]

app5.gauche(deplacement[2])
app5.haut(deplacement[1])
app5.droite(deplacement[0])

app5.afficher_deplacement(etapes=True, num_lignes=False)