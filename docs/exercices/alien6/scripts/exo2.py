# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app2 = App("figure2")


deplacements = [5, 4, 3]

app2.gauche(deplacements[2])
app2.haut(deplacements[1])
app2.droite(deplacements[2])
app2.bas(deplacements[0])

app2.dessiner_parcours() 