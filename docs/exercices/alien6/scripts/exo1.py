# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app1 = App("figure1")

deplacements = [5, 4, 3]

app1.gauche(deplacements[0])
app1.haut(deplacements[1])
app1.droite(deplacements[2])

app1.dessiner_parcours() 