# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app4 = App("figure4")


deplacements = [2, 1, 3, 3]

app4.bas(4)
app4.droite(3)
for i in range(4):
    app4.haut(deplacements[i])
    app4.gauche(2)

app4.dessiner_parcours() 