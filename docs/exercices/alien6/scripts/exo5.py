# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = "deplacements[0]" in __USER_CODE__
assert valide is True, "On doit utiliser les valeurs contenues dans le tableau"
app.centrer("figure5") 


# --------- PYODIDE:code --------- #
deplacements = [4, 3, 2]

gauche(deplacements[...])
haut(deplacements[...])
droite(deplacements[...])



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H6', 'E6', 'E10'])
    