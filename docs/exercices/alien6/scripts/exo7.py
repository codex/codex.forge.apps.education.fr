# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = ("deplacements[0]" in __USER_CODE__) and ("deplacements[3]" in __USER_CODE__)
assert valide is True, "On doit utiliser les valeurs contenues dans le tableau"
app.centrer("figure7") 


# --------- PYODIDE:code --------- #
deplacements = [1, 4, 3, 5]

droite(...)
for i in range(...) :
    bas(deplacements[...])
    gauche(deplacements[i])

haut(deplacements[...])



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H13', 'I13', 'I12', 'J12', 'J8', 'K8', 'K5', 'F5'])