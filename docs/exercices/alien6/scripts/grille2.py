# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app6 = App("grille2")

deplacement = [1,4,3,4,2 ]

app6.droite(deplacement[2])
app6.haut(deplacement[0])
app6.droite(deplacement[3])
app6.bas(deplacement[1])
app6.gauche(deplacement[4])

app6.afficher_deplacement(etapes=True, num_lignes=False)