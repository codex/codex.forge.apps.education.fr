# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app7 = App("grille3")

deplacements = [1, 4, 3, 5]

app7.droite(5)
for i in range(3) :
    app7.bas(deplacements[0])
    app7.gauche(deplacements[i])

app7.haut(deplacements[3])

app7.afficher_deplacement(etapes=True, num_lignes=False)