# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app3 = App("figure3")


deplacements = [5, 4, 3, 4]

i = 0
app3.haut(deplacements[i])
i = i + 2
app3.droite(deplacements[i])
i = i - 1
app3.bas(deplacements[i])
i = i + 2
app3.gauche(deplacements[i])

app3.dessiner_parcours() 