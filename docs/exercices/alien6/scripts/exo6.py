# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = "deplacements[0]" in __USER_CODE__
assert valide is True, "On doit utiliser les valeurs contenues dans le tableau"
app.centrer("figure6") 


# --------- PYODIDE:code --------- #
deplacements = [...]

droite(deplacements[2])
haut(deplacements[0])
droite(deplacements[3])
bas(deplacements[1])
gauche(deplacements[4])


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H11', 'G11', 'G15', 'K15', 'K13'])