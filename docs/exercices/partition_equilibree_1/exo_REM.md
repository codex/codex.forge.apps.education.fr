Comme indiquée l'algorithme glouton utilisé ici ne donne pas une réponse optimale dans tous les cas.

Une réponse optimale peut être obtenue dans tous les cas avec une méthode par programmation dynamique.

Il est possible d'utiliser la fonction native `sum`, comme dans le code ci-dessous:

```python
def partition(liste):
    liste.sort(reverse=True)
    liste_1 = []
    liste_2 = []
    for x in liste:
        if sum(liste_1) <= sum(liste_2):
            liste_1.append(x)
        else:
            liste_2.append(x)
    return liste_1, liste_2
```

Mais dans ce cas, le coût devient quadratique en fonction du nombre d'éléments alors qu'avec la solution proposée le coût est linéaire.