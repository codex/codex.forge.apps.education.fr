

# --------- PYODIDE:code --------- #

def partition(liste):
    liste.sort(reverse=True)
    ...

# --------- PYODIDE:corr --------- #

def partition(liste):
    liste.sort(reverse=True)
    liste_1 = []
    liste_2 = []
    somme_1 = 0
    somme_2 = 0
    for x in liste:
        if somme_1 <= somme_2:
            liste_1.append(x)
            somme_1 = somme_1 + x
        else:
            liste_2.append(x)
            somme_2 = somme_2 + x
    return liste_1, liste_2

# --------- PYODIDE:tests --------- #

ens = [10, 8, 5, 7, 9, 5]
ens1 = [10, 7, 5]
ens2 = [9, 8, 5]
assert partition(ens) == (ens1, ens2)
ens = [5, 4, 3, 3, 3]
ens1 = [5, 3]
ens2 = [4, 3, 3]
assert partition(ens) == (ens1, ens2)
ens = [5, 4]
ens1 = [5]
ens2 = [4]
assert partition(ens) == (ens1, ens2)

# --------- PYODIDE:secrets --------- #

ens = [1, 1, 5, 1, 1, 1]
ens1 = [5]
ens2 = [1, 1, 1, 1, 1]
assert partition(ens) == (ens1, ens2), f"Partition incorrecte {ens1} et {ens2}, pour {ens}"
ens = [20, 20, 50, 20, 30, 20]
ens1 = [50, 20, 20]
ens2 = [30, 20, 20]
assert partition(ens) == (ens1, ens2), f"Partition incorrecte {ens1} et {ens2}, pour {ens}"
