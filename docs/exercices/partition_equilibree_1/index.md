---
author: Serge Bays
hide:
    - navigation
    - toc
title: Partition équilibrée (1)
tags:
    - en travaux
    - glouton
difficulty: 250
maj: 07/04/2024
---



Si les éléments d'une liste de longueur $n$ sont répartis entre deux listes `liste_1` et `liste_2` de longueurs respectives $n_1$ et $n_2$, ($n_1+n_2=n$), on dit que
`liste_1` et `liste_2` réalisent une partition de la liste initiale. 
  
On dispose d'une liste de $n$ nombres entiers strictement positifs, et on souhaite répartir ces $n$ nombres en deux listes telles que les sommes respectives des éléments des deux listes soient les plus proches possibles.

Cette partition d'une liste en deux listes s'appelle une *partition équilibrée*.

Par exemple, avec la liste `#!py [10, 8, 5, 7, 9, 5]`, une partition équilibrée est obtenue avec les deux listes `#!py [10, 7, 5]` et `#!py [8, 9, 5]`.

Malgré sa facilité d'énonciation, ce problème est complexe. On utilise dans cet exercice un algorithme glouton pour le résoudre :

*  on trie la liste initiale `liste` par ordre décroissant ;

*  on crée deux listes vides `liste_1` et `liste_2` ;

*  on ajoute successivement chaque nombre de la liste triée `liste` à la liste `liste_1` ou `liste_2` en choisissant à chaque fois celle dont la somme des éléments est la plus petite. En cas d'égalité des sommes, on ajoute le nombre à la liste `liste_1`.

Cette approche gloutonne est **facile à mettre en œuvre** mais elle n'est **pas toujours optimale**. Elle peut en effet générer des listes dont les sommes sont *proches* mais pas *les plus proches possibles*. Pour obtenir une solution optimale dans tous les cas, on peut utiliser une méthode par programmation dynamique, (voir exercice 
{{ lien_exo("partition equilibrée 2", "partition_equilibree_2") }} )


??? example "1er exemple : solution optimale"

	On considère la liste `#!py [10, 8, 5, 7, 9, 5]`. La liste triée est `#!py [10, 9, 8, 7, 5, 5]`.
	
	Les insertions successives sont décrites ci-dessous :
	
	| Valeur insérée | `liste_1`         | `liste_2`        | Remarque                 |
	| :------------- | :---------------- | :--------------- | :----------------------- |
	| État initial   | `#!py []`         | `#!py []`        |                          |
	| `#!py 10`      | `#!py [10]`       | `#!py []`        | Insertion dans `liste_1` |
	| `#!py 9`       | `#!py [10]`       | `#!py [9]`       | Insertion dans `liste_2` |
	| `#!py 8`       | `#!py [10]`       | `#!py [9, 8]`    | Insertion dans `liste_2` |
	| `#!py 7`       | `#!py [10, 7]`    | `#!py [9, 8]`    | Insertion dans `liste_1` |
	| `#!py 5`       | `#!py [10, 7, 5]` | `#!py [9, 8]`    | Insertion dans `liste_1` |
	| `#!py 5`       | `#!py [10, 7, 5]` | `#!py [9, 8, 5]` | Insertion dans `liste_2` |

	Les deux listes `#!py [10, 7, 5]` et `#!py [9, 8, 5]` ont la même somme totale de `#!py 22`. La solution est donc optimale.

??? example "2nd exemple : solution non optimale"

	On considère désormais la liste `#!py [3, 3, 3, 4, 5]`. La liste triée est `#!py [5, 4, 3, 3, 3]`.
	
	Les insertions successives sont décrites ci-dessous :
	
	| Valeur insérée | `liste_1`     | `liste_2`        | Remarque                 |
	| :------------- | :------------ | :--------------- | :----------------------- |
	| État initial   | `#!py []`     | `#!py []`        |                          |
	| `#!py 5`       | `#!py [5]`    | `#!py []`        | Insertion dans `liste_1` |
	| `#!py 4`       | `#!py [5]`    | `#!py [4]`       | Insertion dans `liste_2` |
	| `#!py 3`       | `#!py [5]`    | `#!py [4, 3]`    | Insertion dans `liste_2` |
	| `#!py 3`       | `#!py [5, 3]` | `#!py [4, 3]`    | Insertion dans `liste_1` |
	| `#!py 3`       | `#!py [5, 3]` | `#!py [4, 3, 3]` | Insertion dans `liste_2` |

	Les deux listes `#!py [5, 3]` et `#!py [4, 3, 3]` ont des sommes différentes : `#!py 8` pour `liste_1` et `#!py 10` pour `liste_2`.

	Cette solution n'est pas optimale. La réponse optimale serait `#!py [5, 4]` et `#!py [3, 3, 3]` qui donnent des sommes égales à `#!py 9`.


Compléter le code de la fonction `partition` qui prend en paramètre une liste de nombres nommée `liste` et qui renvoie le tuple composé des deux listes `liste_1` et `liste_2` obtenues par l'algorithme glouton décrit plus haut.

La liste passée en paramètre est triée à l'aide de la méthode `sort`. La valeur du paramètre `reverse` est fixée à `True` afin d'effectuer un tri suivant l'ordre décroissant.


???+ example "Exemples"

	```pycon title=""
	>>> liste = [10, 8, 5, 7, 9, 5]
	>>> partition(liste)
	([10, 7, 5], [9, 8, 5])
	>>> liste = [5, 4, 3, 3, 3]
	>>> partition(liste)
	([5, 3], [4, 3, 3])
	>>> liste = [5, 4]
	>>> partition(liste)
	([5], [4])
	```

{{ IDE('exo') }}


