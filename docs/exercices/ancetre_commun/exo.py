

# --------- PYODIDE:code --------- #

def plus_petit_ancetre_commun(parents, noeud_a, noeud_b):
    ...

# --------- PYODIDE:corr --------- #

def ancetres(parents, noeud):
    ancetres_noeud = [noeud]
    while noeud is not None:
        noeud = parents[noeud]
        ancetres_noeud.append(noeud)
    return ancetres_noeud


def plus_petit_ancetre_commun(parents, noeud_a, noeud_b):
    ancetres_a = ancetres(parents, noeud_a)
    ancetres_b = ancetres(parents, noeud_b)

    while (
        (ancetres_a != []) and (ancetres_b != []) and (ancetres_a[-1] == ancetres_b[-1])
    ):
        ppac = ancetres_a.pop()
        ancetres_b.pop()
    return ppac

# --------- PYODIDE:tests --------- #

assert plus_petit_ancetre_commun([1, None], 0, 1) == 1
parents = [3, 4, 4, None, 3]
assert plus_petit_ancetre_commun(parents, 2, 1) == 4
assert plus_petit_ancetre_commun(parents, 2, 0) == 3
parents = [None, 0, 0, 0, 2, 2, 2, 5, 5, 6, 6, 9, 9]
assert plus_petit_ancetre_commun(parents, 8, 11) == 2
assert plus_petit_ancetre_commun(parents, 1, 10) == 0
assert plus_petit_ancetre_commun(parents, 12, 6) == 6

# --------- PYODIDE:secrets --------- #

# autres tests
def tests():

    N,A,B,D = 10, 50,50, 10

    # sécurité, à la louche
    assert A>D+N and B>D+N, "la configuration des tests est fausse"


    parents = [None] + list(range(A+B))
    for a in range(N):
        attendu = a
        resultat = plus_petit_ancetre_commun(parents, a, A+B-1 - a)
        assert resultat == attendu, "Erreur avec un arbre peigne"

    parents = [None] + [0] * A + [1] * B
    for a in range(N):

        attendu = 0
        resultat = plus_petit_ancetre_commun(parents, a+1, A-a-1)
        assert resultat == attendu, "Erreur avec un arbre très large et peu profond"
        resultat = plus_petit_ancetre_commun(parents, A-a, A+3+a)
        assert resultat == attendu, "Erreur avec un arbre très large et peu profond"

        attendu = 1
        resultat = plus_petit_ancetre_commun(parents, A+B-2*D+a, A+B-1-a)
        assert resultat == attendu, "Erreur avec un arbre très large et peu profond"
        resultat = plus_petit_ancetre_commun(parents, 1+a, A+B-1-a)
        attendu = 1 if a==0 else 0
        assert resultat == attendu, "Erreur avec un arbre très large et peu profond"
tests()
del tests