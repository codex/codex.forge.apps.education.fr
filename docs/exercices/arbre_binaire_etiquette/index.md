---
author: 
    - Serge Bays
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Arbre d'expression arithmétique
tags:
    - arbre binaire
    - arbre
    - récursivité
difficulty: 300
maj: 26/01/2025
---

On considère un arbre binaire et on associe à chaque nœud une *étiquette* (en anglais *label*) contenant un opérateur binaire[^1]. Les nœuds sont numérotés 0, 1, 2, ...

[^1]: un *opérateur binaire* est un opérateur agissant sur deux *opérandes*. L'addition est un opérateur binaire  : on additionne **deux** nombres. La racine carrée n'est pas un opérateur binaire : on calcule la racine carrée d'**un** nombre.

À un arbre binaire étiqueté ne contenant que des opérateurs, on peut associer un arbre étiqueté en ajoutant des opérandes comme ci-dessous.

![arbre binaire et arbre](ab1.svg){ width=50% .autolight .center}

L'arbre binaire à gauche correspond à l'expression: $((.+.)\times (. - (. / .)))$.

L'arbre à droite correspond à l'expression: $((a+b)\times (c - (d / e)))$.
Les opérandes $a$, $b$, $c$, $d$, $e$ peuvent êtres des variables ou des nombres.

La numérotation des nœuds permet de représenter les arbres avec deux dictionnaires :
 
* un dictionnaire des *liens* dans lequel les clés sont les numéros des nœuds internes. La valeur associée à une clé est la liste ordonnée donnant les numéros des deux enfants du nœud étudié ;
* un dictionnaires des *étiquettes* associant à chaque numéro de nœud de l'arbre, l'étiquette de celui-ci.

On remarquera que les nœuds étiquetés avec des opérandes **ne sont pas** des clés du premier dictionnaire.

???+ example "Exemple"

	L'arbre de la figure ci-dessus est représenté par :
	
	* le dictionnaire des liens `#!py arbre = {0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}` ;
	* le dictionnaire des étiquettes `#!py labels = {0: '*', 1: '+', 2: '-', 3: '/', 4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e'}`.

	Ce couple `arbre` et `labels` correspond à l'expression :

	$$((a+b)\times (c - (d / e)))$$
	
	qui est noté `#!py '((a+b)*(c-(d/e)))'` en machine.
	
	En modifiant la valeur des étiquettes en `#!py labels = {0: '+', 1: '-', 2: '/', 3: '*', 4: '0', 5: '1', 6: '2', 7: '3', 8: '4'}`, on obtient l'expression :

	$$((0-1)+(2 /(3 * 4)))$$
	
	qui est noté `#!py '((0-1)+(2/(3*4)))'` en machine.


Écrire une fonction récursive `expression` qui prend en paramètres :

* les deux dictionnaires représentant un arbre et ses étiquettes,
* un entier donnant le numéro de la racine.

Cette fonction renvoie l'expression parenthésée correspondant à l'expression arithmétique modélisée par les paramètres.

???+ example "Exemples"

	```pycon title="Opérande seul"
	>>> arbre = {}
	>>> labels = {0: '17'}
	>>> expression(arbre, labels, 0)
	'17'
	```
	
	```pycon title="Une opération"
	>>> arbre = {0: [1, 2]}
	>>> labels = {0: '+', 1: '3', 2: '5'}
	>>> expression(arbre, labels, 0)
	'(3+5)'
	```
	
	```pycon title="Expression complexe"
	>>> arbre = {0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}
	>>> labels = {0: '*', 1: '+', 2: '-', 3: '/', 4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e'}
	>>> expression(arbre, labels, 0)
	'((a+b)*(c-(d/e)))'
	>>> expression(arbre, labels, 1)
	'(a+b)'
	```

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}



