# --------- PYODIDE:code --------- #


def expression(arbre, labels, noeud):
    ...


# --------- PYODIDE:corr --------- #


def expression(arbre, labels, noeud):
    if noeud in arbre:
        exp_g = expression(arbre, labels, arbre[noeud][0])
        exp_d = expression(arbre, labels, arbre[noeud][1])
        return "(" + exp_g + labels[noeud] + exp_d + ")"
    else:
        return labels[noeud]


# --------- PYODIDE:tests --------- #
# Opérande seul
arbre = {}
labels = {0: "17"}
assert expression(arbre, labels, 0) == "17"
# Une opération
arbre = {0: [1, 2]}
labels = {0: "+", 1: "3", 2: "5"}
assert expression(arbre, labels, 0) == "(3+5)"
# Expression complexe
arbre = {0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}
labels = {0: "*", 1: "+", 2: "-", 3: "/", 4: "a", 5: "b", 6: "c", 7: "d", 8: "e"}
assert expression(arbre, labels, 0) == "((a+b)*(c-(d/e)))"
assert expression(arbre, labels, 1) == "(a+b)"

# --------- PYODIDE:secrets --------- #
arbre, labels, noeud = {}, {0: "5"}, 0
attendu = "5"
assert expression(arbre, labels, noeud) == attendu, f"Erreur avec {arbre=}, {labels=} et {noeud=}"
arbre = {0: [1, 2], 2: [3, 4], 4: [5, 6], 6: [7, 8]}
labels = {0: "*", 1: "1", 2: "*", 3: "2", 4: "*", 5: "3", 6: "*", 7: "4", 8: "5"}
attendu = "(1*(2*(3*(4*5))))"
noeud = 0
assert expression(arbre, labels, noeud) == attendu, f"Erreur avec {arbre=}, {labels=} et {noeud=}"
attendu = "1"
noeud = 1
assert expression(arbre, labels, noeud) == attendu, f"Erreur avec {arbre=}, {labels=} et {noeud=}"
attendu = "(2*(3*(4*5)))"
noeud = 2
assert expression(arbre, labels, noeud) == attendu, f"Erreur avec {arbre=}, {labels=} et {noeud=}"
