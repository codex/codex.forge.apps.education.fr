On remarquera que le parcours utilisé pour visiter les nœuds est un parcours *infixe* : pour chaque nœud interne, on traite le sous-arbre gauche du nœud, puis le nœud, puis le sous-arbre droit du nœud.

Le parenthésage de l'expression est correct si :

- il y a autant de parenthèses ouvrantes `(` que de parenthèses fermantes `)`;

- si $n$ est le nombre de parenthèses ouvrantes, pour tout $k$, $1\leq k \leq n$, la $k$-ième parenthèse ouvrante est placée avant la $k$-ième parenthèse fermante.

![parenthèses](ab3.svg){ width=20% .autolight .center}

Avec cet exemple, les parenthèses sont numérotées de 1 à 8. La suite des numéros des parenthèses ouvrantes, sur l'exemple 1-2-4-5, définit la forme de l'arbre binaire. Il y a 4 parenthèses ouvrantes (1, 2, 4, 5) donc 4 fermantes (3, 6, 7, 8).  Chaque parenthèse ouvrante de numéro 1, 2, 4 ou 5 est placée avant la parenthèse fermante correspondante de numéro 3, 6, 7, 8.
