

# --------- PYODIDE:code --------- #

def arbre_vers_chemin(arbre):
    ...

# --------- PYODIDE:corr --------- #

def arbre_vers_chemin(arbre):
    def parcours_prefixe(arbre, chemin):
        i_premier = 0
        i_dernier = len(arbre) - 1
        for i, sous_arbre in enumerate(arbre):
            if i == i_premier:
                chemin.append((1, 1))
            elif i == i_dernier:
                chemin.append((1, -1))
            else:
                chemin.append((2, 0))
            parcours_prefixe(sous_arbre, chemin)

    chemin = []
    parcours_prefixe(arbre, chemin)
    return chemin

# --------- PYODIDE:tests --------- #

assert arbre_vers_chemin([[[], []], [], []]) == [
    (1, 1),
    (1, 1),
    (1, -1),
    (2, 0),
    (1, -1),
]

assert arbre_vers_chemin([[], [[], []], []]) == [
    (1, 1),
    (2, 0),
    (1, 1),
    (1, -1),
    (1, -1),
]

assert arbre_vers_chemin([[], [[], [[], []], [], [], []], []]) == [
    (1, 1),
    (2, 0),
    (1, 1),
    (2, 0),
    (1, 1),
    (1, -1),
    (2, 0),
    (2, 0),
    (1, -1),
    (1, -1),
]

# --------- PYODIDE:secrets --------- #


# autres tests

assert arbre_vers_chemin([]) == []

assert arbre_vers_chemin([[], []]) == [(1, 1), (1, -1)]

assert arbre_vers_chemin([[], [], []]) == [(1, 1), (2, 0), (1, -1)]

assert arbre_vers_chemin([[[], [], []], [[], [], []]]) == [
    (1, 1),
    (1, 1),
    (2, 0),
    (1, -1),
    (1, -1),
    (1, 1),
    (2, 0),
    (1, -1),
]