

# --------- PYODIDE:code --------- #

def eratosthene(n):
    crible = [True] * (n + 1)
    crible[0] = ...
    if n > 0:
        crible[1] = ...
    for p in range(...):
        if crible[p] == ...:
            # p est premier
            for kp in range(2 * p, n + 1, p):
                crible[...] = ...
    return crible


def premiers_jusque(n):
    crible = eratosthene(...)
    premiers = ...
    return premiers

# --------- PYODIDE:corr --------- #

def eratosthene(n):
    crible = [True] * (n + 1)
    crible[0] = False
    if n > 0:
        crible[1] = False
    for p in range(n + 1):
        if crible[p] is True:
            # p est premier
            for kp in range(2 * p, n + 1, p):
                crible[kp] = False
    return crible


def premiers_jusque(n):
    crible = eratosthene(n)
    premiers = [p for p in range(n + 1) if crible[p]]
    return premiers

# --------- PYODIDE:tests --------- #

assert eratosthene(5) == [False, False, True, True, False, True]
assert eratosthene(6) == [False, False, True, True, False, True, False]
assert premiers_jusque(5) == [2, 3, 5]
assert premiers_jusque(6) == [2, 3, 5]
assert premiers_jusque(20) == [2, 3, 5, 7, 11, 13, 17, 19]

# --------- PYODIDE:secrets --------- #


# autres tests


def est_premier(n):
    if n < 2:
        return False
    for d in range(2, n):
        if n % d == 0:
            return False
    return True


for n in range(37):
    attendu = [est_premier(p) for p in range(n + 1)]
    assert eratosthene(n) == attendu, f"Erreur avec n = {n}"
for n in range(100):
    attendu = [p for p in range(n + 1) if est_premier(p)]
    assert premiers_jusque(n) == attendu, f"Erreur avec n = {n}"