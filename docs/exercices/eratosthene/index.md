---
license: "by-nc-sa"
author:
    - Nicolas Revéret
    - Franck Chambon
hide:
    - navigation
    - toc
title: Crible d'Ératosthène (1)
tags:
    - à trous
    - fonctions
    - maths
    - ep2
difficulty: 210
---



!!! info "Nombres premiers"

    - $0$ et $1$ ne sont pas des nombres premiers, par définition.
    - Pour un entier $n>1$, on dit que $n$ est nombre premier s'il ne possède **que deux** diviseurs entiers $1$ et $n$.

    Par exemple :

    - $2$ est premier ; $1×2$ est le seul produit d'entier égal à $2$.
    - $3$ est premier ; $1×3$ est le seul produit d'entier égal à $3$.
    - $4$ n'est pas premier ; il est aussi multiple de $2$, avec $2×2 = 4$.
    - $5$ est premier ; $1×5$ est le seul produit d'entier égal à $5$.

    **Propriété importante** : un entier $n>1$ est toujours multiple d'un nombre premier, parfois lui-même uniquement. C'est cette propriété que nous allons utiliser pour justifier le fonctionnement du crible d'Ératosthène.

    On va marquer les multiples des nombres premiers, le plus petit entier non marqué sera donc un nombre premier.

!!! abstract "Le crible d'Ératosthène"
    Un crible est une technique qui permet de répondre sur les caractéristiques d'entiers, **non pas un à la fois, mais sur plusieurs à la fois**. Il existe des cribles pour autre chose que les nombres premiers.
    
    Le crible d'Ératosthène permet de déterminer les nombres premiers jusqu'à un certain nombre $n$ fixé. La démarche avec un papier et un crayon est la suivante :

    - On écrit tous les entiers jusqu'à $n$.
    - On raye $0$ et $1$ qui ne sont pas premiers.
    - On répète jusqu'à avoir traité tous les nombres :
        - Le prochain nombre non traité est un nombre premier ; on l'entoure.
        - On raye tous les autres multiples de ce nombre.
    
    ![Crible d'Ératosthène](images/crible.gif){ width="50%" .center }

Avec Python, si l'on cherche les nombres premiers jusqu'à `n` :

- On construit un tableau de $n+1$ booléens `crible`, initialement tous égaux à `#!py True`.
- On modifie `#!py crible[0]` et `#!py crible[1]` à `#!py False` ; $0$ et $1$ ne sont pas premiers. Pour $1$, il faut vérifier avant que $n>0$.
- On parcourt ce tableau de gauche à droite. Pour chaque indice `p` :
    - Si `#!py crible[p]` vaut `#!py True` : le nombre $p$ est premier.
        - On donne la valeur `#!py False` à toutes les cellules de `crible` dont l'indice est un multiple de `p`, on commence avec `#!py 2*p`, puis `#!py 3*p` etc jusqu'à la fin du tableau.
    - Sinon, `#!py crible[p]` vaut `#!py False` : le nombre $p$ n'est pas premier. On n'effectue aucun changement sur le tableau.

**Utilisation** : On peut établir ensuite la liste des nombres premiers jusqu'à $n$ en filtrant les indices des cellules de `crible` valant `#!py True`.


!!! tip "Astuce"
    L'expression Python `#!py range(2*p, n + 1, p)` permet d'itérer sur tous les multiples de `p`, de `#!py 2*p` inclus à `n` inclus.


Compléter la fonction `eratosthene` :

- prenant en paramètre un entier `n` positif,
- renvoyant le tableau `crible` de taille $n+1$ contenant des booléens, `crible[p]` indique si `p` est premier.

Compléter la fonction `premiers_jusque` :

- prenant en paramètre un entier `n` positif,
- renvoyant la liste des nombres premiers jusqu'à $n$.

???+ example "Exemples"

    ```pycon title=""
    >>> eratosthene(5)
    [False, False, True, True, False, True]
    >>> eratosthene(6)
    [False, False, True, True, False, True, False]
    >>> premiers_jusque(5)
    [2, 3, 5]
    >>> premiers_jusque(6)
    [2, 3, 5]
    >>> premiers_jusque(20)
    [2, 3, 5, 7, 11, 13, 17, 19]
    ```

{{ IDE('exo') }}
