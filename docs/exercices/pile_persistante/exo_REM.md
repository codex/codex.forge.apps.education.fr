Les piles persistantes sont utilisées par certains algorithmes dont l'efficacité est liée à la connaissance de l'historique des différents états d'une pile.
