---
author: Serge Bays
hide:
    - navigation
    - toc
title: Pile persistante
tags:
    - pile
    - programmation orientée objet
difficulty: 210
maj: 24/11/2024
---

# Pile persistante

On veut écrire une classe pour gérer une pile à l'aide d'une liste chainée. On dispose d'une classe `Maillon` permettant la création d'un maillon de la chaine, celui-ci étant constitué d'une donnée `valeur` et d'une référence au maillon suivant de la chaine:

```python
class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant
```

Une pile non vide est représentée par des maillons avec un maillon de tête, le dernier ajouté à la pile avec la méthode `empile` et celui qu'on va enlever de la pile avec la méthode `depile`.

Le maillon de tête contient l'élément au sommet de la pile. Ci-dessous la représentation d'une pile dans laquelle on a empilé 9, puis 2, puis 8, puis 5:

![représentation d'une pile](pile.svg){ width=30% .autolight .center}

Afin d'obtenir une structure *persistante* les méthodes `empile` et `depile` créent et renvoient une nouvelle pile sans modifier la pile initiale. Ceci peut permettre de garder une trace des états successifs de la pile.

Principe de fonctionnement de la méthode `depile` qui prend en paramètre une pile 1 et renvoie une pile 2:

![représentation de la fonction depile](depile.svg){ width=30% .autolight .center}

Attention, la méthode `depile` ne renvoie pas l'élément dépilé.

Principe de fonctionnement de la méthode `empile` qui prend en paramètre une pile 1 et un élément, (ici le nombre 7), et renvoie une pile 2:

![représentation de la méthode empile](empile.svg){ width=36% .autolight .center}

On complète la classe `Pile` avec une méthode `sommet` qui permet de récupérer si c'est nécessaire le sommet d'une pile avant un dépilement.

Compléter la classe `Pile` et vérifier votre travail sur les exemples. On vous donne la méthode `__init__` qui initialise une pile, la méthode `__str__` qui permet un affichage des éléments de la pile (avec la fonction `print`), la méthode `est_vide` qui renvoie `True` si la pile est vide et `False` sinon. 

???+ example "Exemples"

    ```pycon title=""
    >>> p = Pile()  # une pile vide
    >>> p.est_vide()
    True
    >>> p = p.empile(9)
    >>> p.est_vide()
    False
    >>> p1 = p.empile(5)
    >>> p2 = p1.empile(8)
	>>> print(p1)
	5 -> 9
    >>> print(p2)
    8 -> 5 -> 9
    >>> p2.sommet()
    8
    >>> p3 = p2.depile()
    >>> print(p3)
    5 -> 9
    >>> print(p2)
    8 -> 5 -> 9
    >>> print(p2.depile())
    5 -> 9
    ```

{{ IDE('exo') }}


