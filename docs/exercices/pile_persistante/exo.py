

# --------- PYODIDE:code --------- #

class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        if self.suivant is None:
            return str(self.valeur)
        return str(self.valeur) + ' -> ' + str(self.suivant)

class Pile:
    def __init__(self, maillon=None):
        self.maillon = maillon  # None si la pile est vide

    def est_vide(self):
        return self.maillon is None
    
    def empile(self, elt):
        return ...

    def sommet(self):
        if not self.est_vide():
            return ...

    def depile(self):
        if not self.est_vide():
            return ...

    def __str__(self):
        return str(self.maillon)

# --------- PYODIDE:corr --------- #

class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        if self.suivant is None:
            return str(self.valeur)
        return str(self.valeur) + ' -> ' + str(self.suivant)

class Pile:
    def __init__(self, maillon=None):
        self.maillon = maillon  # None si la pile est vide

    def est_vide(self):
        return self.maillon is None
    
    def empile(self, elt):
        return Pile(Maillon(elt, self.maillon))

    def sommet(self):
        if not self.est_vide():
            return self.maillon.valeur

    def depile(self):
        if not self.est_vide():
            return Pile(self.maillon.suivant)

    def __str__(self):
        return str(self.maillon)

# --------- PYODIDE:tests --------- #

p = Pile()
assert p.est_vide()
p = p.empile(5)
assert not p.est_vide()
piles = {0: Pile()}
for i in range(1, 4):
    piles[i] = piles[i-1].empile(i)
assert piles[3].sommet() == 3
p = piles[3].depile()
assert p.sommet() == 2
assert piles[3].sommet() == 3

# --------- PYODIDE:secrets --------- #

# tests

p = Pile()
assert p.est_vide()
p = p.empile(5)
assert not p.est_vide()
piles = {0: Pile()}
for i in range(1, 4):
    piles[i] = piles[i-1].empile(i)
assert piles[3].sommet() == 3
p = piles[3].depile()
assert p.sommet() == 2
assert piles[3].sommet() == 3

# autres tests

p = Pile()
p = p.empile(1)
p = p.empile(2)
assert p.sommet() == 2
p1 = p.empile(3)
p2 = p1.empile(4)
assert p1.sommet() == 3
assert p2.sommet() == 4
p = p.empile(5)
p = p.depile()
assert p.sommet() == 2
p = p.depile()
p = p.depile()
assert p.est_vide() is True
piles = {0: Pile()}
for i in range(1, 10):
    piles[i] = piles[i-1].empile(i)
assert piles[9].sommet() == 9
p = piles[9].depile()
assert p.sommet() == 8
assert piles[9].sommet() == 9
