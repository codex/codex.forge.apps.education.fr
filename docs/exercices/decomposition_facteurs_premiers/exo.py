# --------- PYODIDE:code --------- #

premiers = [
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
    43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
]

def decomposition(n):
    ...

# --------- PYODIDE:corr --------- #

premiers = [
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
    43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
]

def decomposition(n):
    dico = {}
    p = 2
    i = 0
    while i < len(premiers) and p <= n:
        p = premiers[i]
        while n % p == 0:
            if p in dico:
                dico[p] = dico[p] + 1
            else:
                dico[p] = 1
            n = n // p
        i = i + 1
    return dico

# --------- PYODIDE:tests --------- #

assert decomposition(36) == {2: 2, 3: 2}, "Erreur sur la décomposition de 36"
assert decomposition(64) == {2: 6}, "Erreur sur la décomposition de 64"
assert decomposition(30) == {2: 1, 3: 1, 5: 1}, "Erreur sur la décomposition de 30"
assert decomposition(83) == {83: 1}, "Erreur sur la décomposition de 83"

# --------- PYODIDE:secrets --------- #

from random import randrange
for _ in range(20):
    nb_fact = randrange(1, 5)
    d = {}
    for i in range(nb_fact):
        f = premiers[randrange(len(premiers))]
        if f not in d:
            d[f] = randrange(1, 6)
        n = 1
        for cle, val in d.items():
            n = n * (cle**val)
        assert decomposition(n) == d, f"Erreur sur la décomposition de {n = }"
