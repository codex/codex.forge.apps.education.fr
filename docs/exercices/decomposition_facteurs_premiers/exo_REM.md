Plutôt que renvoyer un dictionnaire, la fonction `decomposition` pourrait renvoyer une chaîne de caractères représentant la décomposition en facteurs premiers d'un entier $n$.

Par exemple, avec $n = 48$, la décomposition s'écrit $2^4 \times 3^1$ que l'on peut représenter par la chaîne `#!py '2^4 x 3^1'`.

Il suffit pour cela de compléter la fonction `decomposition`:

```python
def decomposition(n):
    dico = {}
    p = 2
    i = 0
    while i < len(premiers) and p <= n:
        p = premiers[i]
        while n % p == 0:
            if p in dico:
                dico[p] = dico[p] + 1
            else:
                dico[p] = 1
            n = n // p
        i = i + 1
    rep = ''
    for cle, val in dico.items():
        rep += str(cle) + '^' + str(val) + ' x '
    return rep[:-3]
```
