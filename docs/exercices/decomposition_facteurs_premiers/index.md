---
author: Serge Bays
hide:
    - navigation
    - toc
title: Décomposition en facteurs premiers
difficulty: 240
tags:
    - dictionnaire
    - maths
maj: 01/02/2024
---

On donne la liste des nombres premiers inférieurs à $100$ et on s'intéresse à la décomposition en facteurs premiers d'un entier $n$ compris entre $2$ et $100$.

Par exemple:

* $16 = 2^4$;
* $72 = 2^3 \times 3^2$;
* $45 = 3^2 \times 5^1$.


On demande d'écrire la fonction `decomposition` qui prend en paramètre un entier `n` compris entre $2$ et $100$ et renvoie la décomposition en facteurs premiers de `n`.

Cette décomposition est représentée par un dictionnaire. Par exemple la décomposition $3^2 \times 5^1$ est représentée par `#!py {3: 2, 5: 1}`.

???+ example "Exemples"

    ```pycon title=""
    >>> decomposition(72)
    {2: 3, 3: 2}
    >>> decomposition(16)
    {2: 4}
    ```
    
{{ IDE('exo') }}
