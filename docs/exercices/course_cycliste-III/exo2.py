

# --------- PYODIDE:env --------- #


def dernier(classement):
    """renvoie le dossard du coureur en dernière position de la course."""
    derniere_position = len(classement) - 1
    return classement[derniere_position]

def depasse(classement, place):
    indice_coureur = place - 1
    indice_precedent = indice_coureur - 1
    coureur_precedent = classement[indice_precedent]
    classement[indice_precedent] = classement[indice_coureur]
    classement[indice_coureur] = coureur_precedent


def position(classement, dossard):
    for position_coureur in range(len(classement)):
        if classement[position_coureur] == dossard:
            return position_coureur + 1

# --------- PYODIDE:code --------- #

def crevaison(classement, dossard):
    ...


# --------- PYODIDE:corr --------- #

def crevaison(classement, dossard):
    position_coureur = position(classement, dossard)
    while dossard != dernier(classement):
        depasse(classement, position_coureur + 1)
        position_coureur += 1

# --------- PYODIDE:tests --------- #

classement_actuel = ["Nadia", "Djamil", "Thomas", "Elizabeth", "Laure"]
depasse(classement_actuel, 3)
assert classement_actuel == ['Nadia', 'Thomas', 'Djamil', 'Elizabeth', 'Laure']

# --------- PYODIDE:secrets --------- #


course = ["Nadia", "Thomas", "Djamil", "Elizabeth", "Laure"]
crevaison(course, "Djamil")
assert course == ["Nadia", "Thomas", "Elizabeth", "Laure", "Djamil"]
crevaison(course, "Nadia")
assert course == ["Thomas", "Elizabeth", "Laure", "Djamil", "Nadia"]

course = ["moi"]
crevaison(course, "moi")
assert course == ["moi"]
