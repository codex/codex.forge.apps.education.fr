

# --------- PYODIDE:code --------- #

def position(classement, dossard):
    ...


# --------- PYODIDE:corr --------- #

def position(classement, dossard):
    for i in range(len(classement)):
        if classement[i] == dossard:
            return i + 1

# --------- PYODIDE:tests --------- #

course = ["Nadia", "Djamil", "Thomas", "Elizabeth", "Laure"]
assert position(course, "Elizabeth") == 4

# --------- PYODIDE:secrets --------- #

course = ["Nadia-01", "Franck-64", "Thomas-31", "Elizabeth-22", "Laure-66"]
pos = 1
for dossard in course:
    assert position(course, dossard) == pos
    pos += 1


course = ["moi"]
assert position(course, "moi") == 1
