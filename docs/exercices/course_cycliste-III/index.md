---
author: Pierre Marquestaut
hide:
    - navigation
    - tocI
title: La course cycliste(III)
tags:
    - liste/tableau
difficulty : 205
---



Une course cycliste se déroule sur un circuit permettant de nombreux dépassements.

![course](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/V%C3%A9locourse.jpg/220px-V%C3%A9locourse.jpg){.center title='ludovic from Guissény. (Bretagne, Finistère), France, CC BY-SA 2.0, via Wikimedia Commons'}

Chaque cycliste est identifié par un dossard sur lequel est inscrit son prénom : `#!py "Nadia"`, `#!py "Djamil"`...   
On garantit que tous les prénoms sont différents.

Leurs places dans le classement sont stockées de façon ordonnée dans une liste : le premier du classement se trouve à la première position de la liste, le deuxième à la deuxième position, etc... Par exemple `#!py classement_actuel = ["Nadia", "Djamil", "Thomas", "Elizabeth", "Laure"]`. Nadia est la première et Laure la dernière.

Écrire la fonction `position` qui prend en paramètres le tableau qui stocke la course et une chaine de caractères contenant le dossard d'un cycliste et qui renvoie la place de ce cycliste dans la course. On supposera que le dossard donné est bien présent dans le classement.


!!! warning "Attention"

    La place du coureur dans le classement ne correspond pas exactement à l'indice du tableau : le coureur à la première place sera à la première position du tableau, celui à la deuxième place en deuxième position, etc...

    
```pycon title=""
>>> position(classement_actuel, "Nadia")
1
>>> position(classement_actuel, "Thomas")
3
```

{{ IDE('exo1') }}

Il peut arriver qu'un cycliste crève, et se fasse doubler par tous les autres coureurs et se retrouve à la dernière place, puis reparte.

On dispose de la fonction `dernier` qui prend en paramètre le tableau qui stocke la course et qui renvoie le coureur à la dernière place du classement.

???+ example "Exemple"

    ```pycon title=""
    >>> dernier(classement_actuel)
    'Laure'
    ```

On dispose de la fonction `depasse` qui prend en paramètres le tableau qui stocke la course et la place d'un coureur et modifie puis renvoie le tableau avec le coureur qui est passé devant le coureur précédent.

???+ example "Exemple"

    ```pycon title=""
    >>> classement_actuel = ["Nadia", "Djamil", "Thomas", "Elizabeth", "Laure"]
    >>> depasse(classement_actuel, 3)
    >>> classement_actuel
    ['Nadia', 'Thomas', 'Djamil', 'Elizabeth', 'Laure']
    ```

* Écrire la fonction `crevaison` qui prend en paramètres le tableau qui stocke la course et le dossard d'un coureur qui a crevé et modifie puis renvoie le tableau. 


??? warning "Modification *en place*"

    La liste `classement` est directement modifiée, **il ne faut pas construire une nouvelle liste**.
    
    Il est inutile de la renvoyer modifiée.

??? tips "Indication"
    
    On partira du principe que tous les coureurs derrière le malchanceux coureur le doublent jusqu'à ce que le coureur soit le dernier. 

??? warning "Contrainte"

    On utilisera **obligatoirement** les fonctions `position`, `depasse` et `dernier`.


???+ example "Exemple"

    ```pycon title=""
    >>> crevaison(course, "Thomas")
    >>> classement_actuel
    ['Nadia', 'Djamil', 'Elizabeth', 'Laure', 'Thomas']
    ```

{{ IDE('exo2') }}
