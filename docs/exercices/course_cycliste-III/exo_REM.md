On pouvait également opter pour la solution suivante :
```python
def dernier(classement):
    return classement[-1]
```
