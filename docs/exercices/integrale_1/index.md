---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Aire sous une courbe (1)
tags:
    - maths
difficulty: 200
maj: 08/04/2024
---

On considère dans cet exercice la fonction $f$ définie sur $\left[0~;~1\right]$ par $f(x)=x^2$. On appelle $C_f$ sa représentation graphique dans un repère orthonormé.

On cherche à calculer une valeur approchée de l'aire du domaine situé entre :

* l'axe des abscisses ;
* la courbe $C_f$ ;
* les droites d'équations $x=0$ et $x=1$.

Ce domaine est colorié ci-dessous (à gauche).

<div style="display: flex;justify-content: space-around;">
    <img src="base.svg" class="autolight" width=30%>
    <img src="quatre.svg" class="autolight" width=30%>
</div>

Il est possible de calculer cette valeur approchée en utilisant la *méthode des rectangles* [^1] suivante :

[^1]: On propose ici une version simplifiée de cette méthode qui considère des rectangles ayant tous la même largeur.

* on découpe l'intervalle $\left[0~;~1\right]$ en $n>0$ subdivisions régulières. Chaque subdivision a une largeur de $\dfrac1n$ ;
* il est possible de numéroter chaque subdivision avec un entier $0 \leqslant k < n$ ;
* dans la $k$-ième subdivision, on dessine le rectangle de largeur $\dfrac{1}{n}$ et de hauteur $\left(k\times \dfrac{1}{n}\right)^2$ ;
* la somme des aires de tous les rectangles donne une valeur approchée (par défaut) de l'aire du domaine étudié.

La figure de droite illustre cette méthode dans le cas $n=4$. La valeur approchée obtenue vaut alors :

$$0^2\times0,25+0,25^2\times0,25+0,5^2\times0,25+0,75^2\times0,25=0,21875$$

Il est clair que plus le nombre de subdivisions est important, plus la valeur calculée est proche de la valeur exacte de l'aire cherchée.

Écrire la fonction `aire` qui prend en paramètre un entier `n` strictement positif et renvoie la valeur approchée renvoyée par la méthode des rectangles décrite utilisant $n$ rectangles.

{{ remarque('assertion') }}

{{ remarque('proximite_flottants') }}

???+ example "Exemples"

    ```pycon title=""
    >>> aire(1)
    0.0
    >>> aire(2)
    0.125
    >>> aire(4)
    0.21875
    ```

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}

