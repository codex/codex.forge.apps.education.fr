Il y a de nombreuses manières d'écrire cette fonction.

Par exemple:

```python
def union(d1, d2):
    d = {cle: valeur for (cle, valeur) in d1.items()}
    for cle in d2:
        if cle not in d:
            d[cle] = d2[cle]
        else:
            d[cle] = d[cle] + d2[cle]
    return d
```

ou bien:

```python
def union(d1, d2):
    d = {}
    for cle in d1:
        if cle in d2:
            d[cle] = d1[cle] + d2[cle]
        else:
            d[cle] = d1[cle]
    for cle in d2:
        if cle not in d:
            d[cle] = d2[cle]
    return d
```

ou bien:

```python
def union(d1, d2):
    d = {}
    for cle in d1.keys() - d2.keys():
        d[cle] = d1[cle]
    for cle in d2.keys() - d1.keys():
        d[cle] = d2[cle]
    for cle in d1.keys() & d2.keys():
        d[cle] = d1[cle] + d2[cle]
    return d
```
