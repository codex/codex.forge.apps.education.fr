---
author: Serge Bays
hide:
    - navigation
    - toc
title: Union de dictionnaires
tags:
    - dictionnaire
    - ep1
difficulty: 180
maj: 01/03/2024
---



Écrire une fonction `union` qui prend en paramètres deux dictionnaires `d1` et `d2`, dont les valeurs associées aux clés sont des nombres, et qui renvoie un dictionnaire `d`
défini ainsi:

* les clés de `d` sont celles de `d1` et `d2` réunies;
* la valeur associée à une clé dans le dictionnaire `d` est la somme de ses valeurs associées dans les dictionnaires `d1` et `d2` si la clé est présente dans ces deux dictionnaires et sinon la valeur associée dans le dictionnaire où elle est présente.


???+ example "Exemples"

    ```pycon 
    >>> union({1: 4, 2: 3}, {1: 5, 3: 7})
    {1: 9, 2: 3, 3: 7}
    >>> union({}, {'a': 7, 'b': 5})
    {'a': 7, 'b': 5}
    >>> union({'a': 7, 'b': 5}, {})
    {'a': 7, 'b': 5}
	>>> union({'Aya': 15, 'Edith': 12}, {'Edith': 15, 'Aya': 12})
    {'Aya': 27, 'Edith': 27}
    ```
	
    
{{ IDE('exo') }}
