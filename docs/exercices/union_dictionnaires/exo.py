

# --------- PYODIDE:code --------- #

def union(d1, d2):
    ...

# --------- PYODIDE:corr --------- #

def union(d1, d2):
    d = {}
    for cle in d1:
        d[cle] = d1[cle]
    for cle in d2:
        if cle not in d:
            d[cle] = d2[cle]
        else:
            d[cle] = d[cle] + d2[cle]
    return d

# --------- PYODIDE:tests --------- #

assert union({1: 4, 2: 3}, {1: 5, 3: 7}) == {1: 9, 2: 3, 3: 7}
assert union({}, {'a': 7, 'b': 5}) == {'a': 7, 'b': 5}
assert union({'a': 7, 'b': 5}, {}) == {'a': 7, 'b': 5}

# --------- PYODIDE:secrets --------- #


# tests secrets
assert union({}, {}) == {}
d1 = {i: i for i in range(10)}
d = {i: 2*i for i in range(10)}
assert union(d1, d1) == d
d1 = {i: i for i in range(10)}
d2 = {i: i for i in range(10, 20)}
d = {i: i for i in range(20)}
assert union(d1, d2) == d
d1 = {i: i for i in range(11)}
d2 = {i: i for i in range(10, 20)}
d = {i: i for i in range(20)}
d[10] += 10
assert union(d1, d2) == d