Il peut être intéressant de remarquer qu'au prix de quelques opérations inutiles, il est possible d'éviter certaines redondances dans le code de la fonction `disposition_valide`.

En effet, il y a de nombreuses similarités, avec 6 structures du type :

```python title=""
    for ... in range(...):
        if ...:
            return False
```

Si l'on peut avoir 6 boucles identiques, on peut alors regrouper toutes les conditions en une seule, en liant les résultats avec des opérateurs `#!py or`.

Pour cela, il faut uniformiser les `range(...)`. Concernant les diagonales, on pourrait très bien vérifier les diagonales les plus courtes. Techniquement, cela ne sert à rien car elles sont forcément valides (il ne peut pas y avoir plus d'une dame sur celles-ci), mais cela reste correct d'un point de vue logique.
<br>On obtient alors :

```python title=""
    for i in range(0, n):
        if diagonale_haut(echiquier, i, 0) > 1:
            return False
    for j in range(1, n):
        if diagonale_haut(echiquier, n - 1, j) > 1:
            return False

    # Les diagonales "bas"
    for i in range(0, n):
        if diagonale_bas(echiquier, i, 0) > 1:
            return False
    for j in range(1, n):
        if diagonale_bas(echiquier, 0, j) > 1:
            return False
```

Les `range(0, n)` sont équivalents à `range(n)`, et la seule différence qui reste vient des `range(1,n)`.

Si on utilise `range(n)` pour les 4 boucles, on a donc une implantation "homogène", au prix de faire la vérification sur les 2 plus grandes diagonales deux fois :

* `diagonale_haut(echiquier, i, 0)` avec $i=n-1$
* `diagonale_haut(echiquier, n-1, j)` avec $j=0$
* `diagonale_bas(echiquier, i, 0)` avec $i=0$
* `diagonale_bas(echiquier, 0, j)` avec $j=0$

Si l'on accepte ces désavantages on obtient alors la solution suivante :

```python title=""
def disposition_valide(echiquier):
    n = len(echiquier)
    for k in range(n):
        if (
            ligne(echiquier, k) != 1
            or colonne(echiquier, k) != 1
            or diagonale_haut(echiquier, k, 0) > 1
            or diagonale_haut(echiquier, n-1, k) > 1
            or diagonale_bas(echiquier, k, 0) > 1
            or diagonale_bas(echiquier, 0, k) > 1
        ):
            return False
    return True
```

Noter que:

* Afin de clarifier le code, l'indice de boucle est renommé `k`, car ce n'est plus spécifiquement un indice vertical ou horizontal. C'est maintenant simplement une valeur comprise dans $[0;n[$.
* Dans le même but, la condition est écrite dans des parenthèses (rien à voir avec un tuple !) afin de pouvoir écrire chaque appel de fonction sur une ligne différente.
* Le code plus générique rend la logique plus facile à suivre.
* Enfin, il est important de remarquer que le débogage de cette fonction peut s'avérer moins évident à gérer (mais avec un peu d'habitude, on s'en sort encore).

