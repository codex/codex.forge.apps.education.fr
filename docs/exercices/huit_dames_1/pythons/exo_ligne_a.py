# --------- PYODIDE:ignore --------- #
auto_run = lambda f:f()

# --------- PYODIDE:env --------- #
def ligne(echiquier, i):
    ...


# --------- PYODIDE:code --------- #
def ligne(echiquier, i):
    ...


# --------- PYODIDE:corr --------- #
def ligne(echiquier, i):
    return sum(echiquier[i])


# --------- PYODIDE:tests --------- #
grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert ligne(grille, 0) == 1
assert ligne(grille, 1) == 1
assert ligne(grille, 7) == 1

invalide = [
    [0, 0, 0],
    [1, 1, 0],
    [0, 0, 1],
]

assert ligne(invalide, 0) == 0
assert ligne(invalide, 1) == 2
assert ligne(invalide, 2) == 1
# --------- PYODIDE:secrets --------- #

@auto_run
def _():

    from random import randrange as rand

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        for i in range(len(grille)):
            attendu = sum(grille[i])
            assert (
                ligne(copy, i) == attendu
            ), f"Erreur en demandant la ligne {i = } de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])

