Il est aussi possible d'utiliser ici une boucle `#!py for`.
<br>L'idée est de quantifier les déplacements le long d'une diagonale. Partant de $(i,j)$ pour une diagonale "bas", on peut:

* Descendre d'au plus $taille-i$ lignes, en comptant la ligne en cours.
* Se déplacer à droite d'au plus $taille-j$ colonnes.
* C'est le minimum de ces deux valeurs qui va donner le vrai nombre de déplacements possibles sans sortir de l'échiquier.
* On part de $(i,j)$ en descendant vers la droite de $k$ étapes, donc les coordonnées à vérifier sont du type $(i+k,j+k)$.

On obtient alors le code suivant :

```python title=""
def diagonale_bas(echiquier, i, j):
    taille = len(echiquier)
    n_deplacements = min(taille-i, taille-j)
    return sum( echiquier[i+k][j+k] for k in range(n_deplacements) )
```

