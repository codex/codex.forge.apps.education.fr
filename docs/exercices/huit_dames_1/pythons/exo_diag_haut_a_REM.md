Il est aussi possible d'utiliser ici une boucle `#!py for`.
<br>L'idée est de quantifier les déplacements le long d'une diagonale. Partant de $(i,j)$ pour une diagonale "haute", on peut:

* Remonter au plus de $i+1$ lignes, en comptant la ligne en cours (qu'il faut également vérifier).
* Se déplacer à droite d'au plus $taille-j$ colonnes.
* C'est le minimum de ces deux valeurs qui va donner le vrai nombre de déplacements possibles sans sortir de l'échiquier.
* On part de $(i,j)$ en montant vers la droite de $k$ étapes, donc les coordonnées à vérifier sont du type $(i-k,j+k)$.

On obtient alors le code suivant :

```python title=""
def diagonale_haut(echiquier, i, j):
    n_deplacements = min(i+1, len(echiquier)-j)
    return sum(echiquier[i-k][j+k] for k in range(n_deplacements))
```

