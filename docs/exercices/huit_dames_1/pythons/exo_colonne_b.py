# --------- PYODIDE:ignore --------- #
auto_run = lambda f:f()

# --------- PYODIDE:env --------- #
def colonne(echiquier, j):
    ...


# --------- PYODIDE:code --------- #
def colonne(echiquier, j):
    return sum(... for ... in ...)


# --------- PYODIDE:corr --------- #
def colonne(echiquier, j):
    return sum(li[j] for li in echiquier)


# --------- PYODIDE:tests --------- #

grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert colonne(grille, 0) == 1
assert colonne(grille, 1) == 1
assert colonne(grille, 7) == 1

invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert colonne(invalide, 0) == 0
assert colonne(invalide, 1) == 2
assert colonne(invalide, 2) == 1
# --------- PYODIDE:secrets --------- #

@auto_run
def _():

    from random import randrange as rand

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        for j in range(len(grille)):
            attendu = sum([li[j] for li in grille])
            assert (
                colonne(copy, j) == attendu
            ), f"Erreur en demandant la colonne {j = } de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])


