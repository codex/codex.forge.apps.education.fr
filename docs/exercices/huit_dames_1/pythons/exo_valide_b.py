# --------- PYODIDE:env --------- #
def ligne(echiquier, i):
    return sum(echiquier[i])


def colonne(echiquier, j):
    return sum(li[j] for li in echiquier)


def diagonale_haut(echiquier, i, j):
    n = len(echiquier)
    resultat = 0
    while i >= 0 and j < n:
        resultat += echiquier[i][j]
        i -= 1
        j += 1
    return resultat


def diagonale_bas(echiquier, i, j):
    n = len(echiquier)
    resultat = 0
    while i < n and j < n:
        resultat += echiquier[i][j]
        i += 1
        j += 1
    return resultat


# --------- PYODIDE:code --------- #
def disposition_valide(echiquier):
    n = ...

    # Les lignes et les colonnes
    for k in range(...):
        if ...(..., ...) != ...:
            return False
        if ... != ...:
            return False

    # Les diagonales "haut"
    for i in range(..., ...):
        if ... > 1:
            return False
    for j in range(1, n - 1):
        if ... > 1:
            return False

    # Les diagonales "bas"
    for i in range(..., ...):
        if ... > 1:
            return False
    for j in range(..., ...):
        if ... > 1:
            return False

    return ...


# --------- PYODIDE:corr --------- #
def disposition_valide(echiquier):
    n = len(echiquier)

    # Les lignes et les colonnes
    for k in range(n):
        if ligne(echiquier, k) != 1:
            return False
        if colonne(echiquier, k) != 1:
            return False

    # Les diagonales "haut"
    for i in range(1, n):
        if diagonale_haut(echiquier, i, 0) > 1:
            return False
    for j in range(1, n - 1):
        if diagonale_haut(echiquier, n - 1, j) > 1:
            return False

    # Les diagonales "bas"
    for i in range(0, n - 1):
        if diagonale_bas(echiquier, i, 0) > 1:
            return False
    for j in range(1, n - 1):
        if diagonale_bas(echiquier, 0, j) > 1:
            return False

    return True


# --------- PYODIDE:tests --------- #
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert disposition_valide(valide) is True
assert disposition_valide(invalide) is False

grille = [
    [0,0,0],
    [0,0,1],
    [1,0,0],
]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"

# --------- PYODIDE:secrets --------- #
grille = [
    [0, 0, 0, 0, 1, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
]
assert disposition_valide(grille) is True, f"Erreur avec {grille = }"
grille = [[1]]
assert disposition_valide(grille) is True, f"Erreur avec {grille = }"
grille = [[1, 0], [1, 1]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 1, 0, 0], [0, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 1, 0], [0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
