# --- ignore, ligne_, colonne_, diag_bas_, diag_haut_ --- #
auto_run = lambda f:f()

# --- hdr, ligne_ --- #
def ligne(echiquier, i):
    ...


# --- exo, ligne_ --- #
""" # skip
def ligne(echiquier, i):
    return ...(...[...])


"""  # skip


# --- vide, ligne_ --- #
def ligne(echiquier, i):
    ...


# --- corr, ligne_ --- #
def ligne(echiquier, i):
    return sum(echiquier[i])


# --- tests, ligne_ --- #
grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert ligne(grille, 0) == 1
assert ligne(grille, 1) == 1
assert ligne(grille, 7) == 1

invalide = [
    [0, 0, 0],
    [1, 1, 0],
    [0, 0, 1],
]

assert ligne(invalide, 0) == 0
assert ligne(invalide, 1) == 2
assert ligne(invalide, 2) == 1
# --- secrets, ligne_ --- #

@auto_run
def _():

    from random import randrange as rand

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        for i in range(len(grille)):
            attendu = sum(grille[i])
            assert (
                ligne(copy, i) == attendu
            ), f"Erreur en demandant la ligne {i = } de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])

# --- hdr, colonne_ --- #
def colonne(echiquier, j):
    ...


# --- exo, colonne_ --- #
"""  # skip
def colonne(echiquier, j):
    return sum(... for ... in ...)
"""  # skip


# --- vide, colonne_ --- #
def colonne(echiquier, j):
    ...


# --- corr, colonne_ --- #
def colonne(echiquier, j):
    return sum(li[j] for li in echiquier)


# --- tests, colonne_ --- #

grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert colonne(grille, 0) == 1
assert colonne(grille, 1) == 1
assert colonne(grille, 7) == 1

invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert colonne(invalide, 0) == 0
assert colonne(invalide, 1) == 2
assert colonne(invalide, 2) == 1
# --- secrets, colonne_ --- #

@auto_run
def _():

    from random import randrange as rand

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        for j in range(len(grille)):
            attendu = sum([li[j] for li in grille])
            assert (
                colonne(copy, j) == attendu
            ), f"Erreur en demandant la colonne {j = } de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])


# --- hdr, diag_haut_ --- #
def diagonale_haut(echiquier, i, j):
    ...


# --- exo, diag_haut_ --- #
""" # skip
def diagonale_haut(echiquier, i, j):
    taille = ...
    resultat = ...
    while i ... and ...:
        ...
        i = ...
        j = ...
    return ...
"""  # skip


# --- vide, diag_haut_ --- #
def diagonale_haut(echiquier, i, j):
    ...


# --- corr, diag_haut_ --- #

def diagonale_haut(echiquier, i, j):
    taille = len(echiquier)
    resultat = 0
    while i >= 0 and j < taille:
        resultat += echiquier[i][j]
        i -= 1
        j += 1
    return resultat


# --- rem, diag_haut_ --- #
""" # skip
Il est aussi possible d'utiliser ici une boucle `#!py for`.
<br>L'idée est de quantifier les déplacements le long d'une diagonale. Partant de $(i,j)$ pour une diagonale "haute", on peut:

* Remonter au plus de $i+1$ lignes, en comptant la ligne en cours (qu'il faut également vérifier).
* Se déplacer à droite d'au plus $taille-j$ colonnes.
* C'est le minimum de ces deux valeurs qui va donner le vrai nombre de déplacements possibles sans sortir de l'échiquier.
* On part de $(i,j)$ en montant vers la droite de $k$ étapes, donc les coordonnées à vérifier sont du type $(i-k,j+k)$.

On obtient alors le code suivant :

```python title=""
def diagonale_haut(echiquier, i, j):
    n_deplacements = min(i+1, len(echiquier)-j)
    return sum(echiquier[i-k][j+k] for k in range(n_deplacements))
```
""" # skip

# --- tests, diag_haut_ --- #

grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert diagonale_haut(grille, 0, 0) == 0
assert diagonale_haut(grille, 1, 0) == 0
assert diagonale_haut(grille, 2, 0) == 0
assert diagonale_haut(grille, 7, 0) == 1
assert diagonale_haut(grille, 7, 5) == 1
assert diagonale_haut(grille, 6, 6) == 0

invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert diagonale_haut(invalide, 0, 0) == 0
assert diagonale_haut(invalide, 1, 0) == 1
assert diagonale_haut(invalide, 2, 0) == 1
# --- secrets, diag_haut_ --- #
@auto_run
def _():

    from random import randrange as rand, shuffle

    def _diagonale_haut_(echiquier, i, j):
        n = len(echiquier)
        resultat = 0
        while i >= 0 and j < n:
            resultat += echiquier[i][j]
            i -= 1
            j += 1
        return resultat

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        rng = [*range(len(grille))]
        shuffle(rng)
        ijs = rng[:]
        shuffle(rng)
        ijs = [*zip(ijs,rng)]

        for i,j in ijs:
            attendu = _diagonale_haut_(grille, i, j)
            assert (
                diagonale_haut(copy, i, j) == attendu
            ), f"Erreur pour diagonale_haut(..., {i=}, {j=}) de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])

# --- hdr, diag_bas_ --- #
def diagonale_bas(echiquier, i, j):
    ...


# --- exo, diag_bas_ --- #
""" # skip
def diagonale_bas(echiquier, i, j):
    taille = ...
    resultat = ...
    while i ... and j ...:
        ...
        i = ...
        j = ...
    return ...
"""  # skip


# --- vide, diag_bas_ --- #
def diagonale_bas(echiquier, i, j):
    ...


# --- corr, diag_bas_ --- #
def diagonale_bas(echiquier, i, j):
    taille = len(echiquier)
    resultat = 0
    while i < taille and j < taille:
        resultat += echiquier[i][j]
        i += 1
        j += 1
    return resultat


# --- rem, diag_bas_ --- #
""" # skip
Il est aussi possible d'utiliser ici une boucle `#!py for`.
<br>L'idée est de quantifier les déplacements le long d'une diagonale. Partant de $(i,j)$ pour une diagonale "bas", on peut:

* Descendre d'au plus $taille-i$ lignes, en comptant la ligne en cours.
* Se déplacer à droite d'au plus $taille-j$ colonnes.
* C'est le minimum de ces deux valeurs qui va donner le vrai nombre de déplacements possibles sans sortir de l'échiquier.
* On part de $(i,j)$ en descendant vers la droite de $k$ étapes, donc les coordonnées à vérifier sont du type $(i+k,j+k)$.

On obtient alors le code suivant :

```python title=""
def diagonale_bas(echiquier, i, j):
    taille = len(echiquier)
    n_deplacements = min(taille-i, taille-j)
    return sum( echiquier[i+k][j+k] for k in range(n_deplacements) )
```
""" # skip

# --- tests, diag_bas_ --- #
grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert diagonale_bas(grille, 0, 0) == 1
assert diagonale_bas(grille, 1, 0) == 1
assert diagonale_bas(grille, 2, 0) == 1
assert diagonale_bas(grille, 7, 0) == 0
assert diagonale_bas(grille, 5, 4) == 1
assert diagonale_bas(grille, 6, 5) == 0


invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert diagonale_bas(invalide, 0, 0) == 2
assert diagonale_bas(invalide, 1, 0) == 0
assert diagonale_bas(invalide, 2, 0) == 0

# --- secrets, diag_bas_ --- #

@auto_run
def _():

    from random import randrange as rand, shuffle

    def _diagonale_bas_(echiquier, i, j):
        n = len(echiquier)
        resultat = 0
        while i < n and j < n:
            resultat += echiquier[i][j]
            i += 1
            j += 1
        return resultat

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        rng = [*range(len(grille))]
        shuffle(rng)
        ijs = rng[:]
        shuffle(rng)
        ijs = [*zip(ijs,rng)]

        for i,j in ijs:
            attendu = _diagonale_bas_(grille, i, j)
            assert (
                diagonale_bas(copy, i, j) == attendu
            ), f"Erreur pour diagonale_bas(..., {i=}, {j=}) de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])


# --- hdr, valide_ --- #
def ligne(echiquier, i):
    return sum(echiquier[i])


def colonne(echiquier, j):
    return sum(li[j] for li in echiquier)


def diagonale_haut(echiquier, i, j):
    n = len(echiquier)
    resultat = 0
    while i >= 0 and j < n:
        resultat += echiquier[i][j]
        i -= 1
        j += 1
    return resultat


def diagonale_bas(echiquier, i, j):
    n = len(echiquier)
    resultat = 0
    while i < n and j < n:
        resultat += echiquier[i][j]
        i += 1
        j += 1
    return resultat


# --- exo, valide_ --- #
""" # skip
def disposition_valide(echiquier):
    n = ...

    # Les lignes et les colonnes
    for k in range(...):
        if ...(..., ...) != ...:
            return False
        if ... != ...:
            return False

    # Les diagonales "haut"
    for i in range(..., ...):
        if ... > 1:
            return False
    for j in range(1, n - 1):
        if ... > 1:
            return False

    # Les diagonales "bas"
    for i in range(..., ...):
        if ... > 1:
            return False
    for j in range(..., ...):
        if ... > 1:
            return False

    return ...
"""  # skip


# --- vide, valide_ --- #
def disposition_valide(echiquier):
    ...


# --- corr, valide_ --- #
def disposition_valide(echiquier):
    n = len(echiquier)

    # Les lignes et les colonnes
    for k in range(n):
        if ligne(echiquier, k) != 1:
            return False
        if colonne(echiquier, k) != 1:
            return False

    # Les diagonales "haut"
    for i in range(1, n):
        if diagonale_haut(echiquier, i, 0) > 1:
            return False
    for j in range(1, n - 1):
        if diagonale_haut(echiquier, n - 1, j) > 1:
            return False

    # Les diagonales "bas"
    for i in range(0, n - 1):
        if diagonale_bas(echiquier, i, 0) > 1:
            return False
    for j in range(1, n - 1):
        if diagonale_bas(echiquier, 0, j) > 1:
            return False

    return True


# --- rem, valide_ --- #
""" # skip
Il peut être intéressant de remarquer qu'au prix de quelques opérations inutiles, il est possible d'éviter certaines redondances dans le code de la fonction `disposition_valide`.

En effet, il y a de nombreuses similarités, avec 6 structures du type :

```python title=""
    for ... in range(...):
        if ...:
            return False
```

Si l'on peut avoir 6 boucles identiques, on peut alors regrouper toutes les conditions en une seule, en liant les résultats avec des opérateurs `#!py or`.

Pour cela, il faut uniformiser les `range(...)`. Concernant les diagonales, on pourrait très bien vérifier les diagonales les plus courtes. Techniquement, cela ne sert à rien car elles sont forcément valides (il ne peut pas y avoir plus d'une dame sur celles-ci), mais cela reste correct d'un point de vue logique.
<br>On obtient alors :

```python title=""
    for i in range(0, n):
        if diagonale_haut(echiquier, i, 0) > 1:
            return False
    for j in range(1, n):
        if diagonale_haut(echiquier, n - 1, j) > 1:
            return False

    # Les diagonales "bas"
    for i in range(0, n):
        if diagonale_bas(echiquier, i, 0) > 1:
            return False
    for j in range(1, n):
        if diagonale_bas(echiquier, 0, j) > 1:
            return False
```

Les `range(0, n)` sont équivalents à `range(n)`, et la seule différence qui reste vient des `range(1,n)`.

Si on utilise `range(n)` pour les 4 boucles, on a donc une implantation "homogène", au prix de faire la vérification sur les 2 plus grandes diagonales deux fois :

* `diagonale_haut(echiquier, i, 0)` avec $i=n-1$
* `diagonale_haut(echiquier, n-1, j)` avec $j=0$
* `diagonale_bas(echiquier, i, 0)` avec $i=0$
* `diagonale_bas(echiquier, 0, j)` avec $j=0$

Si l'on accepte ces désavantages on obtient alors la solution suivante :

```python title=""
def disposition_valide(echiquier):
    n = len(echiquier)
    for k in range(n):
        if (
            ligne(echiquier, k) != 1
            or colonne(echiquier, k) != 1
            or diagonale_haut(echiquier, k, 0) > 1
            or diagonale_haut(echiquier, n-1, k) > 1
            or diagonale_bas(echiquier, k, 0) > 1
            or diagonale_bas(echiquier, 0, k) > 1
        ):
            return False
    return True
```

Noter que:

* Afin de clarifier le code, l'indice de boucle est renommé `k`, car ce n'est plus spécifiquement un indice vertical ou horizontal. C'est maintenant simplement une valeur comprise dans $[0;n[$.
* Dans le même but, la condition est écrite dans des parenthèses (rien à voir avec un tuple !) afin de pouvoir écrire chaque appel de fonction sur une ligne différente.
* Le code plus générique rend la logique plus facile à suivre.
* Enfin, il est important de remarquer que le débogage de cette fonction peut s'avérer moins évident à gérer (mais avec un peu d'habitude, on s'en sort encore).
""" # skip

# --- tests, valide_ --- #
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert disposition_valide(valide) is True
assert disposition_valide(invalide) is False

grille = [
    [0,0,0],
    [0,0,1],
    [1,0,0],
]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"

# --- secrets, valide_ --- #
grille = [
    [0, 0, 0, 0, 1, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
]
assert disposition_valide(grille) is True, f"Erreur avec {grille = }"
grille = [[1]]
assert disposition_valide(grille) is True, f"Erreur avec {grille = }"
grille = [[1, 0], [1, 1]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 1, 0, 0], [0, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
grille = [[0, 0, 1, 0], [0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]]
assert disposition_valide(grille) is False, f"Erreur avec {grille = }"
