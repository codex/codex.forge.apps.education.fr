# --------- PYODIDE:ignore --------- #
auto_run = lambda f:f()

# --------- PYODIDE:env --------- #
def diagonale_bas(echiquier, i, j):
    ...


# --------- PYODIDE:code --------- #
def diagonale_bas(echiquier, i, j):
    taille = ...
    resultat = ...
    while i ... and j ...:
        ...
        i = ...
        j = ...
    return ...


# --------- PYODIDE:corr --------- #
def diagonale_bas(echiquier, i, j):
    taille = len(echiquier)
    resultat = 0
    while i < taille and j < taille:
        resultat += echiquier[i][j]
        i += 1
        j += 1
    return resultat


# --------- PYODIDE:tests --------- #
grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert diagonale_bas(grille, 0, 0) == 1
assert diagonale_bas(grille, 1, 0) == 1
assert diagonale_bas(grille, 2, 0) == 1
assert diagonale_bas(grille, 7, 0) == 0
assert diagonale_bas(grille, 5, 4) == 1
assert diagonale_bas(grille, 6, 5) == 0


invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert diagonale_bas(invalide, 0, 0) == 2
assert diagonale_bas(invalide, 1, 0) == 0
assert diagonale_bas(invalide, 2, 0) == 0

# --------- PYODIDE:secrets --------- #

@auto_run
def _():

    from random import randrange as rand, shuffle

    def _diagonale_bas_(echiquier, i, j):
        n = len(echiquier)
        resultat = 0
        while i < n and j < n:
            resultat += echiquier[i][j]
            i += 1
            j += 1
        return resultat

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        rng = [*range(len(grille))]
        shuffle(rng)
        ijs = rng[:]
        shuffle(rng)
        ijs = [*zip(ijs,rng)]

        for i,j in ijs:
            attendu = _diagonale_bas_(grille, i, j)
            assert (
                diagonale_bas(copy, i, j) == attendu
            ), f"Erreur pour diagonale_bas(..., {i=}, {j=}) de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])


