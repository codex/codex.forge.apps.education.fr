# --------- PYODIDE:ignore --------- #
auto_run = lambda f:f()

# --------- PYODIDE:env --------- #
def diagonale_haut(echiquier, i, j):
    ...


# --------- PYODIDE:code --------- #
def diagonale_haut(echiquier, i, j):
    taille = ...
    resultat = ...
    while i ... and ...:
        ...
        i = ...
        j = ...
    return ...


# --------- PYODIDE:corr --------- #

def diagonale_haut(echiquier, i, j):
    taille = len(echiquier)
    resultat = 0
    while i >= 0 and j < taille:
        resultat += echiquier[i][j]
        i -= 1
        j += 1
    return resultat


# --------- PYODIDE:tests --------- #

grille = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

assert diagonale_haut(grille, 0, 0) == 0
assert diagonale_haut(grille, 1, 0) == 0
assert diagonale_haut(grille, 2, 0) == 0
assert diagonale_haut(grille, 7, 0) == 1
assert diagonale_haut(grille, 7, 5) == 1
assert diagonale_haut(grille, 6, 6) == 0

invalide = [
    [0, 1, 0],
    [0, 1, 0],
    [0, 0, 1],
]

assert diagonale_haut(invalide, 0, 0) == 0
assert diagonale_haut(invalide, 1, 0) == 1
assert diagonale_haut(invalide, 2, 0) == 1
# --------- PYODIDE:secrets --------- #
@auto_run
def _():

    from random import randrange as rand, shuffle

    def _diagonale_haut_(echiquier, i, j):
        n = len(echiquier)
        resultat = 0
        while i >= 0 and j < n:
            resultat += echiquier[i][j]
            i -= 1
            j += 1
        return resultat

    def check(grille):
        copy = [r[:] for r in grille]
        s_grille = '\n'.join(map(str, grille))
        # print(*grille,'',sep='\n')

        rng = [*range(len(grille))]
        shuffle(rng)
        ijs = rng[:]
        shuffle(rng)
        ijs = [*zip(ijs,rng)]

        for i,j in ijs:
            attendu = _diagonale_haut_(grille, i, j)
            assert (
                diagonale_haut(copy, i, j) == attendu
            ), f"Erreur pour diagonale_haut(..., {i=}, {j=}) de la grille: [\n{ s_grille }\n]"


    check([
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
    ])

    for size in range(2,11):
        check([
            [int(not rand(size)) for _ in range(size)]
            for _ in range(size)
        ])

