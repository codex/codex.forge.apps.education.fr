---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Problème des huit dames (1)
difficulty: 220
tags:
    - grille
    - fonctions
    - booléen
---



Aux échecs, la dame est capable de se déplacer dans toutes les directions.

<center>
![Mouvement des dames](mvt_dame.svg){width="25%" }
![Disposition valide](valide_1.svg){width="25%" }
</center>

Le problème des huit dames consiste à placer $8$ dames sur un échiquier classique ($8$ lignes et $8$ colonnes) de façon à ce qu'aucune d'entre elles ne soit menacée par une autre. Ainsi il n'y a qu'une seule dame par ligne, colonne ou diagonale. La figure ci-dessus (à droite) illustre une disposition valide.


On considère dans cet exercice des « échiquiers » de taille $n \ge 1$ variable. On garantit que l'échiquier est bien carré. Selon la taille de l'échiquier, on pourra placer plus ou moins de dames.

Le problème considéré dans cet exercice consiste donc à vérifier qu'il y a exactement $n$ dames dans un échiquier de $n \times n$ cases de façon à ce qu'aucune d'entre elles ne soit menacée par une autre.

Les échiquiers seront représentés en machine par des listes de listes contenant :

* `#!py 0` si la case correspondante est vide,
* `#!py 1` si elle contient une dame.

??? example "Exemples de listes"

    La liste ci-dessous représente la disposition valide donnée plus haut.

    ```python
    valide = [
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0]
    ]
    ```

    La liste ci-dessous représente une disposition invalide dans un échiquier de taille $n = 3$ :


    ```python
    invalide = [
        [0, 0, 1],
        [1, 0, 0],
        [0, 1, 0]
    ]
    ```

    Cette seconde disposition est invalide car deux dames sont sur une même diagonale (`#!py invalide[1][0]` et `#!py invalide[2][1]` toutes deux la valeurs `#!py 1`).


Vous devez écrire différentes fonctions qui vont servir à compter le nombre de dames selon différents axes d'un échiquier, puis la fonction permettant de valider la disposition :

* `ligne(echiquier, i)` renvoie le nombre de dames sur une ligne de l'échiquier ;

* `colonne(echiquier, j)` renvoie le nombre de dames sur une colonne de l'échiquier ;

* `diagonale_haut(echiquier, i, j)` et `diagonale_bas(echiquier, i, j)` renvoient chacune le nombre de dames sur une diagonale de l'échiquier ;

* `disposition_valide(echiquier)` renvoie le booléen répondant à la question « *la disposition est-elle valide ?* ».

On garantit que toutes les listes de listes passées en paramètres ont bien le même nombre de lignes et de colonnes et que tous les indices sont valides (`#!py 0 <= indice < n`).

??? example "Exemples"

    On utilise les échiquiers `valide` et `invalide` définis plus haut.

    ```pycon title="Les lignes"
    >>> ligne(valide, 0)
    1  # [0, 0, 0, 1, 0, 0, 0, 0]
    >>> ligne(valide, 1)
    1  # [0, 0, 0, 0, 0, 0, 1, 0]
    >>> ligne(valide, 7)
    1  # [0, 0, 0, 0, 0, 1, 0, 0]
    ```

    ```pycon title="Les colonnes"
    >>> colonne(valide, 0)
    1  # [0, 0, 0, 0, 0, 0, 1, 0]
    >>> colonne(valide, 1)
    1  # [0, 0, 0, 0, 1, 0, 0, 0]
    >>> colonne(valide, 7)
    1  # [0, 0, 0, 1, 0, 0, 0, 0]
    ```

    ```pycon title="Les diagonales ↗"
    >>> diagonale_haut(valide, 0, 0)
    0  # [0]
    >>> diagonale_haut(valide, 2, 0)
    0  # [0, 0, 0]
    >>> diagonale_haut(valide, 7, 0)
    1  # [0, 0, 0, 0, 0, 0, 1, 0]
    >>> diagonale_haut(valide, 3, 4)
    1  # [0, 0, 1, 0]
    ```

    ```pycon title="Les diagonales ↘"
    >>> diagonale_bas(valide, 0, 0)
    1  # [0, 0, 1, 0, 0, 0, 0, 0]
    >>> diagonale_bas(valide, 1, 0)
    1  # [0, 0, 0, 0, 1, 0, 0]
    >>> diagonale_bas(valide, 2, 0)
    1  # [0, 0, 0, 0, 0, 1]
    >>> diagonale_bas(valide, 7, 0)
    0  # [0]
    >>> diagonale_bas(invalide, 1, 0)
    2  # [1, 1]
    ```

    ```pycon title="Disposition valide ?"
    >>> disposition_valide(valide)
    True
    >>> disposition_valide(invalide)
    False
    ```

??? question "Fonction `ligne`"

    Écrire la fonction `ligne` qui prend en paramètres une liste `echiquier` et l'indice d'une ligne `i` et renvoie le nombre de dames dans cette ligne dans l'échiquier.

    {{ double_IDE(
        './pythons/exo_ligne_a',
        './pythons/exo_ligne_b',
        title2 = "Version à compléter",
    ) }}

??? question "Fonction `colonne`"


    Écrire la fonction `colonne` qui prend en paramètres une liste `echiquier` et l'indice d'une colonne `j` et renvoie le nombre de dames dans cette colonne dans l'échiquier.

    {{ double_IDE(
        './pythons/exo_colonne_a',
        './pythons/exo_colonne_b',
        title2 = "Version à compléter",
    ) }}

??? question "Fonction `diagonale_haut`"

    Écrire la fonction `diagonale_haut` qui prend en paramètres une liste `echiquier` et les coordonnées d'une case (ligne `i` et colonne `j`) et renvoie le nombre de dames dans la diagonale débutant dans la case indiquée et dirigée vers le **haut à droite** ↗.

    {{ double_IDE(
        './pythons/exo_diag_haut_a',
        './pythons/exo_diag_haut_b',
        title2 = "Version à compléter",
    ) }}

??? question "Fonction `diagonale_bas`"

    Écrire la fonction `diagonale_haut` qui prend en paramètres une liste `echiquier` et les coordonnées d'une case (ligne `i` et colonne `j`) et renvoie le nombre de dames dans la diagonale débutant dans la case indiquée et dirigée vers le **bas à droite** ↘.

    {{ double_IDE(
        './pythons/exo_diag_bas_a',
        './pythons/exo_diag_bas_b',
        title2 = "Version à compléter",
    ) }}

??? question "Fonction `disposition_valide`"

    Écrire la fonction `disposition_valide` qui prend en paramètre une liste `echiquier` et renvoie le booléen répondant à la question « *la disposition satisfait-elle le problème des $N$ dames ?* ».

    Toutes les fonctions précédentes ont été importées, dans leur version corrigée, dans les éditeurs ci-dessous. **Vous pouvez directement les utiliser**.

    ??? tip "Aide (1)"

        On pourra (mais ce n'est pas une obligation) effectuer les vérifications illustrées par les figures suivantes :

        <center>
        ![Lignes](recherche_lignes.svg){width="25%"} ![Colonnes](recherche_colonnes.svg){width="25%"}

        ![Diagonales NE](recherche_diagonales_NE.svg){width="25%"} ![Diagonale SE](recherche_diagonales_SE.svg){width="25%"}
        </center>

    ??? tip "Aide (2)"

        Ne pas oublier que le nombre total de dames doit être de $N$ également.

        Cela peut être vérifié en ajoutant une nouvelle partie à l'implantation, ou bien il est possible de faire cette vérification en utilisant des conditions appropriées dans le code utilisant les 4 fonctions : `ligne`, `colonne`, `diagonale_haut` et `diagonale_bas`.

    {{ double_IDE(
        './pythons/exo_valide_a',
        './pythons/exo_valide_b',
        title2 = "Version à compléter",
    ) }}
