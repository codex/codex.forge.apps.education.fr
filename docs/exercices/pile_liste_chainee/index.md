---
author: Serge Bays
hide:
    - navigation
    - toc
title: Pile avec liste chaînée
tags:
    - pile
    - programmation orientée objet
difficulty: 220
maj: 24/11/2024
---

# Pile avec une liste chainée

On veut écrire une classe pour gérer une pile à l'aide d'une liste chainée. On dispose d'une classe `Maillon` permettant la création d'un maillon de la chaine, celui-ci étant constitué d'une donnée `valeur` et d'une référence au maillon suivant de la chaine:

```python
class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant
```

Une pile non vide est représentée par des maillons avec un maillon de tête, le dernier ajouté à la pile avec la fonction `empile` et celui qu'on va enlever de la pile avec la fonction `depile`.

Le maillon de tête contient l'élément au sommet de la pile. Ci-dessous la représentation d'une pile dans laquelle on a empilé 9, puis 2, puis 8, puis 5:

![représentation d'une pile](pile.svg){ width=30% .autolight .center}

Compléter la classe `Pile` et vérifier votre travail sur les exemples. On vous donne la méthode `__init__` qui initialise une pile, la méthode `__str__` qui permet un affichage des éléments de la pile, la méthode `est_vide` qui renvoie `True` si la pile est vide et `False` sinon. 

???+ example "Exemples"

    ```pycon title=""
    >>> p = Pile()  # une pile vide
    >>> p.est_vide()
    True
    >>> p.empile(9)
    >>> p.est_vide()
    False
    >>> p.empile(5)
    >>> p.empile(8)
    >>> print(p)
    8 -> 5 -> 9
    >>> p.depile()
    8
    >>> p.depile()
    5
    ```

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo') }}


