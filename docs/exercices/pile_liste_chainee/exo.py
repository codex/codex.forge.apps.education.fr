# --------- PYODIDE:code --------- #
class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        if self.suivant is None:
            return str(self.valeur)
        return str(self.valeur) + " -> " + str(self.suivant)


class Pile:
    def __init__(self, maillon=None):
        self.sommet = maillon

    def est_vide(self):
        return self.sommet is None

    def __str__(self):
        return str(self.sommet)

    def empile(self, element):
        nouveau_maillon = ...
        self.sommet = ...

    def depile(self):
        if not self.est_vide():
            maillon = self.sommet
            element = ...
            self.sommet = ...
            return ...
        else:
            return None


# --------- PYODIDE:corr --------- #


class Maillon:
    def __init__(self, valeur, suivant):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        if self.suivant is None:
            return str(self.valeur)
        return str(self.valeur) + " -> " + str(self.suivant)


class Pile:
    def __init__(self, maillon=None):
        self.sommet = maillon

    def est_vide(self):
        return self.sommet is None

    def __str__(self):
        return str(self.sommet)

    def empile(self, element):
        nouveau_maillon = Maillon(element, self.sommet)
        self.sommet = nouveau_maillon

    def depile(self):
        if not self.est_vide():
            maillon = self.sommet
            element = maillon.valeur
            self.sommet = maillon.suivant
            return element
        else:
            return None


# --------- PYODIDE:tests --------- #

p = Pile()
assert p.est_vide()
p.depile()
p.empile(5)
assert not p.est_vide()
p.empile(8)
p.empile(2)
p.empile(9)
assert p.depile() == 9
assert p.depile() == 2

# --------- PYODIDE:secrets --------- #

# tests

p = Pile()
assert p.est_vide()
p.depile()
p.empile(5)
assert not p.est_vide()
p.empile(8)
p.empile(2)
p.empile(9)
assert p.depile() == 9
assert p.depile() == 2

# autres tests

p = Pile()
p.empile(1)
p.empile(2)
assert p.depile() == 2
p.empile(3)
p.empile(4)
assert p.depile() == 4
assert p.depile() == 3
p.empile(5)
assert p.depile() == 5
assert p.depile() == 1
assert p.est_vide() is True
p.empile(6)
p.empile(7)
assert p.est_vide() is False
assert p.depile() == 7
assert p.depile() == 6
assert p.est_vide()
