### `return None`

Dans la méthode `depile`, le `else: return None` n'est pas nécessaire puisque
si un appel de fonction ne se termine pas par un `return`, la valeur
`None` est automatiquement renvoyée.

L'exercice [File à partir d'une liste chainée](../../exercices/file_avec_liste/index.md) propose la représentation d'une file par une liste chainée.
