# --- exo --- #
""" # skip
def digicrack(digicode):
    for a in range(...):
        for b in ...:
            for c ...:
                for ...:
                    if digicode(...):
                        return ...
""" # skip


# --- vide --- #
def digicrack(digicode):
    ...


# --- corr --- #
def digicrack(digicode):
    for a in range(10):
        for b in range(10):
            for c in range(10):
                for d in range(10):
                    if digicode(a, b, c, d):
                        return (a, b, c, d)
# --- tests --- #
def digicode_1(a, b, c, d):
    return a + b - c - d == 0


a, b, c, d = digicrack(digicode_1)
assert digicode_1(a, b, c, d), f"Erreur avec le digicode_1"


def digicode_2(a, b, c, d):
    return 156*a - 820*b + 1463*c - 465*d == 7607


a, b, c, d = digicrack(digicode_2)
assert digicode_2(a, b, c, d), f"Erreur avec le digicode_2"

# --- secrets --- #
from random import randrange

for _ in range(10):
    attendu = tuple(randrange(0, 10) for _ in range(4))
    digicode = lambda a, b, c, d: (a, b, c, d) == attendu
    a, b, c, d = digicrack(digicode)
    assert digicode(a, b, c, d) is True, "Erreur avec un digicode secret"
