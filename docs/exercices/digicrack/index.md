---
author:
    - Jean Diraison
    - Romain Janvier
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Casseur de digicode
difficulty: 210
tags:
    - tuple
    - booléen
    - boucle
    - à trous
---

On cherche dans cet exercice à déterminer la combinaison d'un digicode. Celui-ci est représenté une fonction
`digicode` prenant quatre paramètres `a`, `b`, `c` et `d` correspondant chacun à un entier compris entre $0$ et $9$ (inclus l'un et l'autre) et renvoyant `#!py True` si la combinaison formée par ces quatre chiffres est valide et `#!py False` sinon.

On peut prendre par exemple :

```python
def digicode_1(a, b, c, d):
    return a + b - c - d == 0
```

L'appel `#!py digicode_1(1, 2, 3, 4)` renvoie `#!py False`. `#!py (1, 2, 3, 4)` n'est donc pas une combinaison valide.

Par contre `#!py digicode_1(4, 5, 4, 5)` renvoie `#!py True`. `#!py (4, 5, 4, 5)` est une combinaison valide.

??? note "Plusieurs solutions !"

    Ce digicode présente de nombreuses solutions valides (`#!py (0, 0, 0, 0)`, `#!py (3, 4, 5, 2)`...).
    
    De façon générale, un « digicode » peut avoir plusieurs solutions. On cherche à en déterminer une parmi celles-ci.
    

Vous devez écrire la fonction `digicrack` qui prend en paramètre une fonction `digicode` et renvoie
un tuple de quatre entiers, chacun compris entre $0$ et $9$, correspondant à une combinaison acceptée par le digicode.

On garantit que le « digicode » admet au moins une combinaison valide.

???+ example "Exemples"

    ```pycon title=""
    >>> def digicode_1(a, b, c, d):
    ...     return a + b - c - d == 0
    ...
    >>> digicrack(digicode_1)
    (4, 5, 4, 5)
    >>> def digicode_2(a, b, c, d):
    ...     return 156*a - 820*b + 1463*c - 465*d == 7607
    ...
    >>> digicrack(digicode_2)
    (3, 5, 8, 1)
    ```

??? tip "Indice"

    Votre fonction n'a pas besoin d'analyser le fonctionnement de la fonction `digicode`.
    
    Il n'y a pas tant de combinaisons possible que ça, vous pouvez toutes les tester ! :muscle:

    En cryptanalyse, on appelle cette méthode **attaque par force brute**.

=== "Version vide"
    {{ IDE('./pythons/exo_a') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_b') }}

{{ remarque('ordre_sup') }}