

# --------- PYODIDE:env --------- #

def somme_chiffres(n):
    ...

# --------- PYODIDE:code --------- #

def somme_chiffres(n):
    ...

# --------- PYODIDE:corr --------- #

def somme_chiffres(n):
    if n < 10:
        return n
    else:
        return n % 10 + somme_chiffres(n//10)

# --------- PYODIDE:tests --------- #

assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13

# --------- PYODIDE:secrets --------- #


# tests secrets
assert somme_chiffres(0) == 0
assert somme_chiffres(1) == 1
assert somme_chiffres(9) == 9
assert somme_chiffres(100000000000000000000000000) == 1
assert somme_chiffres(500000000000000000000000000) == 5
assert somme_chiffres(12345678901234567890) == 90