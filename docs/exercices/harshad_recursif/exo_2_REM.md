La fonction `harshad` est simple : on se contente de tester si le reste de la division de `n` par `somme_chiffres(n)` est égal à `0`. Si oui, le nombre est *harshad*, si non il ne l'est pas.
