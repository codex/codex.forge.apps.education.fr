---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Nombres harshad (récursif)
difficulty: 130
tags:
    - à trous
    - maths
    - récursivité
maj: 01/02/2024
---


Un entier naturel non nul $n$ est dit *harshad*, si $n$ est divisible par la somme des chiffres de $n$. Le nom de *harshad* a été donné par le mathématicien [Dattatreya Ramachandra Kaprekar](https://fr.wikipedia.org/wiki/Dattatreya_Ramachandra_Kaprekar) (1905 - 1986), il signifie "*grande joie*" en sanskrit.

On précise qu'un nombre entier $b$ est un « diviseur » de $a$ si le reste de la division euclidienne de $a$ par $b$ vaut $0$.

Par exemple $18$ est un nombre *harshad* car $1+8=9$ divise $18$. En effet $18 =2\times (1+8)+0$.

Le but est d'écrire une fonction permettant de déterminer si un nombre est un nombre de harshad ou non. Mais avant cela, il faudra écrire une fonction qui fait la somme des chiffres d'un nombre.


??? question "Question 1 : somme des chiffres"
    Écrire une fonction **récursive** `somme_chiffres` qui prend en paramètre un entier positif `n` et qui renvoie la somme des chiffres de `n`. On n'utilisera que des opérations mathématiques. On s'interdira donc de convertir `n` en une chaîne de caractères.


    !!! example "Exemples"

        ```pycon
        >>> somme_chiffres(8)
        8
        >>> somme_chiffres(18)
        9
        >>> somme_chiffres(409)
        13
        ```

    ???+ warning "Contrainte"

        On interdit dans cet exercice d'utiliser `#!py str` et `#!py sum`.

    ??? tip "Rappels sur la division euclidienne"
        Avec Python, on rappelle qu'il est possible de calculer le quotient d'un nombre `n` par `#!py 10` en faisant `#!py n // 10`.

        De même, `#!py n % 10` renvoie le reste de la division euclidienne de `n` par `#!py 10`.

        !!! example "Exemple"

            ```pycon
            >>> 409 % 10
            9
            >>> 409 // 10
            40
            >>> 40 % 10
            0
            >>> 40 // 10
            4
            ```


    === "Code vide"
        {{ IDE('exo_1a', SANS="str,sum") }}
    === "Code à compléter"
        {{ IDE('exo_1b', SANS="str,sum") }}


??? question "Question 2 : nombres de harshad"
    Vous devez écrire une fonction `harshad(n)` prenant en paramètre un nombre entier positif `n` et renvoyant `#!py True`  si `n` est un nombre *harshad* et `#!py False` sinon.

    Vous devez utiliser la fonction `somme_chiffres`. Même si vous n'avez pas réussi à traiter la question précédente, vous pouvez utiliser cette fonction qui est chargée automatiquement pour cette question.

    ??? tip "Tester si un nombre est divisible par un autre"
        On rappelle qu'un nombre $a$ est divisible par $b$ si le reste de la division euclidienne de $a$ par $b$ est 0.

    !!! example "Exemples"

        ```pycon
        >>> harshad(18)
        True
        >>> harshad(72)
        True
        >>> harshad(11)
        False
        ```

    {{ IDE('exo_2') }}

