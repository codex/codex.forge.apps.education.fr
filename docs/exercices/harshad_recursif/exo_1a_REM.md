*   Prenons un exemple : afin de calculer la somme des chiffres de $1409$ on doit additionner le chiffre des unités $9$, celui des dizaines $0$, celui des centaines $4$ et celui des milliers $1$.

    Le chiffre des unités vaut $9$. C'est le reste de la division de $1409$ par $10$. On l'obtient en Python en faisant `1409 % 10`.

    Ce calcul effectué, on calcule le quotient de $1409$ par $10$ : on obtient $140$. On l'obtient en Python en faisant `1409 // 10`.

    On peut alors recommencer la démarche (reste de la division suivie de quotient) jusqu'à ce que le quotient soit strictement inférieur à 10, pour obtenir successivement le chiffre des dizaines, des centaines, puis des milliers.

    | Étape | Valeur de $n$ | Reste de la division par $10$ | Quotient de la division par $10$ |
    | :---: | :-----------: | :---------------------------: | :------------------------------: |
    |   1   |     $1409$    |              $9$              |               $140$              |
    |   2   |     $140$     |              $0$              |               $14$               |
    |   3   |      $14$     |              $4$              |               $1$                |

    Remarquons qu'il n'est pas nécessaire de réaliser une quatrième étape pour obtenir le chiffre des milliers (qui est égal à $1$ ici) puisqu'il s'agit du quotient de la division par $10$ de l'étape 3.

*   Le fonction `somme_chiffres` est récursive. Le *cas de base* est atteint lorsque l'argument `n` est strictement inférieur à 10. On renvoie alors `n`.

    Dans les autres cas (`n` strictement positif), on renvoie le chiffre des unités de `n` additionnés à la somme des chiffres du quotient de `n` par `10`.

    Pour $1409$ on fait donc :

    $\begin{align*} 
    \text{somme_chiffres}(1409) &=  9 + \text{somme_chiffres}(140) \\ 
     &=  9 + 0 + \text{somme_chiffres}(14) \\
     &=  9 + 0 + 4 + \text{somme_chiffres}(1) \\
     &= 9 + 0 + 4 + 1\\
     &=14
    \end{align*}$
