# --- exo, 1 --- #
def somme_chiffres(n):
    if ...:
        ...
    else:
        ...

# --- vide, 1 | hdr, 1--- #
def somme_chiffres(n):
    ...

# --- corr, 1 | hdr, 2 --- #
def somme_chiffres(n):
    if n < 10:
        return n
    else:
        return n % 10 + somme_chiffres(n//10)

# --- tests, 1 --- #
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
# --- secrets, 1 --- #
assert somme_chiffres(0) == 0
assert somme_chiffres(1) == 1
assert somme_chiffres(9) == 9
assert somme_chiffres(100000000000000000000000000) == 1
assert somme_chiffres(500000000000000000000000000) == 5
assert somme_chiffres(12345678901234567890) == 90
# --- rem, 1 --- #
''' # skip
*   Prenons un exemple : afin de calculer la somme des chiffres de $1409$ on doit additionner le chiffre des unités $9$, celui des dizaines $0$, celui des centaines $4$ et celui des milliers $1$.

    Le chiffre des unités vaut $9$. C'est le reste de la division de $1409$ par $10$. On l'obtient en Python en faisant `1409 % 10`.

    Ce calcul effectué, on calcule le quotient de $1409$ par $10$ : on obtient $140$. On l'obtient en Python en faisant `1409 // 10`.

    On peut alors recommencer la démarche (reste de la division suivie de quotient) jusqu'à ce que le quotient soit strictement inférieur à 10, pour obtenir successivement le chiffre des dizaines, des centaines, puis des milliers.

    | Étape | Valeur de $n$ | Reste de la division par $10$ | Quotient de la division par $10$ |
    | :---: | :-----------: | :---------------------------: | :------------------------------: |
    |   1   |     $1409$    |              $9$              |               $140$              |
    |   2   |     $140$     |              $0$              |               $14$               |
    |   3   |      $14$     |              $4$              |               $1$                |

    Remarquons qu'il n'est pas nécessaire de réaliser une quatrième étape pour obtenir le chiffre des milliers (qui est égal à $1$ ici) puisqu'il s'agit du quotient de la division par $10$ de l'étape 3.

*   Le fonction `somme_chiffres` est récursive. Le *cas de base* est atteint lorsque l'argument `n` est strictement inférieur à 10. On renvoie alors `n`.

    Dans les autres cas (`n` strictement positif), on renvoie le chiffre des unités de `n` additionnés à la somme des chiffres du quotient de `n` par `10`.

    Pour $1409$ on fait donc :

    $\begin{align*} 
    \text{somme_chiffres}(1409) &=  9 + \text{somme_chiffres}(140) \\ 
     &=  9 + 0 + \text{somme_chiffres}(14) \\
     &=  9 + 0 + 4 + \text{somme_chiffres}(1) \\
     &= 9 + 0 + 4 + 1\\
     &=14
    \end{align*}$
''' # skip
# --- exo, 2 --- #
def harshad(n):
    ...

# --- corr, 2 --- #
def harshad(n):
    return n % somme_chiffres(n) == 0

# --- tests, 2 --- #
assert harshad(18) is True
assert harshad(72) is True
assert harshad(11) is False
# --- secrets, 2 --- #
HARSHAD = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 18, 20, 21, 24, 27, 30,
    36, 40, 42, 45, 48, 50, 54, 60, 63, 70, 72, 80, 81, 84, 90,
    100, 102, 108, 110, 111, 112, 114, 117, 120, 126, 132, 133,
    135, 140, 144, 150, 152, 153, 156, 162, 171, 180, 190, 192,
    195, 198, 200, 201, 204
]
for n in range(1, 1 + HARSHAD[-1]):
    assert harshad(n) == (n in HARSHAD), f"Erreur avec {n}"
# --- rem, 2 --- #
''' # skip
La fonction `harshad` est simple : on se contente de tester si le reste de la division de `n` par `somme_chiffres(n)` est égal à `0`. Si oui, le nombre est *harshad*, si non il ne l'est pas.
''' # skip
