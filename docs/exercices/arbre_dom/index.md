---
author: 
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Arbre du DOM
difficulty: 280
tags:
    - arbre
    - récursivité
    - programmation orientée objet
maj: 15/01/2025
---

L'arbre du DOM (pour *Document Object Model *) est une façon de représenter les documents HTML sous forme d'arbres dont les nœuds sont les balises et les contenus textuels du document. **On se limite dans cet exercice aux nœuds correspondant à des balises HTML.**

Considérons par exemple le document HTML ci-dessous (on n'a noté que les balises et pas leur contenu textuel) :

```html title="Document HTML"
<html>     
    <head>   
    </head>   
    <body>   
        <h1>   
          <div>
          </div>
          <p>
          </p>
      </h1>   
      <div>
          <p>  
          </p>  
      </div>
  </body>   
</html>     
```

On rappelle que la plupart des balises HTML fonctionnent par paires : une balise ouvrante (par exemple `#!html <p>`) et une balise fermante (dans ce cas `#!html </p>`).

Ce document HTML correspond à l'arbre ci-dessous :

![Arbre DOM](arbre_DOM.drawio.png){.autolight width=35% .center}

On représente les arbres de cet exercices à l'aide de la programmation orientée objet. On introduit donc une classe `Arbre` caractérisée par deux attributs :

* le nom le la balise à la racine de l'arbre (attribut `nom`, au format `#!py str`) ;
* la liste des sous-arbres de cet arbre (attribut `enfants` qui est une liste, potentiellement vide, d'éléments de type `#!py Arbre`).

```pycon title="Exemple d'utilisation"
>>> p = Arbre("p", [])  # un arbre représentant une balise 'p', sans enfants
>>> img = Arbre("img", [])
>>> div = Arbre("div", [p, img])
>>> div.nom
'div'
>>> print(div)
div  
  p  
  img
>>> for enfant in div.enfants:
...     print(enfant.nom)
...
p
img
```

??? note "La classe `#!py Arbre`"

    ```python title=""
    class Arbre:
        def __init__(self, nom, enfants):
            self.nom = nom
            self.enfants = enfants

        def __repr__(self, decalage=0):
            s = f"{' ' * decalage}{self.nom}\n"
            for enfant in self.enfants:
                s += enfant.__repr__(decalage + 2)
            return s
    ```
    
    Si `#!py a` est un objet de type `#!py Arbre`, on peut donc :
    
    * récupérer le nom de la balise en faisant `#!py a.nom` ;
    * parcourir ses enfants en faisant `#!py for e in a.enfants: ...`.

    -----------------------

    L'arbre du DOM utilisé dans les exemples est créé par le script ci-dessous :
    
    ```python title="Arbre utilisé dans les exercices"
    p1 = Arbre("p", [])
    div1 = Arbre("div", [p1])
    p2 = Arbre("p", [])
    div2 = Arbre("div", [])
    h1 = Arbre("h1", [div2, p2])
    body = Arbre("body", [h1, div1])
    head = Arbre("head", [])
    arbre_dom = Arbre("html", [head, body])
    ```

??? question "1. Nombre total de balises dans l'arbre"

    Écrire la fonction `#!py nb_balises` qui prend en paramètre un objet de type `#!py Arbre` représentant un arbre du DOM et renvoie le nombre total de balises qu'il contient.

    ???+ example "Exemples"

        ![Arbre DOM](arbre_DOM.drawio.png){.autolight width=35% .center}

        ```pycon title=""
        >>> nb_balises(body)
        6
        >>> nb_balises(arbre_dom)
        8
        ```

    {{ IDE('exo_nb_balises') }}

??? question "2. Nombre de balises d'un certain type"

    Écrire la fonction `#!py compte_balises` qui prend en paramètres un objet de type `#!py Arbre` représentant un arbre du DOM
    ainsi qu'une chaîne de caractères `#!py balise` et renvoie le nombre de balises de ce type présentes dans l'arbre.

    ???+ example "Exemples"

        ![Arbre DOM](arbre_DOM.drawio.png){.autolight width=35% .center}

        ```pycon title=""
        >>> compte_balises(arbre_dom, "p")
        2
        >>> compte_balises(arbre_dom, "div")
        2
        >>> compte_balises(arbre_dom, "img")
        0
        ```

    {{ IDE('exo_compte_balises') }}


??? question "3. Nombre de balises en *cascade*"

    Le style d'un élément de l'arbre DOM peut être défini à l'aide de règles CSS (pour *Cascading Style Sheet*).
    
    Une règle CSS s'applique à **certains éléments** de l'arbre et à **tous leurs descendants** (elle s'applique *en cascade*). 
    
    Par exemple, si une règle vise les éléments de type `#!HTML div`, elle s'appliquera à toutes les `#!HTML div` 
    mais aussi à tous les éléments contenus dans les sous-arbres de racine un élément `#!HTML div` **quels que soient leur type**.

    ![Arbre DOM](arbre_DOM_CSS.drawio.png){.autolight width=80% .center}

    Écrire la fonction `#!py compte_balises_cascade` qui prend en paramètres un objet de type `#!py Arbre` représentant un arbre du DOM
    ainsi qu'une chaîne de caractères `#!py balises` et renvoie le nombre de balises auxquelles s'appliquent une règle CSS visant ce type de balise.


    Une version corrigée des fonctions `#!py nb_balises` et `#!py compte_balises` est directement utilisable dans l'éditeur ci-dessous.

    ???+ example "Exemples"

        ![Arbre DOM](arbre_DOM.drawio.png){.autolight width=35% .center}

        ```pycon title=""
        >>> compte_balises_cascade(arbre_dom, "p")
        2
        >>> compte_balises_cascade(arbre_dom, "div")
        3
        >>> compte_balises_cascade(arbre_dom, "img")
        0
        ```

    {{ IDE('exo_compte_balises_cascade') }}

