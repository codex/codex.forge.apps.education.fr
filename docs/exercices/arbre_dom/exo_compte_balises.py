# --------- PYODIDE:env --------- #
class Arbre:
    def __init__(self, nom, enfants):
        self.nom = nom
        self.enfants = enfants

    def __repr__(self, decalage=0):
        s = f"{' ' * decalage}{self.nom}\n"
        for enfant in self.enfants:
            s += enfant.__repr__(decalage + 2)
        return s


def nb_balises(arbre):
    t = 1
    for enfant in arbre.enfants:
        t += nb_balises(enfant)
    return t


def compte_balises(arbre, balise): ...
def compte_balises_cascade(arbre, balise): ...


p1 = Arbre("p", [])
div1 = Arbre("div", [p1])
p2 = Arbre("p", [])
div2 = Arbre("div", [])
h1 = Arbre("h1", [div2, p2])
body = Arbre("body", [h1, div1])
head = Arbre("head", [])
arbre_dom = Arbre("html", [head, body])
# --------- PYODIDE:code --------- #
def compte_balises(arbre, balise): 
    ...


# --------- PYODIDE:corr --------- #
def compte_balises(arbre, balise):
    t = 0
    if arbre.nom == balise:
        t = 1
    for enfant in arbre.enfants:
        t += compte_balises(enfant, balise)
    return t


# --------- PYODIDE:tests --------- #
assert compte_balises(arbre_dom, "p") == 2
assert compte_balises(arbre_dom, "div") == 2
assert compte_balises(arbre_dom, "img") == 0
# --------- PYODIDE:secrets --------- #
arbre = p1
balise = "p"
attendu = 1
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
arbre = div1
balise = "p"
attendu = 1
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
balise = "div"
attendu = 1
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
ps = [Arbre("p", [Arbre("img", []), Arbre("code", [])]) for _ in range(4)]
nav = [
    Arbre("nav", [Arbre("ul", [Arbre("li", []), Arbre("li", []), Arbre("li", [])])])
    for _ in range(3)
]
div1 = Arbre("div", ps)
div2 = Arbre("div", nav)
h1 = Arbre("h1", [])
body = Arbre("body", [h1, div2, div1])
head = Arbre("head", [Arbre("title", []), Arbre("link", [])])
arbre_dom = Arbre("html", [head, body])

for arbre in ps:
    attendu = 1
    balise = "p"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
    attendu = 1
    balise = "img"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
    attendu = 0
    balise = "a"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
for arbre in nav:
    attendu = 0
    balise = "p"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
    attendu = 1
    balise = "ul"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
    attendu = 3
    balise = "li"
    assert (
        compte_balises(arbre, balise) == attendu
    ), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
arbre = div1
attendu = 1
balise = "div"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
attendu = 4
balise = "p"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
arbre = div2
attendu = 3
balise = "ul"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
attendu = 3
balise = "nav"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
arbre = h1
attendu = 1
balise = "h1"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
attendu = 0
balise = "h2"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
arbre = body
attendu = 2
balise = "div"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
attendu = 3
balise = "nav"
assert (
    compte_balises(arbre, balise) == attendu
), f"Erreur en cherchant {balise = } dans l'arbre ci-dessous :\n{arbre}"
