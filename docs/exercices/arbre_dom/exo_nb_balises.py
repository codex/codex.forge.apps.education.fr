# --------- PYODIDE:env --------- #
class Arbre:
    def __init__(self, nom, enfants):
        self.nom = nom
        self.enfants = enfants

    def __repr__(self, decalage=0):
        s = f"{' ' * decalage}{self.nom}\n"
        for enfant in self.enfants:
            s += enfant.__repr__(decalage + 2)
        return s


def nb_balises(arbre): ...
def compte_balises(arbre, balise): ...
def compte_balises_cascade(arbre, balise): ...

p1 = Arbre("p", [])
div1 = Arbre("div", [p1])
p2 = Arbre("p", [])
div2 = Arbre("div", [])
h1 = Arbre("h1", [div2, p2])
body = Arbre("body", [h1, div1])
head = Arbre("head", [])
arbre_dom = Arbre("html", [head, body])
# --------- PYODIDE:code --------- #
def nb_balises(arbre):
    ...


# --------- PYODIDE:corr --------- #
def nb_balises(arbre):
    t = 1
    for enfant in arbre.enfants:
        t += nb_balises(enfant)
    return t


# --------- PYODIDE:tests --------- #
assert nb_balises(body) == 6
assert nb_balises(arbre_dom) == 8
# --------- PYODIDE:secrets --------- #
arbre = p1
attendu = 1
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
arbre = div1
attendu = 2
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
ps = [Arbre("p", [Arbre("img", []), Arbre("code", [])]) for _ in range(4)]
nav = [
    Arbre("nav", [Arbre("ul", [Arbre("li", []), Arbre("li", []), Arbre("li", [])])])
    for _ in range(3)
]
div1 = Arbre("div", ps)
div2 = Arbre("div", nav)
h1 = Arbre("h1", [])
body = Arbre("body", [h1, div2, div1])
head = Arbre("head", [Arbre("title", []), Arbre("link", [])])
arbre_dom = Arbre("html", [head, body])

for arbre in ps:
    attendu = 3
    assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
for arbre in nav:
    attendu = 5
    assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
arbre = div1
attendu = 13
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
arbre = div2
attendu = 16
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
arbre = h1
attendu = 1
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
arbre = body
attendu = 31
assert nb_balises(arbre) == attendu, f"Erreur avec l'arbre ci-dessous :\n{arbre}"
