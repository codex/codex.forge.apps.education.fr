---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Redimensionner un tableau
tags:
    - à trous
    - grille
status: relecture
---

On considère un tableau de dimensions $l_1 \times h_1$ (largeur $l_1 > 0$ et hauteur $h_1 > 0$) et l'on souhaite modifier ses dimensions afin de créer un nouveau tableau de dimensions $l_2 \times h_2$.


Bien entendu, les deux tableaux contiendront autant de valeurs l'un et l'autre. On garantit donc que l'on aura toujours $l_1 \times h_1 = l_2 \times h_2$. Les éléments du nouveau tableau seront identiques à ceux du premier : lire les valeurs, ligne par ligne et de gauche à droite dans les deux tableaux donnera le même résultat.

Par exemple, le tableau de dimensions $4 \times 3$ :

$$
\begin{array}{|c|c|c|c|}
\hline
1&2&3&4\\
\hline
5&6&7&8\\
\hline
9&10&11&12\\
\hline
\end{array}
$$

pourra être transformé en un nouveau tableau de dimensions $6 \times 2$ :

$$
\begin{array}{|c|c|c|c|c|c|}
\hline
1&2&3&4&5&6\\
\hline
7&8&9&10&11&12\\
\hline
\end{array}
$$

Les tableaux seront représentés par des listes de listes Python. Ainsi les deux tableaux ci-dessus seront représentés par :

```python title=""
tab_1 = [
    [1,  2,  3,  4],
    [5,  6,  7,  8],
    [9, 10, 11, 12]
    ]
tab_2 = [
    [1, 2, 3,  4,  5,  6],
    [7, 8, 9, 10, 11, 12]
    ]
```

On doit tout d'abord créer un nouveau tableau aux bonnes dimensions. Le code Python ci-dessous permet de créer un tableau de 3 lignes sur 5 colonnes rempli de `#!py 0` :

```python title=""
tab_vide = [[0 for j in range(5)] for i in range(3)]
```

On doit ensuite mettre en correspondance les cellules du premier tableau et celles du second. Pour cela on propose la méthode suivante :

* on crée des coordonnées, par exemple `i` et `j`. `i` est initialisée à `#!py 0`, `j` à `#!py 0` ;

* on parcourt l'ensemble des lignes et, pour chacune, l'ensemble des valeurs du tableau de départ ;

* à chaque itération :

    * si `j` est égal à la nouvelle largeur, on passe à la ligne suivante (`#!py i += 1`) et on recommence à la première colonne (`#!py j = 0`),
    
    * on insère dans la cellule de coordonnées `i` et `j` du nouveau tableau la valeur lue dans la boucle,
    
    * on incrémente `j` pour passer à une nouvelle colonne.

Écrire la fonction `redimensionner` prenant en paramètres :

* le tableau `tableau` à redimensionner sous forme d'une liste de listes, toutes non vides ;

* la nouvelle largeur `nouvelle_largeur` ;

* la nouvelle hauteur `nouvelle_hauteur`.

Cette fonction renvoie un nouveau tableau redimensionné sous forme d'une liste de listes.

???+ example "Exemples"

    ```pycon title=""
    >>> tab = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> redimensionner(tab, 6, 2)
    [[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12]]
    >>> redimensionner(tab, 3, 4)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]
    ```

{{ IDE('exo') }}