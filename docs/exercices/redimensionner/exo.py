# --------- PYODIDE:code --------- #

def redimensionner(tableau, nouvelle_largeur, nouvelle_hauteur):
    nouveau_tab = [[0 for j in range(...)] for i in range(...)]

    i, j = ..., ...
    for ligne in tableau:
        for valeur in ligne:
            if j == ...:
                ...
                ...
            nouveau_tab[...][...] = valeur
            ... += 1
    return nouveau_tab

# --------- PYODIDE:corr --------- #

def redimensionner(tableau, nouvelle_largeur, nouvelle_hauteur):
    nouveau_tab = [[0 for j in range(nouvelle_largeur)] for i in range(nouvelle_hauteur)]

    i, j = 0, 0
    for ligne in tableau:
        for valeur in ligne:
            if j == nouvelle_largeur:
                i += 1
                j = 0
            nouveau_tab[i][j] = valeur
            j += 1
    return nouveau_tab


# --------- PYODIDE:tests --------- #

tab_1 = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
tab_2 = [[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12]]
tab_3 = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]
assert redimensionner(tab_1, 6, 2) == tab_2
assert redimensionner(tab_1, 3, 4) == tab_3

# --------- PYODIDE:secrets --------- #
tab = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
l, h = 12, 1
attendu = [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
assert redimensionner(tab, l, h) == attendu, f"Erreur en redimensionnant {tab = } aux dimensions {(l, h) = }"
l, h = 1, 12
attendu = [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]]
assert redimensionner(tab, l, h) == attendu, f"Erreur en redimensionnant {tab = } aux dimensions {(l, h) = }"
l, h = 2, 6
attendu = [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11, 12]]
assert redimensionner(tab, l, h) == attendu, f"Erreur en redimensionnant {tab = } aux dimensions {(l, h) = }"
tab = [
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
]
l, h = 5, 4
attendu = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20]
]
assert redimensionner(tab, l, h) == attendu, f"Erreur en redimensionnant {tab = } aux dimensions {(l, h) = }"