

# --------- PYODIDE:code --------- #

def nb_batiments_eclaires(hauteurs):
    ...

# --------- PYODIDE:corr --------- #

def nb_batiments_eclaires(hauteurs):
    resultat = 0
    plafond = 0
    for h in hauteurs:
        if h > plafond:
            plafond = h
            resultat += 1
    return resultat

# --------- PYODIDE:tests --------- #

assert 4 == nb_batiments_eclaires([2, 1, 4, 2, 0, 4, 5, 3, 5, 6])
assert 1 == nb_batiments_eclaires([0, 3, 1, 2])

# --------- PYODIDE:secrets --------- #


# autres tests
# assert 0 == nb_batiments_eclaires([]) la liste ne peut pas être vide d'après l'énoncé
assert 0 == nb_batiments_eclaires([0]), "erreur avec [0]"
assert 0 == nb_batiments_eclaires([0] * 10), "erreur avec [0] * 10"
assert 1 == nb_batiments_eclaires([1]), "erreur avec [1]"
assert 1 == nb_batiments_eclaires([10, 5]), "erreur avec [10, 5]"
assert 2 == nb_batiments_eclaires([5, 10]) , "erreur avec [5, 10]"
assert 1 == nb_batiments_eclaires([0, 10, 5, 0]), "erreur avec [0, 10, 5, 0]"
assert 2 == nb_batiments_eclaires([0, 5, 10, 0]), "erreur avec [0, 5, 10, 0]"
assert 2 == nb_batiments_eclaires([1, 10, 5, 1]), "erreur avec [1, 10, 5, 1]"
assert 3 == nb_batiments_eclaires([1, 5, 10, 1]), "erreur avec [1, 5, 10, 1]"
assert 1 == nb_batiments_eclaires([10] * 5), "erreur avec [10]*5"
assert 1 == nb_batiments_eclaires([0, 10, 10, 10, 0]), "erreur avec [0, 10, 10, 10, 0]"