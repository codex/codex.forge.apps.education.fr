

# --------- PYODIDE:env --------- #

def repartir(a_diviser, nb_parts):
    """Ne pas modifier"""
    quotient, reste = divmod(a_diviser, nb_parts)
    repartition = [quotient for _ in range(nb_parts)]
    for i in range(reste):
        repartition[i] += 1
    return repartition

# --------- PYODIDE:code --------- #

def degrade(couleur_1, couleur_2, nb):
    ...

# --------- PYODIDE:corr --------- #

def degrade(couleur_1, couleur_2, nb):
    r1, v1, b1 = couleur_1
    r2, v2, b2 = couleur_2
    complements_r = repartir(r2 - r1, nb + 1)
    complements_v = repartir(v2 - v1, nb + 1)
    complements_b = repartir(b2 - b1, nb + 1)
    resultat = [couleur_1]
    for i in range(nb + 1):
        r, v, b = resultat[i]
        delta_r = complements_r[i]
        delta_v = complements_v[i]
        delta_b = complements_b[i]
        resultat.append((r + delta_r, v + delta_v, b + delta_b))
    return resultat

# --------- PYODIDE:tests --------- #

assert degrade((150, 80, 120), (230, 20, 160), 3) == [
    (150, 80, 120),
    (170, 65, 130),
    (190, 50, 140),
    (210, 35, 150),
    (230, 20, 160),
], "Échec exemple 1"
assert degrade((10, 100, 220), (102, 200, 178), 4) == [
    (10, 100, 220),
    (29, 120, 212),
    (48, 140, 204),
    (66, 160, 196),
    (84, 180, 187),
    (102, 200, 178),
], "Échec exemple 2"

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert degrade((185, 171, 84), (241, 217, 90), 2) == [(185, 171, 84), (204, 187, 86), (223, 202, 88), (241, 217, 90)]
assert degrade((227, 54, 175), (208, 241, 58), 5) == [
    (227, 54, 175),
    (224, 86, 156),
    (221, 117, 137),
    (218, 148, 118),
    (215, 179, 98),
    (212, 210, 78),
    (208, 241, 58),
]
assert degrade((0, 0, 0), (0, 0, 0), 2) == [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)]
assert degrade((20, 30, 40), (100, 100, 100), 1) == [(20, 30, 40), (60, 65, 70), (100, 100, 100)]