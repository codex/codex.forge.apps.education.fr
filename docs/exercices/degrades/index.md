---
author:
    - Sébastien Hoarau
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Dégradés de couleurs
tags:
    - à trous
    - liste/tableau
    - tuple
    - int
difficulty: 250
---



Bob est _webdesigner_ et triste. Il a l'habitude d'utiliser un outil en ligne pour créer de beaux dégradés de couleurs pour ses sites web :

![image blender color](blender.png){ .center}

 Mais aujourd'hui le site est hors-ligne... Nous allons aider Bob en écrivant une fonction qui génère le dégradé. 
 
 Une couleur est codée par un triplet $(r, v, b)$ qui représente dans l'ordre la quantité de rouge, de vert et de bleu. Chaque composante est un **entier** entre $0$ et $255$. 

L'outil de Bob prend trois paramètres :

- la couleur de départ `couleur_1` ;

- la couleur d'arrivée `couleur_2` ;

- le nombre `nb` de points intermédiaires strictement positif.

Le résultat est une liste ordonnée de couleurs `[couleur_1, intermediaire_1, ..., intermediaire_nb, couleur_2]` comportant `nb + 2` tuples : la couleur de départ, les `nb` couleurs intermédiaires et la couleur d'arrivée.

???+ tip "Calcul des couleurs intermédiaires"

    Voilà, sur un exemple, comment calculer les couleurs intermédiaires, c'est-à-dire les valeurs intermédiaires de chacune des composantes. On se donne deux couleurs `#!py couleur_1 = (100, 200, 50)` et `#!py couleur_2 = (150, 100, 100)`.

    Supposons que l'on souhaite $3$ couleurs intermédiaires. Pour la première composante, on doit passer de $100$ à $150$ sur $4$ intervalles (en incluant la couleur d'arrivée). Il faut donc répartir $50$ points en $4$, par exemple $50=13+ 13 + 12 + 12$. 

    La composante rouge de la couleur `intermediaire_1` vaudra $113 = 100 + 13$. Celle de la couleur `intermediaire_2` vaudra $126 = 113 + 13$. On procède de même pour les autres couleurs intermédiaires et pour les composantes verte et bleue.

    On fournit une fonction `repartir` qui prend deux entiers en paramètres : `a_diviser` qui est la quantité à diviser équitablement (dans notre exemple $50$) et `nb_parts` en combien on doit couper la valeur `a_diviser` ($4$ dans l'exemple).
    
    ```pycon
    >>> repartir(50, 4)
    [13, 13, 12, 12]
    >>> repartir(1, 2)
    [1, 0]
    >>> repartir(0, 2)
    [0, 0]
    ```
    
    Cette fonction est déjà importée dans le fichier : vous pouvez directement l'utiliser. Pour information, voici son code :
    
    ```python
    def repartir(a_diviser, nb_parts):
        quotient, reste = divmod(a_diviser, nb_parts)
        repartition = [quotient for _ in range(nb_parts)]
        for i in range(reste):
            repartition[i] += 1
        return repartition
    ```
    
    On utilise la fonction `divmod` qui calcule et renvoie efficacement le quotient et le reste des deux entiers passés en argument.

Compléter la fonction `degrade` qui prend les mêmes trois paramètres dont l'outil de Bob a besoin et renvoie la liste des couleurs formant le dégradé comme expliqué ci-dessus.

??? note "Aides"

    Lorsqu'on a un triple `mon_triplet`, on peut utiliser le _décompactage_ (*unpacking* en anglais) pour récupérer les 3 valeurs dans des variables, comme ceci : `a, b, c = mon_triplet`
    
    Pour ajouter un tuple à la fin d'une liste on doit utiliser une double paire de parenthèses : `#!py ma_liste.append((x, y))`.

???+ example "Exemples"

      ```pycon
      >>> C1 = (150, 80, 120)
      >>> C2 = (230, 20, 160)
      >>> degrade(C1, C2, 3)
      [(150, 80, 120), (170, 65, 130), (190, 50, 140), (210, 35, 150), (230, 20, 160)]
      ```

      Dans cet exemple tous les intervalles de valeurs étaient divisibles par $4$, donnant un dégradé parfaitement uniforme. Ce n'est pas forcément le cas, ce n'est pas grave : certains intervalles seront un peu plus grand que les autres c'est tout (un exemple où on divise par $5$) :

      ```pycon
      >>> C3 = (10, 100, 220)
      >>> C4 = (102, 200, 178)
      >>> degrade(C3, C4, 4)
      [(10, 100, 220), (29, 120, 212), (48, 140, 204), (66, 160, 196), (84, 180, 187), (102, 200, 178)]
      ```

      Ici les deux premiers intervalles sont plus grands que les trois autres (de $1$).


=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
