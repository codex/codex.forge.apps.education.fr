---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Jointure de dictionnaires
tags: 
    - dictionnaire
difficulty: 220
---



Le responsable d'un site web dispose de deux tableaux contenant des informations sur les abonnés du site :

* le premier tableau contient l'adresse email de chaque abonné ainsi que son identifiant (un nombre entier unique)
* le second tableau contient l'identifiant de **certains** abonnés ainsi que leur "pseudo" 

Toutes les adresses email et tous les identifiants sont uniques.

Par exemple :

* le premier tableau

|     Email      |  ID   |
| :------------: | :---: |
| alice@fake.com |   1   |
|  bob@bidon.fr  |   2   |
| chris@false.uk |   3   |

* le second tableau :
  
|  ID   | Pseudo |
| :---: | :----: |
|   1   | alice  |
|   2   |  B0b   |

Comme on peut le voir, le second tableau ne contient pas nécessairement les informations de **tous** les abonnés.

Ces tableaux sont codés en Python sous forme de dictionnaires. On aura ainsi :

```python
emails_ids = {'alice@fake.com': 1, 'bob@bidon.fr': 2, 'chris@false.uk': 3}
ids_pseudos = {1: 'alice', 2: 'B0b'}
```

Le responsable du site vous demande d'associer à chaque adresse email, le pseudo correspondant. Si un abonné n'a pas de pseudo associé, on donnera son adresse email à la place.

Vous devez donc écrire en Python la fonction `jointure` qui :

* prend en argument les deux dictionnaires
* renvoie le dictionnaire dont les clés sont les adresses email des abonnés et les valeurs leur pseudo (ou leur adresse email s'ils n'ont pas renseigné de pseudo).

???+ example "Exemples"

    ```pycon title=""
    >>> emails_ids = {'alice@fake.com': 1, 'bob@bidon.fr': 2, 'chris@false.uk': 3}
    >>> ids_pseudos = {1: 'alice', 2: 'B0b', 3: 'ChristoF'}
    >>> jointure(emails_ids, ids_pseudos)
    {'alice@fake.com': 'alice', 'bob@bidon.fr': 'B0b', 'chris@false.uk': 'ChristoF'}
    ```
    
    ```pycon title=""
    >>> emails_ids = {'alice@fake.com': 1, 'bob@bidon.fr': 2, 'chris@false.uk': 3}
    >>> ids_pseudos = {1: 'alice', 2: 'B0b'}
    >>> jointure(emails_ids, ids_pseudos)
    {'alice@fake.com': 'alice', 'bob@bidon.fr': 'B0b', 'chris@false.uk': 'chris@false.uk'}
    ```

{{ IDE('exo') }}
