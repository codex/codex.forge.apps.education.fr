# --------- PYODIDE:env --------- #


def comparer(dico1, dico2):
    for cle in dico1:
        if cle not in dico2:
            return False
        if not sorted(dico1[cle]) == sorted(dico2[cle]):
            return False
    return True


# --------- PYODIDE:code --------- #


def abonnes(reseau): 
    ...


# --------- PYODIDE:corr --------- #


def abonnes(reseau):
    reseau_abonnes = {utilisateur: [] for utilisateur in reseau}
    for utilisateur in reseau:
        for personne in reseau[utilisateur]:
            reseau_abonnes[personne].append(utilisateur)
    return reseau_abonnes

# --------- PYODIDE:tests --------- #

petit = {"Anna": ["Basile"], "Basile": []}
assert abonnes(petit) == {"Anna": [], "Basile": ["Anna"]}

immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}


assert comparer(
    abonnes(immediam),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Carl": [],
        "Eroll": ["Billy", "Gaby"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll"],
    },
)


# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
immediam_2 = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert comparer(
    abonnes(immediam_2),
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll", "Gaby"],
        "Eroll": ["Flynn", "Gaby"],
        "Carl": [],  # rajouté
        "Charles": [],  # rajouté

    },
)

# J'ai commenté cette fonction car pour les tests il me semble qu'il faut tester la fonction etre_aime
#def abonnes(reseau):
#   resultat = {personne: [] for personne in reseau}
#    for personne in reseau:
#        for personne_suivie in reseau[personne]:
#            resultat[personne_suivie].append(personne)
#    return resultat




immediam_3 = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}


assert comparer(
    abonnes(immediam_3),   
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Eroll": ["Billy", "Gaby"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll"],
        "Carl": [],  # rajouté
    },
)


immediam_4 = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert comparer(
    abonnes(immediam_4),    # J'ai remplacé abonnes par etre_aime
    {
        "Billy": ["Anna", "Carl", "Eroll"],
        "Anna": ["Billy"],
        "Gaby": ["Dora", "Eroll", "Flynn"],
        "Dora": ["Eroll"],
        "Flynn": ["Eroll", "Gaby"],
        "Eroll": ["Flynn", "Gaby"],
        "Carl": [],  # rajouté
        "Charles": [],  # rajouté
    },
)