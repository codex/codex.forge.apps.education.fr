On a ainsi converti des listes de successeurs en listes de prédécesseurs.

A noter que décrire un graphe au travers des listes de prédécesseurs revient à décrire le **graphe transposé**, graphe qui a les mêmes sommets mais avec le sens des arcs inversé.

Si dans cette représentation, une flèche allant d'un sommet B vers un
sommet A indique que l'utilisateur B « suit » l'utilisateur A :

![graphe](graphe.svg){width=30% .autolight .center}

dans le graphe transposé, la flèche allant du sommet Α vers le
sommet B indique que l'utilisateur A « est suivi » par l'utilisateur B :

![graphe](graphe_t.svg){width=30% .autolight .center}

```python
def transpose(reseau): 
    reseau_t = {sommet: [] for sommet in reseau}
    for sommet, liste_successeurs in reseau.items():
        for successeur in liste_successeurs:
            reseau_t[successeur].append(sommet)
    return reseau_t
```

On remarquera que si on transpose à nouveau un graphe transposé, on retrouve le graphe initial :

```pycon
>>> graphe_transpose = transpose(reseau)
>>> graphe_transpose_transpose = transpose(graphe_transpose)
>>> comparer(graphe_transpose_transpose, reseau)
True
```