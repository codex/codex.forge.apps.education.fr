---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Alien (4) - Boucles bornées
tags:
    - SNT
difficulty : 050
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :

    
    * « {{ lien_exo("Alien (1) : Appels de fonctions", "alien_1") }} »,

    * « {{ lien_exo("Alien (2) : Variables et affectations", "alien2") }} »,
    
    * « {{ lien_exo("Alien (3) : Instructions conditionnelles", "alien3") }} »,
    
    * « {{ lien_exo("Alien (4) : Boucles bornées", "alien4") }} »,
    
    * « {{ lien_exo("Alien (5) : Fonctions", "alien5") }} »,
    
    * « {{ lien_exo("Alien (6) : Tableaux", "alien6") }} ».

Les règles sont simples : l'alien :alien: se situe au départ au centre de la grille et peut être déplacé avec les fonctions `haut`, `bas`, `gauche` et `droite`.

L'objectif est de trouver la case finale de l'alien (et donc son parcours) après exécution du programme donné.

??? example "Rappels des différentes instructions"

    * `haut(4)` déplace l'alien de `#!py 4` cases vers le haut ;
    * `haut(-2)` déplace l'alien de `#!py 2` cases vers le bas ;
    * `haut()` déplace l'alien de `#!py 1` case vers le haut (c'est donc équivalent à `#!py haut(1)`).

    Le principe est similaire avec les trois autres fonctions.

??? info "Consignes"
    Dans cet exercice, on trouve deux types de questions :

    * **Dessinez le parcours** : dessinez le parcours de l'alien en cliquant sur **la case d'arrivée** de chaque instruction exécutée. Vous pourrez ensuite valider votre parcours pour vérifier s'il est correct.

    * **Codez le parcours** : écrire le code nécessaire pour obtenir le déplacement souhaité (les numéros correspondent aux différentes étapes).

    !!! warning "Attention"
        Le code ne devra pas dépasser les $10$ lignes.



!!! abstract "Boucle bornée"
    Une **instruction itérative**, ou **boucle**, permet de répéter plusieurs fois une séquences d'instructions.
    
    ```python
    for loop in range(2) : 
        haut(3)
    ``` 
    Le nombre entre parenthèses dans `#!py range(nombre)` indique le nombre de répétitions des instructions. Dans cet exemple, on répète $2$ fois un déplacement de l'alien de $3$ cases vers le haut.
     
    Les instructions répétées dans la boucle (corps) doivent être décalées à l'aide de la touche tabulation. 

??? question "Question 1: Dessinez le parcours"
    ```python { .inline .end .w45 }
    for _ in range(5) :
        gauche(1)
        haut(1)
    ```
    {{ run('scripts/exo1') }}
    {{ figure("figure1",
            inner_text="L'image est en train de se charger", 
            admo_title="Dessinez le parcours") }}


??? question "Question 2 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    gauche(5)
    for _ in range(3) :
        haut(1)
        droite(2)
    bas(4)
    ```
    {{ run('scripts/exo2') }}
    {{ figure("figure2",
            inner_text="L'image est en train de se charger", 
            admo_title="Dessinez le parcours") }}



??? question "Question 3 : Codez le parcours"


    ![alien](images/alien32.png)

    {{ IDE('scripts/exo5') }}
    {{ figure("figure5",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 4 : Codez le parcours"

    ![alien](images/alien33.png)

    {{ IDE('scripts/exo6') }}
    {{ figure("figure6",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}



!!! abstract "Compteur"

    Une boucle avec **compteur** permet de répéter des instructions un certain nombre de fois tout en mettant à jour automatiquement une variable compteur qui est initialisée à zéro.
    
    ```python
    for compteur in range(3):
        droite(compteur)
    ```   
    La variable `compteur` est automatiquement initialisée à $0$ et automatiquement augmentée de $1$ à la fin de chaque tour de boucle (sauf au dernier). Elle prendra donc tour à tour les valeurs $0$, $1$ et $2$


??? question "Question 5 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    bas(7)
    droite(1)
    for a in range(3) :
        haut(a+1)
    ```
    {{ run('scripts/exo3') }}
    {{ figure("figure3",
            inner_text="L'image est en train de se charger", 
            admo_title="Dessinez le parcours") }}   



??? question "Question 6 : Codez le parcours"

    {{ run('scripts/grille2') }}
    {{ figure("grille2",admo_title="Figure attendue") }}

    {{ IDE('scripts/exo8') }}
    {{ figure("figure8",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

!!! abstract "Bornes"
    Il est possible d'indiquer la borne de début (incluse) et de fin (exclue).
    
    ```python
    for compteur in range(2, 6):
        droite(compteur)
    ```   
    La variable `compteur` est automatiquement initialisée à $2$ et automatiquement augmentée de $1$ à la fin de chaque tour jusqu'à $6$ exclu (il ne prendra pas cette valeur).

??? question "Question 7 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    gauche(7)
    bas(7)
    for a in range(1, 4) :
        droite(a)
        haut(a)
    gauche(2)
    ```

    {{ run('scripts/exo4') }}
    {{ figure("figure4") }}

??? question "Question 8 : Codez le parcours"

    {{ run('scripts/grille1') }}
    {{ figure("grille1",admo_title="Figure attendue") }}

    {{ IDE('scripts/exo7') }}
    {{ figure("figure7",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}



