# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app5 = App("grille1")

app5.bas(2)
app5.gauche(5)
for i in range(1,4) :
    app5.haut(i)
    app5.droite(i+2)
app5.bas(2)

app5.afficher_deplacement(etapes=True, num_lignes=False)

