# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = len(__USER_CODE__.split("\n"))<11
assert valide is True, "le code ne doit pas dépasser les 10 lignes !"
app.centrer("figure5") 


# --------- PYODIDE:code --------- #
haut(...)
...
    gauche(...)
haut(...)
...
    droite(...)
bas(...)



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'E8', 'E6', 'E4', 'E2', 'B2', 'B5', 'B8', 'B11', 'B14', 'F14'])