# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app1 = App("figure4")

app1.gauche(7)
app1.bas(7)
for a in range(1, 4) :
    app1.droite(a)
    app1.haut(a)
app1.gauche(2)

app1.dessiner_parcours()