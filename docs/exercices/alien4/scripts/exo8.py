# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = len(__USER_CODE__.split("\n"))<11
assert valide is True, "le code ne doit pas dépasser les 10 lignes !"
app.centrer("figure8") 


# --------- PYODIDE:code --------- #
droite(...)
haut(...)
for i in range(...) :
    gauche(i+1)
    bas(...)



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H11', 'D11', 'D10', 'E10', 'E8', 'G8', 'G5', 'J5', 'J1','N1'])