# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = len(__USER_CODE__.split("\n"))<11
assert valide is True, "le code ne doit pas dépasser les 10 lignes !"
app.centrer("figure6") 


# --------- PYODIDE:code --------- #
...
droite(...)
for _ in range(...) :
    bas(...)
    gauche(...)
    ...
    droite(...)
haut(...)



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'A8', 'A15', 'C15', 'C11', 'E11', 'E14', 'G14', 'G10', 'I10', 'I13', 'K13', 'K9', 'M9', 'M12', 'L12'])