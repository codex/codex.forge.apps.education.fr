# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

valide = len(__USER_CODE__.split("\n"))<11
assert valide is True, "le code ne doit pas dépasser les 10 lignes !"
app.centrer("figure7") 


# --------- PYODIDE:code --------- #
bas(...)
gauche(...)
for i in range(...,...) :
    haut(i)
    droite(...)
bas(2)



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'J8', 'J3', 'I3', 'I6', 'G6', 'G10', 'D10', 'D15', 'F15'])