# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5

app.centrer("figure5") 
valide = len(__USER_CODE__.split("\n"))<11
assert valide is True, "le code ne doit pas dépasser les 10 lignes !"
# --------- PYODIDE:code --------- #
bas(...)
gauche(...)
for _ in range(...) :
    haut(...)
    droite(...)
bas(...)



# --------- PYODIDE:post --------- #
if valide : 
    app.verifier_programme(['H8', 'K8', 'K3', 'I3', 'I6', 'G6', 'G9', 'E9', 'E12', 'C12', 'C15', 'E15'])