On peut aussi utiliser un générateur afin de parcourir les différents sous_arbres :

```python
def hauteur(arbre):
    if arbre == []:
        return 0
    return 1 + max(hauteur(sous_arbre) for sous_arbre in arbre)
```