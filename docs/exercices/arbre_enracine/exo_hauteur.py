# --------- PYODIDE:code --------- #
def hauteur(arbre): 
    ...


# --------- PYODIDE:corr --------- #
def hauteur(arbre):
    if arbre == []:
        return 0
    h = 0
    for sous_arbre in arbre:
        h = max(h, hauteur(sous_arbre))
    return 1 + h


# --------- PYODIDE:tests --------- #
assert hauteur([]) == 0
assert hauteur([[], [], [[]]]) == 2
# --------- PYODIDE:secrets --------- #
arbre = [[]]
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], []]
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [], []]
attendu = 1
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]], [[]], [], []]
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]], []]
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[[]], []]
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]]]
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[[]]]
attendu = 2
assert hauteur(arbre) == attendu, f"Erreur avec {arbre = }"
