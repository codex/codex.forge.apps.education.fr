# --------- PYODIDE:code --------- #
def taille(arbre): 
    ...


# --------- PYODIDE:corr --------- #
def taille(arbre):
    t = 1
    for sous_arbre in arbre:
        t = t + taille(sous_arbre)
    return t


# --------- PYODIDE:tests --------- #
assert taille([]) == 1
assert taille([[], [], [[]]]) == 5
# --------- PYODIDE:secrets --------- #
arbre = [[]]
attendu = 2
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], []]
attendu = 3
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [], []]
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]], [[]], [], []]
attendu = 8
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]], []]
attendu = 5
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[[]], []]
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[], [[]]]
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = [[[]]]
attendu = 3
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
