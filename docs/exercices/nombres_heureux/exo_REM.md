Avec les opérateurs `%` et `//` et une fonction qui calcule la somme des carrés des chiffres de `n`:

```python
def somme_carres(n):
    somme = 0
    while n != 0:
        somme = somme + (n%10)**2
        n = n // 10
    return somme
    
def heureux(n):
    if n == 0:
        return False
    while True:
        if n == 1:
            return True
        elif n == 4:
            return False
        else:
            n = somme_carres(n)
```
