

# --------- PYODIDE:code --------- #

def heureux(n):
    ...

# --------- PYODIDE:corr --------- #

def heureux(n):
    if n == 0:
        return False
    while n != 1 and n !=4:
        s = 0
        for c in str(n):
            s += int(c) ** 2
        n = s
    if n == 1:
        return True
    return False

# --------- PYODIDE:tests --------- #

for i in range(10):
    if i == 1 or i == 7:
        assert heureux(i)
    else:
        assert not heureux(i)

# --------- PYODIDE:secrets --------- #

# tests
for i in range(10):
    if i == 1 or i == 7:
        assert heureux(i), f"Erreur avec {i = }"
    else:
        assert not heureux(i), f"Erreur avec {i = }"

# tests secrets
H = {1, 7, 10, 13, 19, 23, 28, 31, 32, 44, 49, 68, 70, 79, 82, 86, 91, 94, 97,
100, 103, 109, 129, 130, 133, 139, 167, 176, 188, 190, 192, 193, 203, 208,
219, 226, 230, 236, 239, 262, 263, 280, 291, 293}
for i in range(300):
    if i in H: assert heureux(i), f"Erreur avec {i = }"
    else: assert not heureux(i), f"Erreur avec {i = }"
