---
author: Serge Bays
hide:
    - navigation
    - toc
title: Nombres heureux
tags:
    - maths
    - int
difficulty: 200
maj: 01/03/2024
---



On considère un entier naturel $n$ et on calcule la somme $s$ des carrés des chiffres de $n$. Par exemple si $n = 32$ alors $s = 9+4=13$.
On recommence avec $n=13$ et on obtient $s=1+9=10$. On recommence avec $n=10$ et on obtient $s=1+0=1$. Si on recommence on obtient toujours $1$.
On démontre qu'en appliquant ce processus, soit on reste à $0$ (si $n=0$), soit on arrive à $1$ et on y reste, soit on arrive à $4$ 
et on va reproduire la boucle $4, 16, 37, 58, 89, 145, 42, 20, 4$.

En appliquant le processus décrit ci-dessus à un nombre $n$, si on arrive à $1$, on dit que le nombre $n$ est *heureux*.


Écrire la fonction `heureux` qui prend en paramètre un entier `n` positif et renvoie `True` si le nombre est heureux et `False`sinon.

!!! info "Indice"
    On pourra utiliser les fonctions `str` et `int` pour obtenir les chiffres à partir de l'écriture d'un nombre. Par exemple `int(str(387)[1])`est le nombre 8.
    Une autre possibilité est d'utiliser les opérateurs `%` et `//`.

???+ example "Exemples"

    ```pycon title=""
    >>> heureux(32)
    True
    >>> heureux(58)
    False
    >>> heureux(0)
    False
    ```
    
{{ IDE('exo') }}
