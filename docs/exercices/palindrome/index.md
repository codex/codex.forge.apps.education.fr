---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Création d'un palindrome
tags:
    - string
difficulty: 238
---


Un palindrome est un mot qui se lit lettre par lettre, de gauche à droite, exactement comme de droite à gauche.

???+ example "Exemples"

    - `#!py "kayak"`, `#!py "radar"`, `#!py "rdtxtdr"` sont des palindromes de longueur impaire.
    - `#!py "elle"`, `#!py "serres"`, `#!py "afeefa"` sont des palindromes de longueur paire.

On peut construire un palindrome à partir d'une chaine de caractères et d'un **autre palindrome**, en concaténant la chaine, le palindrome et la chaine renversée.

1. `#!py "kayak"` peut s'obtenir avec `#!py "ka", "y", "ak"`
2. `#!py "radar"` peut s'obtenir avec `#!py "r", "ada", "r"`
3. `#!py "rdtxtdr"` peut s'obtenir avec `#!py "rd", "txt", "dr"`
4. `#!py "elle"` peut s'obtenir avec `#!py "e", "ll", "e"`
5. `#!py "serres"` peut s'obtenir avec `#!py "ser", "", "res"`
6. `#!py "arfettttefra"` peut s'obtenir avec `#!py "ar", "fettttef", "ra"`

Écrire une fonction `cree_palindrome` qui prend deux paramètres : une chaine de caractères `mot` et une chaine de caractères qui sera `palindrome`. La fonction renvoie le palindrome créé en concaténant `mot`, `palindrome`, et le renversement de `mot`.

???+ example "Exemples"

    ```pycon title=""
    >>> cree_palindrome("ka", "y")
    'kayak'
    >>> cree_palindrome("ser", "")
    'serres'
    >>> cree_palindrome("r", "ada")
    'radar'
    >>> cree_palindrome("ar", "fettttef")
    'arfettttefra'
    ```

On garantit que `palindrome` est bien un palindrome, il sera inutile de le vérifier.

???+ warning "Contraintes"

    **On n'utilisera pas** les tranches de chaines de caractères ni les fonctions `#!py reverse*`.

{{ IDE('exo', SANS=".reverse, reversed") }}
