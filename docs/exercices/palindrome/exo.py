

# --------- PYODIDE:code --------- #

def cree_palindrome(mot, palindrome):
    ...

# --------- PYODIDE:corr --------- #

def cree_palindrome(mot, palindrome):
    sortie = mot + palindrome
    l = len(mot)
    for i in range(l):
        sortie += mot[l - 1 - i]
    return sortie

# --------- PYODIDE:tests --------- #

assert cree_palindrome("ka", "y") == "kayak"
assert cree_palindrome("ser", "") == "serres"
assert cree_palindrome("r", "ada") == "radar"
assert cree_palindrome("ar", "fettttef") == "arfettttefra"

# --------- PYODIDE:secrets --------- #


# autres tests

assert cree_palindrome("", "") == ""
assert cree_palindrome("", "a") == "a"
assert cree_palindrome("a", "") == "aa"
assert cree_palindrome("abcd", "tyuuyt") == "abcdtyuuytdcba"
assert cree_palindrome("abcde", "tyuzuyt") == "abcdetyuzuytedcba"