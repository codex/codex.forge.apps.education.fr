---
license: "by-nc-sa"
author: 
    - Franck Chambon
    - Pierre Marquestaut
difficulty: 350
hide:
    - navigation
    - toc
title: Recherche dans une grille triée
tags:
    - grille
    - diviser pour régner
---

On considère un tableau bidimensionnel d'éléments distincts, dont chaque ligne et chaque colonne est **triée dans l'ordre croissant**. On souhaite savoir si un élément `cible` est présent dans ce tableau.

???+ example "Exemple"
    Cette grille possède bien chaque ligne et chaque colonne triées.

    $$
    \begin{matrix}
        11 & 33 & \mathbf{42} & 63 \\
        20 & 52 & 67 & 80 \\
        25 & 61 & 88 & 95 \\
    \end{matrix}
    $$

    - $42$ est présente à la ligne 0, colonne 2.
    - $24$ est absent.

!!! info "Fonctions à utliser"
    La `grille` utilisée ici **ne sera pas** une liste Python et vous n'avez pas un accès direct aux données qu'elle contient.
    
    En revanche,  vous avez à votre disposition trois fonctions (déjà chargées en mémoire) telles que :

    1. `nb_lignes(grille)` renvoie le nombre $n$ de lignes de la grille.
    2. `nb_colonnes(grille)` renvoie le nombre $m$ de colonnes de la grille.
    3. `donne_valeur(grille, i, j)` renvoie la valeur de `grille` à la ligne `i` et la colonne `j`.


On vous demande d'écrire une fonction telle que `recherche(cible, grille)` renvoie `None` si la `cible` est absente de `grille`, sinon le tuple `(i, j)` tel que `donne_valeur(grille, i, j)` est égal à la `cible`. On définit le coût de la recherche comme le nombre d'appels à la fonction `donne_valeur`.

On rappelle que la grille possède la propriété que chaque ligne et chaque colonne sont triées.

???+ warning "Attention"

    Certaines des grilles utilisées dans les tests contiennent beaucoup de valeurs. Une méthode de coût linéaire sera inefficace face à celles-ci.

    On limite donc le nombre de lectures dans chaque grille à 2000. Passée cette valeur maximale, tout nouvel accès provoquera une erreur.

    Dans les tests, la fonction `nb_acces` permet de connaitre le nombre de lectures de la grille. 



???+ example "Exemples"
    Avec la grille définie comme dans le premier test public, on a

    ```pycon
    >>> recherche(42, grille)
    (0, 2)
    >>> recherche(24, grille) is None
    True
    ```

??? tip "Indice"
    En partant d'un des coins supérieur droit ou inférieur gauche :

    - soit on trouve la cible,
    - soit on peut éliminer une ligne entière ou une colonne entière

    Dans le pire des cas, en $n+m$ tentatives, on a éliminé toutes les lignes et toutes les colonnes.

    Après avoir éliminé une ligne ou une colonne, on se retrouve à chercher une cible dans une grille rectangulaire, ainsi on se retrouve dans une situation similaire à la précédente.


{{ IDE('exo') }}

