Il existe plusieurs algorithmes pour une grille de $n$ lignes et $m$ colonnes, dont on donne le coût **dans le pire des cas** est :

1. Recherche exhaustive dans chaque case, dont le coût est $n×m$.
2. Recherche dichotomique sur chaque ligne dont le coût est environ $n\log(m)$.
3. Recherche dichotomique sur chaque colonne dont le coût est environ $m\log(n)$.
4. Recherche efficace, dont le coût est $m+n$.

On attend une recherche efficace. Une écriture itérative pourra être plus simple qu'une écriture récursive. Les deux sont possibles.

