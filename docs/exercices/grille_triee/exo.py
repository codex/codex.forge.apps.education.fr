# --------- PYODIDE:env --------- #
class Grille:
    def __init__(self, g):
        self.__grille = g
        self.__cout = 0

    def nb_lignes(self):
       return len(self.__grille)

    def nb_colonnes(self):
        return len(self.__grille[0])
    
    def nb_lectures(self):
        return self.__cout
    
        
    def donne_valeur(self, i, j):
        assert 0 <= i < self.nb_lignes(), "Numéro de ligne incorrect"
        assert 0 <= j < self.nb_colonnes(), "Numéro de colonne incorrect"
        self.__cout += 1
        return self.__grille[i][j]

def nb_lignes(grille):
    return grille.nb_lignes()


def nb_colonnes(grille):
    return grille.nb_colonnes()


def donne_valeur(grille, i, j):
    return grille.donne_valeur(i, j)

grille = Grille([
    [11, 33, 42, 63],
    [20, 52, 67, 80],
    [25, 61, 88, 95],
])

def nb_acces(grille):
    nb = grille.nb_lectures()
    grille._Grille__cout = 0
    return nb

# --------- PYODIDE:code --------- #

def recherche(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    ...

# --------- PYODIDE:corr --------- #

def recherche(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    while (i < n) and (j >= 0):
        x = donne_valeur(grille, i, j)
        if x > cible:
            j -= 1
        elif x < cible:
            i += 1
        else:
            return (i, j)
    return None




# --------- PYODIDE:tests --------- #

# La grille utilisée contient les valeurs suivantes :

"""
 [
    [11, 33, 42, 63],
    [20, 52, 67, 80],
    [25, 61, 88, 95],
]
"""


resultat = recherche(42, grille)
assert resultat == (0, 2), "Mauvaises coordonnées"
acces = nb_acces(grille)
assert acces <= 7, f"Trop de tentatives : {acces}"


resultat = recherche(24, grille)
assert resultat is None, "La cible est absente"
acces = nb_acces(grille)
assert acces <= 7, f"Trop de tentatives : {acces}"


# --------- PYODIDE:secrets --------- #


# autres tests
grille = Grille([
    [11, 33, 42, 63],
    [20, 52, 67, 80],
    [25, 61, 88, 95],
    [45, 62, 90, 100],
    [60, 70, 99, 119],
])

resultat = recherche(randrange(1, 10**6), grille)
acces = nb_acces(grille)
trop = acces > 7

assert resultat == (3, 2), "Mauvaises coordonnées"
assert trop is False, f"Trop de tentatives : {acces}"


grille = Grille([[(1000 * 1000 + i * 1000 + j) * 2 for j in range(1000)] for i in range(1000) ])


from random import randrange

resultat = recherche(randrange(1, 10**6), grille)
acces = nb_acces(grille)
trop = acces > 2000

assert resultat is None, "La cible est absente"
assert trop is False, f"Trop de tentatives : {acces}"


for _ in range(10):
    i0 = randrange(0, 1000)
    j0 = randrange(0, 1000)
    cible = (1000 * 1000 + i0 * 1000 + j0) * 2

    
    resultat = recherche(cible, grille)
    acces = nb_acces(grille)
    trop = acces > 2000
    assert resultat == (i0, j0), "Mauvaises coordonnées"
    assert trop is False, f"Trop de tentatives : {acces}"
    

    resultat = recherche(cible + 1, grille)
    acces = nb_acces(grille)
    trop = acces > 2000
    assert resultat is None, "cible absente"
    assert trop is False, f"Trop de tentatives : {acces}"
    
