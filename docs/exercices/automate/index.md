---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Automate et reconnaissance de motif
tags:
    - tuple
    - dictionnaire
    - à trous
---

On considère les automates décrits dans l'exercice [Automate en dictionnaire](../../exercices/automate_0/index.md).

!!! tip "Conseil"

    On conseille fortement de lire l'exercice cité plus haut afin de se familiariser avec la représentation des automates sous forme de dictionnaires Python.

Un tel automate permet de déterminer si une chaîne de caractère correspond à un certain motif :

* on débute dans l'état de départ en lisant le premier caractère de la chaîne ;

* s'il existe une règle correspondant à ce couple « état - caractère lu », on passe dans l'état indiqué par la règle et on lit le caractère suivant ;
* si une telle règle n'existe pas, la chaîne étudiée ne correspond pas au motif cherché ;
* une fois tous les caractères de la chaîne lus, si l'on se trouve dans l'état final alors celle-ci correspond au motif.

Par exemple les chaînes `#!py "b"`, `#!py "abcaa"` et `#!py "abbcb"` correspondent au motif décrit dans l'automate ci-dessous. Les chaînes `#!py "abbc"`, `#!py "abbbd"` et `#!py "ba"` n'y correspondent pas.

![Automate](automate.svg){ .center .autolight }

Les automates et leurs règles de transitions sont décrits à l'aide de dictionnaires Python (voir exercice  [Automate en dictionnaire](../../exercices/automate_0/index.md)).

On demande d'écrire la fonction `correspond` qui :

* prend en arguments :
    * une chaîne de caractères `chaine`,
    * une ensemble de règles données sous forme d'un dictionnaire `regles`,
    * l'état de départ `debut`,
    * l'état final `fin`,
* et renvoie le booléen indiquant si la chaîne proposée satisfait aux règles données.

???+ example "Exemples"

    ```pycon title=""
    >>> regles = {(0, "a"): 1, (0, "b"): 2, (1, "a"): 2, (1, "b"): 1, (1, "c"): 0}
    >>> debut = 0
    >>> fin = 2
    >>> correspond("b", regles, debut, fin)
    True
    >>> correspond("abbcaa", regles, debut, fin)
    True
    >>> correspond("abbcb", regles, debut, fin)
    True
    >>> correspond("abbc", regles, debut, fin)
    False
    >>> correspond("abbbd", regles, debut, fin)
    False
    >>> correspond("ba", regles, debut, fin)
    False
    ```

=== "Version vide"
    {{ IDE('exo_a') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}
