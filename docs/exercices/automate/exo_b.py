

# --------- PYODIDE:code --------- #

def correspond(chaine, regles, debut, fin):
    etat = ...
    for caractere in chaine:
        if ...:
            etat = ...
        else:
            return ...
    return ...

# --------- PYODIDE:corr --------- #

def correspond(chaine, regles, debut, fin):
    etat = debut
    for caractere in chaine:
        if (etat, caractere) in regles:
            etat = regles[(etat, caractere)]
        else:
            return False
    return etat == fin

# --------- PYODIDE:tests --------- #

regles = {(0, "a"): 1, (0, "b"): 2, (1, "a"): 2, (1, "b"): 1, (1, "c"): 0}
debut = 0
fin = 2

assert correspond("b", regles, debut, fin) is True
assert correspond("abba", regles, debut, fin) is True
assert correspond("acaa", regles, debut, fin) is True
assert correspond("abbbcaa", regles, debut, fin) is True
assert correspond("abbc", regles, debut, fin) is False
assert correspond("abbbd", regles, debut, fin) is False
assert correspond("ba", regles, debut, fin) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
# Expression régulière cherchée : ba+to+
regles = {(0, "b"): 1, (1, "a"): 2, (2, "a"): 2, (2, "t"): 3, (3, "o"): 4, (4, "o"): 4}
debut = 0
fin = 4

assert correspond("bato", regles, debut, fin) is True
assert correspond("baato", regles, debut, fin) is True
assert correspond("batoo", regles, debut, fin) is True
assert correspond("baaaatoo", regles, debut, fin) is True
assert correspond("aatoo", regles, debut, fin) is False
assert correspond("baaat", regles, debut, fin) is False
assert correspond("btooo", regles, debut, fin) is False