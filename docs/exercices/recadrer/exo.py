

# --------- PYODIDE:code --------- #

def recadre(mesures, val_min, val_max):
    for i in range(len(...)):
        if mesures[i] < ...:
            mesures[i] = ...
        elif ...:
            ...

# --------- PYODIDE:corr --------- #

def recadre(mesures, val_min, val_max):
    for i in range(len(mesures)):
        if mesures[i] < val_min:
            mesures[i] = val_min
        elif mesures[i] > val_max:
            mesures[i] = val_max

# --------- PYODIDE:tests --------- #

mesures_1 = [1, 2, 3, 4, 5, 6]
recadre(mesures_1, 3, 5)
assert mesures_1 == [3, 3, 3, 4, 5, 5]

mesures_2 = [7.1, -9.0, -3.1, 15.0, 987.7, -624.89]
recadre(mesures_2, -5.3, 15.0)
assert mesures_2 == [7.1, -5.3, -3.1, 15.0, 15.0, -5.3]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
vide = []
recadre(vide, 0, 100)
assert vide == []

rien_a_changer = [1, 2, 3, 4, 5]
recadre(rien_a_changer, 0, 6)
assert rien_a_changer == [1, 2, 3, 4, 5]

tous_trop_petits = [-9, -3, -4, -99, -2, -6]
recadre(tous_trop_petits, -1, 15)
assert tous_trop_petits == [-1, -1, -1, -1, -1, -1]

tous_trop_grands = [99, 999, 198, 57, 16, 79, 35]
recadre(tous_trop_grands, -1, 15)
assert tous_trop_grands == [15, 15, 15, 15, 15, 15, 15]