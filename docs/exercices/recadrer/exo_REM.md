Dans cet exercice, le tableau pris en paramètre `mesures` a été modifié **en place**. Parfois, nous ne voulons pas, au contraire, qu'un tableau pris en paramètre soit modifié. 

👉 Pour bien comprendre la différence, vous pouvez chercher à résoudre l'exercice "Remplacer une valeur".
