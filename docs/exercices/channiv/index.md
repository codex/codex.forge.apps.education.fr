---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
difficulty: 150
title: Anniversaire de chat
tags:
    - string
---

=== "Carte en français"

    [![](./channiv_fr.jpeg){ width=20% .center }](./channiv_fr_2048.jpeg)

=== "Carte en anglais"

    [![](./channiv_en.jpeg){ width=20% .center }](./channiv_en_2048.jpg)

Lorsqu'un chat souhaite offrir une carte d'anniversaire à un ami chat, il lui offre une carte avec un chat tout mignon avec un texte sur son âge dans sa vie actuelle. Oui, les chats ont plusieurs vies... Ce qui fait de très nombreuses cartes possibles à créer.

La société _CatCard_ propose de personnaliser la carte avec un texte en anglais (`#!py "en"`) ou en français (`#!py "fr"`) qui dépend de l'`age` et du `num_vie` ; des entiers strictement positifs. Si langue demandée n'est ni `#!py "fr"` ni `#!py "en"`, la société _CatCard_ propose une réponse `'Je donne ma langue au chat'`

Écrire une fonction `channiv` qui renvoie le texte demandé comme sur les exemples suivants :

???+ example "Exemples"

    ```pycon title=""
    >>> channiv(3, 4)
    'Joyeux 3e channiversaire de ta 4e vie'
    >>> channiv(3, 4, "en")
    'Happy purrthday, for 3rd year of 4th life'
    >>> channiv(1, 1, "fr")
    'Joyeux 1er channiversaire de ta 1re vie'
    >>> channiv(1, 1, "en")
    'Happy purrthday, for 1st year of 1st life'
    >>> channiv(3, 4, "miaou")
    'Je donne ma langue au chat'
    ```

!!! info "Paramètre par défaut"
    On pourra constater que `#!py channiv(3, 4)` est équivalent à `#!py channiv(3, 4, "fr")`, en effet la majorité des clients de CatCard sont français ; ils veulent que la fonction soit configurée pour renvoyer par défaut un texte en français.

    Le mécanisme pour ce faire est le paramètre par défaut `lang` dans la définition de `channiv`

    ```python
    def channiv(age, num_vie, lang="fr"):
    ```


!!! quote "Ordinal abrégé"
    En français, on place **seulement** un `e` après le nombre, sauf pour `#!py 1er` (premier) et `#!py 1re` (première).

    > On trouve souvent des abréviations en `ème`, elles ne sont pas recommandées, sauf pour le cas d'une lettre comme nième, ou pième.

    En anglais, on place `th` après le nombre, sauf pour `#!py 1st` (_first_), `#!py 2nd` (_second_) et `#!py 3rd` (_third_).

{{ IDE('exo') }}
