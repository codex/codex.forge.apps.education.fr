

# --------- PYODIDE:code --------- #

def channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        ...
    elif lang == "en":
        ...
    else:
        return "Je donne ma langue au chat"

# --------- PYODIDE:corr --------- #

def channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        carte = "Joyeux "
        carte += str(age)
        if age == 1:
            carte += "er"
        else:
            carte += "e"
        carte += " channiversaire de ta "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "re"
        else:
            carte += "e"
        carte += " vie"
        return carte
    elif lang == "en":
        carte = "Happy purrthday, for "
        carte += str(age)
        if age == 1:
            carte += "st"
        elif age == 2:
            carte += "nd"
        elif age == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " year of "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "st"
        elif num_vie == 2:
            carte += "nd"
        elif num_vie == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " life"
        return carte
    else:
        return "Je donne ma langue au chat"

# --------- PYODIDE:tests --------- #

assert channiv(3, 4) == "Joyeux 3e channiversaire de ta 4e vie"
assert channiv(3, 4, "en") == "Happy purrthday, for 3rd year of 4th life"
assert channiv(1, 1, "fr") == "Joyeux 1er channiversaire de ta 1re vie"
assert channiv(1, 1, "en") == "Happy purrthday, for 1st year of 1st life"
assert channiv(3, 4, "miaou") == "Je donne ma langue au chat"

# --------- PYODIDE:secrets --------- #


# Autres tests


def secret_channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        carte = "Joyeux "
        carte += str(age)
        if age == 1:
            carte += "er"
        else:
            carte += "e"
        carte += " channiversaire de ta "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "re"
        else:
            carte += "e"
        carte += " vie"
        return carte
    elif lang == "en":
        carte = "Happy purrthday, for "
        carte += str(age)
        if age == 1:
            carte += "st"
        elif age == 2:
            carte += "nd"
        elif age == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " year of "
        carte += str(num_vie)
        if num_vie == 1:
            carte += "st"
        elif num_vie == 2:
            carte += "nd"
        elif num_vie == 3:
            carte += "rd"
        else:
            carte += "th"
        carte += " life"
        return carte
    else:
        return "Je donne ma langue au chat"


for age in range(1, 17):
    for num_vie in range(1, 17):
        attendu_fr = secret_channiv(age, num_vie)
        assert channiv(age, num_vie) == attendu_fr, f"Erreur avec {age = } et {num_vie = }, en français"
        attendu_en = secret_channiv(age, num_vie, "en")
        assert (
            channiv(age, num_vie, "en") == attendu_en
        ), f"Erreur avec {age = }, {num_vie = }, en anglais"

assert channiv(1, 1, "rrrr") == "Je donne ma langue au chat"