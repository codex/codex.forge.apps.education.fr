# Version, avec `divmod`

La fonction `divmod` renvoie le quotient **et** le reste d'une division entière.

```python
def rendu(somme_a_rendre):
    n_5, somme_a_rendre = divmod(somme_a_rendre, 5)
    n_2, somme_a_rendre = divmod(somme_a_rendre, 2)
    n_1 = somme_a_rendre

    return (n_5, n_2, n_1)
```


# Version avec une boucle

```python
VALEURS = (5, 2, 1)

def rendu(somme_a_rendre):
    resultat = [0] * len(valeurs)
    for i in range(len(valeurs)):
        resultat[i], somme_a_rendre = divmod(somme_a_rendre, VALEURS[i])

    return tuple(retour)
```

Cette dernière version est utile si on envisage un autre système de pièces avec de nombreuses valeurs.

# Les systèmes de monnaie canoniques

* A chaque étape de l'algorithme, on prend les pièces de plus grande valeur possible car la liste `PIECES` est triée par ordre décroissant. Ce type d'algorithme, qui à chaque étape réalise le meilleur choix possible, est appelé **algorithme glouton**.
* L'algorithme glouton, ne permet pas forcément de rendre la monnaie avec un minimum de pièces. Ce n'est le cas que si le système de monnaie (liste des pièces disponibles) est conçu pour cela. On a alors un système de monnaie **canonique**. Par exemple pour le système non canonique `PIECES = (4, 3, 1)` un algoritme glouton donnera pour 6 trois pièces : 4, 1, 1 alors qu'il en suffit de deux : 3, 3.
