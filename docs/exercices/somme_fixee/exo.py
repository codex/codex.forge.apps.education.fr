

# --------- PYODIDE:code --------- #

def est_somme(x):
    ...

# --------- PYODIDE:corr --------- #

def est_somme(x):
    i = 0
    j = taille() - 1
    while i < j:
        y = somme(i, j)
        if y < x:
            i += 1
        elif y > x:
            j -= 1
        else:
            return True
    return False

# --------- PYODIDE:tests --------- #

# test 1
tableau_1 = [3, 4, 6, 9, 13, 18, 19]


def taille():
    return len(tableau_1)


nb_tests = 0


def somme(i, j):
    global nb_tests
    assert 0 <= i < j < taille(), "Mauvais indices"
    nb_tests += 1
    correct = nb_tests <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    return tableau_1[i] + tableau_1[j]


assert est_somme(20) is False

# test 2
tab_2 = [3, 5, 7, 8, 9, 12, 16, 19]


def taille():
    return len(tab_2)


nb_s = 0


def somme(i, j):
    global nb_s
    assert 0 <= i < j < taille(), "Mauvais indices"
    nb_s += 1
    correct = nb_s <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    return tab_2[i] + tab_2[j]


assert est_somme(21) is True

# --------- PYODIDE:secrets --------- #


# autres tests


def secret_est_somme(x):
    i = 0
    j = taille() - 1
    while i < j:
        y = somme(i, j)
        if y < x:
            i += 1
        elif y > x:
            j -= 1
        else:
            return True
    return False


# test 1

t_secret = [1, 2, 3, 5, 8, 13, 21]


def taille():
    return len(t_secret)


def somme(i, j):
    mouchard.append(None)
    correct = len(mouchard) <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    assert 0 <= i < j < taille()
    return t_secret[i] + t_secret[j]


for x in range(t_secret[0], 2 * t_secret[-1] + 2):
    mouchard = []
    attendu = est_somme(x)
    mouchard = []
    juste = attendu == secret_est_somme(x)
    assert juste is True, "Erreur sur un test secret"

# test 2

t_secret = [11, 12, 13, 15, 18, 23, 31, 44]


def taille():
    return len(t_secret)


def somme(i, j):
    mouchard.append(None)
    correct = len(mouchard) <= taille()
    assert correct, "Il faut utiliser `somme` moins souvent"
    assert 0 <= i < j < taille()
    return t_secret[i] + t_secret[j]


for x in range(t_secret[0], 2 * t_secret[-1] + 2):
    mouchard = []
    attendu = est_somme(x)
    mouchard = []
    juste = attendu == secret_est_somme(x)
    assert juste is True, "Erreur sur un test secret"

del somme