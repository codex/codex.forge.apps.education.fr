# --------- PYODIDE:env --------- #
import matplotlib
# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécifique (id="cible")
from js import document 
document.pyodideMplTarget = document.getElementById("cible")
# on vide la div si elle contenait du texte
document.getElementById("cible").textContent = ""

def liste_termes(N):
    termes = [0, 1]
    for n in range(2, N):
        termes.append(5 * termes[n - 1] - 8 * termes[n - 2])
    return termes
# --------- PYODIDE:code --------- #

import matplotlib.pyplot as plt
n =10
x = [i for i in range(n)]
y = liste_termes(n)

# Tracé des éléments de la suite
plt.plot(range(n), y, 'og')
plt.show()