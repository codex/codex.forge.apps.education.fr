---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Listes des termes d'une suite mathématique
tags:
    - maths
    - à trous
    - liste/tableau
difficulty: 210
maj: 09/07/2024
---


Soit $(u_n)$ la suite définie par récurrence par :

\[
\left\{
          \begin{array}{ll}
            u_0 = 0 \\
            u_1 = 1 \\
            u_{n} = 5 u_{n-1} - 8 u_{n-2}  \hspace{3ex} \text{pour} \hspace{1ex} n>1 \\  
          \end{array}
\right.
   \]

On souhaite créer la fonction `liste_termes` qui renvoie une liste contenant les `N` premiers termes de cette suite.
On suppose que `N` est strictement supérieur à $1$.

???+ warning "Contrainte"

    On demande ici de programmer une fonction qui puisse renvoyer un nombre élevé de termes.

???+ example "Exemples"

    ```pycon title=""
    >>> liste_termes(2)
    [0, 1]
    >>> liste_termes(3)
    [0, 1, 5]
    >>> liste_termes(4)
    [0, 1, 5, 17]
    >>> liste_termes(10)
    [0, 1, 5, 17, 45, 89, 85, -287, -2115, -8279]
    ```

{{ remarque('assertion') }}

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}

!!! note "Graphique"
    Il est possible de visualiser ces termes avec le code suivant :

    {{ IDE('visu') }}

    {{ figure('cible') }}
        