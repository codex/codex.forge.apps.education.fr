# --------- PYODIDE:env --------- #

def convertir_en_min_sec(secondes):
    pass

def convertir_en_h_min_sec(secondes):
    pass

# --------- PYODIDE:code --------- #

def convertir_en_min_sec(secondes):
    ...


# --------- PYODIDE:corr --------- #

def convertir_en_min_sec(secondes):
    return secondes // 60, secondes % 60


# --------- PYODIDE:tests --------- #

assert convertir_en_min_sec(23652) == (394, 12)
assert convertir_en_min_sec(0) == (0, 0)

# --------- PYODIDE:secrets --------- #

assert convertir_en_min_sec(17) == (0, 17)
assert convertir_en_min_sec(59) == (0, 59)
assert convertir_en_min_sec(60) == (1, 0)
assert convertir_en_min_sec(1000) == (16, 40)
assert convertir_en_min_sec(600) == (10, 0)
assert convertir_en_min_sec(602) == (10, 2)
assert convertir_en_min_sec(0) == (0, 0)