---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Les durées (III)
tags:
    - tuple
    - maths
difficulty: 110
---

Les durées peuvent être exprimées en secondes, en minutes-secondes, ou en heures-minutes-secondes.

Ainsi, une durée de 23 652 secondes peut aussi être exprimée par :

```python title="Différents formats"
secondes = 23652
minutes_secondes = (394, 12)
heures_minutes_secondes = (6, 34, 12)
```

On souhaite créer des fonctions permettant de convertir des durées exprimées en secondes vers les deux autres formats.

Les deux questions sont indépendantes.

??? question "1. Secondes vers Minutes-Secondes"

    Écrire la fonction `convertir_en_min_sec` qui prend en paramètre une durée exprimée en secondes et renvoie la durée exprimée en minutes et secondes.

    ???+ example "Exemple"

        ```pycon title=""
        >>> convertir_en_min_sec(23652)
        (394, 12)
        >>> convertir_en_min_sec(0)
        (0, 0)
        ```
    {{ remarque('operateurs_division') }}

    {{ IDE('exo1') }}


??? question "2. Secondes vers Heures-Minutes-Secondes"
    
    Écrire la fonction `convertir_en_h_min_sec` qui prend en paramètre une durée exprimée en secondes et qui renvoie la durée exprimée en heures, minutes et secondes.

    ???+ example "Exemple"

        ```pycon title=""
        >>> convertir_en_h_min_sec(23652)
        (6, 34, 12)
        >>> convertir_en_h_min_sec(0)
        (0, 0, 0)
        ```

    {{ remarque('operateurs_division') }}

    Une version valide de la fonction `convertir_en_min_sec` est disponible dans cet éditeur. Vous pouvez directement l'utiliser si vous le souhaitez.

    {{ IDE('exo2') }}
