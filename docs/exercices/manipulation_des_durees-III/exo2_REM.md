# Utilisation d'un fonction déjà définie

Pour la fonction `convertir_en_h_min_sec`, il est possible d'utiliser `convertir_en_min_sec` pour faire une partie du travail :

```python
def convertir_en_h_min_sec(duree_sec):
    minutes, secondes = convertir_en_min_sec(duree_sec)
    return minutes // 60, minutes % 60, secondes
```

On peut même utiliser `convertir_en_min_sec` de façon détournée. En effet, convertir des secondes en secondes et minutes est similaire au fait de convertir des minutes en minutes et heures.

```python
def convertir_en_h_min_sec(duree_sec):
    minutes, secondes = convertir_en_min_sec(duree_sec)
    heures, minutes = convertir_en_min_sec(minutes)
    return heures, minutes, secondes
```

Il faut néanmoins faire attention parce que de telles astuces de programmation peuvent rendre la lecture et la vérification du code plus complexe.