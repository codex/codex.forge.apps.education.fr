# --------- PYODIDE:env --------- #
def convertir_en_min_sec(duree_sec):
    return duree_sec // 60, duree_sec % 60


def convertir_en_h_min_sec(secondes):
    pass


# --------- PYODIDE:code --------- #


def convertir_en_h_min_sec(secondes):
    ...


# --------- PYODIDE:corr --------- #
def convertir_en_h_min_sec(secondes):
    heures = secondes // 3600
    secondes_restantes = secondes % 3600
    return heures, secondes_restantes // 60, secondes_restantes % 60


# --------- PYODIDE:tests --------- #

assert convertir_en_h_min_sec(23652) == (6, 34, 12)
assert convertir_en_h_min_sec(0) == (0, 0, 0)

# --------- PYODIDE:secrets --------- #
assert convertir_en_h_min_sec(10000) == (2, 46, 40)
assert convertir_en_h_min_sec(7200) == (2, 0, 0)
assert convertir_en_h_min_sec(7300) == (2, 1, 40)
assert convertir_en_h_min_sec(3600) == (1, 0, 0)
assert convertir_en_h_min_sec(60) == (0, 1, 0)
assert convertir_en_h_min_sec(1) == (0, 0, 1)
assert convertir_en_h_min_sec(0) == (0, 0, 0)
