---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Livraisons à Manhattan
tags:
    - glouton
difficulty: 250
maj: 25/04/2024
---




Un livreur new-yorkais organise sa tournée. Le quartier dans lequel il évolue est un quadrillage :
toutes les rues suivent l'axe Nord ↔ Sud ou l'axe Est ↔ Ouest.

Les adresses de ses livraisons sont donc des couples de coordonnées, `#!py (2, 3)` par exemple. La première coordonnée est l'*abscisse* du point, la seconde son *ordonnée*.

Ces adresses de livraison sont données dans une liste python. Par exemple :

```python
livraisons = [(2, 0), (0, 1), (3, 3), (2, 3)]
```

Ce qui correspond à la carte ci-dessous :

![La carte](carte.svg){.autolight .center}

Afin de déterminer son itinéraire, il décide de toujours livrer en priorité l'adresse la plus « proche » de sa position actuelle. Se déplaçant le long des rues, il mesure la distance $AB$ entre deux points de coordonnées $(x_A\,; y_A)$ et $(x_B\,; y_B)$ en faisant :

$$AB = \text{abs}(x_B-x_A)+\text{abs}(y_B-y_A)$$

où $\text{abs}(x)$ est la valeur absolue du nombre $x$ : $\text{abs}(3-5)=\text{abs}(5-3)=2$.

??? tip "Coup de pouce (1)"
    Avec Python, `#!py abs(x)` renvoie la valeur absolue de `x`

??? tip "Coup de pouce (2)"
    On rappelle que Python permet de récupérer facilement les différents valeurs d'un tuple grâce au *décompactage* (*unpacking* en anglais). Par exemple :

    ```pycon
    >>> x, y = (3, 8)
    >>> x
    3
    >>> y
    8
    ```
    
On se propose dans cet exercice d'aider le livreur à déterminer son trajet en écrivant les deux fonctions décrites ci-dessous.

??? question "Fonction `distance`"

    Écrire la fonction `distance` qui prend en paramètres les coordonnées de deux points `A` et `B` donnés chacun sous la forme d'un *tuple* de deux entiers et qui calcule la distance entre ces deux points.

    ???+ example "Exemples"

        ```pycon title=""
        >>> A = (0, 1)
        >>> B = (2, 3)
        >>> C = (1, 6)
        >>> distance(A, B)
        4
        >>> distance(B, A)
        4
        >>> distance(A, C)
        6
        >>> distance(B, C)
        4
        ```

    {{ IDE('exo_distance') }}
    
??? question "Fonction `prochaine_livraison`"

    Écrire la fonction `prochaine_livraison` qui prend en paramètres la liste des adresses de livraison et la position actuelle du livreur et renvoie les coordonnées du prochain point à livrer.


    Si deux points sont à la même distance de la position actuelle du livreur on renverra les coordonnées du premier point rencontré dans la liste.

    Une version valide de la fonction `distance` est chargée dans l'éditeur. Vous pouvez directement l'utiliser.

    {{ interdiction('min, sort, sorted')}}

    ???+ example "Exemples"

        ```pycon title=""
        >>> A = (0, 1)
        >>> B = (2, 3)
        >>> distance(A, B)
        4
        >>> livraisons_1 = [(2, 0), (0, 1), (3, 3), (2, 3)]
        >>> prochaine_livraison(livraisons_1, (0, 0))
        (0, 1)
        >>> livraisons_2 = [(2, 0), (3, 3), (2, 3)]
        >>> prochaine_livraison(livraisons_2, (0, 1))
        (2, 0)
        >>> livraisons_3 = [(3, 3), (2, 3)]
        >>> prochaine_livraison(livraisons_3, (2, 0))
        (2, 3)
        >>> livraisons_4 = [(3, 3)]
        >>> prochaine_livraison(livraisons_4, (2, 3))
        (3, 3)
        >>> livraisons_5 = [(1, 0), (0, 1)]
        >>> prochaine_livraison(livraisons_5, (0, 0))
        (1, 0)
        ```

    {{ IDE('exo_prochain') }}
