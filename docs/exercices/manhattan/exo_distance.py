# --------- PYODIDE:env --------- #
def distance(A, B):
    ...


# --------- PYODIDE:code --------- #
def distance(A, B):
    ...


# --------- PYODIDE:corr --------- #
def distance(A, B):
    xA, yA = A
    xB, yB = B
    return abs(xB - xA) + abs(yB - yA)


# --------- PYODIDE:tests --------- #
A = (0, 1)
B = (2, 3)
C = (1, 6)
assert distance(A, B) == 4
assert distance(B, A) == 4
assert distance(A, C) == 6
assert distance(B, C) == 4
# --------- PYODIDE:secrets --------- #

# Tests
from random import randrange

taille = 10
points = [(randrange(0, 100), randrange(0, 100)) for _ in range(10)]
A = points[0]
B = points[0]
attendu = 0
assert distance(A, B) == attendu, f"Erreur avec {A = } et {B = }"
for i in range(taille - 1):
    A = points[i]
    B = points[i + 1]
    attendu = abs(A[0] - B[0]) + abs(A[1] - B[1])
    assert distance(A, B) == attendu, f"Erreur avec {A = } et {B = }"
