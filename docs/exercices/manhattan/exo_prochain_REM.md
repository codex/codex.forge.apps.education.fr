Il s'agit en réalité d'une recherche de minimum.

Le livreur peut construire son trajet en effectuant différentes recherches successives.

Cette recherche de trajet se fait en choisissant à chaque étape la prochaine livraison la plus proche de la position actuelle. Facile à mettre en œuvre, cette méthode ne garantit pas que la longueur totale du trajet soit la plus courte.

