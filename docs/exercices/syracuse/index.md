---
license: "by-nc-sa"
author: 
    - Franck Chambon
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Suite de Syracuse
tags:
    - maths
    - int
difficulty: 210
maj: 16/10/2024
---

Pour construire la suite de Syracuse d'un entier $n$ strictement positif, on répète la transformation suivante :

- Si $n$ est pair, on le divise par $2$ ;
- Sinon $n$ devient $3n+1$.

On admet que, quel que soit l'entier $n$ choisi au départ, la suite finit toujours par atteindre la valeur $1$. On convient donc d'interrompre les transformations lorsque l'on a atteint la valeur $1$.

Ainsi, en prenant de $n=3$, on obtient :

$$3 \longrightarrow 10 \longrightarrow 5 \longrightarrow 16 \longrightarrow 8 \longrightarrow 4 \longrightarrow  2  \longrightarrow 1$$

On rappelle que `#!py n % 2` renvoie le reste dans la division de `n` par `#!py 2`. Si ce reste est nul, c'est que `n` est un nombre pair.


{{ remarque('assertion') }}


??? question "Longueur du vol"

    On a admis que, quel que soit l'entier $n$ choisi, ce processus de calcul finissait toujours par atteindre $1$. Par exemple en partant de la valeur $3$, il y a $7$ transformations avant d'atteindre $1$.    

    Ce nombre de transformations nécessaires est appelé « **longueur du vol** » de l'entier $n$.

    Écrire une fonction `longueur_vol` qui prend en paramètre un entier `n` strictement positif et renvoie la longueur du vol de cet entier.

    ???+ example "Exemple"

        ```pycon title=""
        >>> longueur_vol(3)
        7
        >>> longueur_vol(7)
        16
        >>> longueur_vol(1)
        0
        ```

    === "Version vide"
        {{ IDE('exo_vol_vide') }}
    === "Version à compléter"
        {{ IDE('exo_vol_trous') }}

??? question "Suite des termes"


    Écrire une fonction `syracuse` prenant en paramètres un entier `n` strictement positif et qui renvoie la liste des termes de la suite de Syracuse de l'entier `#!py n`.


    On rappelle les instructions suivantes :
    
    ```pycon
    >>> nombres = [3]       # création d'une liste contenant la valeur 3
    >>> nombres.append(10)  # ajout de la valeur 10 (à la fin)
    >>> nombres
    [3, 10]
    ```

    ???+ example "Exemple"

        ```pycon title=""
        >>> syracuse(3)
        [3, 10, 5, 16, 8, 4, 2, 1]
        >>> syracuse(7)
        [7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
        >>> syracuse(1)
        [1]
        ```

    === "Version vide"
        {{ IDE('exo_vide') }}
    === "Version à compléter"
        {{ IDE('exo_trous') }}
