

# --------- PYODIDE:code --------- #

def syracuse(n):
    suite = [...]
    while ...:
        if ...:
            n = ...
        else:
            n = ...
        suite.append(...)
    return ...

# --------- PYODIDE:corr --------- #

def syracuse(n):
    suite = [n]
    while n != 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
        suite.append(n)
    return suite

# --------- PYODIDE:tests --------- #

assert syracuse(3) == [3, 10, 5, 16, 8, 4, 2, 1]
assert syracuse(7) == [7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
assert syracuse(1) == [1]

# --------- PYODIDE:secrets --------- #
def _solution_(n):
    suite = [n]
    while n != 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
        suite.append(n)
    return suite


for n in range(1, 200):
    resultat = _solution_(n)
    assert syracuse(n) == resultat, f"Erreur avec {n}"