

# --------- PYODIDE:code --------- #

def longueur_vol(n):
    ...

# --------- PYODIDE:corr --------- #

def longueur_vol(n):
    vol = 0
    while n != 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
        vol = vol + 1
    return vol

# --------- PYODIDE:tests --------- #

assert longueur_vol(3) == 7
assert longueur_vol(7) == 16
assert longueur_vol(1) == 0

# --------- PYODIDE:secrets --------- #
def _solution_(n):
    vol = 0
    while n != 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
        vol = vol + 1
    return vol
    
for n in range(1, 200):
    resultat = _solution_(n)
    assert longueur_vol(n) == resultat, f"Erreur avec {n}"