Il s'agit du problème dit du *Drapeau Hollandais* inventé par E. Dijkstra. 

Ce tri a pour avantage d'être de coût linéaire et de trier le tableau en un seul passage.

Cependant son utilisation est limitée à des tableaux composés de trois valeurs distinctes.