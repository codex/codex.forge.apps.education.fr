---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Tri de trois valeurs
tags:
    - tri
difficulty: 250
---


???+ abstract "Le tri du drapeau hollandais en bref"

    On l'utilise dans le cadre des tableaux ne contenant que trois valeurs distinctes (petite, moyenne et grande).

    Il a pour avantage d'être de coût linéaire et de trier le tableau en un seul passage.

    On partage le tableau en quatre zones :


    * les petites valeurs,
    * puis les valeurs moyennes,
    * puis les valeurs pas encore triées,
    * enfin les grandes valeurs.

    ![Schéma](images/petit_drapeau.png#only-light){ width=50% .center }
       
    ![Schéma](images/petit_drapeau_dark.png#only-dark){ width=50% .center }
    
    L'algorithme s'arrête lorsque la zone des valeurs à trier est vide.
    

On souhaite trier un `tableau` ne contenant que trois valeurs `a`, `b` et `c` répétées un nombre quelconque de fois. On souhaite trier ce tableau afin de placer au début les `a` suivis des `b` et enfin des `c`.

Par exemple, avec `#!py nombres = [2, 0, 2, 1, 2, 1]` on a `#!py a = 0`, `#!py b = 1` et `#!py c = 2`. Le tableau trié est `#!py [0, 1, 1, 2, 2, 2]`.

Dans ce cas de figure, on peut utiliser le tri du drapeau hollandais.

??? note "Remarque"

    Ce tri a été inventé par [Edsger Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) qui était de nationalité néelandaise.

    Il a donné son nom à l'algorithme en référence aux trois couleurs du drapeau néerlandais : Rouge, Blanc, Bleu.

    ![Drapeau hollandais](images/flag.jpg){ .center width=25% }

L'idée est de maintenir quatre zones dans le tableau :

* la première ne contient que des `a`,
* la deuxième que des `b`,
* la troisième des valeurs pas encore triées,
* enfin la dernière zone ne contient que des `c`.

Ces zones sont délimitées par les bornes suivantes (toutes incluses):

1. de l'indice `0` à `prochain_a - 1`,
2. de `prochain_a` à `actuel`,
3. de `actuel + 1` à `prochain_c`,
4. de `prochain_c + 1` à `#!py len(tableau) - 1`

![Les quatre zones](images/drapeau.png#only-light){.center width=100%}
![Les quatre zones](images/drapeau_dark.png#only-dark){.center width=100%}

L'algorithme fonctionne ainsi :

* Les variables `prochain_a` et `actuel` sont initialisées à `#!py 0`,
  
* La variable `prochain_c` est initialisée à `#!py len(tableau) - 1`,
  
* On répète les actions suivantes tant que la zone des valeurs restant à trier est non vide :
  
    * On lit l'élément à l'indice `actuel` :

        * Si c'est un `a`, on le rajoute à la fin de la zone des `a` en échangeant les éléments d'indices `prochain_a` et `actuel` (étape 3 sur les figures),
        * Si c'est un `b`, on la laisse à sa position  (étape 2 et 6),
        * Si c'est un `c`, on la place à la position précédant le début de la zone des `c` en échangeant les éléments d'indices `prochain_c` et `actuel` (étape 1, 4 et 5),
        * Dans tous les cas, on prend soin de mettre à jour les différents indices afin de refléter les nouvelles étendues des zones.

Les figures ci-dessous illustrent le tri du tableau `#!py [2, 0, 2, 1, 2, 1]` :

* Étape 1

    ![Étape 1](images/drapeau_1.png#only-light){ width=40% align=left}
    ![Étape 1](images/drapeau_1_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.
---
* Étape 2

    ![Étape 2](images/drapeau_2.png#only-light){ width=40% align=left}
    ![Étape 2](images/drapeau_2_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 1` : on le laisse à cette position.

    `actuel` est incrémenté.

---
* Étape 3

    ![Étape 3](images/drapeau_3.png#only-light){ width=40% align=left}
    ![Étape 3](images/drapeau_3_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 0` : on le place à la fin de la zone des `a`.

    `prochain_a` et `actuel` sont incrémentés.

---
* Étape 4

    ![Étape 4](images/drapeau_4.png#only-light){ width=40% align=left}
    ![Étape 4](images/drapeau_4_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.

---
* Étape 5

    ![Étape 5](images/drapeau_5.png#only-light){ width=40% align=left}
    ![Étape 5](images/drapeau_5_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 2` : on le place au début de la zone des `c`.

    `prochain_c` est décrémenté.

---
* Étape 6

    ![Étape 6](images/drapeau_6.png#only-light){ width=40% align=left}
    ![Étape 6](images/drapeau_6_dark.png#only-dark){ width=40% align=left}

    On lit un `#!py 1` : on le laisse à cette position.

---
* Étape 7

    ![Étape 7](images/drapeau_7.png#only-light){ width=40% align=left}
    ![Étape 7](images/drapeau_7_dark.png#only-dark){ width=40% align=left}

    `actuel` est strictement supérieur à `prochain_c`.
    
    L'algorithme se termine.

Vous devez écrire la fonction `tri_3_valeurs` qui prend en argument un `tableau` ainsi que les trois valeurs distinctes le composant `a`, `b` et `c`  et le trie **en place** en positionnant les `a` au début suivis des `b` et enfin des `c`.

{{ remarque("tri_interdit") }}

=== "Version vide"
    {{ IDE('exo_a', SANS = 'sort, sorted') }}

=== "Version à compléter"
    {{ IDE('exo_b', SANS = 'sort, sorted') }}
