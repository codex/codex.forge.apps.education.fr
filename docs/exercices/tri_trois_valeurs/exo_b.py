

# --------- PYODIDE:code --------- #

def tri_3_valeurs(nombres):
    prochain_a = 0
    actuel = 0
    prochain_c = len(nombres) - 1

    while ... <= ...:
        if nombres[...] == 0:
            nombres[...], nombres[...] = nombres[...], nombres[...]
            prochain_a += ...
            actuel += ...
        elif nombres[...] == 1:
            actuel += ...
        else:
            nombres[...], nombres[...] = nombres[...], nombres[...]
            prochain_c -= ...

# --------- PYODIDE:corr --------- #

def tri_3_valeurs(nombres):
    prochain_a = 0
    actuel = 0
    prochain_c = len(nombres) - 1

    while actuel <= prochain_c:
        if nombres[actuel] == 0:
            nombres[actuel], nombres[prochain_a] = nombres[prochain_a], nombres[actuel]
            prochain_a += 1
            actuel += 1
        elif nombres[actuel] == 1:
            actuel += 1
        else:
            nombres[actuel], nombres[prochain_c] = nombres[prochain_c], nombres[actuel]
            prochain_c -= 1


# --------- PYODIDE:tests --------- #

nombres = [2, 0, 2, 1, 2, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 2, 2, 2]

nombres = [1, 1, 0, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 1]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from random import shuffle

nombres = [0] * 10
tri_3_valeurs(nombres)
assert nombres == [0] * 10

nombres = [1] * 10
tri_3_valeurs(nombres)
assert nombres == [1] * 10

nombres = [2] * 10
tri_3_valeurs(nombres)
assert nombres == [2] * 10

nombres = [0] * 10 + [2] * 10
shuffle(nombres)
tri_3_valeurs(nombres)
assert nombres == [0] * 10 + [2] * 10

nombres = [0] * 10 + [1] * 10 + [2] * 10
shuffle(nombres)
tri_3_valeurs(nombres)
assert nombres == [0] * 10 + [1] * 10 + [2] * 10