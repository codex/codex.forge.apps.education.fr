---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Occurrences d'un caractère dans un mot
tags:
    - string
difficulty: 133
---

Écrire une fonction `compte_occurrences` qui prend en paramètres `cible`, un caractère (une chaine de caractères de longueur 1), et `mot`, une chaine de caractères, et qui renvoie le nombre d'occurrences de `cible` dans `mot` ; c'est-à-dire le nombre de fois où `cible` apparait dans `mot`.

???+ warning "Contrainte"

    On n'utilisera pas la méthode `#!py count`.

???+ example "Exemples"

    ```pycon title=""
    >>> compte_occurrences("o", "bonjour")
    2
    >>> compte_occurrences("a", "abracadabra")
    5
    >>> compte_occurrences("i", "abracadabra")
    0
    ```

{{ IDE('exo', SANS=".count") }}
