

# --------- PYODIDE:code --------- #

def compte_occurrences(cible, mot):
    ...

# --------- PYODIDE:corr --------- #

def compte_occurrences(cible, mot):
    nb_occurrences = 0
    for lettre in mot:
        if lettre == cible:
            nb_occurrences += 1
    return nb_occurrences

# --------- PYODIDE:tests --------- #

assert compte_occurrences("o", "bonjour") == 2
assert compte_occurrences("a", "abracadabra") == 5
assert compte_occurrences("i", "abracadabra") == 0

# --------- PYODIDE:secrets --------- #


# autres tests
assert compte_occurrences("a", "") == 0
assert compte_occurrences("a", "a") == 1
assert compte_occurrences("a", "b") == 0
assert compte_occurrences("a", "b" * 1000) == 0
assert compte_occurrences("a", "a" * 1000) == 1000
assert compte_occurrences("b", "ab" * 1000) == 1000