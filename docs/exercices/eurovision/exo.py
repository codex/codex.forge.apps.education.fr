# --------- PYODIDE:code --------- #
def cumul_votes(votes):
    ...


# --------- PYODIDE:corr --------- #
def cumul_votes(votes):
    resultat = {}
    for un_vote in votes:
        for pays, points in un_vote.items():
            if pays in resultat:
                resultat[pays] += points
            else:
                resultat[pays] = points
    return resultat


# --------- PYODIDE:tests --------- #
votes = [{"France": 5, "Espagne": 3, "Irlande": 1}]
assert cumul_votes(votes) == {"France": 5, "Espagne": 3, "Irlande": 1}

votes = [
    {"France": 5, "Espagne": 3, "Irlande": 1},
    {"Portugal": 5, "Espagne": 3, "Irlande": 1},
    {"France": 5, "Estonie": 3, "Irlande": 1},
]
assert cumul_votes(votes) == {"France": 10, "Espagne": 6, "Irlande": 3, "Portugal": 5, "Estonie": 3}


# --------- PYODIDE:secrets --------- #
votes = [
    {"France": 5, "Espagne": 3, "Irlande": 1},
    {"France": 5, "Espagne": 3, "Irlande": 1},
    {"France": 5, "Espagne": 3, "Irlande": 1},
]
assert cumul_votes(votes) == {"France": 15, "Espagne": 9, "Irlande": 3}
votes = [
    {"Angleterre": 5, "Espagne": 3, "Irlande": 1},
    {"Ecosse": 5, "Italie": 3, "Slovénie": 1},
    {"France": 5, "Finlande": 3, "Malte": 1},
]
assert cumul_votes(votes) == {
    "Angleterre": 5,
    "Espagne": 3,
    "Irlande": 1,
    "Ecosse": 5,
    "Italie": 3,
    "Slovénie": 1,
    "France": 5,
    "Finlande": 3,
    "Malte": 1,
}
