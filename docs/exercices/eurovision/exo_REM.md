Il est également possible de ne pas utiliser la méthode `item` :

```python
def cumul_votes(votes):
    resultat = {}
    for un_vote in votes:
        for pays in un_vote:
            if pays not in resultat:
                resultat[pays] = un_vote[pays]
            else:
                resultat[pays] += un_vote[pays]
    return resultat
```