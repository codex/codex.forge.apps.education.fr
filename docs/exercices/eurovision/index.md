---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Concours de l'Eurovision
tags:
    - double boucle
    - dictionnaire
difficulty: 290
maj: 31/03/2024
---


# Concours de l'Eurovision

Le concours de l'Eurovision est une compétition qui permet de désigner la meilleure chanson européenne.

Pour cela, chaque pays membre attribue $1$, $3$ ou $5$ points à trois pays de son choix.


???+ example "Exemple de votes"
	L'Espagne peut par exemple décider d'attribuer $5$ points à la France, $3$ à l'Estonie et $1$ à l'Irlande, alors que la France peut attribuer $5$ points au Portugal, $3$ à l'Espagne et $1$ à l'Irlande.

Les votes d'un pays sont représentés par un dictionnaire ayant pour clés les noms des pays et pour valeurs les points attribués. L'ensemble des votes est donc représenté par une liste de dictionnaires.

Pour déterminer le vainqueur, on souhaite additionner, pour chaque pays, l'ensemble des points qui lui ont été attribués.

Écrire la fonction `cumul_votes` qui :

* prend en paramètre une liste `votes` ;

* renvoie un dictionnaire contenant le cumul des votes.

On garantit que la liste n'est pas vide et que chaque dictionnaire représente un vote valide.
	
???+ example "Exemples"
	
	```pycon title=""
    >>> votes = [{"France": 5, "Espagne": 3, "Irlande": 1}]
    >>> cumul_votes(votes)
    {'France': 5, 'Espagne': 3, 'Irlande': 1}
	>>> votes = [
    ...     {"France": 5, "Espagne": 3,"Irlande": 1},
    ...     {"Portugal": 5, "Espagne": 3,"Irlande": 1},
    ...     {"France": 5, "Estonie": 3,"Irlande": 1}
    ... ]
	>>> cumul_votes(votes)
	{'France': 10, 'Espagne': 6, 'Irlande': 3, 'Portugal': 5, 'Estonie': 3}
	```

??? note "Indice"
	Il s'agit d'un problème d'union de dictionnaires comme présenté dans  {{  lien_exo("cet exercice", "union_dictionnaires") }}
	
{{ IDE('exo') }}
