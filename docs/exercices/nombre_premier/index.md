---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Nombre premier
tags:
    - maths
    - int
    - à trous
difficulty: 130
maj: 01/02/2024
---



Un nombre premier est un entier positif qui admet exactement deux diviseurs entiers et positifs distincts : $1$ et lui-même. Ainsi :

* $0$ n'est pas premier car il est divisible par tous les entiers non nuls ;
* $1$ n'est pas premier car il n'admet que lui même comme diviseur ;
* $2$ est premier car il n'est divisible que par $1$ et $2$ ;
* $6$ n'est pas premier car il admet quatre diviseurs : $1$, $2$, $3$ et $6$.

??? "Divisibilité"

    $a$ est divisible par $b$ si le reste de la division euclidienne de $a$ par $b$ vaut $0$.

    Avec **Python** l'expression `#!py a % b` est alors évaluée à `#!py 0`.

Cette définition permet de formuler un test de primalité simple. Considérons un nombre entier positif $n$ :

* Si $n$ est strictement inférieur à $2$, il n'est pas premier ;
* Sinon, si $n$ est divisible par l'un des entiers compris entre $2$ et $\sqrt{n}$ (inclus l'un et l'autre), alors il n'est pas premier ;
* Dans le cas contraire, $n$ est premier.

On demande d'écrire la fonction `est_premier` qui prend en paramètre un entier $n$ positif ou nul et renvoie `#!py True` si ce nombre est premier, `#!py False` dans le cas contraire.

Deux versions, construites autour de boucles `#!py for` ou `#!py while`, sont proposées.

???+ example "Exemples"

    ```pycon title=""
    >>> est_premier(0)
    False
    >>> est_premier(1)
    False
    >>> est_premier(2)
    True
    >>> est_premier(3)
    True
    >>> est_premier(4)
    False
    >>> est_premier(31)
    True
    ```

{{ remarque('assertion') }}

??? question "Version avec une boucle `#!py for`"

    Une première approche consiste à utiliser une boucle `#!py for` afin de parcourir tous les diviseurs potentiels de $n$ entre $2$ et $\sqrt{n}$ (inclus l'un et l'autre).

    La fonction permettant de calculer la racine carrée d'un nombre positif fait partie du module `#!py math`. Elle est importée au début de cette version de l'exercice : `#!py from math import sqrt`.

    Attention toutefois, cette fonction renvoie toujours un nombre flottant : `#!py sqrt(5)` est évalué à environ `#!py 2.23606797749979`.

    On peut néanmoins arrondir ce résultat à l'entier inférieur ou égal en utilisant `#!py int` : `#!py int(sqrt(5))` est évalué à `#!py 2`.

    On rappelle de plus que l'instruction `#!py range(5, 12)` parcourt les entiers entre `#!py 5` (inclus) et `#!py 12` (exclu).

    === "Version vide"
        {{ IDE('./pythons/exo_for_b') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_for_a') }}

??? question "Version avec une boucle `#!py while`"

    Le calcul d'une racine carrée est coûteux. Il est possible de s'en passer en observant que si $d$ est un entier positif tel que $d \le \sqrt{n}$ alors on a nécessairement $d^2 \le n$.

    Cette observation permet de parcourir tous les diviseurs souhaités à l'aide d'une boucle `#!py while`.

    === "Version vide"
        {{ IDE('./pythons/exo_while_b') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_while_a') }}
