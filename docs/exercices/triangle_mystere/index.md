---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Triangle mystérieux
tags:
    - SNT
    - maths
difficulty: 90
maj: 21/10/2024
ides:
    forbid_corr_and_REMs_with_infinite_attempts: false
    forbid_hidden_corr_and_REMs_without_secrets: false
---

On souhaite dans cet exercice construire une figure géométrique à l'aide d'un processus *itératif* : nous allons reproduire, à plusieurs reprises, les mêmes étapes de construction.

La démarche est la suivante :

* on considère trois points $A$, $B$ et $C$ formant un triangle ;

* on place, **à l'intérieur du triangle**, un point $P$ aléatoire ;

* on répète, autant de fois que souhaité, les étapes suivantes :
    * on choisit aléatoirement un des points $A$, $B$ ou $C$ ;
    
    * on construit le milieu du segment d'extrémités $P$ et ce sommet aléatoire ;
    
    * on recommence en considérant que $P$ est désormais le milieu obtenu à cette étape.

Cette construction permet d'obtenir une « jolie » figure... Laquelle ? Pour le savoir, vous devez réussir l'exercice !

-----------------------------

Cet exercice comporte plusieurs questions permettant d'envisager successivement les différentes étapes de la construction.

Les éditeurs associés à chaque question restent toutefois **indépendants** : vous pouvez les aborder dans l'ordre souhaité.

??? question "Dessiner des points"

    On utilise sur cette page une fonction `#!py dessine_point` qui prend en paramètre les coordonnées d'un point, fournies sous la forme d'un couple de nombres, et affiche ce point sur une figure.
    
    Cette fonction `#!py dessine_point` est déjà chargée dans l'éditeur : **il n'est pas demandé de la rédiger**. Vous devez simplement l'utiliser !
    
    Reproduire la figure ci-dessous dans laquelle les sommets du rectangles ont pour coordonnées :
    
    * $\left(0~;~0\right)$ ;
    * $\left(400~;~ 0\right)$ ;
    * $\left(400~;~ 200\right)$ ;
    * $\left(0~;~ 200\right)$.

    Les autres points sont régulièrement espacés de $20$ pixels.

    ![Rectangle](images/rectangle.svg){width=25% .center}
    
    ??? tip "Boucle `#!py for`"
    
        On pourra utiliser une boucle `#!py for` comme dans l'exemple ci-dessous qui dessine le premier « segment » du rectangle :
        
        ```python title=""
        for x in range(0, 420, 20):  # x varie entre 0 (inclus) et 420 (exclu), de 20 en 20
            P = (x, 0)
            dessine_point(P)
        ```
    
    {{ IDE('pythons/exo_dessine', MODE="delayed_reveal") }}

    {{ figure('figure_dessine', div_class='py_mk_figure center autolight', admo_kind='???+') }}

??? question "Point aléatoire dans un triangle"

    Le point $P$, utilisé au départ de la construction, doit être choisi aléatoirement à l'intérieur du triangle $ABC$. Nous allons pour ce faire calculer une *moyenne pondérée* des coordonnées des sommets.
    
    L'idée est la suivante :
    
    * on se donne trois points $A~\left(x_A~;~y_A\right)$, $B~\left(x_B~;~y_B\right)$ et $C~\left(x_C~;~y_C\right)$ ;

    * on choisit aléatoirement trois coefficients positifs $a$, $b$ et $c$ dont la somme est non nulle ;

    * les coordonnées du point $P$ sont alors :

    $$x_P = \frac{a\times x_A+b\times x_B+c\times x_C}{a+b+c}\\
      \qquad\qquad\qquad  y_P = \frac{a\times y_A+b\times y_B+c\times y_C}{a+b+c}\\
    $$
    
    Compléter le code ci-dessous afin de calculer les coordonnées du point $P$.
    
    {{ IDE('pythons/exo_moyenne', MODE='revealed') }}

    
??? question "Milieu d'un segment"

    Soit $A$ et $B$ deux points du plan de coordonnées $\left(x_A~;~y_A\right)$ et $\left(x_B~;~y_B\right)$. Ces points sont représentés en Python par deux couples de nombres `#!py (x_A, y_A)` et `#!py (x_B, y_B)`.
    
    Écrire la fonction `milieu` qui prend en paramètres les couples de coordonnées `a` et `b` des points $A$
    et $B$ et renvoie les coordonnées du milieu $M$ de $\left[AB\right]$.
    
    ??? tip "`x, y = coords`"
    
        On rappelle que Python permet facilement de récupérer les différents éléments d'un couple de coordonnées en faisant par exemple :
        
        ```pycon title="Récupérer des coordonnées"
        >>> coords = (3, 5)
        >>> x, y = coords
        >>> x
        3
        >>> y
        5
        ```
    
    ??? note "~~Entier~~ $\longrightarrow$ Décimal"
    
        Lorsque Python effectue une division, il considère toujours que le résultat est un nombre décimal (un nombre flottant dit-on en informatique).
        
        C'est pourquoi l'expression `#!py (2 + 6) / 2` est évaluée à `#!py 4.0`.
        
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> A = (0, 2)
        >>> B = (3, 6)
        >>> milieu(A, B)
        (1.5, 4.0)
        >>> C = (10, 4)
        >>> D = (5, -8)
        >>> milieu(C, D)
        (7.5, -2.0)
        ```

    === "Version vide"
        {{ IDE('pythons/exo_milieu_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_milieu_trous') }}

??? question "Figure complète"

    À ce stade, nous savons :
    
    * dessiner des points ;
    
    * placer un point aléatoirement à l'intérieur d'un triangle ;
    
    * calculer les coordonnées du milieu d'un segment.

    Nous pouvons donc, en assemblant ces différents éléments, construire la figure souhaitée.
    
    Quelques dernières remarques :
    
    * on propose d'utiliser les points $A$, $B$ et $C$ de coordonnées $A~(0~;~0)$, $B~(500~;~0)$ et $C~(250~;~433)$ mais vous pouvez utiliser d'autres points ;
    * la fonction `#!py milieu` est déjà importée dans l'éditeur, vous pouvez directement l'utiliser ;
    * on propose de répéter le processus de construction $20$ fois. Là encore **vous pouvez modifier cette valeur**.

    Compléter l'éditeur ci-dessous afin de construire la figure souhaitée !

    ??? info "Rappel de l'algorithme de construction"
    
        ```text title=""
        Fixer le nombre de répétitions à 20
        
        Créer les points A, B et C
        Créer la liste des sommets contenant les points A, B et C
        Dessiner les points A, B et C
        
        Créer les coefficients aléatoires a, b et c
        Calculer les coordonnées du point P initial
        Dessiner le point P
        
        Répéter "nombre de répétitions" fois :
            Choisir un sommet aléatoire
            Remplacer les coordonnées de P par celles du milieu du segment [P ; sommet aléatoire]
            Dessiner le point P
        ```

    ??? tip "Choisir un élément aléatoire"
    
        On choisira un sommet aléatoire en utilisant la fonction `#!py choice` du module `#!py random` :
        
        ```python title=""
        # Import de la fonction
        from random import choice
        
        # La liste dans laquelle nous allons piocher
        neveux = ["Riri", "Fifi", "Loulou"]
        
        # Choix d'un neveu aléatoire parmi ceux listés dans « neveux »
        neveu_aleatoire = choice(neveux)
        ```

    === "Version vide"
        {{ IDE('pythons/exo_triangle_vide', MODE="delayed_reveal") }}

        {{ figure('figure_vide', div_class='py_mk_figure center autolight', admo_kind='???+') }}
        
    === "Version à compléter"
        {{ IDE('pythons/exo_triangle_trous',  MODE="delayed_reveal") }}

        {{ figure('figure_trous', div_class='py_mk_figure center autolight', admo_kind='???+') }}