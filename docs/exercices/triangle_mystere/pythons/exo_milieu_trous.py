# --------- PYODIDE:code --------- #
def milieu(point_A, point_B):
    x_A, y_A = ...
    ...
    x_M = ...
    ...
    return (..., ...)


# --------- PYODIDE:corr --------- #
def milieu(point_A, point_B):
    x_A, y_A = point_A
    x_B, y_B = point_B
    x_M = (x_A + x_B) / 2
    y_M = (y_A + y_B) / 2
    return (x_M, y_M)


# --------- PYODIDE:tests --------- #

A = (0, 2)
B = (3, 6)
assert milieu(A, B) == (1.5, 4.0)
C = (10, 4)
D = (5, -8)
assert milieu(C, D) == (7.5, -2.0)

# --------- PYODIDE:secrets --------- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    A = (x_A, y_A)
    B = (x_B, y_B)
    attendu = ((x_A + x_B) / 2, (y_A + y_B) / 2)
    assert milieu(A, B) == attendu, f"Erreur avec a {A} et b {B}"
