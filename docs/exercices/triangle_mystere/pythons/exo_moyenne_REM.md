Dans les faits, nous utiliserons de « vrais » coefficients aléatoires. Ceux-ci peuvent être générés par la fonction `#!py random` en faisant :

```python
# Import de la fonction
from random import random

# Les coefficients aléatoires
a = random()
b = random()
c = random()
```

La fonction [`#!py random`](https://docs.python.org/fr/3/library/random.html#random.random) renvoie un nombre *pseudo-aléatoire* compris entre $0$ (inclus) et $1$ (exclu).