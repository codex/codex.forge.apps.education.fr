La figure obtenue ressemble fortement au [*triangle de Sierpińsky*](https://fr.wikipedia.org/wiki/Triangle_de_Sierpi%C5%84ski){target="_blank"}.

Le processus de construction que nous venons de mettre en œuvre s'appelle le [jeu du chaos](https://fr.wikipedia.org/wiki/Jeu_du_chaos){target="_blank"}. Il est possible de le mener à partir de polygones comptant un nombre quelconque de côtés.

Si ce côté est noté $n$, alors, placer à chaque étape le nouveau point $P$ au $\dfrac{n}{n+3}$-ième du segment permet d'obtenir une « jolie » figure. Pour $n=3$, on retrouve bien le milieu : $\dfrac{3}{3+3}=\dfrac12$.

<center>
![Quatre côtés](images/4.svg){width=30%} ![Cinq côtés](images/5.svg){width=30%} ![Six côtés](images/6.svg){width=30%}
</center>