# --- PYODIDE:env --- #
import js
import drawsvg as draw

LARGEUR = 400
HAUTEUR = 200
DECALAGE_X = 20
DECALAGE_Y = DECALAGE_X
R_POINT = 3
ARGS_DESSIN = {"fill": "lightblue", "stroke_width": 1, "stroke": "blue"}
figure = draw.Drawing(
    LARGEUR + DECALAGE_X * 2, HAUTEUR + DECALAGE_Y * 2, origin="bottom-left"
)


def dessine_point(coords, args=None):
    """Dessine le point dont on fournit les coordonnées sous forme d'un couple de nombres"""
    if args is None:
        args = ARGS_DESSIN
    x, y = coords
    figure.append(draw.Circle(x + DECALAGE_X, -y - DECALAGE_Y, R_POINT, **args))


# --- PYODIDE:code --- #
...
# --- PYODIDE:corr --- #
for x in range(0, 400, 20):
    P = (x, 0)
    dessine_point(P)
    P = (x, 200)
    dessine_point(P)

for y in range(0, 200, 20):
    P = (0, y)
    dessine_point(P)
    P = (400, y)
    dessine_point(P)

# --- PYODIDE:post --- #
js.document.getElementById("figure_dessine").innerHTML = figure.as_svg()
