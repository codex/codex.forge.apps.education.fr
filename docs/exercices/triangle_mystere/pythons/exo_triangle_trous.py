# --- PYODIDE:env --- #
import js
from random import choice, random
import drawsvg as draw

TAILLE = 500
DECALAGE_X = 20
DECALAGE_Y = DECALAGE_X
R_POINT = 3
ARGS_DESSIN = {"fill": "lightblue", "stroke_width": 1, "stroke": "blue"}
figure = draw.Drawing(
    TAILLE + DECALAGE_X * 2, TAILLE + DECALAGE_Y * 2, origin="bottom-left"
)


def dessine_point(coords, args=None):
    """Dessine le point dont on fournit les coordonnées sous forme d'un couple de nombres"""
    if args is None:
        args = ARGS_DESSIN
    x, y = coords
    figure.append(draw.Circle(x + DECALAGE_X, -y - DECALAGE_Y, R_POINT, **args))


def milieu(point_A, point_B):
    """Calcule et renvoie les coordonnées du milieu du segment [ab]"""
    x_A, y_A = point_A
    x_B, y_B = point_B
    x_M = (x_A + x_B) / 2
    y_M = (y_A + y_B) / 2
    return (x_M, y_M)


# --- PYODIDE:code --- #
# Le nombre de répétitions du processus
# N'hésitez pas à augmenter cette valeur
nb_points = 20

# Le triangle de base
A = (..., ...)
B = ...
C = ...
sommets = [...]
dessine_point(A)
...
...


# Le point initial aléatoire
a = random()
b = ...
...
xa, ya = A
xb, yb = B
xc, yc = C
P = (..., ...)
dessine_point(P)

# Les "nb_points" points successifs
for n in range(nb_points):
    sommet_aleatoire = ...
    P = ...
    dessine_point(P)
# --- PYODIDE:corr --- #
nb_points = 20

# Le triangle de base
A = (0, 0)
B = (500, 0)
C = (250, 433)
sommets = [A, B, C]
dessine_point(A)
dessine_point(B)
dessine_point(C)

# Le point initial aléatoire
a = random()
b = random()
c = random()
x_A, y_A = A
x_B, y_B = B
x_C, y_C = C
P = ((a * x_A + b * x_B + c * x_C) / (a + b + c), (a * y_A + b * y_B + c * y_C) / (a + b + c))
dessine_point(P)

# Les "nb_points" points successifs
for n in range(nb_points):
    sommet_aleatoire = choice(sommets)
    P = milieu(P, sommet_aleatoire)
    dessine_point(P)

# --- PYODIDE:post --- #
js.document.getElementById("figure_trous").innerHTML = figure.as_svg()