# --- PYODIDE:code --- #
# Les coordonnées des points
A = (10, 20)
B = (40, 70)
C = (25, 30)
x_A, y_A = A
x_B, y_B = B
x_C, y_C = C


# Les coefficients
a = 0.25
b = 0.95
c = 0.80

# Le point P
x_P = ...
y_P = ...

print(f"Le point P généré a pour coordonnées {x_P, y_P}")
# --- PYODIDE:secrets --- #
assert (x_P, y_P) == (30.25, 47.75)
# --- PYODIDE:corr --- #
x_P = (a * x_A + b * x_B + c * x_C) / (a + b + c)
y_P = (a * y_A + b * y_B + c * y_C) / (a + b + c)
