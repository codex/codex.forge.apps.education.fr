---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Détermination d'un seuil (2)
tags:
    - à trous
    - boucle
    - maths
difficulty: 150
---

Soit $(u_n)$ la suite définie pour tout entier naturel $n$ par :

\[
  \begin{cases}
  u_0 = 1 \\
  u_{n+1} = 0,75 u_n + 7 \quad \text{ pour }n \geqslant 0
  \end{cases}  
\]

On peut montrer que cette suite tend vers $28$ quand $n$ tend vers $+\infty$. Cela signifie que pour toute valeur de $\epsilon > 0$, il existe un rang $N$ à partir duquel on a, pour tout $n \geqslant N$ :

$$28 - \epsilon \leqslant u_n \leqslant 28 + \epsilon$$

La dernière condition peut aussi s'écrire de façon plus concise:
 
$$|u_n - 28| \leqslant \epsilon$$
 
On rappelle à ce titre que $|a - b|$ est la valeur absolue de $a-b$ et peut s'interpréter comme la *distance* entre les nombres $a$ et $b$. Python permet de calculer la valeur absolue d'un nombre `x` en faisant `#!py abs(x)`.

Écrire le code de la fonction `seuil` qui prend en paramètre le nombre `precision` et renvoie
la valeur du plus petit entier tel que l'on ait $|u_n - 28| \leqslant \text{precision}$.

On garantit que `precision` est un nombre réel supérieur ou égal à $10^{-14}$.



{{ remarque('assertion') }}

???+ example "Exemples"

    ```pycon title=""
    >>> seuil(1)    # u_12 ≃ 27,1 et ǀ27,1 - 28ǀ ⩽ 1
    12
    >>> seuil(0.1)  # u_20 ≃ 27,91 et ǀ27,91 - 28ǀ ⩽ 0,1
    20
    ```
    
=== "Version vide"
    {{ IDE('./pythons/exo_b')}}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a')}}

    
    
