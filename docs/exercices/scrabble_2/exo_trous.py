# --------- PYODIDE:code --------- #
VALEURS =  {
    'A': 1, 'B': 3, 'C': 3, 'D': 2, 'E': 1, 'F': 4,
    'G': 2, 'H': 4, 'I': 1, 'J': 8, 'K': 10, 'L': 1,
    'M': 2, 'N': 1, 'O': 1, 'P': 3, 'Q': 8, 'R': 1,
    'S': 1, 'T': 1, 'U': 1, 'V': 4, 'W': 10, 'X': 10,
    'Y': 10, 'Z': 10
}

BONUS_LETTRE = {'+': 2, '*': 3}

BONUS_MOT = {'#': 2, '@': 3}

def calcul_score(mot, cases):
    score = ...
    multiplicateur_total = ...
    for i in range(len(mot)):
        lettre, symbole_bonus = mot[i], cases[i]
        points_lettre = ...
        if symbole_bonus in BONUS_LETTRE:
            points_lettre = ...
        elif symbole_bonus in ... :
            multiplicateur_total = ...
        score = score + ...
    return ...

# --------- PYODIDE:corr --------- #
VALEURS =  {
    'A': 1, 'B': 3, 'C': 3, 'D': 2, 'E': 1, 'F': 4,
    'G': 2, 'H': 4, 'I': 1, 'J': 8, 'K': 10, 'L': 1,
    'M': 2, 'N': 1, 'O': 1, 'P': 3, 'Q': 8, 'R': 1,
    'S': 1, 'T': 1, 'U': 1, 'V': 4, 'W': 10, 'X': 10,
    'Y': 10, 'Z': 10
}

BONUS_LETTRE = {'+': 2, '*': 3}
BONUS_MOT = {'#': 2, '@': 3}

def calcul_score(mot, cases):
    score = 0
    multiplicateur_total = 1
    for i in range(len(mot)):
        lettre, symbole_bonus = mot[i], cases[i]
        points_lettre = VALEURS[lettre]
        if symbole_bonus in BONUS_LETTRE:
            points_lettre = points_lettre * BONUS_LETTRE[symbole_bonus]
        elif symbole_bonus in BONUS_MOT:
            multiplicateur_total = multiplicateur_total * BONUS_MOT[symbole_bonus]
        score = score + points_lettre
    return score * multiplicateur_total

# --------- PYODIDE:tests --------- #
assert calcul_score("GIRAFE","+---#-") == 24
assert calcul_score("KAYAK", "--+--") == 42
assert calcul_score("INFORMATIQUE", "--@---------") == 69

# --------- PYODIDE:secrets --------- #
# autres tests
assert calcul_score("OXYGENER", "@--+---@") == 261
assert calcul_score("PROGRAMME", "*---*---*") == 24
assert calcul_score("PYTHON", "------") == 20


