Il est aussi possible de compléter les dictionnaires des bonus afin qu'ils couvrent tous l'ensemble des caractères existant. Ceci permet de se passer de structures conditionnelles (`#!py if ... elif ...`) :

```python title=""
BONUS_LETTRE = {'+': 2, '*': 3, '-': 1, '#': 1, '@': 1}
BONUS_MOT = {'#': 2, '@': 3, '+': 1, '*': 1, '-': 1}

def calcul_score(mot, cases):
    score = 0
    bonus_mot = 1
    for i in range(len(mot)):
        lettre, symbole_bonus = mot[i], cases[i]
        points_lettre = VALEURS[lettre]
        score += points_lettre * BONUS_LETTRE[ symbole_bonus ]
        bonus_mot *= BONUS_MOT[ symbole_bonus ]
    return score * bonus_mot
```