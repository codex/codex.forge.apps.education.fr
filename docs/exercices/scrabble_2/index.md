---
author:
    - Sébastien Hoarau
    - Fabrice Nativel
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Score d'un mot au Scrabble (II)
tags:
    - à trous
    - string
    - dictionnaire
difficulty: 250
maj: 02/11/2024
---

Au Scrabble, chaque lettre possède une valeur et le score d'un mot est la somme des valeurs des lettres qui le compose. Ce score peut être modifié par des bonus liés au placement du mot sur le plateau de jeu. Les bonus possibles sont :

* « *lettre compte double* » qui multiplie par 2 la valeur de la lettre positionnée sur cet emplacement ;
* « *lettre compte triple* » qui multiplie par 3 la valeur de la lettre positionnée sur cet emplacement ;
* « *mot compte double* » qui multiplie par 2 la valeur totale du mot (après application des éventuels bonus de lettre) ;
* « *mot compte triple* » qui multiplie par 3 la valeur totale du mot (après application des éventuels bonus de lettre).

Une case du plateau ne peut contenir qu'un seul bonus. Il est possible que le score d'un mot soit modifié par plusieurs bonus « mot ». Dans ce cas, les bonus sont tous appliqués.

???+ example "Exemple"

    On place le mot :

    ![GIRAFE](girafe.svg){.center width=30%}

    sur les cases suivantes :

    ![Cases](cases.svg){.center width=30%}

    Sa valeur est : $\left(2\times2 + 1 + 1 + 1 + 4 + 1 \right) \times 2 = 24$

On modélise les propriétés des cases sur lesquelles sont posées les lettres par les symboles suivants :

* `#!py "+"` indique un bonus « *lettre compte double* »  ;
* `#!py "*"` indique un bonus « *lettre compte triple* »  ;
* `#!py "#"` indique un bonus « *mot compte double* »  ;
* `#!py "@"` indique un bonus « *mot compte triple* » ;
* `#!py "-"` indique une case sans bonus.

Les cases sur lesquelles sont posées les lettres d'un mot sont modélisées par une chaine de caractères que l'on appellera `#!py cases`. Dans l'exemple précédent, `#!py cases` est la chaine `#!py "+---#-"`.

Compléter la définition de la fonction `calcul_score` qui prend en paramètres deux chaines de caractères :

* `mot` dont on veut calculer le score ;
* `cases` représentant l'emplacement du mot.

Cette fonction calcule le score du `mot` en tenant compte des éventuels bonus des `cases`.

On fournit trois dictionnaires `#!py VALEURS`, `#!py BONUS_LETTRE` et `#!py BONUS_MOT` contentant les valeurs des lettres
au Scrabble et les coefficients multiplicateurs des différents bonus.

=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
