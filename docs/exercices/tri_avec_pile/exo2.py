# --------- PYODIDE:env --------- #

def insertion(x, pile):
    if len(pile) == 0 or x < pile[len(pile)-1]:
        pile.append(x)
    else:
        y = pile.pop()
        insertion(x, pile)
        pile.append(y)

# --------- PYODIDE:code --------- #

def tri(tab):
    ...

# --------- PYODIDE:corr --------- #

def tri(tab):
    n = len(tab)
    pile = []
    for i in range(n):
        insertion(tab[i], pile)
    for i in range(n):
        tab[i] = pile.pop()

# --------- PYODIDE:tests --------- #

t = []
tri(t)
assert t == []
t = [5]
tri(t)
assert t == [5]
t = [8, 5, 9, 3, 8, 2, 7, 1, 5]
tri(t)
assert t == [1, 2, 3, 5, 5, 7, 8, 8, 9]

# --------- PYODIDE:secrets --------- #

# tests

t = []
tri(t)
assert t == []
t = [5]
tri(t)
assert t == [5]
t = [8, 5, 9, 3, 8, 2, 7, 1, 5]
tri(t)
assert t == [1, 2, 3, 5, 5, 7, 8, 8, 9]

# autres tests

from random import shuffle
t = [i for i in range(100)]
shuffle(t)
tri(t)
assert t == [i for i in range(100)]
