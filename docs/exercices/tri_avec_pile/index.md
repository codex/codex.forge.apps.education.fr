---
author: Serge Bays
hide:
    - navigation
    - toc
title: Tri avec une pile
tags:
    - pile
    - tri
    - récursivité
difficulty: 210
maj: 25/01/2025
---

# Tri avec une pile

L'objectif est de trier les éléments d'un tableau. On suppose que les éléments contenus dans le tableau peuvent être comparés avec l'opérateur `<`.

Le tri est effectué suivant l'ordre croissant. Donc, lorsque le tableau est trié, les éléments y sont rangés du plus petit au plus grand. Un tableau vide est trié.

On se sert d'une pile et pour la représenter on utilise simplement une liste Python avec **uniquement les méthodes `append`, `pop` et la fonction `len`**. Attention, **on ne peut pas utiliser d'indice avec une pile**.


??? question "1. Insertion dans une pile"

    Écrire une fonction **récursive** `insertion` qui insère un élément `x` à la bonne place dans une pile où les éléments sont empilés du plus grand au plus petit. L'élément `x` et la pile sont les paramètres de la fonction. Cette pile est modifiée par la fonction qui ne renvoie rien.

    ???+ example "Exemple"

        ```pycon title=""
        >>> p = [8, 7, 5, 4, 3, 1]
        >>> x = 6
        >>> insertion(x, p)
        >>> p
        [8, 7, 6, 5, 4, 3, 1]
        ```

    {{ IDE('exo1') }}


??? question "2. Tri d'un tableau"
    
    Écrire une fonction `tri` qui prend en paramètre un tableau à trier, insère les éléments de ce tableau dans une pile à l'aide de la fonction `insertion` et  lorsque tous les éléments ont été insérés, les retire de la pile et les place un à un (à la bonne place) dans le tableau qui est alors trié suivant l'ordre croissant. Le tableau est modifié et la fonction ne renvoie rien.

    ???+ example "Exemple"

        ```pycon title=""
        >>> t = [8, 5, 9, 3, 8, 2, 7, 1, 5]
        >>> tri(t)
        >>> t
        [1, 2, 3, 5, 5, 7, 8, 8, 9]
        ```

    {{ IDE('exo2') }}