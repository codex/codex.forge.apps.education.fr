La complexité du tri est la même que celle du tri insertion classique (linéaire dans le meilleur des cas, quadratique dans le pire des cas et en moyenne).

Mais avec le programme donné dans la correction, le pire des cas est celui où les éléments du tableau initial sont rangés suivant l'ordre croissant. 
Si on souhaite retrouver les mêmes cas que ceux du tri insertion 
classique, on peut écrire la fonction `tri`ainsi:

```python
def tri(tab):
    n = len(tab)
    pile = []
    for i in range(n-1, -1, 0)
        insertion(tab[i], pile)
    for i in range(n):
        tab[i] = pile.pop()
```