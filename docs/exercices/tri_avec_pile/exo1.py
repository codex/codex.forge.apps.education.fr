

# --------- PYODIDE:code --------- #

def insertion(x, pile):
    ...

# --------- PYODIDE:corr --------- #

def insertion(x, pile):
    if len(pile) == 0:
        pile.append(x)
    else:
        y = pile.pop()
        if x > y:
            insertion(x, pile)
            pile.append(y)
        else:
            pile.append(y)
            pile.append(x)

# --------- PYODIDE:tests --------- #

x = 5
p = []
insertion(x, p)
assert p == [5]
p = [3]
insertion(x, p)
assert p == [5, 3]
p = [8, 7, 5, 4, 3, 1]
x = 6
insertion(x, p)
assert p == [8, 7, 6, 5, 4, 3, 1]

# --------- PYODIDE:secrets --------- #

# tests

x = 5
p = []
insertion(x, p)
assert p == [5]
p = [3]
insertion(x, p)
assert p == [5, 3]
p = [8, 7, 5, 4, 3, 1]
x = 6
insertion(x, p)
assert p == [8, 7, 6, 5, 4, 3, 1]

# autres tests

p = [i for i in range(100, 0, -1)]
insertion(50, p)
assert p == [i for i in range(100, 50, -1)] + [50] + [i for i in range(50, 0, -1)]
