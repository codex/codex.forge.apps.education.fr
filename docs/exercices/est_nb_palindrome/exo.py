

# --------- PYODIDE:code --------- #

def inverse_chaine(chaine):
    resultat = ...
    for caractere in chaine:
        resultat = ...
    return resultat


def est_palindrome(chaine):
    inverse = inverse_chaine(chaine)
    return ...


def est_palindromique(nombre):
    chaine = ...
    return est_palindrome(chaine)

# --------- PYODIDE:corr --------- #

def inverse_chaine(chaine):
    resultat = ""
    for caractere in chaine:
        resultat = caractere + resultat
    return resultat


def est_palindrome(chaine):
    inverse = inverse_chaine(chaine)
    return chaine == inverse


def est_palindromique(nombre):
    chaine = str(nombre)
    return est_palindrome(chaine)

# --------- PYODIDE:tests --------- #

assert inverse_chaine("bac") == "cab"
assert est_palindrome("NSI") is False
assert est_palindrome("ISN-NSI") is True
assert est_palindromique(214312) is False
assert est_palindromique(213312) is True

# --------- PYODIDE:secrets --------- #

# tests

assert inverse_chaine("bac") == "cab"

assert est_palindrome("NSI") is False

assert est_palindrome("ISN-NSI") is True

assert est_palindromique(214312) is False

assert est_palindromique(213312) is True


# autres tests


assert inverse_chaine("azerty") == "ytreza"
assert inverse_chaine("azerty0") == "0ytreza"

assert est_palindrome("AZERTY") is False
assert est_palindrome("AZERTY0") is False

assert est_palindrome("ABCTCBA") is True
assert est_palindrome("ABCDDCBA") is True

assert est_palindromique(132213) is False
assert est_palindromique(1327213) is False

assert est_palindromique(122345543221) is True
assert est_palindromique(1223457543221) is True
