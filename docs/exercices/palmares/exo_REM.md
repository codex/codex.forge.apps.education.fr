D'un point de vue formel, il s'agit de rechercher la valeur maximale dans un dictionnaire ainsi que la liste des clés associées à cette valeur maximale.

Les notes étant garanties positives ou nulles, on peut initialiser la variable `note_max` à `0`.

* La solution proposée utilise la méthode `values()` et un parcours des valeurs du dictionnaire. Si une valeur est strictement supérieure à la note maximale, on met celle-ci à jour. Le second parcours permet de sélectionner les prénom des élèves ayant eu la note maximale. 
* Au lieu d'utiliser la méthode `values()`, on peut également parcourir l'ensemble des clés du dictionnaire (les élèves) en testant les valeurs associées (leurs notes respectives). 
* On aurait aussi utiliser une liste en compréhension pour le second parcours :

```python
def palmares(notes):
    note_max = 0
    for prenom in notes:
        if notes[prenom] > note_max:
            note_max = notes[prenom]
    return (note_max, [prenom for prenom in notes if notes[prenom] == note_max])
```

Il est aussi possible de procéder en une seule passe :

```python
def palmares(notes):
    note_max = 0
    eleves = []
    for prenom, note in notes.items():
        if note > note_max:
            note_max = note
            eleves = [prenom]
        elif note == note_max:
            eleves.append(prenom)
    return (note_max, eleves)
```
