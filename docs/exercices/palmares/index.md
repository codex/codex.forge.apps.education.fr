---
author:
    - Mireille Coilhac
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Palmarès
difficulty: 210
tags:
    - dictionnaire
    - tuple
    - ep2
maj: 01/02/2024
---

Après un devoir, un professeur désire connaître les prénoms de l'élève ou des élèves ayant obtenus la meilleure note.

Les notes sont fournies dans un dictionnaire **Python** dans lequel les clés sont les prénoms des élèves et les valeurs leur note.

```python
notes = {
  "Alice": 18,
  "Bob": 10,
  "Charles": 8,
  "David": 12,
  "Erwann": 18,
  "Fanny": 14,
  "Guillaume": 18,
  "Hugo": 14
}
```


On désire créer une fonction `palmares` qui prend en paramètre un tel dictionnaire `notes` et renvoie un tuple de deux éléments :

* la note maximale qui a été attribuée ;

* la liste contenant les noms du ou des élèves ayant obtenu cette note.

Dans l'exemple précédent on obtient `#!py (18, ['Alice', 'Erwann', 'Guillaume'])`.

On garantit que le dictionnaire n'est pas vide, que tous les prénoms sont différents et que toutes les notes sont des entiers positifs ou nuls.

??? tip "Pas de problème d'ordre"

    Il est inutile de se soucier de l'ordre des prénoms dans la liste des résultats.
    
    Seule leur présence au sein de la liste importe.

??? warning "Fonction `max` interdite"

    On interdit ici d'utiliser `max` qui diminuerait grandement l'intérêt de l'exercice.

??? success "Un ou deux tours ?"

    Il est possible de résoudre cet exercice à l'aide de deux parcours... mais aussi de le faire en une seule passe !

???+ example "Exemples"

    ```pycon title=""
    >>> notes = {
    ...     "Alice": 18,
    ...     "Bob": 10,
    ...     "Charles": 8,
    ...     "David": 12,
    ...     "Erwann": 17,
    ...     "Fanny": 14,
    ...     "Guillaume": 16,
    ...     "Hugo": 14
    ... }
    >>> palmares(notes)
    (18, ['Alice'])
    >>> notes = {
    ...     "Bob": 10,
    ...     "Charles": 8,
    ...     "David": 12,
    ...     "Hugo": 14,
    ...     "Fanny": 14
    ... }
    >>> palmares(notes)
    (14, ['Hugo', 'Fanny'])
    >>> # on accepte aussi (14, ['Fanny', 'Hugo'])
    >>> notes = {
    ...     "Romuald": 15,
    ...     "Ronan": 14,
    ...     "David": 15
    ... }
    >>> palmares(notes)
    (15, ['David', 'Romuald'])
    >>> # on accepte aussi (15, ['Romuald', 'David'])
    ```

{{ IDE('exo') }}
