

# --------- PYODIDE:code --------- #

def palmares(notes):
    ...

# --------- PYODIDE:corr --------- #

def palmares(notes):
    note_max = 0
    for note in notes.values():
        if note > note_max:
            note_max = note
    eleves = []
    for prenom in notes:
        if notes[prenom] == note_max:
            eleves.append(prenom)
    return (note_max, eleves)

# --------- PYODIDE:tests --------- #

notes = {
    "Alice": 18,
    "Bob": 10,
    "Charles": 8,
    "David": 12,
    "Erwann": 17,
    "Fanny": 14,
    "Guillaume": 16,
    "Hugo": 14,
}
note_max, eleves = palmares(notes)
eleves.sort()  # tri des prénoms
assert (note_max, eleves) == (18, ["Alice"])

notes = {"Bob": 10, "Charles": 8, "David": 12, "Hugo": 14, "Fanny": 14}
note_max, eleves = palmares(notes)
eleves.sort()  # tri des prénoms
assert (note_max, eleves) == (14, ["Fanny", "Hugo"])

notes = {"Romuald": 15, "Ronan": 14, "David": 15}
note_max, eleves = palmares(notes)
eleves.sort()  # tri des prénoms
assert (note_max, eleves) == (15, ["David", "Romuald"])

# --------- PYODIDE:secrets --------- #

# Tests
notes = {
    "Alice": 18,
    "Bob": 10,
    "Charles": 8,
    "David": 12,
    "Erwann": 17,
    "Fanny": 14,
    "Guillaume": 16,
    "Hugo": 14,
}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (18, ["Alice"])

notes = {"Bob": 10, "Charles": 8, "David": 12, "Hugo": 14, "Fanny": 14}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (14, ["Fanny", "Hugo"])

notes = {"Romuald": 15, "Ronan": 14, "David": 15}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (15, ["David", "Romuald"])


# Tests supplémentaires
from random import randint, sample

notes = {
    "Alice": 18,
    "Bob": 18,
    "Charles": 18,
    "David": 18,
    "Erwann": 17,
    "Fanny": 18,
    "Guillaume": 18,
    "Hugo": 18,
}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (
    18,
    ["Alice", "Bob", "Charles", "David", "Fanny", "Guillaume", "Hugo"],
)

notes = {
    "Alice": 20,
    "Bob": 0,
    "Charles": 0,
    "David": 0,
    "Erwann": 20,
    "Fanny": 0,
    "Guillaume": 0,
    "Hugo": 0,
}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (20, ["Alice", "Erwann"])

notes = {"Nicolas": 0}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (0, ["Nicolas"])

notes = {"Nicolas": 20}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (20, ["Nicolas"])

alphabet = [chr(k) for k in range(65, 91)]
for _ in range(20):
    eleves = sample(alphabet, 20)
    notes = {eleve: randint(0, 20) for eleve in eleves}
    maxi = max(notes.values())
    note_max, eleves = palmares(notes)
    assert (note_max, sorted(eleves)) == (
        maxi,
        sorted([e for e in notes if notes[e] == maxi]),
    )
