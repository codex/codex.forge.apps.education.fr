---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: La Belote
tags:
    - à trous
    - dictionnaire
    - programmation orientée objet
difficulty: 200
maj: 31/03/2024
---

La belote est un jeu qui se joue avec 32 cartes, chaque carte ayant une hauteur et une couleur. Les différentes hauteurs sont dans l'ordre 7, 8, 9, 10, Valet, Dame, Roi, As et les différentes couleurs sont « pique », « carreau », « cœur », « trèfle ».

On modélise un paquet de cartes, en *programmation orientée objet*, par deux classes: une classe `Carte` et une classe `Paquet`.

??? info "Les classes `Carte` et `Paquet`"

	Les classes `Carte` et `Paquet` sont définies ainsi :
	
	```mermaid
	classDiagram
    direction RL
    Paquet o-- Carte
    class Paquet{
        list contenu
        __init__()
        cart_pos()
        taille()
        ajoute_carte()
    } 
    class Carte {
        __init__()
        int hauteur
        couleur_carte()
        hauteur_carte()
        int couleur   
    }
    ```

	* **Classe Carte :**

	| méthode | explication |
	|:------------|:--------------|
	| __init__ | prend un entier `h` et un entier `c` et crée la carte correspondante |
	| couleur_carte | renvoie la couleur de la carte |
	| hauteur_carte | renvoie la hauteur de la carte |

	* **Classe Paquet :**

	| méthode | explication |
	|:------------|:--------------|
	| __init__ | crée un paquet vide |
	| carte_pos | prend un entier `n` et renvoie la carte à l'indice `n` |
	| taille | renvoie le nombre de cartes |


    Dans la classe `Carte`, les hauteurs et les couleurs sont repérées par les codes suivants :
	
	```python
    HAUTEURS = {
		1: 'As', 7: '7', 8: '8', 9: '9', 10: '10',
		11: 'Valet', 12: 'Dame', 13: 'Roi'
		}

    COULEURS = {1 : "pique", 2 : "cœur", 3: "carreau", 4 : "trèfle"}
	```
	
	
???+ example "Exemple d'utilisation"
	
	```pycon title=""
	>>> jeu = Paquet()
	>>> jeu.ajoute_carte(Carte(1, 3))  # Ajout de l'as de carreau
	>>> c1 = jeu.carte_pos(0)
	>>> c1.couleur_carte()
	'carreau'
	>>> c1.hauteur_carte()
	'As'
    >>> jeu.taille()
    1
	```

Avant chaque manche, on choisit une couleur d'atout parmi les quatre couleurs.

Chaque carte rapporte des points. Ces points dépendent de la hauteur de la carte, mais aussi du fait que la couleur soit l'atout ou non.

Les points hors atout sont les suivants :

|   7   |   8   |   9   |  10   | Valet | Dame  |  Roi  |  As   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   0   |   0   |   0   |  10   |   2   |   3   |   4   |  11   |

Les points à l'atout sont les suivants :

|   7   |   8   |   9   |  10   | Valet | Dame  |  Roi  |  As   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   0   |   0   |  14   |  10   |  20   |   3   |   4   |  11   |


???+ example "Calculs de points"
	
	On considère un paquet contenant les cartes suivantes : As de cœur (:hearts:), 7 :hearts:, 9 de trèfle (:clubs:), 9 de pique (:spades:), Valet :clubs:, Valet :hearts:, Dame de carreau (:diamonds:) et Roi :diamonds:.

	Si on choisit carreau comme atout, le décompte des points est le suivant :
	
	| As :hearts: | 7 :hearts: | 9 :clubs: | 9 :spades: | Valet :clubs: | Valet :hearts: | Dame :diamonds: | Roi :diamonds: | **Total** |
	| :---------: | :--------: | :-------: | :--------: | :-----------: | :------------: | :-------------: | :------------: | :-------: |
	|     11      |     0      |     0     |     0      |       2       |       2        |        3        |       4        |  **22**   |

	Si on choisit trèfle comme atout on obtient par contre :
	
	| As :hearts: | 7 :hearts: | 9 :clubs: | 9 :spades: | Valet :clubs: | Valet :hearts: | Dame :diamonds: | Roi :diamonds: | **Total** |
	| :---------: | :--------: | :-------: | :--------: | :-----------: | :------------: | :-------------: | :------------: | :-------: |
	|     11      |     0      |    14     |     0      |      20       |       2        |        3        |       4        |  **54**   |

Les points des cartes sont stockés dans deux dictionnaires `VALEURS` et `VALEURS_ATOUT`. Le dictionnaire `VALEURS_ATOUT` est à compléter.

Écrire la fonction `calculer_points` qui :

* prend en paramètres un objet `jeu` de la classe `Paquet` décrivant un jeu de cartes ainsi qu'une chaîne de caractères `atout` indiquant la couleur d'atout
* et renvoie le total de points des cartes dans ce jeu.


On garantit que la variable `jeu` contient des cartes bien formées (couleur et hauteur valides) et que la valeur de `atout` fait partie de `('pique', 'cœur', 'carreau', 'trèfle')`.

???+ example "Exemples"

	```python title=""
	>>> jeu = Paquet()
	>>> jeu.ajoute_carte(Carte(1, 2))
	>>> jeu.ajoute_carte(Carte(7, 2))
	>>> jeu.ajoute_carte(Carte(9, 4))
	>>> jeu.ajoute_carte(Carte(9, 1))
	>>> jeu.ajoute_carte(Carte(11, 4))
	>>> jeu.ajoute_carte(Carte(11, 2))
	>>> jeu.ajoute_carte(Carte(12, 3))
	>>> jeu.ajoute_carte(Carte(13, 3))
	>>> calculer_point(jeu, "carreau")
	22
	>>> calculer_point(jeu, "trèfle")
	54
	```
	
{{ IDE('exo') }}
