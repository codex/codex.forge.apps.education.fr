# --------- PYODIDE:env --------- #
HAUTEURS = {
    1: "As",
    7: "7",
    8: "8",
    9: "9",
    10: "10",
    11: "Valet",
    12: "Dame",
    13: "Roi",
}

COULEURS = {1 : "pique", 2 : "cœur", 3: "carreau", 4 : "trèfle"}

class Carte:


    def __init__(self, h, c):
        """
        Initialise les attributs hauteur et couleur,
        Une hauteur a une valeur allant de 1 à 13,
        """
        self.hauteur = h
        self.couleur = c

    def hauteur_carte(self):
        """Renvoie la hauteur de la carte"""
        return HAUTEURS[self.hauteur]

    def couleur_carte(self):
        """Renvoie la couleur de la carte"""
        return COULEURS[self.couleur]

    def __str__(self):
        """Renvoie la chaîne décrivant la carte"""
        return f"{self.hauteur_carte()} de {self.couleur_carte()}"

    def __repr__(self):
        """Renvoie la chaîne décrivant la carte"""
        return str(self)


class Paquet:
    def __init__(self):
        """Initialise l'attribut contenu avec une liste vide"""
        self.contenu = []

    def taille(self):
        """Renvoie le nombre de carte contenu dans le paquet"""
        return len(self.contenu)

    def ajoute_carte(self, carte):
        """Renvoie le nombre de carte contenu dans le paquet"""
        self.contenu.append(carte)

    def carte_pos(self, pos):
        """Renvoie la carte qui se trouve à la position pos dans le paquet
        la paramètre pos a une valeur comprise entre 0 et 31."""
        assert 0 <= pos < 32, "La valeur de pos n'est pas valide"
        return self.contenu[pos]

    def __str__(self):
        """Renvoie la chaîne décrivant le paquet"""
        return f"{[c for c in self.contenu]}"


# --------- PYODIDE:code --------- #
VALEURS = {"As": 11, "7": 0, "8": 0, "9": 0, "10": 10, "Valet": 2, "Dame": 3, "Roi": 4}
VALEURS_ATOUT = {...}


def calculer_point(jeu, atout):
    ...


# --------- PYODIDE:corr --------- #
VALEURS = {"As": 11, "7": 0, "8": 0, "9": 0, "10": 10, "Valet": 2, "Dame": 3, "Roi": 4}
VALEURS_ATOUT = {
    "As": 11,
    "7": 0,
    "8": 0,
    "9": 14,
    "10": 10,
    "Valet": 20,
    "Dame": 3,
    "Roi": 4,
}


def calculer_point(jeu, atout):
    points = 0
    for i in range(jeu.taille()):
        carte = jeu.carte_pos(i)
        if carte.couleur_carte() == atout:
            points = points + VALEURS_ATOUT[carte.hauteur_carte()]
        else:
            points = points + VALEURS[carte.hauteur_carte()]
    return points


# --------- PYODIDE:tests --------- #
jeu = Paquet()
jeu.ajoute_carte(Carte(1, 2))
jeu.ajoute_carte(Carte(7, 2))
jeu.ajoute_carte(Carte(9, 4))
jeu.ajoute_carte(Carte(9, 1))
jeu.ajoute_carte(Carte(11, 4))
jeu.ajoute_carte(Carte(11, 2))
jeu.ajoute_carte(Carte(12, 3))
jeu.ajoute_carte(Carte(13, 3))

assert calculer_point(jeu, "carreau") == 22
assert calculer_point(jeu, "trèfle") == 54
# --------- PYODIDE:secrets --------- #
from random import sample, randrange

jeu = []


def _calculer_point_(jeu, atout):
    points = 0
    for i in range(jeu.taille()):
        carte = jeu.carte_pos(i)
        if carte.couleur_carte() == atout:
            points = points + VALEURS_ATOUT[carte.hauteur_carte()]
        else:
            points = points + VALEURS[carte.hauteur_carte()]
    return points


for couleur in (1, 2, 3, 4):
    for hauteur in [1, 7, 8, 9, 10, 11, 12, 13]:
        jeu.append(Carte(hauteur, couleur))
paquet = Paquet()
taille = randrange(5, 33)
for c in sample(jeu, taille):
    paquet.ajoute_carte(c)

for atout in ("pique", "cœur", "carreau", "trèfle"):
    attendu = _calculer_point_(paquet, atout)
    assert (
        calculer_point(paquet, atout) == attendu
    ), f"Erreur avec le {paquet = } et l'{atout = }"
