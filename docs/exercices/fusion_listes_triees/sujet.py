# --- vide --- #
def fusion(liste_a, liste_b):
    ...

# --- exo --- #
def fusion(liste_a, liste_b):
    taille_a = len(...)
    taille_b = ...
    liste_triee = ...
    i_a = ...
    i_b = ...
    while (i_a < ...) and (...):
        if liste_a[i_a] < ...:
            liste_triee.append(...)
            i_a = ...
        else:
            ...
            ...
    while i_a < ...:
        liste_triee.append(...)
        ...
    while ...:
        ...
        ...
    return ...

# --- corr --- #
def fusion(liste_a, liste_b):
    taille_a = len(liste_a)
    taille_b = len(liste_b)
    liste_triee = []
    i_a = 0
    i_b = 0
    while (i_a < taille_a) and (i_b < taille_b):
        if liste_a[i_a] < liste_b[i_b]:
            liste_triee.append(liste_a[i_a])
            i_a += 1
        else:
            liste_triee.append(liste_b[i_b])
            i_b += 1
    while i_a < taille_a:
        liste_triee.append(liste_a[i_a])
        i_a += 1
    while i_b < taille_b:
        liste_triee.append(liste_b[i_b])
        i_b += 1
    return liste_triee

# --- tests --- #
assert fusion([1, 6, 10], [0, 7, 8, 9]) == [0, 1, 6, 7, 8, 9, 10]

assert fusion([1, 6, 10], []) == [1, 6, 10]

assert fusion([], [0, 7, 8, 9]) == [0, 7, 8, 9]

# --- secrets --- #
assert fusion([3, 5], [2, 5]) == [2, 3, 5, 5]
assert fusion([-2, 4], [-3, 5, 10]) == [-3, -2, 4, 5, 10]
assert fusion([4], [2, 6]) == [2, 4, 6]

assert fusion([10, 10], [1, 1, 1, 1]) == [1, 1, 1, 1, 10, 10]
assert fusion([1, 2, 3, 6, 7, 8], [4, 5, 11, 11]) == [1, 2, 3, 4, 5, 6, 7, 8, 11, 11]
assert fusion([1, 2, 3, 6, 7, 8], [10, 11, 15, 15, 20]) == [
    1,
    2,
    3,
    6,
    7,
    8,
    10,
    11,
    15,
    15,
    20,
]
assert fusion([1, 3, 5, 7], [2, 4, 6, 8]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([5, 6, 15, 16, 25, 26], [1, 2, 10, 11, 20, 21]) == [
    1,
    2,
    5,
    6,
    10,
    11,
    15,
    16,
    20,
    21,
    25,
    26,
]

assert fusion([8, 9, 15, 987, 17613], []) == [8, 9, 15, 987, 17613]
assert fusion([], [8, 9, 12, 987, 17614]) == [8, 9, 12, 987, 17614]
assert fusion([1, 2], [3, 4, 5, 6, 7, 8]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([3, 4, 5, 6, 7, 8], [1, 2]) == [1, 2, 3, 4, 5, 6, 7, 8]
assert fusion([164646, 2321319], [3, 4, 5, 6, 7, 8]) == [3, 4, 5, 6, 7, 8, 164646, 2321319]
assert fusion([3, 4, 5, 6, 7, 8], [164646, 2321319]) == [3, 4, 5, 6, 7, 8, 164646, 2321319]

# --- rem --- #
''' # skip
👉 Dans la solution proposée, on sort de la boucle :
`#!py while i_a < len(liste_a) and i_b < len(liste_b):`
lorsque `#!py i_a > len(liste_a)` ou `#!py  i_b < len(liste_b)`. C'est-à-dire lorsqu'on a pris toutes les valeurs d'une des deux listes. L'autre liste n'a peut-être pas encore été entièrement parcourue.

Il y a donc au plus une des deux boucles `while` suivantes qui sera exécutée.


👉 Autre solution possible avec une seule boucle `for`:

```python
def fusion(liste_a, liste_b):
    n_a = len(liste_a)
    n_b = len(liste_b)
    i_a = 0
    i_b = 0
    liste_triee = []
    for i in range(n_a+n_b):
        if i_a < n_a and (i_b >= n_b or liste_a[i_a] <= liste_b[i_b]):
            liste_triee.append(liste_a[i_a])
            i_a += 1
        else:
            liste_triee.append(liste_b[i_b])
            i_b += 1
    return liste_triee
```
On rajoute la valeur venant de `liste_a` si :

-   Elle n'a pas été totalement parcourue (`#!py i_a < n_a`)
-   et si une des deux conditions est vérifiée :

    -   la liste `liste_b` a été totalement parcourue (`#!py i_b >= n_b`)
    -   la valeur courante de `liste_a` est inférieure à celle de
        `liste_b` (`#!py liste_a[i_a] <= liste_b[i_b]`)

Dans tous les autres cas, il faut prendre la valeur courante de `liste_b`.
''' # skip
