On utilise une recherche dichotomique itérative.

Il est aussi possible de procéder de manière récursive en introduisant des paramètres :

* `debut` désigne l'indice inclus du début de la zone de recherche dans le *log*,

* `fin` désigne l'indice exclu de la fin de la zone de recherche dans le *log*.

Afin de ne pas surcharger le premier appel `indice_panne(log)`, on donne des valeurs par défaut à ces paramètres.

```python
def indice_panne(log, debut=None, fin=None):
    if debut is None:
        debut = 0
        fin = len(log)

    if not log[debut]:
        return debut

    milieu = (debut + fin) // 2
    if log[milieu - 1] and not log[milieu]:
        return milieu
    elif not log[milieu]:
        return indice_panne(log, debut, milieu)
    else:
        return indice_panne(log, milieu + 1, fin)
```

On notera que dans cette version l'indice de fin de la zone de recherche est exclu de celle-ci. La recherche dans la zone `#!py (debut, fin)` s'apparente alors au fonctionnement classique de la fonction `#!py range(debut, fin)` : la valeur `#!py debut` est incluse alors que `#!py fin` est exclue.
