

# --------- PYODIDE:code --------- #

def indice_panne(log):
    ...

# --------- PYODIDE:corr --------- #

def indice_panne(log):
    if not log[0]:
        return 0

    debut = 0
    fin = len(log) - 1

    while debut <= fin:
        milieu = (debut + fin) // 2
        if log[milieu - 1] and not log[milieu]:
            return milieu
        elif not log[milieu]:
            fin = milieu - 1
        else:
            debut = milieu + 1

# --------- PYODIDE:tests --------- #

assert indice_panne([True, False]) == 1
assert indice_panne([False, False, False]) == 0
assert indice_panne([True] * 10 + [False] * 100) == 10
assert indice_panne([True, True, False, False, False]) == 2

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from random import randrange


class Log(list):
    def __init__(self, longueur, premier_faux):
        assert longueur > 1_000, "La longueur doit être supérieure à 1000"
        self._longueur = longueur
        self._premier_faux = premier_faux
        self._compteur_lectures = 0
        self._lectures_max = 500

    def __len__(self):
        return self._longueur

    def __getitem__(self, i):
        if type(i) is slice:
            indices = range(*i.indices(self._longueur))
            return [self.__getitem__(k) for k in indices]
        self._compteur_lectures += 1
        if self._compteur_lectures > self._lectures_max:
            raise LookupError(
                f"Il faut réaliser strictement moins de {self._lectures_max} lectures dans le tableau"
            )
        if i < 0:
            i = self._longueur + i
        if not (0 <= i < self._longueur):
            raise IndexError("L'indice demandé est invalide")
        return True if i < self._premier_faux else False

    def __getitem_bis__(self, i):
        return True if i < self._premier_faux else False

    def __setitem__(self, i, x):
        raise NotImplementedError("Il est interdit de modifier les valeurs")

    def __str__(self):
        return f"[{self.__getitem_bis__(0)}, {self.__getitem_bis__(1)}, ..., {self.__getitem_bis__(self._longueur - 1)}]"

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        for i in range(self._longueur):
            yield self.__getitem__(i)


for test in range(20):
    attendu = randrange(0, 100)
    faux = randrange(1, 100)
    log = [True] * attendu + [False] * faux
    assert indice_panne(log) == attendu, f"Erreur avec {log}"

    exposant = randrange(6, 10)
    N = 10**exposant
    attendu = randrange(1, N)
    grand_log = Log(N, attendu)
    assert (
        indice_panne(grand_log) == attendu
    ), f"Erreur lors de la recherche dans un tableau de 10**{exposant} valeurs"