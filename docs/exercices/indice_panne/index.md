---
author: Nicolas Revéret
difficulty: 330
hide:
    - navigation
    - toc
title: Indice d'une panne
tags:
    - booléen
    - dichotomie
    - diviser pour régner
    - récursivité
---


Une sonde interroge à intervalles réguliers l'état de fonctionnement d'un système électronique. Celui-ci peut être en marche ou en panne.

La sonde est programmée pour enregistrer les résultats de ses requêtes dans un *log*. Il s'agit d'un tableau de booléens (une `#!py list` Python) dans lequel les valeurs `#!py True` précèdent les `#!py False`. La valeur `#!py True` indique que le système est en marche, `#!py False` qu'il est en panne.

Une panne nécessite une intervention humaine et ne peut donc pas disparaître seule : elle persiste jusqu'à la fin de l'enregistrement.

```python
#         0     1      2      3      4
log = [True, True, False, False, False]
```

Lors d'une vérification on constate que le système est en panne : le *log* contient au moins une valeur `#!py False` en dernière position. On se demande à quel moment a débuté cette panne.

Dans l'exemple précédent, le premier `#!py False` est à l'indice `#!py 2` : la panne a débuté à l'instant `#!py 2`.

Écrire la fonction `#!py indice_panne` qui prend en paramètre le tableau de booléens `#!py log` et renvoie l'instant du démarrage de la panne.

On garantit que le *log* n'est pas vide et que, au moment de la vérification, le système est en panne (la dernière valeur du tableau est `False`).

???+ danger "Attention"

    La panne du système a aussi corrompu le fichier de *log*. Vous ne pouvez pas lire plus de 500 valeurs dans celui-ci. Passé ce nombre de lectures, tout nouvel accès lèvera une erreur.

    Il est donc important de bien concevoir votre algorithme car les *logs* utilisés dans les tests secrets peuvent être très longs : un milliard de valeurs !

???+ example "Exemples"

    ```pycon title=""
    >>> indice_panne([True, False])
    1
    >>> indice_panne([False, False, False])
    0
    >>> indice_panne([True] * 10 + [False] * 100)
    10
    >>> indice_panne([True, True, False, False, False])
    2
    ```

{{ IDE('exo') }}

??? tip "Astuce (1)"

    Il s'agit d'une recherche dans un tableau trié : les valeurs `#!py True` sont au début, les `#!py False` à la fin.

??? tip "Astuce (2)"

    Si le *log* ne contient que des valeurs `#!py False`, il faut renvoyer `#!py 0`. 
    
    Dans le cas contraire, l'indice cherché est l'unique indice `#!py i` qui vérifie `#!py log[i - 1] and not log[i]`.
