# --------- PYODIDE:code --------- #
class Duree:
    def __init__(self, h, m, s):
        """Instanciation
        Les minutes et secondes doivent être compris entre 0 et 59
        """
        assert 0 <= m < 60 and 0 <= s < 60
        self.heures = h
        self.minutes = m
        self.secondes = s

    def en_secondes(self):
        """Conversion en secondes"""
        return self.heures * 3600 + self.minutes * 60 + self.secondes

    def ajoute_minutes(self, mins):
        """Ajout de mins minutes"""
        self.heures += (self.minutes + mins) // 60
        self.minutes = (self.minutes + mins) % 60

    def ajoute_secondes(self, secs):
        """Ajout de secs minutes"""
        self.ajoute_minutes((self.secondes + secs) // 60)
        self.secondes = (self.secondes + secs) % 60

    def __repr__(self):
        """Mise en forme pour l'affichage"""
        return f"{self.heures}:{self.minutes}:{self.secondes}"

    def __eq__(self, autre):
        """Test d'égalité"""
        return ...

    def __le__(self, autre):
        """Teste si cet objet Duree est plus court ou égal que l'objet Duree autre"""
        ...

    def __add__(self, autre):
        """Addition de cette durée et de autre"""
        ...

    def __sub__(self, autre):
        """Soustraction de autre à cette durée"""
        assert autre ... self
        ...


# --------- PYODIDE:corr --------- #
class Duree:
    def __init__(self, h, m, s):
        """Instanciation
        Les minutes et secondes doivent être compris entre 0 et 59
        """
        assert 0 <= m < 60 and 0 <= s < 60
        self.heures = h
        self.minutes = m
        self.secondes = s

    def en_secondes(self):
        """Conversion en secondes"""
        return self.heures * 3600 + self.minutes * 60 + self.secondes

    def ajoute_minutes(self, mins):
        """Ajout de mins minutes"""
        self.heures += (self.minutes + mins) // 60
        self.minutes = (self.minutes + mins) % 60

    def ajoute_secondes(self, secs):
        """Ajout de secs minutes"""
        self.ajoute_minutes((self.secondes + secs) // 60)
        self.secondes = (self.secondes + secs) % 60

    def __repr__(self):
        """Mise en forme pour l'affichage"""
        return f"{self.heures}:{self.minutes}:{self.secondes}"

    def __eq__(self, autre):
        """Test d'égalité"""
        return self.en_secondes() == autre.en_secondes()

    def __neq__(self, autre):
        """Test de différence"""
        return self.en_secondes() != autre.en_secondes()

    def __le__(self, autre):
        """Teste si cet objet Duree est plus court ou égal que l'objet Duree autre"""
        return self.en_secondes() <= autre.en_secondes()

    def __add__(self, autre):
        """Addition de cette durée et de autre"""
        cumul = Duree(0, 0, 0)
        cumul.ajoute_secondes(self.en_secondes() + autre.en_secondes())
        return cumul

    def __sub__(self, autre):
        """Soustraction de autre à cette durée"""
        assert autre <= self
        delta = Duree(0, 0, 0)
        delta.ajoute_secondes(self.en_secondes() - autre.en_secondes())

        return delta


# --------- PYODIDE:tests --------- #

duree_1 = Duree(2, 45, 52)
duree_2 = Duree(2, 45, 52)
duree_3 = Duree(5, 12, 26)
assert duree_1 == duree_2
assert not duree_1 == duree_3
assert duree_1 != duree_3
assert duree_1 <= duree_3
assert duree_1 + duree_3 == Duree(7, 58, 18)
assert duree_3 - duree_1 == Duree(2, 26, 34)


# --------- PYODIDE:secrets --------- #

duree_1 = Duree(2, 45, 52)
duree_2 = Duree(5, 51, 4)
assert (
    duree_1 == duree_2
) is False, f"Erreur en testant l'égalité de {duree_1} et {duree_2}"
assert (
    duree_1 != duree_2
) is True, f"Erreur en testant la différence de {duree_1} et {duree_2}"
assert duree_1 <= duree_2, f"Erreur en comparant {duree_1} et {duree_2}"
attendu = duree_2 + duree_1
assert duree_2 + duree_1 == attendu, f"Erreur en additionnant {duree_2} et {duree_1}"
attendu = Duree(3, 5, 12)
assert duree_2 - duree_1 == attendu, f"Erreur en soustrayant {duree_2} et {duree_1}"
