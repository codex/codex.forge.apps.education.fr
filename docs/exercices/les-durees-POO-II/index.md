---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Les durées en POO (2)
tags:
    - à trous
    - programmation orientée objet
    - supérieur
maj: 16/05/2024
difficulty : 310
---

On s'intéresse dans cet exercice à une classe `Duree` dont on fournit le code ci-dessous.

???+ info "La classe `Duree`"

    ```python
    class Duree:
        def __init__(self, h, m, s):
            """Instanciation
            Les minutes et secondes doivent être compris entre 0 et 59
            """
            assert 0 <= m < 60 and 0 <= s < 60
            self.heures = h
            self.minutes = m
            self.secondes = s

        def en_secondes(self):
            """Conversion en secondes"""
            return self.heures * 3600 + self.minutes * 60 + self.secondes

        def ajoute_minutes(self, mins):
            """Ajout de mins minutes"""
            self.heures += (self.minutes + mins) // 60
            self.minutes = (self.minutes + mins) % 60

        def ajoute_secondes(self, secs):
            """Ajout de secs minutes"""
            self.ajoute_minutes((self.secondes + secs) // 60)
            self.secondes = (self.secondes + secs) % 60

        def __repr__(self):
            """Mise en forme pour l'affichage"""
            return f"{self.heures}:{self.minutes}:{self.secondes}"
    ```

    La méthode `__repr__()` est une méthode *spéciale* qui n'est pas directement appelée, mais appelée automatiquement lorsque l'on souhaite afficher une *représentation* de l'objet.

    Dans le cas présent, cette méthode est appelée lorsque l'on souhaite convertir un objet de type `Duree` en une chaine de caractères (par exemple, lors de l'appel de la fonction `#!py print`). Cette méthode renvoie une chaine de caractères sous la forme "hh&#58;mm:ss". Par exemple, pour un objet initialisé avec les valeurs 3 h, 20 min et 2 s, la méthode renvoie `#!py '3:20:2'`

    On fournit ci-dessous des exemples d'utilisation :

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_1
    2:45:52
    >>> duree_1.en_secondes()
    9952
    >>> duree_1.ajoute_secondes(20)
    >>> duree_1.en_secondes()
    9972
    >>> duree_1.ajoute_minutes(30)
    >>> duree_1.en_secondes()
    11772
    ```

On souhaite dans cet exercice ajouter des méthodes *spéciales*. Ces différentes fonctions sont exécutées lors de l'utilisation de l'opérateur correspondant (voir chaque exemple).

??? question "`__eq__`"

    La méthode `__eq__` (pour ***eq****ual*) permet de tester l'égalité de deux objets de type `Duree`.

    Cette méthode renvoie `True` si ces objets représentent la même durée, `False` dans le cas contraire.

    Elle est automatiquement appelée lors de l'utilisation de l'opérateur `==`.

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_2 = Duree(2, 45, 52)
    >>> duree_3 = Duree(5, 12, 26)
    >>> duree_1 == duree_2
    True
    >>> duree_1 == duree_3
    False
    ```

??? question "`__neq__`"

    La méthode `__neq__` (pour ***n****ot* ***eq****ual*) permet de tester la différence de deux objets de type `Duree`.

    Cette méthode renvoie `True` si ces objets ne représentent pas la même durée, `False` dans le cas contraire.

    Elle est automatiquement appelée lors de l'utilisation de l'opérateur `!=`.

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_2 = Duree(2, 45, 52)
    >>> duree_3 = Duree(5, 12, 26)
    >>> duree_1 != duree_2
    False
    >>> duree_1 != duree_3
    True
    ```

??? question "`__le__`"

    La méthode `__le__` (pour ***l****ess than or* ***e****qual*) permet de comparer deux objets de type `Duree`.

    Elle renvoie `True` si la durée représentée par le premier objet est inférieure ou égale à celle représentée par le second.

    Elle est automatiquement appelée lors de l'utilisation de l'opérateur `<=`.

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_3 = Duree(5, 12, 26)
    >>> duree_1 <= duree_3
    True
    >>> duree_3 <= duree_1
    False
    ```

??? question "`__add__`"

    La méthode `__add__` permet d'additionner les durées représentées par deux objets de type `Duree`.

    Elle renvoie un nouvel objet de type `Duree` sans modifier les deux objets concernés par l'opération.

    Elle est automatiquement appelée lors de l'utilisation de l'opérateur `+`.

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_3 = Duree(5, 12, 26)
    >>> cumul = duree_1 + duree_3
    >>> cumul
    7:58:18
    >>> duree_1
    2:45:52
    ```

??? question "`__sub__`"

    La méthode `__sub__` permet de soustraire les durées représentées par deux objets de type `Duree`.

    Elle renvoie un nouvel objet de type `Duree` sans modifier les deux objets concernés par l'opération. **Attention** : avant d'effectuer l'opération, on vérifie que la durée à soustraire est inférieure ou égale à l'autre durée.

    Elle est automatiquement appelée lors de l'utilisation de l'opérateur `-`.

    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_3 = Duree(5, 12, 26)
    >>> # duree_1 - duree_3 est impossible
    >>> delta = duree_3 - duree_1
    >>> delta
    2:26:34
    >>> duree_1
    2:45:52
    ```

Compléter le code suivant.

{{ IDE('exo') }}