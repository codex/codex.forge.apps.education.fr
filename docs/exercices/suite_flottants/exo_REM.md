On peut observer que la suite est constante : tous les termes valent $\cfrac{1}{3}$ .

Le calcul avec les flottants est un calcul approché. Les arrondis peuvent dans certains cas s'accumuler.

Le problème avec le calcul de $4u_n-1$ est que  `#!py 4 * 1/3 - 1` n'est pas égal en machine à `#!py 1/3`:

```pycon
>>> 4 * 1/3 - 1
0.33333333333333326
>>> 1/3
0.3333333333333333
```

Donc, supposons qu'on utilise la forme `#!py u = 4*u - 1` pour coder la fonction:

```python
def termes(n):
    u = 1 / 3
    liste = [u]
    for i in range(1, n + 1):
        u = 4*u - 1
        liste.append(u)
    return liste
```

On demande la liste des termes de rang `0` à `50` inclus avec `#!py termes_0_50 = termes(50)`.
Avec ce code, on obtient par exemple $0$ pour `#!py termes_0_50[27]`, alors que $u_{27}$ vaut $1/3$ comme tous les termes de la suite. Et pour les termes suivants on obtient $-1, -5, -21, ...$. Les erreurs s'accumulent.

Considérons la forme `#!py u = 3 * (4/3 * u - 1/3)`. On peut vérifier que `3 * ((4/3)*(1/3) - 1/3)` vaut bien `1/3`.

On pourrait donc écrire la solution ainsi:

```python
def termes(n):
    u = 1 / 3
    liste = [u]
    for i in range(1, n + 1):
        u = 3 * ((4 / 3) * u - 1 / 3)
        liste.append(u)
    return liste
```
