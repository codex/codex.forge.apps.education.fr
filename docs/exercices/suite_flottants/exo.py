# --------- PYODIDE:code --------- #

def termes(n):
    ...

# --------- PYODIDE:corr --------- #

def termes(n):
    return [1/3] * (n+1)

# --------- PYODIDE:tests --------- #

termes_0_10 = termes(10)
for i in range(11):
    assert 0.33333333 <= termes_0_10[i] < 0.33333334, "Erreur pour le terme de rang "+str(i)

# --------- PYODIDE:secrets --------- #


# tests secrets
termes_0_50 = termes(50)
for i in range(10, 51):
    assert 0.33333333 <= termes_0_50[i] < 0.33333334, "Erreur pour le terme de rang "+str(i)
