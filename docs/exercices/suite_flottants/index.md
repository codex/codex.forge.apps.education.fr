---
author: Serge Bays
hide:
    - navigation
    - toc
title: Suite et flottants
tags:
    - maths
    - float
difficulty: 150
maj: 04/04/2024
---



## Calcul des termes d'une suite

On considère la suite définie par récurrence par:

$$
\left\{
	\begin{array}{ll}
		u_0 = \cfrac{1}{3} \\
		u_{n+1} = 4 u_{n} - 1 \hspace{3ex} \text{pour tout entier naturel }n \\
	\end{array}
\right.
$$

Écrire une fonction `termes` qui prend en paramètre un entier `n` positif ou nul et renvoie la liste des termes de rang `0` à `n` inclus.

??? tip "Remarque"

    L'expression de $u_{n+1}$ en fonction de $u_n$ peut s'écrire de différentes manières.
    Par exemple: $u_{n+1} = 4 \times u_{n} - 1$, ou $u_{n+1} = 4 \times \left(u_{n} - \cfrac{1}{4}\right)$, ou $u_{n+1} = 3 \times \left(\cfrac{4}{3} u_{n} - \cfrac{1}{3}\right)$, etc.

??? tip "Aide"

    Le calcul avec les flottants est un calcul approché. Suivant l'expression utilisée, le résultat peut être différent.
    

{{ remarque('assertion') }}
   
{{ IDE('exo') }}
