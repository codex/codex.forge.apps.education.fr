---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Jeu de parcours
tags:
    - à trous
    - booléen
difficulty: 210
maj: 03/06/2024
---


Dans un jeu de hasard, on se déplace sur une grille de case en case, certaines cases peuvent faire avancer le pion quand d'autres le font reculer.

Le parcours est de taille variable dont la sortie (située en dehors du parcours) est repérée par le nombre de cases du parcours. 

Sur chaque case se trouve le numéro de la prochaine case où doit se déplacer le pion.

!!! example "Exemples de parcours"
    Ce premier parcours possède $6$ cases. Le chiffre $6$ indique donc la sortie du parcours.

    ![Alt text](oie.drawio.svg)

    Ce deuxième parcours possède $8$ cases. Le chiffre $8$ indique donc la sortie du parcours.
    
    ![Alt text](oie4.svg)


Le pion se trouve sur la case départ et on jette une seule fois un dé à six face. On déplace alors le pion sur le parcours d'autant de cases que la valeur indiquée par le dé. On arrive alors sur une case dont la valeur est celle de la prochaine case à visiter. On avance ainsi, par rebonds successifs, de case en case.

Durant le parcours, il peut y avoir trois situations possibles :

* On arrive sur la case d'arrivée : la partie est gagnée ;
* On arrive sur une case pas encore visitée : la partie continue ;
* On arrive sur une case déjà visitée : la partie est perdue.


!!! example "Exemples de déplacements"
    Dans cet exemple, la case d'arrivée est désignée par la valeur $6$, qui est le nombre de cases du parcours.

    Si le lancer de dé donne $4$, alors on déplace le pion de $4$ cases et il atterrit sur la case $3$. Le pion suit les cases suivantes : $4$ - $1$ - $6$ et la partie est alors gagnée.

    ![Alt text](oie2.drawio.svg)

    Si le lancer de dé donne $1$, alors on déplace le pion de $1$ case et il atterrit sur la case $0$. Le pion va sur la case suivante $2$ puis revient sur la case $0$ et la partie est alors perdue.

    ![Alt text](oie3.drawio.svg)

    Ainsi, les lancers $1$, $3$ et $6$ sont perdants, alors que les lancers $2$, $4$ et $5$ sont gagnants.

Le parcours de jeu est représenté sous la forme d'un tableau d'entiers `jeu_de_parcours`: si le pion est positionné sur la case d'indice `i` de la grille, 
`#!py jeu_de_parcours[i]` contient l'indice de la case suivante sur laquelle le pion doit se déplacer. On certifie que cet indice est valide, c'est à dire 
compris entre $0$ et la longueur de la grille.

```python title=""
jeu_de_parcours = [2, 6, 0, 4, 1, 0]
```

Pour détecter que l'on tourne en rond indéfiniment, on va cocher chaque fois que l'on passe sur une case. 

Écrire la fonction `suivre_parcours` qui prend en paramètre le tableau `parcours` qui représente le parcours et l'entier `dé`, la valeur du dé. Cette fonction renvoie le booléen `True` si la partie est gagnée, `False` sinon.

On pourra constater que la partie est gagnée si elle se termine sans que le pion n'ait jamais parcouru de boucle.


??? tip "Indice"

    On pourra créer un tableau `deja_vu` (de booléens) de même taille que le parcours et initialement rempli de `False`. Chaque fois que le pion passe sur une case du parcours, on affecte `True` à l'élément de `deja_vu` d'indice correspondant.

{{ IDE('exo_a') }}

On souhaite maintenant déterminer, pour un parcours donné,la liste des lancers de dé qui seraient gagnants.

En utilisant la fonction `suivre_parcours`, écrire la fonction `liste_gagnants` qui prend en paramètre une liste représentant le parcours. Cette fonction renvoie une liste d'entiers contenant les lancers de dé pour lesquels la partie est gagnée.

{{ IDE('exo_b') }}
