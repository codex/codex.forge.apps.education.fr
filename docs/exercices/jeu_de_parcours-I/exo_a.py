# --------- PYODIDE:env --------- #

def suivre_parcours(parcours, dé):
    pass

# --------- PYODIDE:code --------- #

def suivre_parcours(parcours, dé):
    numero_case = dé - 1
    nombre_cases = ...
    # création d'une structure deja_vu
    ...
    while numero_case != nombre_cases:
        # On vérifie si le pion n'est pas déjà passé sur la case
        if ... :
            # On coche que l'on est passé sur la case
            ...
            # On récupère l'indice de la prochaine case
            ...
        else:
            return False

    return True

# --------- PYODIDE:corr --------- #

def suivre_parcours(parcours, dé):
    numero_case = dé - 1
    arrivée = len(parcours)
    nombre_cases = len(parcours)
    # création du tableau deja_vu
    deja_vu = [False for _ in range(nombre_cases)]

    while numero_case != nombre_cases :
        # On vérifie si le pion est déjà passé sur la case
        if deja_vu[numero_case] != True:
            # On coche que l'on est passé sur la case
            deja_vu[numero_case] = True
            # On récupère l'indice de la prochaine case
            numero_case = parcours[numero_case]
        else:
            return False

    return True

# --------- PYODIDE:tests --------- #

jeu_de_parcours = [2, 6, 0, 4, 1, 0]

assert suivre_parcours(jeu_de_parcours, 1) is False
assert suivre_parcours(jeu_de_parcours, 2) is True
assert suivre_parcours(jeu_de_parcours, 3) is False
assert suivre_parcours(jeu_de_parcours, 4) is True
assert suivre_parcours(jeu_de_parcours, 5) is True
assert suivre_parcours(jeu_de_parcours, 6) is False

jeu_de_parcours = [1, 6, 7, 4, 8, 0, 5, 3]

assert suivre_parcours(jeu_de_parcours, 1) is False
assert suivre_parcours(jeu_de_parcours, 2) is False
assert suivre_parcours(jeu_de_parcours, 3) is True
assert suivre_parcours(jeu_de_parcours, 4) is True
assert suivre_parcours(jeu_de_parcours, 5) is True
assert suivre_parcours(jeu_de_parcours, 6) is False

# --------- PYODIDE:secrets --------- #

jeu_de_parcours = [1, 8, 10, 13, 18, 15, 20, 16, 14, 1, 20,
              15, 21, 11, 17, 19, 4, 12, 7, 16, 9]

assert suivre_parcours(jeu_de_parcours, 1) is True
assert suivre_parcours(jeu_de_parcours, 2) is True
assert suivre_parcours(jeu_de_parcours, 3) is True
assert suivre_parcours(jeu_de_parcours, 4) is False
assert suivre_parcours(jeu_de_parcours, 5) is False
assert suivre_parcours(jeu_de_parcours, 6) is False

jeu_de_parcours_secret = [1, 2, 3, 4, 5, 6, 7]
assert suivre_parcours(jeu_de_parcours_secret, 1) is True
assert suivre_parcours(jeu_de_parcours_secret, 2) is True
assert suivre_parcours(jeu_de_parcours_secret, 3) is True
assert suivre_parcours(jeu_de_parcours_secret, 4) is True
assert suivre_parcours(jeu_de_parcours_secret, 5) is True
assert suivre_parcours(jeu_de_parcours_secret, 6) is True

jeu_de_parcours_secret = [0, 1, 2, 3, 4, 5, 6]
assert suivre_parcours(jeu_de_parcours_secret, 1) is False
assert suivre_parcours(jeu_de_parcours_secret, 2) is False
assert suivre_parcours(jeu_de_parcours_secret, 3) is False
assert suivre_parcours(jeu_de_parcours_secret, 4) is False
assert suivre_parcours(jeu_de_parcours_secret, 5) is False
assert suivre_parcours(jeu_de_parcours_secret, 6) is False

jeu_de_parcours_secret = [9 for _ in range (9)]
assert suivre_parcours(jeu_de_parcours_secret, 1) is True
assert suivre_parcours(jeu_de_parcours_secret, 2) is True
assert suivre_parcours(jeu_de_parcours_secret, 3) is True
assert suivre_parcours(jeu_de_parcours_secret, 4) is True
assert suivre_parcours(jeu_de_parcours_secret, 5) is True
assert suivre_parcours(jeu_de_parcours_secret, 6) is True