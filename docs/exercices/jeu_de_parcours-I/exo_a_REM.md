Pour initialiser le tableau, on aurait pu utiliser la **méthode par concaténation** :

```python
deja_vu = [False] * len(parcours)
```

Par contre, il est important de privilégier la **méthode par construction** : 

```python
deja_vu = [False for _ in range(nombre_cases)]
```

* La méthode par concaténation recopie n fois le même élément.
* La méthode par construction crée n éléments distincts.

Pour un tableau de booléens (comme dans cet exercice), cela ne fait pas de différence.

Par contre, si le tableau contient des objets mutables (comme un tableau de tableaux), la méthode par concaténation est source de nombreuses erreurs.
