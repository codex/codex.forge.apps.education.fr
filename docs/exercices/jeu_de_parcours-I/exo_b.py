# --------- PYODIDE:env --------- #

def suivre_parcours(parcours, dé):
    numero_case = dé - 1
    arrivée = len(parcours)
    nombre_cases = len(parcours)
    # création du tableau deja_vu
    deja_vu = [False for _ in range(nombre_cases)]

    while numero_case != nombre_cases :
        #On vérifie si le pion est déjà passé sur la case
        if deja_vu[numero_case] != True:
            #On coche que l'on est passé sur la case
            deja_vu[numero_case] = True
            #On récupère l'indice de la prochaine case
            numero_case = parcours[numero_case]
        else:
            return False

    return True

# --------- PYODIDE:code --------- #

def liste_gagnants(parcours):
    ...

# --------- PYODIDE:corr --------- #

def liste_gagnants(parcours):
    return [i for i in range(1, 7) if suivre_parcours(parcours, i)]

# --------- PYODIDE:tests --------- #

jeu_de_parcours = [2, 6, 0, 4, 1, 0]

assert liste_gagnants(jeu_de_parcours) == [2, 4, 5]

# --------- PYODIDE:secrets --------- #

jeu_de_parcours = [1, 8, 10, 13, 18, 15, 20, 16, 14, 1, 20,
              15, 21, 11, 17, 19, 4, 12, 7, 16, 9]

assert liste_gagnants(jeu_de_parcours) == [1, 2, 3]

jeu_de_parcours_secret = [1, 2, 3, 4, 5, 6, 7]
assert liste_gagnants(jeu_de_parcours_secret) == [1, 2, 3, 4, 5, 6]


jeu_de_parcours_secret = [0, 1, 2, 3, 4, 5, 6]
assert liste_gagnants(jeu_de_parcours_secret) == []

jeu_de_parcours_secret = [9 for _ in range (9)]
assert liste_gagnants(jeu_de_parcours_secret) == [1, 2, 3, 4, 5, 6]