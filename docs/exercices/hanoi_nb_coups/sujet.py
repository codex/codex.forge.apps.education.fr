# --- hdr, recurrente_--- #
def nb_coups(n):
    ...


# --- vide, recurrente_--- #
def nb_coups(n):
    ...


# --- exo, recurrente_ --- #
def nb_coups(n):
    u = ...
    for i in range(1, n):
        u = ...
    return ...


# --- corr, recurrente_ --- #
def nb_coups(n):
    u = 1
    for i in range(1, n):
        u = 2 * u + 1
    return u


# --- hdr, explicite --- #
def nb_coups(n):
    ...


# --- exo, explicite --- #
def nb_coups(n):
    ...


# --- corr, explicite --- #
def nb_coups(n):
    return 2**n - 1


# --- tests, explicite, recurrente_ --- #
assert nb_coups(1) == 1
assert nb_coups(2) == 3
assert nb_coups(3) == 7
assert nb_coups(10) == 1023
# --- secrets, recurrente_ --- #
f = lambda n: 2**n - 1
for n in range(1, 20):
    attendu = f(n)
    assert nb_coups(n) == attendu, f"Erreur avec {n = }"
# --- secrets, explicite --- #
f = lambda n: 2**n - 1
for n in range(1, 20):
    attendu = f(n)
    assert nb_coups(n) == attendu, f"Erreur avec {n = }"

from random import randrange
n = randrange(10**6, 10**8)
attendu = f(n)
assert nb_coups(n) == attendu, f"Erreur avec {n = }"