On a en effet, pour tout $n > 0$ :

$$u_n=2^n-1$$

Attention toutefois, une vérification dans **certains cas particuliers** ne permet pas de prouver que cette formule est vraie pour **toutes les valeurs de $n$ strictement positives**.

Une preuve de ce résultat fait intervenir la suite annexe $(v_n)$ définie pour tout entier $n>0$ par :

$$v_n = u_n + 1$$

On peut montrer que cette suite annexe est géométrique de premier terme $v_1=2$ et de raison $q=2$.

On en déduit que, pour tout $n > 0$ :

$$v_n = 2^n$$

et donc :

$$
\begin{align*}
u_n &= v_n-1\\
&=2^n-1
\end{align*}
$$