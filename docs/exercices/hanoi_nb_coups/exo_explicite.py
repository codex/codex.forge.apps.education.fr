# --------- PYODIDE:env --------- #
def nb_coups(n):
    ...


# --------- PYODIDE:code --------- #
def nb_coups(n):
    ...


# --------- PYODIDE:corr --------- #
def nb_coups(n):
    return 2**n - 1


# --------- PYODIDE:tests --------- #
assert nb_coups(1) == 1
assert nb_coups(2) == 3
assert nb_coups(3) == 7
assert nb_coups(10) == 1023
# --------- PYODIDE:secrets --------- #
f = lambda n: 2**n - 1
for n in range(1, 20):
    attendu = f(n)
    assert nb_coups(n) == attendu, f"Erreur avec {n = }"

from random import randrange
n = randrange(10**6, 10**8)
attendu = f(n)
assert nb_coups(n) == attendu, f"Erreur avec {n = }"