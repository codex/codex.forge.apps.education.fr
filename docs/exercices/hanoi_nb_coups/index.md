---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Nombre de coups de la tour de Hanoï
tags:
    - maths
    - à trous
difficulty: 90
maj: 03/11/2024
---

La tour de Hanoï est un jeu inventé par le mathématicien français [Édouard Lucas](https://fr.wikipedia.org/wiki/%C3%89douard_Lucas){target="_blank"}.

Les règles sont simples :

* le jeu est organisé autour de trois bâtons de bois que l'on nommera $a$, $b$ et $c$ ;
* sur ces bâtons on glisse des pièces circulaires de diamètres différents. Le nombre de pièces est appelé $n$ ;
* les pièces doivent toujours être empilées dans un ordre décroissant : il est interdit de poser une grande pièce sur une petite ;
* à chaque étape on déplace une pièce d'un bâton à l'autre ;
* initialement, les pièces sont sur le bâton $a$. Le but du jeu est de les amener sur le bâton $c$.

![Tour de Hanoï](Tower_of_Hanoi.jpg){ .center width=30%}

Un stratégie de résolution optimale, qui résout le problème en un nombre *minimal* de déplacements, repose sur l'observation suivante. Transporter $n$ pièces du bâton $a$ vers le $c$ nécessite de :

* transporter les $n - 1$ pièces du dessus du bâton $a$ vers le $b$ ;
* déplacer la plus grande pièce du bâton $a$ vers le $c$ ;
* transporter les $n - 1$ pièces du bâton $b$ vers le $c$.

L'illustration ci-dessous[^1] illustre la résolution du problème dans le cas $n = 4$ :

[^1]: Les illustrations de cette page proviennent d'<a href="https://commons.wikimedia.org/w/index.php?curid=228623" target="_blank">ici</a> et <a href="https://commons.wikimedia.org/w/index.php?curid=85401" target="_blank">là</a>.

![Animation](Tower_of_Hanoi_4.gif){.center}

On cherche dans cet exercice à calculer le nombre de coups minimal permettant de résoudre le problème à $n$ pièces. On note appelle $(u_n)$ la suite définie pour $n > 0$ et donnant le nombre de coups nécessaires à l'exécution de la stratégie proposée dans le cas de $n$ pièces.

On a donc :

$$
\begin{align*}
    u_1 &= 1&\\
    \\
    u_2 &= 1 + 1 + 1 \\
        &= 3\\
    \\
    u_3 &= 3 + 1 + 3 \\
        &= 7\\
    \\
    u_4 &= 7 + 1 + 7 \\
        &= 15
\end{align*}
$$

{{ remarque('assertion') }}


??? question "Suite définie par récurrence"

    De façon générale, on montre que :

    $$
    \begin{align*}
    & u_1=1\\
    \text{Pour tout }n>0,\qquad & u_{n+1} = 2u_n+1
    \end{align*}
    $$
    
    Écrire la fonction `#!py nb_coups` qui prend en paramètre l'entier `#!py n` représentant le nombre de pièces à déplacer (`#!py n > 0`) et renvoie le nombre de coups permettant de résoudre le problème en appliquant la stratégie décrite.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> nb_coups(1)
        1
        >>> nb_coups(2)
        3
        >>> nb_coups(3)
        7
        >>> nb_coups(10)
        1023
        ```
    
    === "Version vide"
        {{ IDE('exo_recurrente_vide')}}
    === "Version à compléter"
        {{ IDE('exo_recurrente_trous')}}

??? question "Suite définie explicitement"

    On donne ci-dessous les premières valeurs de $u_n$ :

    $$u_1 = 1 \qquad u_2 = 3 \qquad u_3 = 7 \qquad u_4 = 15 \qquad...\qquad u_{10} = 1\,023 \qquad...\qquad u_{20} = 1\,048\,575 \qquad$$
    
    On peut montrer qu'il existe une formule permettant de calculer directement $u_n$ à l'aide de la seule valeur de $n$ et ce, **sans utiliser par la formule de récurrence** de la question précédente.
    
    On ne fournit pas cette formule explicite, **il est demandé de la conjecturer**.
    
    Écrire la fonction `#!py nb_coups` qui prend en paramètre l'entier `#!py n` représentant le nombre de pièces à déplacer (`#!py n > 0`) et renvoie le nombre de coups permettant de résoudre le problème en appliquant la stratégie décrite.
    
    ??? warning "Grandes valeurs de $n$"
    
        La fonction demandée doit calculer explicitement la valeur cherchée sans utiliser de boucle ni de formule de récurrence.
        
        Les tests secrets portent sur de très grandes valeurs de $n$. Ainsi, un algorithme structuré autour d'une boucle aura pour conséquence de **bloquer le navigateur**.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> nb_coups(1)
        1
        >>> nb_coups(2)
        3
        >>> nb_coups(3)
        7
        >>> nb_coups(10)
        1023
        ```
    
    {{ IDE('exo_explicite')}}