

# --------- PYODIDE:code --------- #

R = [None, 1]
S = [None, 2]
...

# --------- PYODIDE:corr --------- #

R = [None, 1, 3]
S = [None, 2, 4, 5, 6]
i_r = 2  # l'indice du dernier élément de R
r_suivant = 7
while i_r < 10000:
    R.append(r_suivant)
    i_r += 1
    prochain = R[i_r] + S[i_r]
    if len(S) < 10000:
        for s in range(r_suivant + 1, prochain):
            S.append(s)
    r_suivant = prochain

# --------- PYODIDE:tests --------- #

assert R[3] == 7
assert S[3] == 5
assert (len(R) >= 10000) and (len(S) >= 10000)

# --------- PYODIDE:secrets --------- #


# autres tests

debut_R = [
    None,
    1,
    3,
    7,
    12,
    18,
    26,
    35,
    45,
    56,
    69,
    83,
    98,
    114,
    131,
    150,
    170,
    191,
    213,
    236,
]
debut_S = [None, 2, 4, 5, 6, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24]
fin_R = [
    50672311,
    50682424,
    50692538,
    50702653,
    50712769,
    50722886,
    50733004,
    50743123,
    50753243,
    50763364,
    50773486,
    50783609,
    50793733,
    50803858,
    50813984,
    50824111,
    50834239,
    50844368,
    50854498,
    50864629,
]
fin_S = [
    10113,
    10114,
    10115,
    10116,
    10117,
    10118,
    10119,
    10120,
    10121,
    10122,
    10123,
    10124,
    10125,
    10126,
    10127,
    10128,
    10129,
    10130,
    10131,
    10132,
]

attendu = debut_R
assert R[:20] == attendu
attendu = debut_S
assert S[:20] == attendu

attendu = fin_R
assert R[10000 - 20 : 10000] == attendu
attendu = fin_S
assert S[10000 - 20 : 10000] == attendu