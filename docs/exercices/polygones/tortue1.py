# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

def m_a_j():
    done()
    document.getElementById("cible_1").innerHTML = Screen().html


# --- PYODIDE:code --- #
from turtle import *

for i in range(4):
    forward(100)
    right(90)


# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j()
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j()

