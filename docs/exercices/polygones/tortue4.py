# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

def angle_poly(nbre):
    return 360/nbre

def m_a_j():
    done()
    document.getElementById("cible_4").innerHTML = Screen().html

# --- PYODIDE:code --- #
from turtle import *

def poly_reg(longueur, n):
    ...
  
poly_reg(100, 5)
poly_reg(80, 4)

# --- PYODIDE:corr --- #
def poly_reg(longueur, n):
    angle = angle_poly(n)
    for i in range(n):
        forward(longueur)
        left(angle)
  


# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j()
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j()

