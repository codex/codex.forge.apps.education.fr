---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Les polygones
difficulty: 90
maj: 23/10/2024
tags:
    - SNT
    - maths
ides:
    forbid_corr_and_REMs_with_infinite_attempts: false
    forbid_hidden_corr_and_REMs_without_secrets: false
---

Le but de cette activité est de dessiner des polygones réguliers *convexes* grâce au module `#!py turtle`.

Un polygone régulier est un polygone à la fois équilatéral (tous ses côtés ont la même longueur) et équiangle (tous ses angles ont la même mesure).

Un polygone régulier est convexe si ses côtés ne se croisent pas.

Le tableau ci-dessous montre les fonctionnalités du module `#!py turtle` utilisées :

| Fonctionnalité      | Description                                  |
| ------------------- | -------------------------------------------- |
| `#!py forward(100)` | fait avancer la tortue de 100 pixels         |
| `#!py left(45)`     | fait tourner la tortue de 45° vers la gauche |
| `#!py right(90)`    | fait tourner la tortue de 90° vers la droite |
| `#!py color('red')` | teint la couleur du pinceau en rouge         |


???+ example "Exemple"

    L'exemple suivant permet de tracer un carré (polygone régulier à 4 côtés) :

    {{ IDE('tortue1') }}

    {{ figure('cible_1', admo_kind='???+', div_class='py_mk_figure center autolight')}}

Les questions suivantes peuvent être traitées de manière indépendante. Les fonctions précédentes seront chargées en mémoire.

??? question "1. Construire un triangle équilatéral"

    Créer la procédure `triangle_equi` qui prend en paramètre la longueur de ses côtés `longueur` et trace un triangle équilatéral.

    ??? tip "Aide"
    
        On pourra suivre l'algorithme suivant, écrit en langage naturel :
        
        ```text title=""
        Fonction triangle_equi(longueur) :
            Répéter 3 fois :
                avancer de longueur
                tourner à gauche de 120°
        ```

    {{ IDE('tortue2', MODE="delayed_reveal") }}

    {{ figure('cible_2', div_class='py_mk_figure center autolight', admo_kind='???+') }}



??? question "2. Calcul de l'angle de construction"
    
    On souhaite maintenant construire un polygone régulier quel que soit le nombre de ses côtés. La tortue doit donc, dans chaque cas, tourner d'un angle différent. On appelle *angle de construction* cet angle.

    Créer une fonction `angle_poly` qui renvoie l'angle de construction en fonction du nombre de côtés `n` du polygone.
    
    ??? tip "Aide 1"
        
        La somme totale des angles de construction d'une figure convexe est égale à $360°$.
    
        ![polygone](polygone.gif){ width=30% .center .autolight}

    ??? tip "Aide 2"
        
        On pourra suivre l'algorithme suivant, écrit en langage naturel :

        ```text title=""
        Fonction angle_poly(n):
            angle = 360 / n
            Renvoyer angle
        ```

    {{ IDE('tortue3') }}


??? question "3. Construire un polygone régulier à `n` cotés"
    
    Créer une procédure `poly_reg` qui construit un polygone régulier en prenant comme paramètres la `longueur` et le nombre de côtés `n`.

    La fonction `angle_poly` pourra être réutilisée car déjà chargée en mémoire.
    
    ??? tip "Aide"
        
        On pourra suivre l'algorithme suivant, écrit en langage naturel :

        ```text title=""
        Fonction poly_reg(longueur, n) :
            angle = angle_poly(n)
            Répéter n fois :
                avancer de longueur
                tourner à gauche de angle
        ```
        
    {{ IDE('tortue4', MODE="delayed_reveal") }}

    {{ figure('cible_4', div_class='py_mk_figure center autolight', admo_kind='???+') }}


??? question "4. Construire un polygone régulier à `n` cotés colorés"

    Modifier la procédure `poly_reg` pour tracer en bleu les polygones dont le nombre de côtés est pair et en rouge les autres.

    La fonction `angle_poly` pourra être réutilisée car déjà chargée en mémoire.
    
    {{ remarque('modulo') }}

    ??? tip "Aide"
        
        On pourra suivre l'algorithme suivant, écrit en langage naturel :

        ```text title=""
        Fonction poly_reg(longueur, n) :
            angle = angle_poly(n)
            Si n est pair alors :
                couleur : bleue
            Sinon :
                couleur : rouge
            Répéter n fois
                avancer de longueur
                tourner à gauche de angle
        ```

    {{ IDE('tortue5', MODE="delayed_reveal") }}

    {{ figure('cible_5', div_class='py_mk_figure center autolight', admo_kind='???+') }}


??? question "5. Construire un polygone régulier aléatoire"

    Créer la fonction `poly_aleatoire` qui construit un polygone régulier avec un nombre aléatoire de côtés. La fonction prend comme paramètre la longueur des côtés et renvoie `True` si le polygone créé est un triangle, `False` sinon.
    
    Le nombre aléatoire de côtés sera choisi entre 3 et 10, inclus l'un et l'autre.

    La fonction `angle_poly` pourra être réutilisée car déjà chargée en mémoire.
    
    {{ remarque('randint') }}

    ??? tip "Aide"
        
        On pourra suivre l'algorithme suivant, écrit en langage naturel :

        ```text title=""
        Fonction poly_aleatoire(longueur) :
            n = entier aléatoire entre 3 et 10
            poly_reg(longueur, n)
            Si n égale 3 :
                Renvoyer Vrai
            Sinon  :
                Renvoyer Faux
        ```
    {{ IDE('tortue6', MODE="delayed_reveal") }}

    {{ figure('cible_6', div_class='py_mk_figure center autolight', admo_kind='???+') }}



??? question "6. Construire un nombre aléatoire de polygones réguliers"

    Écrire un programme qui construit des polygones aléatoires de côté mesurant `#!py 100` pixels jusqu'à construire un triangle.

    Le nombre aléatoire de côtés sera choisi entre 3 et 10, inclus l'un et l'autre.

    {{ remarque('randint')  }}
    
    Les fonctions `angle_poly` et `poly_aleatoire` pourront être réutilisées car déjà chargées en mémoire.
    ??? tip "Aide"
        
        On pourra suivre l'algorithme suivant, écrit en langage naturel :

        ```text title=""
        est_triangle = Faux
        Tant que est_triangle est Faux Faire :
            est_triangle = poly_aleatoire(100)
        ```
    
    {{ IDE('tortue7', MODE="delayed_reveal") }}

    {{ figure('cible_7', div_class='py_mk_figure center autolight', admo_kind='???+') }}