# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

def m_a_j():
    done()
    document.getElementById("cible_2").innerHTML = Screen().html


# --- PYODIDE:code --- #
from turtle import *

def triangle_equi(longueur):
    ...
  
triangle_equi(50)
triangle_equi(100)

# --- PYODIDE:corr --- #
def triangle_equi(longueur):
    for i in range(3):
        forward(longueur)
        left(120)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j()
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j()

