# --- PYODIDE:env --- #
from js import document
from turtle import *
if "restart" in globals():
    restart()

def m_a_j():
    done()
    document.getElementById("cible_7").innerHTML = Screen().html


def angle_poly(cote):
    return 360/cote

def poly_reg(longueur, nbre) :
    angle = angle_poly(nbre)
    if nbre%2 == 0:
        color('blue')
    else : 
        color('red')
    for i in range(nbre):
        forward(longueur)
        left(angle)
        
def poly_aleatoire(longueur):
    nbre_cotes = randint(3, 10)
    poly_reg(longueur,nbre_cotes)
    return nbre_cotes == 3

penup()
setposition(0,-100)
pendown()   
    
# --- PYODIDE:code --- #
from turtle import *
from random import randint

...
  
# --- PYODIDE:corr --- #
est_triangle = False
while est_triangle == False :
    est_triangle = poly_aleatoire(100)
      
# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j()
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j()

