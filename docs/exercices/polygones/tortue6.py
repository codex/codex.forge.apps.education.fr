# --- PYODIDE:env --- #
from js import document
from turtle import *

if "restart" in globals():
    restart()

def m_a_j():
    done()
    document.getElementById("cible_6").innerHTML = Screen().html


def angle_poly(nbre):
    return 360/nbre

def poly_reg(longueur, nbre) :
    angle = angle_poly(nbre)
    if nbre%2 == 0:
        color('blue')
    else : 
        color('red')
    for i in range(nbre):
        forward(longueur)
        left(angle)

penup()
setposition(0,-100)
pendown()   

# --- PYODIDE:code --- #
from turtle import *
from random import randint

def poly_aleatoire(longueur):
    ...
  
poly_aleatoire(100)
poly_aleatoire(80)


# --- PYODIDE:corr --- #
def poly_aleatoire(longueur):
    n = randint(3, 10)
    poly_reg(longueur, n)
    return n == 3
  


# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j()
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j()

