# --- PYODIDE:env --- #


def angle_poly(longueur):
    pass


# --- PYODIDE:code --- #
def angle_poly(n):
    ...


# --------- PYODIDE:tests --------- #
assert angle_poly(4) == 90


# --------- PYODIDE:corr --------- #
def angle_poly(n):
    return 360 / n


# --------- PYODIDE:secrets --------- #

assert angle_poly(3) == 120.0
assert angle_poly(10) == 36.0
assert angle_poly(6) == 60.0
