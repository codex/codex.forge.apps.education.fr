Le problème revient à sélectionner le minimum dans une liste. Deux subtilités toutefois :

* la liste contient les "étiquettes" des données à comparer (les prénoms des joueurs alors que l'on compare les classements) ;
* certains joueurs n'ont pas de classement : on utilise alors une valeur par défaut supérieure au plus grand classement possible.
