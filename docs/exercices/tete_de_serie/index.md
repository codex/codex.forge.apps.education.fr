---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Tête de série
tags:
    - dictionnaire
    - à trous
difficulty: 205
maj: 01/03/2024
---

L'organisateur d'un tournoi sportif souhaite déterminer le nom du joueur participant le mieux classé dans le classement international.

Les classements des joueurs sont des entiers tous distincts et compris entre `#!py 1` et `len(classement)`.

Il connaît donc le nom des participants au tournoi, donné sous forme d'une liste, ainsi que le classement, donné sous forme d'un dictionnaire.

Par exemple :

```python
participants = ['Stefanos', 'Rafael', 'David', 'Novak']
classement = {'Novak': 1, 'Daniil': 2, 'Alexander': 3, 'Stefanos': 4, 'Rafael': 5}
```

Comme on peut le voir dans l'exemple :

* tous les joueurs classés ne sont pas obligatoirement inscrits au tournoi (ici `Daniil` et `Alexander` ne participent pas) ;

* certains participants au tournoi, invités d'honneur, n'ont pas de classement (`David` ici)... On considérera donc qu'ils sont moins bien classés que n'importe lequel des joueurs classés.

Vous devez écrire une fonction `tete_de_serie` prenant en paramètres la liste des joueurs inscrits au tournoi `participants` ainsi que le dictionnaire `classement` et renvoyant le prénom du joueur le mieux classé.

!!! warning "Attention"

    Le dictionnaire `classement` ne devra pas être modifié lors de l'exécution de la fonction

Si le tournoi ne comporte que des joueurs non classés, la fonction renverra le prénom du premier joueur de la liste, sinon la fonction renverra le prénom du joueur le mieux classé.

???+ example "Exemples"

    ```pycon title=""
    >>> classement = {'Novak': 1, 'Daniil': 2, 'Alexander': 3, 'Stefanos': 4, 'Rafael': 5}
    >>> participants = ['Stefanos', 'Novak', 'Rafael']
    >>> tete_de_serie(participants, classement)
    'Novak'
    >>> participants = ['Stefanos', 'Rafael', 'David', 'Novak']
    >>> tete_de_serie(participants, classement)
    'Novak'
    >>> participants = ['David', 'Novak', 'Alexander', 'Daniil']
    >>> tete_de_serie(participants, classement)
    'Novak'
    >>> participants = ['David', 'Olivier']
    >>> tete_de_serie(participants, classement)
    'David'
    ```

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_a_completer') }}
