

# --------- PYODIDE:code --------- #

def plage(n, signe):
    ...

# --------- PYODIDE:corr --------- #

def plage(n, signe):
    if signe:
        limite = 2 ** (n - 1)
        return -limite, limite - 1
    else:
        return 0, 2**n - 1

# --------- PYODIDE:tests --------- #

assert plage(1, False) == (0, 1)
assert plage(1, True) == (-1, 0)
assert plage(8, False) == (0, 255)
assert plage(8, True) == (-128, 127)

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
for n in range(1, 100):
    mini_signe = -(2 ** (n - 1))
    maxi_signe = 2 ** (n - 1) - 1
    mini_non_signe = 0
    maxi_non_signe = 2**n - 1
    assert plage(n, True) == (mini_signe, maxi_signe)
    assert plage(n, False) == (mini_non_signe, maxi_non_signe)