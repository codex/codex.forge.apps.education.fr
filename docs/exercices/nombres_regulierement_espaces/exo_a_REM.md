Avec des listes en compréhension, il est possible de faire plus court :

```python
def linspace(a, b, n, ferme):
    if ferme:
        return [a + i*(b-a)/(n-1) for i in range(n)]
    else:
        return [a + i*(b-a)/n for i in range(n)]
```
