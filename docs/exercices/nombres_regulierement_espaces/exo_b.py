

# --------- PYODIDE:code --------- #

def sont_proches(a, b):
    return abs(a - b) < 10**-6

def listes_proches(liste1, liste2):
    if len(liste1) != len(liste2):
        return False
    for i in range(len(liste1)):
        if not sont_proches(liste1[i], liste2[i]):
            return False
    return True

def linspace(a, b, n, ferme):
    if ferme:
        pas = ...
    else:
        ...
    valeurs = ...
    x = ...
    for i in range(...):
        valeurs.append(...)
        x = ...
    return valeurs

# --------- PYODIDE:corr --------- #

def sont_proches(a, b):
    return abs(a - b) < 10**-6

def listes_proches(liste1, liste2):
    if len(liste1) != len(liste2):
        return False
    for i in range(len(liste1)):
        if not sont_proches(liste1[i], liste2[i]):
            return False
    return True

def linspace(a, b, n, ferme):
    if ferme:
        pas = (b - a) / (n - 1)
    else:
        pas = (b - a) / n
    valeurs = []
    x = a
    for i in range(n):
        valeurs.append(x)
        x = x + pas
    return valeurs

# --------- PYODIDE:tests --------- #

resultat = linspace(2.0, 4.0, 5, True)
attendu = [2.0, 2.5, 3.0, 3.5, 4.0]
assert listes_proches(resultat, attendu), "Erreur avec linspace(2.0, 4.0, 5, True)"

resultat = linspace(5.0, 9.5, 6, False)
attendu = [5.0, 5.75, 6.5, 7.25, 8.0, 8.75]
assert listes_proches(resultat, attendu), "Erreur avec linspace(5.0, 9.5, 6, False)"

# --------- PYODIDE:secrets --------- #

def sont_proches(a, b):
    return abs(a - b) < 10**-6

def listes_proches(liste1, liste2):
    if len(liste1) != len(liste2):
        return False
    for i in range(len(liste1)):
        if not sont_proches(liste1[i], liste2[i]):
            return False
    return True
# tests secrets
resultat = linspace(-10, 10, 7, True)
attendu = [-30/3, -20/3, -10/3, 0, 10/3, 20/3, 30/3]
assert listes_proches(resultat, attendu), "Erreur avec linspace(-10, 10, 7, True)"

resultat = linspace(-0.1, 0.1, 3, False)
attendu = [-1/10, -1/30, 1/30]
assert listes_proches(resultat, attendu), "Erreur avec linspace(-0.1, 0.1, 3, False)"