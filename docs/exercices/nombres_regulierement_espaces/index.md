---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Nombres régulièrement espacés
tags:
    - float
    - à trous
difficulty: 310
---



Pour construire la représentation graphique d'une fonction définie sur un intervalle, on calcule les images de plusieurs valeurs régulièrement espacées, ce qui permet de placer des points à relier. Encore faut-il disposer de ces valeurs. Comment les choisir ? Voici quelques exemples :

- Pour une fonction définie sur $[2.0, 4.0]$, avec 5 points régulièrement espacés, on prend $2.0, 2.5, 3.0, 3.5, 4.0$.
- Pour une fonction définie sur $[5.0, 6.5[$, avec 6 points régulièrement espacés, on prend $5.0, 5.25, 5.5, 5.75, 6.0, 6.25$.

Dans cet exercice, on se limite aux intervalles de type $[a\,;\,b]$ ($b$ est inclus) ou $[a\,;\,b[$ ($b$ est exclu) pour lesquels on souhaite proposer $n$ nombres régulièrement espacés, où $n$ est un nombre entier au moins égal à 2.

- Si l'intervalle est de type $[a\,;\,b]$, on part de $a$ et on avance par pas de $\dfrac{b-a}{n-1}$.
- Si l'intervalle est de type $[a\,;\,b[$, on part de $a$ et on avance par pas de $\dfrac{b-a}{n}$.

Écrire une fonction `linspace` telle que `#!py linspace(a, b, n, ferme)` renvoie une liste de $n$ nombres flottants régulièrement espacés en partant de `#!py a`, où `#!py a` et `#!py b` sont des flottants, `#!py n` est un entier au moins égal à 2, et `#!py ferme` est un booléen.

- Si le booléen `#!py ferme` est égal à `#!py True`, on choisira un pas adapté à l'intervalle fermé à droite $[a\,;\,b]$ ($b$ est inclus). 
- Si le booléen `#!py ferme` est égal à `#!py False`, on choisira un pas adapté à l'intervalle ouvert à droite $[a\,;\,b[$ ($b$ est exclu). 


On garantit que $a$ et $b$ sont compris entre $-10^9$ et $10^9$, et que la différence $b-a$ est supérieure à $10^{-9}$. On garantit que $n$ est inférieur à $1\,000$.


???+ example "Exemples"

    ```pycon title=""
    >>> linspace(2.0, 4.0, 5, True)
    [2.0, 2.5, 3.0, 3.5, 4.0]
    >>> linspace(5.0, 9.5, 6, False)
    [5.0, 5.75, 6.5, 7.25, 8.0, 8.75]
    ```

??? warning "Comparaison avec les nombres non entiers"

    La comparaison des nombres non entiers repose sur leur représentation en machine. 
    
    Celle-ci n'est pas exacte pour les nombres flottants qui ne peuvent pas s'écrire avec un nombre fini de nombres sous la forme $2^k$ avec $k$ entier relatif. On n'est donc jamais certain que le résultat renvoyé par `a == b` entre deux flottants soit juste.
    
    C'est pourquoi les tests ne vérifient pas l'égalité des résultats et des valeurs attendues mais leur proximité à l'aide de la fonction `sont_proches`. On considère que deux nombres dont l'écart est inférieur à $10^{-6}$, c'est-à-dire un millionième, sont égaux.

    On rajoute également une fonction `listes_proches` qui permet
    de déterminer si deux listes de même longueur ont leurs éléments qui
    sont proches deux à deux.


??? info "Pourquoi le nom `#!py linspace` ?"
    La fonction `#!py linspace` existe dans le module `#!py numpy`.
    
    Il est souhaitable de se passer d'un si gros module si c'est uniquement pour utiliser une fonction comme `#!py linspace`. Cet exercice montre comment construire cette fonction qui est utile pour des représentations graphiques.

=== "Version vide"
    {{ IDE('exo_a') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}
