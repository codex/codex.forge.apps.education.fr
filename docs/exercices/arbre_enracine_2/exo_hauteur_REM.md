On peut aussi utiliser un générateur afin de parcourir les différents sous_arbres :

```python
def hauteur(arbre):
    if len(arbre) == 1:
        return 0
    return 1 + max(hauteur(arbre[i]) for i in range(1, len(arbre)))
```