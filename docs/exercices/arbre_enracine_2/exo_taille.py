# --------- PYODIDE:code --------- #
def taille(arbre):
    ...

# --------- PYODIDE:corr --------- #
def taille(arbre):
    t = 1
    for i in range(1, len(arbre)):
        t = t + taille(arbre[i])
    return t

# --------- PYODIDE:tests --------- #
assert taille(["a"]) == 1
assert taille(["a", ["b"], ["c"], ["d", ["e"]]]) == 5
# --------- PYODIDE:secrets --------- #
arbre = ["a", ["b"]]
attendu = 2
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ["a", ["b"], ["c"]]
attendu = 3
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ["a", ["b"], ["c"], ["d"]]
attendu = 4
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ["a", ["b", ["e"]], ["c"], ["d"]]
attendu = 5
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ["a", ["b", ["e", ["f"]]], ["c"], ["d"]]
attendu = 6
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
arbre = ["a", ["b", ["c", ["d"]]], ["e", ["f", ["g"]]], ["h"]]
attendu = 8
assert taille(arbre) == attendu, f"Erreur avec {arbre = }"
