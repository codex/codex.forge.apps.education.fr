---
author: 
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Taille et hauteur d'un arbre (2)
difficulty: 280
tags:
    - arbre
    - récursivité
maj: 14/01/2025
---

??? info "Différentes représentations"

    Cet exercice demande d'écrire les fonctions calculant la taille et hauteur d'un arbre.
    Il fait partie d'une série dans laquelle on utilise différentes représentations :
    
    * {{ lien_exo("Représentation avec des listes **sans** étiquettes", "arbre_enracine") }} ;
    * {{ lien_exo("Représentation avec des listes **avec** étiquettes", "arbre_enracine_3") }} ;
    * {{ lien_exo("Représentation avec des tuples", "arbre_enracine_2") }}.

Dans tout le sujet, on considère des **arbres enracinés**. On rappelle que les arbres binaires ne sont pas des arbres enracinés.

On rappelle la définition d'un arbre enraciné : il s'agit d'une racine qui possède $0$, $1$ ou plusieurs sous-arbres qui sont eux-mêmes des arbres enracinés.

Un arbre enraciné qui ne possède pas de sous-arbre est appelé feuille.

On représente un arbre enraciné par une liste contenant la valeur portée par la racine suivie des sous-arbres. Une feuille est donc représentée par une liste d'un unique élément `#!py [x]` où `#!py x` est une valeur quelconque.

L'arbre représenté par `#!py ["a", ["b"], ["c"], ["d", ["e"]]]` a quant à lui une racine portant la valeur `#!py "a"` et possède trois sous-arbres :

- le premier est une feuille,

- le deuxième est une feuille,

- le troisième est un arbre qui a un sous-arbre qui est une feuille.

Cet arbre se dessine ainsi : 

<center>

```mermaid
graph TD
    R{a} --> N1{b}
    R    --> N2{c}
    R    --> N3{d}
    N3   --> N4{e}
```
</center>

**Dans cet exercice**, la définition de la hauteur d'un arbre est le nombre maximal de filiations pour rejoindre la racine à une feuille. Un arbre réduit à sa racine a donc une hauteur égale à $0$.

??? question "1. Taille de l'arbre"

    Écrire la fonction `#!py taille` qui prend en paramètre une liste représentant un arbre et renvoie sa taille.

    ???+ example "Exemples"

        ```pycon title=""
        >>> taille(["a"])
        1
        >>> taille(["a", ["b"], ["c"], ["d", ["e"]]])
        5
        ```

    {{ IDE('exo_taille') }}

??? question "2. Hauteur de l'arbre"

    Écrire la fonction `#!py hauteur` qui prend en paramètre une liste représentant un arbre et renvoie sa hauteur.


    ???+ example "Exemples"

        ```pycon title=""
        >>> hauteur(["a"])
        0
        >>> hauteur(["a", ["b"], ["c"], ["d", ["e"]]])
        2
        ```

    {{ IDE('exo_hauteur') }}

