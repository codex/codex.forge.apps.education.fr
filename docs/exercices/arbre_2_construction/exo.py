

# --------- PYODIDE:code --------- #

def construction(arbre1, arbre2):
    ...

# --------- PYODIDE:corr --------- #

def construction(arbre1, arbre2):
    n = len(arbre1)
    m = len(arbre2)
    arbre = [0] * (n + m + 1)
    for i in range(n):
        if arbre1[i] == i:
            arbre[i] = n + m
        else:
            arbre[i] = arbre1[i]
    for i in range(m):
        if arbre2[i] == i:
            arbre[i + n] = n + m
        else:
            arbre[i + n] = arbre2[i] + n
    arbre[n + m] = n + m
    return arbre

# --------- PYODIDE:tests --------- #

a1 = [3, 3, 3, 3]
a2 = [1, 1, 1, 2, 2]
assert construction(a1, a2) == [3, 3, 3, 9, 5, 9, 5, 6, 6, 9]
a1 = [2, 2, 2]
a2 = [2, 2, 3, 3, 3]
assert construction(a1, a2) == [2, 2, 8, 5, 5, 6, 8, 6, 8]

# --------- PYODIDE:secrets --------- #

a1 = [0]
a2 = [0]
assert construction(a1, a2) == [2, 2, 2]
a1 = [5, 5, 6, 6, 6, 7, 7, 7]
a2 = [0]
assert construction(a1, a2) == [5, 5, 6, 6, 6, 7, 7, 9, 9, 9]
a1 = [0]
a2 = [5, 5, 6, 6, 6, 7, 7, 7]
assert construction(a1, a2) == [9, 6, 6, 7, 7, 7, 8, 8, 9, 9]
a1 = [1, 1, 1]
a2 = [1, 1, 1]
assert construction(a1, a2) == [1, 6, 1, 4, 6, 4, 6]