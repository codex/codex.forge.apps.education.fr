Il s'agit de l'algorithme de recherche dichotomique classique.

Le calcul de l'indice du milieu mérite toutefois quelques commentaires. Ici on propose `#!py milieu = (debut + fin) // 2`.

Or, dans certains langages de programmation comme le C, la taille des entiers est limitée. Ce calcul `#!py milieu = (debut + fin) // 2` peut parfois excéder cette taille limite. En effet, si les deux indices `#!py debut` et `#!py fin` sont grands, leur somme `#!py debut + fin` peut dépasser la valeur limite.

Dans ce cas on calcule l'indice du milieu en utilisant la largeur de l'intervalle : `#!py milieu = debut + (fin - debut) // 2`.

Dans le cas présent, le problème ne se pose pas car, en Python, les entiers n'ont pas de taille limite.

Il arrive dans certains cas, que la recherche ne s'effectue pas dans le tableau entier mais sur une portion de celui-ci.
On fournit alors à l'algorithme les indices de début et de fin de la zone de recherche. On convient dans l'exemple ci-dessous que l'indice de début est inclus alors que celui de fin est exclu de la zone de recherche (à l'image de `#!py range`).

```python
def indice(tableau, cible, debut, fin):
    while debut < fin:
        milieu = (debut + fin) // 2
        if cible < tableau[milieu]:
            fin = milieu - 1
        elif cible > tableau[milieu]:
            debut = milieu + 1
        else:
            return milieu

    # La cible n'est pas présente : on lève une erreur
    raise ValueError("La valeur cible n'est pas dans le tableau")
```
