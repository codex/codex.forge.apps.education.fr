---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Recherche dichotomique (indice)
difficulty: 220
tags:
    - liste/tableau
    - dichotomie
    - ep1
maj: 01/03/2024
---



On considère dans cet exercice des tableaux non vides contenant des nombres entiers, tous distincts, triés dans l'ordre croissant.

On cherche à déterminer l'indice d'une valeur `#!py cible` dans ce tableau à l'aide d'une **recherche dichotomique** dans sa version itérative.

Écrire la fonction `#!py indice` qui prend en paramètres le tableau de nombres `#!py tableau` et la valeur cherchée `#!py cible`.

Si la `#!py cible` est dans le tableau, la fonction renverra son indice. Dans le cas contraire, la fonction lèvera une erreur.

???+ note "Lever une erreur"

    Python permet de lever des erreurs en utilisant la syntaxe `#!py raise <Erreur>`.

    Dans le cas présent, on fera `#!py raise ValueError("La valeur cible n'est pas dans le tableau")`.


???+ danger "Attention"

    Les tableaux des tests secrets peuvent être très grands. Une recherche linéaire naïve prendrait trop de temps lors de l'exécution.

    Les tests secrets limitent le nombre de lectures dans le tableau à 100. Si votre code accède à plus de 100 valeurs dans le tableau, une erreur sera levée.

???+ example "Exemples"

    ```pycon title=""
    >>> tableau = [23, 28, 29, 35, 37]
    >>> indice(tableau, 23)
    0
    >>> indice(tableau, 29)
    2
    >>> indice(tableau, 37)
    4
    ```

{{ IDE('exo') }}
