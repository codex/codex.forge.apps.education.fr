# --------- PYODIDE:env --------- #

def depouille(urne):
    resultat = {}
    for bulletin in urne:
        if bulletin in resultat:
            resultat[bulletin] = resultat[bulletin] + 1
        else:
            resultat[bulletin] = 1
    return resultat

# --------- PYODIDE:code --------- #


def vainqueur(effectifs_votes):
    nb_votes_max = 0
    for candidat in effectifs_votes:
        nb_votes = ...
        if ... > ...:
            nb_votes_max = ...
    gagnants = [nom for nom in effectifs_votes if effectifs_votes[nom] == ...]
    return ...


# --------- PYODIDE:corr --------- #



def vainqueur(effectifs_votes):
    nb_votes_max = 0
    for candidat in effectifs_votes:
        nb_votes = effectifs_votes[candidat]
        if nb_votes > nb_votes_max:
            nb_votes_max = nb_votes
    gagnants = [nom for nom in effectifs_votes if effectifs_votes[nom] == nb_votes_max]
    return gagnants

# --------- PYODIDE:tests --------- #

resultats == {'Oreilles sales': 3, 'Extra Vomit': 4, 'Lady Baba': 3}
assert vainqueur(resultats) == ['Extra Vomit']

resultats_2 == {'Poons': 2, 'DrRed': 2, 'Soleran': 3, 'Mimeen': 3, 'Zajy': 2, 'Kashur': 1}
assert sorted(vainqueur(resultats_2)) == ['Mimeen', 'Soleran']

# --------- PYODIDE:secrets --------- #


# Autres tests

urne_3 = ['Soleran', 'Kashur', 'Poons', 'DrRed', 'Kashur', 'DrRed', 'Mimeen', 'Mimeen', 'Mimeen', 'Zajy', 'Mimeen', 'Poons', 'Soleran', 'Poons', 'Kashur', 'Zajy', 'DrRed', 'Soleran', 'DrRed', 'Kashur', 'Mimeen', 'Kashur', 'Mimeen', 'Kashur', 'Zajy', 'Poons', 'Kashur', 'Poons', 'Zajy', 'Soleran', 'DrRed', 'Mimeen', 'Zajy', 'DrRed', 'DrRed']

urne_4 = ['Poons', 'Kashur', 'Poons', 'DrRed', 'Kashur', 'DrRed', 'Mimeen', 'Mimeen', 'Mimeen', 'Poons', 'Mimeen', 'Poons', 'Soleran', 'Poons', 'Kashur', 'Zajy', 'DrRed', 'Soleran', 'DrRed', 'Kashur', 'Mimeen', 'Kashur', 'Mimeen', 'Kashur', 'Zajy', 'Poons', 'Kashur', 'Poons', 'Zajy', 'Soleran', 'DrRed', 'Mimeen', 'Zajy', 'DrRed', 'DrRed']

assert sorted(vainqueur(depouille(urne_3))) == ['DrRed', 'Kashur', 'Mimeen']
assert sorted(vainqueur(depouille(urne_4))) == ['DrRed', 'Kashur', 'Mimeen', 'Poons']