

# --------- PYODIDE:code --------- #

def depouille(urne):
    ...



# --------- PYODIDE:corr --------- #

def depouille(urne):
    resultat = {}
    for bulletin in urne:
        if bulletin in resultat:
            resultat[bulletin] = resultat[bulletin] + 1
        else:
            resultat[bulletin] = 1
    return resultat



# --------- PYODIDE:tests --------- #

urne = ['Oreilles sales', 'Oreilles sales', 'Oreilles sales',
      'Extra Vomit', 'Lady Baba', 'Extra Vomit', 'Lady Baba',
      'Extra Vomit', 'Lady Baba', 'Extra Vomit']

urne_2 = ['Poons', 'DrRed', 'Soleran', 'Mimeen', 'Poons', 'Soleran', 'Mimeen', 'Zajy', 'Soleran', 'DrRed', 'Zajy', 'Kashur', 'Mimeen']

resultats = depouille(urne)
resultats_2 = depouille(urne_2)
assert resultats == {'Oreilles sales': 3, 'Extra Vomit': 4, 'Lady Baba': 3}
assert resultats_2 == {'Poons': 2, 'DrRed': 2, 'Soleran': 3, 'Mimeen': 3, 'Zajy': 2, 'Kashur': 1}

# --------- PYODIDE:secrets --------- #


# Autres tests

urne_3 = ['Soleran', 'Kashur', 'Poons', 'DrRed', 'Kashur', 'DrRed', 'Mimeen', 'Mimeen', 'Mimeen', 'Zajy', 'Mimeen', 'Poons', 'Soleran', 'Poons', 'Kashur', 'Zajy', 'DrRed', 'Soleran', 'DrRed', 'Kashur', 'Mimeen', 'Kashur', 'Mimeen', 'Kashur', 'Zajy', 'Poons', 'Kashur', 'Poons', 'Zajy', 'Soleran', 'DrRed', 'Mimeen', 'Zajy', 'DrRed', 'DrRed']

urne_4 = ['Poons', 'Kashur', 'Poons', 'DrRed', 'Kashur', 'DrRed', 'Mimeen', 'Mimeen', 'Mimeen', 'Poons', 'Mimeen', 'Poons', 'Soleran', 'Poons', 'Kashur', 'Zajy', 'DrRed', 'Soleran', 'DrRed', 'Kashur', 'Mimeen', 'Kashur', 'Mimeen', 'Kashur', 'Zajy', 'Poons', 'Kashur', 'Poons', 'Zajy', 'Soleran', 'DrRed', 'Mimeen', 'Zajy', 'DrRed', 'DrRed']

assert depouille(urne_3) == {'Soleran': 4, 'Kashur': 7, 'Poons': 5, 'DrRed': 7, 'Mimeen': 7, 'Zajy': 5}
assert depouille(urne_4) == {'Poons': 7, 'Kashur': 7, 'DrRed': 7, 'Mimeen': 7, 'Soleran': 3, 'Zajy': 4}