---
author:
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Meilleures apparitions dans un tableau
tags:
    - à trous
    - dictionnaire
    - ep2
difficulty: 220
--- 

Chaque soir, les auditeurs d'une radio votent en ligne pour leur artiste favori. Ces votes sont stockés dans un tableau.

???+ example "Exemple"

    ```pycon title=""
    >>> urne = ['Oreilles sales', 'Oreilles sales', 'Oreilles sales', 'Extra Vomit', 'Lady Baba', 'Extra Vomit', 'Lady Baba', 'Extra Vomit', 'Lady Baba', 'Extra Vomit']
    >>> depouille(urne)
    {'Oreilles sales': 3, 'Extra Vomit': 4, 'Lady Baba': 3}
    >>> vainqueur(depouille(urne))
    ['Extra Vomit']
    >>> urne_2 = ['Poons', 'DrRed', 'Soleran', 'Mimeen', 'Poons', 'Soleran', 'Mimeen', 'Zajy', 'Soleran', 'DrRed', 'Zajy', 'Kashur', 'Mimeen']
    >>> depouille(urne_2)
    {'Poons': 2, 'DrRed': 2, 'Soleran': 3, 'Mimeen': 3, 'Zajy': 2, 'Kashur': 1}
    >>> sorted(vainqueur(depouille(urne_2)))
    ['Mimeen', 'Soleran']
    ```

??? question "La fonction `depouille`"

    La fonction `depouille` prend la liste des votes et renvoie un dictionnaire contenant le nombre de votes pour chaque artiste. 

    Compléter la fonction `depouille` fournie dans l'IDE pour qu'elle renvoie les résultats attendus.

    === "Version vide"
        {{ IDE('exo_a_vide') }}
    === "Version à compléter"
        {{ IDE('exo_a') }}


??? question "La fonction `vainqueur`"

    La fonction `vainqueur` doit désigner le nom du ou des gagnants. Elle prend en paramètre un dictionnaire dont la structure est celle renvoyé par la fonction `depouille` et renvoie un tableau. Ce tableau peut donc contenir plusieurs éléments s'il y a des artistes ex aequo.

    Compléter la fonction `vainqueur` fournie dans l'IDE pour qu'elle renvoie les résultats attendus. La liste des artistes renvoyée pourra contenir les vaiqueurs dans n'importe quel ordre. Les tests s'effectuent en effet sur la version triée de cette liste, `#!py sorted(vainqueur(resultats_2))` par exemple. 

    === "Version vide"
        {{ IDE('exo_vide') }}
    === "Version à compléter"
        {{ IDE('exo') }}


