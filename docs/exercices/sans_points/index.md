---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Avec ou sans points
tags:
    - string
    - booléen
difficulty: 220
---


 
Une adresse électronique est composée de deux parties séparées par un « @ » :

* la partie *locale*, avant le « @ »,
* la partie *domaine*, après le « @ ».

Certains services de messagerie ignorent les caractères `.` dans la partie *locale*. Ainsi, sur ces services, les messages envoyés aux adresses `cours.premiere@e-nsi.fr`, `co.ur.spremi.ere@e-nsi.fr` et `cours..premiere@e-nsi.fr` arrivent tous au même destinataire `courspremiere@e-nsi.fr`.

Par contre, les adresses `cours_premiere@e-nsi.fr`, `courspremier@e-nsi.fr` et `courspremiere@aeif.org` ne correspondent pas à ce destinataire.

On cherche dans cet exercice à vérifier qu'une adresse correspond bien à une adresse de référence. L'adresse de référence est l'adresse électronique écrite **sans aucun point dans la partie *locale***.

Les adresses se correspondent donc si :

- sans tenir compte des caractères `.`, les deux parties *locales* comportent les mêmes caractères présents dans le même ordre,
- si les parties *domaines* sont strictement égales.

On garantit que les deux adresses ne comptent qu'un seul caractère `'@'`.

Écrire la fonction `correspond` qui détermine si `adresse` correspond à `reference`. Les deux arguments sont :

* `adresse` : l'adresse électronique dont on souhaite tester la correspondance,
  
* `reference` : l'adresse électronique de référence.

La fonction `correspond` renvoie `True` ou `False` selon que les deux adresses correspondent ou non.

!!! danger "Contrainte"

    On interdit d'utiliser les méthodes `split` et `replace` des chaînes de caractères .


???+ example "Exemples"

    ```pycon title=""
    >>> correspond("courspremiere@e-nsi.fr", "courspremiere@e-nsi.fr")
    True
    >>> correspond("cours.premiere@e-nsi.fr", "courspremiere@e-nsi.fr")
    True
    >>> correspond("co.ur.spremi.ere@e-nsi.fr", "courspremiere@e-nsi.fr")
    True
    >>> correspond("cours..premiere@e-nsi.fr", "courspremiere@e-nsi.fr")
    True
    >>> correspond(".courspremiere.@e-nsi.fr", "courspremiere@e-nsi.fr")
    True
    >>> correspond("cours_premiere@e-nsi.fr", "courspremiere@e-nsi.fr")
    False
    >>> correspond("courspremier@e-nsi.fr", "courspremiere@e-nsi.fr")
    False
    >>> correspond("courspremiere@aeif.org", "courspremiere@e-nsi.fr")
    False
    ```

{{ IDE('exo') }}
