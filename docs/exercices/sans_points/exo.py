

# --------- PYODIDE:code --------- #

def correspond(adresse, reference):
    ...

# --------- PYODIDE:corr --------- #

def correspond(adresse, reference):
    longueur_adr = len(adresse)
    longueur_ref = len(reference)

    i_adr = 0
    i_ref = 0

    local = True

    while (i_adr < longueur_adr) and (i_ref < longueur_ref):
        if adresse[i_adr] == "." and local:
            i_adr += 1
        elif adresse[i_adr] != reference[i_ref]:
            return False
        else:
            if reference[i_ref] == "@":
                local = False
            i_adr += 1
            i_ref += 1
    return (i_adr == longueur_adr) and (i_ref == longueur_ref)

# --------- PYODIDE:tests --------- #

assert correspond("courspremiere@e-nsi.fr", "courspremiere@e-nsi.fr") is True
assert correspond("cours.premiere@e-nsi.fr", "courspremiere@e-nsi.fr") is True
assert correspond("co.ur.spremi.ere@e-nsi.fr", "courspremiere@e-nsi.fr") is True
assert correspond("cours..premiere@e-nsi.fr", "courspremiere@e-nsi.fr") is True
assert correspond(".courspremiere.@e-nsi.fr", "courspremiere@e-nsi.fr") is True
assert correspond("cours_premiere@e-nsi.fr", "courspremiere@e-nsi.fr") is False
assert correspond("courspremier@e-nsi.fr", "courspremiere@e-nsi.fr") is False
assert correspond("courspremiere@aeif.org", "courspremiere@e-nsi.fr") is False

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert correspond("spe_isn@serveur.org", "spe_nsi@serveur.org") is False
assert correspond("specialite_nsi@serveur.org", "spe_nsi@serveur.org") is False
assert correspond("..spe_nsi@serveur.org", "spe_nsi@serveur.org") is True
assert correspond("spe_nsi..@serveur.org", "spe_nsi@serveur.org") is True


import string, random

carcateres = string.ascii_lowercase + string.digits[:-2] + "-_"
serveur_A = "@e-nsi.fr"
serveur_B = "@e-nsi.de"
serveur_C = "@e-nsi.fra"
for _ in range(100):
    locale_ref = [random.choice(carcateres) for _ in range(10)]
    reference = "".join(locale_ref) + serveur_A
    locale_adr = locale_ref.copy()
    for nb_points in range(random.randrange(0, 5)):
        position = random.randrange(0, len(locale_adr))
        locale_adr.insert(position, ".")

    # Identiques
    adresse = "".join(locale_adr) + serveur_A
    assert correspond(adresse, reference) is True, f"Erreur avec {adresse} et {reference}"

    # Serveurs différents
    adresse = "".join(locale_adr) + serveur_B
    assert correspond(adresse, reference) is False, f"Erreur avec {adresse} et {reference}"

    # Serveurs différents (plus long)
    adresse = "".join(locale_adr) + serveur_C
    assert correspond(adresse, reference) is False, f"Erreur avec {adresse} et {reference}"

    # Partie locale différente
    adresse = "".join(locale_ref)[:-2] + "89" + serveur_A
    assert correspond(adresse, reference) is False, f"Erreur avec {adresse} et {reference}"

    # Partie locale plus courte
    adresse = "".join(locale_ref)[:-2] + serveur_A
    assert correspond(adresse, reference) is False, f"Erreur avec {adresse} et {reference}"

    # Partie locale plus longue
    adresse = "".join(locale_ref) + "".join(locale_ref)[:2] + serveur_A
    assert correspond(adresse, reference) is False, f"Erreur avec {adresse} et {reference}"