Cet exercice demande de coder une fonction approchant la méthode `flatten` du module `numpy`.


# Une solution


Dans la fonction `aplatir` on parcourt l'ensemble des lignes et des valeurs de `tableau` en l'on ajoute les valeurs au fur et à mesure dans une liste `resultat`.

# Variante avec `extend`

```python
def aplatir(tableau):
    resultat = []
    for ligne in tableau:
        resultat.extend(ligne)
    return resultat
```

# Variante avec une double compréhension

```python
def aplatir(tableau):
    return [element for ligne in tableau for element in ligne]
```
