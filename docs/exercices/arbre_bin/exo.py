

# --------- PYODIDE:code --------- #

class ArbreBinaire:
    def __init__(self, gauche=None, etiquette=None, droite=None):
        self.gauche = gauche
        self.etiquette = etiquette
        self.droite = droite

    def est_vide(self):
        return self.etiquette is None

    def hauteur(self):
        if self.est_vide():
            return 0
        else:
            return ...

    def taille(self):
        ...

# --------- PYODIDE:corr --------- #

class ArbreBinaire:
    def __init__(self, gauche=None, etiquette=None, droite=None):
        self.gauche = gauche
        self.etiquette = etiquette
        self.droite = droite

    def est_vide(self):
        return self.etiquette is None

    def hauteur(self):
        if self.est_vide():
            return 0
        else:
            return 1 + max(self.gauche.hauteur(), self.droite.hauteur())

    def taille(self):
        if self.est_vide():
            return 0
        else:
            return 1 + self.gauche.taille() + self.droite.taille()

# --------- PYODIDE:tests --------- #

nil = ArbreBinaire()
assert nil.est_vide()
assert nil.taille() == 0
assert nil.hauteur() == 0

arbre_D = ArbreBinaire(nil, "N4", nil)
assert arbre_D.est_vide() is False
assert arbre_D.taille() == 1
assert arbre_D.hauteur() == 1

arbre_E = ArbreBinaire(nil, "N5", nil)

arbre_B = ArbreBinaire(arbre_D, "N2", arbre_E)
arbre_C = ArbreBinaire(nil, "N3", nil)
arbre_A = ArbreBinaire(arbre_B, "N1", arbre_C)
assert arbre_A.est_vide() is False

assert arbre_B.taille() == 3, f"{arbre_B.taille()}"
assert arbre_B.hauteur() == 2, f"{arbre_B.hauteur()}"

assert arbre_A.taille() == 5, f"{arbre_A.taille()}"
assert arbre_A.hauteur() == 3, f"{arbre_A.hauteur()}"

# --------- PYODIDE:secrets --------- #


# autres tests


nil = ArbreBinaire()
assert nil.est_vide()
assert nil.taille() == 0
assert nil.hauteur() == 0

noeud_1 = ArbreBinaire(nil, "machin", nil)
assert noeud_1.est_vide() is False
assert noeud_1.taille() == 1
assert noeud_1.hauteur() == 1

noeud_2 = ArbreBinaire(nil, "chose", nil)

arbre_A1 = ArbreBinaire(noeud_2, "AA1", noeud_1)
noeud_3 = ArbreBinaire(nil, "3", nil)
arbre_A2 = ArbreBinaire(noeud_3, "AA2", arbre_A1)
assert arbre_A2.est_vide() is False

assert arbre_A1.taille() == 3, f"{arbre_A1.taille()}"
assert arbre_A1.hauteur() == 2, f"{arbre_A1.hauteur()}"

assert arbre_A2.taille() == 5, f"{arbre_A2.taille()}"
assert arbre_A2.hauteur() == 3, f"{arbre_A2.hauteur()}"