On crée tout d'abord la liste des booléens indicateurs de la présence de chaque lettre dans la phrase.

On parcourt ensuite la chaîne de caractères. Pour chaque caractère on bascule la valeur du booléen à `#!py True`.

La chaîne est donc un pangramme si, en fin de boucle, la liste `presences` ne contient que des `#!py True`. On le vérifie, ici, à l'aide d'une boucle.

Il aurait aussi été possible d'utiliser un dictionnaire. Dans ce cas, on n'aurait pas eu recours à la fonction `#!py indice` :

```python
def est_pangramme(phrase):
    presences = {}
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[caractere] = True

    return len(presences) == 26
```


Notons enfin que, dans le cas d'une liste de booléens, la fonction `all` permet de tester que chacun d'entre eux vaut `#!y True`. On peut ainsi simplifier le dernier test :

```python
def est_pangramme(phrase):
    presences = [False] * 26
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    return all(presences)
```
