

# --------- PYODIDE:code --------- #

def indice(minuscule):
    return ord(minuscule) - ord("a")


def est_pangramme(phrase):
    ...

# --------- PYODIDE:corr --------- #

def indice(minuscule):
    return ord(minuscule) - ord("a")


def est_pangramme(phrase):
    presences = [False] * 26

    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    for i in range(26):
        if not presences[i]:
            return False

    return True

# --------- PYODIDE:tests --------- #

assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") is True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") is False
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w") is True
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") is False

# --------- PYODIDE:secrets --------- #

# Tests
assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") is True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") is False
assert (
    est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w")
    is True
)
assert (
    est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") is False
)

# Tests supplémentaires
from string import ascii_lowercase

assert est_pangramme("the quick brown fox jumps over The lazy dog.") is True
assert (
    est_pangramme(
        "hier, au zoo, j'ai vu dix guépards, cinq zébus, un yak et le wapiti fumer"
    )
    is True
)
# Tout dans l'ordre
assert est_pangramme(ascii_lowercase) is True
# Tout à l'envers
assert est_pangramme(ascii_lowercase[::-1]) is True
# Tout en double
assert est_pangramme(ascii_lowercase * 2) is True
# Sans le "a"
assert est_pangramme(" ".join(ascii_lowercase[1:])) is False
# Sans le "b"
assert est_pangramme(" ".join("a" + ascii_lowercase[2:])) is False
# Sans le "z"
assert est_pangramme(" ".join(ascii_lowercase[:-1])) is False
