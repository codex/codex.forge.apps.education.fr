---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Reconnaître un pangramme
tags:
    - booléen
    - dictionnaire
difficulty: 220
---


Un *pangramme* est une phrase contenant toutes les lettres de l'alphabet.

Il existe dans l'alphabet latin utilisé en français 26 lettres minuscules « usuelles ». Si l'on tient compte des lettres accentuées et des ligatures (*à*, *â*, ..., *ç*, *æ* et *œ*), le décompte passe à 42 caractères minuscules distincts.

**On se limite dans cet exercice aux lettres minuscules usuelles de l'alphabet : *a*, *b*, *c*, ..., *z*.**

Un exemple classique de pangramme est « *portez ce vieux whisky au juge blond qui fume* ». Chaque lettre y apparaît **au moins une fois**.

Par contre « *portez __un__ vieux whisky au juge blond qui fume* » n'est pas un pangramme ; il manque la lettre *c*.

Vous devez écrire une fonction `est_pangramme` qui :

* prend en paramètre une chaîne de caractères `phrase`,
* renvoie le booléen indiquant si cette chaîne est un pangramme ou non.

On fournit la fonction `indice` qui prend en argument une lettre entre `#!py "a"` et `#!py "z"` et renvoie son indice dans l'alphabet (en débutant à `#!py 0` pour `#!py "a"`). **Il n'est pas indispensable de l'utiliser**.

???+ example "Exemples"

    ```pycon title=""
    >>> indice("a")
    0
    >>> indice("z")
    25
    >>> est_pangramme("portez ce vieux whisky au juge blond qui fume !")
    True
    >>> est_pangramme("portez un vieux whisky au juge blond qui fume !")
    False
    >>> est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w")
    True
    >>> est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w")
    False
    ```

{{ IDE('exo', MAX=5) }}


??? tip "Astuce"

    On pourra utiliser une structure de données, une liste de booléens ou un dictionnaire par exemple, afin de garder trace des lettres lues lors du parcours de la chaîne de caractères.