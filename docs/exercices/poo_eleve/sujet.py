# --- hdr, constructeur_ --- #
class Eleve:
    pass


# --- vide, constructeur_ --- #
class Eleve: 
    ...


# --- exo, constructeur_ --- #
"""  # skip
class Eleve:
    def __init__(self, ..., ..., ...):
        self.... = ...
        self.... = ...
        self.... = ...
        self.... = ...
"""  # skip


# --- corr, constructeur_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}


# --- tests, constructeur_ --- #
albert = Eleve("Albert", "Einstein", "Te2")
assert albert.prenom == "Albert"
assert albert.nom == "Einstein"
assert albert.classe == "Te2"
assert albert.moyennes == {}
# --- secrets, constructeur_ --- #
gustave = Eleve("Gustave", "Flaubert", "Te1")
assert gustave.prenom == "Gustave", "Erreur sur l'attribut prenom"
assert gustave.nom == "Flaubert", "Erreur sur l'attribut nom"
assert gustave.classe == "Te1", "Erreur sur l'attribut classe"
assert gustave.moyennes == {}, "Erreur sur l'attribut moyennes"


# --- hdr, modif_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___


# --- vide, modif_ --- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute le constructeur

    # Vous ne devez saisir QUE la méthode `modifie_moyenne`

    ...


# --- exo, modif_ --- #
"""  # skip
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute le constructeur
    
    # Vous ne devez saisir QUE la méthode `modifie_moyenne`
    
    def ...(..., ..., ...):
        ...[...] = ...

        
"""  # skip


# --- corr, modif_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne


# --- tests, modif_ --- #
ajout_methodes_precedentes(Eleve)
carl = Eleve("Carl Friedrich", "Gauss", "Te3")
carl.modifie_moyenne("arithmétique", 20)
carl.modifie_moyenne("chimie", 12)
assert carl.moyennes == {"arithmétique": 20, "chimie": 12}
carl.modifie_moyenne("chimie", 13)
assert carl.moyennes == {"arithmétique": 20, "chimie": 13}
# --- secrets, modif_ --- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
assert albert.moyennes == {
    "physique": 20,
    "maths": 20,
    "allemand": 17,
}, "Erreur lors de l'ajout de moyennes"
albert.modifie_moyenne("allemand", 20)
assert albert.moyennes == {
    "physique": 20,
    "maths": 20,
    "allemand": 20,
}, "Erreur lors de l'ajout de moyennes"


# --- hdr, moyenne_de_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_


# --- vide, moyenne_de_ --- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`

    # Vous ne devez saisir QUE la méthode `moyenne_de`

    ...

# --- exo, moyenne_de_ --- #
"""  # skip
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    
    # Vous ne devez saisir QUE la méthode `moyenne_de`
        
    def ...(..., ...):
        if ... in ...:
            return ...
        else:
            return ...

"""  # skip


# --- corr, moyenne_de_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None


# --- rem, moyenne_de_ --- #
"""  # skip
Notons, qu'en Python, une fonction sans `#!py return` renvoie par défaut `#!py None`.

Ainsi le code précédent pourrait être écrit sans le `#!py else` qui serait géré implicitement :

```python title=""
def moyenne_de(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]
``` 
"""  # skip


# --- tests, moyenne_de_ --- #
ajout_methodes_precedentes(Eleve)
donald = Eleve("Donald", "Knuth", "Te7")
donald.modifie_moyenne("informatique", 20)
donald.modifie_moyenne("musique", 13)
assert donald.moyenne_de("informatique") == 20
assert donald.moyenne_de("musique") == 13
assert donald.moyenne_de("lancer de javelot") is None
# --- secrets, moyenne_de_ --- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
assert (
    albert.moyenne_de("physique") == 20
), "Erreur lors de la récupération d'une moyenne"
assert (
    albert.moyenne_de("japonais") is None
), "Erreur lors de la récupération d'une moyenne"


# --- hdr, moyenne_simple_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass

    def moyenne_simple(self):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def _moyenne_de_(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_
    Classe.moyenne_de = _moyenne_de_


# --- vide, moyenne_simple_ --- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`

    # Vous ne devez saisir QUE la méthode `moyenne_simple`

    ...


# --- exo, moyenne_simple_ --- #
"""  # skip
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`

    # Vous ne devez saisir QUE la méthode `moyenne_simple`

    def moyenne_simple(self):
        if ... > 0:
            total = ...
            for moy in self.moyennes....():
                total += ...
            return ... / ...
        else:
            return ...

"""  # skip


# --- corr, moyenne_simple_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)
        else:
            return None


# --- tests, moyenne_simple_ --- #
ajout_methodes_precedentes(Eleve)
jane = Eleve("Jane", "Goodall", "Te3")
jane.modifie_moyenne("éthologie", 20)
jane.modifie_moyenne("théorie des groupes", 14)
assert jane.moyenne_simple() == 17.0
# --- secrets, moyenne_simple_ --- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
assert albert.moyenne_simple() == 19.0, "Erreur lors du calcul d'une moyenne simple"
evariste = Eleve("Evariste", "Galois", "Te3")
assert evariste.moyenne_simple() is None, "Erreur lors du calcul d'une moyenne simple"
# --- rem, moyenne_simple_ --- #
"""  # skip
Comme indiqué dans l'exercice précédent, le `#!py else: return None` peut être omis.

Ainsi le code précédent est équivalent à :

```python title=""
def moyenne_simple(self):
    if len(self.moyennes) > 0:
        total = 0
        for moy in self.moyennes.values():
            total += moy
        return total / len(self.moyennes)
``` 
"""  # skip


# --- hdr, moyenne_ponderee_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass

    def moyenne_simple(self):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def _moyenne_de_(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]


def _moyenne_simple_(self):
    if len(self.moyennes) > 0:
        total = 0
        for moy in self.moyennes.values():
            total += moy
        return total / len(self.moyennes)


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_
    Classe.moyenne_de = _moyenne_de_
    Classe.moyenne_simple = _moyenne_simple_


# --- vide, moyenne_ponderee_ --- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`
    # * la méthode `moyenne_simple`

    # Vous ne devez saisir QUE la méthode `moyenne_ponderee`

    ...


# --- exo, moyenne_ponderee_ --- #
"""  # skip
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`
    # * la méthode `moyenne_simple`

    # Vous ne devez saisir QUE la méthode `moyenne_ponderee`

    def moyenne_ponderee(self, coeffs):
        if ...:
            total = ...
            total_coeffs = ...
            for matiere in ...:
                if ... not in ...:
                    raise ValueError("Il manque au moins un coefficient")
                total += ...
                total_coeffs += ...
            return ... / ...
        else:
            return ...

            
"""  # skip


# --- corr, moyenne_ponderee_ --- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)

    def moyenne_ponderee(self, coeffs):
        if len(self.moyennes) > 0:
            total = 0
            total_coeffs = 0
            for matiere in self.moyennes:
                if matiere not in coeffs:
                    raise ValueError("Il manque au moins un coefficient")
                total += self.moyennes[matiere] * coeffs[matiere]
                total_coeffs += coeffs[matiere]
            return total / total_coeffs
        else:
            return None


# --- tests, moyenne_ponderee_ --- #
ajout_methodes_precedentes(Eleve)
margaret = Eleve("Margaret", "Hamilton", "Te5")
margaret.modifie_moyenne("études spatiales", 20)
margaret.modifie_moyenne("maths", 14)
coeffs = {"études spatiales": 1, "maths": 0.5}
assert margaret.moyenne_ponderee(coeffs) == 18.0
# --- secrets, moyenne_ponderee_ --- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
coeffs = {"physique": 1, "maths": 1, "allemand": 0.5}
assert (
    albert.moyenne_ponderee(coeffs) == 19.4
), "Erreur lors du calcul d'une moyenne pondérée"
coeffs = {"physique": 1, "maths": 1}
try:
    albert.moyenne_ponderee(coeffs)
except ValueError as e:
    pass
except:
    raise AssertionError("Un coefficient manquant doit générer une `ValueError`")
# --- rem, moyenne_ponderee_ --- #
"""  # skip
Comme indiqué dans l'exercice précédent, le `#!py else: return None` peut être omis.

Ainsi le code précédent est équivalent à :

```python title=""
def moyenne_ponderee(self, coeffs):
    if len(self.moyennes) > 0:
        total = 0
        total_coeffs = 0
        for matiere in self.moyennes:
            if matiere not in coeffs:
                raise ValueError("Il manque au moins un coefficient")
            total += self.moyennes[matiere] * coeffs[matiere]
            total_coeffs += coeffs[matiere]
        return total / total_coeffs
``` 
"""  # skip