# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_


# --------- PYODIDE:code --------- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    
    # Vous ne devez saisir QUE la méthode `moyenne_de`
        
    def ...(..., ...):
        if ... in ...:
            return ...
        else:
            return ...



# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None


# --------- PYODIDE:tests --------- #
ajout_methodes_precedentes(Eleve)
donald = Eleve("Donald", "Knuth", "Te7")
donald.modifie_moyenne("informatique", 20)
donald.modifie_moyenne("musique", 13)
assert donald.moyenne_de("informatique") == 20
assert donald.moyenne_de("musique") == 13
assert donald.moyenne_de("lancer de javelot") is None
# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
assert (
    albert.moyenne_de("physique") == 20
), "Erreur lors de la récupération d'une moyenne"
assert (
    albert.moyenne_de("japonais") is None
), "Erreur lors de la récupération d'une moyenne"


