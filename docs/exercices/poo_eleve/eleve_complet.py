"""
On souhaite écrire une classe `Eleve`.

Un `Eleve` possède trois attributs :

* son nom (str) ;
* sa classe (str) ;
* ses moyennes (dict). Ce dictionnaire est de la forme `{"matière": moyenne}`,
  la matière est un `str` et la moyenne un `float`.

Un `Eleve` possèd plusieurs méthodes :

* un constructeur qui crée un élève à partir de son nom et de sa classe. 
  Le dictionnaire de moyennes est initialement vide ;
* une méthode `moyenne_de(matiere: str) -> float` qui prend en paramètre une matière (str)
  et renvoie la moyenne correspondante. 
  La méthode génère une `ValueError` si la matière n'est pas dans le dictionnaire ;
* une méthode `moyenne_generale() -> float` qui calcule et renvoie la moyenne générale 
  (moyenne des moyennes) de cet élève.
  La méthode renverra `None` si l'élève n'a aucune moyenne ;
* une méthode `moyenne_ponderee(coeffs: dict[str, float]) -> float` qui calcule et renvoie
  la moyenne pondérée en appliquant le coefficients passés en paramètre.
  Le dictionnaire `coeffs` est de la forme `{"matière": coeff}` où `matire` est un `str`
  et `coeff` un `float`.
  Si une matière pour laquelle l'élève possède une moyenne n'a pas de coefficient associé dans `coeffs`,
  la méthode génère une erreur
  La méthode renvoie `None` si l'élève n'a aucune moyenne
  

  
On n'oubliera pas de saisir une documentation générale de la classe et des méthodes
"""


class Eleve:
    """
    Un `Eleve` possède trois attributs :
    * son nom (str) ;
    * sa classe (str) ;
    * ses moyennes (dict). Ce dictionnaire est de la forme `{"matière": moyenne}`,
    la matière est un `str` et la moyenne un `float`.
    """

    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def moyenne_de(self, matiere):
        """
        Prend en paramètre une matière (str) et renvoie la moyenne correspondante.
        La méthode génère une `ValueError` si la matière n'est pas dans le dictionnaire
        """
        if matiere not in self.moyennes:
            raise ValueError("L'élève n'a pas de moyenne dans cette matière")
        return self.moyennes[matiere]

    def moyenne_generale(self):
        """
        Calcule et renvoie la moyenne générale (moyenne des moyennes) de cet élève.
        La méthode renvoie `None` si l'élève n'a aucune moyenne
        """
        if self.moyennes != {}:
            total = 0
            for x in self.moyennes.values():
                total += x
            return total / len(self.moyennes)

    def moyenne_ponderee(self, coeffs):
        """
        Calcule et renvoie la moyenne pondérée en appliquant le coefficients passés en paramètre.
        Le dictionnaire `coeffs` est de la forme `{"matière": coeff}` où `matire` est un `str`
        et `coeff` un `float`.
        Si une matière pour laquelle l'élève possède une moyenne n'a pas de coefficient associé dans `coeffs`,
        la méthode génère une erreur
        La méthode renvoie `None` si l'élève n'a aucune moyenne
        """
        for m in self.moyennes:
            if m not in coeffs:
                raise ValueError('Le coefficient de "{m}" n\'est pas fourni')

        if self.moyennes != {}:
            total = 0
            total_coeff = 0
            for m in self.moyennes:
                total += self.moyennes[m] * coeffs[m]
                total_coeff += coeffs[m]

            return total / total_coeff


# Tests
albert = Eleve("Albert", "Einstein", "Te2")
assert albert.prenom == "Albert", "Erreur sur l'attribut nom"
assert albert.nom == "Einstein", "Erreur sur l'attribut nom"
assert albert.classe == "Te2", "Erreur sur l'attribut classe"
assert albert.moyennes == {}, "Erreur sur l'attribut moyennes"
albert.moyennes["maths"] = 20
albert.moyennes["physique"] = 20
albert.moyennes["allemand"] = 19
albert.moyennes["français"] = 5
assert albert.moyennes == {
    "maths": 20,
    "physique": 20,
    "allemand": 19,
    "français": 5,
}, "Erreur sur l'attribut moyennes"
assert albert.moyenne_de("maths") == 20, "Erreur sur la méthode moyenne_de"
assert albert.moyenne_generale() == 16, "Erreur sur la méthode moyenne_generale"
coeffs = {
    "maths": 1,
    "physique": 1,
    "allemand": 1,
    "français": 1,
}
assert (
    albert.moyenne_ponderee(coeffs) == albert.moyenne_generale()
), "Erreur sur la méthode moyenne_ponderee"
coeffs = {
    "maths": 3,
    "physique": 3,
    "allemand": 2,
    "français": 0,
}
assert (
    albert.moyenne_ponderee(coeffs) == 19.75
), "Erreur sur la méthode moyenne_ponderee"
