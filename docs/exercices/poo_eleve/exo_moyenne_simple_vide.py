# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass

    def moyenne_simple(self):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def _moyenne_de_(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_
    Classe.moyenne_de = _moyenne_de_


# --------- PYODIDE:code --------- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`

    # Vous ne devez saisir QUE la méthode `moyenne_simple`

    ...


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)
        else:
            return None


# --------- PYODIDE:tests --------- #
ajout_methodes_precedentes(Eleve)
jane = Eleve("Jane", "Goodall", "Te3")
jane.modifie_moyenne("éthologie", 20)
jane.modifie_moyenne("théorie des groupes", 14)
assert jane.moyenne_simple() == 17.0
# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
assert albert.moyenne_simple() == 19.0, "Erreur lors du calcul d'une moyenne simple"
evariste = Eleve("Evariste", "Galois", "Te3")
assert evariste.moyenne_simple() is None, "Erreur lors du calcul d'une moyenne simple"
