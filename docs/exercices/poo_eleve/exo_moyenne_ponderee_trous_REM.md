Comme indiqué dans l'exercice précédent, le `#!py else: return None` peut être omis.

Ainsi le code précédent est équivalent à :

```python title=""
def moyenne_ponderee(self, coeffs):
    if len(self.moyennes) > 0:
        total = 0
        total_coeffs = 0
        for matiere in self.moyennes:
            if matiere not in coeffs:
                raise ValueError("Il manque au moins un coefficient")
            total += self.moyennes[matiere] * coeffs[matiere]
            total_coeffs += coeffs[matiere]
        return total / total_coeffs
``` 
