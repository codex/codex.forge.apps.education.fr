# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        pass

    def modifie_moyenne(self, matiere, moyenne):
        pass

    def moyenne_de(self, matiere):
        pass

    def moyenne_simple(self):
        pass


def ___init___(self, prenom, nom, classe):
    self.prenom = prenom
    self.nom = nom
    self.classe = classe
    self.moyennes = {}


def _modifie_moyenne_(self, matiere, moyenne):
    self.moyennes[matiere] = moyenne


def _moyenne_de_(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]


def _moyenne_simple_(self):
    if len(self.moyennes) > 0:
        total = 0
        for moy in self.moyennes.values():
            total += moy
        return total / len(self.moyennes)


def ajout_methodes_precedentes(Classe):
    Classe.__init__ = ___init___
    Classe.modifie_moyenne = _modifie_moyenne_
    Classe.moyenne_de = _moyenne_de_
    Classe.moyenne_simple = _moyenne_simple_


# --------- PYODIDE:code --------- #
class Eleve:
    # L'appel `ajout_methodes_precedentes(Eleve)` ajoute :
    # * le constructeur
    # * la méthode `modifie_moyenne`
    # * la méthode `moyenne_de`
    # * la méthode `moyenne_simple`

    # Vous ne devez saisir QUE la méthode `moyenne_ponderee`

    ...


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)

    def moyenne_ponderee(self, coeffs):
        if len(self.moyennes) > 0:
            total = 0
            total_coeffs = 0
            for matiere in self.moyennes:
                if matiere not in coeffs:
                    raise ValueError("Il manque au moins un coefficient")
                total += self.moyennes[matiere] * coeffs[matiere]
                total_coeffs += coeffs[matiere]
            return total / total_coeffs
        else:
            return None


# --------- PYODIDE:tests --------- #
ajout_methodes_precedentes(Eleve)
margaret = Eleve("Margaret", "Hamilton", "Te5")
margaret.modifie_moyenne("études spatiales", 20)
margaret.modifie_moyenne("maths", 14)
coeffs = {"études spatiales": 1, "maths": 0.5}
assert margaret.moyenne_ponderee(coeffs) == 18.0
# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
coeffs = {"physique": 1, "maths": 1, "allemand": 0.5}
assert (
    albert.moyenne_ponderee(coeffs) == 19.4
), "Erreur lors du calcul d'une moyenne pondérée"
coeffs = {"physique": 1, "maths": 1}
try:
    albert.moyenne_ponderee(coeffs)
except ValueError as e:
    pass
except:
    raise AssertionError("Un coefficient manquant doit générer une `ValueError`")
