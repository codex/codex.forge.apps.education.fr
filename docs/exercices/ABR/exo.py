# --------- PYODIDE:env --------- #
# pylint: disable=C0103, C0116, C0115, E0401, E0102, C0415
import p5, js

from lib_draw import AffichageAbrCompact

# Passer à True pour que la complexité en temps de `ABR.contient` devienne linéaire, durant
# les tests de performances:
OVERRIDE_SLOW = False


afficheur = AffichageAbrCompact(
    target_id = "visu_arbre",
    est_vide = lambda ab: not ab.racine,
    gauche = lambda ab: ab.racine.gauche,
    droit  = lambda ab: ab.racine.droit,
    valeur = lambda ab: ab.racine.element,
    insere = lambda ab,v: ab.insere(v),
    creer_vide = lambda: ABR(),
    taille_fenetre = (750, 600),
)

insere_abr = afficheur.insere_et_affiche


# --------- PYODIDE:code --------- #

class Noeud:

    def __init__(self, gauche, element, droit):
        self.gauche = gauche
        self.element = element
        self.droit = droit


class ABR:

    def __init__(self):
        self.racine = None


    def est_vide(self) -> bool:
        return ...


    def insere(self, element) -> None:
        if self.est_vide():     # On insère element en mettant à jour racine
            ...
        elif element < ... :    # insertion à gauche
            ...
        elif ... < element :    # ...?
            ...
        else:
            pass # Notez qu'il n'y a rien à faire ici car les doublons sont ignorés !


    def contient(self, element) -> bool:
        ...


    def infixe(self) -> str:
        if self.est_vide():
            return "|"

        a = ...
        b = ...
        c = ...
        return str(a) + str(b) + str(c)









# --------- PYODIDE:corr --------- #

class Noeud:

    def __init__(self, gauche, element, droit):
        self.gauche = gauche
        self.element = element
        self.droit = droit


class ABR:

    def __init__(self):
        self.racine = None


    def est_vide(self) -> bool:
        return self.racine is None


    def insere(self, element) -> None:
        if self.est_vide():
            self.racine = Noeud(ABR(), element, ABR())
        elif element < self.racine.element :
            self.racine.gauche.insere(element)
        elif self.racine.element < element :
            self.racine.droit.insere(element)
        else:
            pass # Notez qu'il n'y a rien à faire ici car les doublons sont ignorés !


    def contient(self, element) -> bool:
        if self.est_vide():
            return False
        elif element < self.racine.element :
            return self.racine.gauche.contient(element)
        elif self.racine.element < element :
            return self.racine.droit.contient(element)
        else:
            return True     # Ici, self.racine.element vaut element !


    def infixe(self) -> str:
        if self.est_vide():
            return "|"

        a = self.racine.gauche.infixe()
        b = self.racine.element
        c = self.racine.droit.infixe()
        return f"{ a }{ b }{ c }"
            # Utilisation d'une f-string (plus élégant que la concaténation)











# --------- PYODIDE:tests --------- #

print('Teste est_vide & insere')
nombres = ABR()
assert nombres.est_vide()

valeurs = [3, 1, 7, 9]
insere_abr(valeurs, nombres)

assert nombres.est_vide() is False, "L'arbre ne devrait plus être vide après insertion de valeurs"

racine = nombres.racine
assert isinstance(racine, Noeud), f"La racine devrait être une instance de Noeud. Trouvé: { racine }"
v = nombres.racine.element
assert v == valeurs[0], f"La racine devrait porter la valeur { valeurs[0] } mais est { v }"


print('Teste contient')
for v in (1,7):
    assert nombres.contient(v), f"{ v } n'a pas été trouvé dans l'ABR"
for v in (8, -13, 111):
    assert nombres.contient(v) is False, f"nombres.contient({ v }) devrait renvoyer False"

print('Teste infixe')
chaine = nombres.infixe()
correct = '|1|3|7|9|'
assert chaine == correct, f"{ chaine } devrait être { correct }"


print("Teste n'insere pas les doublons")
insere_abr(valeurs, nombres)

chaine = nombres.infixe()
correct = '|1|3|7|9|'
assert chaine == correct, f"{ chaine } devrait toujours être { correct }"



print('Teste avec des chaînes de caractères')
fruits_ranges = ABR()
assert fruits_ranges.est_vide()

panier = ["kiwi", "pomme", "abricot", "mangue", "poire"]
insere_abr(panier, fruits_ranges)

assert fruits_ranges.est_vide() is False
assert fruits_ranges.contient("pomme")
assert fruits_ranges.contient("cerise") is False
assert fruits_ranges.infixe() == '|abricot|kiwi|mangue|poire|pomme|'










# --------- PYODIDE:secrets --------- #

@auto_run       # pylint: disable=E0602
async def _():

    import matplotlib.pyplot as plt
    from time import time
    from random import randrange as rand, sample, choice
    from contextlib import asynccontextmanager
    from lib_tests import build_announcer, timer

    key = "IlSigneD'1Z..."
    annonce = build_announcer(key)




    # Logistique pour les tests aléatoires :

    async def test_with(distrib):
        """ Teste sur une distribution NON VIDE de valeurs """

        s = set(distrib)
        abr = ABR()
        assert abr.est_vide()
        insere_abr(distrib, abr)
        await js.sleep(200)

        n = min(len(distrib),6)
        vs = sample(distrib, n) + [rand(-100, 100) for _ in range(6)]
        for v in vs:
            exp = v in s
            act = abr.contient(v)
            assert act==exp, f"abr.contient({ v }): { act } devrait être { exp }"

        act = abr.infixe()
        exp = f"|{ '|'.join(map(str, sorted(s))) }|"
        assert act==exp, f"{act!r} devrait être {exp!r}"


    def ascendant():
        n = 1 + rand(15)
        v = rand(-100,100)
        return [ (v:=v+rand(5)) for _ in range(n)]

    def descendant():
        n = 1 + rand(15)
        v = rand(-100,100)
        return [ (v:=v-rand(5)) for _ in range(n)]

    def aleatoire():
        n = 1 + rand(15)
        return [ rand(-100,100) for _ in range(n)]





    # Tests fixes en plus des tests publics :

    abr = ABR()
    async with annonce("Teste contient sur l'ABR vide"):
        assert abr.contient(4) is False

    async with annonce("Teste infixe sur l'ABR vide"):
        assert abr.infixe() == '|'


    # Tests aléatoires :

    async with annonce("Tests aléatoires"):
        profile = (ascendant, descendant, aleatoire, aleatoire, aleatoire, aleatoire)
        for _ in range(30):
            distrib = choice(profile)()
            # terminal_message(key, len(distrib)!=len(set(distrib)), distrib)
            await test_with(distrib)








    # Tests de performances de la méthode contient :
    #
    #   1) Construit la courbe du profile de performances attendues en fonction de la hauteur de
    #      l'arbre, en utilisant des arbres linéaires (peigne à droite)
    #      => les performances sont proportionnelles à h, même avec une implantation en O(N)
    #
    #   2) Mesure les performances de la méthode de l'utilisateur sur un ABR équilibré.
    #      La taille est choisie relativement petite (500 valeurs => h environ 9), de manière à
    #      ce qu'une mauvaise implantation puisse tout de même finir en temps raisonnable.
    #
    #   3) Vérifie que le temps nécessaire est suffisamment proche du comportement attendu pour O(h).
    #
    # - Les performances sont affichée dans un graphique sous l'IDE.
    # - Il est possible de vérifier le comportement des deux types d'implantations en passant la
    #   variable globale `OVERRIDE_SLOW` à `True` depuis l'éditeur de code (la variable est définie
    #   dans la section env).


    if OVERRIDE_SLOW:
        def contient(self, element):
            return not self.est_vide() and (
                self.racine.element==element
                or self.racine.gauche.contient(element)
                or self.racine.droit.contient(element)
            )
        ABR.contient = contient



    PyodidePlot().target()      # pylint: disable=E0602

    @asynccontextmanager
    async def info_process(log:str, intermediate_msgs=False):
        terminal_message(key, log, new_line=intermediate_msgs)      # pylint: disable=E0602
        await js.sleep()
        start = time()
        yield
        stop = time()
        profiling_time = round(stop-start, 3)
        timing = f" (réalisé en { profiling_time }s)"
        terminal_message(key, timing, format='info')                # pylint: disable=E0602



    def time_contient(abr, calls, truth_predicate):
        start = time()
        for i,n in enumerate(calls):
            assert truth_predicate(abr.contient(n), n)
        stop = time()
        return round(stop-start, 3)


    v,STEP = 0,30
    abr = ABR()

    BATCHES = 5
    N_REQUESTS = 2000
    o_h = []
    H_MAX = 20
    xs = range(H_MAX)

    async with info_process("Établissement du profil de performance attendu", intermediate_msgs=True):
        for _ in xs:
            abr.insere(v)
            timings = []
            for _ in range(BATCHES):
                calls = [v + rand(v-(STEP-5), v+STEP-5) for _ in range(N_REQUESTS)]
                t = time_contient(abr, calls, lambda b, n: b == (n==v))
                timings.append(t)
            v += STEP
            _,*timings,_ = timings
            o_h.append( round( sum(timings)/len(timings), 3) )
            terminal_message(key, '.', new_line=False)          # pylint: disable=E0602
            await js.sleep()



    # 2) Création de l'ABR pour tester les performances

    def lin_build(data, l, h):
        """ Construit un ABR équilibrée à partir d'une liste triée """
        m = l+h>>1
        abr = ABR()
        g = ABR() if l==m else lin_build(data, l, m-1)
        d = ABR() if m==h else lin_build(data, m+1, h)
        abr.racine = Noeud(g, data[m], d)
        return abr

    SIZE = 500
    H_APPROX = SIZE.bit_length()


    title = f"Construction d'un ABR équilibré (N={SIZE}) et des tests ({N_REQUESTS} valeurs)"
    async with info_process(title):
        v = -rand(SIZE//5)
        data = [ (v:=v+1+rand(10)) for _ in range(SIZE) ]
        abr = lin_build(data, 0, len(data)-1)

        low,high = data[0]-1000, data[-1]+1000
        vs = set(data)
        calls = [ rand(low,high) for _ in range(N_REQUESTS)]


    async with info_process(f"Tests de performances ({N_REQUESTS} valeurs)"):
        calls = [v + rand(v-(STEP-5), v+STEP-5) for _ in range(N_REQUESTS)]
        req_t = time_contient(abr, calls, lambda b,n: b == (n in vs))


    plt.plot(xs, o_h, '-', [H_APPROX], [req_t], 'ro')
    y_max = int( plt.axis()[-1] * 1.02 )       # '(convert numpy float64)'
    plt.xticks( [*range(0, y_max, 2)] )
    plt.title(
        "Mesures: $temps~=~f(hauteur)$\n"
        "Le point rouge devrait être proche de la courbe bleue"
    )
    plt.show()

    async with annonce("Vérification des performances pour abr.contient(v)"):
        should_be = o_h[H_APPROX]
        margin_ratio = 2
        assert req_t <= should_be * margin_ratio, f"""\
Fonction abr.contient(...) trop lente.
    La complexité en temps devrait être O(h), alors qu'elle est O(N):
    Le point rouge sur la figure ci-dessous devrait se trouver à proximité de la courbe bleue.
    Temps attendu: environ { should_be }s
    Temps autorisé: { should_be * margin_ratio }s
    Temps mesuré: { req_t }s
"""
