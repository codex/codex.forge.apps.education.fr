---
author:
    - Frédéric Zinelli
hide:
    - navigation
    - toc
title: Arbre binaire de recherche (2 classes)
tags:
    - à trous
    - arbre binaire
    - ABR
    - récursivité
    - programmation orientée objet
difficulty: 301
maj: 01/02/2025
ides:
    forbid_secrets_without_corr_or_REMs: false
---


!!! tip inline end ""

    ```mermaid
    graph TB;
        A((8))-->B((3))
        A-->C((10))
        B-->D((1))
        B-->E((6))
        C-->F((9))
        C-->G((14))
        E-->H((4))
        E-->I((7))
    ```


On donne une partie de la classe `ABR` pour implémenter les **A**rbres **B**inaires de **R**echerche sans doublon : un ensemble fini de nœuds, éventuellement vide, organisés de manière hiérarchique. C'est une arborescence d'éléments comparables.

Un ABR est une structure de nature récursive :

- Soit c'est un ABR vide,
- Soit il possède une valeur et a deux enfants qui sont aussi des ABRs.
<br>Dans ce dernier cas, il existe une contrainte concernant les valeurs présentes dans les sous-arbres gauche et droit, par rapport à la valeur en cours :
    - Toutes les valeurs présentes dans le sous-arbre gauche sont inférieures à la valeur en cours.
    - Toutes les valeurs présentes dans le sous-arbre droit sont supérieures à la valeur en cours.

On considérera ici des ABRs **sans doublons** : il ne sera pas possible d'y insérer plusieurs fois la même valeur.

---

## Choix d'implémentation

Dans cet exercice on choisit d'implémenter les ABR à l'aide de deux classes :


### Attributs

```python title=""
class ABR:
    racine: None|Noeud

class Noeud:
    element: Comparable
    gauche: ABR
    droit: ABR
```

<br>

- L'ABR vide est une instance de la classe `ABR` où l'attribut racine prend la valeur `#!py None`.
- Un ABR non vide est une instance de la classe `ABR` avec une instance de `Noeud` en racine.
<br> C'est l'instance de `Noeud` qui porte la valeur et les enfant `gauche` et `droit` :
    - `gauche` : le sous-ABR à gauche
    - `element` : un élément comparable aux autres (des entiers, des chaînes de caractères, ...)
    - `droit` : le sous-ABR à droit


### Constructeurs

* Les objets `ABR` sont créés vides avec `ABR()`.

* Les objets `Noeud` sont créés avec une valeur et deux sous-ABR vides: `Noeud(ABR(), element, ABR())`.


### Contrats & Méthodes

* Les objets `ABR` peuvent être mutés pour ajouter de nouvelles valeurs à la structure.

* Les objets `Noeud` n'ont pas de méthodes implémentées.

* Les objets `ABR` ont, ou devront, implémenter les méthodes suivantes :
    - `abr.est_vide() -> bool`, qui indique si l'ABR contient des valeurs ou non.
    - `abr.insere(v) -> None`, qui permet d'ajouter la valeur `v` à un emplacement approprié dans l'ABR en cours.
    - `abr.contient(v) -> bool`, qui permet de savoir si l'ABR en cours contient la valeur `v` ou pas.
    - `abr.infixe() -> str`, qui renvoie une chaîne de caractères représentant le contenu de l'ABR, traversé en mode infixe.

<br>

!!! tip "Conséquences"

    Ce choix d'implémentation à deux classes permet d'utiliser le cas de base "arbre vide", dans les implémentations récursives.

    Il présente cependant un léger désavantage concernant l'impact sur la mémoire, car pour stocker une valeur, il faut systématiquement deux objets (+ tous les arbres vides des feuilles de l'ABR).



<br>

???+ example "Exemples"

    ```pycon title=""
    >>> nombres = ABR()
    >>> nombres.est_vide()
    True
    ```

    <br>

    ```pycon title=""
    >>> for x in [3, 7,  9, 1, 9]:
    ...     nombres.insere(x)
    >>> nombres.est_vide()
    False
    >>> nombres.contient(7)
    True
    >>> nombres.contient(8)
    False
    >>> nombres.infixe()
    '|1|3|7|9|'
    ```

    <br>

    ```pycon title=""
    >>> panier = ["kiwi", "pomme", "abricot", "mangue", "poire"]
    >>> fruits_ranges = ABR()
    >>> for fruit in panier:
    ...     fruits_ranges.insere(fruit)
    >>> fruits_ranges.contient("pomme")
    True
    >>> fruits_ranges.contient("cerise")
    False
    >>> fruits_ranges.infixe()
    '|abricot|kiwi|mangue|poire|pomme|'
    ```


## Implémentation

Compléter les méthodes de la classe ABR ci-dessous, en prenant soin de respecter la complexité en temps attendue pour la méthode `contient`.

Quelques remarques concernant les tests :

* Dans les tests, la fonction `insere_abr(lst, abr)` ajoute toutes les valeurs de la liste `lst` dans `abr`, en utilisant sa méthode `abr.insere(element)`, dans l'ordre des valeurs dans `lst`.

* Durant les tests, le dernier ABR construit est systématiquement affiché dans la zone ci-dessous (il est conseillé de travailler en mode "2 colonnes". Les arbres des tests de performances ne sont pas affichés).

* Une fois que toutes les méthodes sont implantées et renvoient des réponses correctes, la complexité en temps de la méthode `abr.contient(element)` est testée, pour vérifier qu'elle respecte les spécifications attendues pour un ABR. Ces tests peuvent être relativement long...

<br>

{{ remarque("typage") }}

<br>


{{ figure('visu_arbre', admo_title="Dernier ABR créé:") }}

{{ IDE('exo', STD_KEY="IlSigneD'1Z...") }}

{{ figure(admo_title="Tests de performances") }}
