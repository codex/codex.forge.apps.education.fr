---
author: Nicolas Revéret
difficulty: 340
hide:
    - navigation
    - toc
title: Nombre de 1
tags:
    - diviser pour régner
    - dichotomie
---

On considère un tableau ne contenant que des `#!py 0` et des `#!py 1`. Ce tableau est trié dans l'ordre croissant et il est possible qu'il ne contienne que des `#!py 0` ou que des `#!py 1`.

On souhaite déterminer combien ce tableau compte-t-il de `#!py 1` ?

Écrire la fonction `compte_uns` qui prend en paramètre un tel tableau et renvoie le nombre de `#!py 1` qu'il contient.


???+ warning "Attention"

    Certains des tableaux utilisés dans les tests sont très grands. Une méthode de coût linéaire sera inefficace face à ceux-ci.

    On limite donc le nombre de lectures dans chaque tableau à 500. Passé cette valeur maximale, tout nouvel accès provoquera une erreur.

    On rappelle à ce titre que le tableau est trié...

???+ example "Exemples"

    ```pycon title=""
    >>> compte_uns([0, 1, 1, 1])
    3
    >>> compte_uns([0, 0, 0, 1, 1])
    2
    >>> compte_uns([0] * 200)
    0
    >>> compte_uns([1] * 300)
    300
    >>> compte_uns([0] * 200 + [1] * 500)
    500
    >>> compte_uns([])
    0
    ```

{{ IDE('exo', MAX=10) }}
