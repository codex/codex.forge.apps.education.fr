

# --------- PYODIDE:code --------- #

def compte_uns(tableau):
    ...

# --------- PYODIDE:corr --------- #

def compte_uns(tableau):
    if tableau == []:
        return 0

    if tableau[0] == 1:
        return len(tableau)

    debut = 0
    fin = len(tableau) - 1
    while debut <= fin:
        milieu = (debut + fin) // 2
        if tableau[milieu] == 0:
            debut = milieu + 1
        elif tableau[milieu - 1] == 1:
            fin = milieu - 1
        else:
            return len(tableau) - milieu

    return 0

# --------- PYODIDE:tests --------- #

assert compte_uns([0, 1, 1, 1]) == 3
assert compte_uns([0, 0, 0, 1, 1]) == 2
assert compte_uns([0] * 200) == 0
assert compte_uns([1] * 300) == 300
assert compte_uns([0] * 200 + [1] * 500) == 500
assert compte_uns([]) == 0

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from random import randrange


assert compte_uns([]) == 0, "Erreur avec le tableau vide"


class Tableau(list):
    def __init__(self, taille, premier_un):
        assert taille > 1_000, "La longueur doit être supérieure à 1000"
        assert taille > premier_un, "Le premier 1 doit être dans le tableau"
        self._taille = taille
        self._premier_un = premier_un
        self._compteur_lectures = 0
        self._lectures_max = 500

    def __len__(self):
        return self._taille

    def __eq__(self, autre):
        if self._taille != len(autre):
            return False

        for i in range(self._taille):
            if self.__getitem__(i) != autre[i]:
                return False

        return True

    def __getitem__(self, i):
        if type(i) is slice:
            indices = range(*i.indices(self._taille))
            return [self.__getitem__(k) for k in indices]
        self._compteur_lectures += 1
        if self._compteur_lectures > self._lectures_max:
            raise LookupError(
                f"Il faut réaliser strictement moins de {self._lectures_max} lectures dans le tableau"
            )
        if i < 0:
            i = self._taille + i
        if not (0 <= i < self._taille):
            raise IndexError("L'indice demandé est invalide")
        return 0 if i < self._premier_un else 1

    def __getitem_bis__(self, i):
        if i < 0:
            i = self._taille + i
        return 0 if i < self._premier_un else 1

    def __setitem__(self, i, x):
        raise NotImplementedError("Il est interdit de modifier les valeurs")

    def __str__(self):
        return f"[{self.__getitem_bis__(0)}, {self.__getitem_bis__(1)}, ..., {self.__getitem_bis__(self._taille)}]"

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        for i in range(self._taille):
            yield self.__getitem__(i)


for _ in range(20):
    taille = randrange(10**5, 10**6)
    premier_un = randrange(0, taille)
    tableau = Tableau(taille, premier_un)
    attendu = taille - premier_un
    assert compte_uns(tableau) == attendu, f"Erreur avec {tableau}"