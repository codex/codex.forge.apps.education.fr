---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Top-Likes !
tags:
    - dictionnaire
    - ep1
difficulty: 210
---



Sur le réseau social TipTop, on s'intéresse au nombre de « *like* » des abonnés. Les données sont stockées dans un dictionnaire où les clés sont les pseudos et les valeurs correspondantes sont les nombres de « *like* » comme ci-dessous :

```python
{'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}
```

Écrire une fonction `top_likes` qui :

  * prend en paramètre un dictionnaire `likes` non-vide dont les clés sont des chaines de caractères et les valeurs associées des entiers positifs ou nuls,

  * renvoie un tuple dont :
      * la première valeur est la clé du dictionnaire associée à la valeur maximale ; en cas d'égalité sur plusieurs clés, on choisira la plus petite suivant un classement alphabétique,
      * la seconde valeur est la valeur maximale présente dans le dictionnaire.

???+ warning "Contraintes"

    On interdit ici d'utiliser `#!py max`, `#!py min`, ainsi que `#!py sort` ou `#!py sorted`.

???+ example "Exemples"

    ```pycon title=""
    >>> top_likes({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50})
    ('Ada', 201)
    >>> top_likes({'Alan': 222, 'Ada': 201, 'Eve': 222, 'Tim': 50})
    ('Alan', 222)
    >>> top_likes({'David': 222, 'Ada': 201, 'Alan': 222, 'Tim': 50})
    ('Alan', 222)
    >>> top_likes({'David': 0, 'Ada': 0, 'Alan': 0, 'Tim': 0})
    ('Ada', 0)
    ```

{{ IDE('exo', SANS="max, min, sorted, .sort") }}
