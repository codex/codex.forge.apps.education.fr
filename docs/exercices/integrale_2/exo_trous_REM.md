Lorsque $n$ tend vers $+\infty$ la somme calculée tend vers l'aire du domaine étudié. On note cette aire à l'aide d'une **intégrale** :

$$A=\int_a^b f(x)\text{d}x$$

La valeur exacte de cette intégrale est $2,625$.

--------------------------------------

La régularité de la subdivision permet de simplifier le calcul en factorisant par les largeurs :

$$\left(f(a)+f(a+1\text{d}x)+f(a+2\text{d}x)+\dots+f(a+(n-1)\text{d}x)\right)\times\text{d}x\simeq2,4888$$


```python
def aire(a, b, n):
    somme = 0
    dx = (b - a) / n
    for k in range(n):
        x = a + k * dx
        somme = somme + f(x)  # f(x) seul et pas f(x) * dx
    return somme * dx         # on multiplie l'ensemble de la somme
```