# --------- PYODIDE:code --------- #
def f(x):
    return 0.5 * x**3 - 0.5 * x**2 - 0.5 * x + 1


def aire(a, b, n):
    somme = ...
    dx = ... / ...
    for k in range(...):
        x = ... + ... * ...
        somme = ... + ... * ...
    return somme


# --------- PYODIDE:corr --------- #
def f(x):
    return 0.5 * x**3 - 0.5 * x**2 - 0.5 * x + 1


def aire(a, b, n):
    somme = 0
    dx = (b - a) / n
    for k in range(n):
        x = a + k * dx
        somme = somme + f(x) * dx
    return somme


# --------- PYODIDE:tests --------- #
a = -1
b = 2
assert abs(aire(a, b, 1) - 1.5) < 1e-6
assert abs(aire(a, b, 2) - 1.78125) < 1e-6
assert abs(aire(a, b, 4) - 2.1328125) < 1e-6
# --------- PYODIDE:secrets --------- #
n = 8
assert abs(aire(a, b, 8) - 2.361328125) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 16
assert abs(aire(a, b, 16) - 2.48876953125) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 32
assert abs(aire(a, b, 32) - 2.5557861328125) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 64
assert abs(aire(a, b, 64) - 2.590118408203125) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 128
assert abs(aire(a, b, 128) - 2.6074905395507812) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 256
assert abs(aire(a, b, 256) - 2.6162281036376953) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 512
assert abs(aire(a, b, 512) - 2.620609760284424) < 1e-6, f"Erreur avec {(a, b, n) = }"
n = 1024
assert abs(aire(a, b, 1024) - 2.622803807258606) < 1e-6, f"Erreur avec {(a, b, n) = }"
a = -1
b = 1
n = 32
assert abs(aire(a, b, n) - 1.666015625) < 1e-6, f"Erreur avec {(a, b, n) = }"
a = 0
b = 2
n = 64
assert abs(aire(a, b, n) - 1.6513671875) < 1e-6, f"Erreur avec {(a, b, n) = }"
