---
title: Histogramme
author: 
    - Guillaume Connan
    - Pierre Marquestaut
    - Nicolas Revéret
hide:
    - navigation
    - toc
tags:
    - liste/tableau
    - maths
difficulty: 150
maj: 5/04/2024
---

On dispose d'une série d'entiers naturels tous compris entre $0$ et $9$.

On souhaite pouvoir représenter dans un histogramme le nombre d'apparitions (ou d'**occurrence**) de chaque nombre dans cette série.

???+ abstract "Exemple d'histogramme"

	La série $[1~;~1~;~1~;~3~;~2]$ peut être représentée par l'histogramme ci-dessous :

	![image](histo_2.svg){ .center .autolight width=50%}
	
	En effet, 

	- la valeur $0$ n'apparaît pas dans la série ;
	- la valeur $1$ apparaît trois fois dans la série ;
	- la valeur $2$ apparaît une fois dans la série ;
	- la valeur $3$ n'apparaît qu'une fois ;
	- enfin,  il  n'y  a  que  des  $0$  ensuite  car  les  nombres  supérieurs  à  $3$ n'apparaissent pas.


On demande de créer une fonction `histogramme` qui prend en paramètre un tableau `entiers` et qui renvoie un tableau de $10$ éléments dans lequel l'élément d'indice `i` est égal au nombre d'occurrences de la valeur `i` dans `entiers`.

On garantit que toutes les valeurs de `entiers` sont des entiers $0 \leqslant n \leqslant 9$.

???+ example "Exemples"

	```pycon title=""
	>>> entiers = [1, 1, 1, 3, 2]  # exemple de l'énoncé
	>>> histogramme(entiers)
	[0, 3, 1, 1, 0, 0, 0, 0, 0, 0]
	>>> entiers = [1, 1, 1, 3, 2, 1, 2 ,5]
	>>> histogramme(entiers)
	[0, 4, 2, 1, 0, 1, 0, 0, 0, 0]
	>>> histogramme([7 for k in range(100)])
	[0, 0, 0, 0, 0, 0, 0, 100, 0, 0]
	>>> histogramme([])
	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	```

{{ IDE('exo') }}
