# --------- PYODIDE:code --------- #
def histogramme(entiers):
    ...


# --------- PYODIDE:corr --------- #
def histogramme(entiers):
    compteurs = [0 for i in range(10)]
    for i in entiers:
        compteurs[i] = compteurs[i] + 1
    return compteurs


# --------- PYODIDE:tests --------- #
entiers = [1, 1, 1, 3, 2]  # exemple de l'énoncé
assert histogramme(entiers) == [0, 3, 1, 1, 0, 0, 0, 0, 0, 0]
entiers = [1, 1, 1, 3, 2, 1, 2, 5]
assert histogramme(entiers) == [0, 4, 2, 1, 0, 1, 0, 0, 0, 0]
assert histogramme([7 for k in range(100)]) == [0, 0, 0, 0, 0, 0, 0, 100, 0, 0]
assert histogramme([]) == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
# --------- PYODIDE:secrets --------- #
entiers = [5, 4, 3]
attendu = [0, 0, 0, 1, 1, 1, 0, 0, 0, 0]
assert histogramme(entiers) == attendu, f"Erreur pour {entiers = }"

entiers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
attendu = [1] * 10
assert histogramme(entiers) == attendu, f"Erreur pour {entiers = }"

entiers = [0, 0, 0, 0, 0]
attendu = [5, 0, 0, 0, 0, 0, 0, 0, 0, 0]
assert histogramme(entiers) == attendu, f"Erreur pour {entiers = }"

entiers = [k for k in range(10)] * 37
attendu = [37] * 10
assert histogramme(entiers) == attendu, f"Erreur pour {entiers = }"
