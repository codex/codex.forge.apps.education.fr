On peut demander d'afficher des "barres" horizontales.

```python
def histogramme(entiers):
    compteurs = [0 for _ in range(10)]
    for i in entiers:
        compteurs[i] += 1
    for i in range(len(compteurs)):
        print(i, ":", "*" * compteurs[i])
```

L'appel `#!py histogramme([1, 1, 1, 3, 2, 1, 2 ,5])` affiche alors :

```python title=""
0 : 
1 : ****
2 : **
3 : *
4 : 
5 : *
6 : 
7 : 
8 : 
9 : 
```
