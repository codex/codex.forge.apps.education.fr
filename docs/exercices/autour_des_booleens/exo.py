

# --------- PYODIDE:code --------- #

# Exemple donné de traduction de condition
# prop_1 : "3 est strictement inférieur à 4"
prop_1 = 3 < 4

# prop_2 : "3 est supérieur ou égal à 4"
prop_2 = ...

# prop_3 : "le caractère 'n' est dans la chaine 'bonjour'"
prop_3 = ...

# prop_4 : "Le calcul 3 + 5 * 2 vaut 16"
prop_4 = ...

# prop_5 : "5 est un nombre pair"
prop_5 = ...

# prop_6 : "12 est dans la liste [k for k in range(12)]"
prop_6 = ...

# prop_7 : "'ju' n'est pas dans 'bonjour'"
prop_7 = ...

# prop_8 : "3 est égal à 1 + 2 et 'a' est dans 'Boole'"
prop_8 = ...

# prop_9 : "3 est égal à 1 + 2 ou 'a' est dans 'Boole'"
prop_9 = ...

# prop_10 : "6 est un nombre pair et un multiple de 3"
prop_10 = ...

# prop_11 : "648 est dans les tables de 2, de 3 et de 4"
prop_11 = ...

# prop_12 : "25 est strictement compris entre 24 et 26"
prop_12 = ...

# prop_13 : "'a' est dans 'hello' ou dans 'hallo'"
prop_13 = ...

# prop_14 : "8 est égal à 4 * 2 et différent de 9 - 1"
prop_14 = ...

# prop_15 : "7 est entre 1 et 5 (inclus l'un et l'autre) ou strictement supérieur à 6"
prop_15 = ...

# prop_16: "5 est strictement compris entre 7 et 8 ou (8 est strictement inférieur à 9)"
prop_16 = ...

# prop_17: "7 est strictement inférieur à 5 et ...
# ...(8 est strictement supérieur à 5 ou strictment inférieur à 9)"
prop_17 = ...

# --------- PYODIDE:corr --------- #

# Exemple donné de traduction de condition
# prop_1 : "3 est strictement inférieur à 4"
prop_1 = 3 < 4

# prop_2 : "3 est supérieur ou égal à 4"
prop_2 = 3 >= 4

# prop_3 : "le caractère 'n' est dans la chaine 'bonjour'"
prop_3 = "n" in "bonjour"

# prop_4 : "Le calcul 3 + 5 * 2 vaut 16"
prop_4 = 3 + 5 * 2 == 16

# prop_5 : "5 est un nombre pair"
prop_5 = 5 % 2 == 0

# prop_6 : "12 est dans la liste [k for k in range(12)]"
prop_6 = 12 in [k for k in range(12)]

# prop_7 : "'ju' n'est pas dans 'bonjour'"
prop_7 = "ju" not in "bonjour"

# prop_8 : "3 est égal à 1 + 2 et 'a' est dans 'Boole'"
prop_8 = (3 == 1 + 2) and ("a" in "Boole")

# prop_9 : "3 est égal à 1 + 2 ou 'a' est dans 'Boole'"
prop_9 = (3 == 1 + 2) or ("a" in "Boole")

# prop_10 : "6 est un nombre pair et un multiple de 3"
prop_10 = (6 % 2 == 0) and (6 % 3 == 0)

# prop_11 : "648 est dans les tables de 2, de 3 et de 4"
prop_11 = ((648 % 2 == 0) and (648 % 3 == 0)) and (648 % 4 == 0)

# prop_12 : "25 est strictement compris entre 24 et 26"
prop_12 = (25 > 24) and (25 < 26)
# prop_12 : "ou"
prop_12 = 24 < 25 < 26

# prop_13 : "'a' est dans 'hello' ou dans 'hallo'"
prop_13 = ("a" in "hello") or ("a" in "hallo")

# prop_14 : "8 est égal à 4 * 2 et différent de 9 - 1"
prop_14 = (8 == 4 * 2) and (8 != 9 - 1)

# prop_15 : "7 est entre 1 et 5 (inclus l'un et l'autre) ou strictement supérieur à 6"
prop_15 = (7 >= 1) and (7 <= 5) or (7 >= 6)
# prop_15 : "ou"
prop_15 = (1 <= 7 <= 5) or (7 >= 6)

# prop_16: "5 est strictement compris entre 7 ou 8 est strictement inférieur à 9"
prop_16 = (7 < 5 and 5 < 8) or 8 < 9

# prop_17: "7 est strictement inférieur à 5 et ...
# ...(8 est strictement supérieur à 5 ou strictment inférieur à 9)"
prop_17 = 7 < 5 and (5 < 8 or 8 < 9)

# --------- PYODIDE:secrets --------- #

# Tests
assert prop_1 is True, "Erreur avec : 3 est strictement inférieur à 4"
assert prop_2 is False, "Erreur avec : 3 est supérieur ou égal à 4"
assert prop_3 is True, "Erreur avec : le caractère 'n' est dans la chaine 'bonjour'"
assert prop_4 is False, "Erreur avec : Le calcul 3 + 5 * 2 vaut 16"
assert prop_5 is False, "Erreur avec : 5 est un nombre pair"
assert prop_6 is False, "Erreur avec : 12 est dans la liste [k for k in range(12)]"
assert prop_7 is True, "Erreur avec : 'ju' n'est pas dans 'bonjour'"
assert prop_8 is False, "Erreur avec : 3 est égal à 1 + 2 et 'a' est dans 'Boole'"
assert prop_9 is True, "Erreur avec : 3 est égal à 1 + 2 ou 'a' est dans 'Boole'"
assert prop_10 is True, "Erreur avec : 6 est un nombre pair et un multiple de 3"
assert prop_11 is True, "Erreur avec : 648 est dans la table de 2, de 3 et de 4"
assert prop_12 is True, "Erreur avec : 25 est compris entre 24 et 26 (au sens strict)"
assert prop_13 is True, "Erreur avec : 'a' est dans 'hello' ou dans 'hallo'"
assert prop_14 is False, "Erreur avec : 8 est égal à 4 * 2 et différent de 9 - 1"
assert prop_15 is True, "Erreur avec : 7 est compris entre 1 et 5 (au sens large) ou strictement supérieur à 6"
assert prop_16 is True, "Erreur avec : 5 est strictement compris entre 7 et 8 ou 8 est strictement inférieur à 9"
assert not (
    prop_17 is True
), "Erreur avec : 7 est strictement inférieur à 5 et (8 est strictement supérieur à 5 ou strictment inférieur à 9)"
