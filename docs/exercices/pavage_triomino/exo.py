

# --------- PYODIDE:code --------- #

def est_pavage(n, i_trou, j_trou, triominos):
    ...

# --------- PYODIDE:corr --------- #

def est_pavage(n, i_trou, j_trou, triominos):
    if 3 * len(triominos) + 1 != n * n:
        return False
    pavage = [[False for j in range(n)] for i in range(n)]
    pavage[i_trou][j_trou] = True
    for (i, j, sens) in triominos:
        if not (0 <= i < n) or not (0 <= j < n):
            return False
        # carré central
        if pavage[i][j]:
            return False
        else:
            pavage[i][j] = True
        # en haut, ou en bas
        if sens > 1:  # en bas
            if i + 1 >= n:
                return False
            if pavage[i + 1][j]:
                return False
            pavage[i + 1][j] = True
        else:  # en haut
            if i - 1 < 0:
                return False
            if pavage[i - 1][j]:
                return False
            pavage[i - 1][j] = True
        # à gauche, ou à droite
        if sens % 2 == 1:  # à droite
            if j + 1 >= n:
                return False
            if pavage[i][j + 1]:
                return False
            pavage[i][j + 1] = True
        else:  # à gauche
            if j - 1 < 0:
                return False
            if pavage[i][j - 1]:
                return False
            pavage[i][j - 1] = True
    # vérification que tout est rempli
    # (facultatif avec le premier test)
    for i in range(n):
        for j in range(n):
            if not pavage[i][j]:
                return False
    return True

# --------- PYODIDE:tests --------- #

assert est_pavage(2, 1, 1, [(0, 0, 3)]) is True

assert est_pavage(3, 1, 2, [(0, 0, 3), (2, 1, 0)]) is False

triominos = [(0, 1, 2), (2, 1, 2), (0, 2, 3), (2, 3, 0), (3, 2, 1)]
assert est_pavage(4, 1, 0, triominos) is False


triominos = [
    (0, 0, 3),
    (0, 2, 3),
    (1, 4, 0),
    (2, 1, 1),
    (2, 3, 3),
    (3, 0, 1),
    (4, 2, 0),
    (4, 4, 0),
]
assert est_pavage(5, 4, 0, triominos) is True

# --------- PYODIDE:secrets --------- #


# autres tests

assert est_pavage(2, 0, 0, [(0, 0, 3)]) is False
assert est_pavage(2, 0, 0, [(0, 1, 2)]) is False

triominos = [
    (0, 0, 3),
    (0, 0, 3),
    (0, 2, 3),
    (1, 4, 0),
    (2, 1, 1),
    (2, 3, 3),
    (3, 0, 1),
    (4, 2, 0),
    (4, 4, 0),
]
assert est_pavage(5, 4, 0, triominos) is False, "Il y a un doublon"

triominos = [
    (0, 2, 3),
    (1, 4, 0),
    (2, 1, 1),
    (2, 3, 3),
    (3, 0, 1),
    (4, 2, 0),
    (4, 4, 0),
]
assert est_pavage(5, 4, 0, triominos) is False, "Il y en manque"


assert (
    est_pavage(4, 2, 1, [(1, 0, 1), (0, 2, 0), (1, 3, 0), (3, 0, 1), (2, 3, 2)])
    is False
), "Dessiner ce cas !!!"