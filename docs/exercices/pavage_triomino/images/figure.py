import drawSvg as draw


def ajoute_grille(d, n, m):
    for i in range(n + 1):
        d.append(draw.Lines(
             0*m, 20*i,
            20*m, 20*i,
            close=False,
            stroke_width=2,
            stroke='grey')
        )
        d.append(draw.Circle(0*m,  20*i, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))
        d.append(draw.Circle(20*m, 20*i, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))

    for j in range(m + 1):
        d.append(draw.Lines(
            20*j,  0*n,
            20*j, 20*n,
            close=False,
            stroke_width=2,
            stroke='grey')
        )
        d.append(draw.Circle(20*j,  0*n, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))
        d.append(draw.Circle(20*j, 20*n, 0.5,
                fill='grey', stroke_width=1, stroke='grey'))

def pose_trou(i, j, couleur='black'):
    d.append(draw.Lines((i+0) * large, (j+0) * large,
                        (i+1) * large, (j+0) * large,
                        (i+1) * large, (j+1) * large,
                        (i+0) * large, (j+1) * large,
                        close=True,
                fill=couleur, stroke_width=2,
                stroke='black'))

def pose_triomino(i, j, direction):
    if direction == 1:
        d.append(draw.Lines((i+0) * large, (j+0) * large,
                            (i+2) * large, (j+0) * large,
                            (i+2) * large, (j+1) * large,
                            (i+1) * large, (j+1) * large,
                            (i+1) * large, (j+2) * large,
                            (i+0) * large, (j+2) * large,
                            close=True,
                    fill='red', stroke_width=2,
                    stroke='black'))
    if direction == 2:
        d.append(draw.Lines((i+0) * large, (j+0) * large,
                            (i+0) * large, (j-1) * large,
                            (i+1) * large, (j-1) * large,
                            (i+1) * large, (j+1) * large,
                            (i-1) * large, (j+1) * large,
                            (i-1) * large, (j+0) * large,
                            close=True,
                    fill='yellow', stroke_width=2,
                    stroke='black'))
    if direction == 3:
        d.append(draw.Lines((i+1) * large, (j+0) * large,
                            (i+2) * large, (j+0) * large,
                            (i+2) * large, (j+1) * large,
                            (i+0) * large, (j+1) * large,
                            (i+0) * large, (j-1) * large,
                            (i+1) * large, (j-1) * large,
                            close=True,
                    fill='blue', stroke_width=2,
                    stroke='black'))
    if direction == 0:
        d.append(draw.Lines((i-1) * large, (j+0) * large,
                            (i+1) * large, (j+0) * large,
                            (i+1) * large, (j+2) * large,
                            (i+0) * large, (j+2) * large,
                            (i+0) * large, (j+1) * large,
                            (i-1) * large, (j+1) * large,
                            close=True,
                    fill='green', stroke_width=2,
                    stroke='black'))


n, m = 10, 10
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)

for i in range(8):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

for triomino in [
    (1, 1, 1),
    (3, 2, 2),
    (2, 3, 0),
    (3, 3, 1),
    (3, 6, 3),
    (6, 6, 2),
    (4, 5, 3),
    (1, 5, 3),
    (1, 6, 1),
    (2, 8, 2),
    (3, 8, 3),
    (5, 7, 0),
    (6, 7, 1),
    (8, 8, 2),
    (8, 1, 0),
    (8, 4, 0),
    (6, 1, 0),
    (4, 2, 3),
    (7, 3, 3),
    (7, 6, 3),
    (6, 4, 2),
]:
    pose_triomino(*triomino)

pose_trou(5, 3)

d.saveSvg(f'pavage.svg')
print(f"![](images/pavage.svg)")


n, m = 6, 16
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (2, 3, 0),
    (5, 3, 1),
    (10, 3, 2),
    (13, 3, 3),
]:
    pose_triomino(*triomino)

for i in range(4):
    d.append(draw.Text(f'Sens {i}', 16, (i*4 + 1)*large, 1*large + 4, fill='grey'))

d.saveSvg(f'triominos.svg')
print(f"![](images/triominos.svg)")

n, m = 4, 4
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (1, 2, 3),
]:
    pose_triomino(*triomino)
pose_trou(2, 1)

for i in range(2):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex2.svg')
print(f"![](images/ex2.svg)")


n, m = 4, 4
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (1, 2, 3),
    (2, 0, 0),
]:
    pose_triomino(*triomino)
pose_trou(3, 1)
pose_trou(3, 2, 'grey')
pose_trou(3, 0, 'grey')

for i in range(3):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex3.svg')
print(f"![](images/ex3.svg)")

n, m = 5, 5
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (2, 3, 2),
    (2, 1, 2),
    (3, 3, 3),
    (4, 1, 0),
    (3, 0, 1),
]:
    pose_triomino(*triomino)
pose_trou(1, 2)
pose_trou(3, 1, 'brown')
pose_trou(1, 0, 'grey')

for i in range(4):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex4.svg')
print(f"![](images/ex4.svg)")

n, m = 6, 6
large = 20
marge = 8

d = draw.Drawing(
    m * large + 2*marge,
    n * large + 2*marge,
    origin=(-marge, -marge),
    displayInline=False
)
#ajoute_grille(d, n, m)


for triomino in [
    (1, 4, 3),
    (3, 4, 3),
    (5, 3, 0),
    (2, 2, 1),
    (1, 1, 1),
    (4, 2, 3),
    (3, 0, 0),
    (5, 0, 0),
]:
    pose_triomino(*triomino)
pose_trou(1, 0)

for i in range(5):
    d.append(draw.Text(f'{i}', 16, 5, (n-i-2)*large + 4, fill='grey'))
    d.append(draw.Text(f'{i}', 16, (i+1)*large + 5, (m-1)*large + 4, fill='grey'))

d.saveSvg(f'ex5.svg')
print(f"![](images/ex5.svg)")
