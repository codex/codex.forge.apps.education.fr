# --------- PYODIDE:env --------- #

def sont_proches(x, y):
    """
    Vérifie que deux nombres sont proches,
    c'est-à-dire égaux au millionième près
    """
    return abs(x - y) < 10**-6

# --------- PYODIDE:code --------- #

def moyenne_olympique(notes):
    ...


def sont_proches(x, y):
    """
    Vérifie que deux nombres sont proches,
    c'est-à-dire égaux au millionième près
    """
    return abs(x - y) < 10**-6

# --------- PYODIDE:corr --------- #

def moyenne_olympique(notes):
    nb_juges = len(notes)
    mini = notes[0]
    maxi = notes[0]
    total = 0
    for note in notes:
        total += note
        if note < mini:
            mini = note
        if note > maxi:
            maxi = note
    return (total - mini - maxi) / (nb_juges - 2)


# --------- PYODIDE:tests --------- #

notes_1 = [2, 0, 2, 10, 2]
assert sont_proches(moyenne_olympique(notes_1), 2.0)

notes_2 = [1, 1, 1, 1, 1, 1]
assert sont_proches(moyenne_olympique(notes_2), 1.0)

notes_3 = [5, 1, 4, 3, 2, 6]
assert sont_proches(moyenne_olympique(notes_3), 3.5)

# --------- PYODIDE:secrets --------- #

def sont_proches(x, y):
    """
    Vérifie que deux nombres sont proches,
    c'est-à-dire égaux au millionième près
    """
    return abs(x - y) < 10**-6

# Tests
notes_1 = [2, 0, 2, 10, 2]
assert abs(moyenne_olympique(notes_1) - 2.0) < 10**-6

notes_2 = [1, 1, 1, 1, 1, 1]
assert abs(moyenne_olympique(notes_2) - 1.0) < 10**-6

notes_3 = [5, 1, 4, 3, 2, 6]
assert abs(moyenne_olympique(notes_3) - 3.5) < 10**-6

# Tests secrets
notes_1 = [1, 2, 4, 5]
notes_2 = [1, 1, 1, 1, 1]

assert abs(moyenne_olympique(notes_1) - 3.0) < 10**-6, f"Erreur avec {notes_1}"
assert abs(moyenne_olympique(notes_2) - 1.0) < 10**-6, f"Erreur avec {notes_2}"

notes_1 = [2, 0, 2, 10, 2]
notes_2 = [1, 1, 1, 1, 1, 1]
assert abs(moyenne_olympique(notes_1) - 2.0) < 10**-6, f"Erreur avec {notes_1}"
assert abs(moyenne_olympique(notes_2) - 1.0) < 10**-6, f"Erreur avec {notes_2}"
notes_3 = [10, 10, 10]
notes_4 = [0, 5, 10]
notes_5 = [0, 10, 10]
notes_6 = [1, 5, 1]
assert abs(moyenne_olympique(notes_3) - 10.0) < 10**-6, f"Erreur avec {notes_3}"
assert abs(moyenne_olympique(notes_4) - 5.0) < 10**-6, f"Erreur avec {notes_4}"
assert abs(moyenne_olympique(notes_5) - 10.0) < 10**-6, f"Erreur avec {notes_5}"
assert abs(moyenne_olympique(notes_6) - 1.0) < 10**-6, f"Erreur avec {notes_6}"

def rand_tests():
    """ Use a function to avoid leaking the originals in the global scope """
    sum = __move_forward__('sum')
    min = __move_forward__('min')
    max = __move_forward__('max')

    from random import randint

    for i in range(1, 21):
        tab = [randint(0, 100) for i in range(i * 50)]
        moyenne = (sum(tab) - min(tab) - max(tab)) / (len(tab) - 2)
        assert abs(moyenne_olympique(tab) - moyenne) < 10**-6

rand_tests()
