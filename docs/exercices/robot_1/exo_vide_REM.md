Il serait aussi possible d'utiliser un dictionnaire associant à chaque instruction la méthode spécifique du robot :

```python
def execute(robot, instructions):
    fonction = {"A": robot.avance, "D": robot.droite, "G": robot.gauche}
    for action in instructions:
        fonction[action]()
```

Dans le cas où l'action est `#!py "A"`, `fonction[action]` est égal à `robot.avance` et donc `fonction[action]()` exécute `robot.avance()`.