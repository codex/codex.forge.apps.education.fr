

# --------- PYODIDE:code --------- #

from math import sqrt

class Carre:
    def __init__(self, nombres):
        self.ordre = int(sqrt(len(nombres)))
        self.tableau = [[nombres[j + i*self.ordre] for ... in range(self.ordre)] for ... in range(self.ordre)]

    def affiche(self):
        '''Affiche un carré'''
        for ligne in self.tableau:
            print(ligne)

    def somme_ligne(self, i):
        '''Calcule la somme des valeurs de la ligne i'''
        ...

    def somme_colonne(self, j):
        '''Calcule la somme des valeurs de la colonne j'''
        ...

    def est_semi_magique(self):
        somme_commune = self.somme_ligne(0)
        ...

# --------- PYODIDE:corr --------- #

from math import sqrt


class Carre:
    def __init__(self, nombres):
        self.ordre = int(sqrt(len(nombres)))
        self.tableau = [
            [nombres[j + i * self.ordre] for j in range(self.ordre)]
            for i in range(self.ordre)
        ]

    def affiche(self):
        """Affiche un carré"""
        for ligne in self.tableau:
            print(ligne)

    def somme_ligne(self, i):
        """Calcule la somme des valeurs de la ligne i"""
        somme = 0
        for j in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def somme_colonne(self, j):
        """Calcule la somme des valeurs de la colonne j"""
        somme = 0
        for i in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def est_semi_magique(self):
        somme_commune = self.somme_ligne(0)
        for k in range(self.ordre):
            if self.somme_ligne(k) != somme_commune:
                return False
            if self.somme_colonne(k) != somme_commune:
                return False
        return True

# --------- PYODIDE:tests --------- #

nombres_2 = (1, 7, 7, 1)
nombres_3 = (3, 4, 5, 4, 4, 4, 5, 4, 3)
nombres_3_bis = (2, 9, 4, 7, 0, 3, 6, 1, 8)
nombres_4 = (1, 2, 3, 4, 11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34)
carre_2 = Carre(nombres_2)
carre_3 = Carre(nombres_3)
carre_3_bis = Carre(nombres_3_bis)
carre_4 = Carre(nombres_4)

print("Carré 4")
carre_4.affiche()

assert carre_3.somme_ligne(0) == 12
assert carre_3_bis.somme_colonne(1) == 10
assert carre_2.est_semi_magique() is True
assert carre_3.est_semi_magique() is True
assert carre_3_bis.est_semi_magique() is False
assert carre_4.est_semi_magique() is False

# --------- PYODIDE:secrets --------- #

# Tests

nombres_2 = (1, 7, 7, 1)
nombres_3 = (3, 4, 5, 4, 4, 4, 5, 4, 3)
nombres_3_bis = (2, 9, 4, 7, 0, 3, 6, 1, 8)
nombres_4 = (1, 2, 3, 4, 11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34)
carre_2 = Carre(nombres_2)
carre_3 = Carre(nombres_3)
carre_3_bis = Carre(nombres_3_bis)
carre_4 = Carre(nombres_4)
assert carre_2.est_semi_magique() is True
assert carre_3.est_semi_magique() is True
assert carre_3_bis.est_semi_magique() is False
assert carre_4.est_semi_magique() is False


# Autres tests
def cree_matrice(nombres):
    n = int(sqrt(len(nombres)))
    tableau = [[nombres[j + i * n] for j in range(n)] for i in range(n)]
    return tableau


nombres_5 = (1, 4, 14, 15, 13, 16, 2, 3, 8, 5, 11, 10, 12, 9, 7, 6)
nombres_6 = (1, 4, 14, 15, 13, 16, 2, 3, 8, 5, 11, 10, 12, 9, 7, 5)
nombres_7 = (2, 4, 14, 15, 13, 16, 2, 3, 8, 5, 11, 10, 12, 9, 7, 6)
nombres_8 = (1, 2, 3, 1, 2, 3, 1, 2, 3)
nombres_9 = (1, 1, 1, 2, 2, 2, 3, 3, 3)
carre_5 = Carre(nombres_5)
carre_6 = Carre(nombres_6)
carre_7 = Carre(nombres_7)
carre_8 = Carre(nombres_8)
carre_9 = Carre(nombres_9)

for nombres in [
    nombres_2,
    nombres_3,
    nombres_4,
    nombres_5,
    nombres_6,
    nombres_7,
    nombres_8,
    nombres_9,
]:
    assert Carre(nombres).tableau == cree_matrice(nombres), "méthode __init__ fausse"

assert carre_5.est_semi_magique() is True
assert carre_6.est_semi_magique() is False
assert carre_7.est_semi_magique() is False
assert carre_8.est_semi_magique() is False, "faux pour (1, 2, 3, 1, 2, 3, 1, 2, 3)"
assert carre_9.est_semi_magique() is False, "faux pour (1, 1, 1, 2, 2, 2, 3, 3, 3)"


# autres tests de Franck

from random import randrange

n = 10
base = [randrange(10**9) for _ in range(n)] * 2
nombres = []
for i in range(n):
    nombres.extend(base[i : i + n])

gros_test = tuple(nombres)
assert Carre(gros_test).est_semi_magique() is True, "Erreur avec un gros test"

truc = randrange(-(10**8), 10**8)
truc = 42 if not truc else truc

for i in range(n):
    for j in range(n):
        nombres[i * n + j] += truc
        gros_test = tuple(nombres)
        assert Carre(gros_test).est_semi_magique() is False, "Erreur avec un gros test"
        nombres[i * n + j] -= truc

for k_e in range(n - 1):
    for k_s in range(k_e + 1, n):

        for i in range(n):
            nombres[n * i + k_s] += truc
            nombres[n * i + k_e] -= truc
            gros_test = tuple(nombres)
            assert (
                Carre(gros_test).est_semi_magique() is False
            ), "Erreur, il faut aussi vérifier les colonnes"

            nombres[n * i + k_s] -= truc
            nombres[n * i + k_e] += truc

        for j in range(n):
            nombres[n * k_s + j] += truc
            nombres[n * k_e + j] -= truc
            gros_test = tuple(nombres)
            assert (
                Carre(gros_test).est_semi_magique() is False
            ), "Erreur, il faut aussi vérifier les lignes"
