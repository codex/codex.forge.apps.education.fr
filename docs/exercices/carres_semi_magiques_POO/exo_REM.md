!!! info "Fonction `sum`"

    Nous aurions pu utiliser la fonction "built-in" `sum` de Python

    Lien vers la documentation Python : [sum](https://docs.python.org/fr/3/library/functions.html#sum)


???+ "Réponses alternatives"

	Autre possibilité de réponses

	```python
	def somme_ligne(self, i):
        '''Calcule la somme des valeurs de la ligne i'''
        somme = sum(self.tableau[i])
        return somme

    
    def somme_colonne(self, j):
        '''Calcule la somme des valeurs de la colonne j'''
        return sum(self.tableau[i][j] for i in range(self.ordre))
    ```




    
