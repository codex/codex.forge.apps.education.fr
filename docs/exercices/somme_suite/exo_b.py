

# --------- PYODIDE:code --------- #

def somme_termes(n):
    somme = ...
    for k in range(..., ...):
        somme = ...

    return ...

# --------- PYODIDE:corr --------- #

def somme_termes(n):
    somme = 0
    for k in range(1, n + 1):
        somme = somme + 1 / k

    return somme

# --------- PYODIDE:tests --------- #

assert somme_termes(1) == 1.0
assert somme_termes(2) == 1.5
assert abs(somme_termes(3) - 1.833333333) < 1e-6
assert abs(somme_termes(70) - 4.83283675) < 1e-6

# --------- PYODIDE:secrets --------- #


# Autres tests

resultats = {
    10: 2.92896825396,
    15: 3.31822899322,
    215: 5.9501774716,
    300: 6.2826638802,
    460: 6.7095287170,
    470: 6.7310118121,
    483: 6.7582671580,
    499: 6.7908234299,
}
for n, attendu in resultats.items():
    assert abs(somme_termes(n) - attendu) < 1e-6, f"Erreur avec {n = }"