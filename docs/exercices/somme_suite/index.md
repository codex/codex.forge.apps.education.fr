---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Somme des termes d'une suite mathématique
tags:
    - float
    - maths
difficulty: 199
maj: 01/03/2024
---



Soit $(u_n)$ la suite  définie pour tout $n > 0$ par $u_n = \frac{1}{n}$. On souhaite calculer la somme $S_n$ des $n$ premiers termes de cette suite.

Par exemple, la somme des $70$ premiers termes est :

\[ 
\begin{align*}
    S_{70}  &=\sum_{k=1}^{70}u_n\\
            &=u_1+u_2+\dots+u_{70}\\
            &= 1+\frac12+\frac13+\dots+\frac1{70}\\
            &\approx 4,832~836~75
\end{align*}
    \]

<!-- &=\frac{42~535~343~474~848~157~886~823~113~473}{8~801~320~137~209~899~102~584~580~800}\\ -->

Écrire la fonction `somme_termes` qui calcule la somme $S_n$ des `n` premiers termes de $(u_n)$.

On garantit que $1 \leqslant n \leqslant 10^3$.

{{ remarque('proximite_flottants') }}

{{ remarque('assertion') }}


???+ example "Exemples"

    ```pycon title=""
    >>> somme_termes(1)
    1.0
    >>> somme_termes(2)
    1.5
    >>> somme_termes(3) 
    1.8333333333333333
    >>> somme_termes(70)
    4.832836757638071
    ```

=== "Version vide"
    {{ IDE('exo') }}
=== "Version à compléter"
    {{ IDE('exo_b') }}
