---
author: Mireille Coilhac
hide:
    - navigation
    - toc
title: Dictionnaires de « J'aime »
tags:
    - dictionnaire
    - fonctions
difficulty: 200
---



Lors d'une soirée déguisée un vote est organisé afin d'élire le meilleur costume.

Chaque personne peut attribuer des « *J'aime* » à aucune, une, ou plusieurs personnes pour son déguisement.
Les résultats de ces « *J'aime* » sont enregistrés dans un dictionnaire dont les clés sont les prénoms des personnes et la valeur associée est la liste des personnes auxquelles elles ont attribué un « *J'aime* ».

Par exemple, pour le dictionnaire :

```python
votes_soiree = {
    "Alice": ["Bob", "Carole", "Dylan"],
    "Bob": ["Carole", "Esma"],
    "Esma": ["Bob", "Alice"],
    "Fabien": ["Dylan"],
    "Carole": [],
    "Dylan":[],
}
```

Comme on peut le voir, Alice a attribué des « *J'aime* » aux déguisements de Bob, Carole et Dylan. Carole n'a quant à elle attribué aucun « *J'aime* ».

Vous devez compléter les deux fonctions suivantes :

* `scores_aimes` prend en argument le dictionnaire `votes` tel que décrit plus haut et renvoie le dictionnaire dont les clés sont les personnes présentes lors de la soirée et les valeurs associées le nombre de « *J'aime* » qu'ils ont reçus ;

* `gagnants` prend en argument le dictionnaire `votes` tel que décrit plus haut et renvoie la liste contenant le nom des vainqueurs du vote (les noms des personnes ayant reçus le plus de « *J'aime* »).



???+ example "Exemples"

    ```pycon title=""
    >>> votes_soiree = {
    ...     "Alice": ["Bob", "Carole", "Dylan"],
    ...     "Bob": ["Carole", "Esma"],
    ...     "Esma": ["Bob", "Alice"],
    ...     "Fabien": ["Dylan"],
    ...     "Carole": [],
    ...     "Dylan":[],
    ... }
    >>> scores_aimes(votes_soiree)
    {'Bob': 2, 'Alice': 1, 'Esma': 1, 'Fabien': 0, 'Carole': 2, 'Dylan': 2}
    >>> gagnants(votes_soiree)
    ['Bob', 'Carole', 'Dylan']
    ```

{{ IDE('exo') }}
