

# --------- PYODIDE:code --------- #

def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime.
    """
    ...


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de : J'aime
    """
    ...

# --------- PYODIDE:corr --------- #

def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime
    """
    bilan = {}
    for personne in votes:
        bilan[personne] = 0

    for personne in votes:
        for personne_choisie in votes[personne]:
            bilan[personne_choisie] = bilan[personne_choisie] + 1
    return bilan


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de : J'aime
    """
    bilan = scores_aimes(votes)
    maxi = 0
    for personne in bilan:
        if bilan[personne] > maxi:
            maxi = bilan[personne]

    liste_gagnants = [nom for nom in bilan if bilan[nom] == maxi]

    return liste_gagnants

# --------- PYODIDE:tests --------- #

votes_soiree = {
    "Alice": ["Bob", "Carole", "Dylan"],
    "Bob": ["Carole", "Esma"],
    "Esma": ["Bob", "Alice"],
    "Fabien": ["Dylan"],
    "Carole": [],
    "Dylan": [],
}


assert scores_aimes(votes_soiree) == {
    "Bob": 2,
    "Alice": 1,
    "Esma": 1,
    "Fabien": 0,
    "Carole": 2,
    "Dylan": 2,
}

assert sorted(gagnants(votes_soiree)) == ["Bob", "Carole", "Dylan"]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
votes_1 = {"a": [], "b": [], "e": [], "f": [], "c": [], "d": []}
assert scores_aimes(votes_1) == {"a": 0, "b": 0, "c": 0, "d": 0, "e": 0, "f": 0}
assert sorted(gagnants(votes_1)) == ["a", "b", "c", "d", "e", "f"]

votes_2 = {"a": ["a"], "b": ["b"], "e": ["e"], "f": ["f"], "c": ["c"], "d": ["d"]}
assert scores_aimes(votes_2) == {"a": 1, "b": 1, "e": 1, "f": 1, "c": 1, "d": 1}
assert sorted(gagnants(votes_2)) == ["a", "b", "c", "d", "e", "f"]

votes_3 = {"a": [], "b": ["a"], "e": ["a"], "f": ["a"], "c": ["a"], "d": ["a"]}
assert scores_aimes(votes_3) == {"a": 5, "b": 0, "e": 0, "f": 0, "c": 0, "d": 0}
assert sorted(gagnants(votes_3)) == ["a"]