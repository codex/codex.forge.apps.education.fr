---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Résolution approchée d'une équation par dichotomie
tags:
    - maths
    - dichotomie
    - à trous
difficulty: 250
maj: 01/03/2024
---

Il arrive, en mathématiques, d'être confronté à une équation que l'on ne peut pas résoudre algébriquement. Il est alors possible d'utiliser un algorithme permettant de calculer une **valeur approchée** d'une solution de l'équation.

On considère dans cet exercice l'équation $(E)$ : $f(x)=0$ :

* supposons que l'on connaisse déjà un intervalle de recherche à cibler, l'intervalle $\left[ \text{début}\,;\,\text{fin}\right]$ ;

* supposons de plus que la fonction $f$ est continue et strictement monotone sur cet intervalle.   

Dans ce cas-là, nous pouvons utiliser l'algorithme de **dichotomie**.

Si $C_f$ est la courbe représentative de $f$, résoudre $(E)$ revient à déterminer l'abscisse d'un point d'intersection de $C_f$ avec l'axe des abscisses.

![Equation](images/0.svg){width=40% .center .autolight}

Voici les trois premières étapes, dans le cas particulier représenté ci-dessus, du déroulement de cet algorithme : 

=== "Etape 1"

    * On calcule l'abscisse du milieu de l'intervalle de recherche ;
    * On observe que $f(\text{milieu})$ et $f(\text{fin})$ sont de signes contraires ;
    * On doit donc poursuivre la recherche sur l'intervalle $[\text{milieu}\,;\,\text{fin}]$ (sur la droite de l'intervalle de recherche comme illustré ci-dessous).

    ![Equation](images/1.svg){width=30% .center .autolight}  
=== "Etape 2"

    * On calcule à nouveau l'abscisse du milieu de l'intervalle de recherche ;
    * On observe que $f(\text{milieu})$ et $f(\text{fin})$ sont de mêmes signes ;
    * On doit donc poursuivre la recherche sur l'intervalle $[\text{début}\,;\,\text{milieu}]$ (sur la gauche de l'intervalle de recherche comme illustré ci-dessous).
    
    ![Equation](images/2.svg){width=30% .center .autolight}
=== "Etape 3"

    * On calcule toujours l'abscisse du milieu de l'intervalle de recherche ;
    * On observe que $f(\text{milieu})$ et $f(\text{fin})$ sont de signes contraires ;
    * On doit donc poursuivre la recherche sur l'intervalle $[\text{milieu}\,;\,\text{fin}]$ (sur la droite de l'intervalle de recherche comme illustré ci-dessous).

    ![Equation](images/3.svg){width=30% .center .autolight}

    👉 On continue le processus jusqu'à obtenir un encadrement de la solution d'amplitude désirée.

??? note "Valeurs de mêmes signes ?"

    On rappelle que les réels $a$ et $b$ distincts non nuls sont de même signe équivaut à : $a \times b > 0$


Si une solution exacte de l'équation n'a pas été trouvée (ce qui est le plus souvent le cas), on répète le principe de dichotomie en divisant par deux l'amplitude de l'intervalle de recherche à chaque étape, jusqu'à ce que son amplitude soit inférieure à une précision souhaitée.

Exprimé en français l'algorithme de dichotomie est donc le suivant (cliquer sur les **+** pour lire les explications en commentaires):

```python title="Algorithme de dichotomie"
Fonction dichotomie(debut, fin, precision) :                   # (1)
    Tant que fin - debut > precision :
        milieu = (debut + fin) / 2
        Si f(milieu) × f(fin) est strictement positif :        # (2)
            fin = milieu
        Sinon si f(milieu) × f(fin) est strictement négatif :  # (3)                                   
            debut = milieu
        Sinon                                                  # (4)                                        
            Renvoyer (milieu, milieu)                          # (5)
    Renvoyer (debut, fin)                                      
```

1. La fonction prend trois paramètres : le debut et la fin de l'intervalle de recherche ainsi que la précision.
2. `milieu` est strictement supérieur à la solution.
3. `milieu` est strictement inférieur la solution.
4. `milieu` est la solution. On renvoie l'intervalle [`milieu`, `milieu`]
5. L'instruction `Renvoyer` provoque la sortie de la fonction. C'est ici une sortie anticipée car la solution
 a été trouvée. Il est inutile de continuer à la chercher.

Nous allons utiliser cette méthode afin de déterminer une valeur approchée à $10^{-6}$ près de l'équation :

$$-x^3+x^2+x+1=0$$

![équation](images/equation.svg){width=40% .center .autolight}
 

Écrire le code de la fonction `dichotomie` qui prend en paramètres les nombres `debut`, `fin` et `precision`.   

`debut`, `fin` délimitent l'intervalle de recherche et `precision` est la précision souhaitée.

Cette fonction renvoie un intervalle d'amplitude `precision` contenant la solution de l'équation étudiée.

{{ remarque('assertion') }}
    
=== "Version vide"
    {{ IDE('./pythons/exo_dichotb')}}
=== "Version à compléter"
    {{ IDE('./pythons/exo_dichota')}}

    
    
