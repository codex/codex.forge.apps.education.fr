# --------- PYODIDE:code --------- #

def somme_valeurs(tableau): 
    ...

# --------- PYODIDE:corr --------- #


def somme_valeurs(tableau): 
    somme = 0
    for valeur in tableau:
        somme =  somme + valeur
    return somme
        
    
# --------- PYODIDE:tests --------- #

mes_valeurs = [5, 6, 4, 7, 2, 8]
assert somme_valeurs(mes_valeurs) == 32
mes_valeurs = [1, 2, 3, 4, 5, 6, 7, 8]
assert somme_valeurs(mes_valeurs) == 36
mes_valeurs = []
assert somme_valeurs(mes_valeurs) == 0
mes_valeurs = [-5, -10, -2, -8]
assert somme_valeurs(mes_valeurs) == -25

# --------- PYODIDE:secrets --------- #
from random import randint
def somme_reponse(tableau): 
    somme = 0
    taille = len(tableau)
    for i in range(taille):
        somme =  somme + tableau[i]
    return somme


n = 100
mes_valeurs = [randint(-n,n) for k in range(n + 1)]
reponse = somme_reponse(mes_valeurs)
assert somme_valeurs(mes_valeurs) == reponse, "Erreur pour le tableau aléatoire mes_valeurs"


n = 1000
mes_valeurs = [randint(-n,n) for k in range(n + 1)]
reponse = somme_reponse(mes_valeurs)
assert somme_valeurs(mes_valeurs) == reponse, "Erreur pour le tableau aléatoire mes_valeurs"