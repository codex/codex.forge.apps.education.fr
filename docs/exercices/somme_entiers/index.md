---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Somme d'une liste d'entiers
tags:
    - liste/tableau
difficulty: 050
---


On dispose d'une liste de valeurs entières regroupées dans un même tableau. 


```python title=""
tableau = [5, 6, 4, 7, 2, 8]
```
    
**Compléter** une fonction `somme_valeurs` qui prend en paramètre un tableau d'entiers et qui renvoie la somme des valeurs de ce tableau.

Attention, ce tableau peut être vide et dans ce cas la fonction renvoie 0.

```pycon title=""
>>> mes_valeurs = [5, 6, 4, 7, 2, 8]
>>> somme_valeurs(mes_valeurs) 
32
>>> mes_valeurs = [1, 2, 3, 4, 5, 6, 7, 8]
>>> somme_valeurs(mes_valeurs)  
36
>>> mes_valeurs = []
>>> somme_valeurs(mes_valeurs)  
0
```
??? tip "Aide (1)"
    On pourra initialiser une variable, puis parcourir les valeurs du tableau pour les ajouter à la variable.

??? tip "Aide (2)"
    Pour ajouter une valeur à une variable, on utilisera l'instruction suivante :
    ```python title=""
    a = a + 100
    ```
    ou en abrégé :
    ```python title=""
    a += 100
    ```

=== "Version vide"
    {{ IDE('exo_a', SANS = "sum") }}
=== "Version Parcours par indices"
    {{ IDE('exo_c', SANS = "sum") }}
=== "Version Parcours par valeurs"
    {{ IDE('exo_b', SANS = "sum") }}



