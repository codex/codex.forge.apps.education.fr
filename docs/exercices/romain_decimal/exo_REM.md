Voici une solution par parcours d'éléments, et non par parcours d'indices :

```python
def valeur(romain):
    valeur_gauche = 0
    total = 0
    for caractere in romain:
        valeur_droite = VALEUR[caractere]
        if valeur_gauche < valeur_droite:
            total = total - valeur_gauche
        else:
            total = total + valeur_gauche
        valeur_gauche = valeur_droite
    return total + valeur_gauche
```
