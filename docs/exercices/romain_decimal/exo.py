

# --------- PYODIDE:code --------- #

VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def valeur(romain):
    ...

# --------- PYODIDE:corr --------- #

VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def valeur(romain):
    n = len(romain)
    cumul = 0
    for i in range(n - 1):
        if VALEUR[romain[i]] < VALEUR[romain[i + 1]]:
            cumul = cumul - VALEUR[romain[i]]
        else:
            cumul = cumul + VALEUR[romain[i]]
    cumul = cumul + VALEUR[romain[n - 1]]
    return cumul

# --------- PYODIDE:tests --------- #

assert valeur("XVI") == 16
assert valeur("MMXXII") == 2022
assert valeur("CDII") == 402
assert valeur("XLII") == 42

# --------- PYODIDE:secrets --------- #


# Autres tests

assert valeur("MCMXCI") == 1991
assert valeur("MCMXCIX") == 1999
assert valeur("MMMCMXCIX") == 3999
assert valeur("XCIV") == 94
assert valeur("MDCLXVI") == 1666
for cle in VALEUR:
    assert valeur(cle) == VALEUR[cle], f"Erreur avec {cle}"