---
license: "by-nc-sa"
author:
    - Mireille Coilhac
    - Franck Chambon
hide:
    - navigation
    - toc
title: Lire les chiffres romains
tags:
    - int
    - dictionnaire
    - ep2
difficulty: 180
--- 



Les chiffres romains sont un système ancien d'écriture des nombres.

Les chiffres (symboles) romains sont: I, V, X, L, C, D et M. Ces symboles représentent respectivement 1, 5, 10, 50, 100, 500 et 1000 en base dix.

Cette association pourra être modélisée par un dictionnaire défini une fois pour toute (une constante) donné ci-dessous :

```python
VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
```


## Principe de l'algorithme

Le principe est de commencer à zéro, puis d'additionner ou retrancher les valeurs de tous les symboles représentant un nombre écrit en chiffres romains. 

* Si un symbole a une valeur **supérieure ou égale** à celui qui se trouve à sa droite, il est **ajouté**.

    !!! example "Exemple XVI"
    
        "XVI" est le nombre 16  car "X" vaut 10 qui est supérieur à 5 (valeur de "V"), qui est lui même supérieur à 1 (valeur de "I").  
        Ainsi "XVI" vaut 0 + 10 + 5 + 1 = 16. 
    

* Si un symbole a une valeur **strictement inférieure** à celui qui se trouve à sa droite, il est **retranché**.

    !!! example "Exemple XIV" 

        "XIV" est le nombre 14  car "X" vaut 10 qui est supérieur ou égal à 1 (valeur de "I"), qui est lui même inférieur à 5 (valeur de "V").  
        Ainsi "XIV" vaut 0 + 10 - 1 + 5 = 14. 

* Si le symbole est en dernière position, il est **ajouté**.


## Travail à réaliser

On souhaite créer une fonction  `valeur` qui prend en paramètre une chaîne de caractères (non vide) représentant un nombre écrit en chiffres romains et qui renvoie sa valeur en écriture décimale.


???+ example "Exemples"

    ```pycon title=""
    >>> valeur("XVI")
    16
    >>> valeur("MMXXII")
    2022
    >>> valeur("CDII")
    402
    >>> valeur("XLII")
    42
    ```  

??? tip "Aide"

    Regardons l'exemple `valeur("CDII")` :  
    Les valeurs associées à `#!py "CDII"` sont successivement : $100$ ; $500$ ; $1$ ; $1$.  
    Nous avons $100 < 500$, $500 \geqslant 1$ et $1 \geqslant 1$.  
    La valeur renvoyée sera donc $0 - 100 + 500 + 1 + 1 = 402$.


Compléter la fonction fournie.

{{ IDE('exo') }}
