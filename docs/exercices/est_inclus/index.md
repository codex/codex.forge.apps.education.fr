---
license: "by-nc-sa"
author:
    - Franck Chambon
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Texte inclus
tags:
    - à trous
    - string
    - fonctions
difficulty: 210
---


L'ADN peut être représenté par une chaine de caractères formée avec les lettres `A`, `T`, `G`, `C`.

- Un **brin** est un petit morceau d'ADN, que l'on retrouve parfois dans
- un **gène** qui est une grande séquence d'ADN.

La fonction `est_inclus` prend en paramètres deux chaines de caractères `brin` et `gene` et renvoie la réponse, un booléen, à la question « Retrouve-t-on `brin` inclus dans `gene` ? ».

Cette fonction utilise une fonction auxiliaire : `correspond(motif, chaine, position)` qui renvoie `True` si on retrouve `motif` exactement à partir de `position` dans `chaine` et `False` sinon.

???+ example "Exemples"

    ```pycon title=""
    >>> correspond("AA", "AAGGTTCC", 4)
    False
    >>> correspond("AT", "ATGCATGC", 4)
    True
    >>> est_inclus("AATC", "GTACAAATCTTGCC")
    True
    >>> est_inclus("AGTC", "GTACAAATCTTGCC")
    False
    >>> est_inclus("AGTC", "GTACAAATCTTGCA")
    False
    >>> est_inclus("AGTC", "GTACAAATCTAGTC")
    True
    ```

Compléter le code Python ci-dessous.

{{ IDE('exo') }}
