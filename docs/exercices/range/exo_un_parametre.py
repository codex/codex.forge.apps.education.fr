# --------- PYODIDE:code --------- #

# Les entiers de 0 (inclus) à 11 (exclu)
liste_1 = list(range(...))

# Les entiers de 0 (inclus) à 20 (exclu)
liste_2 = list(range(...))

# Les entiers de 0 (inclus) à 20 (inclus)
liste_3 = list(range(...))

# --------- PYODIDE:corr --------- #

# Les entiers de 0 (inclus) à 11 (exclu)
liste_1 = list(range(11))

# Les entiers de 0 (inclus) à 20 (exclu)
liste_2 = list(range(20))

# Les entiers de 0 (inclus) à 20 (inclus)
liste_3 = list(range(21))


# --------- PYODIDE:secrets --------- #

# Les entiers de 0 (inclus) à 11 (exclu)
attendu = list(range(11))
assert liste_1 == attendu, "Erreur sur la liste 1"

# Les entiers de 0 (inclus) à 20 (exclu)
attendu = list(range(20))
assert liste_2 == attendu, "Erreur sur la liste 2"

# Les entiers de 0 (inclus) à 20 (inclus)
attendu = list(range(21))
assert liste_3 == attendu, "Erreur sur la liste 3"
