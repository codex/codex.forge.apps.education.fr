# --------- PYODIDE:code --------- #

# Les entiers de 3 (inclus) à 10 (exclu)
liste_4 = list(range(..., ...))

# Les entiers de 12 (inclus) à 20 (inclus)
liste_5 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (exclu)
liste_6 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (inclus)
liste_7 = list(range(..., ...))


# --------- PYODIDE:corr --------- #

# Les entiers de 3 (inclus) à 10 (exclu)
liste_4 = list(range(3, 10))

# Les entiers de 12 (inclus) à 20 (inclus)
liste_5 = list(range(12, 21))

# Les entiers de -10 (inclus) à 1 (exclu)
liste_6 = list(range(-10, 1))

# Les entiers de -10 (inclus) à 1 (inclus)
liste_7 = list(range(-10, 2))


# --------- PYODIDE:secrets --------- #

# Les entiers de 3 (inclus) à 10 (exclu)
attendu = list(range(3, 10))
assert liste_4 == attendu, "Erreur sur la liste 4"

# Les entiers de 12 (inclus) à 20 (inclus)
attendu = list(range(12, 21))
assert liste_5 == attendu, "Erreur sur la liste 5"

# Les entiers de -10 (inclus) à 1 (exclu)
attendu = list(range(-10, 1))
assert liste_6 == attendu, "Erreur sur la liste 6"

# Les entiers de -10 (inclus) à 1 (inclus)
attendu = list(range(-10, 2))
assert liste_7 == attendu, "Erreur sur la liste 7"
