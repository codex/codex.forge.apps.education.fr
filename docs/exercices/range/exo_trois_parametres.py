# --------- PYODIDE:code --------- #

# Les entiers de 20 (inclus) à 0 (inclus)
liste_8 = list(range(..., ..., ...))

# Les entiers de 500 (inclus) à 200 (exclu)
liste_9 = list(range(..., ..., ...))

# Les entiers de 10 (inclus) à -10 (exclu)
liste_10 = list(range(..., ..., ...))

# Les entiers de 25 (inclus) à -25 (inclus)
liste_11 = list(range(..., ..., ...))

# Les impairs de 1 (inclus) à 100 (exclu)
liste_12 = list(range(..., ..., ...))

# Les multiples de 3 de 0 (inclus) à 301 (exclu)
liste_13 = list(range(..., ..., ...))

# Les multiples de 7 de 0 (inclus) à 994 (inclus)
liste_14 = list(range(..., ..., ...))

# Les pairs de -100 (inclus) à 100 (exclu)
liste_15 = list(range(..., ..., ...))

# Les multiples de 3 de 300 (inclus) à 102 (exclu)
liste_16 = list(range(..., ..., ...))

# Les multiples de 13 de 3445 (inclus) à -559 (inclus)
liste_17 = list(range(..., ..., ...))

# --------- PYODIDE:corr --------- #

# Les entiers de 20 (inclus) à 0 (inclus)
liste_8 = list(range(20, -1, -1))

# Les entiers de 500 (inclus) à 200 (exclu)
liste_9 = list(range(500, 200, -1))

# Les entiers de 10 (inclus) à -10 (exclu)
liste_10 = list(range(10, -10, -1))

# Les entiers de 25 (inclus) à -25 (inclus)
liste_11 = list(range(25, -26, -1))

# Les impairs de 1 (inclus) à 100 (exclu)
liste_12 = list(range(1, 100, 2))

# Les multiples de 3 de 0 (inclus) à 301 (exclu)
liste_13 = list(range(0, 301, 3))

# Les multiples de 7 de 0 (inclus) à 994 (inclus)
liste_14 = list(range(0, 995, 7))

# Les pairs de -100 (inclus) à 100 (exclu)
liste_15 = list(range(-100, 100, 2))

# Les multiples de 3 de 300 (inclus) à 102 (exclu)
liste_16 = list(range(300, 102, -3))

# Les multiples de 13 de 3445 (inclus) à -559 (inclus)
liste_17 = list(range(3445, -560, -13))

# --------- PYODIDE:secrets --------- #

# Les entiers de 20 (inclus) à 0 (inclus)
attendu = list(range(20, -1, -1))
assert liste_8 == attendu, "Erreur sur la liste 8"

# Les entiers de 500 (inclus) à 200 (exclu)
attendu = list(range(500, 200, -1))
assert liste_9 == attendu, "Erreur sur la liste 9"

# Les entiers de 10 (inclus) à -10 (exclu)
attendu = list(range(10, -10, -1))
assert liste_10 == attendu, "Erreur sur la liste 10"

# Les entiers de 25 (inclus) à -25 (inclus)
attendu = list(range(25, -26, -1))
assert liste_11 == attendu, "Erreur sur la liste 11"

# Les pairs de 1 (inclus) à 100 (exclu)
attendu = list(range(1, 100, 2))
assert liste_12 == attendu, "Erreur sur la liste 12"

# Les multiples de 3 de 0 (inclus) à 301 (exclu)
attendu = list(range(0, 301, 3))
assert liste_13 == attendu, "Erreur sur la liste 13"

# Les multiples de 7 de 0 (inclus) à 994 (inclus)
attendu = list(range(0, 995, 7))
assert liste_14 == attendu, "Erreur sur la liste 14"

# Les pairs de -100 (inclus) à 100 (exclu)
attendu = list(range(-100, 100, 2))
assert liste_15 == attendu, "Erreur sur la liste 15"

# Les multiples de 3 de 300 (inclus) à 102 (exclu)
attendu = list(range(300, 102, -3))
assert liste_16 == attendu, "Erreur sur la liste 16"

# Les multiples de 13 de 3445 (inclus) à -559 (inclus)
attendu = list(range(3445, -560, -13))
assert liste_17 == attendu, "Erreur sur la liste 17"
