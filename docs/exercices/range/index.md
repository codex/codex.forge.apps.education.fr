---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Autour de range
tags:
    - boucle
---


En Python, l'instruction `#!python range` permet de parcourir des nombres entiers. Cette fonction renvoie un objet **qui n'est pas une liste** et génère les nombres au fur et à mesure de leur utilisation.

Elle peut être utilisée avec **un**, **deux** ou **trois** paramètres.

Quelle que soit la méthode utilisée, ces paramètres sont toujours des **nombres entiers** (positifs ou négatifs).

!!! note "Remarque"

    La conversion en une liste Python (avec `#!py list(range(5))`) permet de visualiser les nombres parcourus par `#!py range`.

??? question "**Un** paramètre : `#!py range(b)`"

    L'appel `#!py range(b)` permet de parcourir les entiers de `#!py 0` (inclus) à `b` (exclu).

    ```pycon
    >>> list(range(5))
    [0, 1, 2, 3, 4]
    >>> list(range(6))
    [0, 1, 2, 3, 4, 5]
    ```

    On note que dans chaque appel, la valeur de `b` est bien exclue du parcours.

    Compléter le code ci-dessous.

    {{ IDE('exo_un_parametre')}}

??? question "**Deux** paramètres : `#!py range(a, b)`"

    L'appel `#!py range(a, b)` permet de parcourir les entiers de `a` (inclus) à `b` (exclu).

    ```pycon
    >>> list(range(0, 5))
    [0, 1, 2, 3, 4]
    >>> list(range(-5, 2))
    [-5, -4, -3, -2, -1, 0, 1]
    ```

    !!! note "Remarque"

        Dans l'utilisation avec un seul paramètre, Python utilise par défaut la valeur `#!py 0` pour `a`.

    Compléter le code ci-dessous.

    {{ IDE('exo_deux_parametres')}}


??? question "**Trois** paramètres : `#!py range(a, b, c)`"

    L'appel `#!py range(a, b, c)` permet de parcourir les entiers de `a` (inclus) à `b` (exclu) avec un pas (un écart) de `c`.

    Ainsi, `#!python range(3, 18, 5)` permet de parcourir successivement les nombres `#!py 3`, `#!py 8` et `#!py 13`. En effet, :

    * `#!py 3` est la valeur de départ (incluse),
    * `#!py 8` la suit. En effet le pas vaut `#!py 5` et `#!py 3 + 5` égale `#!py 8`,
    * `#!py 13` est égal à `#!py 8 + 5`.

    Le `#!py 18` (égal à `#!py 13 + 5`) est bien exclu.

    ```pycon
    >>> list(range(3, 18, 5))
    [3, 8, 13]
    >>> list(range(3, 19, 5))
    [3, 8, 13, 18]
    >>> list(range(20, 15, -1))
    [20, 19, 18, 17, 16]
    ```

    !!! note "Remarque"

        Dans les utilisations avec un ou deux paramètres, Python utilise par défaut la valeur `#!py 1` pour `c`.

    Compléter le code ci-dessous.
 
    {{ IDE('exo_trois_parametres', MAX=12) }}
    

