

# --------- PYODIDE:code --------- #

def proprietaire(numero, contacts):
    ...

# --------- PYODIDE:corr --------- #

def proprietaire(numero, contacts):
    for personne in contacts:
        if contacts[personne] == numero:
            return personne
    return None

# --------- PYODIDE:tests --------- #

contacts = {
    "Victor": 6618563,
    "Thelma": 2978634,
    "Tesnim": 1486372,
    "Louane": 3284298,
    "Moussa": 2978634,
}
assert proprietaire(6618563, contacts) == "Victor"
assert proprietaire(2978634, contacts) in ("Thelma", "Moussa")
assert proprietaire(5178530, contacts) is None

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from string import ascii_letters
from random import choice, randrange
contacts = {}
numeros = {}
taille = 200
for _ in range(taille):
    personne = "".join([choice(ascii_letters) for _ in range(10)])
    numero = randrange(10**6, 10**7)
    contacts[personne] = numero
    numeros.get(numero, []).append(personne)
for numero, personnes in numeros.items():
    assert proprietaire(numero, contacts) in personnes, f"Erreur en cherchant {numero} dans {contacts}"
numero = randrange(10**7, 10**8)
assert proprietaire(numero, contacts) is None, f"Erreur en cherchant {numero} dans {contacts}"