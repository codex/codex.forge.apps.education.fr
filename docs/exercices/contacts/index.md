---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Recherche d'un contact
tags:
    - dictionnaire
difficulty: 170
maj: 01/02/2024
--- 

Une personne trouve dans sa poche un numéro de téléphone sans le nom du contact associé.

Cette personne a pris soin d'enregistrer tous ses contacts dans un dictionnaire Python au format `#!py {nom_du_contact: numero}`. `#!py nom_du_contact` est une chaîne de caractères, `#!py numero` un entier positif.

```python
contacts = {
    "Victor": 6618563,
    "Thelma": 2978634,
    "Tesnim": 1486372,
    "Louane": 3284298,
    "Moussa": 2978634,
}
```

Écrire la fonction `#!py proprietaire` qui prend en argument un numéro de téléphone (`#!py numero`) et le dictionnaire des contacts (`#!py contacts`) et renvoie le nom d'un contact auquel ce numéro est associé.

Si aucun contact n'est associé à ce numéro, la fonction renverra `#!py None`.

Si deux contacts ou plus sont associés à ce numéro, la fonction renverra indifféremment l'un d'entre eux.

???+ example "Exemples"

    ```pycon title=""
    >>> contacts = {
    ...     "Victor": 6618563,
    ...     "Thelma": 2978634,
    ...     "Tesnim": 1486372,
    ...     "Louane": 3284298,
    ...     "Moussa": 2978634,
    ... }
    >>> proprietaire(6618563, contacts)
    'Victor'
    >>> proprietaire(2978634, contacts)
    'Moussa'
    >>> proprietaire(5178530, contacts) is None
    True
    ```

??? note "Dictionnaire et ordre"

    Depuis Python 3.7, les dictionnaires sont parcourus dans l'ordre d'insertion des clés. Rien n'est garanti pour les versions antérieures.
    
    C'est pourquoi dans le cas présent, si deux contacts ou plus sont associés au même numéro, on demande de renvoyer indifféremment l'un d'entre eux.

{{ IDE('exo') }}
