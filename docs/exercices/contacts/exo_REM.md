On pourrait aussi utiliser la méthode `#!py dict.items` :

```python
def proprietaire(numero, contacts):
    for personne, telephone in contacts.items():
        if telephone == numero:
            return personne
    return None
```

Notons aussi que le `#!py return None` en dernière ligne n'est pas indispensable : une fonction Python renvoie `#!py None` par défaut.