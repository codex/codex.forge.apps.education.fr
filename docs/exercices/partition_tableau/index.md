---
author:
    - Gilles Lassus
hide:
    - navigation
    - toc
title: Partition de tableau
difficulty: 160
tags:
    - tri
    - ep1
--- 

Écrire une fonction `partition` qui prend en paramètres un entier `pivot` et une liste d'entiers `tableau` et qui renvoie un tuple composé de trois listes :

- la première liste contient les indices, dans l'ordre croissant, des valeurs de `tableau` strictement inférieures à `pivot` ;
- la deuxième liste contient les indices, dans l'ordre croissant, des valeurs de `tableau` égales à `pivot` ;
- la troisième liste contient les indices, dans l'ordre croissant, des valeurs de `tableau` strictement supérieures à `pivot`.


!!! example "Exemples"

    ```python
    >>> partition(3, [1, 3, 4, 2, 4, 6, 3, 0])
    ([0, 3, 7], [1, 6], [2, 4, 5])
    >>> partition(3, [1, 4, 2, 4, 6, 0])
    ([0, 2, 5], [], [1, 3, 4])
    >>>partition(3, [1, 1, 1, 1])
    ([0, 1, 2, 3], [], [])
    >>> partition(3, [])
    ([], [], [])
    ```
    
{{ IDE('exo') }}
