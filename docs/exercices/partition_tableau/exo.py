

# --------- PYODIDE:code --------- #

def partition(pivot, tableau):
    ...

# --------- PYODIDE:corr --------- #

def partition(pivot, tableau):
    indices_inferieur = []
    indices_egal = []
    indices_superieur = []
    for i in range(len(tableau)):
        if tableau[i] < pivot:
            indices_inferieur.append(i)
        elif tableau[i] > pivot:
            indices_superieur.append(i)
        else:
            indices_egal.append(i)
    return (indices_inferieur, indices_egal, indices_superieur)

# --------- PYODIDE:tests --------- #

assert partition(3, [1, 3, 4, 2, 4, 6, 3, 0]) == ([0, 3, 7], [1, 6], [2, 4, 5])
assert partition(3, [1, 4, 2, 4, 6, 0]) == ([0, 2, 5], [], [1, 3, 4])
assert partition(3, [1, 1, 1, 1]) == ([0, 1, 2, 3], [], [])
assert partition(3, []) == ([], [], [])

# --------- PYODIDE:secrets --------- #


# Autres Tests
assert partition(3, [3, 3, 3, 3]) == ([], [0, 1, 2, 3], [])


def partition_corr(pivot, tableau):
    inferieurs, egaux, superieurs = [], [], []
    for i, x in enumerate(tableau):
        if x < pivot:
            inferieurs.append(i)
        elif x == pivot:
            egaux.append(i)
        else:
            superieurs.append(i)
    return inferieurs, egaux, superieurs


from random import randrange

x_mini = -50
x_maxi = 51
for test in range(20):
    taille = randrange(20, 31)
    tableau = [randrange(x_mini, x_maxi) for _ in range(taille)]
    pivot = randrange(x_mini, x_maxi)
    attendu = partition_corr(pivot, tableau)
    assert (
        partition(pivot, tableau) == attendu
    ), f"Erreur avec {pivot = } et {tableau = }"