Même si on essaie d'être malin en ne parcourant que les éléments suivants pour voir si un élément apparait plusieurs fois, cette fonction a un cout quadratique. En gros, avec un tableau de taille $n$ on va faire environ $n^2$ comparaisons.

L'optimisation de ne regarder que les éléments suivants permet de diviser par 2 le nombre de tests, mais cela reste beaucoup.
