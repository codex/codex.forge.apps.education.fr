

# --------- PYODIDE:code --------- #

def tous_differents(tableau):
    ...

# --------- PYODIDE:corr --------- #

def tous_differents(tableau):
    n = len(tableau)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if tableau[i] == tableau[j]:
                return False
    return True

# --------- PYODIDE:tests --------- #

tableau1 = [1, 2, 3, 6, 2, 4, 5]
assert tous_differents(tableau1) is False

tableau2 = ["chien", "chat", "lion", "poisson"]
assert tous_differents(tableau2) is True

# --------- PYODIDE:secrets --------- #


# autres tests
assert tous_differents([]) is True
assert tous_differents([1, 1]) is False
assert tous_differents([9, 6, 7, 3, 2, 1, 1]) is False
assert tous_differents([9, 6, 7, 3, 2, 1, 1, 1]) is False
assert tous_differents(range(100)) is True
assert tous_differents(["v", "o", "i", "t", "u", "r", "e"]) is True
assert tous_differents(["a", "a", "b", "c", "d", "e"]) is False
assert tous_differents(["a", "a", "b", "c", "d", "e", "a"]) is False
assert tous_differents([1, 2, 3, 4, 1]) is False
assert tous_differents([5, 9, 1, 2, 1, 3, 7]) is False
assert tous_differents([1, 2, 3, 4, 3, 2, 1]) is False
assert tous_differents(["a", "a", 1, 1, "truc", "truc"]) is False
assert tous_differents(["bob"]) is True
assert tous_differents(100*[1]) is False