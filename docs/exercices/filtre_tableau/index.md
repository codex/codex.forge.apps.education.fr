---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Filtrer un tableau
tags:
    - liste/tableau
    - booléen
difficulty: 210
maj: 11/11/2024
---

On considère dans cet exercice les fonctions booléennes suivantes prenant toutes en paramètre un nombre entier :

| Fonction      | Rôle                                                                          | Exemple                                         |
| :------------ | :---------------------------------------------------------------------------- | :---------------------------------------------- |
| `est_positif` | Renvoie `#!py True` si cet entier est strictement positif, `#!py False` sinon | `#!py est_positif(9)` est évalué à `#!py True`  |
| `est_pair`    | Renvoie `#!py True` si cet entier est pair, `#!py False` sinon                | `#!py est_pair(9)` est évalué à `#!py False`    |
| `est_impair`  | Renvoie `#!py True` si cet entier est impair, `#!py False` sinon              | `#!py est_impair(9)` est évalué à `#!py True`   |
| `est_premier` | Renvoie `#!py True` si cet entier est premier, `#!py False` sinon             | `#!py est_premier(9)` est évalué à `#!py False` |

Ces différentes fonctions sont déjà chargées dans l'éditeur. **Il est inutile de les importer**.

??? tip "Codes des fonctions booléennes utilisées"

    ```python
    def est_positif(x):
        return x > 0


    def est_pair(x):
        return x % 2 == 0


    def est_impair(x):
        return x % 2 != 0


    def est_premier(n):
        if n < 2:
            return False

        d = 2
        while d * d <= n:
            if n % d == 0:
                return False
            d = d + 1
        return True
    ```
    
    On signale que la fonction `est_premier` est très rudimentaire et sera peu efficace pour tester la primalité de grands nombres.
    
    Elle reste toutefois fonctionnelle dans le cadre des valeurs testées dans cet exercice.

On cherche dans cet exercice à filtrer des listes de nombres entiers en utilisant ces fonctions.

Si l'on considère par exemple la liste `#!py nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]` alors :

* les entiers strictement positifs dans `nombres` sont : `#!py [1, 2, 3, 4, 5, 6, 7, 8, 9]` ;

* les entiers pairs dans `nombres` sont : `#!py [0, 2, 4, 6, 8]` ;
* les entiers impairs dans `nombres` sont : `#!py [1, 3, 5, 7, 9]` ;
* les entiers premiers dans `nombres` sont : `#!py [2, 3, 5, 7]`.

Écrire la fonction `filtre` qui prend en paramètres une fonction booléenne `f` parmi celles citées plus haut et un tableau de nombres entiers `nombres` et renvoie la liste des valeurs `x` de `nombres` pour lesquelles `f(x)` est évalué à `#!py True`.

???+ warning "Contraintes"

    La liste `nombres` ne doit pas être modifiée.
    
    Les valeurs seront renvoyées dans l'ordre dans lequel elles apparaissent dans `nombres`.

{{ interdiction('filter') }}

???+ example "Exemples"

    ```pycon title=""
    >>> nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> filtre(est_positif, nombres)
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> filtre(est_pair, nombres)
    [0, 2, 4, 6, 8]
    >>> filtre(est_impair, nombres)
    [1, 3, 5, 7, 9]
    >>> filtre(est_premier, nombres)
    [2, 3, 5, 7]
    ```

{{ IDE('./pythons/exo', SANS="filter") }}