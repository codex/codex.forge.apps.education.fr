# --------- PYODIDE:env --------- #
def est_positif(x):
    return x > 0


def est_pair(x):
    return x % 2 == 0


def est_impair(x):
    return x % 2 != 0


def est_premier(n):
    if n < 2:
        return False

    d = 2
    while d * d <= n:
        if n % d == 0:
            return False
        d = d + 1
    return True


# --------- PYODIDE:code --------- #


def filtre(f, nombres):
    ...


# --------- PYODIDE:corr --------- #


def filtre(f, nombres):
    return [x for x in nombres if f(x)]


# --------- PYODIDE:tests --------- #

nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
assert filtre(est_positif, nombres) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
assert filtre(est_pair, nombres) == [0, 2, 4, 6, 8]
assert filtre(est_impair, nombres) == [1, 3, 5, 7, 9]
assert filtre(est_premier, nombres) == [2, 3, 5, 7]

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

nombres = [randrange(-(2**16), 2**16) for _ in range(100)]
for f in (est_positif, est_pair, est_impair, est_premier):
    attendus = [x for x in nombres if f(x)]
    assert (
        filtre(f, nombres) == attendus
    ), f"Erreur en filtrant {nombres} avec {f.__name__}"
