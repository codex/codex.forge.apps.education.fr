---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Numération Shadok (I)
tags:
    - glouton
    - int
    - string
difficulty: 350
---



La langage Shadok ne comporte que 4 syllabes : « $GA$ », « $BU$ », « $ZO$ » et « $MEU$ ». Pour se compter, ils utilisent donc uniquement ces syllabes.

Ainsi  :

* quand il n'y a pas de Shadok, il disent qu'il y a $GA$ Shadok ;
* quand il y a $1$ Shadok, il disent qu'il y a $BU$ Shadok ;
* quand il y a $2$ Shadoks, il disent qu'il y a $ZO$ Shadoks ;
* quand il y a $3$ Shadoks, il disent qu'il y a $MEU$ Shadoks.

Quand il y a $4$ Shadoks, ils les mettent dans une poubelle et comptent les poubelles.
Ainsi $6$ Shadoks correspondent à $BU$ poubelle et $ZO$ Shadoks, dit aussi $BUZO$.

Quand il y a $4$ poubelles de remplies, ils les mettent dans une grande poubelle et comptent les grandes poubelles. Ainsi $33$ Shadoks correspondent à $ZO$ grandes poubelles, $GA$ poubelles et $BU$ Shadok, dit aussi $ZOGABU$.

Quand il y a $4$ grandes poubelles remplies... bref ! Vous avez compris !

Attention, les mots des Shadoks sont au maximum de huit syllabes. Au-delà, les Shadoks oublient le début du mot...

??? note "Les explications en vidéo"

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/lP9PaDs2xgQ?si=RbTSA98-dzc7Iyk9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Écrire la fonction `en_shadock` qui prend en paramètre un `nombre` entier compris entre $0$ et $65~535$ (inclus l'un et l'autre) et renvoie une chaine de caractères qui exprime ce nombre en numération Shadok.

???+ example "Exemples"

    ```pycon title=""
    >>> en_shadok(2)
    'ZO'
    >>> en_shadok(6)
    'BUZO'
    >>> en_shadok(33)
    'ZOGABU'
    ```

??? tips "Astuce"

    Il existe plusieurs méthodes pour résoudre ce problème. L'une d'elle s'appuie sur un algorithme glouton.
    
{{ IDE('exo') }}

