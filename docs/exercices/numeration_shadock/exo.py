

# --------- PYODIDE:code --------- #

def en_shadok(nombre):
    ...

# --------- PYODIDE:corr --------- #

def en_shadok(nombre):
    VALEURS = ["GA", "BU", "ZO", "MEU"]
    if nombre == 0:
        return "GA"
    resultat = ""
    while nombre > 0:
        chiffre = VALEURS[nombre % 4]
        resultat = chiffre + resultat
        nombre = nombre // 4
    return resultat

# --------- PYODIDE:tests --------- #

assert en_shadok(2) == "ZO"
assert en_shadok(6) == "BUZO"
assert en_shadok(33) == "ZOGABU"

# --------- PYODIDE:secrets --------- #

def shadok_correction(nombre):
    VALEURS = ["GA", "BU", "ZO", "MEU"]
    if nombre == 0:
        return "GA"
    resultat = ""
    while nombre > 0:
        chiffre = VALEURS[nombre % 4]
        nombre = nombre // 4
        resultat = chiffre + resultat
    return resultat

for test in range(65536):
    attendu = shadok_correction(test)
    assert en_shadok(test) == attendu, f"Erreur avec la valeur {test}"