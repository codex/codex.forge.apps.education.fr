---
author: Romain Janvier
hide:
    - navigation
    - toc
title: Élocution chez le dentiste
tags:
    - string
difficulty: 140
---

Chez le dentiste, la bouche grande ouverte, lorsqu'on essaie de parler, il
ne reste que les voyelles. Même les ponctuations sont supprimées. 
Vous devez écrire une fonction `dentiste` qui prend en paramètre une chaîne de caractères `texte` et qui renvoie une autre chaine ne contenant que les voyelles de `texte`, placées dans le même ordre que dans `texte`. 

Les voyelles sont données par :
```python
VOYELLES = ['a', 'e', 'i', 'o', 'u', 'y']
```
On ne considérera que des textes écrits en minuscules, sans accents.

???+ example "Exemples"

    ```pycon title=""
    >>> dentiste("j'ai mal")
    'aia'
    >>> dentiste("il fait chaud")
    'iaiau'
    >>> dentiste("")
    ''
    ```

{{ IDE('exo') }}
