fibonacci_mem = [0, 1]


def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= n:
        fib_i = fibonacci_mem[i - 1] + fibonacci_mem[i - 2]
        fibonacci_mem.append(fib_i)
        i = i + 1
    return fibonacci_mem[n]
