Cet algorithme permet de diminuer le coût du programme en temps. 

En revanche, il augmente le coût en espace. En effet, lorsqu'il s'exécute, le programme occupe davantage de place en mémoire.