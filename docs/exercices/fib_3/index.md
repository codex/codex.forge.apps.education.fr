---
license: "by-nc-sa"
author:
    - Mireille Coilhac
    - Franck Chambon
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Suite de Fibonacci (3)
tags:
    - récursivité
    - programmation dynamique
difficulty: 290
maj: 01/03/2024
---



Les premiers termes de la suite de Fibonacci sont :

$$0\rightarrow1\rightarrow1\rightarrow2\rightarrow3\rightarrow5\rightarrow8\rightarrow13\rightarrow21\rightarrow34\rightarrow\cdots$$

Les deux premiers termes sont $0$ et $1$. À partir du troisième, un terme est la somme des deux précédents.

Il est possible de calculer les termes de cette suite par une fonction récursive. Cependant, étant donné que chaque appel nécessite $2$ autres appels interdépendants, le calcul du rang $n$ demande $2^n$ appels.
Une version récursive simple ne peut fonctionner que pour des valeurs de $n$ faibles, inférieures à $25$. 

Pour pouvoir calculer des termes de rangs élevés, il faut éviter de calculer plusieurs fois le même terme. On va donc utiliser une liste `fibonacci_mem` pour stocker les termes déjà calculés, `fibonacci_mem[n]` stocke ainsi le résultat de `fibonacci(n)`.



Écrire la fonction `fibonacci` qui prend en paramètre un entier positif ou nul `n` et renvoie le terme d'indice $n$ de la suite de Fibonacci.


???+ example "Exemples"

    ```pycon title=""
    >>> fibonacci(0)
    0
    >>> fibonacci(3)
    2
    >>> fibonacci(7)
    13
    >>> fibonacci(8)
    21
    >>> fibonacci(9)
    34
    ```

    On a bien `#!py fibonacci(9)` égal à `#!py fibonacci(7) + fibonacci(8)`


??? question "Approche *Top-Down*"

    Dans cette version, on utilise une méthode **récursive** efficace : avant de calculer un terme, on vérifie qu'il n'a pas déjà été calculé.

    On appellera `fib_n` le terme d'indice `n`.

    === "Version vide"
        {{ IDE('exo_reca') }}
    === "Version à compléter"
        {{ IDE('exo_recb') }}

??? question "Approche *Bottom-Up*"

    Dans cette version, on utilise une méthode **itérative** : on calcule chaque terme, depuis le premier terme jusqu'au terme du rang demandé.  
      
    Tant que la longueur de cette liste est insuffisante, on ajoute un nouveau terme.    
    `i` est la taille de la liste, qui augmente de 1 à chaque tour de boucle.

    Par la suite, on appellera `fib_i` le terme d'indice `i`.

    === "Version vide"
        {{ IDE('exo_itera') }}
    === "Version à compléter"
        {{ IDE('exo_iterb') }}
