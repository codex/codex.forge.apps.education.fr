# tests
assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(9) == 34
assert fibonacci(4) == 3


# autres tests


PHI = (1 + 5**0.5) / 2

for n in range(70):
    attendu = round(PHI**n / 5**0.5)
    assert fibonacci(n) == attendu, f"Erreur pour n = {n}"

assert fibonacci(100) == 354224848179261915075, f"Erreur pour n = 100"
