

# --------- PYODIDE:code --------- #

fibonacci_mem = [0, 1]


def fibonacci(n):
    ...

# --------- PYODIDE:corr --------- #

fibonacci_mem = [0, 1]


def fibonacci(n):
    if n > len(fibonacci_mem) - 1:
        fib_n = fibonacci(n - 1) + fibonacci(n - 2)
        fibonacci_mem.append(fib_n)

    return fibonacci_mem[n]

# --------- PYODIDE:tests --------- #

assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(4) == 3
assert fibonacci(9) == 34

# --------- PYODIDE:secrets --------- #


# tests secrets
a, b = 0, 1
for n in range(2, 101):
    attendu = a + b
    assert fibonacci(n) == attendu, f"Erreur pour n = {n}"
    a, b = b, a + b