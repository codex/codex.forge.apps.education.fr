La variable fibonacci_mem permet de stocker dans la mémoire cache les résultats intermédiaires. Lors des appels successifs de la fonction, il n’est donc pas besoin de recalculer ces valeurs.

Cependant, le programme occupe davantage de place en mémoire, ce qui augmente donc son coût en espace.
