# --- exo, iter --- #
fibonacci_mem = [0, 1]


def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= ...:
        fib_i = fibonacci_mem[...] + fibonacci_mem[...]
        fibonacci_mem.append(...)
        i = ...
    return ...


# --- vide, iter --- #
fibonacci_mem = [0, 1]


def fibonacci(n): 
    ...


# --- corr, iter --- #
fibonacci_mem = [0, 1]


def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= n:
        fib_i = fibonacci_mem[i - 1] + fibonacci_mem[i - 2]
        fibonacci_mem.append(fib_i)
        i = i + 1
    return fibonacci_mem[n]


# --- exo, rec --- #
fibonacci_mem = [0, 1]


def fibonacci(n):
    if n > len(fibonacci_mem) - 1:
        fib_i = fibonacci(...) + fibonacci(...)
        fibonacci_mem.append(...)

    return ...


# --- vide, rec --- #
fibonacci_mem = [0, 1]


def fibonacci(n): 
    ...


# --- corr, rec --- #
fibonacci_mem = [0, 1]


def fibonacci(n):
    if n > len(fibonacci_mem) - 1:
        fib_i = fibonacci(n - 1) + fibonacci(n - 2)
        fibonacci_mem.append(fib_i)

    return fibonacci_mem[n]


# --- tests, rec, iter --- #
assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(4) == 3
assert fibonacci(9) == 34
# --- secrets, rec, iter --- #
a, b = 0, 1
for n in range(2, 101):
    attendu = a + b
    assert fibonacci(n) == attendu, f"Erreur pour n = {n}"
    a, b = b, a + b
# --- REM, rec, iter --- #
""" # skip
Cet algorithme permet de diminuer le coût du programme en temps. 

En revanche, il augmente le coût en espace. En effet, lorsqu'il s'exécute, le programme occupe davantage de place en mémoire.
"""  # skip
