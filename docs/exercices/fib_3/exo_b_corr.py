fibonacci_mem = [0, 1]


def fibonacci(n):
    if n > len(fibonacci_mem) - 1:
        fib_i = fibonacci(n - 1) + fibonacci(n - 2)
        fibonacci_mem.append(fib_i)

    return fibonacci_mem[n]
