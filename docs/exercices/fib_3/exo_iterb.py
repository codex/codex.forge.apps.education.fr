

# --------- PYODIDE:code --------- #

fibonacci_mem = [0, 1]


def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= ...:
        fib_i = fibonacci_mem[...] + fibonacci_mem[...]
        fibonacci_mem.append(...)
        i = ...
    return ...

# --------- PYODIDE:corr --------- #

fibonacci_mem = [0, 1]


def fibonacci(n):
    i = len(fibonacci_mem)
    while i <= n:
        fib_i = fibonacci_mem[i - 1] + fibonacci_mem[i - 2]
        fibonacci_mem.append(fib_i)
        i = i + 1
    return fibonacci_mem[n]

# --------- PYODIDE:tests --------- #

assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(4) == 3
assert fibonacci(9) == 34

# --------- PYODIDE:secrets --------- #


# tests secrets
a, b = 0, 1
for n in range(2, 101):
    attendu = a + b
    assert fibonacci(n) == attendu, f"Erreur pour n = {n}"
    a, b = b, a + b