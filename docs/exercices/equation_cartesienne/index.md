---
author: 
    - Mireille Coilhac
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Équation cartésienne de droite
tags:
    - maths
    - tuple
    - fonctions
difficulty: 120
maj: 11/07/2024
---

??? tip "Coordonnées entières"

    Dans ces exercices, afin de simplifier les tests, les coordonnées proposées sont des nombres entiers.
    
    Les formules proposées restent toutefois valables en utilisant des nombres réels.

Une droite peut être définie par la donnée :

- de deux points distincts ;

    ou

- d'un seul point et de sa direction donnée par un vecteur non nul appelé *vecteur directeur*.

![Droites](fig.svg){.center .autolight width=40%}

Une équation cartésienne d'une droite peut s'écrire sous la forme :

$$ax+by+c=0$$

où $a$, $b$ et $c$ sont trois réels non tous nuls.

Une même droite possède **une infinité d'équations cartésiennes équivalentes**. Par exemple, $-5x+y+7=0$ et $15x-3y-21=0$ sont des équations cartésiennes de la même droite passant par les points $C~(1~;~-2)$ et $D~(0~;~-7)$. Les coordonnées des deux points vérifient bien les deux équations :

$$
\begin{align*}
-5\times1+(-2)+7=0 \quad\text{ et }\quad& 15\times1-3\times(-2)-21=0\\
\quad\\
-5\times0+(-7)+7=0 \quad\text{ et }\quad& 15\times0-3\times(-7)-21=0\\
\end{align*}
$$

??? note "Rappel - équation réduite"

    Dans le cas où $x_A \neq x_B$, il existe une unique *équation réduite* pour la droite $(AB)$ de la forme $y=mx+p$. On en déduit immédiatement une équation cartésienne $mx-y+p=0$.

    Les exercices de {{ lien_exo("cette série", "fonctions_affines") }} proposent de déterminer l'équation réduite d'une droite.


La droite passant par le point $A$ et de vecteur directeur $\vec{u} \ne \vec{0}$ est l'ensemble des points $M~(x~;~y)$ tels que les vecteurs $\overrightarrow{AM}$ et $\vec{u}$ sont colinéaires, c'est-à dire tels que le déterminant des vecteurs $\overrightarrow{AM}$ et $\vec{u}$ est égal à $0$. On note aussi $\text{det} \left( \overrightarrow{AM},\vec{u} \right) = 0$.

Si l'on note $x_{\vec{u}}$ et $y_{\vec{u}}$ les coordonnées du vecteur $\vec{u}$, les coefficients $a$, $b$ et $c$ d'une équation cartésienne de cette droite peuvent être calculés à l'aide des formules suivantes : 

$$
\begin{cases}
 a= y_{\vec{u}}\\
 b= -x_{\vec{u}}\\
 c= -a \times x_A - b \times y_A
 \end{cases}
 $$
 
{{ remarque('assertion') }}

??? question "Equation cartésienne à partir d'un point et d'un vecteur directeur"

    Écrire la fonction `equation_point_vecteur` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_u` et `y_u` représentant les coordonnées d'un point $A$ et d'un vecteur non nul $\vec{u}$.
    
    Cette fonction renvoie le triplet `#!py (a, b, c)` tel que $ax+by+c=0$ soit une équation cartésienne de la droite passant par $A$ et dirigée par $\vec{u}$.

    Les tests vérifieront que les coordonnées de deux points distincts de la droite satisfont bien l'équation renvoyée. Le second point $B$ utilisé est l'image du point $A$ par la translation de vecteur $\vec{u}$ :

    $$
    \begin{cases}
        x_B = x_A + x_{\vec{u}}\\
        y_B = y_A + y_{\vec{u}}
    \end{cases}
    $$

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> equation_point_vecteur(2, 3, -1, -5)
        (-5, 1, 7)
        >>> equation_point_vecteur(2, 3, 0, -5)
        (-5, 0, 10)
        >>> equation_point_vecteur(2, 3, -1, 0)
        (0, 1, -3)
        ```

    === "Version vide"
        {{ IDE('exo_eq_point_vecteur_vide') }}
    === "Version à compléter"
        {{ IDE('exo_eq_point_vecteur_trous') }}


??? question "Coordonnées d'un vecteur"

    Soit $A$ et $B$ deux points du plan.
    
    Écrire la fonction `vecteur` qui prend en paramètres les entiers `x_A`, `y_A`, `x_B` et `y_B` représentant les coordonnées des points $A$ et $B$ et renvoie les coordonnées du vecteur $\overrightarrow{AB}$.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> vecteur(2, 3, 1, -2)
        (-1, -5)
        >>> vecteur(2, 3, 2, -5)
        (0, -8)
        >>> vecteur(2, 3, 5, 3)
        (3, 0)
        ```

    === "Version vide"
        {{ IDE('exo_coord_vide') }}
    === "Version à compléter"
        {{ IDE('exo_coord_trous') }}


??? question "Equation cartésienne à partir de deux points"

    Soit $A$ et $B$ deux points distincts du plan.

    Écrire la fonction `equation_deux_points` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` représentant les coordonnées des points $A$ et $B$ et renvoie le triplet `#!py (a, b, c)` tel que $ax+by+c=0$ soit une équation cartésienne de la droite $(AB)$.

    Des versions valides des fonctions `equation_point_vecteur` et `vecteur` des exercices précédents sont déjà importées dans cet éditeur. Vous pouvez directement les utiliser.
    
    ???+ example "Exemples"

        ```pycon title=""
        >>> equation_deux_points(2, 3, 1, -2)
        (-5, 1, 7)
        >>> equation_deux_points(2, 3, 2, -2)
        (-5, 0, 10)
        >>> equation_deux_points(2, 3, 1, 3)
        (0, 1, -3)
        ```

    === "Version vide"
        {{ IDE('exo_eq_points_vide') }}
    === "Version à compléter"
        {{ IDE('exo_eq_points_trous') }}
        



