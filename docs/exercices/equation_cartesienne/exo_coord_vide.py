# --------- PYODIDE:env --------- #
def vecteur(x_A, y_A, x_B, y_B):
    ...


# --------- PYODIDE:code --------- #
def vecteur(x_A, y_A, x_B, y_B):
    ...


# --------- PYODIDE:corr --------- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --------- PYODIDE:tests --------- #
assert vecteur(2, 3, 1, -2) == (-1, -5)
assert vecteur(2, 3, 2, -5) == (0, -8)
assert vecteur(2, 3, 5, 3) == (3, 0)
# --------- PYODIDE:secrets --------- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_B = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_B = randrange(-200, 200) // 2
    attendu = (x_B - x_A, y_B - y_A)
    assert (
        vecteur(x_A, y_A, x_B, y_B) == attendu
    ), f"Erreur avec A {(x_A, y_A)} et B {(x_B, y_B)}"


