# --- hdr, eq_point_vecteur_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


def equation_deux_points(x_A, y_A, x_u, y_u):
    ...


def equation_point_vecteur(x_A, y_A, x_u, y_u):
    ...


# --- vide, eq_point_vecteur_ --- #
def equation_point_vecteur(x_A, y_A, x_u, y_u):
    ...


# --- exo, eq_point_vecteur_ --- #
def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = ...
    b = ...
    c = ...
    return (a, b, c)


# --- corr, eq_point_vecteur_ --- #
def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = y_u
    b = -x_u
    c = -a * x_A - b * y_A
    return (a, b, c)


# --- tests, eq_point_vecteur_ --- #
x_A, y_A = 2, 3
x_u, y_u = -1, -5
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_u, y_u = 0, -5
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_u, y_u = -1, 0
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0
# --- secrets, eq_point_vecteur_ --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_u = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_u = randrange(-200, 200) // 2
    x_B = x_A + x_u
    y_B = y_A + y_u
    (a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
    assert (
        a * x_A + b * y_A + c == 0 and a * x_B + b * y_B + c == 0
    ), f"Erreur avec A {(x_A, y_A)}, u {(x_u, y_u)}"


# --- hdr, coord_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    ...


# --- vide, coord_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    ...


# --- exo, coord_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = ...
    ...
    return (..., ...)


# --- corr, coord_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --- tests, coord_ --- #
assert vecteur(2, 3, 1, -2) == (-1, -5)
assert vecteur(2, 3, 2, -5) == (0, -8)
assert vecteur(2, 3, 5, 3) == (3, 0)
# --- secrets, coord_ --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_B = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_B = randrange(-200, 200) // 2
    attendu = (x_B - x_A, y_B - y_A)
    assert (
        vecteur(x_A, y_A, x_B, y_B) == attendu
    ), f"Erreur avec A {(x_A, y_A)} et B {(x_B, y_B)}"


# --- hdr, eq_points_ --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


def equation_deux_points(x_A, y_A, x_B, y_B):
    ...


def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = y_u
    b = -x_u
    c = -a * x_A - b * y_A
    return (a, b, c)


# --- vide, eq_points_ --- #
def equation_deux_points(x_A, y_A, x_B, y_B):
    ...


# --- exo, eq_points_ --- #
def equation_deux_points(x_A, y_A, x_B, y_B):
    (x_AB, y_AB) = ...
    ...


# --- corr, eq_points_ --- #
def equation_deux_points(x_A, y_A, x_B, y_B):
    (x_AB, y_AB) = vecteur(x_A, y_A, x_B, y_B)
    return equation_point_vecteur(x_A, y_A, x_AB, y_AB)


# --- tests, eq_points_ --- #
x_A, y_A = 2, 3
x_B, y_B = 1, -2
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_B, y_B = 2, -2
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_B, y_B = 1, 3
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0
# --- secrets, eq_points_ --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_B = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_B = randrange(-200, 200) // 2
    if (x_A, y_A) == (x_B, y_B):
        continue
    (a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
    assert (
        a * x_A + b * y_A + c == 0 and a * x_B + b * y_B + c == 0
    ), f"Erreur avec A {(x_A, y_A)}, B {(x_B, y_B)}"

