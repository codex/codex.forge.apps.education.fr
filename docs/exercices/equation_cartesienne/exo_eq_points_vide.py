# --------- PYODIDE:env --------- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


def equation_deux_points(x_A, y_A, x_B, y_B):
    ...


def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = y_u
    b = -x_u
    c = -a * x_A - b * y_A
    return (a, b, c)


# --------- PYODIDE:code --------- #
def equation_deux_points(x_A, y_A, x_B, y_B):
    ...


# --------- PYODIDE:corr --------- #
def equation_deux_points(x_A, y_A, x_B, y_B):
    (x_AB, y_AB) = vecteur(x_A, y_A, x_B, y_B)
    return equation_point_vecteur(x_A, y_A, x_AB, y_AB)


# --------- PYODIDE:tests --------- #
x_A, y_A = 2, 3
x_B, y_B = 1, -2
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_B, y_B = 2, -2
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_B, y_B = 1, 3
(a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
assert a * x_A + b * y_A + c == 0
assert a * x_B + b * y_B + c == 0
# --------- PYODIDE:secrets --------- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_B = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_B = randrange(-200, 200) // 2
    if (x_A, y_A) == (x_B, y_B):
        continue
    (a, b, c) = equation_deux_points(x_A, y_A, x_B, y_B)
    assert (
        a * x_A + b * y_A + c == 0 and a * x_B + b * y_B + c == 0
    ), f"Erreur avec A {(x_A, y_A)}, B {(x_B, y_B)}"

