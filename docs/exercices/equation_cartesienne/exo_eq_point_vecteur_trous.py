# --------- PYODIDE:env --------- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


def equation_deux_points(x_A, y_A, x_u, y_u):
    ...


def equation_point_vecteur(x_A, y_A, x_u, y_u):
    ...


# --------- PYODIDE:code --------- #
def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = ...
    b = ...
    c = ...
    return (a, b, c)


# --------- PYODIDE:corr --------- #
def equation_point_vecteur(x_A, y_A, x_u, y_u):
    a = y_u
    b = -x_u
    c = -a * x_A - b * y_A
    return (a, b, c)


# --------- PYODIDE:tests --------- #
x_A, y_A = 2, 3
x_u, y_u = -1, -5
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_u, y_u = 0, -5
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0

x_A, y_A = 2, 3
x_u, y_u = -1, 0
(a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
assert a * x_A + b * y_A + c == 0
x_B = x_A + x_u
y_B = y_A + y_u
assert a * x_B + b * y_B + c == 0
# --------- PYODIDE:secrets --------- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) // 2
    x_u = randrange(-200, 200) // 2
    y_A = randrange(-200, 200) // 2
    y_u = randrange(-200, 200) // 2
    x_B = x_A + x_u
    y_B = y_A + y_u
    (a, b, c) = equation_point_vecteur(x_A, y_A, x_u, y_u)
    assert (
        a * x_A + b * y_A + c == 0 and a * x_B + b * y_B + c == 0
    ), f"Erreur avec A {(x_A, y_A)}, u {(x_u, y_u)}"


