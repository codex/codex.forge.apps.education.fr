# --------- PYODIDE:code --------- #

def est_semi_magique(carre):
    ...

# --------- PYODIDE:corr --------- #

def est_semi_magique(carre):
    n = len(carre)
    somme_commune = 0
    for j in range(n):
        somme_commune = somme_commune + carre[0][j]
    for i in range(1, n):
        somme_ligne_i = 0
        for j in range(n):
            somme_ligne_i = somme_ligne_i + carre[i][j]
        if somme_ligne_i != somme_commune:
            return False
    for j in range(n):
        somme_colonne_j = 0
        for i in range(n):
            somme_colonne_j = somme_colonne_j + carre[i][j]
        if somme_colonne_j != somme_commune:
            return False
    return True

# --------- PYODIDE:tests --------- #

ex_1 = [
    [4, 5, 7],
    [5, 7, 4],
    [7, 4, 5],
]
assert est_semi_magique(ex_1) is True

ex_2 = [
    [3, 4, 5],
    [4, 4, 4],
    [5, 4, 3],
]
assert est_semi_magique(ex_1) is True

ex_3 = [
    [3, 4, 5, 6],
    [7, 4, 5, 2],
    [1, 5, 4, 8],
    [9, 5, 4, 0],
]
assert est_semi_magique(ex_3) is False  # Contre exemple

# --------- PYODIDE:secrets --------- #

# Tests

ex_1 = [
    [4, 5, 7],
    [5, 7, 4],
    [7, 4, 5],
]
assert est_semi_magique(ex_1) is True

ex_2 = [
    [3, 4, 5],
    [4, 4, 4],
    [5, 4, 3],
]
assert est_semi_magique(ex_1) is True

ex_3 = [
    [3, 4, 5, 6],
    [7, 4, 5, 2],
    [1, 5, 4, 8],
    [9, 5, 4, 0],
]
assert est_semi_magique(ex_3) is False

# Autres tests

def cree_matrice(nombres):
    n = int(sqrt(len(nombres)))
    tableau = [[nombres[j + i * n] for j in range(n)] for i in range(n)]
    return tableau


carre_5 = [[1, 4, 14, 15], [13, 16, 2, 3], [8, 5, 11, 10], [12, 9, 7, 6]]
carre_6 = [[1, 4, 14, 15], [13, 16, 2, 3], [8, 5, 11, 10], [12, 9, 7, 5]]
carre_7 = [[2, 4, 14, 15], [13, 16, 2, 3], [8, 5, 11, 10], [12, 9, 7, 6]]
carre_8 = [[1, 2, 3], [1, 2, 3], [1, 2, 3]]
carre_9 = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]

assert est_semi_magique(carre_5) is True, f"Avec {carre_5}"
assert est_semi_magique(carre_6) is False, f"Avec {carre_6}"
assert est_semi_magique(carre_7) is False, f"Avec {carre_7}"
assert est_semi_magique(carre_8) is False, f"Avec {carre_8}"
assert est_semi_magique(carre_9) is False, f"Avec {carre_9}"


from random import randrange
from copy import deepcopy

n = 10
base = [randrange(10**9) for _ in range(n)] * 2
carre = []
for i in range(n):
    carre.append(base[i : i + n])

gros_test = deepcopy(carre)
assert est_semi_magique(gros_test) is True, "Erreur avec un gros test"

truc = randrange(-(10**8), 10**8)
truc = 42 if not truc else truc

for i in range(n):
    for j in range(n):
        gros_test = deepcopy(carre)
        gros_test[i][j] += truc
        assert est_semi_magique(gros_test) is False, "Erreur avec un gros test"

for k_e in range(n - 1):
    for k_s in range(k_e + 1, n):

        for i in range(n):
            gros_test = deepcopy(carre)
            gros_test[i][k_s] += truc
            gros_test[i][k_e] -= truc
            assert est_semi_magique(gros_test) is False, "Erreur, il faut aussi vérifier les colonnes"

        for j in range(n):
            gros_test = deepcopy(carre)
            gros_test[k_s][j] += truc
            gros_test[k_e][j] -= truc
            assert est_semi_magique(gros_test) is False, "Erreur, il faut aussi vérifier les lignes"
