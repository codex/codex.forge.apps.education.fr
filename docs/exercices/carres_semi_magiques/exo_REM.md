On peut aussi envisager une solution dans un style fonctionnel.

Dans un premier temps, avec la fonction `sum`

!!! info "`sum` et le style fonctionnel"

    La fonction `sum` prend une liste de nombres (ou une compréhension), ou bien un itérable de manière générale et renvoie la somme de toutes les valeurs.

    ```pycon
    >>> sum([0, 1, 2, 3])
    6
    >>> sum(range(4))
    6
    >>> sum(x/2 for x in range(4))
    3.0
    ```

```python
def est_semi_magique(carre):
    n = len(carre)
    somme_commune = sum(carre[0])
    for i in range(1, n):
        if sum(carre[i]) != somme_commune:
            return False
    for j in range(n):
        if sum(carre[i][j] for i in range(n)) != somme_commune:
            return False
    return True
```


??? note "Si vous avez étudié le paradigme fonctionnel, vous pouvez lire les explications suivantes"

    Utilisons la fonction `all`

    !!! info "`all` et le style fonctionnel"

        La fonction `all` prend une liste de booléens (ou une compréhension, comme ici, ou bien un itérable de manière générale) et renvoie `True` uniquement si **tous** les éléments valent `True`, sinon, renvoie `False`.

        ```pycon
        >>> all([True, True, True, True])
        True
        >>> all([True, False, True, True, True])
        False
        ```

    ```python
    def est_semi_magique(carre):
        n = len(carre)
        somme_commune = sum(carre[0])

        return (
            all(sum(carre[i][j] for j in range(n)) == somme_commune for i in range(n))
            and
            all(sum(carre[i][j] for i in range(n)) == somme_commune for j in range(n))
        )
    ```


