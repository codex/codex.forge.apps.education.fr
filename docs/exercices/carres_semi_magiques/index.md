---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Carrés semi-magiques
tags:
    - double boucle
    - grille
difficulty: 220
maj: 09/11/2024
---

Nous travaillons dans cet exercice sur des tableaux carrés d'entiers positifs.

Nous appelons « carré d'ordre $n$ » un tableau de $n$ lignes et $n$ colonnes dont chaque case contient un entier positif.  


!!! info "Carré semi-magique"

    Un carré est dit « semi-magique » lorsque les sommes des éléments situés sur chaque ligne, chaque
    colonne sont égales. Cette somme est appelée *constante magique* ou *densité* du carré semi-magique.

!!! info "Carré semi-magique d'ordre 3"

    === "Exemple 1"

        ```python
        ex_1 = [
          [4, 5, 7],
          [5, 7, 4],
          [7, 4, 5],
        ]
        ```

        Avec une somme commune $4+5+7 = 16$ pour chaque ligne et chaque colonne, cette grille est semi-magique.

    === "Exemple 2"

        ```python
        ex_2 = [
          [3, 4, 5],
          [4, 4, 4],
          [5, 4, 3],
        ]
        ```

        Avec une somme commune $12$ pour chaque ligne et chaque colonne, cette grille est semi-magique.

    === "Exemple 3"

        ```python
        ex_3 = [
          [3, 4, 5, 6],
          [7, 4, 5, 2],
          [1, 5, 4, 8],
          [9, 5, 4, 0],
        ]
        ```

        `ex_3` n'est pas semi-magique car la somme des lignes est égale à 18, mais celle de la première colonne est égale à 20.


!!! question "Objectif"

    Écrire une fonction `est_semi_magique` :
    
    - qui prend en argument :
        - `carre`, une liste de listes d'entiers, dont chaque élément est une liste de même taille que `carre`.
    - et qui renvoie un booléen :
        - `#!py True` : si `carre` est un carré semi-magique
        - `#!py False` : sinon.


???+ example "Exemples"

    ```pycon title=""
    >>> ex_1 = [
    ...   [4, 5, 7],
    ...   [5, 7, 4],
    ...   [7, 4, 5],
    ... ]
    >>> est_semi_magique(ex_1)
    True
    >>> ex_2 = [
    ...   [3, 4, 5],
    ...   [4, 4, 4],
    ...   [5, 4, 3],
    ... ]
    >>> est_semi_magique(ex_2)
    True
    >>> ex_3 = [
    ...   [3, 4, 5, 6],
    ...   [7, 4, 5, 2],
    ...   [1, 5, 4, 8],
    ...   [9, 5, 4, 0],
    ... ]
    >>> est_semi_magique(ex_3)
    False
    ```


{{ IDE('exo') }}
