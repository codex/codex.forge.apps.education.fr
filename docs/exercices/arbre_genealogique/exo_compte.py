# --- PYODIDE:code --- #
def compte(arbre, nom): 
    ...


# --- PYODIDE:corr --- #
def compte(arbre, nom):
    if arbre is None:
        return 0
    sag, personne, sad = arbre
    n = 0
    if personne == nom:
        n = 1
    return n + compte(sag, nom) + compte(sad, nom)


# --- PYODIDE:tests --- #
une_gen = (None, "Pierre", None)
assert compte(une_gen, "Pierre") == 1
assert compte(une_gen, "Martin") == 0
deux_gen = ((None, "Paul", None), "Pierre", (None, "Jacqueline", None))
assert compte(deux_gen, "Pierre") == 1
trois_gen = (
    ((None, "Pierre", (None, "Jacqueline", None)), "Paul", None),
    "Pierre",
    ((None, "Pierre", None), "Jacqueline", None),
)
assert compte(trois_gen, "Pierre") == 3
assert compte(trois_gen, "Jacqueline") == 2
# --- PYODIDE:secrets --- #
a = (None, "u", None)
b = (None, "b", None)
c = (None, "w", None)
d = (None, "u", None)
e = (None, "e", None)
f = (None, "w", None)
g = (None, "u", None)
h = (None, "u", None)
i = (a, "i", b)
j = (None, "j", c)
k = (d, "w", None)
l = (e, "l", f)
m = (g, "u", None)
n = (h, "n", i)
o = (j, "o", None)
p = (None, "w", k)
q = (None, "q", l)
r = (m, "u", n)
s = (o, "w", p)
t = (None, "t", q)
u = (r, "u", t)
v = (None, "t", s)
w = (u, "w", v)

# {nom : compte(w, nom)}
attendus = {
    "a": 0,
    "b": 1,
    "c": 0,
    "d": 0,
    "e": 1,
    "f": 0,
    "g": 0,
    "h": 0,
    "i": 1,
    "j": 1,
    "k": 0,
    "l": 1,
    "m": 0,
    "n": 1,
    "o": 1,
    "p": 0,
    "q": 1,
    "r": 0,
    "s": 0,
    "t": 2,
    "u": 7,
    "v": 0,
    "w": 6,
}

for nom, attendu in attendus.items():
    ab = w
    assert compte(w, nom) == attendu, f"Erreur avec {ab = } et {nom = }"
