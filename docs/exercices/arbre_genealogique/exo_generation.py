# --- PYODIDE:code --- #
def generation(arbre, n): 
    ...


# --- PYODIDE:corr --- #
def generation(arbre, n): 
    if arbre is None:
        return []
    sag, personne, sad = arbre
    if n == 1:
        return [personne]
    return generation(sag, n - 1) + generation(sad, n - 1)


# --- PYODIDE:tests --- #
une_gen = (None, "Moi", None)
assert generation(une_gen, 1) == ["Moi"]
deux_gen = ((None, "Papa", None), "Moi", (None, "Maman", None))
assert generation(deux_gen, 2) == ["Papa", "Maman"]
trois_gen = (
    (None, "Papa", (None, "Mamy P.", None)),
    "Moi",
    ((None, "Papy M.", None), "Maman", None),
)
assert generation(trois_gen, 3) == ["Mamy P.", "Papy M."]
# --- PYODIDE:secrets --- #
a = (None, "a", None)
b = (None, "b", None)
c = (None, "c", None)
d = (None, "d", None)
e = (None, "e", None)
f = (None, "f", None)
g = (None, "g", None)
h = (None, "h", None)
i = (a, "i", b)
j = (None, "j", c)
k = (d, "k", None)
l = (e, "l", f)
m = (g, "m", None)
n = (h, "n", i)
o = (j, "o", None)
p = (None, "p", k)
q = (None, "q", l)
r = (m, "r", n)
s = (o, "s", p)
t = (None, "t", q)
u = (r, "u", t)
v = (None, "t", s)
w = (u, "w", v)

# {n : generation(w, n)}
attendus = {
    1: ["w"],
    2: ["u", "t"],
    3: ["r", "t", "s"],
    4: ["m", "n", "q", "o", "p"],
    5: ["g", "h", "i", "l", "j", "k"],
    6: ["a", "b", "e", "f", "c", "d"],
    7: [],
}

for n in range(1, 8):
    ab = w
    attendu = attendus[n]
    assert generation(ab, n) == attendu, f"Erreur avec {ab = } et {n = }"
