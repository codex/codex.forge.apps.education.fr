---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Arbre généalogique
tags:
    - arbre binaire
    - récursivité
difficulty: 200
maj: 15/01/2025
---

Un généalogiste amateur décide de représenter ses travaux avec Python.

[![Arbre généalogique de Carl Gustav Bielke](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Carl_Gustav_Bielke_antavla_001.jpg/567px-Carl_Gustav_Bielke_antavla_001.jpg){.center width=30%}](https://commons.wikimedia.org/wiki/File:Carl_Gustav_Bielke_antavla_001.jpg){target="_blank"}

Etant face à l'arbre généalogique des parents, grands-parents, etc d'un individu, il utilise pour ce faire des arbres binaires représentés ainsi :

* l'arbre binaire vide est représenté par `#!py None` ;

* un arbre binaire non vide est représenté par un tuple de trois éléments `#!py (sag, personne, sad)` dans lequel :

    * `#!py sag` est le sous-arbre gauche ;
    * `#!py personne` est le nom de la personne à la racine de l'arbre ;
    * `#!py sad` est le sous-arbre droit.


Il suit la convention de la généalogique qui veut que les pères soient placés sur la gauche et les mères sur la droite.

Ainsi l'arbre généalogique de base :

```text title="Arbre de base"
Pierre         Pauline
    \         /
     \       /
      Jacques
```

sera représenté en Python par :

```python title="Arbre de base en Python"
((None, "Pierre", None), "Jacques", (None, "Pauline", None))
```

Il arrive lors de la construction d'un arbre généalogique que l'on ignore l'ascendance d'une personne : l'arbre est interrompu sur certaines branches et est donc déséquilibré. Par exemple :

```text title="Arbre déséquilibré"
Pierre      ?    ?         Martine
    \      /      \       /
     \    /        \     /
      Paul          Aline
          \        /
           \      /
            Pierre        ?
                \        /
                 \      /
                  Marion
```

Dans ce cas, les parents inconnus sont représentés par des arbres vides :
```python title="Arbre déséquilibré en Python"
((((None, "Pierre", None), "Paul", None), "Pierre", (None, "Aline", (None, "Martine", None))), "Marion", None)
```

On cherche dans cet exercice à effectuer différents calculs sur des arbres généalogiques.

??? question "1. Personnes présentes"

    Écrire la fonction `#!py nb_presents` qui prend en paramètre un tuple `#!py arbre` représentant un arbre généalogique et renvoie le nombre de personnes présentes dans cet arbre.

    ???+ example "Exemples"

        ```pycon title=""
        >>> une_gen = (None, "Moi", None)
        >>> nb_presents(une_gen)
        1
        >>> deux_gen = ((None, "Papa", None), "Moi", (None, "Maman", None))
        >>> nb_presents(deux_gen)
        3
        >>> trois_gen = ((None, "Papa", (None, "Mamy P.", None)), "Moi", ((None, "Papy M.", None), "Maman", None))
        >>> nb_presents(trois_gen)
        5
        ```

    {{ IDE('exo_presents') }}

??? question "2. Nombre de générations"

    On définit le nombre de générations d'un arbre généalogique comme le nombre maximal de personnes rencontrées en remontant depuis la racine de l'arbre jusqu'à un ancêtre.
    Un arbre généalogique ne comptant qu'une seule personne a donc un nombre de génération égal à `#!py 1`.

    Écrire la fonction `#!py nb_generations` qui prend en paramètre un tuple `#!py arbre` représentant un arbre généalogique et renvoie sa hauteur.

    ???+ example "Exemples"

        ```pycon title=""
        >>> une_gen = (None, "Moi", None)
        >>> nb_generations(une_gen)
        1
        >>> deux_gen = ((None, "Papa", None), "Moi", (None, "Maman", None))
        >>> nb_generations(deux_gen)
        2
        >>> trois_gen = ((None, "Papa", (None, "Mamy P.", None)), "Moi", ((None, "Papy M.", None), "Maman", None))
        >>> nb_generations(trois_gen)
        3
        ```

    {{ IDE('exo_hauteur') }}

??? question "3. Compter les prénoms identiques"

    Il arrive que certains prénoms apparaissent [plusieurs fois](https://fr.wikipedia.org/wiki/Famille_Bernoulli){target="_blank"} dans un arbre généalogique.
    
    Écrire la fonction `#!py compte` qui prend en paramètre un tuple `#!py arbre` représentant un arbre généalogique ainsi qu'un nom (au format `#!py str`) et renvoie le nombre d'apparitions de ce nom dans l'arbre généalogique.
    

    ???+ example "Exemples"

        ```pycon title=""
        >>> une_gen = (None, "Pierre", None)
        >>> compte(une_gen, "Pierre")
        1
        >>> compte(une_gen, "Martin")
        0
        >>> deux_gen = ((None, "Paul", None), "Pierre", (None, "Jacqueline", None))
        >>> compte(deux_gen, "Pierre")
        1
        >>> trois_gen = (((None, "Pierre", (None, "Jacqueline", None)), "Paul", None), "Pierre", ((None, "Pierre", None), "Jacqueline", None))
        >>> compte(trois_gen, "Pierre")
        3
        >>> compte(trois_gen, "Jacqueline")
        2
        ```

    {{ IDE('exo_compte') }}
        
??? question "4. Extraire une génération"

    On appelle *génération* $n$ dans un arbre, l'ensemble des personnes définies comme suit :
    
    * la personne à la racine de l'arbre forme la génération $1$ ;
    * ses parents forment la génération $2$ ;
    * ses grands-parents la génération $3$ ;
    * ...

    Écrire la fonction `#!py generation` qui prend en paramètre un tuple `#!py arbre` représentant un arbre généalogique ainsi qu'un entier strictement positif `#!py n` et renvoie la liste des noms des personnes situés à la génération `#!py n`.
    
    La liste sera ordonnée de façon à correspondre à la représentation graphique de l'arbre : le nom d'une personne apparaissant sur la gauche sera placé plus tôt dans la liste que le nom d'une personne apparaissant sur la droite.

    ???+ example "Exemples"

        ```pycon title=""
        >>> une_gen = (None, "Moi", None)
        >>> generation(une_gen, 1)
        ['Moi']
        >>> deux_gen = ((None, "Papa", None), "Moi", (None, "Maman", None))
        >>> generation(deux_gen, 2)
        ['Papa', 'Maman']
        >>> trois_gen = ((None, "Papa", (None, "Mamy P.", None)), "Moi", ((None, "Papy M.", None), "Maman", None), )
        >>> generation(trois_gen, 3)
        ['Mamy P.', 'Papy M.']
        ```

    === "Version vide"
        {{ IDE('exo_generation') }}
    === "Version avec un parcours en largeur"
    
        On peut résoudre cet exercice à l'aide d'un parcours en largeur de l'arbre.
        
        Pour ce faire on crée une liste `#!py actuelle` qui contient tous les nœuds non vides de la génération actuelle.
        
        On met alors à jour cette liste en remontant à la génération précédente jusqu'à atteindre le rang de la génération souhaité.
        
        {{ IDE('exo_generation_file') }}
    
    === "Version avec un parcours en profondeur"
    
        On peut résoudre cet exercice à l'aide d'un parcours en profondeur de l'arbre.
        
        Pour ce faire on utilise une structure de pile dans laquelle on ajoute initialement la racine de
        l'arbre ainsi que sa hauteur `#!py 1`.
        
        **Attention** toutefois à l'ordre dans lequel on empile les différents nœuds : une pile répond au modèle « *Dernier Entré, Premier Sorti* » !
        
        {{ IDE('exo_generation_pile') }}