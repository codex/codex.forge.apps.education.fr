# --- PYODIDE:code --- #
def nb_presents(arbre): 
    ...


# --- PYODIDE:corr --- #
def nb_presents(arbre):
    if arbre is None:
        return 0
    sag, personne, sad = arbre
    return 1 + nb_presents(sag) + nb_presents(sad)


# --- PYODIDE:tests --- #
une_gen = (None, "Moi", None)
assert nb_presents(une_gen) == 1
deux_gen = ((None, "Papa", None), "Moi", (None, "Maman", None))
assert nb_presents(deux_gen) == 3
trois_gen = (
    (None, "Papa", (None, "Mamy P.", None)),
    "Moi",
    ((None, "Papy M.", None), "Maman", None),
)
assert nb_presents(trois_gen) == 5
# --- PYODIDE:secrets --- #
a = (None, "a", None)
b = (None, "b", None)
c = (None, "c", None)
d = (None, "d", None)
e = (None, "e", None)
f = (None, "f", None)
g = (None, "g", None)
h = (None, "h", None)
i = (a, "i", b)
j = (None, "j", c)
k = (d, "k", None)
l = (e, "l", f)
m = (g, "m", None)
n = (h, "n", i)
o = (j, "o", None)
p = (None, "p", k)
q = (None, "q", l)
r = (m, "r", n)
s = (o, "s", p)
t = (None, "t", q)
u = (r, "u", t)
v = (None, "t", s)
w = (u, "w", v)

# {ab : (taille, hauteur)}
attendus = {
    a: (1, 1),
    b: (1, 1),
    c: (1, 1),
    d: (1, 1),
    e: (1, 1),
    f: (1, 1),
    g: (1, 1),
    h: (1, 1),
    i: (3, 2),
    j: (2, 2),
    k: (2, 2),
    l: (3, 2),
    m: (2, 2),
    n: (5, 3),
    o: (3, 3),
    p: (3, 3),
    q: (4, 3),
    r: (8, 4),
    s: (7, 4),
    t: (5, 4),
    u: (14, 5),
    v: (8, 5),
    w: (23, 6),
}

for nom in "abcdefghijklmnopqrstuvw":
    ab = globals()[nom]
    attendu, _ = attendus[ab]
    assert nb_presents(ab) == attendu, f"Erreur avec {ab = }"
