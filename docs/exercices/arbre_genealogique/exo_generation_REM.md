Cette solution est rapide a écrire mais nécessite beaucoup d'étapes de « recopiage » de listes.

En effet lors de chaque appel récursif, on fait `#!py return generation(sag, n - 1) + generation(sad, n - 1)`. Or, les valeurs renvoyées par chaque fonction étant des listes, Python doit donc additionner celles-ci.

A chaque appel récursif, Python doit donc *recopier l'ensemble des valeurs* renvoyées dans une nouvelle liste de somme... Ce comportement peut s'avérer coûteux.