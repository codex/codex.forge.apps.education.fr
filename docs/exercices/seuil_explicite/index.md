---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Détermination d'un seuil (1)
tags:
    - à trous
    - boucle
    - maths
difficulty: 150
---

Soit $(u_n)$ la suite définie pour tout entier naturel $n$ par :

$$u_n=\frac{5n^2+13}{n^2+2}$$

On peut montrer que cette suite tend vers $5$ quand $n$ tend vers $+\infty$. Cela signifie que pour toute valeur de $\epsilon > 0$, il existe un rang $N$ à partir duquel on a, pour tout $n \geqslant N$ :

$$5 - \epsilon \leqslant u_n \leqslant 5 + \epsilon$$

La dernière condition peut aussi s'écrire de façon plus concise:
 
$$|u_n - 5| \leqslant \epsilon$$
 
On rappelle à ce titre que $|a - b|$ est la valeur absolue de $a-b$ et peut s'interpréter comme la *distance* entre les nombres $a$ et $b$. Python permet de calculer la valeur absolue d'un nombre `x` en faisant `#!py abs(x)`.

Écrire le code de la fonction `seuil` qui prend en paramètre le nombre `precision` et renvoie
la valeur du plus petit entier tel que l'on ait $|u_n - 5| \leqslant \text{precision}$.

On garantit que `precision` est un nombre réel supérieur ou égal à $10^{-10}$.

{{ remarque('assertion') }}

???+ example "Exemples"

    ```pycon title=""
    >>> seuil(1)    # u_1 = 6 et ǀ6 - 5ǀ ⩽ 1
    1
    >>> seuil(0.1)  # u_6 ≃ 5,08 et ǀ5,08 - 5ǀ <= 0,1
    6
    ```
    
=== "Version vide"
    {{ IDE('./pythons/exo_b')}}
=== "Version à compléter"
    {{ IDE('./pythons/exo_a')}}

    
    
