

# --------- PYODIDE:code --------- #

def seuil(precision):
    ...

# --------- PYODIDE:corr --------- #

def seuil(precision):
    n = 0
    u = 6.5
    while abs(u - 5) > precision:
        n = n + 1
        u = (5 * n**2 + 13) / (n**2 + 2)
    return n

# --------- PYODIDE:tests --------- #

assert seuil(1) == 1  # u_1 = 6 et ǀ6 - 5ǀ ⩽ 1
assert seuil(0.1) == 6  # u_6 ≃ 5,08 et ǀ5,08 - 5ǀ <= 0,1

# --------- PYODIDE:secrets --------- #


# tests secrets
attendus = (
    (0.01, 18),
    (0.001, 55),
    (0.0001, 174),
    (1e-05, 548),
    (1e-06, 1733),
    (1e-07, 5478),
    (1e-08, 17321),
    (1e-09, 54773),
    (1e-10, 173206),
)
for precision, n in attendus:
    assert seuil(precision) == n, f"Erreur avec {precision = }"