---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Produit maximal de k termes consécutifs
tags:
    - programmation dynamique
maj: 01/03/2024
---



**Il est conseillé d'avoir résolu [Somme maximale de k termes consécutifs](../k_consecutifs_1/index.md) avant d'aborder cet exercice.**

Écrire une fonction `produit_maxi` qui prend en paramètres un tableau d’entiers positifs ou nuls `valeurs`, et un entier strictement positif `k`. Cette fonction doit renvoyer le produit maximal de `k` entiers consécutifs du tableau `valeurs`.

On garantit que le tableau `valeurs` est de taille au moins égale à `k` et que `k` est un entier strictement positif.

???+ example "Examples"

    ```pycon title=""
    >>> produit_maxi([0, 1, 2, 3, 2, 1, 0], 3)  # Pour les termes consécutifs 2, 3, 2
    12
    >>> produit_maxi([0, 1, 2, 3, 2, 1, 0], 1)  # Pour le terme 3
    3
    ```

??? warning "Nombre d'opérations limité"

    Il est possible de résoudre ce problème en calculant, pour chaque fenêtre de `k` nombres, le produit des `k` éléments qu'elle contient. Cette méthode effectue `(n - k + 1) * (k - 1)` multiplications.
    
    On peut toutefois être plus efficace en effectuant moins de `n` multiplications et `n` divisions.
    
    La difference est sensible : pour un tableau de $10\,000$ valeurs et une valeur de `k` égale à $100$, la première approche effectue $(10\,000 - 100 + 1) \times (100 - 1) = 980\,199$ multiplications alors que la seconde fera au maximum $2 \times 10\,000 = 20\,000$ multiplications et divisions.
    
    **On limite donc le nombre de multiplications et de divisions possibles à trois fois le nombre d'éléments**.

??? tip "Indice"

    On pourra commencer par faire le produit des valeurs **non nulles** parmi les `k` premières, ainsi que le que le décompte des zéros présents dans cette fenêtre de `k` nombres.
    
    On initialisera ensuite une variable `maxi` avant de parcourir les valeurs restantes en mettant à jour ces trois variables.


=== "Version vide"
    {{ IDE('./pythons/exo_a') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_b') }}


