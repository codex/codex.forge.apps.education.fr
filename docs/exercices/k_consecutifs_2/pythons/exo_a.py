

# --------- PYODIDE:code --------- #

def produit_maxi(valeurs, k):
    ...

# --------- PYODIDE:corr --------- #

def produit_maxi(valeurs, k):
    zeros_dans_fenetre = 0
    produit_non_nuls = 1
    for i in range(k):
        x = valeurs[i]
        if x == 0:
            zeros_dans_fenetre += 1
        else:
            produit_non_nuls *= x
    if zeros_dans_fenetre == 0:
        maxi = produit_non_nuls
    else:
        maxi = 0

    for i in range(k, len(valeurs)):
        x = valeurs[i - k]
        if x == 0:
            zeros_dans_fenetre -= 1
        else:
            produit_non_nuls //= x

        x = valeurs[i]
        if x == 0:
            zeros_dans_fenetre += 1
        else:
            produit_non_nuls *= x

        if zeros_dans_fenetre == 0 and produit_non_nuls > maxi:
            maxi = produit_non_nuls

    return maxi

# --------- PYODIDE:tests --------- #

assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 12
assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3

# --------- PYODIDE:secrets --------- #


# tests secrets
assert produit_maxi([0], 1) == 0
assert produit_maxi([0, 7], 1) == 7
assert produit_maxi([7, 0], 1) == 7
assert produit_maxi([5, 7], 1) == 7
assert produit_maxi([7, 5], 1) == 7
assert produit_maxi([5, 7], 2) == 35
assert produit_maxi([7, 5], 2) == 35
assert produit_maxi([1, 5, 7], 2) == 35
assert produit_maxi([7, 5, 1], 2) == 35
assert produit_maxi([0, 10, 2, 3, 2, 9, 0], 4) == 120
assert produit_maxi([0, 9, 2, 3, 2, 10, 0], 4) == 120
assert produit_maxi([2, 2, 2, 3, 2, 2, 2], 2) == 6
assert produit_maxi([0, 2, 2, 0, 2, 2, 0], 3) == 0
assert produit_maxi([0, 2, 2, 0, 2, 2, 1], 3) == 4

class Entier(int):
    operations_effectuees = 0

    def __mul__(self, autre):
        Entier.operations_effectuees += 1
        return Entier(super().__mul__(autre))

    def __rmul__(self, autre):
        Entier.operations_effectuees += 1
        return Entier(super().__mul__(autre))

    def __floordiv__(self, autre):
        Entier.operations_effectuees += 1
        return Entier(super().__floordiv__(autre))

    def __rfloordiv__(self, autre):
        Entier.operations_effectuees += 1
        return Entier((autre).__floordiv__(self))

    def __truediv__(self, autre):
        Entier.operations_effectuees += 1
        return Entier(super().__truediv__(autre))

    def __rtruediv__(self, autre):
        Entier.operations_effectuees += 1
        return Entier((autre).__truediv__(self))

def _produit_maxi(valeurs, k):
    zeros_dans_fenetre = 0
    produit_non_nuls = 1
    for i in range(k):
        x = valeurs[i]
        if x == 0:
            zeros_dans_fenetre += 1
        else:
            produit_non_nuls *= x
    if zeros_dans_fenetre == 0:
        maxi = produit_non_nuls
    else:
        maxi = 0

    for i in range(k, len(valeurs)):
        x = valeurs[i]
        if x == 0:
            zeros_dans_fenetre += 1
        else:
            produit_non_nuls *= x

        x = valeurs[i - k]
        if x == 0:
            zeros_dans_fenetre -= 1
        else:
            produit_non_nuls //= x

        if zeros_dans_fenetre == 0 and produit_non_nuls > maxi:
            maxi = produit_non_nuls

    return maxi

from random import randrange, shuffle
taille = 10**4
k = 100
valeurs = [Entier(randrange(1, 100)) for _ in range(taille - 10)] + [Entier(0)] * 10
shuffle(valeurs)
attendu = _produit_maxi(valeurs, k)
Entier.operations_effectuees = 0
assert produit_maxi(valeurs, k) == attendu, f"Erreur sur un tableau de {taille} nombres"
operations_effectuees = Entier.operations_effectuees
assert operations_effectuees <= 3 * taille, f"Votre code effectue {operations_effectuees} opérations, la limite est à {3 * taille}"