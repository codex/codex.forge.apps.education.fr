# --------- PYODIDE:env --------- #
from js import document

_prefixes_possibles = ["turtle", "tt"]
if "restart" in globals():
    restart()
else:
    for pref in _prefixes_possibles:
        if pref in globals():
            p = eval(pref)
            p.restart()
            break


def m_a_j():
    cible = "flocon"
    if "done" in globals():
        done()
        document.getElementById(cible).innerHTML = Screen().html
    else:
        for pref in _prefixes_possibles:
            if pref in globals():
                p = eval(pref)
                p.done()
                document.getElementById(cible).innerHTML = p.Screen().html
                break


def modele():
    def __courbe__(longueur, etape):
        if etape == 0:
            forward(longueur)
        else:
            _longueur_sur_3_ = longueur / 3
            __courbe__(_longueur_sur_3_, etape - 1)
            left(60)
            __courbe__(_longueur_sur_3_, etape - 1)
            right(120)
            __courbe__(_longueur_sur_3_, etape - 1)
            left(60)
            __courbe__(_longueur_sur_3_, etape - 1)

    speed(50)
    penup()
    setheading(0)
    goto(-150, 100)
    pendown()
    pencolor("green")
    for _ in range(3):
        __courbe__(300, 5)
        right(120)
    penup()
    setheading(0)
    goto(-150, 100)
    pendown()
    pencolor("var(--md-admonition-fg-color)")


def courbe(longueur, etape):
    if etape == 0:
        forward(longueur)
    else:
        courbe(longueur / 3, etape - 1)
        left(60)
        courbe(longueur / 3, etape - 1)
        right(120)
        courbe(longueur / 3, etape - 1)
        left(60)
        courbe(longueur / 3, etape - 1)


# --- PYODIDE:code --- #
from turtle import *

# Réglages initiaux
speed(50)        # la tortue se déplace rapidement
penup()          # le crayon est levé
goto(-150, 100)  # la tortue se déplace sur la gauche de la figure
pendown()        # le crayon est baissé


def flocon(longueur, etape): 
    ...


# modele()  # Décommenter pour comparer votre figure avec le modèle

flocon(300, 5)
# --- PYODIDE:corr --- #
from turtle import *

# Réglages initiaux
speed(50)        # la tortue se déplace rapidement
penup()          # le crayon est levé
goto(-150, 100)  # la tortue se déplace sur la gauche de la figure
pendown()        # le crayon est baissé


def flocon(longueur, etape):
    for _ in range(3):
        courbe(longueur, etape)
        right(120)


flocon(300, 5)
# --- PYODIDE:post --- #
if "post_async" in globals():
    await post_async()
if "Screen" in globals():
    if Screen().html is None:
        forward(0)
    m_a_j()
elif "turtle" in globals():
    if turtle.Screen().html is None:
        turtle.forward(0)
    m_a_j()

# --------- PYODIDE:post_term --------- #
if (any(pref in globals() for pref in _prefixes_possibles) or "done" in globals()) and "m_a_j" in globals():
    m_a_j()
