# --------- PYODIDE:env --------- #
from js import document

_prefixes_possibles = ["turtle", "tt"]
if "restart" in globals():
    restart()
else:
    for pref in _prefixes_possibles:
        if pref in globals():
            p = eval(pref)
            p.restart()
            break


def m_a_j():
    cible = "courbe"
    if "done" in globals():
        done()
        document.getElementById(cible).innerHTML = Screen().html
    else:
        for pref in _prefixes_possibles:
            if pref in globals():
                p = eval(pref)
                p.done()
                document.getElementById(cible).innerHTML = p.Screen().html
                break


def modele():
    def __courbe__(longueur, etape):
        if etape == 0:
            forward(longueur)
        else:
            _longueur_sur_3 = longueur / 3
            __courbe__(_longueur_sur_3, etape - 1)
            left(60)
            __courbe__(_longueur_sur_3, etape - 1)
            right(120)
            __courbe__(_longueur_sur_3, etape - 1)
            left(60)
            __courbe__(_longueur_sur_3, etape - 1)

    speed(30)
    penup()
    setheading(0)
    goto(-150, 0)
    setheading(0)
    pendown()
    pencolor("green")
    __courbe__(300, 4)
    pencolor("var(--md-admonition-fg-color)")
    penup()
    setheading(0)
    goto(-150, 0)
    pendown()


def courbe(longueur, etape):
    pass


# --- PYODIDE:code --- #
from turtle import *

# Réglages initiaux
speed(50)      # la tortue se déplace rapidement
penup()        # le crayon est levé
goto(-150, 0)  # la tortue se déplace sur la gauche de la figure
pendown()      # le crayon est baissé


def courbe(longueur, etape): 
    ...


# modele()  # Décommenter pour comparer votre figure avec le modèle

courbe(300, 5)
# --- PYODIDE:corr --- #
from turtle import *

# Réglages initiaux
speed(50)      # la tortue se déplace rapidement
penup()        # le crayon est levé
goto(-150, 0)  # la tortue se déplace sur la gauche de la figure
pendown()      # le crayon est baissé


def courbe(longueur, etape):
    if etape == 0:
        forward(longueur)
    else:
        longueur_sur_3 = longueur / 3
        courbe(longueur_sur_3, etape - 1)
        left(60)
        courbe(longueur_sur_3, etape - 1)
        right(120)
        courbe(longueur_sur_3, etape - 1)
        left(60)
        courbe(longueur_sur_3, etape - 1)


courbe(300, 5)
# --- PYODIDE:post --- #
if "post_async" in globals():
    await post_async()
if "Screen" in globals():
    if Screen().html is None:
        forward(0)
    m_a_j()
elif "turtle" in globals():
    if turtle.Screen().html is None:
        turtle.forward(0)
    m_a_j()

# --------- PYODIDE:post_term --------- #
if (any(pref in globals() for pref in _prefixes_possibles) or "done" in globals()) and "m_a_j" in globals():
    m_a_j()
