import drawsvg as draw
from math import pi, cos, sin


def degres(x):
    return 180 * x / pi


def radians(x):
    return pi * x / 180


def forward(longueur, t=None):
    if t is None:
        t = tortue
    a = radians(t["h"])
    nv_x = t["x"] + longueur * cos(a)
    nv_y = t["y"] + longueur * sin(a)
    d.append(draw.Line(t["x"], t["y"], nv_x, nv_y, **ARGS))
    t["x"] = nv_x
    t["y"] = nv_y


def left(angle, t=None):
    if t is None:
        t = tortue
    t["h"] -= angle


def right(angle, t=None):
    if t is None:
        t = tortue
    t["h"] += angle


def heading(t=None):
    if t is None:
        t = tortue
    return t["h"]


def position(t=None):
    if t is None:
        t = tortue
    return t["x"], t["y"]


def setheading(deg, t=None):
    if t is None:
        t = tortue
    t["h"] = deg


def penup():
    pass


def pendown():
    pass


def goto(x, y, t=None):
    if t is None:
        t = tortue
    t["x"] = x
    t["y"] = y


def portion(longueur, etape):
    if etape == 0:
        forward(longueur)
    else:
        portion(longueur // 3, etape - 1)
        left(60)
        portion(longueur // 3, etape - 1)
        right(120)
        portion(longueur // 3, etape - 1)
        left(60)
        portion(longueur // 3, etape - 1)


def flocon(longueur, etape):
    for _ in range(3):
        portion(longueur, etape)
        right(120)

d = draw.Drawing(320, 400, origin="center")
ARGS = {
    "stroke": "black",
    "stroke_width": 1,
}
tortue = {"x": -150, "y": -100, "h": 0}

etapes = 4

# segment(300, etapes)
# d.save_svg(f"etape_{etapes}_NB.svg")

for etapes in range(6):
    d = draw.Drawing(320, 400, origin="center")
    ARGS = {
        "stroke": "black",
        "stroke_width": 1,
    }
    tortue = {"x": -150, "y": -100, "h": 0}

    flocon(300, etapes)
    d.save_svg(f"flocon_{etapes}_NB.svg")
