---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Dessiner le flocon de Koch
difficulty: 250
tags:
    - récursivité
maj: 16/11/2024
---

{{ remarque('tortue') }}

On souhaite dessiner, à l'aide du module `#!py turtle`, le fameux [flocon de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch).

???+ note "Le flocon à l'étape 5"
    ![Le flocon à l'étape 5](images/flocon_5_NB.svg){ .autolight .center width=35%}

Sous ses aspects très élaborés, cette figure peut se construire de façon **récursive** à partir d'un simple triangle équilatéral !

???+ note "Quelques étapes"
    <center>

    ![Etape 0](images/flocon_0_NB.svg){ .autolight width=20%}
    ![Etape 1](images/flocon_1_NB.svg){ .autolight width=20%}
    ![Etape 2](images/flocon_2_NB.svg){ .autolight width=20%}
    ![Etape 3](images/flocon_3_NB.svg){ .autolight width=20%}

    </center>

??? question "1. La courbe de Koch"

    Avant de construire le **flocon**, construisons la **courbe** de Koch :

    ![Une courbe à l'étape 5](images/etape_5_NB.svg){ .autolight .center width=40%}

    Cette courbe peut se construire de façon **récursive** :

    * on se donne un segment de départ de longueur donnée ;
    * pour obtenir l'étape suivante, on partage ce segment en quatre segments trois fois plus petits ;
    * Répéter l'étape précédente sur chaque petit segment permet de construire l'étape suivante...

    ![D'une courbe à l'autre](images/base_koch.svg){ .autolight .center width=30%}

    Vous devez donc écrire la fonction `#!py courbe` qui prend deux paramètres :
    
    * `#!py longueur` : un entier positif indiquant la largeur totale de la courbe à construire ;
    * `#!py profondeur` : le degré de profondeur de la construction.

    Cette fonction construit la figure comme l'illustrent les exemples ci-dessous :
    
    * `#!py courbe(100, 0)` construit l'étape `#!py 0` (un unique segment de `#!py 100` pixels de long) ;
    
    * `#!py courbe(150, 1)` construit l'étape `#!py 1` (quatre segments mesurant chacun `#!py 50` pixels de long) ;
    
    * `#!py courbe(90, 2)` construit l'étape `#!py 2` (seize segments mesurant chacun `#!py 10` pixels de long).

    La fonction `modele` dessine à l'écran la courbe attendue (`#!py longueur=300, etape=5`). Vous pouvez l'exécuter ou non !

    {{ IDE('pythons/courbe_vide', MODE='delayed_reveal', MAX=10)}}

    {{ figure('courbe', div_class="py_mk_figure center", admo_title='Votre courbe') }}

??? question "2. Le flocon de Koch"

    Maintenant que vous savez construire une courbe de Koch, il vous reste à construire l'ensemble du flocon de Koch ! Un simple code *itératif* suffit : de la même façon qu'un *triangle* est formé de trois *segments* consécutifs, le *flocon* est formé de trois *courbes* consécutives.
    
    Vous pouvez utiliser la fonction `#!py courbe` qui est déjà chargée en mémoire dans sa version corrigée.
    
    Vous devez donc écrire la fonction `#!py flocon` qui prend deux paramètres :
    
    * `#!py longueur` : un entier positif indiquant la largeur totale de la figure à construire ;
    * `#!py profondeur` : le degré de profondeur de la construction.

    Cette fonction construit la figure comme expliqué ci-dessous :
    
    * `#!py flocon(100, 0)` construit un triangle équilatéral de `#!py 100` pixels de côté ;
    * `#!py flocon(300, 5)` construit le flocon souhaité : chaque segment a été découpé `#!py 5` fois.

    Encore une fois, la fonction `modele` dessine à l'écran la figure attendue.
    
    {{ IDE('pythons/flocon_vide', MODE='delayed_reveal', MAX=10)}}

    {{ figure('flocon', div_class="py_mk_figure center", admo_title='Votre flocon') }}
    
    
    