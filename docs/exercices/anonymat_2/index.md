---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Anonymat (2)
tags:
    - string
difficulty: 200
---

Avant de rendre public des dossiers sensibles, il arrive que certains organismes les « anonyment » en entier ou partiellement.

Dans le cadre de cet exercice, nous ne programmons qu'une version simpliste : « caviarder » un texte consiste à **remplacer toutes les caractères alphabétiques situées dans une certaine plage d'indices par le caractère `#`**.

Par exemple, pour le texte `L'espion était J. Bond` et les indices `#!py 15` (caractère `J`) et `#!py 21` (`d`) on obtient `L'espion était #. ####`.

On demande d'écrire la fonction `caviarder` qui prend en argument une chaîne de caractères `texte`, deux entiers `debut` et `fin` et renvoie le même texte caviardé  entre les caractères d'indices `debut` et `fin` (inclus l'un et l'autre).

On garantit que `debut` est toujours inférieur ou égal à `fin`.

Selon l'usage informatique, le premier caractère de `texte` a pour indice `#!py 0`. Ainsi le caractère d'indice `#!py 1` de `#!py "Hello"` est `#!py "e"`.

!!! tip "Astuce"

    Si `s` est une chaîne de caractères, l'instruction `s.isalpha()` renvoie `True` si `s` n'est composée que de caractères alphabétiques.
    
    Ainsi `#!py "é".isalpha()` est évalué à `True`, `#!py "Bonjour !".isalpha()` est évalué à `False`.

???+ example "Exemples"

    ```pycon title=""
    >>> caviarder("L'espion était J. Bond", 15, 21)
    "L'espion était #. ####"
    >>> caviarder("Paul est un espion", 100, 200)
    'Paul est un espion'
    ```

    
{{ IDE('exo') }}
