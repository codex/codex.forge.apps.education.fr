

# --------- PYODIDE:code --------- #

def sommet(tableau):
    gauche = 0
    droite = len(tableau) - 1
    while droite - gauche > 1:
        milieu = (...) // 2
        if tableau[milieu - 1] < tableau[milieu] < tableau[milieu + 1]:
            gauche = ...
        elif tableau[milieu - 1] > ... > ...:
            droite = ...
        else:
            return tableau[...]

# --------- PYODIDE:corr --------- #

def sommet(tableau):
    gauche = 0
    droite = len(tableau) - 1
    while droite - gauche > 1:
        milieu = (gauche + droite) // 2
        if tableau[milieu - 1] < tableau[milieu] < tableau[milieu + 1]:
            gauche = milieu
        elif tableau[milieu - 1] > tableau[milieu] > tableau[milieu + 1]:
            droite = milieu
        else:
            return tableau[milieu]

# --------- PYODIDE:tests --------- #

assert sommet([1, 9, 8, 7]) == 9
assert sommet([1, 3, 5, 2]) == 5
assert sommet([1, 2, 3, 4, 5, 4, 3, 2, 1]) == 5
assert sommet([1, 2, 3, 4, 3, 2, 1, 0, -1, -2]) == 4

# --------- PYODIDE:secrets --------- #

assert sommet([4, 5, 7, 5, 3]) == 7, "Erreur avec [4, 5, 7, 5, 3]"
assert sommet([1, 10, 9, 8, 7, 6, 5, 4]) == 10, "Erreur avec [1, 10, 9, 8, 7, 6, 5, 4]"
assert sommet([10, 100, 10, 9, 8, 7, 6]) == 100, "Erreur avec [10, 100, 10, 9, 8, 7, 6]"
assert sommet([1, 2, 4, 8, 4]) == 8, "Erreur avec [1, 2, 4, 8, 4]"
assert sommet([3, 4, 3, 2, 1, 0, -1, -2]) == 4, "Erreur avec [3, 4, 3, 2, 1, 0, -1, -2]"


for i in range(1, 10):
    for j in range(1, 10):
        l = list(range(i)) + list(range(i + j, i - 1, -1))
        assert sommet(l) == i + j


class IntSurcharge:
    __compteur_comparaison = 0

    def __init__(self, x):
        self.__valeur = x

    def __lt__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur < autre.__valeur

    def __gt__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur > autre.__valeur

    def __ne__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur != autre.__valeur

    def __eq__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur == autre.__valeur

    def __le__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur <= autre.__valeur

    def __ge__(self, autre):
        IntSurcharge.__compteur_comparaison += 1
        return self.__valeur >= autre.__valeur

    @property
    def compteur(self):
        return IntSurcharge.__compteur_comparaison

    @property
    def valeur(self):
        return self.__valeur


unimodal = [IntSurcharge(i) for i in range(10000)]
unimodal.extend([IntSurcharge(i) for i in range(10000, 543, -1)])

resultat = sommet(unimodal)

valeur = resultat.valeur
compteur = resultat.compteur
assert valeur == 10000
assert compteur < 200, "Vous avez utilisé une méthode interdite"

