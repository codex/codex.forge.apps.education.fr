Dans le cas d'un graphe non connexe, on utiliserait cette fonction avec chaque composante connexe pour vérifier si chacune d'elles est bipartie ou pas.

On peut remarquer qu'un graphe biparti ne possède aucun cycle impair.

Il y a équivalence entre les deux propriétés: "être biparti" et "ne posséder aucun cycle impair".
