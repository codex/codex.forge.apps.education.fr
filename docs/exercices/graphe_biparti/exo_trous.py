# --------- PYODIDE:code --------- #

from collections import deque

def est_biparti(graphe, sommet_initial):
    couleurs = [0 for s in ...]
    couleurs[sommet_initial] = ...
    file = deque()
    file.append(...)
    while len(file) > ...:
        sommet = file.popleft()
        for voisin in graphe[...]:
            if couleurs[voisin] == 0:
                ...
            elif couleurs[voisin] == ...:
                return ...
    return ...

# --------- PYODIDE:corr --------- #

from collections import deque

def est_biparti(graphe, sommet_initial):
    couleurs = [0 for s in graphe]
    couleurs[sommet_initial] = 1
    file = deque()
    file.append(sommet_initial)
    while len(file) > 0:
        sommet = file.popleft()
        for voisin in graphe[sommet]:
            if couleurs[voisin] == 0:
                couleurs[voisin] = - couleurs[sommet]
                file.append(voisin)
            elif couleurs[voisin] == couleurs[sommet]:
                return False
    return True

# --------- PYODIDE:tests --------- #

g = {0: [1, 2], 1: [0, 2], 2: [0, 1]}
assert not est_biparti(g, 0)
g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
assert est_biparti(g, 0)

# --------- PYODIDE:secrets --------- #

# tests
g = {0: [1, 2], 1: [0, 2], 2: [0, 1]}
assert est_biparti(g, 0) is False
g = {0: [1, 3], 1: [0, 2], 2: [1, 3], 3: [0, 2]}
assert est_biparti(g, 0) is True

# tests secrets
g1 = {0: [1, 4],
    1: [0, 2, 3, 4, 5],
    2: [1],
    3: [1],
    4: [0, 1, 5],
    5: [1, 4]}

assert est_biparti(g1, 0) is False

g2 = {0: [1],
    1: [0, 2, 3, 4],
    2: [1],
    3: [1],
    4: [1, 5],
    5: [4]}

assert est_biparti(g2, 0) is True

g3 = {0: [1, 4],
    1: [0, 2, 3, 5],
    2: [1],
    3: [1],
    4: [0, 5],
    5: [1, 4]}

assert est_biparti(g3, 0) is True

g4 = {0: [1, 4],
    1: [0, 2, 3],
    2: [1],
    3: [1],
    4: [0, 5],
    5: [4]}

assert est_biparti(g4, 0) is True

g5 = {0: [1, 8, 9], 1: [0, 3, 4, 7], 2: [3, 5, 7], 3: [1, 2, 6, 9],
     4: [1, 6, 8, 9], 5: [2, 8, 9], 6: [3, 4, 7], 7: [1, 2, 6, 8],
     8: [0, 4, 5, 7], 9: [0, 3, 4, 5]}

assert est_biparti(g5, 0) is True
