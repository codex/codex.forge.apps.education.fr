---
author:
    - Nicolas Revéret
    - Romain Janvier
hide:
    - navigation
    - toc
difficulty: 50
title: Syntaxe de la POO
tags:
    - programmation orientée objet
---



On travaille dans cet exercice sur deux **classes** :

* la classe `#!py Chose` permet de décrire une chose (objet, élément du décor...) utilisée dans un jeu vidéo ;
* la classe `#!py Personnage` permet de représenter un personnage du jeu.

??? info "La classe `Chose`"
    Une `#!py Chose` est caractérisée par un unique **attribut**, son `#!py genre` de type `#!py str`. Ce genre doit impérativement appartenir à la liste `#!python ["livre", "pioche", "pierre", "gateau", "trésor"]`.

    ```mermaid
    classDiagram
    class Chose{
           genre : str
    }
    ```

??? info "La classe `Personnage`"
    Un `#!py Personnage` est caractérisé par des **attributs** :

    * un `#!py nom`(au format `#!py str`),
    * une `#!py energie` (au format `#!py int`, compris entre `#!py 0` et `#!py 500`),
    * un `#!py inventaire` (au format `#!py list`).

    La classe `#!py Personnage` possède aussi plusieurs méthodes qui sont résumées, avec les attributs, dans le diagramme suivant :

    ```mermaid
    classDiagram
    class Personnage{
           nom : str
           energie : int
           inventaire : list
           prend(truc : Chose) str
           mange(truc : Chose) str
           parle(personne : Personnage) str
           donne(truc : Chose, personne : Personnage) str
    }
    ```

    Dans le diagramme précédent le texte `#!py prend(truc : Chose) : str` signifie que la classe `#!py Personnage` possède une méthode `#!py prend` prenant un argument nommé `#!py truc` de type `#!py Chose` et qui renvoie une chaîne de caractère (`#!py str`).

Complétez le code dans les différents blocs ci dessous avec les instructions valides. Chaque bloc est indépendant des autres et il est possible de le compléter dans le désordre, même s'il est conseillé de le faire dans l'ordre la première fois.

Les deux classes sont déjà définies et vous pouvez les utiliser dans chaque exercice de cette page.

??? question "I) Guybrush et Helaine"
    Guybrush est un pirate et il est marié avec Helaine, la Gouverneure de l'île des pirates.

    ---

    Créez un personnage nommé `#!py 'Guybrush'` qui a `#!py 100` d'énergie. Ce `#!py Personnage` doit être affecté à la variable `#!py guy`.

    Créez également un personnage nommé `#!py 'Helaine'` qui a `#!py 200` d'énergie. Ce `#!py Personnage` doit être affecté à la variable `#!py helaine`.


    ??? tip "Création d'un Personnage"
        Lors de la création d'un objet type `#!py Personnage`, on passe en arguments son nom puis son énergie.

        ```pycon
        >>> batman = Personnage("Batman", 450)
        >>> robin = Personnage("Robin", 300)
        ```

        L'inventaire est créé automatiquement et est initialement vide.

    {{ IDE('exo_1') }}

??? question "II) Les aventures de Guybrush"
    En fait, Guybrush n'est pas vraiment pirate. Il rêve de l'être et s'invente de grandes aventures.

    Helaine est fatiguée de l'entendre raconter ces histoires, toutes fausses, et lui lance un défi. Elle lui demande d'aller trouver le trésor de l'île aux Singes et de le lui ramener.

    ----

    Faites parler Guybrush à Helaine puis faites parler Helaine à Guybrush.

    Vous affecterez le résultat de ces deux conversations dans des variables `#!py conversation1` et `#!py conversation2`.

    ??? tip "Utilisation des méthodes"
        Il est possible d'appliquer des **méthodes** à un `#!py Personnage`. Si `#!py perso` est un objet de la classe `#!py Personnage` :

        * `#!py prend` : `#!py perso` prend un `#!py truc` (de type `#!py Chose`),
        * `#!py mange` : `#!py perso` mange un `#!py truc` (de type `#!py Chose`),
        * `#!py parle` : `#!py perso` parle avec une autre `#!py personne` (de type `#!py Personnage`),
        * `#!py donne` : `#!py perso` donne un `#!py truc` à une autre `#!py personne` (respectivement de types `#!py Chose` et `#!py Personnage`).

        ??? note "Remarque sur le vocabulaire propre à Python"
               La documentation officielle de Python utilise un vocabulaire différent : les attributs et les méthodes d'un objet sont tous dénommés *attributs*. La documentation distingue alors *attributs __données__* et *méthodes*.

        Toutes ces méthodes renvoient des chaînes de caractères décrivant le résultat de l'action.

        ```pycon
        >>> batman.prend(bat_manuel)
        'Batman prend un(e) livre'
        >>> batman.donne(bat_manuel, robin)
        'Batman donne un(e) livre à Robin'
        ```

        Certaines actions sont impossibles. Par exemple `#!py perso` ne peut pas manger un `#!py truc` du genre `#!py "livre"`.

        ```pycon
        >>> robin.mange(bat_manuel)
        'Robin ne peut pas manger un(e) livre !'
        ```


    {{ IDE('exo_2') }}

??? question "III) Se préparer pour le voyage"
    Guybrush n'est pas très en forme. Il a besoin de reprendre des forces pour le voyage qui l'attend.

    Il se rend à la taverne et y prend un gâteau.

    ---

    Affectez à la variable `gateau` un nouvel objet de la classe `#!py Chose` et de genre `#!py 'gâteau'`.

    Faites en sorte que Guybrush prenne le gâteau et affectez le résultat de cette action dans une variable `resultat`.

    ??? tip "Création d'une Chose"
        Il est possible de créer une `#!py Chose` en précisant son genre :

        ```pycon
        >>> bat_manuel = Chose("livre")
        ```

    {{ IDE('exo_3') }}

??? question "IV) Prendre des forces"
    Guybrush mange le gâteau, ce qui lui redonne de l'énergie.

    -----

    Affectez l'énergie initiale de Guybrush à la variable `nrj`.

    Faites le ensuite manger le gâteau et affectez le résultat dans la variable `resultat`.

    Enfin, modifiez l'énergie de Guybrush pour qu'elle soit à 199.

    ??? tip "Accéder aux attributs d'un objet"

        Si `#!py untel` et `#!py unetelle` sont des objets de type `#!py Personnage` et `#!py truc` un objet de type `#!py Chose`, il est par exemple possible :

        * d'accéder à la valeur d'un attribut en saisissant `#!python inv = untel.inventaire`,

        * de modifier la valeur d'un attribut en saisissant `#!python unetelle.energie = 125`.

    {{ IDE('exo_4') }}

??? question "V) Le trésor de l'île aux Singes"
    Après de multiples périples, bien trop longs pour être racontés ici, Guybrush arrive enfin à trouver le trésor de l'île aux Singes.

    Sur le voyage de retour, pris d'hallucinations, il essaie de manger le trésor, qui heureusement n'est pas comestible.

    ---

    Affectez à la variable `tresor` une Chose de genre `#!py 'trésor'`.

    Faites en sorte que Guybrush prenne le trésor et affectez le résultat à la variable `#!py resultat1`.

    Enfin, faites en sorte que Guybrush essaie de manger le trésor et affectez le résultat à la variable `#!py resultat2`.

    {{ IDE('exo_5') }}

??? question "VI) Fin de la quête"
    Victorieux, Guybrush peut enfin ramener le trésor à Helaine, qui pourra le mettre au grenier avec tous ceux qu'elle a déjà trouvés elle-même.

    ----

    Faites en sorte que Guybrush donne le trésor à Helaine et affectez le résultat à la variable `#!py resultat`.

    Il faut également rajouter des tests à la fin.

    {{ IDE('exo_6') }}

??? question "VII) Vers de nouvelles aventures"
    Vous pouvez utiliser l'IDE ci-dessous pour inventer vos propres aventures avec vos propres personnages.

    {{ IDE('exo_7') }}

