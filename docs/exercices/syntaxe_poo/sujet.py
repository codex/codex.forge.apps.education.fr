# --- hdr, 1, 2, 3, 4, 5, 6, 7 --- #
GENRES_OBJETS = {
    "livre": {"utilisable", "peut être pris"},
    "pioche": {"utilisable", "peut être pris"},
    "pierre": {"cassable", "utilisable"},
    "gâteau": {"comestible", "peut être pris"},
    "trésor": {"peut être pris"},
}


class Personnage:
    def __init__(self, nom, energie):
        self._nom = nom
        self._energie = energie
        self._inventaire = []

    @property
    def nom(self):
        return self._nom

    @property
    def energie(self):
        return self._energie

    @energie.setter
    def energie(self, valeur):
        if not 0 <= valeur <= 500:
            return "L'énergie est toujours comprise entre 0 et 500"
        self._energie = valeur

    @property
    def inventaire(self):
        return self._inventaire

    def prend(self, truc):
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas prendre {truc}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas prendre {truc}"
        if (truc in self.inventaire):
            return f"{truc} est déjà dans l'inventaire"
        self._inventaire.append(truc)
        return f"{self.nom} prend {truc}"

    def mange(self, truc):
        if truc not in self._inventaire:
            return f"{truc} n'est pas dans l'inventaire"
        if not ("comestible" in GENRES_OBJETS[truc.genre]):
            return f"{self.nom} ne peut pas manger {truc} !"
        self._inventaire.remove(truc)
        return f"{self.nom} mange {truc}"

    def parle(self, personne):
        if not isinstance(personne, Personnage):
            return f"{self.nom} ne peut pas parler à {personne.nom}"
        return f"{self.nom} et {personne._nom} discutent"

    def donne(self, truc, personne):
        if not isinstance(personne, Personnage):
            return f"{self._nom} ne peut pas donner {truc} à {personne.nom}"
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas donner {truc} à {personne.nom}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas donner {truc}"

        self._inventaire.remove(truc)
        personne._inventaire.append(truc)
        return f"{self.nom} donne {truc} à {personne.nom}"

    def __str__(self):
        return f"{self.nom} ({self._energie}, {list(self._inventaire)})"

    def __repr__(self):
        return self.__str__()


class Chose:
    def __init__(self, genre):
        if not (genre.lower() in GENRES_OBJETS):
            return f"Le genre doit être dans {', '.join([g for g in GENRES_OBJETS])}"
        self._genre = genre.lower()

    @property
    def genre(self):
        return self._genre

    def __str__(self):
        return f"un(e) {self._genre}"

    def __repr__(self):
        return self.__str__()

# --- hdr, 1 --- #
guy = None
helaine = None
# --- exo, 1 --- #
''' # skip
... = ...
''' # skip
...

# --- corr, 1 | hdr, 2, 3, 4, 5, 6 --- #
guy = Personnage("Guybrush", 100)
helaine = Personnage("Helaine", 200)
# --- tests, 1 --- #
assert str(guy) == "Guybrush (100, [])", "Erreur avec la méthode '__init__'"
assert str(helaine) == "Helaine (200, [])", "Erreur avec la méthode '__init__'"
# --- secrets, 1 --- #
assert isinstance(guy, Personnage)
assert isinstance(helaine, Personnage)
# --- hdr, 2 --- #
conversation1 = ""
conversation2 = ""
# --- exo, 2 --- #
conversation1 = ...
...

# --- corr, 2 | hdr, 3, 4, 5 --- #
conversation1 = guy.parle(helaine)
conversation2 = helaine.parle(guy)

# --- tests, 2 --- #
assert conversation1 == "Guybrush et Helaine discutent", "Erreur avec la méthode 'parle'"
assert conversation2 == "Helaine et Guybrush discutent", "Erreur avec la méthode 'parle'"
# --- secrets, 2 --- #
# rien
# --- hdr, 3 --- #
gateau = None
# --- exo, 3 --- #
gateau = ...
resultat = guy.prend(...)

# --- corr, 3 | hdr, 4, 5, 6 --- #
gateau = Chose("gâteau")
resultat = guy.prend(gateau)
# --- tests, 3 --- #
assert gateau.genre == "gâteau", "Erreur avec l'attribut 'genre'"
assert gateau in guy.inventaire, "Erreur avec la méthode 'prend'"
assert resultat == "Guybrush prend un(e) gâteau", "Erreur avec la méthode 'prend'"
# --- secrets, 3 --- #
isinstance(gateau, Chose)
# --- hdr, 4 --- #
nrj = None
guy.energie = 100
resultat = ""
# --- exo, 4 --- #
...
...
...

# --- corr, 4 | hdr, 5, 6 --- #
nrj = guy.energie
resultat = guy.mange(gateau)
guy.energie = 199
# --- tests, 4 --- #
assert nrj == 100, "Erreur avec l'attribut 'nrj'"
assert guy.energie == 199, "Erreur avec l'attribut 'nrj'"
assert gateau not in guy.inventaire, "Erreur avec la méthode 'mange'"
assert resultat == "Guybrush mange un(e) gâteau", "Erreur avec la méthode 'mange'"
# --- secrets, 4 --- #
# rien
# --- hdr, 5 --- #
tresor = None
# --- exo, 5 --- #
...
...
...

# --- corr, 5 | hdr, 6 --- #
tresor = Chose("trésor")
resultat1 = guy.prend(tresor)
resultat2 = guy.mange(tresor)
# --- tests, 5 --- #
assert tresor.genre == "trésor", "Erreur avec l'attribut 'genre'"
assert tresor in guy.inventaire, "Erreur avec la méthode 'prend'"
assert resultat1 == f"Guybrush prend un(e) trésor", "Erreur avec la méthode 'prend'"
assert resultat2 == "Guybrush ne peut pas manger un(e) trésor !", "Erreur avec la méthode 'mange'"
# --- secrets, 5 --- #
isinstance(tresor, Chose)
# --- hdr, 6 --- #
resultat = ""
# --- exo, 6 --- #
...

# --- corr, 6 --- #
resultat = guy.donne(tresor, helaine)

assert tresor in helaine.inventaire, "Le trésor est toujours dans l'inventaire de Guybrush"
assert tresor not in guy.inventaire, "Le trésor n'est pas dans l'inventaire de Helaine"

# --- tests, 6 --- #
assert resultat == "Guybrush donne un(e) trésor à Helaine"
assert ..., "Le trésor est toujours dans l'inventaire de Guybrush"
assert ..., "Le trésor n'est pas dans l'inventaire de Helaine"
# --- secrets, 6 --- #
assert tresor in helaine.inventaire, "Le trésor est toujours dans l'inventaire de Guybrush"
assert tresor not in guy.inventaire, "Le trésor n'est pas dans l'inventaire de Helaine"

# --- exo, 7 --- #
...
