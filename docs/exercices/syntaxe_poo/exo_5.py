

# --------- PYODIDE:env --------- #

GENRES_OBJETS = {
    "livre": {"utilisable", "peut être pris"},
    "pioche": {"utilisable", "peut être pris"},
    "pierre": {"cassable", "utilisable"},
    "gâteau": {"comestible", "peut être pris"},
    "trésor": {"peut être pris"},
}


class Personnage:
    def __init__(self, nom, energie):
        self._nom = nom
        self._energie = energie
        self._inventaire = []

    @property
    def nom(self):
        return self._nom

    @property
    def energie(self):
        return self._energie

    @energie.setter
    def energie(self, valeur):
        if not 0 <= valeur <= 500:
            return "L'énergie est toujours comprise entre 0 et 500"
        self._energie = valeur

    @property
    def inventaire(self):
        return self._inventaire

    def prend(self, truc):
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas prendre {truc}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas prendre {truc}"
        if (truc in self.inventaire):
            return f"{truc} est déjà dans l'inventaire"
        self._inventaire.append(truc)
        return f"{self.nom} prend {truc}"

    def mange(self, truc):
        if truc not in self._inventaire:
            return f"{truc} n'est pas dans l'inventaire"
        if not ("comestible" in GENRES_OBJETS[truc.genre]):
            return f"{self.nom} ne peut pas manger {truc} !"
        self._inventaire.remove(truc)
        return f"{self.nom} mange {truc}"

    def parle(self, personne):
        if not isinstance(personne, Personnage):
            return f"{self.nom} ne peut pas parler à {personne.nom}"
        return f"{self.nom} et {personne._nom} discutent"

    def donne(self, truc, personne):
        if not isinstance(personne, Personnage):
            return f"{self._nom} ne peut pas donner {truc} à {personne.nom}"
        if not isinstance(truc, Chose):
            return f"{self._nom} ne peut pas donner {truc} à {personne.nom}"
        if not ("peut être pris" in GENRES_OBJETS[truc.genre]):
            return f"{self._nom} ne peut pas donner {truc}"

        self._inventaire.remove(truc)
        personne._inventaire.append(truc)
        return f"{self.nom} donne {truc} à {personne.nom}"

    def __str__(self):
        return f"{self.nom} ({self._energie}, {list(self._inventaire)})"

    def __repr__(self):
        return self.__str__()


class Chose:
    def __init__(self, genre):
        if not (genre.lower() in GENRES_OBJETS):
            return f"Le genre doit être dans {', '.join([g for g in GENRES_OBJETS])}"
        self._genre = genre.lower()

    @property
    def genre(self):
        return self._genre

    def __str__(self):
        return f"un(e) {self._genre}"

    def __repr__(self):
        return self.__str__()

guy = Personnage("Guybrush", 100)
helaine = Personnage("Helaine", 200)
conversation1 = guy.parle(helaine)
conversation2 = helaine.parle(guy)

gateau = Chose("gâteau")
resultat = guy.prend(gateau)
nrj = guy.energie
resultat = guy.mange(gateau)
guy.energie = 199
tresor = None

# --------- PYODIDE:code --------- #

...
...
...

# --------- PYODIDE:corr --------- #

tresor = Chose("trésor")
resultat1 = guy.prend(tresor)
resultat2 = guy.mange(tresor)

# --------- PYODIDE:tests --------- #

assert tresor.genre == "trésor", "Erreur avec l'attribut 'genre'"
assert tresor in guy.inventaire, "Erreur avec la méthode 'prend'"
assert resultat1 == f"Guybrush prend un(e) trésor", "Erreur avec la méthode 'prend'"
assert resultat2 == "Guybrush ne peut pas manger un(e) trésor !", "Erreur avec la méthode 'mange'"

# --------- PYODIDE:secrets --------- #


# tests secrets
isinstance(tresor, Chose)