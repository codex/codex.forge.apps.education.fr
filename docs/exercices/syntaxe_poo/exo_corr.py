# Créez un personnage nommé "Guybrush" qui a 100 d'énergie
# Ce Personnage doit être affecté à la variable guy
guy = Personnage("Guybrush", 100)
assert str(guy) == "Guybrush (100, [])"

# Créez un personnage nommé "Helaine" qui a 200 d'énergie
# Ce Personnage doit être affecté à la variable helaine
helaine = Personnage("Helaine", 200)
assert str(helaine) == "Helaine (200, [])"

# Faites parler Guybrush et Helaine
resultat = guy.parle(helaine)
print(resultat)
assert resultat == "Guybrush et Helaine discutent"

# Faites parler Helaine et Guybrush
resultat = helaine.parle(guy)
assert resultat == "Helaine et Guybrush discutent"

# Affectez l'énergie de Guybrush à la variable 'nrj'
nrj = guy.energie
assert nrj == 100

# Faites en sorte que Guybrush ait 199 d'énergie
guy.energie = 199
assert guy.energie == 199

# Créez une chose du genre diamant
# Affectez le à la variable diamant
diamant = Chose("diamant")
assert diamant.genre == "diamant"

# Faites en sorte que Guybrush prenne le diamant
guy.prend(diamant)
assert (diamant in guy.inventaire) is True

# Faites en sorte que Guybrush donne le diamant à Helaine
guy.donne(diamant, helaine)
# Vérifiez à l'aide de deux assertions que :
#   - Guybrush ne possède plus le diamant
#   - Helaine possède le diamant
assert (diamant in guy.inventaire) is False
assert (diamant in helaine.inventaire) is True
