Dans la solution on ne parcourt que la moitié de la matrice, la partie au-dessus de la diagonale.

On pourrait aussi parcourir l'autre moitié.

Si on parcourt toute la matrice, on compte chaque arête deux fois. Il faut donc penser à diviser par deux la somme obtenue.

