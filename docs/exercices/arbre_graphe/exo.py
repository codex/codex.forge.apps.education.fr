

# --------- PYODIDE:code --------- #

def est_arbre(matrice):
    ...

# --------- PYODIDE:corr --------- #

def est_arbre(matrice):
    n = len(matrice)
    nb_aretes = 0
    for i in range(n):
        for j in range(i+1, n):
            if matrice[i][j] == 1:
                nb_aretes = nb_aretes + 1
    return nb_aretes == n - 1

# --------- PYODIDE:tests --------- #

g = [[0, 1], [1, 0]]
assert est_arbre(g)
g = [[0, 0, 1, 1], [0, 0, 1, 0], [1, 1, 0, 0], [1, 0, 0, 0]]
assert est_arbre(g)
g = [[0, 0, 1, 1], [0, 0, 1, 1], [1, 1, 0, 0], [1, 1, 0, 0]]
assert not est_arbre(g)

# --------- PYODIDE:secrets --------- #

g = [[0] * 10 for i in range(10)]
for i in range(9):
    g[i][i+1] = 1
    g[i+1][i] = 1
assert est_arbre(g)
g[5][7] = g[7][5] = 1
assert not est_arbre(g)
g = [[0] * 10 for i in range(10)]
for i in range(1, 10):
    g[i][0] = 1
    g[0][i] = 1
assert est_arbre(g)
g[5][7] = g[7][5] = 1
assert not est_arbre(g)
