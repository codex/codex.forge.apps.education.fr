

# --------- PYODIDE:code --------- #

class Maillon:
    def __init__(self, valeur, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        return str(self.valeur)

class File:
    def __init__(self, premier=None):
        """ premier est le premier maillon par lequel on accède à la liste
            circulaire.
            Dans le cas d'une file, c'est le dernier maillon ajouté à la file
        """
        self.premier = premier

    def est_vide(self):
        return self.premier is None

    def __str__(self):
        if self.est_vide():
            return "None"
        maillon = self.premier
        affiche = str(maillon) + ' -> '
        while maillon.suivant != self.premier:
            affiche = affiche + str(maillon.suivant) + ' -> '
            maillon = maillon.suivant
        affiche = affiche + str(maillon.suivant)
        return affiche

    def enfile(self, elt):
        if self.est_vide():
            self.premier = ...
            self.premier.suivant = ...
        else:
            maillon = ...
            self.premier.suivant = ...
            self.premier = ...

    def defile(self):
        if not self.est_vide():
            premier_maillon = self.premier
            element = ...
            if premier_maillon == premier_maillon.suivant:
                self.premier = ...
            else:
                premier_maillon.suivant = ...
            return ...

# --------- PYODIDE:corr --------- #

class Maillon:
    def __init__(self, valeur, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

    def __str__(self):
        return str(self.valeur)

class File:
    def __init__(self, premier=None):
        """ premier est le premier maillon par lequel on accède à la liste
            circulaire.
            Dans le cas d'une file, c'est le dernier maillon ajouté à la file
        """
        self.premier = premier

    def est_vide(self):
        return self.premier is None

    def __str__(self):
        if self.est_vide():
            return "None"
        maillon = self.premier
        affiche = str(maillon) + ' -> '
        while maillon.suivant != self.premier:
            affiche = affiche + str(maillon.suivant) + ' -> '
            maillon = maillon.suivant
        affiche = affiche + str(maillon.suivant)
        return affiche

    def enfile(self, elt):
        if self.est_vide():
            self.premier = Maillon(elt)
            self.premier.suivant = self.premier
        else:
            maillon = Maillon(elt, self.premier.suivant)
            self.premier.suivant = maillon
            self.premier = maillon

    def defile(self):
        if not self.est_vide():
            premier_maillon = self.premier
            element = premier_maillon.suivant.valeur
            if premier_maillon == premier_maillon.suivant:
                self.premier = None
            else:
                premier_maillon.suivant = premier_maillon.suivant.suivant
            return element

# --------- PYODIDE:tests --------- #

f = File()
assert f.est_vide()
for i in range(4):
    f.enfile(i)
assert not f.est_vide()
assert f.defile() == 0
assert f.defile() == 1

# --------- PYODIDE:secrets --------- #

# tests

f = File()
assert f.est_vide()
for i in range(4):
    f.enfile(i)
assert not f.est_vide()
assert f.defile() == 0
assert f.defile() == 1

# autres tests

f = File()
assert f.est_vide()
for i in range(26):
    f.enfile(chr(97+i))
assert not f.est_vide()
for i in range(26):
    assert f.defile() == chr(97+i)
assert f.est_vide()
