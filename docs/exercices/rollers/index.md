---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Apparier des rollers
tags:
    - tuple
---

Le professeur d'EPS est mécontent. Après une séance de rollers avec ses élèves, il constate que ceux-ci ont abandonné leurs patins dans le désordre dans le vestiaire.

Il est donc face à un grand tas de rollers dans lequel les paires sont toutes mélangées... Pire : **il manque peut-être certains patins !**

Vous devez donc l'aider à vérifier que chaque patin pourra trouver une « âme sœur ».

Pour chaque roller, il connait deux informations : son genre (`#!py "gauche"` ou `#!py "droit"`) et sa pointure (un entier entre `#!py 36` et `#!py 46` inclus l'un et l'autre).

Une paire valide est composée d'un patin gauche et d'un droit, l'un et autre ayant la même pointure.

Les rollers sont donnés sous forme d'une liste dans laquelle chaque élément est un tuple `(genre, pointure)`.
Par exemple `[("gauche", 40), ("gauche", 41), ("droit", 40)]`. Cette liste ne permet pas d'apparier tous les patins : le patin `("gauche", 41)` n'a pas de patin associé.

Écrire la fonction `est_tas_valide` qui prend en argument une liste décrivant les rollers et renvoie `True` ou `False` selon que chaque patin peut être apparié ou non.

!!! warning "Contrainte"

    Il est possible de résoudre ce problème en n'accédant qu'une unique fois aux caractéristiques de chaque patin.

    Les tests supplémentaires vérifieront donc que, lors du traitement d'une liste de $N$ rollers, l'algorithme utilisé **n'accède pas plus de $2 \times N$ fois à la liste**.

!!! warning "Tri"

    Il est possible de résoudre cet exercice sans trier les patins ou leurs pointures. De plus pour de grandes listes de rollers, les tris sont longs à mettre en œuvre. **Les fonctions de tri de Python (`sorted` et `list.sort`) sont donc interdites**.

???+ example "Exemples"

    ```pycon title=""
    >>> est_tas_valide([])
    True
    >>> est_tas_valide([("gauche", 40), ("droit", 40)])
    True
    >>> est_tas_valide([("gauche", 40), ("gauche", 41), ("droit", 40)])
    False
    ```
    
{{ IDE('exo') }}
