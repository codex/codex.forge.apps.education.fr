

# --------- PYODIDE:code --------- #

def est_tas_valide(rollers):
    ...

# --------- PYODIDE:corr --------- #

def est_tas_valide(rollers):
    paires = [0 for pointure in range(36, 47)]
    for genre, pointure in rollers:
        if genre == "gauche":
            paires[pointure - 36] += 1
        else:
            paires[pointure - 36] -= 1

    for valeur in paires:
        if valeur != 0:
            return False
    return True

# --------- PYODIDE:tests --------- #

assert est_tas_valide([]) is True
assert est_tas_valide([("gauche", 40), ("droit", 40)]) is True
assert est_tas_valide([("gauche", 40), ("droit", 40), ("gauche", 41)]) is False

# --------- PYODIDE:secrets --------- #

from random import shuffle, randrange
# Tests supplémentaires

# Nombre de lecture de valeurs inférieur à 2.001 * len(rollers)
class Liste(list):
    def __init__(self, valeurs):
        self.nb_lectures = 0
        super().__init__(valeurs)

    def __getitem__(self, i):
        self.nb_lectures += 1
        return super().__getitem__(i)

    def __iter__(self):
        return Iterateur(self)


class Iterateur:
    def __init__(self, valeurs):
        self.valeurs = valeurs
        self.indice = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.indice += 1
        if self.indice < len(self.valeurs):
            return self.valeurs[self.indice]
        else:
            raise StopIteration


nb_paires = 10_000
rollers = [("gauche", randrange(36, 47)) for _ in range(nb_paires)]
rollers.extend([("droit", pointure) for _, pointure in rollers])
shuffle(rollers)
rollers = Liste(rollers)
assert est_tas_valide(rollers) is True
nb_lectures = rollers.nb_lectures
nb_max = 2.001 * len(rollers)
assert (
    nb_lectures <= nb_max
), f"Le code a lu {nb_lectures} valeurs de la liste qui comporte {nb_paires} rollers. Il faut optimiser le code..."

# Listes valides
for _ in range(5):
    nb_paires = randrange(500, 1001)
    rollers = [("gauche", randrange(36, 47)) for _ in range(nb_paires)]
    rollers.extend([("droit", pointure) for _, pointure in rollers])
    shuffle(rollers)
    assert est_tas_valide(rollers) is True

# Listes invalides
# Plus de droit que de gauche
for _ in range(5):
    nb_paires = randrange(500, 1001)
    rollers = [("gauche", randrange(36, 47)) for _ in range(nb_paires)]
    rollers.extend([("droit", pointure) for _, pointure in rollers])
    rollers.append(("droit", randrange(36, 47)))
    shuffle(rollers)
    assert est_tas_valide(rollers) is False
# Plus de gauche que de droit
for _ in range(5):
    nb_paires = randrange(500, 1001)
    rollers = [("gauche", randrange(36, 47)) for _ in range(nb_paires)]
    rollers.extend([("droit", pointure) for _, pointure in rollers])
    rollers.append(("gauche", randrange(36, 47)))
    shuffle(rollers)
    assert est_tas_valide(rollers) is False
# Autant de chaque genre mais problème de pointures
for _ in range(5):
    nb_paires = randrange(500, 1001)
    rollers = [("gauche", randrange(36, 47)) for _ in range(nb_paires)]
    rollers.extend([("droit", pointure) for _, pointure in rollers])
    rollers.append(("droit", randrange(36, 40)))
    rollers.append(("gauche", randrange(40, 47)))
    shuffle(rollers)
    assert est_tas_valide(rollers) is False