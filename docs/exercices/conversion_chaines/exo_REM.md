
Cette différence de 32 entre les entiers qui encode un caractère en minuscule et le même caractère en majuscule n'est probablement pas le fruit du hasard.

On considére les 128 premiers caractères encodés par un entier allant de 0 à 127. Les caractères allant de `#!py 'A'` à `#!py 'Z'` sont encodés
par les entiers allant de 65 à 90, soit en binaire de `#!py 1000001` à `#!py 1011010`.

Une règle simple permettant de passer des majuscules aux minuscules et réciproquement, 
qui est la même pour tous les caractères alphabétiques non accentués, consiste à modifier le deuxième bit à partir de la gauche (le seul envisageable). 

Le codage de `#!py 'A'` est `#!py 100 0001`, celui de `#!py 'a'` est `#!py 110 0001`, le codage de `#!py 'Z'` est `#!py 1011010`, celui de `#!py 'z'` est `#!py 1111010`.
Or, modifier ce bit de 0 à 1 revient à ajouter 32, le modifier de 1 à 0 revient à soustraire 32.
