---
author: Serge Bays
hide:
    - navigation
    - toc
title: Conversion de chaînes de caractères
tags:
    - string
difficulty: 150
maj: 01/03/2024
---

Étant donnée une chaîne de caractères, l'objectif est de produire une copie de cette chaîne convertie en bas de casse, en « minuscules » dit-on couramment.

Par exemple, la chaîne `#!py "Les algorithmes de Bellman-Ford et de Dijkstra"` sera convertie en `#!py "les algorithmes de bellman-ford et de dijkstra"`.

On rappelle qu'un caractère est encodé par un nombre entier que l'on obtient avec la fonction `#!py ord`. Par exemple `#!py ord('A')` est évalué à `#!py 65`. Les codes des caractères alphabétiques non accentués en majuscule se suivent : `#!py ord('A')` vaut `#!py 65`, `#!py ord('B')` vaut `#!py 66`, `#!py ord('C')` vaut `#!py 67`, ...

Réciproquement, étant donné un entier positif, on obtient le caractère encodé par cet entier avec la fonction `#!py chr`. Par exemple `#!py chr(65)` est évalué à `#!py 'A'`.

On se limite à convertir les caractères allant de `#!py 'A'` à `#!py 'Z'`.

Compléter la fonction `#!py minuscule` qui prend en paramètre une chaîne de caractères `#!py chaine` et renvoie une nouvelle chaîne qui est la copie de la chaîne `#!py chaine` convertie en minuscules.

Pour cela, on remarquera que si `#!py c_maj` est un caractère en majuscule et `#!py c_min` le même caractère en minuscule, alors la différence `#!py ord(c_min) - ord(c_maj)` vaut `#!py 32`.


???+ example "Exemples"

    ```pycon title=""
    >>> minuscule('ABCDE')
    'abcde'
    >>> minuscule("Les algorithmes de Bellman-Ford et de Dijkstra")
    'les algorithmes de bellman-ford et de dijkstra'
    ```

{{ interdiction('str.lower()') }}

{{ IDE('exo') }}

