

# --------- PYODIDE:code --------- #

def minuscule(chaine):
    copie = ...
    for caractere in chaine:
        code = ...
        if ...("A") <= ... <= ...:
            code = ...
        copie = ...
    return ...

# --------- PYODIDE:corr --------- #

def minuscule(chaine):
    copie = ""
    for caractere in chaine:
        code = ord(caractere)
        if ord("A") <= code <= ord("Z"):
            code = code + 32
        copie = copie + chr(code)
    return copie

# --------- PYODIDE:tests --------- #

assert minuscule("ABCDE") == "abcde"
chaine = "Les algorithmes de Bellman-Ford et de Dijkstra"
assert minuscule(chaine) == "les algorithmes de bellman-ford et de dijkstra"

# --------- PYODIDE:secrets --------- #


# tests secrets
assert minuscule("") == ""
from random import choice
from string import ascii_letters
caracteres = ascii_letters + " ,;:!?./"
for _ in range(10):
    chaine = "".join([choice(caracteres) for _ in range(20)])
    attendu = chaine.lower()
    assert minuscule(chaine) == attendu, f"Erreur avec {chaine = }"