

# --------- PYODIDE:env --------- #

def longueur_max(chaines):
    ...

# --------- PYODIDE:code --------- #

def longueur_max(chaines):
    maxi = 0
    for texte in ...:
        if len(...) > ...:
            ...
    return ...

# --------- PYODIDE:corr --------- #

def longueur_max(chaines):
    maxi = 0
    for texte in chaines:
        if len(texte) > maxi:
            maxi = len(texte)

    return maxi

# --------- PYODIDE:tests --------- #

chaines = [""]
assert longueur_max(chaines) == 0
chaines = ["Toto", "Titi", "Tata"]
assert longueur_max(chaines) == 4
chaines = ["Riri", "Fifi", "Loulou"]
assert longueur_max(chaines) == 6
chaines = ["Pomme", "Abricot", "Poire", "Pêche"]
assert longueur_max(chaines) == 7

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange, shuffle

attendu = randrange(1, 10)
chaines = ["a" * k for k in range(attendu + 1)]
shuffle(chaines)
assert longueur_max(chaines) == attendu, f"Erreur avec {chaines}"