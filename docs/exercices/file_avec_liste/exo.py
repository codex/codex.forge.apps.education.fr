

# --------- PYODIDE:code --------- #

class Maillon:
    def __init__(self, valeur):
        self.valeur = valeur
        self.suivant = None

class File:
    def __init__(self):
        self.tete = None
        self.fin = None

    def est_vide(self):
        return self.fin is None

    def affiche(self):
        maillon = self.tete
        print('←', end=' ')
        while maillon is not None:
            print(maillon.valeur, end=' ')
            maillon = maillon.suivant
        print('←')

    def enfile(self, element):
        nouveau_maillon = ...
        if self.est_vide():
            self.tete = ...
        else:
            self.fin.suivant = ...
        self... = nouveau_maillon

    def defile(self):
        if ...:
            resultat = ...
            self.tete = ...
            if self.tete is None:
                self.fin = ...
            return ...
        else:
            return None

# --------- PYODIDE:corr --------- #

class Maillon:
    def __init__(self, valeur):
        self.valeur = valeur
        self.suivant = None


class File:
    def __init__(self):
        self.tete = None
        self.fin = None

    def est_vide(self):
        return self.fin is None

    def affiche(self):
        maillon = self.tete
        print("←", end=" ")
        while maillon is not None:
            print(maillon.valeur, end=" ")
            maillon = maillon.suivant
        print("←")

    def enfile(self, element):
        nouveau_maillon = Maillon(element)
        if self.est_vide():
            self.tete = nouveau_maillon
        else:
            self.fin.suivant = nouveau_maillon
        self.fin = nouveau_maillon

    def defile(self):
        if not self.est_vide():
            resultat = self.tete.valeur
            self.tete = self.tete.suivant
            if self.tete is None:
                self.fin = None
            return resultat
        else:
            return None

# --------- PYODIDE:tests --------- #

file = File()
assert file.est_vide()
file.enfile(2)
assert file.est_vide() is False
file.enfile(5)
file.enfile(7)
assert file.defile() == 2
assert file.defile() == 5

# --------- PYODIDE:secrets --------- #

# tests

file = File()
assert file.est_vide() is True
file.enfile(2)
assert file.est_vide() is False
file.enfile(5)
file.enfile(7)
assert file.defile() == 2
assert file.defile() == 5

# autres tests

file = File()
file.enfile(1)
file.enfile(2)
assert file.defile() == 1
file.enfile(3)
file.enfile(4)
assert file.defile() == 2
assert file.defile() == 3
file.enfile(5)
assert file.defile() == 4
assert file.defile() == 5
assert file.est_vide() is True
file.enfile(6)
file.enfile(7)
assert file.est_vide() is False
assert file.defile() == 6
assert file.defile() == 7
assert file.est_vide()
