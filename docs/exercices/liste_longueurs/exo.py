

# --------- PYODIDE:code --------- #

def filtre_par_longueur(liste_prenoms, ...):
    resultat = []
    for prenom in ...:
        if ... == longueur:
            resultat.append(...)
    return ...

# --------- PYODIDE:corr --------- #

def filtre_par_longueur(liste_prenoms, longueur):
    resultat = []
    for prenom in liste_prenoms:
        if len(prenom) == longueur:
            resultat.append(prenom)
    return resultat

# --------- PYODIDE:tests --------- #

prenoms = ['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas']
assert filtre_par_longueur(prenoms, 7) == ['Francky', 'Charles', 'Nicolas']
assert filtre_par_longueur(prenoms, 3) == ['Léa']
assert filtre_par_longueur(prenoms, 10) == []

# --------- PYODIDE:secrets --------- #

# Tests
prenoms = ["Anne", "Francky", "Charles", "Léa", "Nicolas"]
assert filtre_par_longueur(prenoms, 7) == ["Francky", "Charles", "Nicolas"]
assert filtre_par_longueur(prenoms, 3) == ["Léa"]
assert filtre_par_longueur(prenoms, 10) == []

# Tests supplémentaires

autres = ["CAB", "AB", "ABC", "A" * 30, "B" * 30]
assert filtre_par_longueur(autres, 3) == ["CAB", "ABC"]
assert filtre_par_longueur(autres, 30) == ["A" * 30, "B" * 30]
