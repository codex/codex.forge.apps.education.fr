On pourrait aussi utiliser une liste en compréhension :

```python
def filtre_par_longueur(liste_prenoms, longueur):
    return [prenom for prenom in liste_prenoms if len(prenom) == longueur]
```

