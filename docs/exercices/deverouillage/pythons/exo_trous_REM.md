On utilise un parcours en profondeur itératif s'appuyant sur une pile.

Il est aussi possible de procéder récursivement. On utilise ci-dessous une fonction auxiliaire effectuant le parcours en profondeur et complétant la variable `#!py total` avec des motifs de longueur satisfaisante.

```python
def motif_rec(source, longueur):
    def profondeur(chemin, longueur):
        if longueur == 0:
            total.append(chemin)
        else:
            sommet = chemin[-1]
            for prochain in GRAPHE[sommet]:
                profondeur(chemin + (prochain,), longueur - 1)
            
    total = []
    profondeur((source,), longueur - 1)
    return total
```

D'autre part, les sommets du graphe étant étiquetés par les entiers allant de $0$ à $8$, il est possible de représenter celui-ci par une simple liste et non un dictionnaire :

```python
GRAPHE = [
    [1, 3, 4, 5, 7],
    [0, 2, 3, 4, 5, 6, 8],
    [1, 3, 4, 5, 7],
    [0, 1, 2, 4, 6, 7, 8],
    [0, 1, 2, 3, 5, 6, 7, 8],
    [0, 1, 2, 4, 6, 7, 8],
    [1, 3, 4, 5, 7],
    [0, 2, 3, 4, 5, 6, 8],
    [1, 3, 4, 5, 7],
]
```
