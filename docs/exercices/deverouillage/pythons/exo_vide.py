# --------- PYODIDE:code --------- #
def motifs(source, longueur):
    ...


# --------- PYODIDE:corr --------- #
GRAPHE = {
    0: [1, 3, 4, 5, 7],
    1: [0, 2, 3, 4, 5, 6, 8],
    2: [1, 3, 4, 5, 7],
    3: [0, 1, 2, 4, 6, 7, 8],
    4: [0, 1, 2, 3, 5, 6, 7, 8],
    5: [0, 1, 2, 4, 6, 7, 8],
    6: [1, 3, 4, 5, 7],
    7: [0, 2, 3, 4, 5, 6, 8],
    8: [1, 3, 4, 5, 7],
}


def motifs(source, longueur):
    resultats = []
    pile = [(source,)]
    while len(pile) > 0:
        motif = pile.pop()
        if len(motif) == longueur:
            resultats.append(motif)
        else:
            sommet = motif[-1]
            for prochain in GRAPHE[sommet]:
                pile.append(motif + (prochain,))
    return resultats


# --------- PYODIDE:tests --------- #
resultat = motifs(0, 2)  # motifs issus de 0 et de longueur 2
zero_deux = [(0, 1), (0, 3), (0, 4), (0, 5), (0, 7)]
assert sorted(resultat) == zero_deux

resultat = motifs(1, 3)  # motifs issus de 1 et de longueur 3
un_trois = [
    (1, 0, 1),
    (1, 0, 3),
    (1, 0, 4),
    (1, 0, 5),
    (1, 0, 7),
    (1, 2, 1),
    (1, 2, 3),
    (1, 2, 4),
    (1, 2, 5),
    (1, 2, 7),
    (1, 3, 0),
    (1, 3, 1),
    (1, 3, 2),
    (1, 3, 4),
    (1, 3, 6),
    (1, 3, 7),
    (1, 3, 8),
    (1, 4, 0),
    (1, 4, 1),
    (1, 4, 2),
    (1, 4, 3),
    (1, 4, 5),
    (1, 4, 6),
    (1, 4, 7),
    (1, 4, 8),
    (1, 5, 0),
    (1, 5, 1),
    (1, 5, 2),
    (1, 5, 4),
    (1, 5, 6),
    (1, 5, 7),
    (1, 5, 8),
    (1, 6, 1),
    (1, 6, 3),
    (1, 6, 4),
    (1, 6, 5),
    (1, 6, 7),
    (1, 8, 1),
    (1, 8, 3),
    (1, 8, 4),
    (1, 8, 5),
    (1, 8, 7),
]
assert sorted(resultat) == un_trois

# --------- PYODIDE:secrets --------- #
from random import randrange

_GRAPHE_ = [
    [1, 3, 4, 5, 7],
    [0, 2, 3, 4, 5, 6, 8],
    [1, 3, 4, 5, 7],
    [0, 1, 2, 4, 6, 7, 8],
    [0, 1, 2, 3, 5, 6, 7, 8],
    [0, 1, 2, 4, 6, 7, 8],
    [1, 3, 4, 5, 7],
    [0, 2, 3, 4, 5, 6, 8],
    [1, 3, 4, 5, 7],
]


def _motifs_(source, longueur):
    resultats = []
    pile = [(source,)]
    while len(pile) > 0:
        motif = pile.pop()
        if len(motif) == longueur:
            resultats.append(motif)
        else:
            sommet = motif[-1]
            for prochain in _GRAPHE_[sommet]:
                pile.append(motif + (prochain,))
    return resultats


for _ in range(5):
    source = randrange(0, 9)
    longueur = randrange(4, 7)
    attendu = sorted(_motifs_(source, longueur))
    resultat = sorted(motifs(source, longueur))
    assert resultat == attendu, f"Erreur avec {source, longueur = }"
