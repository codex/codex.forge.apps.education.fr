---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Transposition d'une matrice
tags:
    - à trous
    - grille
difficulty: 210
---

En mathématiques, une matrice est un tableau de nombres. La transposition d'une matrice est l'opération qui, concrètement, échange le rôle des lignes et des colonnes. On l'utilise par exemple pour multiplier deux matrices et,
 en infographie, pour des transformations d'images.

Considérons par exemple la matrice ci-dessous :

$$\begin{bmatrix}
1 & \mathbf{2} & 3 & 4\\
5 & \mathbf{6} & 7 & 8\\
9 & \mathbf{10} & 11 & 12\\
\end{bmatrix}$$

Sa version transposée est :

$$\begin{bmatrix}
1 & 5 & 9\\
\mathbf{2} & \mathbf{6} & \mathbf{10}\\
3 & 7 & 11\\
4 & 8 & 12\\
\end{bmatrix}$$

La colonne d'indice $1$ est devenue la ligne d'indice $1$.

On représente les matrices par des listes de listes Python contenant toutes le même nombre d'éléments.

On souhaite écrire une fonction `transposition` qui renvoie la matrice obtenue en transposant une matrice nommée `matrice` passée en paramètre. Pour ce faire on écrira tout d'abord les fonctions telles que :

- `hauteur(matrice)` renvoie le nombre de lignes de `matrice` ;
- `largeur(matrice)` renvoie le nombre de colonnes de `matrice` ;
- `ligne(matrice, i)` renvoie la ligne d'indice `i` de `matrice` sous forme d'une liste ;
- `colonne(matrice, j)` renvoie la colonne d'indice `j` de `matrice` sous forme d'une liste.
 
Les questions ci-dessous demandent d'écrire chacune de ces fonctions. Elles sont toutes indépendantes.
Il est possible, à partir de la question 2., d'utiliser les fonctions demandées dans les questions précédentes.

On garantit que toutes les matrices passées en paramètres aux différentes fonctions comptent au moins une ligne et une colonne.

??? question "Fonction `hauteur`"
    
    La fonction `hauteur` renvoie le nombre de lignes de la matrice passée en paramètre.
    
    ???+ example "Exemples"

        ```pycon title=""
        >>> matrice = [
        ...     [0, 1, 1, 1],
        ...     [1, 0, 1, 0],
        ...     [0, 0, 0, 1],
        ... ]
        >>> hauteur(matrice)
        3
        ```

        ```pycon title=""
        >>> matrice = [
        ...     ['a', 'b', 'c'],
        ...     ['d', 'e', 'f'],
        ...     ['g', 'h', 'i'],
        ...     ['j', 'k', 'l']
        ... ]
        >>> hauteur(matrice)
        4
        ```
    {{ IDE('exo_hauteur') }}

??? question "Fonction `largeur`"

    La fonction `largeur` renvoie le nombre de colonnes de la matrice passée en paramètre.

    ???+ example "Exemples"
        ```pycon title=""
        >>> matrice = [
        ...     [0, 1, 1, 1],
        ...     [1, 0, 1, 0],
        ...     [0, 0, 0, 1],
        ... ]
        >>> largeur(matrice)
        4
        ```

        ```pycon title=""
        >>> matrice = [
        ...     ['a', 'b', 'c'],
        ...     ['d', 'e', 'f'],
        ...     ['g', 'h', 'i'],
        ...     ['j', 'k', 'l']
        ... ]
        >>> largeur(matrice)
        3
        ```

    {{ IDE('exo_largeur') }}

??? question "Fonction `ligne`"

    La fonction `ligne` prend en paramètre une liste de listes `matrice` ainsi qu'un indice `i` et renvoie la ligne d'indice `i` de `matrice` sous forme d'une liste.
    
    On garantit que `i` est un indice valide.

    ???+ example "Exemples"

        ```pycon title=""
        >>> matrice = [
        ...     [0, 1, 1, 1],
        ...     [1, 0, 1, 0],
        ...     [0, 0, 0, 1],
        ... ]
        >>> ligne(matrice, 0)
        [0, 1, 1, 1]
        ```

        ```pycon title=""
        >>> matrice = [
        ...     ['a', 'b', 'c'],
        ...     ['d', 'e', 'f'],
        ...     ['g', 'h', 'i'],
        ...     ['j', 'k', 'l']
        ... ]
        >>> ligne(matrice, 3)
        ['j', 'k', 'l']
        ```
        
    {{ IDE('exo_ligne') }}

??? question "Fonction `colonne`"

    La fonction `colonne` prend en paramètre une liste de listes `matrice` ainsi qu'un indice `j` et renvoie la colonne d'indice `j` de `matrice` sous forme d'une liste.
    
    On garantit que `j` est un indice valide.

    ???+ example "Exemples"

        ```pycon title=""
        >>> matrice = [
        ...     [0, 1, 1, 1],
        ...     [1, 0, 1, 0],
        ...     [0, 0, 0, 1],
        ... ]
        >>> colonne(matrice, 1)
        [1, 0, 0]
        ```

        ```pycon title=""
        >>> matrice = [
        ...     ['a', 'b', 'c'],
        ...     ['d', 'e', 'f'],
        ...     ['g', 'h', 'i'],
        ...     ['j', 'k', 'l']
        ... ]
        >>> colonne(matrice, 0)
        ['a', 'd', 'g', 'j']
        ```
        
    {{ IDE('exo_colonne') }}

??? question "Fonction `transposition`"

    La fonction `transposition` renvoie la matrice obtenue en transposant la  matrice représentée par la liste de listes nommée `matrice` passée en paramètre.

    ???+ example "Exemples"

        ```pycon title=""
        >>> matrice = [
        ...     [0, 1, 1, 1],
        ...     [1, 0, 1, 0],
        ...     [0, 0, 0, 1],
        ... ]
        >>> transposition(matrice)
        [[0, 1, 0], [1, 0, 0], [1, 1, 0], [1, 0, 1]]
        ```

        ```pycon title=""
        >>> matrice = [
        ...     ['a', 'b', 'c'],
        ...     ['d', 'e', 'f'],
        ...     ['g', 'h', 'i'],
        ...     ['j', 'k', 'l']
        ... ]
        >>> transposition(matrice)
        [['a', 'd', 'g', 'j'], ['b', 'e', 'h', 'k'], ['c', 'f', 'i', 'l']]

        ```

    {{ IDE('exo_transposition') }}


