# --------- PYODIDE:env --------- #
def hauteur(matrice):
    pass


# --------- PYODIDE:code --------- #
def hauteur(matrice): 
    ...


# --------- PYODIDE:corr --------- #
def hauteur(matrice):
    return len(matrice)


# --------- PYODIDE:tests --------- #
matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]
assert hauteur(matrice) == 3


matrice = [
    ["a", "b", "c"],
    ["d", "e", "f"],
    ["g", "h", "i"],
    ["j", "k", "l"],
]
assert hauteur(matrice) == 4


matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]
assert hauteur(matrice) == 3

# --------- PYODIDE:secrets --------- #


# autres tests


matrice = [
    ["#", "#", "#", "#", "#"],
    ["#", "#", "#", "#", "#"],
    [".", ".", "#", "#", "#"],
    ["#", ".", "#", "#", "#"],
]
assert hauteur(matrice) == 4


une_colonne = [
    [1],
    [3],
    [5],
    [7],
]
une_ligne = [[1, 2, 9, 3, 4, 9, 5, 6, 9, 7, 8, 9]]
assert hauteur(une_colonne) == 4, f"Erreur avec {une_colonne}"
assert hauteur(une_ligne) == 1, f"Erreur avec {une_ligne}"
