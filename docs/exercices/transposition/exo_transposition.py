# --------- PYODIDE:env --------- #
def ligne(matrice, i):
    return matrice[i]


def hauteur(matrice):
    return len(matrice)


def largeur(matrice):
    return len(matrice[0])


def colonne(matrice, j):
    return [matrice[i][j] for i in range(hauteur(matrice))]


# --------- PYODIDE:code --------- #
def transposition(matrice):
    ...


# --------- PYODIDE:corr --------- #
def transposition(matrice):
    return [colonne(matrice, j) for j in range(largeur(matrice))]


# --------- PYODIDE:tests --------- #
matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]

t_tab = [
    [0, 1, 0],
    [1, 0, 0],
    [1, 1, 0],
    [1, 0, 1],
]
assert transposition(matrice) == t_tab
assert transposition(t_tab) == matrice

matrice = [
    ["a", "b", "c"],
    ["d", "e", "f"],
    ["g", "h", "i"],
    ["j", "k", "l"],
]

t_tab = [
    ["a", "d", "g", "j"],
    ["b", "e", "h", "k"],
    ["c", "f", "i", "l"],
]
assert transposition(matrice) == t_tab
assert transposition(t_tab) == matrice

# --------- PYODIDE:secrets --------- #
matrice = [
    ["#", "#", "#", "#", "#"],
    ["#", "#", "#", "#", "#"],
    [".", ".", "#", "#", "#"],
    ["#", ".", "#", "#", "#"],
]

t_tab = [
    ["#", "#", ".", "#"],
    ["#", "#", ".", "."],
    ["#", "#", "#", "#"],
    ["#", "#", "#", "#"],
    ["#", "#", "#", "#"],
]
assert transposition(matrice) == t_tab
assert transposition(t_tab) == matrice


une_colonne = [
    [1],
    [2],
    [9],
    [3],
    [4],
    [9],
    [5],
    [6],
    [9],
    [7],
    [8],
    [9],
]
une_ligne = [[1, 2, 9, 3, 4, 9, 5, 6, 9, 7, 8, 9]]

assert transposition(une_ligne) == une_colonne, f"Erreur avec {une_ligne}"
assert transposition(une_colonne) == une_ligne, f"Erreur avec {une_colonne}"
