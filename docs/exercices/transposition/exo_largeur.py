# --------- PYODIDE:env --------- #
def largeur(matrice):
    pass

def hauteur(matrice):
    return len(matrice)

# --------- PYODIDE:code --------- #
def largeur(matrice): 
    ...


# --------- PYODIDE:corr --------- #
def largeur(matrice):
    return len(matrice[0])


# --------- PYODIDE:tests --------- #

matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]

assert largeur(matrice) == 4

matrice = [
    ["a", "b", "c"],
    ["d", "e", "f"],
    ["g", "h", "i"],
    ["j", "k", "l"],
]

assert largeur(matrice) == 3


# --------- PYODIDE:secrets --------- #


# autres tests


matrice = [
    ["#", "#", "#", "#", "#"],
    ["#", "#", "#", "#", "#"],
    [".", ".", "#", "#", "#"],
    ["#", ".", "#", "#", "#"],
]

assert largeur(matrice) == 5


une_colonne = [
    [1],
    [3],
    [5],
    [7],
]
une_ligne = [[1, 2, 9, 3, 4, 9, 5, 6, 9, 7, 8, 9]]

assert largeur(une_colonne) == 1, f"Erreur avec {une_colonne}"
assert largeur(une_ligne) == 12, f"Erreur avec {une_ligne}"
