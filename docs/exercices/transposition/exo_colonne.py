# --------- PYODIDE:env --------- #
def ligne(matrice, i):
    return matrice[i]

def hauteur(matrice):
    return len(matrice)

def largeur(matrice):
    return len(matrice[0])


# --------- PYODIDE:code --------- #
def colonne(matrice, j):
    return [matrice[...][...] for i in range(...)]


# --------- PYODIDE:corr --------- #
def colonne(matrice, j):
    return [matrice[i][j] for i in range(hauteur(matrice))]


# --------- PYODIDE:tests --------- #
matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]

assert colonne(matrice, 1) == [1, 0, 0]

matrice = [
    ["a", "b", "c"],
    ["d", "e", "f"],
    ["g", "h", "i"],
    ["j", "k", "l"],
]

assert colonne(matrice, 0) == ["a", "d", "g", "j"]


# --------- PYODIDE:secrets --------- #


# autres tests


matrice = [
    ["#", "#", "#", "#", "#"],
    ["#", "#", "#", "#", "#"],
    [".", ".", "#", "#", "#"],
    ["#", ".", "#", "#", "#"],
]

assert colonne(matrice, 0) == ["#", "#", ".", "#"]


une_colonne = [
    [1],
    [3],
    [5],
    [7],
]
une_ligne = [[1, 2, 9, 3, 4, 9, 5, 6, 9, 7, 8, 9]]

assert colonne(une_colonne, 0) == [
    1,
    3,
    5,
    7,
], f"Erreur avec la colonne 0 de {une_colonne}"
