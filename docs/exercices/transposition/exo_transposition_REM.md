Il est aussi possible de n'utiliser qu'une seule fonction :

```python
def transposition(tab):
    n = len(tab)
    p = len(tab[0])
    t_tab = [[0 for i in range(n)] for j in range(p)]
    for j in range(p):
        for i in range(n):
            t_tab[j][i] = tab[i][j]
    return t_tab
```

On peut, enfin, utiliser une liste en compréhension :

```python
def transposition(tab):
    n = len(tab)
    p = len(tab[0])
    return [[tab[i][j] for i in range(n)] for j in range(p)]
```

