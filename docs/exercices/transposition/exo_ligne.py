# --------- PYODIDE:env --------- #
def ligne(matrice, i):
    pass


def hauteur(matrice):
    return len(matrice)


def largeur(matrice):
    return len(matrice[0])


# --------- PYODIDE:code --------- #
def ligne(matrice, i):
    ...


# --------- PYODIDE:corr --------- #
def ligne(matrice, i):
    return matrice[i]


# --------- PYODIDE:tests --------- #

matrice = [
    [0, 1, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
]

assert ligne(matrice, 0) == [0, 1, 1, 1]


matrice = [
    ["a", "b", "c"],
    ["d", "e", "f"],
    ["g", "h", "i"],
    ["j", "k", "l"],
]

assert ligne(matrice, 3) == ["j", "k", "l"]

# --------- PYODIDE:secrets --------- #


# autres tests


matrice = [
    ["#", "#", "#", "#", "#"],
    ["#", "#", "#", "#", "#"],
    [".", ".", "#", "#", "#"],
    ["#", ".", "#", "#", "#"],
]

assert ligne(matrice, 3) == ["#", ".", "#", "#", "#"]


une_colonne = [
    [1],
    [3],
    [5],
    [7],
]
une_ligne = [[1, 2, 9, 3, 4, 9, 5, 6, 9, 7, 8, 9]]

assert ligne(une_colonne, 0) == [1], f"Erreur avec la ligne 0 de {une_colonne}"
assert ligne(une_ligne, 0) == [
    1,
    2,
    9,
    3,
    4,
    9,
    5,
    6,
    9,
    7,
    8,
    9,
], f"Erreur avec la ligne 0 de {une_ligne}"
