---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Double du précédent dans un tableau
tags:
    - liste/tableau
    - booléen
difficulty: 120
---


Écrire une fonction `nombres_puis_double` qui prend en paramètre un tableau de nombres entiers `valeurs`. Cette fonction renvoie la liste (éventuellement vide) des couples d'entiers `(a, b)` tels que `a` et `b` soient deux termes successifs de `valeurs`, et la valeur de `b` soit le double de celle de `a`.

???+ example "Exemples"

    ```pycon title=""
    >>> nombres_puis_double([1, 4, 2, 5])
    []
    >>> nombres_puis_double([1, 3, 6, 7])
    [(3, 6)]
    >>> nombres_puis_double([7, 1, 2, 5, 3, 6])
    [(1, 2), (3, 6)]
    >>> nombres_puis_double([5, 1, 2, 4, 8, -5, -10, 7])
    [(1, 2), (2, 4), (4, 8), (-5, -10)]
    ```

{{ IDE('exo') }}
