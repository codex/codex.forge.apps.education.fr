---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Géométrie vectorielle
tags:
    - maths
    - tuple
    - float
difficulty: 60
maj: 01/03/2024
---

Cette page regroupe différents exercices simples sur la géométrie vectorielle. 

Les exercices sont tous indépendants et peuvent être traités dans un ordre quelconque.

??? note "Coordonnées décimales"

    Dans tous les exercices, les coordonnées proposées sont des nombres décimaux (nombres flottants dit-on en informatique).
    
    Python représente les nombres décimaux en binaire et doit, dans certains cas, les arrondir.
        
    Dans ces exercices, les tests secrets vérifient donc que les valeurs renvoyées par chaque fonction sont proches à $10^{-6}$ près des valeurs attendues.

{{ remarque('assertion') }}

??? question "Milieu d'un segment"

    Soit $A$ et $B$ deux points du plan.
    
    Écrire la fonction `milieu` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` des points $A$
    et $B$ et renvoie les coordonnées du milieu $M$ de $\left[AB\right]$.
    
    ??? note "~~Entier~~ $\longrightarrow$ Décimal"
    
        Lorsque Python effectue une division, il considère toujours que le résultat est un nombre décimal (un nombre flottant dit-on en informatique).
        
        C'est pourquoi l'expression `#!py (2 + 6) / 2` est évaluée à `#!py 4.0`.
        
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> milieu(0, 2, 3, 6)
        (1.5, 4.0)
        >>> milieu(10, 4, 5, -8)
        (7.5, -2.0)
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_milieua') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_milieub') }}

??? question "Coordonnées d'un vecteur"

    Soit $A$ et $B$ deux points du plan.
    
    Écrire la fonction `vecteur` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` des points $A$
    et $B$ et renvoie les coordonnées du vecteur $\overrightarrow{AB}$.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> vecteur(0, 2, 3, 6)
        (3, 4)
        >>> vecteur(10, 4, 5, -8)
        (-5, -12)
        ```
    
    === "Version vide"
        {{ IDE('./pythons/exo_vecteura') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_vecteurb') }}

??? question "Norme d'un vecteur"

    Soit $\vec{u}$ un vecteur du plan muni d'un repère orthonormé.
    
    Écrire la fonction `norme` qui prend en paramètres les coordonnées `x_U` et `y_U` du vecteur $\vec{u}$ renvoie la norme $\lvert\lvert\vec{u}\lvert\lvert$.

    ??? tip "Racine carrée"
    
        Il est possible de calculer la racine carrée d'un nombre positif en utilisant la fonction `#!py sqrt` présente dans le module `#!py math`.
        
        On fait alors `#!py sqrt(5)` pour calculer $\sqrt5$.
        
        L'import est fait au début du code et la fonction est donc directement utilisable.
    
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> norme(3, 4)
        5.0
        >>> norme(-5, -12)
        13.0
        ```
        
    {{ IDE('./pythons/exo_normea') }}

??? question "Longueur d'un segment"

    Soit $A$ et $B$ deux points du plan muni d'un repère orthonormé.
    
    Écrire la fonction `distance` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` des points $A$
    et $B$ et renvoie la longueur $AB$.

    ??? tip "Racine carrée"
    
        Il est possible de calculer la racine carrée d'un nombre positif en utilisant la fonction `#!py sqrt` présente dans le module `#!py math`.
        
        On fait alors `#!py sqrt(5)` pour calculer $\sqrt5$.
        
        L'import est fait au début du code et la fonction est donc directement utilisable.

    ??? tip "Fonctions `vecteur` et `norme` chargées"
    
        Les fonctions `vecteur` et `norme` décrites dans les exercices précédents sont déjà chargées dans l'éditeur.
        
        Vous pouvez les utiliser si vous le souhaitez.

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> distance(0, 2, 3, 6)
        5.0
        >>> distance(10, 4, 5, -8)
        13.0
        ```
        
    {{ IDE('./pythons/exo_distancea') }}
    
??? question "Vecteurs colinéaires"

    Soit $\vec{u}$ et $\vec{v}$ deux vecteurs du plan.
    
    Écrire la fonction `colineaires` qui prend en paramètres les coordonnées `x_u`, `y_u`, `x_v` et `y_v` des vecteurs $\vec{u}$ et $\vec{v}$ et renvoie la valeur booléenne indiquant si ces vecteurs sont colinéaires ou non.

    ??? note "Valeur booléenne"
    
        Il n'existe que deux valeurs booléennes : $Vrai$ et $Faux$.
        
        En Python on les note `#!py True` et `#!py False`.


    ???+ example "Exemples"
    
        ```pycon title=""
        >>> colineaires(3, 8, -6, -16)
        True
        >>> colineaires(3, 8, 6, 17)
        False
        ```    

    {{ IDE('./pythons/exo_colineairea') }}
    
??? question "Points alignés"

    Soit $A$, $B$ et $C$ trois points du plan.
    
    Écrire la fonction `alignes` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B`, `y_B`, `x_C` et `y_C` des points $A$, $B$ et $C$ et renvoie la valeur booléenne indiquant si ces points sont alignés ou non.
    
    ??? note "Valeur booléenne"
    
        Il n'existe que deux valeurs booléennes : $Vrai$ et $Faux$.
        
        En Python on les note `#!py True` et `#!py False`.
        
    ??? tip "Fonctions `vecteur` et `colineaires` chargées"
    
        Les fonctions `vecteur` et `colineaires` décrites dans les exercices précédents sont déjà chargées dans l'éditeur.
        
        Vous pouvez les utiliser si vous le souhaitez.

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> alignes(-2, 2, 1, 1, 7, -1)
        True
        >>> alignes(-2, 2, 1, 1, 7, -2)
        False
        ```

    === "Version vide"
        {{ IDE('./pythons/exo_alignesa') }}
    
    === "Version à compléter"
        {{ IDE('./pythons/exo_alignesb') }}
    
    === "Version à compléter (bis)"
        {{ IDE('./pythons/exo_alignes_bisa') }}
