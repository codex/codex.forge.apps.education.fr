---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Indice ou valeur ?
tags:
    - liste/tableau
---



On donne un tableau de nombres `nombres`.

Compléter le code des deux fonctions ci-dessous :

* `somme_par_indices_pairs` prend le tableau en argument et renvoie la somme des valeurs placées à des **indices pairs** ;
  
* `somme_des_valeurs_paires` prend le tableau en argument et renvoie la somme des **valeurs paires**.

Par exemple :

```pycon
>>> # indices  0  1  2
>>> nombres = [4, 6, 3]
>>> somme_par_indices_pairs(nombres)
7
>>> somme_des_valeurs_paires(nombres)
10
```

En effet, :

* il y a deux indices pairs (`#!py 0` et `#!py 2`) et la somme des valeurs correspondantes vaut `#!py 7`.
* il y a deux valeurs paires (`#!py 4` et `#!py 6`) et leur somme vaut `#!py 10`.

On rappelle qu'il est possible de tester la parité d'un nombre `n` en faisant `n % 2 == 0` :

```pycon
>>> 14 % 2 == 0
True
>>> 15 % 2 == 0
False
```


???+ example "Exemples"

    ```pycon title=""
    >>> somme_par_indices_pairs([]) 
    0
    >>> somme_des_valeurs_paires([]) 
    0
    >>> somme_par_indices_pairs([4, 6, 3])
    7
    >>> somme_des_valeurs_paires([4, 6, 3])
    10
    ```

{{ remarque('assertion') }}

{{ IDE('exo') }}
