Pour le parcours sur les indices, on pourrait aussi **jouer sur le pas** :

```python
def somme_par_indices_pairs(nombres):
    somme = 0
    for i in range(0, len(nombres), 2):
        somme += nombres[i]
    return somme
```

Dans ce cas, on ne lit qu'un indice sur deux en débutant à `0`.

Dans le même esprit, les tranches permettent de modifier les listes **en omettant certains indices** :

```python
def somme_par_indices_pairs(nombres):
    somme = 0
    for x in nombres[::2]:
        somme += x
    return somme
```

Dans ce cas, on indique à Python de ne prendre qu'un nombre `x` sur deux. La notation `nombres[::2]` est un raccourci pour
`nombres[0:len(nombres):2]`.

D'autre part, la fonction `#!py sum` permet d'utiliser une approche **fonctionnelle** :

```python
def somme_par_indices_pairs(nombres):
    return sum([nombres[i] for i in range(0, len(nombres), 2)])
    
def somme_des_valeurs_paires(nombres):
    return sum([x for x in nombres if x % 2 == 0])
```
