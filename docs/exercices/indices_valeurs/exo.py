

# --------- PYODIDE:code --------- #

def somme_par_indices_pairs(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

def somme_des_valeurs_paires(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

# --------- PYODIDE:corr --------- #

def somme_par_indices_pairs(nombres):
    somme = 0
    for i in range(len(nombres)):
        if i % 2 == 0:
            somme += nombres[i]

    return somme


def somme_des_valeurs_paires(nombres):
    somme = 0
    for x in nombres:
        if x % 2 == 0:
            somme += x

    return somme

# --------- PYODIDE:tests --------- #

assert somme_par_indices_pairs([]) == 0
assert somme_des_valeurs_paires([]) == 0
assert somme_par_indices_pairs([4, 6, 3])== 7
assert somme_des_valeurs_paires([4, 6, 3])== 10

# --------- PYODIDE:secrets --------- #

# Tests
assert somme_par_indices_pairs([]) == 0
assert somme_des_valeurs_paires([]) == 0
assert somme_par_indices_pairs([4, 6, 3]) == 7
assert somme_des_valeurs_paires([4, 6, 3]) == 10

# Tests supplémentaires
nombres = [-17, 9, -8, -4, 3, 15, 12, -2, -19, 10]
assert somme_par_indices_pairs(nombres) == -29
assert somme_des_valeurs_paires(nombres) == 8
nombres = [-5, -2, -20, -5, -18, -12, -2, -20, -10, -10]
assert somme_par_indices_pairs(nombres) == -55
assert somme_des_valeurs_paires(nombres) == -94
