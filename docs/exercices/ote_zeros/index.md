---
author:
    - Guillaume Connan
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Ôte zéros
tags:
    - liste/tableau
    - binaire
difficulty: 150
maj: 01/03/2024
---



On dispose  d'une liste de `#!py 0`  et de `#!py 1`  qui représente un nombre entier en
base $2$, le bit de poids faible se trouvant à gauche.

Par exemple, le nombre $6$ s'écrit $110$ en binaire en plaçant le bit de poids faible à droite 
et $011$ en écrivant ce bit de poids faible à gauche. Ce nombre est représenté en machine
par la liste `#!py [0, 1, 1]`. Il est aussi possible de le représenter avec des `#!py 0` inutiles par `#!py [0, 1, 1, 0, 0, 0]` 
de la même façon que l'on peut écrire, de façon impropre, $6=0006$.

Écrire la fonction `ote_zeros` qui prend une liste `bits` représentant un nombre en binaire 
(avec le bit de poids faible à gauche) et supprime les éventuels zéros inutiles à la droite de `bits`.


On garantit que la liste passée en argument est non vide.

??? warning "Modification en place"

    La liste `bits` passée en argument est directement modifiée. Il est inutile de la renvoyer.

???+ example "Exemples"

    ```pycon title=""
    >>> zero = [0, 0, 0, 0]
    >>> ote_zeros(zero)
    >>> zero
    [0]
    >>> un = [1]
    >>> ote_zeros(un)
    >>> un
    [1]
    >>> six = [0, 1, 1, 0, 0, 0]
    >>> ote_zeros(six)
    >>> six
    [0, 1, 1]
    ```

{{ IDE('./pythons/exo') }}
