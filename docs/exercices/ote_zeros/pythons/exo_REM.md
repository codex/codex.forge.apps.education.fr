Il est possible d'alléger un peu l'écriture en utilisant `#!py -1` pour accéder au dernier élément de la liste :

```python
def ote_zeros(bits):
    while len(bits) > 1 and bits[-1] == 0:
        bits.pop()
```
