

# --------- PYODIDE:code --------- #

def ote_zeros(bits):
    ...

# --------- PYODIDE:corr --------- #

def ote_zeros(bits):
    while len(bits) > 1 and bits[len(bits) - 1] == 0:
        bits.pop()

# --------- PYODIDE:tests --------- #

zero = [0, 0, 0, 0]
ote_zeros(zero)
assert zero == [0]
un = [1]
ote_zeros(un)
assert un == [1]
six = [0, 1, 1, 0, 0, 0]
ote_zeros(six)
assert six == [0, 1, 1]

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(20):
    n = randrange(2**8, 2**12 + 1)
    zeros = [0] * randrange(0, 20)
    attendu = list(map(int, bin(n)[2:][::-1]))
    bits = attendu + zeros
    bits_ = bits.copy()
    ote_zeros(bits)
    assert bits == attendu, f"Erreur avec {bits_}"