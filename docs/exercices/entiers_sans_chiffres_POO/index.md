---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Écrire les entiers sans les chiffres (POO)
tags:
    - à trous
    - int
    - programmation orientée objet
    - supérieur
difficulty: 310
maj: 23/08/2024
---

On souhaite dans cet exercice créer une classe `Entier` permettant de représenter des **nombres entiers positifs ou nuls**, de tester leur égalité, de les additionner et les soustraire.

On fournit donc une classe `#!py Entier` dont l'interface est détaillée ci-dessous dans le cadre d'un exemple :

* `#!py a = Entier()` : crée un objet représentant l'entier $0$ et l'affecte à la variable `#!py a` ;

* `#!py a.incremente()` : augmente la valeur de l'entier représenté par `#!py a` de une unité. `#!py a` représente désormais l'entier $1$ ;
* `#!py a.est_nul()` : renvoie le booléen indiquant si `#!py a` représente l'entier nul. Dans le cas présent, cette expression est évaluée à `#!py False` ;
* `#!py b = a.copie()` : crée un entier de même valeur que `#!py a` et l'affecte à la variable `#!py b`. Ces deux objets sont distincts : modifier l'un n'a pas d'incidence sur l'autre ;
* `#!py b = b.decremente()` : diminue la valeur de l'entier représenté par `#!py b` de une unité. `#!py b` représente désormais l'entier $0$, `#!py a` représente toujours $1$.

La méthode `#!py decremente` génère une `#!py ValueError` si l'entier auquel on l'applique représente $0$. Il est en effet impossible de représenter un entier inférieur à $0$ dans le cadre des entiers positifs.

Il est aussi possible de tester l'égalité de deux entiers en faisant par exemple `#!py a == b` qui, dans le cas présent, est évalué à `#!py False`.

La classe `Entier` est fournie pour information ci-dessous.

??? info "Classe `Entier`"

    Les entiers sont représentés par des listes imbriquées. Ainsi :

    * $0$ est représenté par la liste vide `#!py []`,
    * $1$ est représenté par la liste contenant une liste vide `#!py [[]]`,
    * $2$ est représenté par la liste contenant une liste contenant une liste vide `#!py [[[]]]`,
    * etc...

    Le code ci-dessous est fourni à titre indicatif. On doit résoudre l'exercice en n'utilisant que les méthodes `est_nul`, `copie`, `incremente` et `decremente`.

    ```python title=""
    class Entier:
        def __init__(self):
            "Crée un entier égal à zéro"
            self.liste = []

        def est_nul(self):
            "Indique si cet entier est nul ou non"
            return self.liste == []

        def incremente(self):
            "Augmente la valeur de cet entier de un"
            self.liste = [self.liste]

        def decremente(self):
            "Diminue, si possible, la valeur de cet entier de un"
            if self.est_nul():
                raise ValueError("L'entier est nul")
            else:
                self.liste = self.liste.pop()

        def copie(self):
            "Renvoie un entier égal à cet entier"
            nouveau = Entier()
            while nouveau.liste != self.liste:
                nouveau.incremente()
            return nouveau

        def __eq__(self, autre):
            "Teste l'égalité de deux entiers"
            return self.liste == autre.liste

        def __repr__(self):
            "Renvoie la représentation d'un objet de cet entier"
            return f"'Entier' représenté par {self.liste}"
    ```

    La méthode `#!py __eq__` a un statut particulier. Elle fait partie des [*méthodes spéciales de Python*](https://docs.python.org/fr/3/reference/datamodel.html#special-method-names). Définir la méthode `__eq__` dans une classe permet de tester l'égalité de deux instances de cette classe en faisant `a == b`. Dans ce cas, Python exécute l'action `#!py a.__eq__(b)`.

    De la même façon, `#!py __repr__` est une méthode spéciale permettant de représenter un objet dans la console.

On souhaite mettre en place l'addition et la soustraction d'entiers. Pour cela on complète les méthodes spéciales  `__add__` et `__sub__` qui sont appelées lorsque l'on exécute `a + b` et `a - b`. Ces méthodes créent chacune un nouvel entier représentant le nombre correspondant à l'opération demandée.

**Attention**, certaines soustractions sont impossibles dans le cadre des entiers positifs ou nuls. Dans ce cas la méthode `#!py __sub__` générera une erreur du type `#!py ArithmeticError`.

???+ example "Exemples"

    Ci-dessous, les objets `zero`, `un`, `deux` et `trois` sont des instances de la classe `Entier`représentant les nombres entiers correspondants.

    ```pycon title=""
    >>> resultat = zero + zero
    >>> resultat == zero
    True
    >>> resultat = un + deux
    >>> resultat == trois
    True
    >>> resultat = zero - zero
    >>> resultat == zero
    True
    >>> resultat = trois - un
    >>> resultat == deux
    True
    ```

???+ tip "Générer une erreur"

    Il est possible avec Python d'interrompre l'exécution du code en générant une erreur. Pour ce faire on utilise l'instruction `#!py raise <Erreur>`. La méthode `decremente` génère ainsi une erreur : `#!py raise ValueError("L'entier est nul")`.

    Dans le cas présent, si une soustraction impossible est effectuée, on saisira `#!py raise ArithmeticError("Opération impossible")`.

    Python propose de plus un mécanisme permettant d'intercepter de telles erreurs sans interrompre l'exécution. Cette structure utilise les mots clés [`#!py try` et `#!py except`](https://docs.python.org/fr/3/tutorial/errors.html#handling-exceptions) et est utilisée dans les tests de cet exercice.

???+ danger "Contrainte"

    Dans tout l'exercice, il est interdit d'accéder aux attributs des objets de la classe `Entier`.

    On n'utilisera que les méthodes `#!py incremente`, `#!py decremente` et `#!py copie`.


=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
