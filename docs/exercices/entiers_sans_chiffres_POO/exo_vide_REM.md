Cet exercice permet de se familiariser avec les méthodes spéciales de Python. Outre celles mentionnées dans le sujet, on rencontre entre autres :

* `__ne__` : détermine si deux objets sont différents (*not equal*),

* `__gt__` : détermine si l'objet qui subit la méthode est strictement supérieur à celui passé en argument (*greater than*),
* `__ge__` : détermine si l'objet qui subit la méthode est supérieur ou égal à celui passé en argument (*greater or equal*),
* `__lt__` : détermine si l'objet qui subit la méthode est strictement inférieur à celui passé en argument (*less than*),
* `__le__` : détermine si l'objet qui subit la méthode est inférieur ou égal à celui passé en argument (*less or equal*),
* `__mul__` : multiplie l'objet qui subit la méthode et celui passé en argument. Renvoie un nouvel objet.