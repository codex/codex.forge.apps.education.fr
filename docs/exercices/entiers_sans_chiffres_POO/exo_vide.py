# --------- PYODIDE:code --------- #
class Entier:
    def __init__(self):
        "Crée un entier égal à zéro"
        self.liste = []

    def est_nul(self):
        "Indique si cet entier est nul ou non"
        return self.liste == []

    def incremente(self):
        "Augmente la valeur de cet entier de un"
        self.liste = [self.liste]

    def decremente(self):
        "Diminue, si possible, la valeur de cet entier de un"
        if self.est_nul():
            raise ValueError("L'entier est nul")
        else:
            self.liste = self.liste.pop()

    def copie(self):
        "Renvoie un entier égal à cet entier"
        nouveau = Entier()
        while nouveau.liste != self.liste:
            nouveau.incremente()
        return nouveau

    def __eq__(self, autre):
        "Teste l'égalité de deux entiers"
        return self.liste == autre.liste

    def __repr__(self):
        "Renvoie la représentation d'un objet de cet entier"
        return f"'Entier' représenté par {self.liste}"

    def __add__(self, autre):
        """
        Additionne cet entier à un autre entier
        Renvoie un nouvel entier
        """
        ...

    def __sub__(self, autre):
        """
        Soustrait un autre entier à celui-ci
        Renvoie un nouvel entier
        Génère une ArithmeticError si l'opération renvoie un entier strictement négatif
        """
        ...


# --------- PYODIDE:corr --------- #


class Entier:
    def __init__(self):
        "Crée un entier égal à zéro"
        self.liste = []

    def est_nul(self):
        "Indique si cet entier est nul ou non"
        return self.liste == []

    def incremente(self):
        "Augmente la valeur de cet entier de un"
        self.liste = [self.liste]

    def decremente(self):
        "Diminue, si possible, la valeur de cet entier de un"
        if self.est_nul():
            raise ValueError("L'entier est nul")
        else:
            self.liste = self.liste.pop()

    def copie(self):
        "Renvoie un entier égal à cet entier"
        nouveau = Entier()
        while nouveau.liste != self.liste:
            nouveau.incremente()
        return nouveau

    def __eq__(self, autre):
        "Teste l'égalité de deux entiers"
        return self.liste == autre.liste

    def __repr__(self):
        "Renvoie la représentation d'un objet de cet entier"
        return f"'Entier' représenté par {self.liste}"

    def __add__(self, autre):
        """
        Additionne cet entier à un autre entier
        Renvoie un nouvel entier
        """
        resultat = self.copie()
        temp = autre.copie()
        while not temp.est_nul():
            temp.decremente()
            resultat.incremente()
        return resultat

    def __sub__(self, autre):
        """
        Soustrait un autre entier à celui-ci
        Renvoie un nouvel entier
        Génère une ArithmeticError si l'opération renvoie un entier strictement négatif
        """
        resultat = self.copie()
        temp = autre.copie()
        while not temp.est_nul():
            if resultat.est_nul():
                raise ArithmeticError("Opération impossible")
            temp.decremente()
            resultat.decremente()
        return resultat


# --------- PYODIDE:tests --------- #

# Nombres usuels
zero = Entier()

un = Entier()
un.incremente()
un_bis = un.copie()

deux = Entier()
deux.incremente()
deux.incremente()
deux_bis = deux.copie()

trois = Entier()
trois.incremente()
trois.incremente()
trois.incremente()
trois_bis = trois.copie()

# Additions
assert zero + zero == zero

assert un + deux == trois
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

# Différences
assert zero - zero == zero

assert trois - un == deux
assert trois == trois_bis and un == un_bis, "Les objets ne doivent pas être modifiés"

try:
    un - deux
except ArithmeticError:
    pass
except:
    raise AssertionError("Cette opération devrait générer une ArithmeticError")

# --------- PYODIDE:secrets --------- #
# Tests supplémentaires
nombres = [Entier()]
for k in range(1, 20):
    n = nombres[-1].copie()
    n.incremente()
    nombres.append(n)
for i in range(10):
    a = nombres[i]
    b = nombres[i + 1]
    assert a + b == nombres[2 * i + 1], f"Erreur sur une addtion"
    assert b - a == nombres[1], f"Erreur sur une soustraction"
    try:
        a - b
    except ArithmeticError:
        pass
    except:
        raise AssertionError("Une soustraction impossible devrait générer une ArithmeticError")
