

# --------- PYODIDE:code --------- #

def supprime_premier(chaine, cible):
    ...


def anagrammes(chaine1, chaine2):
    ...

# --------- PYODIDE:corr --------- #

def supprime_premier(chaine: str, cible: str) -> str:
    resultat = ""
    deja_vu = False
    for caractere in chaine:
        if caractere == cible and not deja_vu:
            deja_vu = True
        else:
            resultat += caractere
    return deja_vu, resultat


def anagrammes(chaine1: str, chaine2: str) -> bool:
    if len(chaine1) == 1:
        return chaine1 == chaine2
    else:
        cible = chaine1[0]
        vrai, chaine1_sans_cible = supprime_premier(chaine1, cible)
        cible_dans_2, chaine2_sans_cible = supprime_premier(chaine2, cible)
        if cible_dans_2:
            return anagrammes(chaine1_sans_cible, chaine2_sans_cible)
        else:
            return False

# --------- PYODIDE:tests --------- #

assert supprime_premier("ukulélé", "u") == (True, "kulélé")
assert supprime_premier("ukulélé", "é") == (True, "ukullé")
assert supprime_premier("ukulélé", "a") == (False, "ukulélé")
assert anagrammes("chien", "niche")
assert anagrammes("énergie noire", "reine ignorée")
assert anagrammes("louve", "poule") is False

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert supprime_premier("mississippi", "m") == (True, "ississippi")
assert supprime_premier("mississippi", "a") == (False, "mississippi")
assert anagrammes("nsi", "isn")
assert anagrammes("nsi", "snt") is False
assert anagrammes("clint  eastwood", "old west action")
assert anagrammes("astronomers ", "moon starers")
assert anagrammes("astronome", "metronome") is False