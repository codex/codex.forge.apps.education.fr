---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Cadeaux circulaires
tags:
    - à trous
    - dictionnaire
    - graphe
    - ep2
difficulty: 140
---



{{ version_ep() }}

Pour fêter la nouvelle année, un groupe d'amis a décidé qu'il y aurait une distribution de petits cadeaux lors de la soirée.
Pour préparer cela, chacun a écrit son nom sur un papier qu'il a déposé dans un chapeau.

Au moment de la distribution de cadeaux, chaque participant tire un papier. Il découvre ainsi la personne à laquelle il va devoir faire son cadeau (potentiellement lui).

Un tel appariement des invités s'appelle en mathématiques une *permutation*. On cherche dans cet exercice à déterminer si cette permutation est *circulaire*.

On garantit que :

- il y a au moins une personne invitée à cette fête ;
- chaque personne fait un cadeau à une unique personne (éventuellement elle-même),
- chaque personne ne reçoit qu'un unique cadeau (éventuellement en provenance d'elle-même).

On représente une distribution par un dictionnaire **Python** dans lequel :

* les clés sont les personnes offrant le cadeau ;
* les valeurs sont les personnes recevant le cadeau.

!!! info "Conséquence des règles sur le dictionnaire"

    Les règles garantissent que :
    
    * le dictionnaire est non-vide
    * chaque personne apparaît exactement une fois en tant que clé du dictionnaire
    * et exactement une fois en tant que valeur associée à une clé.


??? example "Exemple pour `distribution_a`"

    Voici un exemple, avec 6 personnes, de « distribution » de cadeaux qui respecte les
    règles ci-dessus :

    - Anne fait un cadeau à Élodie ;
    - Élodie fait un cadeau à Bruno ;
    - Bruno fait un cadeau à Fidel ;
    - Fidel fait un cadeau à Anne ;
    - Claude fait un cadeau à Denis ;
    - Denis fait un cadeau à Claude.

    ```mermaid
    flowchart LR
        A(["Anne"]) --> E(["Élodie"])
        E --> B(["Bruno"])
        B --> F(["Fidel"])
        F --> A
        C(["Claude"]) --> D(["Denis"])
        D --> C
    ```

    Le dictionnaire correspondant à cette distribution est le suivant :

    ```python
    distribution_a = {
        "Anne": "Élodie",
        "Élodie": "Bruno",
        "Bruno": "Fidel",
        "Fidel": "Anne",
        "Claude": "Denis", 
        "Denis": "Claude",
        }
    ```

    Dans cette distribution il y a deux cycles distincts : 
    
    * un premier cycle avec Anne, Élodie, Bruno, Fidel ;
    * et un second cycle avec Claude et Denis. 

    Cette distribution **n'est donc pas** circulaire.

??? example "Exemple pour `distribution_b`"

        
    ```python
    distribution_b = {
        "Anne": "Claude",
        "Bruno": "Fidel",
        "Claude": "Élodie", 
        "Denis": "Anne",
        "Élodie": "Bruno",
        "Fidel": "Denis"
        }
    ```

    ```mermaid
    flowchart LR
        A(["Anne"]) --> C(["Claude"])
        B([Bruno]) --> F(["Fidel"])
        C(["Claude"]) --> E(["Élodie"])
        D([Denis]) --> A
        E --> B
        F --> D
    ```

    Cette distribution comporte un unique cycle : Anne, Claude, Élodie, Bruno, Fidel, Denis. Cette distribution **est** donc circulaire.
    
Une présentation est dite *circulaire* si elle ne présente qu'un unique cycle. Pour savoir si une distribution est circulaire, on peut utiliser l'algorithme ci-dessous :

- on part d'un donateur initial;
- on inspecte son destinataire dans la distribution,
- ce destinataire devient à son tour donateur ;
- on recommence le parcours jusqu'à ce que le destinataire soit le donateur initial ;
- la distribution est circulaire si on l'a effectué autant de « sauts » qu'il y a de personnes dans le groupe.

Compléter la fonction `#!py est_circulaire`.

???+ example "Exemples"

    ```pycon title="" title=""
    >>> distribution_a = {
        "Anne": "Élodie",
        "Bruno": "Fidel",
        "Claude": "Denis",
        "Denis": "Claude",
        "Élodie": "Bruno",
        "Fidel": "Anne"
        }
    >>> est_circulaire(distribution_a)
    False
    >>> distribution_b = {
        "Anne": "Claude",
        "Bruno": "Fidel",
        "Claude": "Élodie",
        "Denis": "Anne",
        "Élodie": "Bruno",
        "Fidel": "Denis"
        }
    >>> est_circulaire(distribution_b)
    True
    ```

=== "Version vide"
    {{ IDE('exo_a') }}

=== "Version à compléter"
    {{ IDE('exo_b') }}







