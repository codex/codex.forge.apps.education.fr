

# --------- PYODIDE:code --------- #

def matrice_vers_dict(matrice):
    ...
    
# --------- PYODIDE:corr --------- #

def matrice_vers_dict(matrice):
    d = {}
    for i in range(len(matrice)):
        d[i] = []
        for j in range(len(matrice[i])):
            if matrice[i][j] == 1:
                d[i].append(j)
    return d

# --------- PYODIDE:tests --------- #

m = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
assert matrice_vers_dict(m) == {0: [1, 2], 1: [0, 2, 3], 2: [0, 1, 3], 3: [1, 2]}
m = [[0, 1], [1, 0]]
assert matrice_vers_dict(m) == {0: [1], 1: [0]}
m = [[0, 0], [1, 0]]
assert matrice_vers_dict(m) == {0: [], 1: [0]}
m = [[0, 0], [0, 0]]
assert matrice_vers_dict(m) == {0: [], 1: []}
m = [[0]]
assert matrice_vers_dict(m) == {0: []}
m = [[1]]
assert matrice_vers_dict(m) == {0: [0]}

# --------- PYODIDE:secrets --------- #


# tests secrets
m = [[0] * 10**3 for i in range(10**3)]
d = {i: [] for i in range(10**3)}
assert matrice_vers_dict(m) == d, "Erreur avec m = [[0] * 10**3 for i in range(10**3)]"
m = [[1] * 10**3 for i in range(10**3)]
d = {i: [k for k in range(10**3)] for i in range(10**3)}
assert matrice_vers_dict(m) == d, "Erreur avec m = [[1] * 10**3 for i in range(10**3)]"
m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]
d = {i: [k for k in range(10**3)] if i%2 else [] for i in range(10**3)}
assert matrice_vers_dict(m) == d, "Erreur avec m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]"