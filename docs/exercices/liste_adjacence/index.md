---
author: Serge Bays
hide:
    - navigation
    - toc
title: Liste d'adjacence, matrice d'adjacence
tags:
    - graphe
difficulty: 250
maj: 01/03/2024
---



Un graphe fini peut être représenté par une *matrice d'adjacence* ou par une *liste d'adjacence*.

 
Une *matrice d'adjacence* est une matrice $n$ lignes, $n$ colonnes, si le graphe contient $n$ sommets.   
Les sommets sont numérotés de $0$ à $n-1$. À l'intersection de la ligne $i$  et de la colonne $j$, on écrit un $1$ si $j$ est un successeur de $i$ et un $0$ sinon.

Une *liste d'adjacence* est constituée de listes regroupées dans un tableau ou un dictionnaire.   
Chacune des listes contient les successeurs (ou les prédécesseurs) d'un sommet.

???+ example "Un exemple de graphe"

    ![graphe orienté](g1.svg){width=25% .autolight .center}

    La matrice d'adjacence est :	
	
    \[
	\left(
    \begin{array}{cccccc}
    0 & 1 & 1 & 0 & 0 & 0 \\
	1 & 0 & 1 & 1 & 0 & 0 \\
	1 & 1 & 0 & 1 & 0 & 0 \\
	0 & 1 & 1 & 0 & 0 & 0 \\
	0 & 0 & 0 & 0 & 0 & 1 \\
	0 & 0 & 0 & 0 & 1 & 0 \\
	\end{array}
	\right)
	\]

    Cette matrice est implémentée par la liste de listes : 
    
    <center>`#!py [[0, 1, 1, 0, 0, 0], [1, 0, 1, 1, 0, 0], [1, 1, 0, 1, 0, 0], [0, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 0]]`</center>
	
	Liste d'adjacence sous forme de dictionnaire :   
	les clés du dictionnaire sont les numéros des sommets. Pour chaque clé, la valeur associée est la liste des successeurs du sommet représenté par la clé.
	
    <center>`#!py {0: [1, 2], 1: [0, 2, 3], 2: [0, 1, 3], 3: [1, 2], 4: [5], 5: [4]}`</center>
	
    Liste d'adjacence sous forme de liste :   
	les indices de la liste sont les numéros des sommets. Pour chaque indice, la valeur associée est la liste des successeurs du sommet représenté par l'indice.
	
    <center>`#!py [[1, 2], [0, 2, 3], [0, 1, 3], [1, 2], [5], [4]]`</center>

??? abstract "I) Passage matrice vers dictionnaire"

    Compléter la fonction `matrice_vers_dict` qui prend en paramètre une matrice d'adjacence et renvoie une liste d'adjacence sous forme de dictionnaire.

    ???+ example "Exemples"

        ```pycon
        >>> m = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
        >>> matrice_vers_dict(m)
        {0: [1, 2], 1: [0, 2, 3], 2: [0, 1, 3], 3: [1, 2]}
        ```
	
    === "Version vide"
        {{ IDE('exo_1_vide') }}
    === "Version à compléter"
        {{ IDE('exo1') }}

??? abstract "II) Passage matrice vers liste"

    En s'inspirant de la question précédente, compléter la fonction `matrice_vers_liste` qui prend en paramètre une matrice d'adjacence et renvoie une liste d'adjacence sous forme de liste.

    ???+ example "Exemples"

        ```pycon
        >>> m = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
        >>> matrice_vers_liste(m)
        [[1, 2], [0, 2, 3], [0, 1, 3], [1, 2]]
        ```

    {{ IDE('exo2') }}

??? abstract "III) Passage dictionnaire vers matrice"

    Compléter la fonction `dict_vers_matrice` qui prend en paramètre une liste d'adjacence sous forme de dictionnaire et renvoie la matrice d'adjacence.
	
    ???+ example "Exemples"

        ```pycon
        >>> dico = {0: [1, 2], 1: [0, 2, 3], 2: [0, 1, 3], 3: [1, 2]}
        >>> dict_vers_matrice(dico)
        [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
        ```

    ??? tip "Aide"
        La deuxième ligne du corps de la fonction, `matrice = ...`, sert à initialiser une matrice remplie de 0.

    {{ IDE('exo3') }}

??? abstract "IV) Passage liste vers matrice"

    En s'inspirant de la question précédente, compléter la fonction `liste_vers_matrice` qui prend en paramètre une liste d'adjacence sous forme de liste et renvoie la matrice d'adjacence.

    ???+ example "Exemples"

        ```pycon
        >>> liste = [[1, 2], [0, 2, 3], [0, 1, 3], [1, 2]]
        >>> liste_vers_matrice(liste)
        [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
        ```

    {{ IDE('exo4') }}


