

# --------- PYODIDE:code --------- #

def liste_vers_matrice(liste):
    n = len(liste)
    ...

# --------- PYODIDE:corr --------- #

def liste_vers_matrice(liste):
    n = len(liste)
    matrice = [n * [0] for i in range(n)]
    for sommet in range(n):
        for successeur in liste[sommet]:
            matrice[sommet][successeur] = 1
    return matrice

# --------- PYODIDE:tests --------- #

lst = [[1, 2], [0, 2, 3], [0, 1, 3], [1, 2]]
assert liste_vers_matrice(lst) == [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
lst = [[1], [0]]
assert liste_vers_matrice(lst) == [[0, 1], [1, 0]]
lst = [[], [0]]
assert liste_vers_matrice(lst) == [[0, 0], [1, 0]]
lst = [[], []]
assert liste_vers_matrice(lst) == [[0, 0], [0, 0]]
lst = [[]]
assert liste_vers_matrice(lst) == [[0]]
lst = [[0]]
assert liste_vers_matrice(lst) == [[1]]

# --------- PYODIDE:secrets --------- #


# tests secrets
lst = [[] for i in range(10**3)]
m = [[0] * 10**3 for i in range(10**3)]
assert liste_vers_matrice(lst) == m, "Erreur avec lst = [[] for i in range(10**3)]"
lst = [[k for k in range(10**3)] for i in range(10**3)]
m = [[1] * 10**3 for i in range(10**3)]
assert liste_vers_matrice(lst) == m, "Erreur avec lst = [[k for k in range(10**3)] for i in range(10**3)]"
lst = [[k for k in range(10**3)] if i%2 else [] for i in range(10**3)]
m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]
assert liste_vers_matrice(lst) == m, "Erreur avec lst = [[k for k in range(10**3)] if i%2 else [] for i in range(10**3)]"