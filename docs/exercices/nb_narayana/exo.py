

# --------- PYODIDE:code --------- #

factorial = [1]


def narayana(n, k):
    i = len(factorial)
    while n >= i:
        f_i = factorial[...] * ...
        factorial.append(...)
        i = ...
    return ...

# --------- PYODIDE:corr --------- #

factorial = [1]


def narayana(n, k):
    i = len(factorial)
    while n >= i:
        f_i = factorial[i - 1] * i
        factorial.append(f_i)
        i += 1
    return (
        (factorial[n] // factorial[k] // factorial[n - k])
        * (factorial[n] // factorial[k - 1] // factorial[n - k + 1])
        // n
    )

# --------- PYODIDE:tests --------- #

assert narayana(4, 3) == 6
assert narayana(5, 3) == 20

# --------- PYODIDE:secrets --------- #


# autres tests

NARAYANA = [
    [],
    [1],
    [1, 1],
    [1, 3, 1],
    [1, 6, 6, 1],
    [1, 10, 20, 10, 1],
    [1, 15, 50, 50, 15, 1],
    [1, 21, 105, 175, 105, 21, 1],
    [1, 28, 196, 490, 490, 196, 28, 1],
    [1, 36, 336, 1176, 1764, 1176, 336, 36, 1],
    [1, 45, 540, 2520, 5292, 5292, 2520, 540, 45, 1],
    [1, 55, 825, 4950, 13860, 19404, 13860, 4950, 825, 55, 1],
]

for n, ligne in enumerate(NARAYANA):
    for k in range(1, n):
        assert narayana(n, k) == NARAYANA[n][k - 1]