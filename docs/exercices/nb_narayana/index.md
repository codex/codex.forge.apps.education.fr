---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Nombres de Narayana
tags:
    - arbre
    - mémoïsation
    - en travaux
---


On admettra que le nombre d'arbres ordonnés à $n+1$ nœuds et $k$ feuilles est égal à $N(n, k)$, un nombre de Narayana dont on donnera la formule plus bas.

!!! info "Les arbres à 4+1 nœuds et 2 feuilles"

    Il y en a $N(4, 2) = 6$.

    ![](images/arbre_5_2_0.svg)
    ![](images/arbre_5_2_1.svg)
    ![](images/arbre_5_2_2.svg)
    ![](images/arbre_5_2_3.svg)
    ![](images/arbre_5_2_4.svg)
    ![](images/arbre_5_2_5.svg)

Écrire une fonction telle que `narayana(n, k)` renvoie le nombre d'arbres ayant `n+1` nœuds et `k` feuilles. On utilisera un dictionnaire pour mémoriser les résultats intermédiaires.

Formules :

$$N(n, k) = \frac1n \binom{n}{k} \binom{n}{k-1}$$

$$\binom{n}{k} = \frac{n!}{k!(n-k)!}$$

$$n! = 1×2×3×...×(n-1)×n\quad\text{, et } 0! = 1$$

On n'utilisera pas le module `math` pour cet exercice.

???+ example "Exemples"

    ```pycon title=""
    >>> narayana(4, 3)
    6
    >>> narayana(5, 3)
    20
    ```

{{ IDE('exo', SANS="math") }}

??? tip "Arbres à 5+1 nœuds et 3 feuilles"
    Il y en a $N(5, 3) = 20$

    ![](images/arbre_6_3_0.svg)
    ![](images/arbre_6_3_1.svg)
    ![](images/arbre_6_3_2.svg)
    ![](images/arbre_6_3_3.svg)
    ![](images/arbre_6_3_4.svg)
    ![](images/arbre_6_3_5.svg)
    ![](images/arbre_6_3_6.svg)
    ![](images/arbre_6_3_7.svg)
    ![](images/arbre_6_3_8.svg)
    ![](images/arbre_6_3_9.svg)
    ![](images/arbre_6_3_10.svg)
    ![](images/arbre_6_3_11.svg)
    ![](images/arbre_6_3_12.svg)
    ![](images/arbre_6_3_13.svg)
    ![](images/arbre_6_3_14.svg)
    ![](images/arbre_6_3_15.svg)
    ![](images/arbre_6_3_16.svg)
    ![](images/arbre_6_3_17.svg)
    ![](images/arbre_6_3_18.svg)
    ![](images/arbre_6_3_19.svg)
