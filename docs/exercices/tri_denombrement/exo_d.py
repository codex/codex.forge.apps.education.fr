# --------- PYODIDE:env --------- #
def denombrement(nombres, maxi):
    effectifs = [0] * (maxi + 1)
    for nb in nombres:
        effectifs[nb] += 1
    return effectifs

def maximum(nombres):
    maxi = 0
    for nb in nombres:
        if nb > maxi:
            maxi = nb
    return maxi

# --------- PYODIDE:code --------- #

def tri_denombrement_en_place(nombres):
    maxi = ...
    effectifs =   ...
    # On crée une variable stockant la position d'écriture
    indice_ecriture = 0
    for n in range(...):
        ...

# --------- PYODIDE:corr --------- #

def tri_denombrement_en_place(nombres):
    maxi = maximum(nombres)
    effectifs = denombrement(nombres, maxi)
    indice_ecriture = 0
    for n in range(maxi + 1):
        for _ in range(effectifs[n]):
            nombres[indice_ecriture] = n
            indice_ecriture += 1

# --------- PYODIDE:tests --------- #

nombres = [4, 5, 4, 2]
tri_denombrement_en_place(nombres)
assert nombres == [2, 4, 4, 5]
nombres = [3, 8, 7, 3, 5]
tri_denombrement_en_place(nombres)
assert nombres == [3, 3, 5, 7, 8]
nombres = [1, 2, 3, 4, 5]
tri_denombrement_en_place(nombres)
assert nombres == [1, 2, 3, 4, 5]
nombres = [5, 4, 3, 2, 1]
tri_denombrement_en_place(nombres)
assert nombres == [1, 2, 3, 4, 5]

# --------- PYODIDE:secrets --------- #

nombres = [4, 4, 4]
tri_denombrement_en_place(nombres)
assert nombres == [4, 4, 4]
nombres = list(range(100, -1, -1))
tri_denombrement_en_place(nombres)
assert nombres == list(range(101))
nombres = [1, 2]*10
tri_denombrement_en_place(nombres)
assert nombres == [1]*10+[2]*10
nombres = [10**3, 0]
tri_denombrement_en_place(nombres)
assert nombres == [0, 10**3]
