# --------- PYODIDE:env --------- #
def denombrement(nombres, maxi):
    pass

# --------- PYODIDE:code --------- #
def denombrement(nombres, maxi):
    effectifs = [0] * (maxi + 1)
    for nb in nombres:
        ...
    return effectifs

# --------- PYODIDE:corr --------- #

def denombrement(nombres, maxi):
    effectifs = [0] * (maxi + 1)
    for nb in nombres:
        effectifs[nb] += 1
    return effectifs


# --------- PYODIDE:tests --------- #

nombres = [4, 5, 4, 2]
assert denombrement(nombres, 5) == [0, 0, 1, 0, 2, 1]
nombres = [3, 3, 2, 0, 3, 0, 2, 0, 2, 3]
assert denombrement(nombres, 3) ==  [3, 0, 3, 4]

# --------- PYODIDE:secrets --------- #

nombres = [4, 4, 4]
assert denombrement(nombres, 4) == [0, 0, 0, 0, 3]
nombres = list(range(100, -1, -1))
assert denombrement(nombres, 100) == [1] * 101