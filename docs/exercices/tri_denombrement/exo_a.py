# --------- PYODIDE:env --------- #
def maximum(nombres):
    pass


# --------- PYODIDE:code --------- #
def maximum(nombres):
    ...


# --------- PYODIDE:corr --------- #
def maximum(nombres):
    maxi = 0
    for nb in nombres:
        if nb > maxi:
            maxi = nb
    return maxi


# --------- PYODIDE:tests --------- #
assert maximum([98, 12, 104, 23, 131, 9]) == 131
assert maximum([27, 24, 3, 15]) == 27

# --------- PYODIDE:secrets --------- #
assert maximum([1, 2, 3, 4, 5]) == 5
assert maximum([5, 4, 3, 2, 1]) == 5
assert maximum([5, 5, 5]) == 5
assert maximum([1, 2]) == 2
