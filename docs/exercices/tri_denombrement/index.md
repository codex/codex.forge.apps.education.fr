---
author: 
    - Nicolas Revéret
    - Mireille Coilhac
hide:
    - navigation
    - toc
title: Tri par dénombrement
tags:
    - à trous
    - tri
    - int
    - ep2
maj: 28/02/2025
difficulty: 250
---

On souhaite dans cet exercice trier des tableaux non vides de **nombres entiers positifs ou nuls**.

Pour ce faire on utilise l'algorithme du **tri par dénombrement**.

Les étapes sont les suivantes :

* déterminer la valeur maximale dans le tableau,

* regrouper dans un autre tableau le nombre d'apparitions de chaque valeur entre 0 et le maximum déterminé à l'étape précédente. Dans ce tableau d'effectifs, la valeur à l'indice `i` donnera le nombre d'apparitions de `i` dans le tableau initial,

* parcourir le tableau des effectifs et ajouter autant de fois que nécessaire chaque indice dans le tableau de nombres triés.


???+ example "Exemple"

    On souhaite trier le tableau `nombres = [3, 3, 2, 0, 3, 0, 2, 0, 2, 3]` :

    * La valeur maximale est `#!py 3`

    * Le tableau des effectifs vaut :

    ```python
    # indice     0  1  2  3
    effectifs = [3, 0, 3, 4]
    ```

    Le `#!py 3` à l'indice `#!py 2` signifie que le nombre `#!py 2` apparaît trois fois dans `nombres`.

    * On parcourt `effectifs` :
        * à l'indice `#!py 0` on trouve la valeur `#!py 3` : on insère trois fois `#!py 0` dans le tableau `nombres_tries`,
        * à l'indice `#!py 1` on trouve la valeur `#!py 0` : on n'effectue pas d'insertion,
        * à l'indice `#!py 2` on trouve la valeur `#!py 3` : on insère trois fois `#!py 2` dans `nombres_tries`,
        * à l'indice `#!py 3` on trouve la valeur `#!py 4` : on insère quatre fois `#!py 3` dans `nombres_tries`.

    * On obtient le tableau `[0, 0, 0, 2, 2, 2, 3, 3, 3, 3]`.

Vous devez écrire quatre fonctions. Chaque question est indépendante.

???+ warning "Contrainte pour toutes les questions"

    On interdit d'utiliser dans toutes les questions la méthode de tri `#!py sort` et la fonction de tri native `#!py sorted`.

??? question "1. La fonction `#!py maximum`"
    Écrire la fonction `#!py maximum` qui prend en paramètre un tableau de nombres `nombres` et renvoie la valeur maximale dans celui-ci.

    ???+ warning "Contrainte"

        On interdit d'utiliser les fonctions natives `#!py max` et `#!py min`.

    {{ IDE('exo_a', SANS = "max, min, sorted, .sort") }}

??? question "2. La fonction `#!py denombrement`"
    Compléter la fonction `#!py denombrement` qui prend en paramètres un tableau de nombres `nombres` et son `maximum` et renvoie un tableau d'entiers représentant les effectifs de chaque valeur.

    ???+ example "Exemples"

        ```pycon title=""
        >>> nombres = [3, 3, 2, 0, 3, 0, 2, 0, 2, 3]
        >>> denombrement(nombres, 3)
        [3, 0, 3, 4]
        ```

    {{ IDE('exo_b', SANS = "max, sorted, .sort") }}

??? question "3. La fonction `#!py tri_denombrement`"
    
    Compléter la fonction `tri_denombrement` qui prend en paramètre un tableau de nombres `nombres` et renvoie un tableau contenant les mêmes valeurs triées dans l'ordre croissant.

    Les fonctions `#!py maximum` et `#!py denombrement` sont déjà chargées en mémoire.

    ???+ example "Exemples"

        ```pycon title=""
        >>> nombres = [4, 5, 4, 2]
        >>> tri_denombrement(nombres)
        [2, 4, 4, 5]
        >>> nombres = [5, 4, 3, 2, 1]
        >>> tri_denombrement(nombres)
        [1, 2, 3, 4, 5]
        >>> nombres = []
        >>> tri_denombrement(nombres)
        []
        ```

    {{ IDE('exo_c', SANS = "max, sorted, .sort") }}

??? question "4. La fonction `#!py tri_denombrement_en_place`"

    Il n'est pas nécessaire de créer un autre tableau pour répondre à ce problème. On peut directement modifier le tableau passé en paramètre. Une version en place du tri va réécrire les valeurs triées directement dans le tableau de départ. Pour ce faire, il faut garder trace de l'indice où l'on doit écrire la prochaine valeur.

    Compléter la fonction `tri_denombrement_en_place` qui prend en paramètre un tableau de nombres `nombres` et modifie en place le tableau contenant les mêmes valeurs triées dans l'ordre croissant. Cette fonction[^1] ne renvoie rien.

    
    Les fonctions `#!py maximum` et `#!py denombrement` sont déjà chargées en mémoire.

    ???+ example "Exemples"

        ```pycon title=""
        >>> nombres = [4, 5, 4, 2]
        >>> tri_denombrement_en_place(nombres)
        >>> nombres
        [2, 4, 4, 5]
        >>> nombres = [5, 4, 3, 2, 1]
        >>> tri_denombrement(nombres)
        >>> nombres
        [1, 2, 3, 4, 5]
        >>> nombres = []
        >>> tri_denombrement(nombres)
        >>> nombres
        []
        ```

    {{ IDE('exo_d', SANS = "max, sorted, .sort") }}


[^1]: Une telle fonction qui ne renvoie rien est appelée **procédure**.
