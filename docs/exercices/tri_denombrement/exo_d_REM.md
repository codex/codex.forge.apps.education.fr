Il s'agit d'un tri original car de complexité linéaire si la valeur maximale est de l’ordre du nombre d’éléments (avec un tableau de $n$ éléments et une valeur maximale de $n^2$ la complexité serait quadratique). 

Avantageux sur ce point, il l'est moins sur les deux points suivants :

* cet algorithme ne s'applique qu'aux valeurs entières positives ou nulles,
* il nécessite de créer un tableau annexe contenant les effectifs ce qui peut consommer de la mémoire.