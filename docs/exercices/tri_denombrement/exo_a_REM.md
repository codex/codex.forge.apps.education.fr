La fonction `maximum` est classique : on initialise le maximum à `0` puis on lit l'ensemble des valeurs.

Attention toutefois : on peut initialiser à `0` car les nombres sont tous des entiers positifs ou nuls.

Dans tous les autres cas, on initialisera le maximum avec la première valeur du tableau.