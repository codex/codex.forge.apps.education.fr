# --------- PYODIDE:env --------- #
def denombrement(nombres, maxi):
    effectifs = [0] * (maxi + 1)
    for nb in nombres:
        effectifs[nb] += 1
    return effectifs

def maximum(nombres):
    maxi = 0
    for nb in nombres:
        if nb > maxi:
            maxi = nb
    return maxi

# --------- PYODIDE:code --------- #

def tri_denombrement(nombres):
    maxi = ...
    effectifs =   ...
    # On crée une nouvelle liste
    # pour stocker les nombres triés
    nombres_tries = []
    for n in range(...):
        ...
    return nombres_tries

# --------- PYODIDE:corr --------- #

def tri_denombrement(nombres):
    maxi = maximum(nombres)
    effectifs = denombrement(nombres, maxi)
    nombres_tries = []
    for n in range(maxi + 1):
        nombres_tries = nombres_tries + effectifs[n] * [n]
    return nombres_tries

# --------- PYODIDE:tests --------- #

nombres = [4, 5, 4, 2]
assert tri_denombrement(nombres) == [2, 4, 4, 5]
nombres = [3, 8, 7, 3, 5]
assert tri_denombrement(nombres) == [3, 3, 5, 7, 8]
nombres = [1, 2, 3, 4, 5]
assert tri_denombrement(nombres) == [1, 2, 3, 4, 5]
nombres = [5, 4, 3, 2, 1]
assert tri_denombrement(nombres) == [1, 2, 3, 4, 5]

# --------- PYODIDE:secrets --------- #

nombres = [4, 4, 4]
assert tri_denombrement(nombres) == [4, 4, 4]
nombres = list(range(100, -1, -1))
assert tri_denombrement(nombres) == list(range(101))
nombres = [1, 2]*10
assert tri_denombrement(nombres) == [1]*10+[2]*10
nombres = [10**3, 0]
assert tri_denombrement(nombres) == [0, 10**3]