# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas
import p5


app.centrer("figure3") 


# --------- PYODIDE:code --------- #
gauche(...)
haut(...)
droite(...)
bas(...)



# --------- PYODIDE:post --------- #
app.verifier_programme(['H8', 'H5', 'D5', 'D10', 'F10'])
