---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Alien (1) - Appels de fonctions
tags:
    - SNT
difficulty : 50
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :

    
    * « {{ lien_exo("Alien (1) : Appels de fonctions", "alien_1") }} »,

    * « {{ lien_exo("Alien (2) : Variables et affectations", "alien2") }} »,
    
    * « {{ lien_exo("Alien (3) : Instructions conditionnelles", "alien3") }} »,
    
    * « {{ lien_exo("Alien (4) : Instructions conditionnelles", "alien4") }} »,
    
    * « {{ lien_exo("Alien (5) : Fonctions", "alien5") }} »,
    
    * « {{ lien_exo("Alien (6) : Tableaux", "alien6") }} ».

Les règles sont simples : l'alien :alien: se situe au départ au centre de la grille et peut être déplacé avec les fonctions `haut`, `bas`, `gauche` et `droite`.

L'objectif est de trouver la case finale de l'alien (et donc son parcours) après exécution du programme donné.

???+ example "Les différentes instructions"

    * `haut(4)` déplace l'alien de `#!py 4` cases vers le haut ;
    * `haut(-2)` déplace l'alien de `#!py 2` cases vers le bas ;
    * `haut()` déplace l'alien de `#!py 1` case vers le haut (c'est donc équivalent à `#!py haut(1)`).

    Le principe est similaire avec les trois autres fonctions.

Pour les deux questions suivantes, dessinez le parcours de l'alien en cliquant sur **la case d'arrivée** de chaque instruction exécutée. Vous pourrez ensuite valider votre parcours pour vérifier s'il est correct.

??? question "Question 1"
    ```python { .inline .end .w45 }
    gauche(3)
    haut(2)
    droite(7)
    ```
    {{ run('exo_1') }}
    {{ figure("figure1") }}

??? question "Question 2"
    ```python { .inline .end .w45 }
    droite(6)
    bas(5)
    gauche(4)
    haut(3)
    droite(2)
    bas(1)
    ```
    {{ run('exo_2') }}
    {{ figure("figure2") }}

Pour les questions suivantes, écrire le code nécessaire pour obtenir le déplacement souhaité (les numéros correspondent aux différentes étapes) :

??? question "Question 3"
    ![alien](alien1.png)

    {{ IDE('exo_3') }} 
    {{ figure("figure3",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 4"
    ![alien](alien2.png)

    {{ IDE('exo_4') }}
    {{ figure("figure4",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}
