Notons que dans les cas où `tableau` a strictement moins de 2 éléments `#!py range(1, len(tableau))` ne fait aucun tour de boucle.

# Version fonctionnelle hors programme de NSI

On peut coder cette phrase logique :

> Le tableau est trié s'il a moins de deux éléments ou alors si chacun des éléments à partir du deuxième est supérieur ou égal 
à son prédécesseur.

L'appel `all(booleens)` est évalué à `#!py True` si tous les éléments de `booleens` sont égaux à `#!py True`, sinon à `#!py False`.

Si l'objet itérable est vide, l'appel `all(booleens)` renvoie également `#!py True`.

```python
def est_trie(tableau):
    return all(tableau[i] >= tableau[i - 1] for i in range(1, len(tableau)))
```

Rappelons que dans les cas où `tableau` a strictement moins de deux éléments `#!py range(1, len(tableau))` ne fait aucun tour de boucle 
et donc `#!py all(...)` renvoie `True`.

