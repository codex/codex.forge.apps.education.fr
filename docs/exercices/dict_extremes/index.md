---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
title: Dictionnaire de valeurs extrêmes
tags:
    - dictionnaire
    - ep1
difficulty: 201
---

Écrire une fonction `extremes` qui prend en paramètre un tableau `valeurs` de nombres _a priori_ non triés, et qui renvoie la plus petite ainsi que la plus grande valeur du tableau sous la forme d'un dictionnaire à deux clés `'min'` et `'max'`.

Si le tableau est vide, les extrêmes n'existent pas ; on utilisera alors `None` pour chacun.

???+ warning "Contrainte"

    On n'utilisera pas les fonctions `#!py min`, `#!py max`, `#!py sort`, `#!py sorted` fournies par le langage.

???+ example "Exemples"

    ```pycon title=""
    >>> valeurs = [0, 1, 4, 2, -2, 9, 3, 1, 7, 1]
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': -2, 'max': 9}
    >>> valeurs = [37, 37]
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': 37, 'max': 37}
    >>> valeurs = []
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': None, 'max': None}
    ```


{{ IDE('exo', SANS="min, max, .sort, sorted") }}
