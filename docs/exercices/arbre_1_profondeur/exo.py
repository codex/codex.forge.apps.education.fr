

# --------- PYODIDE:code --------- #

def profondeur(arbre, n):
    ...

# --------- PYODIDE:corr --------- #

def profondeur(arbre, n):
    if n == arbre[n]:
        return 0
    else:
        return 1 + profondeur(arbre, arbre[n])

# --------- PYODIDE:tests --------- #

arbre = [1, 2, 2, 2]
assert profondeur(arbre, 0) == 2
assert profondeur(arbre, 1) == 1
assert profondeur(arbre, 2) == 0
arbre = [0, 0, 0, 0, 1, 3, 3, 3]
assert profondeur(arbre, 6) == 2

# --------- PYODIDE:secrets --------- #

# tests
arbre = [1, 2, 2, 2]
assert profondeur(arbre, 0) == 2
assert profondeur(arbre, 1) == 1
assert profondeur(arbre, 2) == 0
assert profondeur(arbre, 3) == 1
arbre = [0, 0, 0, 0, 1, 3, 3, 3]
assert profondeur(arbre, 6) == 2
# tests secrets
arbre = [4, 0, 6, 1, 4, 4, 5, 5]
assert profondeur(arbre, 0) == 1
assert profondeur(arbre, 1) == 2
assert profondeur(arbre, 2) == 3
assert profondeur(arbre, 3) == 3
assert profondeur(arbre, 4) == 0
assert profondeur(arbre, 5) == 1
assert profondeur(arbre, 6) == 2
assert profondeur(arbre, 7) == 2
