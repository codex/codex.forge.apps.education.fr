L'implémentation des arbres choisie dans cet exercice permet d'écrire une solution sous la forme d'une fonction itérative simple:

```pycon
def profondeur(arbre, n):
    longueur = 0
    while arbre[n] != 0:  # arbre[n] est le numéro du parent de n
        n = arbre[n]
        longueur = longueur + 1
    return longueur
```

Pour la construction d'un arbre à partir de deux arbres, voir l'exercice [arbre 2 construction](../../exercices/arbre_2_construction/index.md)

Note: la plus grande des profondeurs est la *hauteur* de l'arbre. Voir l'exercice [arbre 3 hauteur](../../en_travaux/arbre_3_hauteur/index.md)
