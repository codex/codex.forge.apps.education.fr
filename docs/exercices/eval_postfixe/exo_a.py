

# --------- PYODIDE:env --------- #

class Pile:
    """Classe définissant une structure de pile"""

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def empile(self, x):
        """Place x au sommet de la pile"""
        self.contenu.append(x)

    def depile(self):
        """Retire et renvoie l'élément placé au sommet de la pile.
        Provoque une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError('La pile est vide')
        return self.contenu.pop()

# --------- PYODIDE:code --------- #

def evaluation_postfixe(expression):
    ...

# --------- PYODIDE:corr --------- #

def evaluation_postfixe(expression):
    pile = Pile()
    for element in expression:
        if element != "+" and element != "*":
            pile.empile(element)
        else:
            if element == "+":
                resultat = pile.depile() + pile.depile()
            else:
                resultat = pile.depile() * pile.depile()
            pile.empile(resultat)
    return pile.depile()

# --------- PYODIDE:tests --------- #

assert evaluation_postfixe([3, 2, "*", 5, "+"]) == 11
assert evaluation_postfixe([2, 3, "+", 5, "*"]) == 25
assert evaluation_postfixe([2]) == 2
assert evaluation_postfixe([2, 3, 4, "*", "*"]) == 24

# --------- PYODIDE:secrets --------- #


# tests secrets
expression = [3, 2, 5, "+", "+"]
attendu = 10
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"
expression = [100, 3, "*", 5, "*"]
attendu = 1500
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"
expression = [0]
attendu = 0
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"
expression = list(range(1, 11)) + ["+"] * 9
attendu = 55
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"
expression = list(range(1, 11)) + ["*"] * 9
attendu = 3_628_800
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"
from random import choice, randrange

# test aléatoire
operateurs = "+*"
dico = {"*": lambda a, b: a * b, "+": lambda a, b: a + b}
expression = [randrange(-10, 10), randrange(-10, 10)]
pile = expression[:]
for _ in range(5):
    op = choice(operateurs)
    n = randrange(-10, 10)
    pile = [dico[op](pile.pop(), pile.pop())] + [n]
    expression += [op, n]
op = choice(operateurs)
attendu = dico[op](pile.pop(), pile.pop())
expression += [op]
assert evaluation_postfixe(expression) == attendu, f"Erreur avec {expression = }"