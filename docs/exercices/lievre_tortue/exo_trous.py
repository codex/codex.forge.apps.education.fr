# --------- PYODIDE:code --------- #


class Cellule:
    def __init__(self):
        self.suivante = None


def contient_boucle(tete):
    tortue = tete
    lievre = ...
    while lievre is not ... and lievre.suivante is not ...:
        tortue = ...
        lievre = ...
        if lievre == ...:
            return ...
    return ...


# --------- PYODIDE:corr --------- #


def contient_boucle(tete):
    tortue = tete
    lievre = tete
    while lievre is not None and lievre.suivante is not None:
        tortue = tortue.suivante
        lievre = lievre.suivante.suivante
        if lievre == tortue:
            return True
    return False


# --------- PYODIDE:tests --------- #

# liste test_A : 18 cellules avec boucle, celle de l'énoncé
liste_test_A = Cellule()

actuelle = liste_test_A
for i in range(1, 18):
    actuelle.suivante = Cellule()
    actuelle = actuelle.suivante
    if i == 13:
        debut_boucle = actuelle

actuelle.suivante = debut_boucle  # la suivante de la n°17 devient la n°13

assert contient_boucle(liste_test_A) is True

# liste liste_test_B : 8 cellules, sans boucle
liste_test_B = Cellule()

actuelle = liste_test_B
for i in range(1, 8):
    actuelle.suivante = Cellule()
    actuelle = actuelle.suivante

assert contient_boucle(liste_test_B) is False

# --------- PYODIDE:secrets --------- #


# autres tests

# liste test_4 : une boucle globale
test_4 = Cellule()

pos = test_4
for i in range(1, 8):
    pos.suivante = Cellule()
    pos = pos.suivante

pos.suivante = test_4

# liste test5 : 2 cellules en boucle
test_5 = Cellule()
test_5.suivante = Cellule()
test_5.suivante.suivante = test_5


# liste test_6 : 7 cellules sans boucle
prec = test_6 = Cellule()
for i in range(1, 7):
    prec.suivante = Cellule()
    prec = prec.suivante


assert contient_boucle(test_4) is True
assert contient_boucle(test_5) is True
assert contient_boucle(test_6) is False
