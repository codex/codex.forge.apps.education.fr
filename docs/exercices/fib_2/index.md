---
license: "by-nc-sa"
author: 
    - Franck Chambon
    - Serge Bays
hide:
    - navigation
    - toc
title: Suite de Fibonacci (2)
tags:
    - maths
    - récursivité
difficulty: 350
maj: 01/03/2024
---



Les premiers termes de la suite de Fibonacci sont :

$$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, \cdots$$

- Les deux premiers termes sont $0$ et $1$.
- À partir du troisième, un terme est la somme des deux précédents.

Il existe une formule récursive efficace pour calculer **deux nombres de Fibonacci consécutifs**.

$F_0=0$, $F_1=1$ et pour $k>0$, on admet que :

$$\begin{cases}
F_{2k-1} = F_{k}^{2} + F_{k-1}^{2}\\
F_{2k} = F_{k}^{2} + 2×F_{k}×F_{k-1}\\
F_{2k+1} = F_{2k} + F_{2k-1}
\end{cases}$$

Écrire une fonction récursive `fibonacci` qui prend en paramètre un entier `n` strictement positif et renvoie le couple $(F_{n-1}, F_n)$

!!! abstract "Méthode appliquée à `n = 9`"
    - L'appel `#!py fibonacci(9)` souhaite renvoyer $(F_8, F_9)$
    - $9$ est impair, on va demander $(F_7, F_8)$, on pourra en déduire $F_9$
    - On appelle `#!py fibonacci(8)`
    - $8$ est pair, on va demander $(F_3, F_4)$
    - On appelle `#!py fibonacci(4)`
    - $4$ est pair, on va demander $(F_1, F_2)$
    - On appelle `#!py fibonacci(2)`
    - $2$ est pair, on va demander $(F_0, F_1)$
    - On appelle `#!py fibonacci(1)`
    - $1$ est un cas de base, `#!py fibonacci(1)` renvoie `(0, 1)`
    - On déduit
        - $F_1 = F_1^2 + F_0^2 = 1$
        - $F_2 = F_1^2 + 2×F_1×F_0 = 1$
        - l'appel `#!py fibonacci(2)` renvoie `(1, 1)`
    - On déduit
        - $F_3 = F_2^2 + F_1^2 = 2$
        - $F_4 = F_2^2 + 2×F_2×F_1 = 3$
        - l'appel `#!py fibonacci(4)` renvoie `(2, 3)`
    - On déduit
        - $F_7 = F_4^2 + F_3^2 = 13$
        - $F_8 = F_4^2 + 2×F_4×F_3 = 21$
        - l'appel `#!py fibonacci(8)` renvoie `(13, 21)`
    - On déduit
        - $F_9 = F_8 + F_7 = 34$
        - l'appel `#!py fibonacci(9)` renvoie `(21, 34)`

??? tip "Indice : Algorithme"
    - Si `n` est le cas de base, on renvoie la réponse directement.
    - Sinon,
        - on fait un appel récursif avec `n//2` pour obtenir le couple $(F_{k-1}, F_k)$, (`k` vaut `n//2`)
        - on calcule $F_{2k-1}$ et $F_{2k}$
        - si `n` est pair, (`n` vaut `2 * k`), alors on renvoie le couple $(F_{2k-1}, F_{2k})$
        - sinon, `n` est impair, (`n` vaut `2*k + 1`, $F_{2k+1}=F_{2k-1}+F_{2k}$), et alors on renvoie le couple $(F_{2k}, F_{2k+1})$


???+ example "Exemples"

    ```pycon title=""
    >>> fibonacci(1)
    (0, 1)
    >>> fibonacci(2)
    (1, 1)
    >>> fibonacci(3)
    (1, 2)
    >>> fibonacci(9)
    (21, 34)
    >>> fibonacci(4)
    (2, 3)
    ```

{{ IDE('exo') }}

