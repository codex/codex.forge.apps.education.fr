

# --------- PYODIDE:code --------- #

def evenements(trains):
    les_evenements = []
    for (arrivee, depart) in ...:
        les_evenements.append((..., ...))
        les_evenements.append((..., ...))
    return les_evenements


def nb_maxi_quais(trains):
    les_evenements = evenements(trains)
    les_evenements.sort()

    nb_quais_occupes = 0
    maxi = 0
    for (horaire, variation) in ...:
        ...

    return ...

# --------- PYODIDE:corr --------- #

def evenements(trains):
    evenements = []
    for (arrivee, depart) in trains:
        evenements.append((arrivee, +1))
        evenements.append((depart, -1))
    return evenements


def nb_maxi_quais(trains):
    les_evenements = evenements(trains)
    les_evenements.sort()

    nb_quais_occupes = 0
    maxi = 0
    for (horaire, variation) in les_evenements:
        nb_quais_occupes += variation
        if nb_quais_occupes > maxi:
            maxi = nb_quais_occupes
    return maxi

# --------- PYODIDE:tests --------- #

trains = [(3, 5), (2, 4), (6, 8)]
assert sorted(evenements(trains)) == [(2, 1), (3, 1), (4, -1), (5, -1), (6, 1), (8, -1)]
assert nb_maxi_quais(trains) == 2

trains = [(1, 3), (6, 7), (5, 6), (3, 5)]
assert sorted(evenements(trains)) == [
    (1, 1),
    (3, -1),
    (3, 1),
    (5, -1),
    (5, 1),
    (6, -1),
    (6, 1),
    (7, -1),
]
assert nb_maxi_quais(trains) == 1

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
assert evenements([]) == [], "Erreur avec une liste vide"
assert nb_maxi_quais([]) == 0, "Erreur avec une liste vide"

trains = [(k, k + 1) for k in range(50)]
assert nb_maxi_quais(trains) == 1, f"Erreur avec {trains = }"

trains = [(k, k + 1) for k in range(20, 0, -1)]
assert nb_maxi_quais(trains) == 1, f"Erreur avec {trains = }"

trains = [(k, k + 2) for k in range(50)]
assert nb_maxi_quais(trains) == 2, f"Erreur avec {trains = }"

trains = [(0, 20 - k) for k in range(10)]
assert nb_maxi_quais(trains) == 10, f"Erreur avec {trains = }"

from functools import reduce
from random import randrange


def nb_maxi_quais_corr(trains):
    les_evenements = reduce(
        lambda evts, train: evts + [(train[0], 1), (train[1], -1)], trains, []
    )
    les_evenements.sort()
    quais_occupes = 0
    maxi = 0
    for (_, variation) in les_evenements:
        quais_occupes += variation
        if quais_occupes > maxi:
            maxi = quais_occupes
    return les_evenements, maxi


for test in range(20):
    nb_trains = randrange(20, 50)
    trains = []
    for un_train in range(nb_trains):
        arrivee = randrange(0, 2 * nb_trains)
        depart = arrivee + randrange(1, 10)
        trains.append((arrivee, depart))
    evts_attendus, quais_attendus = nb_maxi_quais_corr(trains)
    assert (
        sorted(evenements(trains)) == evts_attendus
    ), f"Erreur avec les trains {trains}"
    assert nb_maxi_quais(trains) == quais_attendus, f"Erreur avec les trains {trains}"