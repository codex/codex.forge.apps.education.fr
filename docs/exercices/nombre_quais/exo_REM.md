Ce problème fait partie de la grande famille des problèmes d'optimisation de tâches. On peut utiliser un algorithme équivalent pour déterminer par exemple le nombre de collaborateurs devant travailler en même temps sur diverses tâches partagées.

Une fois la liste des évènements créée, on la trie.

Le choix des valeurs `+1` pour les arrivées et `-1` pour les départs permet d'assurer qu'en cas d'horaires identiques, les évènements de départs seront envisagés avant ceux d'arrivée. On libère ainsi les quais en priorité.

Une fois ce tri effectué, on parcourt les évènements dans l'ordre chronologique :

* pour chaque évènement, on modifie la valeur du nombre de quais utilisés à l'aide de la `variation` associée à l'évènement,

* on vérifie ensuite que l'on n'a pas dépassé le nombre maximal de quais occupés. Si c'est le cas on met à jour la valeur `maxi`.

À la fin du parcours, on renvoie le nombre de quais maximal.
