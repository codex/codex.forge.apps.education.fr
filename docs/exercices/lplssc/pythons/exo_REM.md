Dans la fonction principale on commence par créer le dictionnaire `longueurs` initialement vide qui permettra de *mémoïser*. On lance ensuite les appels récursifs en se demandant quelle est la longueur de la plus longue sous-suite commune si l'on considère
toutes les lettres de chaque texte : `lplssc_rec(len(texte_a), len(texte_b))`.

Comme la sous-fonction est récursive, il est bon de débuter par un cas de base. C'est ici le cas ou l'un des deux textes ne contient aucun caractère. Dans ce cas, on renvoie directement 0.

On vérifie ensuite que le cas a été traité ou non. Pour cela on se demande si la clé `(taille_a, taille_b)` n'est pas présente dans le dictionnaire `longueurs`.

Si c'est le cas, il faut traiter cette comparaison :

* si les derniers caractères considérés sont identiques, on passe aux caractères précédents et l'on ajoute `#!py 1` au résultat. Cette valeur est immédiatement stockée dans le dictionnaire `longueurs`,
* si les caractères diffèrent, on étudie les deux cas précisés par l'énoncé. On place dans le dictionnaire le résultat le plus grand.

En dernier lieu, on renvoie la valeur que l'on vient d'insérer dans le dictionnaire.