Cette fonction est itérative et construite autour d'une boucle `#!py for`.

Le tableau stockant les longueurs des différentes sous-suites communes devrait avoir `#!py 1 + len(texte_a)` colonnes et `#!py 1 + len(texte_b)` lignes.
On constate néanmoins que les formules de récurrence ne font appels qu'à des valeurs issues de la même ligne ou de la précédente.

On peut donc se contenter d'utiliser deux tableaux de `#!py 1` ligne.

La première ligne du tableau et la première colonne ne contiennent que des `#!py 0`.

Pour compléter les autres cases, on utilise les formules de récurrence.
