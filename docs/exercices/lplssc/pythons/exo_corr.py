def lplssc(texte_a, texte_b):
    def lplssc_rec(i_a, i_b):
        if i_a == -1 or i_b == -1:
            return 0
        if (i_a, i_b) not in memoire:
            if texte_a[i_a] == texte_b[i_b]:
                memoire[(i_a, i_b)] = 1 + lplssc_rec(i_a - 1, i_b - 1)
            else:
                memoire[(i_a, i_b)] = max(
                    lplssc_rec(i_a - 1, i_b), lplssc_rec(i_a, i_b - 1)
                )
        return memoire[(i_a, i_b)]

    i_a = len(texte_a) - 1
    i_b = len(texte_b) - 1
    memoire = {}
    return lplssc_rec(i_a, i_b)
