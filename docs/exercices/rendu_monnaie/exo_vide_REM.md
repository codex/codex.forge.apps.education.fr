La version présentée dans la correction est celle avec concaténation.
Le coût de est quadratique, à cause du coût linéaire de la concaténation.
Il faut en effet, lors de chaque appel récursif, recopier l'ensemble des valeurs des pièces déjà rendues.

Il vaut mieux essayer de faire la version avec `#!py append` pour obtenir un coût linéaire.
