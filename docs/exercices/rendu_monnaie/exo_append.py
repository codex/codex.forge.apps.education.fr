# --------- PYODIDE:code --------- #

PIECES = [100, 50, 20, 10, 5, 2, 1]


def rendu_monnaie_rec(a_rendre, i, deja_rendu):
    if a_rendre == 0:
        return ...
    p = PIECES[i]
    if p <= ...:
        deja_rendu.append(...)
        return rendu_monnaie_rec(..., i, deja_rendu)
    else:
        return rendu_monnaie_rec(..., ..., ...)


def rendu_monnaie(a_rendre):
    return rendu_monnaie_rec(a_rendre, 0, [])


# --------- PYODIDE:corr --------- #

PIECES = [100, 50, 20, 10, 5, 2, 1]


def rendu_monnaie_rec(a_rendre, i, deja_rendu):
    if a_rendre == 0:
        return deja_rendu
    p = PIECES[i]
    if p <= a_rendre:
        deja_rendu.append(p)
        return rendu_monnaie_rec(a_rendre - p, i, deja_rendu)
    else:
        return rendu_monnaie_rec(a_rendre, i + 1, deja_rendu)


def rendu_monnaie(a_rendre):
    return rendu_monnaie_rec(a_rendre, 0, [])


# --------- PYODIDE:tests --------- #

assert rendu_monnaie(68) == [50, 10, 5, 2, 1]
assert rendu_monnaie(291) == [100, 100, 50, 20, 20, 1]

# --------- PYODIDE:secrets --------- #

x = 68
attendu = [50, 10, 5, 2, 1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 291
attendu = [100, 100, 50, 20, 20, 1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 0
attendu = []
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 1
attendu = [1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 2
attendu = [2]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 4
attendu = [2, 2]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 500
attendu = [100, 100, 100, 100, 100]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = sum(PIECES)
attendu = PIECES[:]
assert rendu_monnaie(x) == attendu, "Problème si on doit rendre toutes les pièces"
