# --------- PYODIDE:code --------- #

PIECES = [100, 50, 20, 10, 5, 2, 1]


# si besoin, rajouter un paramètre pour faire la version avec append
def rendu_monnaie_rec(a_rendre, i):
    ...


def rendu_monnaie(a_rendre):
    return rendu_monnaie_rec(a_rendre, 0)  # rajouter un paramètre si nécessaire


# --------- PYODIDE:corr --------- #

PIECES = [100, 50, 20, 10, 5, 2, 1]


# Version avec concaténation
def rendu_monnaie_rec(a_rendre, i):
    if a_rendre == 0:
        return []
    p = PIECES[i]
    if p <= a_rendre:
        return [p] + rendu_monnaie_rec(a_rendre - p, i)
    else:
        return rendu_monnaie_rec(a_rendre, i + 1)


def rendu_monnaie(a_rendre):
    return rendu_monnaie_rec(a_rendre, 0)


# --------- PYODIDE:tests --------- #

assert rendu_monnaie(68) == [50, 10, 5, 2, 1]
assert rendu_monnaie(291) == [100, 100, 50, 20, 20, 1]

# --------- PYODIDE:secrets --------- #

x = 68
attendu = [50, 10, 5, 2, 1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 291
attendu = [100, 100, 50, 20, 20, 1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 0
attendu = []
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 1
attendu = [1]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 2
attendu = [2]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 4
attendu = [2, 2]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = 500
attendu = [100, 100, 100, 100, 100]
assert rendu_monnaie(x) == attendu, f"Erreur pour le rendu de {x} €"
x = sum(PIECES)
attendu = PIECES[:]
assert rendu_monnaie(x) == attendu, "Problème si on doit rendre toutes les pièces"
