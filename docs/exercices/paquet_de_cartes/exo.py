

# --------- PYODIDE:code --------- #

class Carte:
    def __init__(self, h, c):
        """
        Initialise les attributs hauteur et couleur,
        une hauteur a une valeur allant de 1 à 13,
        une couleur a une valeur allant de 1 a 4.
         """
        self.hauteur = h
        self.couleur = c

    def hauteur_carte(self):
        """ Renvoie la hauteur de la carte """
        hauteurs = {1: 'As', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7',
                    8: '8', 9: '9', 10: '10', 11: 'Valet', 12: 'Dame', 13: 'Roi'}
        return hauteurs[self.hauteur]

    def couleur_carte(self):
        """ Renvoie la couleur de la carte """
        couleurs = {1: 'pique', 2: 'coeur', 3: 'carreau', 4: 'trèfle'}
        return couleurs[self.couleur]

class Paquet:
    def __init__(self):
        """ Initialise l’attribut contenu avec la liste des 52 objets Carte possibles
        rangés en suivant l’ordre des couleurs, pique, cœur, carreau,trèfle
        et pour chaque couleur par valeurs croissantes des hauteurs. """
        ...

    def carte_pos(self, pos):
        """ Renvoie la carte qui se trouve à la position pos dans le paquet
        la paramètre pos a une valeur comprise entre 0 et 51. """
        assert ...
        ...

# --------- PYODIDE:corr --------- #

class Carte:
    def __init__(self, h, c):
        """
        Initialise les attributs hauteur et couleur,
        une hauteur a une valeur allant de 1 à 13,
        une couleur a une valeur allant de 1 a 4.
         """
        self.hauteur = h
        self.couleur = c

    def hauteur_carte(self):
        """ Renvoie la hauteur de la carte """
        hauteurs = {1: 'As', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7',
                    8: '8', 9: '9', 10: '10', 11: 'Valet', 12: 'Dame', 13: 'Roi'}
        return hauteurs[self.hauteur]

    def couleur_carte(self):
        """ Renvoie la couleur de la carte """
        couleurs = {1: 'pique', 2: 'coeur', 3: 'carreau', 4: 'trèfle'}
        return couleurs[self.couleur]

class Paquet:
    def __init__(self):
        """ Initialise l’attribut contenu avec la liste des 52 objets Carte possibles
        rangés en suivant l’ordre des couleurs, pique, cœur, carreau,trèfle
        et pour chaque couleur par valeurs croissantes des hauteurs. """
        self.contenu = []
        for c in range(1, 5):
            for h in range(1, 14):
                self.contenu.append(Carte(h, c))


    def carte_pos(self, pos):
        """ Renvoie la carte qui se trouve à la position pos dans le paquet
        la paramètre pos a une valeur comprise entre 0 et 51. """
        assert 0 <= pos <= 51, "La valeur de pos n'est pas valide"
        return self.contenu[pos]

# --------- PYODIDE:tests --------- #

jeu = Paquet()
c1 = jeu.carte_pos(0)
assert c1.couleur_carte() == 'pique'
assert c1.hauteur_carte() == 'As'
c2 = jeu.carte_pos(1)
assert c2.couleur_carte() == 'pique'
assert c2.hauteur_carte() == '2'
c3 = jeu.carte_pos(2)
assert c3.couleur_carte() == 'pique'
assert c3.hauteur_carte() == '3'
c4 = jeu.carte_pos(13)
assert c4.couleur_carte() == 'coeur'
assert c4.hauteur_carte() == 'As'
c5 = jeu.carte_pos(51)
assert c5.couleur_carte() == 'trèfle'
assert c5.hauteur_carte() == 'Roi'

# --------- PYODIDE:secrets --------- #


# tests secrets
jeu = Paquet()
assert len(jeu.contenu) == 52, "Le paquet ne contient pas 52 cartes"
i = 0
for couleur in ['pique', 'coeur', 'carreau', 'trèfle']:
    j = 0
    for hauteur in ['As', '2', '3', '4', '5', '6', '7',
                 '8', '9', '10', 'Valet', 'Dame', 'Roi']:
        carte = jeu.carte_pos(i*13+j)
        assert carte.couleur_carte() == couleur, "erreur carte " + str(i*4+j)
        assert carte.hauteur_carte() == hauteur, "erreur carte " + str(i*4+j)
        j += 1
    i += 1