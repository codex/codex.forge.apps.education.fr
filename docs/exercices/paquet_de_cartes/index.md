---
author: Serge Bays
hide:
    - navigation
    - toc
title: Paquet de cartes
tags:
    - à trous
    - dictionnaire
    - programmation orientée objet
    - ep2
difficulty: 200
maj: 01/03/2024
---




Dans un paquet de 52 cartes à jouer, chaque carte a une hauteur et une couleur. Les différentes hauteurs sont dans l'ordre As, 2, 3, 4, ..., 9, 10, Valet, Dame, Roi
et les différentes couleurs sont dans l'ordre pique, carreau, cœur, trèfle.


On modélise un paquet de carte, en *programmation orientée objet*, par deux classes: une classe `Carte` et une classe `Paquet`.

Compléter le code de la classe `Paquet` en suivant pour chaque méthode la spécification donnée.

!!! exemple "Exemple d'utilisation"
	```pycon
	>>> jeu = Paquet()
	>>> c1 = jeu.carte_pos(0)
	>>> c1.couleur_carte()
	'pique'
	>>> c1.hauteur_carte()
	'As'
	>>> c2 = jeu.carte_pos(1)
	>>> c2.couleur_carte()
	'pique'
	>>> c2.hauteur_carte()
	'2'
	```
Si la valeur de la position passée en paramètre à la méthode `carte_pos` n'est pas valide, l'assertion renverra le message `"La valeur de pos n'est pas valide"`.


{{ IDE('exo') }}
