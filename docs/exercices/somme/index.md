---
license: "by-nc-sa"
author: Franck Chambon
hide:
    - navigation
    - toc
difficulty: 1
title: Somme de deux entiers
tags:
    - fonctions
---

Compléter la fonction ci-dessous pour qu'elle renvoie la somme des deux arguments `a` et `b`.

???+ example "Exemple"

    ```pycon title=""
    >>> somme(10, 32)
    42
    >>> somme(100, 7)
    107
    ```

{{ remarque('assertion') }}

{{ IDE('exo') }}
