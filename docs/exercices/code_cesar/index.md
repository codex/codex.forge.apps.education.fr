---
license: "by-nc-sa"
author:
    - Romain Janvier
    - Mireille Coilhac
    - Nicolas Revéret
    - Franck Chambon
hide:
    - navigation
    - toc
title: Chiffre de César
tags:
    - string
    - ep2
difficulty: 150
---

Le chiffrement de César transforme un message en changeant chaque lettre par une autre obtenue par décalage circulaire dans l'alphabet de la lettre d'origine. Par exemple, avec un décalage de 3, le `#!py 'A'` se transforme en `#!py 'D'`, le `#!py 'B'` en `#!py 'E'`, ..., le `#!py 'X'` en `#!py 'A'`, le `#!py 'Y'` en `#!py 'B'` et le `#!py 'Z'` en `#!py 'C'`.


```mermaid
flowchart TD
  subgraph "Décalage de 3"
    direction TB
    bef1[...]
    W1[W]
    X1[X]
    Y1[Y]
    Z1[Z]
    A1[A]
    B1[B]
    C1[C]
    D1[D]
    aft1[...]
  end
  subgraph " "
    bef2[...]
    Z2[Z]
    A2[A]
    B2[B]
    C2[C]
    D2[D]
    E2[E]
    F2[F]
    G2[G]
    aft2[...]
  end
  W1 --> Z2
  X1 --> A2
  Y1 --> B2
  Z1 --> C2
  A1 --> D2
  B1 --> E2
  C1 --> F2
  D1 --> G2

```


Les autres caractères (`#!py '!'`, `#!py '?'`...) ne sont pas transformés et sont simplement recopiés tels quels dans le message codé. 

Dans cet exercice, nous ne prendrons que des lettres majuscules.

On fournit les deux fonctions

- `indice` : renvoie l'indice dans l'alphabet d'une lettre majuscule en commençant à 0.
- `majuscule` : renvoie la lettre majuscule d'indice donné.


???+ example "Exemples"
    ```pycon
    >>> indice('C')
    2
    >>> majuscule(4)
    'E'
    ```

!!! tip "Remarque"
    Pour opérer un décalage circulaire d'un indice, on utilise l'opération modulo $26$ qui renvoie un résultat de $0$ inclus à $26$ exclu.

    Décalage de 8 pour la lettre `#!py 'Z'`
    ```pycon
    >>> indice('Z')
    25
    >>> 25 + 8
    33
    >>> 33 % 26
    7
    >>> majuscule(7)
    'H'
    ```
    Le chiffrement de `#!py 'Z'` sera ici `#!py 'H'`.

!!! abstract "Objectif"
    Écrire la fonction `cesar` qui prend en paramètres une chaine de caractères `message` et un nombre entier `decalage` et renvoie le nouveau message chiffré avec le chiffre de César utilisant ce `decalage`.

    On constate que pour déchiffrer un message, il suffit d'utiliser la clé opposée à celle du chiffrement.

???+ example "Exemples"
    ```pycon
    >>> cesar("HELLO WORLD!", 5) 
    'MJQQT BTWQI!'
    >>> cesar("MJQQT BTWQI!", -5) 
    'HELLO WORLD!'
    ```
    
{{ IDE('exo') }}

