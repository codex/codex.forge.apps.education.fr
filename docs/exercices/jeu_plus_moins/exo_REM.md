Cette fonction ne renvoie rien, en effet il n'y a pas de `return`.  
Cela revient au même que si on avait ajouté en dernière ligne : `return None`. On appelle parfois ces fonctions des procédures.
