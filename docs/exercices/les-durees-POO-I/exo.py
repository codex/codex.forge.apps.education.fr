# --------- PYODIDE:code --------- #


class Duree:
    def __init__(self, h, m, s):
        assert 0 <= m < 60 and 0 <= s < 60
        self.heures = h
        self.minutes = m
        self.secondes = s

    def en_secondes(self):
        return ...

    def ajoute_minutes(self, mins):
        ...

    def ajoute_secondes(self, secs):
        ...

    def __repr__(self):
        return f"{self.heures}:{self.minutes}:{self.secondes}"

# --------- PYODIDE:corr --------- #
class Duree:
    def __init__(self, h, m, s):
        assert 0 <= m < 60 and 0 <= s < 60
        self.heures = h
        self.minutes = m
        self.secondes = s

    def en_secondes(self):
        return self.heures * 3600 + self.minutes * 60 + self.secondes

    def ajoute_minutes(self, mins):
        self.heures += (self.minutes + mins) // 60
        self.minutes = (self.minutes + mins) % 60

    def ajoute_secondes(self, secs):
        self.ajoute_minutes((self.secondes + secs) // 60)
        self.secondes = (self.secondes + secs) % 60

    def __repr__(self):
        return f"{self.heures}:{self.minutes}:{self.secondes}"


# --------- PYODIDE:tests --------- #

duree_0 = Duree(0, 0, 0)
assert duree_0.en_secondes() == 0
duree_0.ajoute_minutes(12)
assert duree_0.en_secondes() == 720
duree_0.ajoute_secondes(3600)
assert duree_0.en_secondes() == 4320

duree_1 = Duree(2, 45, 52)
assert duree_1.en_secondes() == 9952
duree_1.ajoute_secondes(20)
assert duree_1.en_secondes() == 9972
duree_1.ajoute_minutes(30)
assert duree_1.en_secondes() == 11772


# --------- PYODIDE:secrets --------- #

duree_2 = Duree(5, 51, 4)
assert duree_2.en_secondes() == 21064, f"Erreur avec la conversion en secondes de {duree_2}"
duree_2.ajoute_secondes(120)
assert duree_2.en_secondes() == 21184, f"Erreur avec l'ajout de 120 secondes à {duree_2}"
duree_2.ajoute_minutes(5)
assert duree_2.en_secondes() == 21484, f"Erreur avec l'ajout de 5 minutess à {duree_2}"

duree_3 = Duree(1, 1, 1)
assert duree_3.en_secondes() == 3661, f"Erreur avec la conversion en secondes de {duree_3}"
duree_3.ajoute_secondes(3663)
assert duree_3.en_secondes() == 7324, f"Erreur avec l'ajout de 120 secondes à {duree_3}"
duree_3.ajoute_minutes(123)
assert duree_3.en_secondes() == 14704, f"Erreur avec l'ajout de 123 minutess à {duree_3}"