---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Les durées en POO (1)
tags:
    - à trous
    - programmation orientée objet
maj: 08/05/2024
difficulty: 250
---

On s'intéresse dans cet exercice à une classe `Duree` permettant de manipuler une durée exprimée en heures, minutes et secondes.

Un objet de type `Duree` est instancié en indiquant les heures, les minutes et les secondes nécessaires.

```pycon
>>> duree_1 = Duree(2, 45, 52)
>>> duree_1.heures
2
>>> duree_1.minutes
45
>>> duree_1.secondes
52
```

Les secondes et les minutes d'une durée doivent être strictement inférieures à `#!py 60`. Lors de l'instanciation les valeurs passées en paramètres doivent respecter cette règle.

La méthode `en_secondes` renvoie la durée correspondante exprimée en secondes. Cette méthode calcule et renvoie la durée exprimée en secondes mais ne modifie pas les valeurs des attributs `heures`, `minutes` et `secondes`.

Les méthodes `ajoute_minutes` et `ajoute_secondes` permettent d'ajouter respectivement un nombre de minutes ou de secondes positif ou nul passé en paramètre. Si, après l'addition, les minutes (ou les secondes) de l'objet dépassent `#!py 59`, on doit les convertir en heures (ou en minutes).

???+ example "Exemples"

    ```pycon title=""
    >>> duree_0 = Duree(0, 0, 0)
    >>> duree_0.en_secondes()
    0
    >>> duree_0.ajoute_minutes(12)
    >>> duree_0.en_secondes()
    720
    >>> duree_0.ajoute_secondes(3600)
    >>> duree_0.en_secondes()
    4320
    ```
    
    ```pycon title=""
    >>> duree_1 = Duree(2, 45, 52)
    >>> duree_1.en_secondes()
    9952
    >>> duree_1.ajoute_secondes(20)
    >>> duree_1.en_secondes()
    9972
    >>> duree_1.ajoute_minutes(30)
    >>> duree_1.en_secondes()
    11772
    ```

??? tip "Astuce"

    La méthode `ajoute_secondes` pourra utiliser la méthode `ajoute_minutes`

??? info "`__repr__` ?"

    La méthode `__repr__()` est une méthode native qui est appelée lorsque l'on souhaite afficher une *représentation* de l'objet (dans la console le plus souvent). Dans le cas présent, cette méthode est aussi appelée lorsque l'on souhaite  convertir un objet de ype `Duree` en une chaine de caractères (par exemple, lors de l'appel de la fonction `print`). Cette méthode renvoie une chaine de caractères sous la forme "hh&#58;mm:ss". Par exemple, pour un objet initialisé avec les valeurs 3 h, 20 min et 2 s, la méthode renvoie `#!py '3:20:2'`


Compléter le code ci-dessous. 

{{ IDE('exo') }}

??? note "Partie 2"

    Pour la manipulation avancée des durées, on peut faire {{ lien_exo("cet exercice", "les-durees-POO-II") }}.
