On est ici obligé de renseigner systématiquement les trois paramètres lors de l'instanciation de la classe.

Si on veut créer une durée uniquement en minutes/secondes ou en heure, il faut avoir un nombre de paramètres variable. On peut le faire en utilisant 3 paramètres et en fixant une **valeur par défaut**.

```python
class Duree:
    def __init__(self,h = 0, m = 0, s = 0):
        self.heures = h
        self.minutes = m
        self.secondes = s
```

Ainsi, on peut ensuite créer une instance avec trois arguments :

```pycon
>>> d = Duree(1, 25, 50)
>>> d
1h : 25m : 50s
```

ou avec seulement deux arguments :

```pycon
>>> d = Duree(2, 20)
>>> d
'2h : 20h : 0s'
```

Dans ce cas, le paramètre `s` prend la valeur par défaut `#!py 0`.

On peut enfin préciser les paramètres que l'on renseigne :

```pycon
>>> d = Duree(m = 40)
>>> d
'0 h: 40m : 0s'
```

Dans ce dernier cas, les paramètres `h` et `s` prennent la valeur par défaut `#!py 0`.

