On peut également utiliser une liste en compréhension :

```python
def indices(element, entiers):
    return [i for i in range(len(entiers)) if entiers[i] == element]
```
