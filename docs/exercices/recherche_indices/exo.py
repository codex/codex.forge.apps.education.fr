

# --------- PYODIDE:code --------- #

def indices(element, entiers):
    ...

# --------- PYODIDE:corr --------- #

def indices(element, entiers):
    positions = []
    for i in range(len(entiers)):
        if entiers[i] == element:
            positions.append(i)
    return positions

# --------- PYODIDE:tests --------- #

assert indices(3, [3, 2, 1, 3, 2, 1]) == [0, 3]
assert indices(4, [1, 2, 3]) == []
assert indices(1, [1, 1, 1, 1]) == [0, 1, 2, 3]
assert indices(5, [0, 0, 5]) == [2]

# --------- PYODIDE:secrets --------- #


# autres tests

assert indices(-1, [-1, 0, -1, 0, -1, 0, -1]) == [0, 2, 4, 6]
assert indices(10, [2, 10, 3, 10, 4, 10, 5]) == [1, 3, 5]