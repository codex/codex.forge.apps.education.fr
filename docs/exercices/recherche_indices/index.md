---
author: Sébastien Hoarau
hide:
    - navigation
    - toc
title: Recherche d'indices
difficulty: 119
tags:
    - liste/tableau
maj: 01/02/2024
---


Écrire une fonction `indices` qui prend en paramètres un entier `element` et un tableau `entiers` de nombres entiers et qui renvoie la liste croissante des indices de `element` dans le tableau `entiers`.

Cette liste sera donc vide `[]` si `element` n'apparait pas dans `entiers`.

???+ warning "Contraintes"

    On n'utilisera ni la méthode `#!py index`, ni la méthode `#!py max`.

???+ example "Exemples"

    ```pycon title=""
    >>> indices(3, [3, 2, 1, 3, 2, 1])
    [0, 3]
    >>> indices(4, [1, 2, 3])
    []
    >>> indices(10, [2, 10, 3, 10, 4, 10, 5])
    [1, 3, 5]
    ```

{{ IDE('exo', SANS=".index, max") }}

