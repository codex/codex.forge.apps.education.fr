# --------- PYODIDE:env --------- #


class Lampe:
    def __init__(self, nom, zone, pl):
        self.nom = nom
        self.zone = zone
        self.point_lumineux = pl
        self.etat_allume = False
        Lampe.adresse =adresse



# --------- PYODIDE:code --------- #
def adresse(self):
    ...



# --------- PYODIDE:corr --------- #
def adresse(self):
    return (self.zone, self.point_lumineux)





# --------- PYODIDE:tests --------- #
L1 = Lampe("L1", 0, 0)
L2 = Lampe("L2", 0, 1)

assert L1.adresse() == (0, 0)
assert L2.adresse() == (0, 1)


# --------- PYODIDE:secrets --------- #
L4 = Lampe("Lampe salon", 5, 5)
L3 = Lampe("L3", 1, 0)
assert L3.adresse() == (1, 0)
assert L4.adresse() == (5, 5)