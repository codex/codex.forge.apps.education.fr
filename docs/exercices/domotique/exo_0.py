
# --------- PYODIDE:code --------- #
class Lampe:
    def __init__(self, nom, zone, pl):
        self.nom = nom
        ...




# --------- PYODIDE:corr --------- #
class Lampe:
    def __init__(self, nom, zone, pl):
        self.nom = nom
        self.zone = zone
        self.point_lumineux = pl
        self.etat_allume = False



# --------- PYODIDE:tests --------- #
L1 = Lampe("L1", 0, 0)
L2 = Lampe("L2", 0, 1)
L3 = Lampe("L3", 1, 0)

assert L1.nom == "L1"
assert L2.point_lumineux == 1
assert L3.zone == 1
assert L3.etat_allume is False

# --------- PYODIDE:secrets --------- #
L4 = Lampe("Lampe salon", 5, 5)
assert L4.nom == "Lampe salon", "Erreur sur l'attribut nom"
assert L4.zone == 5, "Erreur sur l'attribut zone"
assert L4.point_lumineux == 5, "Erreur sur l'attribut point_lumineux"
assert L4.etat_allume is False, "Erreur sur l'attribut etat_allume"