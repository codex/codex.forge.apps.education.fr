# --------- PYODIDE:env --------- #
import p5


class Lampe:
    def __init__(self, nom, zone, pl):
        self.nom = nom
        self.zone = zone
        self.point_lumineux = pl
        self.etat_allume = False
        Lampe.actionne_telerupteur =actionne_telerupteur

    def allume(self):
        self.etat_allume = True
    
    def eteint(self):
        self.etat_allume = False



# --------- PYODIDE:code --------- #
def actionne_telerupteur(self):
    ...


# --------- PYODIDE:corr --------- #
def actionne_telerupteur(self):
    self.etat_allume = not self.etat_allume




# --------- PYODIDE:tests --------- #
L1 = Lampe("L1", 0, 0)
L2 = Lampe("L2", 0, 1)
L1.actionne_telerupteur()
L2.actionne_telerupteur()
L2.actionne_telerupteur()
assert L1.etat_allume is True
assert L2.etat_allume is False


# --------- PYODIDE:secrets --------- #
L4 = Lampe("Lampe salon", 5, 5)
assert L4.etat_allume is False
L4.actionne_telerupteur()
assert L4.etat_allume is True
L4.actionne_telerupteur()
assert L4.etat_allume is False

# --------- PYODIDE:post --------- #
lampes = [L1,L2,L3]
class App:
    def __init__(self):
        p5.run(self.setup,self.draw, target="cible_4")
    def draw(self):
        nb = 0
        for L in lampes:
            if L.etat_allume:
                p5.fill(255,255,0)
            else:
                p5.fill(0)
            p5.circle(20+nb*40, 50, 30)
            p5.stroke(0)
            p5.textSize(22)
            p5.strokeWeight(1)
            p5.fill(0)
            p5.text(L.nom, 10+nb*40,95)
            nb +=1
        p5.noLoop()

    def setup(self):
        """
        Fonction setup nécessaire au module p5.
        Définit tous les paramètres nécessaires à l'affichage et affiche 
        """
        self.i = 0
        p5.createCanvas(600, 150)
        p5.textFont("Calibri")
        p5.textSize(40)


        # AFFICHAGES PRÉLIMINAIRES (ou définifs s'il n'y a pas d'animation)
        p5.stroke(0)
        p5.textSize(22)
        p5.strokeWeight(1)
        
app = App()
