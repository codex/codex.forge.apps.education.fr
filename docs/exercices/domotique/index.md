---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: La domotique (I)
tags:
    - programmation orientée objet
difficulty: 100
maj: 31/03/2024
---

On souhaite créer une application domotique afin de pouvoir contrôler l'éclairage dans les différentes pièces d'une maison.

Cet exercice se concentre sur la création d'une classe permettant de contrôler une lampe.

Chaque lampe est caractérisée par un nom (une chaîne de caractères) et une adresse. Celle-ci est composée de deux nombres entiers : un numéro de zone et un numéro de point lumineux. Ces deux nombres sont compris entre `#!py 0` et `#!py 20`.

La variable booléenne `etat_allume` indique si la lampe est allumée (`#!py etat_allume= True`) ou éteinte ((`#!py etat_allume= False`). Par défaut, la lampe est éteinte.
 
??? info "La classes `Lampe`"

	La classe `Lampe` est définie ainsi :
	
	```mermaid
	classDiagram
    class Lampe 
        Lampe : nom [str]
        Lampe : zone [int]
        Lampe : point_lumineux [int]
        Lampe : etat_allume [bool]
        Lampe : __init__(self, nom, zone, pl)
        Lampe : adresse(self)
        Lampe : allume(self)
        Lampe : eteint(self)
        Lampe : allume_si_zone(self, zone_souhaitee)
        Lampe : actionne_telerupteur(self)
    
    ```

{{ remarque('methodes') }}

??? question "1. Créer une `Lampe`"

    Compléter le constructeur de la classe `Lampe`.

    ```pycon title=""
    >>> L1 = Lampe("L1", 0, 0)
    >>> L2 = Lampe("L2", 0, 1)
    >>> L3 = Lampe("L3", 1, 0)
    >>> L1.nom
    'L1'
    >>> L2.nom
    'L2'
    >>> L3.zone
    1
    >>> L3.etat_allume
    False
    ```
    {{ IDE('exo_0')}}

??? question "2. Récupérer l'adresse d'une `Lampe`"

    Compléter la méthode `adresse` de la classe `Lampe` qui renvoie l'adresse de la lampe sous la forme d'un couple d'entiers : le numéro de zone suivi du numéro de point lumineux.

    ```pycon title=""
    >>> L2 = Lampe("L2", 0, 1)
    >>> L2.adresse()
    (0, 1)
    ```
    {{ IDE('exo_1')}}



??? question "3. Allumer et éteindre une `Lampe`"

    Compléter les méthodes `allume` et `eteint` qui permettent d'allumer et d'éteindre la lampe.

    ```pycon title=""
    >>> L1.allume()
    >>> L1.etat_allume
    True
    >>> L1.eteint()
    >>> L1.etat_allume
    False    
    ```
    {{ IDE('exo_2')}}

    {{ figure('cible_2') }} 

??? question "4. Allumer les lampes d'une zone"

    On souhaite pouvoir allumer toutes les lampes situées dans une même zone.

    Compléter la méthode `allume_si_zone` qui permet d'allumer une lampe si elle appartient à la zone dont le numéro est passé en paramètre de la méthode.

    ```pycon title=""
    >>> L1 = Lampe("L1", 0, 0)
    >>> L3 = Lampe("L3", 1, 0)
    >>> L1.allume_si_zone(0)
    >>> L1.etat_allume
    True
    >>> L3.allume_si_zone(0)
    >>> L3.etat_allume
    False    
    ```
    {{ IDE('exo_3')}}

    {{ figure('cible_3') }} 

??? question "5. Utiliser un télérupteur"

    Le mode télérupteur donne la possibilité avec un même interrupteur bouton de pouvoir allumer et éteindre une lampe.

    Compléter la méthode `actionne_telerupteur` qui permet de faire basculer l'état de la lampe, de allumée à éteinte ou d'éteinte à allumée.
    
    ```pycon title=""
    >>> L1 = Lampe("L1", 0, 0)
    >>> L1.actionne_telerupteur()
    >>> L1.etat_allume
    True
    >>> L1.actionne_telerupteur()
    >>> L1.etat_allume
    False    
    ```    
    {{ IDE('exo_4')}}

    {{ figure('cible_4') }} 