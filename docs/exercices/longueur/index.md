---
author: Serge Bays
hide:
    - navigation
    - toc
title: Longueur d'un objet
tags:
    - string
    - liste/tableau
    - tuple
    - dictionnaire
difficulty: 50
maj: 23/08/2024
---


On dispose en Python d'une fonction `len` qui renvoie la longueur d'un objet de type `str`, `tuple`, `list` ou `dict`.

???+ example "Exemples d'utilisation:"

    ```pycon title=""
    >>> len('abcde')
    5
    >>> len((4, 8))
    2
    >>> len([5, 8, 3, 4])
    4
    >>> len({'A': 65, 'B': 66, 'C': 67})
    3
    ```
	
On demande d'écrire une fonction `longueur` qui prend en paramètre un objet de type `str`, `tuple`, `list` ou `dict` et qui renvoie sa longueur, **sans utiliser la fonction `len`**. 
  
{{ IDE('exo', SANS = "len, .__len__") }}
