

# --------- PYODIDE:code --------- #

def longueur(objet):
    ...

# --------- PYODIDE:corr --------- #

def longueur(objet):
    n = 0
    for elt in objet:
        n = n + 1
    return n

# --------- PYODIDE:tests --------- #

ob = [5, 8, 3, 4]
assert longueur(ob) == 4
ob = (4, 8)
assert longueur(ob) == 2
ob = 'abcde'
assert longueur(ob) == 5
ob = {'A': 65, 'B': 66, 'C': 67}
assert longueur(ob) == 3

# --------- PYODIDE:secrets --------- #

for ob in ([], (), '', {}):
    assert longueur(ob) == 0
assert longueur(([1, 2], 'a', (4, 5, 6), {'A':65, 'B': 66})) == 4
