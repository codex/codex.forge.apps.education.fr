---
hide:
    - navigation
    - toc
search:
    exclude: true
---

# <img src="./logo_robot_128x128.png" style="height:2em;" alt=""> CodEx : le code par les exercices


Ce site propose des exercices d'apprentissage de l'algorithmique et de la programmation par le biais d'exercices variés. Le langage utilisé est [**Python**](https://www.python.org/).

Les exercices proposés ont été **écrits**, **testés**, **corrigés** et **améliorés** par des professeurs d'informatique du secondaire et du supérieur.

Aucune installation, aucune inscription ne sont nécessaires : tous les programmes sont exécutés sur votre machine, tablette ou téléphone.

??? infos "Vidéo de présentation de **CodEx**"

    <center>
        <iframe title="CodEx - présentation" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/e92ca685-935e-4e20-8b7b-8af4f800e580" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </center>

    Cette vidéo est complétée par [deux autres](https://codex.forge.apps.education.fr/aide/#tutoriels-videos) expliquant comment chercher un exercice et le fonctionnement des éditeurs.

<div class="accueil-container">
<div class="admonition tip accueil-div">
<p class="admonition-title">Débuter <img alt="🚀" class="twemoji" src="https://cdn.jsdelivr.net/gh/jdecked/twemoji@15.0.3/assets/svg/1f680.svg" title=":rocket:"></p>
<p>Vous pouvez choisir les exercices de plusieurs façons :</p>
<ul>
<li>
🧭 <a href="recherche/">en cherchant un exercice</a> par titre, difficulté ou thématiques
<li>
</li>
<li>
🚶 <a href="parcours/">en suivant un des parcours</a> proposés
</li>
<li>
🐦 en choisissant <a href="exam/">une épreuve pratique blanche</a>
</li>
<li>
✅ en consultant <a href="aide/">la page d'aide</a> qui précise le fonctionnement du site
</li>
</ul>
</div>

<div class="admonition tip accueil-div">
<p class="admonition-title">À l'aventure <img alt="🐯" class="twemoji" src="https://cdn.jsdelivr.net/gh/jdecked/twemoji@15.0.3/assets/svg/1f42f.svg" title=":tiger:"></p>
<p>Vous pouvez aussi traiter un exercice au hasard :<button class="md-tag" onclick="exercice_hasard()" style="float:right" id="bouton-relancer">Relancer</button></i></p>
<p><span><span class="md-tag debutant">Débutant</span></span><a id="exo-debutant" href="#"></a></p>
<p><span><span class="md-tag facile">Facile</span></span><a id="exo-facile" href="#"></a></p>
<p><span><span class="md-tag moyen">Moyen</span></span><a id="exo-moyen" href="#"></a></p>
<p><span><span class="md-tag difficile">Difficile</span></span><a id="exo-difficile" href="#"></a></p>
</div>


