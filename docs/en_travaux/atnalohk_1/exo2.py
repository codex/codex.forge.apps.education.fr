# --------- PYODIDE:env --------- #
DIRECTIONS = [(-1,0), (1, 0), (0, 1), (0, -1)]
EAU = 0
ILE = 1

def nombre_iles(archipel):
    pass

def est_dans(archipel, i, j):
    n, m = len(archipel), len(archipel[0])
    return 0 <= i < n and 0 <= j < m

def est_eau(archipel, i, j):
    n, m = len(archipel), len(archipel[0])
    return 0 <= i < n and 0 <= j < m and archipel[i][j] == EAU

def est_ile(archipel, i, j):
    n, m = len(archipel), len(archipel[0])
    return 0 <= i < n and 0 <= j < m and archipel[i][j] == ILE

FRONTIERE = -1

# --------- PYODIDE:code --------- #
DIRECTIONS = [(-1,0), (1, 0), (0, 1), (0, -1)]
EAU = 0
ILE = 1

def ajoute_cases_ile(archipel, i, j, d):
    ...

def nombre_iles(archipel):
    ...

# --------- PYODIDE:corr --------- #
# Avec un ensemble (set) plutôt qu'un dictionnaire
# def ajoute_cases_ile(archipel, i, j, s):
#     """ [[int]], int, int, dict -> dict
#     Ajoute à d toutes les cases de l'île contenant la case (i, j) """
#     s.add((i, j))
#     for di, dj in DIRECTIONS:
#         ni, nj = i + di, j + dj
#         if est_ile(archipel, ni, nj) and not (ni, nj) in s:
#             ajoute_cases_ile_cnx(archipel, ni, nj, s)
#     return s

def ajoute_cases_ile(archipel, i, j, d):
    d[(i, j)] = True
    for di, dj in DIRECTIONS:
        ni, nj = i + di, j + dj
        if est_ile(archipel, ni, nj) and not (ni, nj) in d:
            ajoute_cases_ile(archipel, ni, nj, d)
    return d

def nombre_iles(archipel):
    n, m = len(archipel), len(archipel[0])
    nb_iles = 0
    cases_ile = dict()
    for i in range(n):
        for j in range(m):
            if est_ile(archipel, i, j) and not (i, j) in cases_ile:
                nb_iles += 1
                ajoute_cases_ile(archipel, i, j, cases_ile)
    return nb_iles

# --------- PYODIDE:tests --------- #
from copy import deepcopy

archipel1 = [
    [0, 0, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 0]
    ]
archipel1_bis = deepcopy(archipel1) # copie indépendante de archipel1
archipel2 = [
    [1, 0, 1, 0],
    [0, 1, 0, 1],
    [1, 0, 1, 0]
    ]
archipel2_bis = deepcopy(archipel2) # copie indépendante de archipel1

assert nombre_iles(archipel1) == 2
assert archipel1 == archipel1_bis, "L'archipel a été modifié" 
assert nombre_iles(archipel2) == 6
assert archipel2 == archipel2_bis, "L'archipel a été modifié"

# --------- PYODIDE:secrets --------- #
import random
def random_cases_eau_adj(archipel, i, j, mini=2, maxi=4):
    """ [[int]], int, int -> [(int, int)]
    Renvoie la liste des cases EAU adjacentes à (i, j),
    selectionnées aléatoirement (mini deux, maxi 4) """
    cases_eau_adj = [(i + di, j + dj) for di, dj in DIRECTIONS
                     if est_eau(archipel, i + di, j + dj)]
    return random.choices(cases_eau_adj, k = random.randint(mini, maxi))

def genere_ile(archipel, i, j, taille):
    """ [[int]], int, int, int -> [[int]], {(int, int)}
    Modifie taille éléments de archipel de type EAU adjacents
    pour les transformer en ILE. Renvoie l'ensemble des indices
    que forme l'île ainsi constituée. """
    file_cases = [(i, j)]
    ile = set()
    while len(ile) < taille:
        i, j = file_cases.pop(0)   
        if est_eau(archipel, i, j):
            archipel[i][j] = ILE
            ile.add((i, j))
        # certaines cases peuvent parfois être ajoutées deux fois à la file...
        file_cases.extend(random_cases_eau_adj(archipel, i, j)) 
    # print(f"Génération d'une ile de taile {len(ile)} : {ile}")
    return archipel, ile

def frontiere(archipel, ile):
    """ [[int]], {(int, int)} -> {(int, int)}
    Renvoie les indices des cases EAU à la frontière
    des cases dont les indices sont ceux présents dans ile """
    f = set()
    for i, j in ile:
        for di, dj in DIRECTIONS:
            ni, nj = i + di, j + dj
            if est_eau(archipel, ni, nj) and not (ni, nj) in ile:
                archipel[ni][nj] = FRONTIERE
                f.add((ni, nj))
    return f                  

def essai_genere_archipel(n, m, tailles_iles):
    """ int, int, int, [int] -> [[int]], [{(int, int)}]
    Renvoie une grille de taille n×m constituée de n_iles = len(tailles_iles)
    séparées par au moins une case de FRONTIERE. Les îles
    iles[0], iles[1], ... i[n_iles - 1] sont de taille respectives
    tailles[0], tailles[1], ..., tailles[n_iles - 1]
    cases de type ILE adjacentes (haut bas gauche droite).
    Renvoie également l'ensemble des îles (ensemble d'indices
    du archipel) de l'archipel ainsi formé.
    Soulève IndexError si l'algorithme aléatoire échoue à générer
    une telle configuration (trop d'îles). """
    archipel = [[0 for _ in range(m)]
               for _ in range(n)] 
    cases = {(i, j) for i in range(n)
             for j in range(m)}
    iles = []
    for taille in tailles_iles:
        i, j = random.choice(tuple(cases)) # ... No comment.
        archipel, ile = genere_ile(archipel, i, j, taille=taille)
        iles.append(ile)
        cases = cases.difference(ile).difference(frontiere(archipel, ile))
    for i in range(n):
        for j in range(m):
            if archipel[i][j] == FRONTIERE:
                archipel[i][j] = EAU
    return archipel, iles

def genere_archipel(*args, essais=10):
    for i in range(essais):
        try:
            return essai_genere_archipel(*args)
        except IndexError:
            continue
    raise ValueError("Impossible de génerer un archipel")

# Tests
for n in range(6):
    archipel, iles = genere_archipel(10, 10, list(range(n + 1))) 
    archipelc = deepcopy(archipel)
    reponse = nombre_iles(archipel)
    attendu = n
    assert reponse == n, f"Erreur avec {archipel = }"
    assert archipel == archipelc, "L'archipel a été modifié"
