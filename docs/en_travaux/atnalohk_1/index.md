---
author:
    - Florian Picard
hide:
    - navigation
    - toc
title: L'archipel d'Atnalohk (I)
difficulty: 220
tags:
    - dictionnaire
    - graphe
    - récursivité
    - pile
    - brouillon
maj: 27/09/2024
---

Le magnifique l'archipel d'Atnalohk, perdu dans les îles du pacifique, est constitué d'une myriade d'îles, toutes plus splendides les unes que les autres.

![img](archipel200.png){.center .autolight}
![img](archipel2200.png){.center .autolight}
![img](archipel3200.png){.center .autolight}
 

On modélise un archipel par une liste de listes `archipel` de taille $n\times m$ ($n$ listes contenant chacune $m$ valeurs : soit `#!py 0` qui représente l'eau, soit `#!py 1` qui représentent la terre.

Dans toute la suite, on appelle « coordonnées » d'un bloc le couple $(i,j)$ dans lequel $i$ est le numéro de ligne et $j$ le numéro de colonne.

On fournit les fonctions suivantes :

- `est_dans` : étant donné une liste de listes `archipel` et deux entiers `i` et `j`, renvoie `#!py True` si et seulement si `i` et `j` sont des indices compatibles avec la taille de l'archipel, c'est à dire si $0 \leqslant  i < n$ et $0 \leqslant  j < m$. Dans le cas contraire, la fonction renvoie `#!py False` ;

- `est_eau` : étant donné une liste de listes `archipel` et deux entiers `i` et `j`, renvoie `#!py True` si et seulement si `i` et `j` la case d'indice $(i, j)$ de `archipel` correspond à une case d'eau, c'est à dire si `i` et `j` sont compatibles avec la taille de l'archipel et si `archipel[i][j]` vaut `0`. Dans le cas contraire, la fonction renvoie `#!py False` ;

- `est_ile` : étant donné une liste de listes `archipel` et deux entiers `i` et `j`, renvoie `#!py True` si et seulement si `i` et `j` la case d'indice $(i, j)$ de `archipel` correspond à une case de terre, c'est à dire si `i` et `j` sont compatibles avec la taille de l'archipel et si `archipel[i][j]` vaut `1`. Dans le cas contraire, la fonction renvoie `#!py False`.

Ces trois fonctions sont déjà importées dans les éditeurs. Vous pouvez directement les utiliser.

??? info "Fonctions fournies"

    Pour information, on fournit ci-dessous les codes des trois fonctions décrites ci-dessus :
    
    ```python title=""
    ILE = 1
    EAU, FRONTIERE = 0, -1
    
    def est_dans(archipel, i, j):
        n, m = len(archipel), len(archipel[0])
        return 0 <= i < n and 0 <= j < m

    def est_eau(archipel, i, j):
        n, m = len(archipel), len(archipel[0])
        return 0 <= i < n and 0 <= j < m and archipel[i][j] == EAU

    def est_ile(archipel, i, j):
        n, m = len(archipel), len(archipel[0])
        return 0 <= i < n and 0 <= j < m and archipel[i][j] == ILE
    ```
    
??? definition "Île"

	On appelle **île** de l'archipel tout ensemble d'indices adjacents dans les directions nord, sud, est, ouest. Deux cases adjacentes en diagonale forment deux îles distinctes.

	Par exemple, soient les deux variables `archipel1` et `archipel2`, représentant chacune un archipel de taille $3\times 4$.
	
	```python
	archipel1 = [
        [0, 0, 1, 1],
	    [1, 0, 1, 0],
	    [0, 0, 0, 0]
        ]
	archipel2 = [
        [1, 0, 1, 0],
	    [0, 1, 0, 1],
	    [1, 0, 1, 0]
        ]
	```
	
	- `archipel1` est constitué de deux îles : la première est constituée du bloc d'indice $(1, 0)$, la seconde des blocs d'indices $(0, 2)$, $(0, 3)$ et $(1, 2)$ ;

	- `archipel2` est constitué de six îles : chaque île est constituée d'un seul bloc.

L'objectif de cet exercice est d'écrire une fonction `nombre_iles` qui, étant donné une liste de listes `archipel`, renvoie le nombre d'îles dont est composé l'archipel. 

??? question "Modification **autorisée**"

    Dans cette section on suppose que l'**on peut** modifier la liste de listes `archipel`. L'algorithme permettant de compter le nombre d'îles de l'archipel est le suivant :

    Pour chacun des blocs d'indice $(i, j)$ de l'archipel :
    
    - si le bloc d'indice $(i, j)$ correspond à une case d'eau : on ne fait rien ;
    
    - si le bloc d'indice $(i, j)$ correspond à une case de terre : on ajoute $1$ au nombre d'îles. Afin de ne pas recompter cette île par la suite, on affecte la valeur `#!py 0` à toutes les cases de terre de la liste `archipel` qui font partie de la même île.

    On pourra écrire une fonction auxiliaire `detruit_ile` qui étant donné une liste de listes `archipel` et deux entiers `i` et `j` met à `#!py 0` toutes les cases de terre d'`archipel` connectées à la case d'indice $(i, j)$.

    {{ IDE('exo1') }}


??? question "Modification **interdite**"

    Dans cette section on suppose que l'on **ne peut pas** modifier la liste de listes `archipel`.
    
    On commencera par écrire une fonction `ajoute_cases_ile` qui, étant donné une liste de listes `archipel`, deux indices `i` et `j` et un dictionnaire `d` met à jour le dictionnaire `d` de la manière suivante :

    - les clés initialement présentes dans le dictionnaire ne sont pas modifiées ;

    - on associe la valeur `#!py True` à la clé `#!py (u, v)` lorsqu'il existe un chemin constitué uniquement de cases de terre adjacentes qui mène de `(i, j)` à `(u ,v)`.

    ``` pycon
    >>> archipel3 = [
    ...    [0, 1, 1],
    ...    [1, 0, 1],
    ...    [1, 1, 0]
    ...    ]
    >>> vues = dict()
    >>> ajoute_cases_ile(archipel3, 0, 1, vues)
    >>> vues
    {(0, 1): True, (0, 2): True, (1, 2): True}
    >>> ajoute_cases_ile(archipel3, 2, 0, vues)
    >>> vues
    {(0, 1): True, (0, 2): True, (1, 2): True, (2, 0): True, (1, 0): True, (2, 1): True}
    ```

    L'algorithme permettant de compter le nombre d'îles de l'archipel est alors :

    -   on initialise une variable `vues` de type `dict` permettant de tenir compte des cases de terre déjà visitées par l'algorithme ;

    -   pour chacune des cases d'indice $(i, j)$ de l'`archipel`, 
        -   si la case d'indice $(i, j)$ correspond à une case d'eau : on ne fait rien ;

        -   si la case d'indice $(i, j)$ correspond à une case de terre :
            -   si la case d'indice $(i, j)$ est une clé du dictionnaire `vues` cela signifie qu'elle a déjà été comptabilisée : on ne fait rien ;

            -   sinon : on ajoute au dictionnaire `vues` toutes les cases de terre de la matrice `archipel` qui appartiennent à la même île que la case d'indice $(i, j)$.

    {{ IDE('exo2') }}

