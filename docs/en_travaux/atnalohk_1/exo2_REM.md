On peut considérer un archipel comme un graphe non orienté : dont les sommets sont les cases de la grille. Ceux-ci sont reliés par une arête si et seulement si ils sont adjacents dans les directions nord/sud/est/ouest (pas en diagonale).

La solution proposée met alors en place en deux étapes :

-   la fonction `ajoute_cases_ile` étant donné une position dans la grille, effectue un **parcours en profondeur**, en ajoutant au dictionnaire `d` toutes les cases de la **composante connexe** de la case d'indice $(i, j)$ dans `archipel`.
-   dans un second temps, la fonction `nombre_iles_non` **parcourt tous les sommets du graphe** représenté par la matrice `archipel` via parcours de tableau bidimensionnel et on applique la fonction `ajoute_cases_ile` à chaque composante connexe.

