# --------- PYODIDE:code --------- #
def somme_maximale(grille):
    hauteur = ...
    largeur = ...
    ligne_maxis = [0 for _ in range(...)]
    ligne_maxis[...] = ...
    for j in range(1, largeur):
        ligne_maxis[j] = ... + ...
    for i in range(1, hauteur):
        suivante = [0 for _ in range(...)]
        suivante[0] = ... + ...
        for j in range(1, largeur):
            suivante[j] = ... + ...
        ligne_maxis = ...
    return ...


# --------- PYODIDE:corr --------- #
def somme_maximale(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    ligne_maxis = [0 for _ in range(largeur)]
    ligne_maxis[0] = grille[0][0]
    for j in range(1, largeur):
        ligne_maxis[j] = grille[0][j] + ligne_maxis[j - 1]
    for i in range(1, hauteur):
        suivante = [0 for _ in range(largeur)]
        suivante[0] = grille[i][0] + ligne_maxis[0]
        for j in range(1, largeur):
            suivante[j] = grille[i][j] + max(ligne_maxis[j], suivante[j - 1])
        ligne_maxis = suivante
    return ligne_maxis[largeur - 1]


# --------- PYODIDE:tests --------- #
grille = [[3, 5, 6], [4, 9, 1]]
assert somme_maximale(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme_maximale(grille) == 11
grille = [[1]]
assert somme_maximale(grille) == 1


# --------- PYODIDE:secrets --------- #
from random import randrange


def _somme_max_(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [0 for _ in range(largeur)]
    maxis[0] = grille[0][0]
    for j in range(1, largeur):
        maxis[j] = grille[0][j] + maxis[j - 1]
    for i in range(1, hauteur):
        suivante = [0 for _ in range(largeur)]
        suivante[0] = grille[i][0] + maxis[0]
        for j in range(1, largeur):
            suivante[j] = grille[i][j] + max(maxis[j], suivante[j - 1])
        maxis = suivante
    return maxis[largeur - 1]


for _ in range(20):
    hauteur = randrange(1, 30)
    largeur = randrange(1, 30)
    grille = [[randrange(0, 50) for _ in range(largeur)] for _ in range(hauteur)]
    attendu = _somme_max_(grille)
    assert somme_maximale(grille) == attendu, f"Erreur avec {grille = }"
