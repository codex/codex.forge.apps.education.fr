# --------- PYODIDE:code --------- #
def somme_maximale(grille):
    def somme_maximale_rec(i, j): 
        ...
    ...


# --------- PYODIDE:corr --------- #
def somme_maximale(grille):
    def somme_maximale_rec(i, j):
        if maxis[i][j] is None:
            if i == 0:
                maxis[i][j] = grille[i][j] + somme_maximale_rec(i, j - 1)
            elif j == 0:
                maxis[i][j] = grille[i][j] + somme_maximale_rec(i - 1, j)
            else:
                maxis[i][j] = grille[i][j] + max(
                    somme_maximale_rec(i - 1, j), somme_maximale_rec(i, j - 1)
                )
        return maxis[i][j]

    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [[None for _ in range(largeur)] for _ in range(hauteur)]
    maxis[0][0] = grille[0][0]
    return somme_maximale_rec(hauteur - 1, largeur - 1)


# --------- PYODIDE:tests --------- #
grille = [[3, 5, 6], [4, 9, 1]]
assert somme_maximale(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme_maximale(grille) == 11
grille = [[1]]
assert somme_maximale(grille) == 1


# --------- PYODIDE:secrets --------- #
from random import randrange


def _somme_max_(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [0 for _ in range(largeur)]
    maxis[0] = grille[0][0]
    for j in range(1, largeur):
        maxis[j] = grille[0][j] + maxis[j - 1]
    for i in range(1, hauteur):
        suivante = [0 for _ in range(largeur)]
        suivante[0] = grille[i][0] + maxis[0]
        for j in range(1, largeur):
            suivante[j] = grille[i][j] + max(maxis[j], suivante[j - 1])
        maxis = suivante
    return maxis[largeur - 1]


for _ in range(20):
    hauteur = randrange(1, 30)
    largeur = randrange(1, 30)
    grille = [[randrange(0, 50) for _ in range(largeur)] for _ in range(hauteur)]
    attendu = _somme_max_(grille)
    assert somme_maximale(grille) == attendu, f"Erreur avec {grille = }"
