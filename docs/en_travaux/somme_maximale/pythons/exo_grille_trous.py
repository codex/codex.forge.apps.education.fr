# --------- PYODIDE:code --------- #
def somme_maximale(grille):
    hauteur = ...
    largeur = ...
    maxis = [[0 for _ in range(...)] for _ in range(...)]
    maxis[...][...] = ...
    for i in range(1, hauteur):
        maxis[i][0] = ... + ...
    for j in range(1, largeur):
        maxis[0][j] = ... + ...
    for i in range(1, hauteur):
        for j in range(1, largeur):
            maxis[i][j] = ...
    return ...


# --------- PYODIDE:corr --------- #
def somme_maximale(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [[0 for _ in range(largeur)] for _ in range(hauteur)]
    maxis[0][0] = grille[0][0]
    for i in range(1, hauteur):
        maxis[i][0] = grille[i][0] + maxis[i - 1][0]
    for j in range(1, largeur):
        maxis[0][j] = grille[0][j] + maxis[0][j - 1]
    for i in range(1, hauteur):
        for j in range(1, largeur):
            maxis[i][j] = grille[i][j] + max(maxis[i - 1][j], maxis[i][j - 1])
    return maxis[hauteur - 1][largeur - 1]


# --------- PYODIDE:tests --------- #
grille = [[3, 5, 6], [4, 9, 1]]
assert somme_maximale(grille) == 18
grille = [[0, 7, 1, 3]]
assert somme_maximale(grille) == 11
grille = [[1]]
assert somme_maximale(grille) == 1


# --------- PYODIDE:secrets --------- #
from random import randrange


def _somme_max_(grille):
    hauteur = len(grille)
    largeur = len(grille[0])
    maxis = [0 for _ in range(largeur)]
    maxis[0] = grille[0][0]
    for j in range(1, largeur):
        maxis[j] = grille[0][j] + maxis[j - 1]
    for i in range(1, hauteur):
        suivante = [0 for _ in range(largeur)]
        suivante[0] = grille[i][0] + maxis[0]
        for j in range(1, largeur):
            suivante[j] = grille[i][j] + max(maxis[j], suivante[j - 1])
        maxis = suivante
    return maxis[largeur - 1]


for _ in range(20):
    hauteur = randrange(1, 30)
    largeur = randrange(1, 30)
    grille = [[randrange(0, 50) for _ in range(largeur)] for _ in range(hauteur)]
    attendu = _somme_max_(grille)
    assert somme_maximale(grille) == attendu, f"Erreur avec {grille = }"
