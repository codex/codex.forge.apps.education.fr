On voit sur cet exercice qu'il peut être périlleux de manipuler l'attribut `montant_depose` en dehors de la classe `Livret` car cela se fait sans les vérifications nécessaires.

On dit qu'un attribut est **privé** quand il n'est pas directement accessible en dehors de la définition de la classe. 

On doit alors utiliser des méthodes appelées **accesseurs** pour lire leur valeur et d'autres appelées **mutateurs** pour les modifier.

Dans notre exemple, les attributs `montant_depose` et `total_interets` doivent être considérés comme privés. 

Leur caractère privé est indiqué par le symbole $-$ devant leur nom dans le diagramme ci-dessous :

```mermaid
classDiagram
    class Livret {
     __init__()
    +int plafond
    +float taux
    -float montant_depose
    -float total_interets
    recupere_solde()
    depose()
    retire()
    calcule_interets()
    }
```    

La méthode `calcule_interets` est un mutateur pour l'attribut `total_interets` et les méthodes `depose` et `retire` des mutateurs pour l'attribut `montant_depose`.

La méthode `recupere_solde` sert d'accesseur pour les deux attributs.