# --------- PYODIDE:code --------- #

class Livret:
    def __init__(self, montant_depart, taux_interet, plafond):
        self.montant_depose = montant_depart
        self.taux = taux_interet
        self.plafond = plafond
        self.total_interets = 0

    def recupere_solde(self):
        ...     
           
    def depose(self, montant):
        ...
        
    def retire(self, montant):
        ...
    
    def calcule_interets(self):
        ...
        


# --------- PYODIDE:corr --------- #


class Livret:
    def __init__(self, montant_depart, taux_interet, plafond):
        self.montant_depose = montant_depart
        self.taux = taux_interet
        self.plafond = plafond
        self.total_interets = 0
      
    def recupere_solde(self):
        return self.montant_depose + self.total_interets

    def depose(self, montant):
        if montant + self.montant_depose < self.plafond:
            self.montant_depose += montant
            return True
        else:
            return False
        
    def retire(self, montant):
        if self.montant_depose - montant > 0:
            self.montant_depose -= montant
            return True
        else:
            return False
    
    def calcule_interets(self):
        self.total_interets += self.montant_depose * self.taux

# --------- PYODIDE:tests --------- #
livret_1 = Livret(500, 0.02, 20000)
assert livret_1.depose(1000) is True
assert livret_1.recupere_solde() == 1500
assert livret_1.depose(100000) is False
assert livret_1.recupere_solde() == 1500
livret_1.calcule_interets()
assert abs(livret_1.recupere_solde() - 1530) < 10e-6
assert livret_1.retire(10000) is False
assert abs(livret_1.recupere_solde() - 1530) < 10e-6
assert livret_1.retire(1000) is True
assert abs(livret_1.recupere_solde() - 530) < 10e-6


# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
livret_2 = Livret(500, 0.1, 200000)
assert livret_2.depose(10000) is True
assert livret_2.recupere_solde() == 10500
assert livret_2.depose(100000) is True
assert livret_2.recupere_solde() == 110500
assert livret_2.depose(10000000) is False
assert livret_2.recupere_solde() == 110500
assert livret_2.retire(500) is True
assert livret_2.recupere_solde() == 110000
assert livret_2.retire(10000) is True
assert livret_2.recupere_solde() == 100000
assert livret_2.retire(100001) is False
assert livret_2.recupere_solde() == 100000
livret_2.calcule_interets()
assert abs(livret_2.recupere_solde() - 110000) < 10e-6