---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Livret d'épargne
difficulty: 150
tags:
    - programmation orientée objet
    - en travaux
maj: 06/06/2024
---

Un livret d'épargne permet de placer de l'argent et d'obtenir des intérêts.

Un livret se caractérise par :

* son plafond, qui ne doit pas être dépassé par les sommes déposées,
* son taux d'intérêts, qui permet de calculer chaque mois les intérêts qui se rajoute au solde.

Dans cet exercice, on souhaite créer une classe `Livret`, décrite ci-dessous :


```mermaid
classDiagram
    class Livret {
     __init__()
    +int plafond
    +float taux
    -float montant_depose
    -float total_interets
    recupere_solde()
    depose()
    retire()
    calcule_interets()
    }
```    

L'attribut `total_interets` permet de stocker le cumul des intérêts générés par le livret.

Cette classe possède les méthodes décrites ci-dessous :

??? question "La méthode `recupere_solde`"
    La méthode`recupere_solde` renvoie le solde du livret, qui est la somme du montant déposé et des intérêts.

    Exemple : si on a déposé 500 euros, le solde sera de 500 euros. Si cela génère 5 euros d'intérêts, le solde sera alors de 505 euros.

    ```pycon
    >>> livret_1 = Livret(500, 0.1, 20000)
    >>> livret_1.recupere_solde()
    500
    >>> livret_1.calcule_interets()
    >>> livret_1.recupere_solde()
    505
    ```
    
??? question "La méthode `depose`"
    La méthode`depose` prend en paramètre le montant du dépot, modifie l'attibut `montant_depose` si cela n'entraine pas le dépassement du plafond, et qui renvoie `True` si l'opération est effectuée et `False` sinon.

    Exemple : si on a déposé $5000$ euros, il est possible de déposer $1000$ euros. Mais avec un plafond fixé à $20000$, on ne pourra pas $16000$ euros.Si cela génère 5 euros d'intérêts, le solde sera alors de $505$ euros.

    ```pycon
    >>> livret_1 = Livret(5000, 0.02, 20000)
    >>> livret_1.depose(1000)
    True
    >>> livret_1.recupere_solde()
    6000
    >>> livret_1.depose(16000)
    False
    ```

??? question "La méthode `retire`"
    La méthode`retire` prend en paramètre le montant du retrait souhaité, modifie l'attibut `montant_depose` si cela n'entraine pas un solde négatif, et qui renvoie `True` si l'opération est effectuée et `False` sinon.

    Exemple : si on a déposé $5000$ euros au départ, il est possible de retirer $1000$ euros, mais pas $7000$ car le solde deviendrait négatif.

    ```pycon
    >>> livret_1 = Livret(5000, 0.02, 20000)
    >>> livret_1.retire(1000)
    True
    >>> livret_1.recupere_solde()
    4000
    >>> livret_1.retire(7000)
    False
    ```

??? question "La méthode `calcule_interets`"
    La méthode `calcule_interets` calcule les intérêts du livret et les additionne aux intérêts déjà calculés. (Les intérêts ne sont pas pris en compte pour le plafond, qui ne concerne que les sommes déposées.)
    
    Les intérêts sont calculés sur le solde.

    Exemple : si on a déposé $5000$ euros au départ, un taux de $0.01$ (soit $1%$) ajouterait $50$ euros d'intérêts. Une deuxième application du taux génèrerait $50,5$ euros d'intérêts, pour un gain total de $100.5$ euros.

    ```pycon
    >>> livret_1 = Livret(5000, 0.01, 20000)
    >>> livret_1.calcule_interets()
    >>> livret_1.recupere_solde()
    5050
    >>> livret_1.calcule_interets()
    >>> livret_1.recupere_solde()
    5100.5
    ```

{{ remarque('proximite_flottants') }}

Compléter la classe ci-dessous :

{{ IDE('exo', MAX_SIZE=45) }}
