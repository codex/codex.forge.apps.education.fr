# --------- PYODIDE:env --------- #

from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.append(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.popleft()

# --------- PYODIDE:code --------- #

def degres_separation(reseau, membre1):
    ...

# --------- PYODIDE:corr --------- #

def degres_separation(reseau, membre1):
    file = File()
    file.enfile((membre1,0))
    degres = {membre1 : 0}
    while not file.est_vide():
        membre, degre = file.defile()
        for ami in reseau[membre]:
            if ami not in degres:
                degres[ami] = degre+1
                file.enfile((ami, degre+1))
    return degres

# --------- PYODIDE:tests --------- #

immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna", "Eroll"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Gaby"],
    "Gaby": ["Eroll"],
}
assert degres_separation(immediam, "Billy") == {'Billy': 0, 'Anna': 1, 'Eroll': 1, 'Dora': 2, 'Flynn': 2, 'Gaby': 2}
assert degres_separation(immediam, "Eroll") == {'Eroll': 0, 'Billy': 1, 'Dora': 1, 'Flynn': 1, 'Gaby': 1, 'Anna': 2}
assert degres_separation(immediam, "Flynn") == {'Flynn': 0, 'Gaby': 1, 'Eroll': 2, 'Billy': 3, 'Dora': 3, 'Anna': 4}

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
immediam = {
    "Anna": ["Billy"],
    "Billy": ["Anna"],
    "Carl": ["Billy"],
    "Dora": ["Gaby"],
    "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
    "Flynn": ["Eroll", "Gaby"],
    "Gaby": ["Eroll", "Flynn"],
    "Charles": [],
}

assert degres_separation(immediam, "Anna") == {'Anna': 0, 'Billy': 1}
assert degres_separation(immediam, "Gaby") == {'Gaby': 0, 'Eroll': 1, 'Flynn': 1, 'Billy': 2, 'Dora': 2, 'Anna': 3}
assert degres_separation(immediam, "Dora") == {'Dora': 0, 'Gaby': 1, 'Eroll': 2, 'Flynn': 2, 'Billy': 3, 'Anna': 4}