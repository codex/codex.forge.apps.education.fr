---
author:
    - Guillaume Connan
    - Nicolas Revéret
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Degré de séparation
difficulty: 220
tags:
    - dictionnaire
    - graphe
    - file
    - en travaux
maj: 06/06/2024
---

On considère le schéma suivant où une flèche allant d'une personne Α vers une
personne B indique que la personne A « suit » la personne B sur son compte
*Immediam*. On dit alors que B est un *ami* de A.

??? example "Voici un exemple de réseau *Immediam*"
    ```mermaid
    flowchart LR
        A([Anna]) --> B([Billy])
        B --> A
        B --> E([Eroll])
        C([Carl]) --> B
        D([Dora]) --> G([Gaby])
        E --> B
        E --> D
        E --> G
        E --> F([Flynn])
        F --> G
        G --> E
    ```
    Les amis d'Eroll sont Dora, Gaby, Flynn et Billy.

    Les amis des amis d'Eroll sont donc Gaby (amie à la fois de Dora et Flynn) et Anna (amie de Billy).

!!! abstract "Degré de séparation"
    Dans un réseau social, le degré de séparation est le nombre de relations entre deux individus du réseau.

    Ainsi, il y a $1$ degré de séparation entre Billy et Eroll, et $3$ degrés entre Carl et Flynn.


On représente ce réseau *Immediam* en machine par un dictionnaire dans lequel :

- les clés sont les chaînes de caractères correspondant aux noms des personnes inscrites, 
  
- les valeurs associées sont des listes de chaînes de caractères représentant les personnes suivies.


Écrire une fonction  `degres_separation` qui :

- prend en argument un dictionnaire `reseau` représentant un tel réseau *Immediam* et une chaînes de caractères
`membre` qui désigne un membre du réseau ;

- renvoie un dictionnaire qui associe à chaque membre du réseau un entier indiquant le degré de séparation entre les deux membres. 


??? tip "Aide (1)"

    On pourra utiliser un parcours particulier du graphe. À ce titre, on fournit une la classe `File` dont on donne l'interface ci-dessous.

    {{ remarque('interface_File')}}
    
    {{ remarque('classe_File')}}

??? tip "Aide (2)"

    On pourra stocker des couples (membre, degre_de_separation).


???+ example "Exemples"

    ```pycon title=""
    >>> immediam = {
            "Anna": ["Billy"],
            "Billy": ["Anna", "Eroll"],
            "Carl": ["Billy"],
            "Dora": ["Gaby"],
            "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
            "Flynn": ["Gaby"],
            "Gaby": ["Eroll"],
    }
    >>> degres_separation(immediam, 'Billy')
	{'Billy': 0, 'Anna': 1, 'Eroll': 1, 'Dora': 2, 'Flynn': 2, 'Gaby': 2}
    >>> degres_separation(immediam, 'Flynn')
	{'Eroll': 0, 'Billy': 1, 'Dora': 1, 'Flynn': 1, 'Gaby': 1, 'Anna': 2}
	>>> degres_separation(immediam, 'Eroll')
	{'Flynn': 0, 'Gaby': 1, 'Eroll': 2, 'Billy': 3, 'Dora': 3, 'Anna': 4}
    ```

{{ IDE('exo') }}
