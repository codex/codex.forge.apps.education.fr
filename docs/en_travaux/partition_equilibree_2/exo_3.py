
# --------- PYODIDE:env --------- #

def tableau(liste):
    s = sum(liste)
    tab = [[False for i in range(s+1)]]
    tab[0][0] = True # obtenir 0 avec l'ensemble vide
    for i in range(len(liste)):
        ligne = list(tab[i])
        ligne[liste[i]] = True
        for j in range(s+1):
            if tab[i][j] == True:
                ligne[j+liste[i]] = True
        tab.append(ligne)
    m = s // 2
    while not tab[len(liste)][m]:
        m = m - 1
    return tab, m

# --------- PYODIDE:code --------- #

def partition2(liste, i, t, v, liste_1=None):
    if liste_1 is None:
        liste_1 = []
    ...

# --------- PYODIDE:corr --------- #

def partition2(liste, i, t, v, liste_1=None):
    if liste_1 is None:
        liste_1 = []    
    if i == 0:
        return liste_1
    if t[i-1][v]:
        return partition2(liste, i-1, t, v, liste_1)
    elif t[i-1][v-liste[i-1]]:
        liste_1.append(liste[i-1])
        return partition2(liste, i-1, t, v-liste[i-1], liste_1)

# --------- PYODIDE:tests --------- #

ens = [1]
t, m = tableau(ens)
ens1 = partition2(ens, len(ens), t, m)
assert ens1 == [1] or ens1 == []
ens = [5, 4]
t, m = tableau(ens)
ens1 = partition2(ens, len(ens), t, m)
assert ens1 == [5] or ens1 == [4]
ens = [5, 4, 3, 3, 3]
t, m = tableau(ens)
ens1 = partition2(ens, len(ens), t, m)
assert ens1 == [5, 4] or ens1 == [4, 5] or ens1 == [3, 3, 3]

# --------- PYODIDE:secrets --------- #

ens = [1, 1, 5, 1, 1, 1]
t, m = tableau(ens)
ens1 = partition2(ens, len(ens), t, m)
assert ens1 == [5] or ens1 == [1, 1, 1, 1, 1], f"Partition incorrecte pour {ens}"
ens = [10, 8, 5, 7, 9, 5]
t, m = tableau(ens)
ens1 = partition2(ens, len(ens), t, m)
assert set(ens1) == {10, 7, 5} or set(ens1) == {9, 8, 5}, f"Partition incorrecte pour {ens}"
