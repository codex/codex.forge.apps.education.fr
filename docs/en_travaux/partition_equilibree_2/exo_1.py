

# --------- PYODIDE:code --------- #

def partition(liste, s):
    ...

# --------- PYODIDE:corr --------- #

def partition(liste, s):
    if len(liste) <= 1:
        return liste
    test_1 = [liste[0]] + partition(liste[1:], s-liste[0])
    test_2 = partition(liste[1:], s)
    if abs(sum(test_1) - s) < abs(sum(test_2) - s):
        return test_1
    else:
        return test_2

# --------- PYODIDE:tests --------- #

ens = [1]
ens1 = partition(ens, sum(ens)/2)
assert ens1 == [1] or ens1 == []
ens = [5, 4]
ens1 = partition(ens, sum(ens)/2)
assert ens1 == [5] or ens1 == [4]
ens = [5, 4, 3, 3, 3]
ens1 = partition(ens, sum(ens)/2)
assert ens1 == [5, 4] or ens1 == [4, 5] or ens1 == [3, 3, 3]

# --------- PYODIDE:secrets --------- #

ens = [1, 1, 5, 1, 1, 1]
ens1 = partition(ens, sum(ens)/2)
assert ens1 == [5] or ens1 == [1, 1, 1, 1, 1], f"Partition incorrecte pour {ens}"
ens = [10, 8, 5, 7, 9, 5]
ens1 = partition(ens, sum(ens)/2)
assert set(ens1) == {10, 7, 5} or set(ens1) == {9, 8, 5}, f"Partition incorrecte pour {ens}"
