# --------- PYODIDE:env --------- #

def attendu(liste):
    s = sum(liste)
    tab = [[False for i in range(s+1)]]
    tab[0][0] = True
    for i in range(len(liste)):
        ligne = list(tab[i])
        ligne[liste[i]] = True
        for j in range(s+1):
            if tab[i][j] == True:
                ligne[j+liste[i]] = True
        tab.append(ligne)
    m = s // 2
    while not tab[len(liste)][m]:
        m = m - 1
    return tab, m

# --------- PYODIDE:code --------- #

def tableau(liste):
    ...

# --------- PYODIDE:corr --------- #

def tableau(liste):
    s = sum(liste)
    tab = [[False for i in range(s+1)]]
    tab[0][0] = True # obtenir 0 avec l'ensemble vide
    for i in range(len(liste)):
        ligne = list(tab[i])
        ligne[liste[i]] = True
        for j in range(s+1):
            if tab[i][j] == True:
                ligne[j+liste[i]] = True
        tab.append(ligne)
    m = s // 2
    while not tab[len(liste)][m]:
        m = m - 1
    return tab, m

# --------- PYODIDE:tests --------- #

assert tableau([]) == ([[True]], 0)
assert tableau([1]) == ([[True, False], [True, True]], 0)
t, m = tableau([2, 1])
assert t[0] == [True, False, False, False]
assert t[1] == [True, False, True, False]
assert t[2] == [True, True, True, True]
assert m == 1

# --------- PYODIDE:secrets --------- #

from random import randint
ens = [randint(1, 50) for i in range(500)]
t, m = tableau(ens)
ta, ma = attendu(ens)
assert t == ta, f"Erreur sur le tableau pour la liste {ens}"
assert m == ma, f"Erreur sur la valeur attendue de m"
