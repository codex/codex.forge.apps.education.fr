

# --------- PYODIDE:code --------- #

def perm_rec(liste, pris):
    if len(pris) == ...:
        return [pris.copy()]
    else:
        liste_permutations = ...
        for element in liste:
            if ...:
                pris.append(...)
                for permut in perm_rec(..., ...):
                    liste_permutations.append(...)
                pris.pop()
        return ...

def permutations(liste):
    return perm_rec(..., ...)

# --------- PYODIDE:corr --------- #

def perm_rec(liste, pris):
    if len(pris) == len(liste):
        return [pris.copy()]
    else:
        liste_permutations = []
        for element in liste:
            if element not in pris:
                pris.append(element)
                for permut in perm_rec(liste, pris):
                    liste_permutations.append(permut)
                pris.pop()
        return liste_permutations

def permutations(liste):
    return perm_rec(liste, [])

# tests

assert perm_rec([1, 2, 3], [1]) == [[1, 2, 3], [1, 3, 2]]
assert perm_rec(['a', 'v', 'i', 'o', 'n'], ['n', 'i', 'v']) == [['n', 'i', 'v', 'a', 'o'], ['n', 'i', 'v', 'o', 'a']]
assert perm_rec([7, 4, 5, 3, 8], [3, 4]) == [[3, 4, 7, 5, 8], [3, 4, 7, 8, 5], [3, 4, 5, 7, 8], [3, 4, 5, 8, 7], [3, 4, 8, 7, 5], [3, 4, 8, 5, 7]]
assert perm_rec([6, 1, 4], []) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]

assert permutations([1, 2, 3]) == [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
assert permutations([]) == [[]]
assert permutations([6, 1, 4]) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]

# --------- PYODIDE:tests --------- #

assert perm_rec([1, 2, 3], [1]) == [[1, 2, 3], [1, 3, 2]]
assert perm_rec(['a', 'v', 'i', 'o', 'n'], ['n', 'i', 'v']) == [['n', 'i', 'v', 'a', 'o'], ['n', 'i', 'v', 'o', 'a']]
assert perm_rec([7, 4, 5, 3, 8], [3, 4]) == [[3, 4, 7, 5, 8], [3, 4, 7, 8, 5], [3, 4, 5, 7, 8], [3, 4, 5, 8, 7], [3, 4, 8, 7, 5], [3, 4, 8, 5, 7]]
assert perm_rec([6, 1, 4], []) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]

assert permutations([1, 2, 3]) == [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
assert permutations([]) == [[]]
assert permutations([6, 1, 4]) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]

# --------- PYODIDE:secrets --------- #


# autres tests

assert perm_rec([], []) == ([[]])
assert perm_rec([1, 2, 3, 4], [4, 1, 2, 3]) == [[4, 1, 2, 3]]
assert perm_rec(['c', 'h', 'i', 'e', 'n'], ['n', 'i']) == [['n', 'i', 'c', 'h', 'e'], ['n', 'i', 'c', 'e', 'h'], ['n', 'i', 'h', 'c', 'e'], ['n', 'i', 'h', 'e', 'c'], ['n', 'i', 'e', 'c', 'h'], ['n', 'i', 'e', 'h', 'c']]

assert permutations([1, 2]) == [[1, 2], [2, 1]]
assert permutations([1]) == [[1]]
assert permutations([9, 3, 1, 2]) == [[9, 3, 1, 2], [9, 3, 2, 1], [9, 1, 3, 2], [9, 1, 2, 3], [9, 2, 3, 1], [9, 2, 1, 3], [3, 9, 1, 2], [3, 9, 2, 1], [3, 1, 9, 2], [3, 1, 2, 9], [3, 2, 9, 1], [3, 2, 1, 9], [1, 9, 3, 2], [1, 9, 2, 3], [1, 3, 9, 2], [1, 3, 2, 9], [1, 2, 9, 3], [1, 2, 3, 9], [2, 9, 3, 1], [2, 9, 1, 3], [2, 3, 9, 1], [2, 3, 1, 9], [2, 1, 9, 3], [2, 1, 3, 9]]