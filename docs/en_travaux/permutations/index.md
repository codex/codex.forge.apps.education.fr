---
author: Romain Janvier
hide:
    - navigation
    - toc
title: Permutations
tags:
    - à trous
    - en travaux
    - récursivité
    - liste/tableau
---
# Permutations des éléments d'une liste

L'ordre des éléments d'une liste est important. Mais parfois, on veut pouvoir utiliser un autre ordre. Par exemple `[1, 2, 3]` et `[2, 1, 3]` contiennent les mêmes éléments dans des ordres différents. On dit que ce sont des **permutations** l'une de l'autre.


Voici toutes les permutations de `[1, 2, 3]` :

* `[1, 2, 3]`
* `[1, 3, 2]`
* `[2, 1, 3]`
* `[2, 3, 1]`
* `[3, 1, 2]`
* `[3, 2, 1]`

L'objectif de cet exercice est de faire une fonction `permutations` qui prend une liste et renvoie la liste de toutes les permutations de cette liste. Il faut que tous les éléments de la liste soit différents les uns des autres.

Pour cela, on utilise la fonction récursive `permut_rec` qui prend deux listes `liste` et `pris` et qui renvoie la liste de toutes les permutations de `liste` qui commencent par `pris`. Les éléments de `pris` doivent être des éléments de `liste` sans répétition.

À la fin de l'appel `prem_rec(liste, pris)` il faut que `pris` ait retrouvé son état initial, même s'il est modifié pendant l'appel.

Compléter le code :

???+ example "Exemples"

    ```pycon title=""
    >>> perm_rec([1, 2, 3], [1])
    [[1, 2, 3], [1, 3, 2]]
    >>> perm_rec(['a', 'v', 'i', 'o', 'n'], ['n', 'i', 'v'])
    [['n', 'i', 'v', 'a', 'o'], ['n', 'i', 'v', 'o', 'a']]
    >>> perm_rec([7, 4, 5, 3, 8], [3, 4])
    [[3, 4, 7, 5, 8], [3, 4, 7, 8, 5], [3, 4, 5, 7, 8], [3, 4, 5, 8, 7], [3, 4, 8, 7, 5], [3, 4, 8, 5, 7]]
    >>> perm_rec([6, 1, 4], [])
    [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]
    ```

    ```pycon
    >>> permutations([1, 2, 3])
    [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
    >>> permutations([])
    [[]]
    >>> permutations([6, 1, 4])
    [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]
    ```

{{ IDE('exo') }}

