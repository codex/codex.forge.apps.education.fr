# --- PYODIDE:code --- #
def tri_rapide(tableau, debut, fin):
    ...


# --- PYODIDE:corr --- #
def tri_rapide(tableau, debut, fin):
    # Cas de base
    if fin - debut < 2:
        return

    pivot = tableau[fin - 1]
    curseur = debut

    # Partition du tableau
    for i in range(debut, fin - 1):
        if tableau[i] <= pivot:
            tableau[i], tableau[curseur] = tableau[curseur], tableau[i]
            curseur += 1

    # Positionnement définitif du pivot
    tableau[fin - 1], tableau[curseur] = tableau[curseur], tableau[fin - 1]

    # Appels récursifs
    tri_rapide(tableau, debut, curseur)
    tri_rapide(tableau, curseur + 1, fin)
# --- PYODIDE:tests --- #
tableau_0 = [3, 1, 2]
tri_rapide(tableau_0, 0, len(tableau_0))
assert tableau_0 == [1, 2, 3], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau_1 = [1, 2, 3, 4]
tri_rapide(tableau_1, 0, len(tableau_1))
assert tableau_1 == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_rapide(tableau_2, 0, len(tableau_2))
assert tableau_2 == [-5, -2], "Erreur avec des valeurs négatives"

tableau_3 = []
tri_rapide(tableau_3, 0, len(tableau_3))
assert tableau_3 == [], "Erreur avec un tableau vide"
