---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Tri rapide
tags:
    - en travaux
    - tri
    - diviser pour régner
    - récursivité
difficulty: 250
---



Le tri rapide est un algorithme de tri efficace utilisé dans plusieurs langages de programmation (C et Java par exemple).

 L'idée générale de l'algorithme est la suivante :

* on choisit une valeur « pivot » dans le tableau et on la garde en mémoire (ici on la place à la fin du tableau),
* on parcourt l'ensemble des valeurs et on effectue des échanges de façon à ce que, à l'issue du parcours, les valeurs inférieures ou égales au pivot soient situées au début du tableau, celles strictement supérieures à la fin,
* la position définitive du pivot est alors la frontière des deux zones. On peut ainsi le placer de façon certaine et réitérer la démarche de façon récursive sur chacune des zones.


Observons le déroulement du parcours, étape aussi appelée **partition**.

Ce parcours étudie successivement tous les éléments du tableau (variable `i`).

Lors du parcours on fait en sorte de ramener dans la zone de gauche du tableau les éléments inférieurs ou égaux au pivot. La variable `curseur` permet de repérer la position d'insertion du prochain élément inférieur au pivot.

Ainsi, à chaque étape du parcours, l'algorithme garantit que tous les éléments d'indices strictement inférieurs à `curseur` sont inférieurs ou égaux au pivot.

* État initial

    ![Étape 0](images/pivot_0.png#only-light){ width=45% align=left}
    ![Étape 0](images/pivot_0_dark.png#only-dark){ width=45% align=left}

    Le pivot est le `#!py 5`, il est en dernière position.

    `i` et `curseur` sont initialisés à `#!py 0`.

---

* 1ère étape : `#!py i = 0` et `#!py curseur = 0`

    ![Étape 1](images/pivot_1.png#only-light){ width=45% align=left}
    ![Étape 1](images/pivot_1_dark.png#only-dark){ width=45% align=left}

    Le `#!py 6` lu est strictement supérieur au pivot. On ne fait pas d'échange.

    `i` augmente de `#!py 1`.

    `curseur` n'est pas modifié.

---

* 2ème étape : `#!py i = 1` et `#!py curseur = 0`

    ![Étape 2](images/pivot_2.png#only-light){ width=45% align=left}
    ![Étape 2](images/pivot_2_dark.png#only-dark){ width=45% align=left}

    Le `#!py 7` lu est strictement supérieur au pivot. On ne fait pas d'échange.

    `i` augmente de `#!py 1`.

    `curseur` n'est pas modifié.

---

* 3ème étape : `#!py i = 2` et `#!py curseur = 0`


    ![Étape 3](images/pivot_3.png#only-light){ width=45% align=left}
    ![Étape 3](images/pivot_3_dark.png#only-dark){ width=45% align=left}

    Le `#!py 3` lu est inférieur ou égal au pivot : on l'échange avec le `#!py 6` situé à la position du `curseur`.

    `i` augmente de `#!py 1`.

    `curseur` augmente de `#!py 1`.

---

* 4ème étape : `#!py i = 3` et `#!py curseur = 1`

    ![Étape 4](images/pivot_4.png#only-light){ width=45% align=left}
    ![Étape 4](images/pivot_4_dark.png#only-dark){ width=45% align=left}

    Le `#!py 4` lu est inférieur ou égal au pivot : on l'échange avec le `#!py 7` situé à la position du `curseur`.

    `i` augmente de `#!py 1`.

    `curseur` augmente de `#!py 1`.

---

* 5ème étape : `#!py i = 4` et `#!py curseur = 2`

    ![Étape 5](images/pivot_8.png#only-light){ width=45% align=left}
    ![Étape 5](images/pivot_8_dark.png#only-dark){ width=45% align=left}

    Le `#!py 8` lu est strictement supérieur au pivot. On ne fait pas d'échange.

    `i` augmente de `#!py 1`.

    `curseur` n'est pas modifié.

---

* 6ème étape : `#!py i = 5` et `#!py curseur = 2`

    ![Étape 6](images/pivot_5.png#only-light){ width=45% align=left}
    ![Étape 6](images/pivot_5_dark.png#only-dark){ width=45% align=left}

    Le `#!py 1` lu est inférieur ou égal au pivot : on l'échange avec le `#!py 6` situé à la position du `curseur`.

    `i` augmente de `#!py 1`.

    `curseur` augmente de `#!py 1`.

---

* Fin du parcours : `#!py curseur = 3`

    ![Étape 6](images/pivot_6.png#only-light){ width=45% align=left}
    ![Étape 6](images/pivot_6_dark.png#only-dark){ width=45% align=left}

    En fin de parcours, `curseur` vaut `#!py 3` et est égal à la position définitive du pivot.

    On échange le pivot `#!py 5` et le `#!py 7` (à la position du `curseur`).

---

* Partition terminée :

    ![Étape 7](images/pivot_7.png#only-light){ width=45% align=left}
    ![Étape 7](images/pivot_7_dark.png#only-dark){ width=45% align=left}

    Le pivot est à sa position définitive.

    À sa gauche on ne trouve que des valeurs qui lui sont inférieures ou égales. À sa droite que des valeurs strictement supérieures.

---

??? note "Choix du pivot"

    Le choix du pivot est une étape importante.

    Il est possible par exemple de toujours choisir le dernier élément du tableau. En moyenne ce choix est intéressant mais il est problématique si le tableau est initialement trié (ou presque trié). Dans ce cas, le choix du dernier élément comme pivot augmente le nombre d'opérations nécessaires et donc le coût de l'algorithme.

    !!! note "Remarque"

        On peut montrer que le choix du dernier élément comme pivot n'est pas pénalisant si on prend soin de mélanger le tableau avant de le trier. Il est alors peu probable de rencontrer un tableau quasi-trié !

    Une autre possibilité est de choisir un pivot aléatoire dans le tableau. Dans ce cas, les tableaux initialement triés n'augmenteront pas sensiblement le coût de l'algorithme.


    Dans la suite de cette page on choisira le dernier élément comme pivot afin de simplifier la démarche.

!!! tips "Algorithme"

    On considère un tableau dont on souhaite trier les éléments d'indices compris entre `#!py debut` (inclus) et `#!py fin` (exclu).

    On choisit comme `#!py pivot` le dernier élément de la zone à trier. On a donc `#!py pivot = tableau[fin - 1]`.

    La phase de partition du tableau consiste alors à parcourir tous les éléments de la zone à trier (sauf le pivot) afin de ramener les valeurs inférieures ou égales au pivot au début de la zone, les valeurs supérieures à la fin.

    Pour ce faire on utilise deux indices :

    * `#!py i` est l'indice utilisé lors du parcours. Il stocke l'indice de l'élément lu actuellement et évolue entre `#!py debut` (inclus) et `#!py fin - 1` (exclu),
    * `#!py curseur` est l'indice de l'élément qui sera échangé avec l'élément lu si besoin. Au fil du parcours, on est certain que **tous les éléments d'indice strictement inférieurs à `curseur` sont inférieurs ou égaux au pivot**.

    Ces deux indices sont initialisés à la valeur `debut`.

    Étudions en détail une étape du parcours. On a déjà lu un certain nombre de valeurs et le déroulé de l'algorithme jusqu'à cette étape garantit que `#!py i` est supérieur ou égal à `#!py curseur`.

    On lit donc la valeur `#!py tableau[i]` et on la compare à `#!py pivot` :

    * si cette valeur est strictement supérieure au pivot, comme elle est déjà dans la zone des valeurs supérieures (`#!py i >= curseur`), on ne la déplace pas ;
    * si à l'inverse la valeur est inférieure ou égale au pivot, il faut la ramener dans la zone du début du tableau : on échange les valeurs d'indices `#!py i` et `#!py curseur`. La zone des valeurs inférieures s'est agrandie : on incrémente `#!py curseur` en conséquence.

    À la fin du parcours, les éléments d'indices strictement inférieurs à `#!py curseur` sont tous inférieurs ou égaux au pivot. Ceux d'indice supérieurs ou égaux sont strictement supérieurs au pivot. On peut donc placer le pivot à la position `curseur` en faisant un dernier échange.

    Il est ensuite possible de recommencer le tri sur le début du tableau (indice allant de `#!py debut` inclus à `#!py curseur` exlcus) et la fin (indices entre `#!py curseur + 1` inclus et `#!py fin` exclu).



??? question "La fonction `#!py partition`"

    Compléter la fonction `#!py partition` prenant en argument un `#!py tableau` ainsi que deux entiers `#!py debut` et `#!py fin` et réalisant une partition du tableau entre les indices `#!py debut` (inclus) et `#!py fin` en fonction de la valeur pivot située à l'indice `#!py fin`.<br>La fonction renverra la position finale de la valeur pivot.


    {{ IDE('exo1') }}


??? question "La fonction `#!py tri_rapide`"

    Écrire la fonction `#!py tri_rapide` prenant en argument un `#!py tableau` ainsi que deux entiers `#!py debut` et `#!py fin` et triant le tableau entre les indices `#!py debut` (inclus) et `#!py fin` (exclu) à l'aide du tri rapide.

    !!! tips "La fonction `#!py partition`"
        On pourra s'aider de la fonction `#!py partition` qui est déjà chargée en mémoire.

    {{ IDE('exo2') }}

