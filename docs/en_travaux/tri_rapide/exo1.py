# --- PYODIDE:env --- #
def partition(tableau, debut, fin):
    ...
    
# --- PYODIDE:code --- #
def partition(tableau, debut, fin, pivot):
    pivot = tableau[...]
    curseur = debut

    # Partition du tableau
    for i in range(debut, fin):
        if tableau[...] <= ...:
            tableau[...], tableau[...] = tableau[...], tableau[...]
            curseur += 1

    # Positionnement définitif du pivot
    tableau[...], tableau[...] = tableau[...], tableau[...] 
    return ...



    
# --- PYODIDE:corr --- #
def partition(tableau, debut, fin):
    pivot = tableau[fin]
    curseur = debut

    # Partition du tableau
    for i in range(debut, fin):
        if tableau[i] <= pivot:
            tableau[i], tableau[curseur] = tableau[curseur], tableau[i]
            curseur += 1

    # Positionnement définitif du pivot
    tableau[fin], tableau[curseur] = tableau[curseur], tableau[fin]
    return curseur


# --------- PYODIDE:tests --------- #

nombres = [3, 0, 2, 1, 5, 1]
assert partition(nombres, 0, 5) == 2
assert nombres == [0, 1, 1, 3, 5, 2]

nombres = [7, 2, 5, 5, 9, 1, 9, 2, 5, 7]
assert partition(nombres, 0, len(nombres)-1) == 7
assert nombres == [7, 2, 5, 5, 1, 2, 5, 7, 9, 9]


# --------- PYODIDE:secrets --------- #

nombres = list(range(10, -1, -1))
partition(nombres, 0, len(nombres)-1)
assert nombres == [0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 10]

nombres =  [0] * 10 + [10] * 10 + [5] 
assert partition(nombres, 0, len(nombres)-1) == 10
assert nombres == [0] * 10 + [5] + [10] * 10


