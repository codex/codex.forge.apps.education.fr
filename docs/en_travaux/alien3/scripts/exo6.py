# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas, ligne, colonne
import p5

valide = "if colonne" in __USER_CODE__
assert valide is True, "On doit utiliser les structures conditionnelles"
app.centrer("figure6") 


# --------- PYODIDE:code --------- #
a = ...
b = 5
gauche(2)
... colonne() < 4:
    bas(b)
...:
    haut(b)
droite(a)


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H6', 'M6', 'M8'])