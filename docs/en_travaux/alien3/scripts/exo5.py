# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas, ligne, colonne
import p5

valide = "if colonne" in __USER_CODE__
assert valide is True, "On doit utiliser les structures conditionnelles"
app.centrer("figure5") 


# --------- PYODIDE:code --------- #
gauche(...)
if ligne()  ... :
    e = 2
else :
    e = 3
haut(e)
droite(8)
if colonne() ... :
    f = 5
else :
    f = 8
bas(f)
droite(...)



# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H3', 'F3', 'F11', 'N11', 'N13'])