# --------- PYODIDE:env --------- #
from alien_python import App, app, gauche, droite, haut, bas, ligne, colonne
import p5

valide = "if colonne" in __USER_CODE__
assert valide is True, "On doit utiliser les structures conditionnelles"
app.centrer("figure7") 


# --------- PYODIDE:code --------- #
gauche(...)
if ligne() < ...:
    bas(5)
    if colonne() > ...:
        droite(2)
    haut(2)
droite(2)


# --------- PYODIDE:post --------- #
if valide:
    app.verifier_programme(['H8', 'H4', 'M4', 'M6', 'K6', 'K8'])