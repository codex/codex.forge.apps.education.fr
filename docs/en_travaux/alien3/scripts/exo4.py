# --------- PYODIDE:env --------- #
from alien_python import *
import p5

app1 = App("figure4")


app1.gauche(4)
if app1.case() == "H5":
    app1.haut(2)
    app1.gauche(3)
elif app1.case() == "H4":
    app1.haut(3)
    app1.bas(2)
else:
    app1.haut(2)

app1.dessiner_parcours() 