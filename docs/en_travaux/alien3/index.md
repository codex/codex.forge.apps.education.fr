---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Alien (3) - Instructions conditionnelles
tags:
    - en travaux
    - SNT
difficulty : 050
---

??? note "Série d'exercices"
    Cet exercice fait partie d'une série :
    
    * « {{ lien_exo("Alien (1) : Appels de fonctions", "alien_1") }} »,

    * « {{ lien_exo("Alien (2) : Variables et affectations", "alien2") }} »,
    
    * « {{ lien_exo("Alien (3) : Instructions conditionnelles", "alien3") }} »,
    
    * « {{ lien_exo("Alien (4) : Boucles bornées", "alien4") }} »,
    
    * « {{ lien_exo("Alien (5) : Fonctions", "alien5") }} »,
    
    * « {{ lien_exo("Alien (6) : Tableaux", "alien6") }} ».

Les règles sont simples : l'alien :alien: se situe au départ au centre de la grille et peut être déplacé avec les fonctions `haut`, `bas`, `gauche` et `droite`.

L'objectif est de trouver la case finale de l'alien (et donc son parcours) après exécution du programme donné.

???+ example "Les différentes instructions"

    * `haut(4)` déplace l'alien de `#!py 4` cases vers le haut ;
    * `haut(-2)` déplace l'alien de `#!py 2` cases vers le bas ;
    * `haut()` déplace l'alien de `#!py 1` case vers le haut (c'est donc équivalent à `#!py haut(1)`).

    Le principe est similaire avec les trois autres fonctions.

    Trois autres fonctions permettent de situer l'alien dans la grille :

    * `case()` renvoie la case sur laquelle se trouve l'alien ;
    * `ligne()` renvoie la ligne de la case sur laquelle se trouve l'alien ;
    * `colonne()` renvoie la colonne de la case sur laquelle se trouve l'alien.

Pour les questions suivantes, dessinez le parcours de l'alien en cliquant sur **la case d'arrivée** de chaque instruction exécutée. Vous pourrez ensuite valider votre parcours pour vérifier s'il est correct.

!!! abstract "Instruction conditionnelle"

    Une **instruction conditionnelle**, ou instruction de test, permet de *faire des choix* en fonction de la valeur d'une *condition*. On parle souvent d'une instruction « *si ... alors* », ou « *if ... else* » en anglais.

    ```python
    if condition_1:
        bloc_instructions_1
    elif condition_2:
        bloc_instructions_2
    else:
        bloc_instructions_3
    ```
    
    Le code ci-dessus indique que si la `condition_1` est vraie, on n'exécute que `bloc_instruction_1`, sinon on regarde si `condition_2` est vraie, on n'exécute que `bloc_instruction_2`, et si les deux conditions sont fausses, on n'exécute que `bloc_instruction_3`
        
    Les mots-clés « `#!py if` », « `#!py elif` » (contraction de *else if*) et « `#!py else` » sont les traductions respectives de « si », « sinon si » et « sinon ».

???+ abstract "Comparaison"

    Une **condition** est une instruction qui est soit vraie, soit fausse. On dit qu'il s'agit d'une **expression booléenne**.

    Pour tester des inégalité larges (comme $a \leqslant b$ et $a \geqslant b$) ou la différence (comme $a\neq b$) on utilise les syntaxes suivantes :
    
    - le signe `<=` pour *inférieur ou égal* ;
    
    - le signe `>=` pour *supérieur ou égal* ;
    
    - le signe `!=` pour *n'est pas égal à*.

    On peut résumer les tests possibles dans le tableau ci-dessous :

    | Test                       | Syntaxe Python |
    | -------------------------- | -------------- |
    | $a=b$                      | `a == b`       |
    | $a\neq b$                  | `a != b`       |
    | $a<b$                      | `a < b`        |
    | $a\leqslant b$             | `a <= b`       |
    | $a>b$                      | `a > b`        |
    | $a\geqslant b$             | `a >= b`       |
    | $a<b<c$                    | `a < b < c`    |
    | $a\leqslant b \leqslant c$ | `a <= b <= c`  |
    | $a<b\leqslant c$           | `a < b <= c`   |

???+ warning "Source d'erreur classique"
    Le test d'égalité entre deux variables se fait avec un double égal `==` (car le simple `=` a un rôle différent : celui d'affecter une valeur à une variable).


??? question "Question 1 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    gauche(2)
    bas(3)
    if case() == "K6":
        droite(5)
    else:
        haut(5)
    haut(2)
    ```
    {{ run('scripts/exo1') }}
    {{ figure("figure1") }}
 

??? question "Question 2 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    haut(4)
    if ligne() < "E" :
        droite(6)
        bas(3)
    gauche(3)
    if colonne() < 6 :
        haut(2)
    gauche(2)
    ```
    {{ run('scripts/exo2') }}
    {{ figure("figure2") }}
 
??? question "Question 3 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    droite(4)
    if colonne() < 5:
        haut(3)
        gauche(4)
    else:
        haut(4)
    bas(2)
    ```
    {{ run('scripts/exo3') }}
    {{ figure("figure3") }}
 
??? question "Question 4 : Dessinez le parcours"
    ```python { .inline .end .w45 }
    gauche(4)
    if case() == "H5":
        haut(2)
        gauche(3)
    elif case() == "H4":
        haut(3)
        bas(2)
    else:
        haut(2)
    ```
    {{ run('scripts/exo4') }}
    {{ figure("figure4") }}

Pour les questions suivantes écrire le code nécessaire pour obtenir le déplacement souhaité (les numéros correspondent aux différentes étapes).

??? question "Question 5 : Codez le parcours"
    ![alien](images/alien41.png)

    {{ IDE('scripts/exo5') }}
    {{ figure("figure5",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 6 : Codez le parcours"
    ![alien](images/alien42.png)

    {{ IDE('scripts/exo6') }}
    {{ figure("figure6",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

??? question "Question 7 : Codez le parcours"
    ![alien](images/alien43.png)

    {{ IDE('scripts/exo7') }}
    {{ figure("figure7",
            inner_text="En cas d'erreur, le parcours s'affichera ici", 
            admo_title="Tracé du parcours") }}

