---
author:
    - Pierre Marquestaut
    - Denis Quenton
hide:
    - navigation
    - toc
title: Etat d'un processus
tags:
    - à trous
    - en travaux
    - programmation orientée objet
difficulty : 260
---


Un processus peut être dans un des trois états suivants : **prêt**, **élu** ou **bloqué**.
```mermaid
stateDiagram
    [*] --> prêt
    prêt --> élu
    élu --> prêt
    élu --> bloqué
    bloqué --> prêt
    élu --> [*]
```

Lorsqu'il est créé, un processus obtient l'état **prêt**. Cependant, dans le cadre de cet exercice, on souhaite simuler qu'un processus puisse avoir une date d'arrivée (correspondant un temps d'horloge). Ainsi, un processus est créé avec l'état **non créé** (état initial du diagramme représenté par un point noir), et passera à **prêt** quand il aura atteint sa date d'arrivée.

A l'inverse, le processus passera à l'état **terminé** (état final du diagramme représenté par un point cerclé de noir) quand sa durée d'exécution sera atteinte.

On souhaite créer une classe `Processus` qui permette de gérer le changement d'état du processus.

??? info "La Classe `Processus`"
    Un processus est un objet de la classe `Processus`. Il possède les attributs suivants :

    - `nom` : nom du processus,
    - `arrivee` : heure d'arrivée du processus dans la file d'attente,
    - `etat` : `#!py "élu"`, `#!py "prêt"`, `#!py "bloqué"`, `#!py "non créé"` et `#!py "terminé"`,
    - `duree` : durée restante supposée d'exécution du processus,
    - `temps_alloué` : temps d'exécution alloué par le processeur lorsqu'il élit le processus, initialisé à la valeur du quantum.

    Il dispose des méthodes suivantes :

    * `est_pret` qui prend en argument le temps d'horloge, change l'état à `#!py "prêt"` si ce temps correspond à sa date d'arrivée, et renvoie `True` si l'état du processus est à prêt.
    * `elit` qui change l'état à `#!py "élu"` et initialise le temps alloué avec la valeur du quantum passée en paramètre,
    * `execute` qui décrément de $1$ temps d'horloge le temps alloué et la durée d'exécution et peut changer l'état du processus à `#!py "terminé"` si la durée devient nulle ou à `#!py "prêt"` si le temps alloué devient nul,
    * `bloque` qui fait passer l'état du processus à `#!py "bloqué"`,
    * `debloque` qui fait passer l'état du processus à `#!py "prêt"`,

    ???+ example "Exemples"

        ```pycon title=""
        >>> processus_courant = Processus("P1", 4, arrivee = 0)
        >>> processus_courant.est_pret(0)
        >>> processus_courant
        {'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 4}
        >>> processus_courant.elit(2)
        >>> processus_courant
        {'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 4}
        >>> processus_courant.execute()
        >>> processus_courant
        {'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 3}
        >>> processus_courant.execute()
        >>> processus_courant
        {'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 2}
        >>> processus_courant.elit(2)
        >>> processus_courant
        {'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 2}
        >>> processus_courant.execute()
        >>> processus_courant
        {'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 1}"
        >>> processus_courant.bloque()
        >>> processus_courant
        {'etat': 'bloqué', 'nom': 'P1', 'arrivee': 0, 'duree': 1}
        >>> processus_courant.debloque()
        >>> processus_courant
        {'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 1}
        >>> processus_courant.elit(2)
        >>> processus_courant.execute()
        >>> processus_courant
        {'etat': 'terminé', 'nom': 'P1', 'arrivee': 0, 'duree': 0}
        ```


Compléter le code suivant.

{{ IDE('exo') }}
