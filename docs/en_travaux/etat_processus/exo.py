# --------- PYODIDE:code --------- #
class Processus():

    def __init__(self, nom, duree, arrivee=0):
        self.etat = "non créé"
        self.nom = nom
        self.duree = duree
        self.arrivee = arrivee
        self.temps_aloue = 0
        
    def elit(self, quantum):
        ...
        
    def execute(self):
        self.duree = self.duree - 1
        self.temps_alloue = self.temps_alloue - 1
        if self.duree == 0:
            self.etat = ...
        elif self.temps_alloue == 0:
            self.etat = ...
            
    def est_pret(self, horloge):
        if self.arrivee == horloge:
            self.etat = ...
        return ...
    
    def bloque(self):
        ...
        
    def debloque(self):
        ...            

    def __repr__(self):
        return str({ "etat": self.etat, "nom":self.nom, "arrivee" :self.arrivee, "duree":self.duree})


# --------- PYODIDE:corr --------- #
class Processus():

    def __init__(self, nom, duree, arrivee=0):
        self.etat = "non créé"
        self.nom = nom
        self.duree = duree
        self.arrivee = arrivee
        self.temps_aloue = 0
        
    def elit(self, quantum):
        self.temps_alloue = quantum
        self.etat = "élu"
        
    def execute(self):
        self.duree = self.duree - 1
        self.temps_alloue = self.temps_alloue - 1
        if self.duree == 0:
            self.etat = "terminé"
        elif self.temps_alloue == 0:
            self.etat = "prêt"
            
    def est_pret(self, horloge):
        if self.arrivee == horloge:
            self.etat = "prêt"
        return self.etat == "prêt"
    
    def bloque(self):
        self.etat = "bloqué"
        
    def debloque(self):
        self.etat = "prêt"
            

    def __repr__(self):
        return str({ "etat": self.etat, "nom":self.nom, "arrivee" :self.arrivee, "duree":self.duree})

# --------- PYODIDE:tests --------- #

QUANTUM = 2
processus_courant = Processus("P1", 4, arrivee = 0)
temps_horloge = 0
assert processus_courant.est_pret(temps_horloge) is True
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 4}"
processus_courant.elit(QUANTUM)
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 4}"
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 3}"
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 2}"
processus_courant.elit(QUANTUM)
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 2}"
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P1', 'arrivee': 0, 'duree': 1}"
processus_courant.bloque()
assert str(processus_courant) == "{'etat': 'bloqué', 'nom': 'P1', 'arrivee': 0, 'duree': 1}"
processus_courant.debloque()
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P1', 'arrivee': 0, 'duree': 1}"
processus_courant.elit(QUANTUM)
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'terminé', 'nom': 'P1', 'arrivee': 0, 'duree': 0}"

# --------- PYODIDE:secrets --------- #
QUANTUM = 1
processus_courant = Processus("P4", 2, arrivee = 0)
temps_horloge = 0
assert processus_courant.est_pret(temps_horloge) is True
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P4', 'arrivee': 0, 'duree': 2}"
processus_courant.elit(QUANTUM)
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P4', 'arrivee': 0, 'duree': 2}"
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P4', 'arrivee': 0, 'duree': 1}"
processus_courant.elit(QUANTUM)
assert str(processus_courant) == "{'etat': 'élu', 'nom': 'P4', 'arrivee': 0, 'duree': 1}"
processus_courant.bloque()
assert str(processus_courant) == "{'etat': 'bloqué', 'nom': 'P4', 'arrivee': 0, 'duree': 1}"
processus_courant.debloque()
assert str(processus_courant) == "{'etat': 'prêt', 'nom': 'P4', 'arrivee': 0, 'duree': 1}"
processus_courant.elit(QUANTUM)
processus_courant.execute()
assert str(processus_courant) == "{'etat': 'terminé', 'nom': 'P4', 'arrivee': 0, 'duree': 0}"

