# --------- PYODIDE:env --------- #
def cree_pile_vide():
    """Crée et renvoie une pile vide"""
    return []


def est_vide(p):
    """
    Renvoie le booléen répondant à la question :
    la pile p est-elle vide ?
    """
    return p == []


def empile(p, x):
    """Empile x au sommet de p"""
    p.append(x)


def depile(p):
    """
    Dépile et renvoie l'élément au sommet de p
    Provoque une erreur si p est vide
    """
    if est_vide(p):
        raise ValueError("La pile est vide")
    return p.pop()


# --------- PYODIDE:code --------- #
def renverse_deux(p):
    a = ...()
    b = ...()
    ...(p, ...)
    ...(p, ...)


# --------- PYODIDE:corr --------- #
def renverse_deux(p):
    a = depile(p)
    b = depile(p)
    empile(p, a)
    empile(p, b)


# --------- PYODIDE:tests --------- #
p = cree_pile_vide()
empile(p, "a")
empile(p, "b")
renverse_deux(p)
q = cree_pile_vide()
empile(q, "b")
empile(q, "a")
assert p == q


# --------- PYODIDE:secrets --------- #
p = cree_pile_vide()
q = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1):
    empile(p, x)
    empile(q, x)
    empile(r, x)
renverse_deux(p)
q[-1], q[-2] = q[-2], q[-1]
assert p == q, f"Erreur avec la pile {r}"
p = cree_pile_vide()
q = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5):
    empile(p, x)
    empile(q, x)
    empile(r, x)
renverse_deux(p)
q[-1], q[-2] = q[-2], q[-1]
assert p == q, f"Erreur avec la pile {r}"
