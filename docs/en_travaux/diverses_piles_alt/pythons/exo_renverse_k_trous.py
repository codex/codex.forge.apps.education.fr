# --------- PYODIDE:env --------- #
def cree_pile_vide():
    """Crée et renvoie une pile vide"""
    return []


def est_vide(p):
    """
    Renvoie le booléen répondant à la question :
    la pile p est-elle vide ?
    """
    return p == []


def empile(p, x):
    """Empile x au sommet de p"""
    p.append(x)


def depile(p):
    """
    Dépile et renvoie l'élément au sommet de p
    Provoque une erreur si p est vide
    """
    if est_vide(p):
        raise ValueError("La pile est vide")
    return p.pop()
    
    
# --------- PYODIDE:code --------- #
def renverse_k(p, k):
    annexe_1 = ...
    annexe_2 = ...
    for _ in range(k):
        ...(annexe_1, ....)
    for _ in range(k):
        ...(annexe_2, ....)
    for _ in range(k):
        ...


# --------- PYODIDE:corr --------- #
def renverse_k(p, k):
    annexe_1 = cree_pile_vide()
    annexe_2 = cree_pile_vide()
    for _ in range(k):
        empile(annexe_1, depile(p))
    for _ in range(k):
        empile(annexe_2, depile(annexe_1))
    for _ in range(k):
        empile(p, depile(annexe_2))


# --------- PYODIDE:tests --------- #
p = cree_pile_vide()
empile(p, "a")
empile(p, "b")
empile(p, "c")
empile(p, "d")
empile(p, "e")
renverse_k(p, 4)
q = cree_pile_vide()
empile(q, "a")
empile(q, "e")
empile(q, "d")
empile(q, "c")
empile(q, "b")
assert p == q


# --------- PYODIDE:secrets --------- #
p = cree_pile_vide()
attendu = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5, 6):
    empile(p, x)
    empile(attendu, x)
    empile(r, x)
k = 0
renverse_k(p, k)
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
k = 7
renverse_k(p, k)
attendu = attendu[::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
p = cree_pile_vide()
attendu = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5, 6):
    empile(p, x)
    empile(attendu, x)
    empile(r, x)
k = 5
renverse_k(p, k)
attendu[-5:] = attendu[-5:][::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
