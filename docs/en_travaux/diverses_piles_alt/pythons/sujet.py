# --- hdr, , renverse_deux_, taille_, fusion_simple_ --- #
def cree_pile_vide():
    """Crée et renvoie une pile vide"""
    return []


def est_vide(p):
    """
    Renvoie le booléen répondant à la question :
    la pile p est-elle vide ?
    """
    return p == []


def empile(p, x):
    """Empile x au sommet de p"""
    p.append(x)


def depile(p):
    """
    Dépile et renvoie l'élément au sommet de p
    Provoque une erreur si p est vide
    """
    if est_vide(p):
        raise ValueError("La pile est vide")
    return p.pop()


# --- exo, taille_ --- #
""" # skip
def taille(p):
    annexe = ...
    t = ...
    while not ...:
        empile(annexe, ...)
        t = ...
    while not ...:
        ...
    return ...
"""  # skip


# --- vide, taille_ --- #
def taille(p): ...


# --- corr, taille_ --- #
def taille(p):
    annexe = cree_pile_vide()
    t = 0
    while not est_vide(p):
        empile(annexe, depile(p))
        t = t + 1
    while not est_vide(annexe):
        empile(p, depile(annexe))
    return t


# --- tests, taille_ --- #
p = cree_pile_vide()
assert taille(p) == 0
empile(p, "a")
empile(p, "b")
assert taille(p) == 2
q = cree_pile_vide()
empile(q, "a")
empile(q, "b")
assert p == q


# --- secrets, taille_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5):
    empile(p, x)
    empile(q, x)
    empile(r, x)
assert taille(p) == 6
assert p == q, f"Erreur avec la pile {r}"

# --- exo, renverse_deux_ --- #
""" # skip
def renverse_deux(p):
    a = ...()
    b = ...()
    ...(p, ...)
    ...(p, ...)
"""  # skip


# --- vide, renverse_deux_ --- #
def renverse_deux(p): ...


# --- corr, renverse_deux_ --- #
def renverse_deux(p):
    a = depile(p)
    b = depile(p)
    empile(p, a)
    empile(p, b)


# --- tests, renverse_deux_ --- #
p = cree_pile_vide()
empile(p, "a")
empile(p, "b")
renverse_deux(p)
q = cree_pile_vide()
empile(q, "b")
empile(q, "a")
assert p == q


# --- secrets, renverse_deux_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1):
    empile(p, x)
    empile(q, x)
    empile(r, x)
renverse_deux(p)
q[-1], q[-2] = q[-2], q[-1]
assert p == q, f"Erreur avec la pile {r}"
p = cree_pile_vide()
q = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5):
    empile(p, x)
    empile(q, x)
    empile(r, x)
renverse_deux(p)
q[-1], q[-2] = q[-2], q[-1]
assert p == q, f"Erreur avec la pile {r}"
# --- exo, fusion_simple_ --- #
""" # skip
def fusion_simple(p, q):
    r = ...
    while ...:
        ...(r, ...)
        ...
    return r
"""  # skip


# --- vide, fusion_simple_ --- #
def fusion_simple(p, q):
    ...


# --- corr, fusion_simple_ --- #
def fusion_simple(p, q):
    r = cree_pile_vide()
    while not est_vide(p):
        empile(r, depile(p))
        empile(r, depile(q))
    return r


# --- tests, fusion_simple_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
empile(p, "c")
empile(p, "a")
empile(q, "d")
empile(q, "b")
r = fusion_simple(p, q)
a = cree_pile_vide()
empile(a, "a")
empile(a, "b")
empile(a, "c")
empile(a, "d")
assert r == a


# --- secrets, fusion_simple_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
attendu = cree_pile_vide()
assert fusion_simple(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = cree_pile_vide()
qq = cree_pile_vide()
for x in (0, 1, 2):
    empile(p, x)
    empile(pp, x)
for x in (4, 5, 6):
    empile(q, x)
    empile(qq, x)
attendu = cree_pile_vide()
attendu = [2, 6, 1, 5, 0, 4]
assert fusion_simple(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
# --- exo, fusion_ --- #
""" # skip
def fusion(p, q):
    r = ...
    while ...:
        ...(r, ...)
        ...
    while ...:
        ...(r, ...)
    while ...:
        ...
    return r
"""  # skip


# --- vide, fusion_ --- #
def fusion(p, q):
    ...


# --- corr, fusion_ --- #
def fusion(p, q):
    r = cree_pile_vide()
    while not est_vide(p) and not est_vide(q):
        empile(r, depile(p))
        empile(r, depile(q))
    while not est_vide(p):
        empile(r, depile(p))
    while not est_vide(q):
        empile(r, depile(q))
    return r


# --- tests, fusion_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
empile(p, "e")
empile(p, "c")
empile(p, "a")
empile(q, "d")
empile(q, "b")
r = fusion(p, q)
a = cree_pile_vide()
empile(a, "a")
empile(a, "b")
empile(a, "c")
empile(a, "d")
empile(a, "e")
assert r == a


# --- secrets, fusion_ --- #
p = cree_pile_vide()
q = cree_pile_vide()
attendu = cree_pile_vide()
assert fusion(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = cree_pile_vide()
qq = cree_pile_vide()
for x in (0, 1, 2, 3):
    empile(p, x)
    empile(pp, x)
for x in (4, 5, 6):
    empile(q, x)
    empile(qq, x)
attendu = cree_pile_vide()
attendu = [3, 6, 2, 5, 1, 4, 0]
assert fusion(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
pp = cree_pile_vide()
qq = cree_pile_vide()
for x in (0, 1):
    empile(p, x)
    empile(pp, x)
for x in (4, 5, 6):
    empile(q, x)
    empile(qq, x)
attendu = cree_pile_vide()
attendu = [1, 6, 0, 5, 4]
assert fusion(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
# --- exo, renverse_k_ --- #
""" # skip
def renverse_k(p, k):
    annexe_1 = ...
    annexe_2 = ...
    for _ in range(k):
        ...(annexe_1, ....)
    for _ in range(k):
        ...(annexe_2, ....)
    for _ in range(k):
        ...
"""  # skip


# --- vide, renverse_k_ --- #
def renverse_k(p, k):
    ...


# --- corr, renverse_k_ --- #
def renverse_k(p, k):
    annexe_1 = cree_pile_vide()
    annexe_2 = cree_pile_vide()
    for _ in range(k):
        empile(annexe_1, depile(p))
    for _ in range(k):
        empile(annexe_2, depile(annexe_1))
    for _ in range(k):
        empile(p, depile(annexe_2))


# --- tests, renverse_k_ --- #
p = cree_pile_vide()
empile(p, "a")
empile(p, "b")
empile(p, "c")
empile(p, "d")
empile(p, "e")
renverse_k(p, 4)
q = cree_pile_vide()
empile(q, "a")
empile(q, "e")
empile(q, "d")
empile(q, "c")
empile(q, "b")
assert p == q


# --- secrets, renverse_k_ --- #
p = cree_pile_vide()
attendu = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5, 6):
    empile(p, x)
    empile(attendu, x)
    empile(r, x)
k = 0
renverse_k(p, k)
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
k = 7
renverse_k(p, k)
attendu = attendu[::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
p = cree_pile_vide()
attendu = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5, 6):
    empile(p, x)
    empile(attendu, x)
    empile(r, x)
k = 5
renverse_k(p, k)
attendu[-5:] = attendu[-5:][::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
