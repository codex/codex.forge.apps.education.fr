# --------- PYODIDE:env --------- #
def cree_pile_vide():
    """Crée et renvoie une pile vide"""
    return []


def est_vide(p):
    """
    Renvoie le booléen répondant à la question :
    la pile p est-elle vide ?
    """
    return p == []


def empile(p, x):
    """Empile x au sommet de p"""
    p.append(x)


def depile(p):
    """
    Dépile et renvoie l'élément au sommet de p
    Provoque une erreur si p est vide
    """
    if est_vide(p):
        raise ValueError("La pile est vide")
    return p.pop()


# --------- PYODIDE:code --------- #
def fusion_simple(p, q):
    r = ...
    while ...:
        ...(r, ...)
        ...
    return r


# --------- PYODIDE:corr --------- #
def fusion_simple(p, q):
    r = cree_pile_vide()
    while not est_vide(p):
        empile(r, depile(p))
        empile(r, depile(q))
    return r


# --------- PYODIDE:tests --------- #
p = cree_pile_vide()
q = cree_pile_vide()
empile(p, "c")
empile(p, "a")
empile(q, "d")
empile(q, "b")
r = fusion_simple(p, q)
a = cree_pile_vide()
empile(a, "a")
empile(a, "b")
empile(a, "c")
empile(a, "d")
assert r == a


# --------- PYODIDE:secrets --------- #
p = cree_pile_vide()
q = cree_pile_vide()
attendu = cree_pile_vide()
assert fusion_simple(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = cree_pile_vide()
qq = cree_pile_vide()
for x in (0, 1, 2):
    empile(p, x)
    empile(pp, x)
for x in (4, 5, 6):
    empile(q, x)
    empile(qq, x)
attendu = cree_pile_vide()
attendu = [2, 6, 1, 5, 0, 4]
assert fusion_simple(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
