# --------- PYODIDE:env --------- #
def cree_pile_vide():
    """Crée et renvoie une pile vide"""
    return []


def est_vide(p):
    """
    Renvoie le booléen répondant à la question :
    la pile p est-elle vide ?
    """
    return p == []


def empile(p, x):
    """Empile x au sommet de p"""
    p.append(x)


def depile(p):
    """
    Dépile et renvoie l'élément au sommet de p
    Provoque une erreur si p est vide
    """
    if est_vide(p):
        raise ValueError("La pile est vide")
    return p.pop()


# --------- PYODIDE:code --------- #
def taille(p):
    ...


# --------- PYODIDE:corr --------- #
def taille(p):
    annexe = cree_pile_vide()
    t = 0
    while not est_vide(p):
        empile(annexe, depile(p))
        t = t + 1
    while not est_vide(annexe):
        empile(p, depile(annexe))
    return t


# --------- PYODIDE:tests --------- #
p = cree_pile_vide()
assert taille(p) == 0
empile(p, "a")
empile(p, "b")
assert taille(p) == 2
q = cree_pile_vide()
empile(q, "a")
empile(q, "b")
assert p == q


# --------- PYODIDE:secrets --------- #
p = cree_pile_vide()
q = cree_pile_vide()
r = cree_pile_vide()
for x in (0, 1, 2, 3, 4, 5):
    empile(p, x)
    empile(q, x)
    empile(r, x)
assert taille(p) == 6
assert p == q, f"Erreur avec la pile {r}"

