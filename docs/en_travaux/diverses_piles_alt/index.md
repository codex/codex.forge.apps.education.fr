---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Diverses piles
difficulty: 250
tags:
    - en travaux
    - pile
maj: 15/10/2024
---

??? note "Un exercice : deux versions"

    Cet exercice sur les piles est proposé en deux versions :
    
    * {{ lien_exo("en programmation orientée objet", "diverses_piles") }} ;
    * en modifiant directement les piles (cet exercice).

On s'intéresse sur cette page au type abstrait de données « *Pile* » que l'on munit des fonctions primitives suivantes :

* `#!py cree_pile_vide()` : crée et renvoie une pile vide ;
* `#!py est_vide(p)` : renvoie le booléen indiquant si la pile `#!py p` passée en paramètre est vide ou non ;
* `#!py empile(p, x)` : empile la valeur `#!py x` au sommet de la pile `#!py p` ;
* `#!py depile(p)` : dépile et renvoie la valeur au sommet de la pile `#!py p`. Provoque une erreur si la pile est vide.

Ces diverses fonctions sont déjà chargées.

Les lignes ci-dessous présentent un exemple d'utilisation. **On prendra soin, dans les différents exercices, de n'utiliser que les fonctions présentées ci-dessous**.

??? tip "Utilisation"

    ```pycon title=""
    >>> p = cree_pile_vide()  # création d'une pile vide
    >>> est_vide(p)           # la pile est-elle vide ? -> Oui
    True
    >>> empile(p, 0)          # empile 0 dans la pile
    >>> est_vide(p)           # la pile est-elle vide ? -> Non
    False
    >>> empile(p, 1)          # empile 1 dans la pile
    >>> p                     # affichage du contenu de p
    [0, 1]
    >>> depile(p)             # dépile une valeur
    1
    >>> p
    [0]
    >>> q = cree_pile_vide()
    >>> empile(q, 1)
    >>> q
    [1]
    >>> p == q                # les piles p et q sont-elles égales ? -> Non
    False
    ```

Les exercices ci-dessous sont indépendants. Ils sont néanmoins présentés dans un certain ordre permettant d'aborder successivement plusieurs techniques.

??? warning "Jouons le jeu !"

    Disons-le dès maintenant : les piles mises en œuvre ici sont des listes python et peuvent donc subir toutes les opérations classiques applicables aux listes.

    **Toutefois**, ces exercices visent à utiliser certaines techniques classiques liées aux piles. Il est donc demandé de les traiter **en se limitant à l'utilisation des fonctions primitives présentées**.
    
??? question "Calculer la taille d'une pile"

    Écrire la fonction `#!py taille` qui prend en paramètre une pile et renvoie son nombre d'éléments.

    A l'issue du traitement, la pile doit avoir retrouvé son état initial.
    
    ```pycon title=""
    >>> p = cree_pile_vide()
    >>> taille(p)
    0
    >>> empile(p, "a")
    >>> empile(p, "b")
    >>> taille(p)
    2
    ```
    
    ??? tip "Aide"
    
        On pourra utiliser une pile annexe.
        
        === "État initial"
            ![Etape 1](images/taille_1_transp.png){.center .autolight width=20%}
        === "1er transfert"
            ![Etape 2](images/taille_2_transp.png){.center .autolight width=20%}
        === "2nd transfert"
            ![Etape 3](images/taille_3_transp.png){.center .autolight width=20%}
        === "Retour à l'état initial"
            ![Etape 4](images/taille_4_transp.png){.center .autolight width=20%}
            
    
    === "Version vide"
        {{ IDE('pythons/exo_taille_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_taille_trous') }}

??? question "Renverser les deux valeurs au sommet"

    Écrire la fonction `#!py renverse_deux` qui prend en paramètre une pile et échange l'ordre des deux valeurs au sommet.

    La transformation se fait en place : on modifie directement sur la pile et il est donc inutile de la renvoyer.

    On garantit que la pile compte au moins deux éléments.
    
    ```pycon title=""
    >>> p = cree_pile_vide()
    >>> empile(p, "a")
    >>> empile(p, "b")
    >>> empile(p, "c")
    >>> p
    ['a', 'b', 'c']
    >>> renverse_deux(p)
    >>> p
    ['a', 'c', 'b']
    ```
    
    === "Version vide"
        {{ IDE('pythons/exo_renverse_deux_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_renverse_deux_trous') }}


??? question "Renverser les k valeurs au sommet"

    Écrire la fonction `#!py renverse_k` qui prend en paramètre une pile ainsi qu'un entier `#!py k` et renverse l'ordre des `#!py k` premiers éléments de la pile. Les autres éléments ne sont pas modifiés.

    La transformation se fait en place : on modifie directement sur la pile et il est donc inutile de la renvoyer.

    On garantit que la pile compte au moins `#!py k` éléments.
    
    
    ```pycon title=""
    >>> p = cree_pile_vide()
    >>> empile(p, "a")
    >>> empile(p, "b")
    >>> empile(p, "c")
    >>> empile(p, "d")
    >>> empile(p, "e")
    >>> p
    ['a', 'b', 'c', 'd', 'e']
    >>> renverse_k(p, 4)
    >>> p
    ['a', 'e', 'd', 'c', 'b']
    ```

    ??? tip "Aide"
    
        On pourra utiliser deux piles annexes.
        
        === "État initial"
            ![Etape 1](images/1_transp.png){.center .autolight width=20%}
        === "Fin de la 1ère phase"
            ![Etape 2](images/2_transp.png){.center .autolight width=20%}
        === "Fin de la 2nde phase"
            ![Etape 3](images/3_transp.png){.center .autolight width=20%}
        === "État final"
            ![Etape 4](images/4_transp.png){.center .autolight width=20%}
            
    
    === "Version vide"
        {{ IDE('pythons/exo_renverse_k_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_renverse_k_trous') }}


??? question "Fusionner deux piles de mêmes tailles"

    On considère `#!py p` et `#!py q` deux piles contenant le même nombre d'éléments.
    
    On appelle « *fusion* de `#!py p` et `#!py q` » la pile obtenue en empilant alternativement les valeurs dépilées de `#!py p` puis `#!py q`.

    Écrire la fonction `#!py fusion_simple` qui prend en paramètre deux piles **de même taille** et renvoie la pile résultante.

    Les piles `#!py p` et `#!py q` peuvent être modifiées lors du traitement.
    
    ```pycon title=""
    >>> p = cree_pile_vide()
    >>> empile(p, "c")
    >>> empile(p, "a")
    >>> p
    ['c', 'a']
    >>> q = cree_pile_vide()
    >>> empile(q, "d")
    >>> empile(q, "b")
    ['d', 'b']
    >>> fusion_simple(p, q)
    ['a', 'b', 'c', 'd']
    ```
    
    === "Version vide"
        {{ IDE('pythons/exo_fusion_simple_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_fusion_simple_trous') }}
        

??? question "Fusionner deux piles de tailles inconnues"

    On considère `#!py p` et `#!py q` deux piles dont on ignore le nombre d'éléments.
    
    On appelle « *fusion* de `#!py p` et `#!py q` » la pile obtenue en empilant alternativement les valeurs dépilées de `#!py p` puis `#!py q`.
    
    Les piles n'ayant pas nécessairement le même nombre d'éléments, il est possible lors de la fusion que l'une d'elles soit vide avant l'autre.
    Dans ce cas, **on terminera la fusion en vidant l'autre pile dans la pile résultante**.

    Écrire la fonction `#!py fusion` qui prend en paramètre deux piles **de tailles inconnues** et renvoie la pile résultante.

    Les piles `#!py p` et `#!py q` peuvent être modifiées lors du traitement.
    
    ```pycon title=""
    >>> p = cree_pile_vide()
    >>> empile(p, "e")
    >>> empile(p, "c")
    >>> empile(p, "a")
    >>> p
    ['e', 'c', 'a']
    >>> q = cree_pile_vide()
    >>> empile(q, "d")
    >>> empile(q, "b")
    ['d', 'b']
    >>> fusion_simple(p, q)
    ['a', 'b', 'c', 'd', 'e']
    ```
    
    ??? tip "Aide"
    
        On pourra organiser le code en trois temps :
        
        * les deux piles sont non-vides ;
        * la pile `#!py p` est non-vide ;
        * la pile `#!py q` est non-vide.
    
    === "Version vide"
        {{ IDE('pythons/exo_fusion_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_fusion_trous') }}