# --------- PYODIDE:ignore --------- #
# pylint: disable=C0103, C0116, C0115, C0410, C0411, C0413, E0401, E0102, C0415, W0611, W0404

# --------- PYODIDE:env --------- #
import p5, js

from lib_draw import AffichageAbCompact

afficheur = AffichageAbCompact(
    est_vide = lambda ab: not ab,
    gauche = lambda ab: ab[1],
    droit  = lambda ab: ab[2],
    valeur = lambda ab: ab[0],
    creer_vide = lambda: None,
    taille_fenetre = (750, 400),
)


# --------- PYODIDE:code --------- #
def est_abr(arbre):
    ...


# --------- PYODIDE:corr --------- #
from math import inf

def est_abr(arbre):

    def interne(mini, ab, maxi):
        if not ab:
            return True
        val, sag, sad = ab
        return mini <= val and val < maxi and interne(mini, sag, val) and interne(val, sad, maxi)

    return interne(-inf, arbre, inf)




# --------- PYODIDE:tests --------- #

ab = None
assert est_abr(ab) is True


ab_1 = (5, None, None)
assert est_abr(ab_1) is True


ab_2 = (5, None,
         (7, None, None))
assert est_abr(ab_2) is True


ab_3 = (5, (7, None, None),
           None)
assert est_abr(ab_3) is False


ab_4 = (1, (0, None,
               (4, None, None)
           ),
           (2, None,
               (3, None, None)
           )
        )
assert est_abr(ab_4) is False


ab_5 = (4, (1, (0, None, None),
               (3, None, None),
           ),
           (5, None,
               (6, None, None),
           )
        )
assert est_abr(ab_5) is True


ab_6 = (4, (1, (0, None, None),
               (3, None, None),
           ),
           (5, (4, None, None),
               (6, None, None),
           )
        )
assert est_abr(ab_6) is True


ab_7 = (4, (1, (0, None, None),
               (3, None, None),
           ),
           (5, (5, None, None),
               (6, None, None),
           )
       )
assert est_abr(ab_7) is False






# --------- PYODIDE:secrets --------- #

@auto_run               # pylint: disable=E0602
async def _():
    # global timer      # To make tries through the terminal


    from math import inf
    from random import choice, randrange as rand
    from collections import Counter

    from lib_tests import build_announcer, timer

    CHECK = False       # Comment out to control from outside
    USE_SLOW = False    # Comment out to control from outside

    DRAW = plt = False
    # DRAW = True ; import matplotlib.pyplot as plt
        # Uncomment to see the matplotlib drawing
        # (import commented out to avoid automatic installation)

    key = "Yepikaïe"
    annonce = build_announcer(key)



    async def test_ab(ab, exp=None):
        if exp is None:
            exp = ref_est_abr(ab)
        act = est_abr(ab)

        if act is not exp or CHECK:
            nope  = 'n\'est pas'
            title = f"Cet arbre { 'est' if exp else nope } un ABR."
            afficheur.changer_arbre(ab,title)
            await js.sleep()
            if CHECK:
                input('resume?')

        assert act is exp
        return exp



    def lin_build(data, l, h):
        """
        Construit un ABR équilibrée à partir d'une liste triée. Si l'élément vaut None,
        renvoie simplement None et interrompt la construction pour ce sous-arbre.
        """
        m = l+h >> 1
        if not data[m]:
            return None
        abr = (
            data[m],
            None if l==m else lin_build(data, l, m-1),
            None if m==h else lin_build(data, m+1, h),
        )
        return abr



    def almost_abr(size=31, tweak=True):
        v = rand(15)
        lst = [ (v:=v+1+rand(5)) for _ in range(size) ]

        profile = rand(4)
        if tweak:
            if profile == 0:
                i,j = rand(size), rand(size)
                lst[j] = lst[i]

            elif profile == 1:
                i = rand(size)
                j = choice( (i-1,)*(i>0) + (i+1,)*(i<30) )
                lst[j] = lst[i]

            elif profile == 2:
                i = rand(size)
                lst[i] += rand(-10,10)

            nullify = {rand(size) for _ in range(rand(7))} - {size-1 >> 1}
            for i in nullify:
                lst[i] = None

        return lin_build(lst, 0, size-1), profile




    def ref_est_abr(arbre):
        def interne(mini, ab, maxi):
            if not ab:
                return True
            v,g,d = ab
            return mini <= v and v < maxi and interne(mini, g, v) and interne(v, d, maxi)

        return interne(-inf, arbre, inf)



    #--------------------------------------------------------
    # Slow ref, from verification_ABR (ratio is x2):

    def minimum_abr(arbre):
        if arbre[1] == None:
            return arbre[0]
        else:
            return minimum_abr(arbre[1])

    def maximum_abr(arbre):
        if arbre[2] == None:
            return arbre[0]
        else:
            return maximum_abr(arbre[2])

    def slow_ref(arbre):
        if arbre == None:
            return True
        v,g,d = arbre
        if g is not None and v <= maximum_abr(g):
            return False
        elif d is not None and v > minimum_abr(d):
            return False
        else:
            return slow_ref(g) and slow_ref(d)



    # Second ref, to compare the impact of destructuring and pick an acceptable exp_ratio
    # This one is around 1.26x slower than the ref.
    def a_bit_slow_ref(arbre):
        def interne(mini, ab, maxi):
            if not ab:
                return True
            return mini <= ab[0] < maxi and interne(mini, ab[1], ab[0]) and interne(ab[0], ab[2], maxi)

        return interne(-inf, arbre, inf)
    # slow_ref = a_bit_slow_ref


    #--------------------------------------------------------



    async def check_perfs(sizes):

        config_tests = [
            ('Solution de référence',  ref_est_abr),
            ('Solution utilisateur ', (slow_ref if USE_SLOW else est_abr)),
        ]
        abrs = [ almost_abr(size, False)[0] for size in sizes]
        exps = [True] * len(abrs)
        exp_act_perfs = [
            await timer(
                title, func,
                lst_of_calls_args = abrs,
                lst_of_expected   = exps,
                leading_new_line  = not i,
                key = key,
            ) for i, (title, func) in enumerate(config_tests)
        ]

        if DRAW:
            PyodidePlot().target()                                      # pylint: disable=E0602
            for ts in exp_act_perfs:
                plt.plot(sizes, ts)
            plt.show()


        ratios = [b/a for a,b in zip(*exp_act_perfs)]
        ratio_moyen = round( sum(ratios)/(len(ratios) or 1), 3)
        exp_ratio = 1.4
        assert ratio_moyen < exp_ratio, (
            f"Fonction {ratio_moyen}x plus lente que la solution de référence.\n"
            f"L'implantation n'est pas suffisamment efficace (limite acceptée: ratio < {exp_ratio})."
        )

        # pylint: disable-next=E0602
        terminal_message(key, f"  => {ratio_moyen}x plus lent que la fonction de référence : ", new_line=False)



    #--------------------------------------------------------



    async with annonce("Tests fixes"):

        ab = (5, (4, (3, None, None),
                    None),
                (7, (6, None, None),
                    None))
        await test_ab(ab, True)


        ab = (8, (4, (1, None,
                        (7, None, None)),
                        None),
                    None)
        await test_ab(ab, False)


        ab = (0, (-3, None, None),
                (21, None,
                        (110, (42, (1, None, None),
                                None),
                            None)
                    )
                )
        await test_ab(ab, False)


        ab = (4, (2, (0, None,
                         (0, None, None)
                     ),
                     None),
                 (8, (4, None,
                         (4,  None,
                              (4, None, None),
                         ),
                     ),
                     None
                 )
                )
        await test_ab(ab, True)




    async with annonce("Tests aléatoires"):
        stats = [Counter() for _ in range(4)]
        for _ in range(100):
            ab, profile = almost_abr()
            exp = await test_ab(ab)
            stats[profile][exp] += 1

        if CHECK:
            print(stats)



    async with annonce("Tests de performances"):
        sizes = (5000, 15000, 30_000, 80_000, 120_000)
        await check_perfs(sizes)
