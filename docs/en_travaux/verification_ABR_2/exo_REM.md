

La solution voulue ici requiert une implémentation linéaire en temps $O(N)$, ce qui est le cas optimal pour ce type de question (il faut vérifier une fois tous les noeuds de l'arbre passé en argument).

Noter que dans la solution proposée, on teste la validité de l'intervalle sur la valeur en cours avant de faire les appels récursifs, afin de ne faire ceux-ci seulement si la valeur est valide (si elle ne l'ast pas, il est inutile de perdre du temps à faire les appels récursifs : on sait déjà que l'arbre n'est pas un ABR).

Il est intéressant ici de comparer avec les approches proposées dans l'exercice {{ lien_exo("Vérification d'un ABR (1)", 'verification_ABR')}}.

Les premières différences sont :

1. La structure des tuples change : `(v, sag, sad)` ici, contre `(sag, v, sad)` dans la première version. Cette modification ne pose aucune difficulté.
1. Les doublons sont autorisés ici et doivent être validés comme étant à droite, alors que la première version ne considérait pas les doublons.



### Solution utilisant `minimum_abr` et `maximum_abr` { .rem_fake_h3 }

Cette approche est légèrement sous-optimale car elle impose de parcourir plusieurs fois les mêmes noeuds dans l'arbre. Elle ne passe donc pas les tests de performances.

D'un point de vue logique, elle peut cependant être mise à jour pour gérer correctement la validation des doublons en modifiant simplement le bon opérateur de comparaison.


### Pas de solution avec un parcours infixe, ici { .rem_fake_h3 }

On pourrait imaginer reprendre la solution avec le parcours infixe.
<br>Cette approche est également optimale en terme de complexité en temps, si elle est réalisée avec toutes ses étapes linéaires en temps (c'est-à-dire : la réalisation du parcours infixe et la vérification que la liste obtenue est bien triée) :

1. Parcours infixe: $O(N)$ (il ne faut donc pas utiliser de concaténation de listes).
1. Vérifier que la liste est croissante par comparaison des valeurs successives: $O(N)$ (donc, sans utiliser de tri).

Mais cette approche n'est pas utilisable ici car **l'abr autorise les doublons à droite**. Or, avec le parcours infixe, on perd l'information sur la structure interne de l'arbre, et il n'est plus possible de savoir si les doublons sont à droite ou à gauche (ou pire : un mélange des deux). En effet, ces trois arbres donnent exactement le même parcours infixe :

```
        5       5       5
       /       / \       \
      5       5   5       5
     /                     \
    5                       5
```


### Sous-fonction ou arguments par défaut ? { .rem_fake_h3 }


On aurait pu utiliser des paramètres par défaut pour passer les bornes de l'intervalle valide directement à la fonction `est_abr` au lieu d'utiliser une sous-fonction :

```python
def est_abr(arbre, mini=-inf, maxi=inf):
    ...
```

Ces arguments sont cependant liés à la logique de résolution et ne devraient pas faire partie de l'interface accessible à l'utilisateur :

- Du point de vue de l'utilisateur, il n'a pas besoin de connaître l'existence de ces arguments.
- Du point de vue du programmeur, ces arguments optionnels sont un "risque" : un utilisateur pourrait appeler la fonction en choisissant les valeurs passées pour les bornes, au lieu de laisser s'appliquer les valeurs par défaut. Cela veut dire qu'un utilisateur pourrait casser la logique de l'algorithme.

L'utilisation d'une sous-fonction est donc une forme d'encapsulation, comme on peut en parler pour la programmation objet :

- La fonction externe représente l'interface disponible à l'utilisateur et seulement celle-ci.
- La sous-fonction isole la logique de résolution de l'interface externe, et garantit que les contrats logiques seront respectés.