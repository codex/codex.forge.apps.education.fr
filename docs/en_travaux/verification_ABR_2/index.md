---
author:
    - Frédéric Zinelli
hide:
    - navigation
    - toc
title: Vérification d'un ABR (2)
tags:
    - en travaux
    - arbre binaire
    - ABR
difficulty: 250
---

!!! abstract "Arbre binaire"
    On considère des arbres binaires qui sont :

    * soit l’arbre vide identifié par `None` ;
    * soit un nœud, contenant une clé et deux sous-arbres gauche et droit et représenté par un triplet `(valeur, sag, sad)` où valeur est la clé, et sag et sad sont les sous-arbres gauche et droit.

!!! example "Exemple"
    ```mermaid
    graph
    a((4)) --- b((1))
    b --- d((0))
    b --- n5((3))
    a --- c((5))
    c --- e((" "))
    c --- n3((6))
    style e fill:none,stroke:none,color:none
    ```

    Ainsi, l’arbre binaire ci-dessus est créé par le code python suivant :
    ```python title=""
    ab_5 = (4, (1, (0, None, None),
                   (3, None, None),
               ),
               (5, None,
                   (6, None, None),
               )
           )
    ```

!!! abstract "Arbre binaire de recherche"

    On définit un arbre binaire de recherche stockant les **doublons à droite** comme étant un arbre binaire qui respecte les propriétés suivantes :

    * les valeurs du sous-arbre gauche sont toutes *inférieures* à celle de la racine ;
    * les valeurs du sous-arbre droit sont toutes *supérieures ou égales* à celle de la racine.

<br>

???+ example "Exemples"

    1. Ainsi l'arbre précédent est bien un ABR.

    1. L'arbre ci-dessous en est un également, avec une valeur en doublon "à droite" :

        ```mermaid
        graph
        a((4)) --- b((1))
        b --- d((0))
        b --- n5((3))
        a --- c((5))
        c --- e((4))
        c --- n3((6))
        ```

        L’arbre binaire ci-dessus est créé par le code python suivant :
        ```python title=""
        ab_6 = (4, (1, (0, None, None),
                       (3, None, None),
                   ),
                   (5, (4, None, None),
                       (6, None, None),
                   )
               )
        ```


    1. En revanche l'arbre ci-dessous n'en est pas un :

        ```mermaid
        graph
        n1((1)) --- n0((0))
        n0 --- n6((" "))
        n0 --- n5((4))
        n1 --- n2((2))
        n2 --- n4((" "))
        n2 --- n3((3))
        style n4 fill:none,stroke:none,color:none
        style n6 fill:none,stroke:none,color:none
        ```

        Il est défini par le code suivant :
        ```python title=""
        ab_4 = (1, (0, None,
                       (4, None, None),
                    2, None,
                       (3, None, None),
                   )
               )
        ```

    1. Et celui-ci n'en est pas un non plus (le doublon est à gauche) :

        ```mermaid
        graph
        a((4)) --- b((1))
        b --- d((0))
        b --- n5((3))
        a --- c((5))
        c --- e((5))
        c --- n3((6))
        ```

        L’arbre binaire ci-dessus est créé par le code python suivant :
        ```python title=""
        ab_7 = (4, (1, (0, None, None),
                       (3, None, None),
                   ),
                   (5, (5, None, None),
                       (6, None, None),
                   )
               )
        ```

<br>

---

<br>


On souhaite écrire la fonction `est_abr` qui prend en paramètre un arbre binaire `arbre`, et renvoie `True` si l'arbre est un arbre binaire de recherche (avec les éventuels doublons à droite) et `False` sinon.

L'implémentation doit être optimale et le comportement doit donc être linéaire en nombre de valeurs dans l'arbre, $O(N)$.


??? "Indices logiques"

    La valeur portée par la racine constitue une limite pour les valeurs envisageables pour ses deux enfants.

    ??? "Indice 2"

        Imaginons que l'on sache que la racine en cours peut prendre n'importe quelle valeur comprise entre $L$ et $R$.

        Si on nomme $V$ la valeur de la racine, que peut-on dire de l'intervalle des valeurs possibles pour l'enfant de gauche ? Et lm'enfant droit ?

        ??? "Indice 3"

            Ne pas oublier qu'il peut y avoir des doublons ! Sur la droite !

            ??? "Indice 4"

                Un des problèmes essentiels à résoudre est de trouver comment "démarrer".
                <br>Quelles sont les valeurs possibles au tout début, lorsqu'on est sur la racine de l'arbre passé en argument à la fonction `est_abr` ?


                ??? "Indice 5"
                    ... Il y en a une infinité !



??? "Indices techniques"

    Lors d'un appel de fonction, il sera nécessaire d'avoir des informations en plus de l'arbre lui-même. Il peut donc être intéressant de travailler avec une fonction auxiliaire, ou même mieux, une sous fonction. Cette fonction accepterait des arguments en plus de l'arbre à valider : autant que nécessaire pour répondre à la question.

    ??? "Indice B"

        Pour chaque appel, il faut savoir quel est l'arbre à valider, et aussi quelle sont les bornes actuellement valides.

        ??? "Indice C"

            Le module `math` contient différentes fonctions... Et constantes...
            <br>Comme par exemple, `inf`.


{{ IDE('exo', STD_KEY="Yepikaïe") }}

Si une erreur est levée durant les tests secrets, l'arbre ayant provoqué l'erreur est affiché ci-dessous :

{{ figure(admo_title="Dernier arbre testé (en cas d'erreur)") }}