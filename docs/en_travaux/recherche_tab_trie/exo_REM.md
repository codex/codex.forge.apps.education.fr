Pour ce problème, on peut écrire une version itérative ou récursive.

De manière indépendante, on peut aussi travailler avec les indices `i` inclus et `j` **exclu**.
