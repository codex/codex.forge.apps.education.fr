---
license: "by-nc-sa"
author: 
    - Franck Chambon
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Rechercher un élément dans un tableau trié
tags:
    - à trous
    - en travaux
    - récursivité
    - dichotomie
    - ep2
difficulty: 250
---


L'objectif de cet exercice est d'écrire une fonction `indice`

- qui prend en argument :
    - un tableau `valeurs` d'éléments comparables **rangés dans l'ordre croissant**
    - une valeur `cible`, comparable à celles du tableau
- qui renvoie :
    - l'indice de `cible` dans le tableau s'il en fait partie
    - `None` sinon

La fonction `indice` utilisera une fonction `indice_recursive` qui sera récursive et qui prendra les mêmes arguments que `indice`, et en plus `debut` et `fin` qui désigneront les indices pour la recherche : de `debut` **inclus** à `fin` **inclus**.

???+ note "Remarque"

    Le tableau `valeurs` pourra être rempli d'entiers ou rempli de chaines de caractères, sans aucun changement à procéder ; en effet ce sont des éléments comparables entre eux, ordre naturel pour les entiers, ordre lexicographique pour les chaines de caractères.

???+ example "Exemples"

    ```pycon title=""
    >>> nombres = [2, 3, 5, 7, 11, 13, 17]
    >>> indice(nombres, 7)
    3
    >>> indice(nombres, 8) is None
    True
    ```

    ```pycon title=""
    >>> fruits = ["abricot", "kiwi", "mangue", "poire", "pomme"]
    >>> fruits == sorted(fruits)  # le tableau est bien trié
    True
    >>> indice(fruits, "kiwi")
    1
    >>> indice(fruits, "cerise") is None
    True
    ```
    
On garantie que le tableau passé en paramètre a été au préalable trié.

=== "En deux fonctions séparées (EP2)"
    {{ IDE('exo') }}
=== "Avec une fonction interne"
    Dans cette version, la fonction `indice_recursive` est définie à l'intérieur de la fonction `indice`.

    {{ IDE('exo_b') }}