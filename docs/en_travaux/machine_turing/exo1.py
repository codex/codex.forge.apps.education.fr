

# --------- PYODIDE:code --------- #

def machine(table, ruban, i):
    etat = "e_0"
    lu = ruban[i]
    while ...:
        action, etat = table[...]
        if action == ...:  # traitement d'un déplacement vers la gauche
            ...
        elif action == ...:    # traitement d'un déplacement vers la droite
            ...
        else:  # écriture d'un caractère
            ruban[...] = ...
        lu = ...


# --------- PYODIDE:corr --------- #

def machine(table, ruban, i):
    etat = "e_0"
    lu = ruban[i]
    while (etat, lu) in table:
        action, etat = table[(etat, lu)]
        if action == "g":
            i = i - 1
        elif action == "d":
            i = i + 1
        else:
            ruban[i] = action
        lu = ruban[i]

# --------- PYODIDE:tests --------- #

table = {("e_0", "0"): ("d", "e_0"), ("e_0", "1"): ("0", "e_0")}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '0', '0', '0', '0', '0', '0', '_', '_']

# --------- PYODIDE:secrets --------- #

# tests

table = {("e_0", "0"): ("d", "e_0"), ("e_0", "1"): ("0", "e_0")}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '0', '0', '0', '0', '0', '0', '_', '_']

# autres tests

table = {("e_0", "0"): ("1", "e_1"), ("e_0", "1"): ("0", "e_1"),
         ("e_1", "0"): ("d", "e_0"), ("e_1", "1"): ("d", "e_0")}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '0', '1', '0', '0', '1', '0', '_', '_']

table = {("e_0", "0"): ("_", "e_1"), ("e_0", "1"): ("_", "e_1"),
         ("e_1", "_"): ("d", "e_0")}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_']*10

table = {("e_0", "0"): ("d", "e_0"), ("e_0", "1"): ("d", "e_0"),
         ("e_0", "_"): ("0", "e_1")}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '1', '0', '1', '1', '0', '1', '0', '_']

table = {("e_0", "0"): ("d", "e_0"), ("e_0", "1"): ("d", "e_0"),
         ("e_0", "_"): ("g", "e_1"), ("e_1", "0"): ("1", "e_2"),
         ("e_1", "_"): ("1", "e_2"), ("e_1", "1"): ("0", "e_2"),
         ("e_2", "0"): ("g", "e_1"),}
r = ['_', '_', '1', '0', '1', '1', '0', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '1', '0', '1', '1', '1', '0', '_', '_']

r = ['_', '_', '0', '_', '_']
machine(table, r, 2)
assert r == ['_', '_', '1', '_', '_']

r = ['_', '_', '1', '1', '1', '_', '_']
machine(table, r, 2)
assert r == ['_', '1', '0', '0', '0', '_', '_']
