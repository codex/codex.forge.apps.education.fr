Attention, si on ne considère qu'un seul état `"e_0"` avec les instructions:
si la tête lit un `"0"` elle écrit `"_"`, si elle lit un `"1"` elle écrit `"_"` et si elle lit `"_"` elle se déplace vers la droite, en restant toujours dans l'état `"e_0"`; alors la machine efface bien les `"0"` et les `"1"`. 

Mais le problème est que cette machine ne s'arrête pas, la tête continue à se déplacer vers la droite.
