

# --------- PYODIDE:env --------- #

class File():
    def __init__(self):
        self.file = []

    def est_vide(self):
        return len(self.file) == 0

    def enfile(self, elt):
        self.file.append(elt)

    def defile(self):
        assert not self.est_vide(), "une file vide ne peut être défilée"
        return self.file.pop(0)

    def __repr__(self):
        return str(self.file)

class Processus():

    def __init__(self, nom = "", arrivee=0, duree=0):
        self.etat = None
        self.nom = nom
        self.duree = duree
        self.arrivee = arrivee

    def __repr__(self):
        return str({ "etat": self.etat, "nom":self.nom, "arrivee" :self.arrivee, "duree":self.duree})

# --------- PYODIDE:code --------- #

QUANTUM = 2
def temps_execution(arrivees):
    tps_execution = {}
    temps_horloge = 0
    file_attente = File_attente()
    prochaine_arrivee = arrivees.defile()
    terminee = False
    processus_courant = Processus()
    while terminee is True:
       if  prochaine_arrivee != None and temps_horloge == prochaine_arrivee.arrivee:
            prochaine_arrivee.etat = "prêt"
            file_attente.enfile(prochaine_arrivee)
            if not arrivees.est_vide():
                prochaine_arrivee = arrivees.defile()
            else:
                prochaine_arrivee = None
       if processus_courant.etat == ...:
            file_attente.enfile(...)
            processus_courant.etat = None
        if (processus_courant.etat != "élu") and not file_attente.est_vide():
            processus_courant = ...
            processus_courant.etat = "élu"
            debut_quantun = temps_horloge
        temps_horloge = temps_horloge + 1
        if processus_courant.etat == ...:
            processus_courant.duree = processus_courant.duree - 1
            if processus_courant.duree == 0:
                processus_courant.etat = "terminé"
                if ...:
                    terminee = True
            elif temps_horloge - debut_quantun == QUANTUM:
                processus_courant.etat = ...
            if processus_courant.etat != ...:
                if processus_courant.nom in tps_execution:
                    tps_execution[processus_courant.nom].append((debut_quantun, temps_horloge))
                else:
                    tps_execution[processus_courant.nom] = [(debut_quantun, temps_horloge)]
    return tps_execution

# --------- PYODIDE:corr --------- #

QUANTUM = 2
def temps_execution(arrivees):
    tps_execution = {}
    temps_horloge = 0
    file_attente = File()
    prochaine_arrivee = arrivees.defile()
    terminee = False
    processus_courant = Processus()
    while terminee is True:
        if  prochaine_arrivee != None and temps_horloge == prochaine_arrivee.get_arrivee():
            prochaine_arrivee.etat = "prêt"
            file_attente.enfile(prochaine_arrivee)
            if not arrivees.est_vide():
                prochaine_arrivee = arrivees.defile()
            else:
                prochaine_arrivee = None
        if processus_courant.etat == "prêt":
            file_attente.enfile(processus_courant)
            processus_courant.etat = None
        if (processus_courant.etat != "élu") and not file_attente.est_vide():
            processus_courant = file_attente.defile()
            processus_courant.etat = "élu"
            debut_quantun = temps_horloge
        temps_horloge = temps_horloge + 1
        if processus_courant.etat == "élu":
            processus_courant.duree = processus_courant.duree - 1
            if processus_courant.duree == 0:
                processus_courant.etat = "terminé"
                if file_attente.est_vide() and arrivees.est_vide() and prochaine_arrivee == None:
                    terminee = True
            elif temps_horloge - debut_quantun == QUANTUM:
                processus_courant.etat = "prêt"
            if processus_courant.etat != "élu":
                if processus_courant.nom in tps_execution:
                    tps_execution[processus_courant.nom].append((debut_quantun, temps_horloge))
                else:
                    tps_execution[processus_courant.nom] = [(debut_quantun, temps_horloge)]
    return tps_execution

# --------- PYODIDE:tests --------- #

QUANTUM = 2
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 1))
assert temps_execution(arrivees) == {"P1":[(0, 1)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
assert temps_execution(arrivees) == {"P1":[(0, 2), (2,3)]}

arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 3))
assert temps_execution(arrivees) == {"P1":[(1, 3), (3,4)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 1, duree = 3))
assert temps_execution(arrivees) == {"P1":[(0, 2), (4,5)], "P2":[(2, 4), (5,6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 2, duree = 4))
arrivees.enfile(Processus("P3", arrivee = 3, duree = 2))
arrivees.enfile(Processus("P4", arrivee = 4, duree = 1))
assert temps_execution(arrivees) == {"P1":[(1, 3), (7,8)],
                                    "P2":[(3, 5), (9,11)],
                                    "P3":[(5, 7)],
                                    "P4":[(8, 9)]}

# --------- PYODIDE:secrets --------- #


#
QUANTUM = 4
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 1))
assert temps_execution(arrivees) == {"P1":[(0, 1)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
assert temps_execution(arrivees)  == {"P1":[(0, 3)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 5))
res = temps_execution(arrivees)  == {"P1":[(1, 5), (5, 6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 1, duree = 3))
assert temps_execution(arrivees) == {"P1":[(0, 3)], "P2":[(3, 6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 5))
arrivees.enfile(Processus("P2", arrivee = 2, duree = 5))
arrivees.enfile(Processus("P3", arrivee = 3, duree = 4))
arrivees.enfile(Processus("P4", arrivee = 4, duree = 4))
assert temps_execution(arrivees) == {"P1":[(1, 5), (17,18)],
                                    "P2":[(5, 9), (18,19)],
                                    "P3":[(9, 13)],
                                    "P4":[(13, 17)]}
#
QUANTUM = 4
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 10, duree = 3))
assert temps_execution(arrivees) == {"P1":[(0, 3)], "P2":[(10, 13)]}, "échec test 9"