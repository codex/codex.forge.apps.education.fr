
# --------- PYODIDE:env --------- #

class File():
    def __init__(self):
        self.file = []

    def est_vide(self):
        return len(self.file) == 0

    def enfile(self, elt):
        self.file.append(elt)

    def defile(self):
        assert not self.est_vide(), "une file vide ne peut être défilée"
        return self.file.pop(0)
    
    def tete(self):
        return self.file[0]

    def __repr__(self):
        return str(self.file)

class Processus():

    def __init__(self, nom, duree, arrivee=0):
        self.etat = None
        self.nom = nom
        self.duree = duree
        self.arrivee = arrivee
        
    def elit(self, quantum):
        self.temps_alloue = quantum
        self.etat = "élu"
        
    def execute(self):
        self.duree = self.duree - 1
        self.temps_alloue = self.temps_alloue - 1
        if self.duree == 0:
            self.etat = "terminé"
        elif self.temps_alloue == 0:
            self.etat = "prêt"
            
    def est_pret(self, horloge):
        if self.arrivee == horloge:
            self.etat = "prêt"
        return self.etat == "prêt"
    
    def bloque(self):
        self.etat = "bloqué"
        
    def debloque(self):
        self.etat = "prêt"            

    def __repr__(self):
        return str({ "etat": self.etat, "nom":self.nom, "arrivee" :self.arrivee, "duree":self.duree})

# --------- PYODIDE:code --------- #
QUANTUM = 2
def temps_execution(arrivees, quantum):
    tps_execution = {}
    temps_horloge = 0
    file_attente = File()
    terminee = False
    processus_courant = None
    while not (arrivees.est_vide() and file_attente.est_vide() and processus_courant == None):
        
        if  not arrivees.est_vide() and arrivees.tete().est_pret(temps_horloge):
            file_attente.enfile(...)
            
        if processus_courant != None and processus_courant.etat == "prêt":
                file_attente.enfile(...)
                processus_courant = ...
                    
        if processus_courant == None and not file_attente.est_vide():
            processus_courant = ...
            processus_courant.elit(quantum)
            debut_quantun = temps_horloge 
        
        temps_horloge = temps_horloge + 1
        if processus_courant != None:
            processus_courant.execute()
            if processus_courant.etat != "élu":
                if processus_courant.nom in tps_execution:
                    tps_execution[processus_courant.nom].append(...)
                else:
                    tps_execution[processus_courant.nom] = [...]
                if processus_courant.etat == "terminé":
                    processus_courant = None

        
    return tps_execution

# --------- PYODIDE:corr --------- #

QUANTUM = 2
def temps_execution(arrivees, quantum):
    tps_execution = {}
    temps_horloge = 0
    file_attente = File()
    terminee = False
    processus_courant = None
    while not (arrivees.est_vide() and file_attente.est_vide() and processus_courant == None):
        
        if  not arrivees.est_vide() and arrivees.tete().est_pret(temps_horloge):
            file_attente.enfile(arrivees.defile())
            
        if processus_courant != None and processus_courant.etat == "prêt":
                file_attente.enfile(processus_courant)
                processus_courant = None
                    
        if processus_courant == None and not file_attente.est_vide():
            processus_courant = file_attente.defile()
            processus_courant.elit(quantum)
            debut_quantun = temps_horloge 
        
        temps_horloge = temps_horloge + 1
        if processus_courant != None:
            processus_courant.execute()
            if processus_courant.etat != "élu":
                if processus_courant.nom in tps_execution:
                    tps_execution[processus_courant.nom].append((debut_quantun, temps_horloge))
                else:
                    tps_execution[processus_courant.nom] = [(debut_quantun, temps_horloge)]
                if processus_courant.etat == "terminé":
                    processus_courant = None

        
    return tps_execution

# --------- PYODIDE:tests --------- #

QUANTUM = 2
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 1))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 1)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 2), (2,3)]}

arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 3))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(1, 3), (3,4)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 1, duree = 3))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 2), (4,5)], "P2":[(2, 4), (5,6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 2, duree = 4))
arrivees.enfile(Processus("P3", arrivee = 3, duree = 2))
arrivees.enfile(Processus("P4", arrivee = 4, duree = 1))

assert temps_execution(arrivees,QUANTUM) == {"P1":[(1, 3), (7,8)],
                                    "P2":[(3, 5), (9,11)],
                                    "P3":[(5, 7)],
                                    "P4":[(8, 9)]}

# --------- PYODIDE:secrets --------- #


#
QUANTUM = 4
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 1))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 1)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
assert temps_execution(arrivees, QUANTUM)  == {"P1":[(0, 3)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 5))
res = temps_execution(arrivees, QUANTUM)  == {"P1":[(1, 5), (5, 6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 1, duree = 3))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 3)], "P2":[(3, 6)]}
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 1, duree = 5))
arrivees.enfile(Processus("P2", arrivee = 2, duree = 5))
arrivees.enfile(Processus("P3", arrivee = 3, duree = 4))
arrivees.enfile(Processus("P4", arrivee = 4, duree = 4))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(1, 5), (17,18)],
                                    "P2":[(5, 9), (18,19)],
                                    "P3":[(9, 13)],
                                    "P4":[(13, 17)]}
#
QUANTUM = 4
arrivees = File()
arrivees.enfile(Processus("P1", arrivee = 0, duree = 3))
arrivees.enfile(Processus("P2", arrivee = 10, duree = 3))
assert temps_execution(arrivees, QUANTUM) == {"P1":[(0, 3)], "P2":[(10, 13)]}, "échec test 9"

