

# --------- PYODIDE:code --------- #
premiere_pile = [42, 15, 64] 
deuxieme_pile = [22, 57, 36] 
troisieme_pile = [22, 57, 36] 

#Empiler l'élément 21 à la première pile 
... 

#Dépiler un élément à la première pile 
element_depile_1 = ... 

#Dépiler un nouvel élément à la première pile 
element_depile_2 = ... 

#Empiler l'élément 99 à la première pile 
... 

#Dépiler un élément de la première pile 
#et l'empiler dans la deuxième pile 
... 

#Dépiler un élément de la deuxième pile 
#et l'empiler dans la troisième pile 
... 

#Dépiler un élément de la troisième pile 
#et l'empiler dans la première pile 
...

# --------- PYODIDE:corr --------- #

premiere_pile = [42, 15, 64] 
deuxieme_pile = [22, 57, 36] 
troiseme_pile = [22, 57, 36] 

#Empiler l'élément 21 à la pile
premiere_pile.append(21)

#Dépiler un élément
element_depile_1 = premiere_pile.pop()

#Dépiler un nouvel élément
element_depile_2 = premiere_pile.pop()

#Empiler l'élément 99 à la première pile
premiere_pile.append(99)

#Dépiler un élément de la première pile
#et l'empiler dans la deuxième pile
element = premiere_pile.pop()
deuxieme_pile.append(element)

#Dépiler un élément de la deuxième pile
#et l'empiler dans la troisième pile
element = deuxieme_pile.pop()
troisieme_pile.append(element)

#Dépiler un élément de la troisième pile
#et l'empiler dans la première pile
element = troisieme_pile.pop()
premiere_pile.append(element)



# --------- PYODIDE:secrets --------- #

# tests
valide = True
terminal_message("1234","Première pile : ", new_line=False)
if premiere_pile == [42, 15, 99]:
    terminal_message("1234","\u2705 succès","success")
else:
    terminal_message("1234","\u274C échec","error")
    valide = False

terminal_message("1234","Deuxième pile : ", new_line=False)
if deuxieme_pile == [22, 57, 36]:
    terminal_message("1234","\u2705 succès","success")
else:
    terminal_message("1234","\u274C échec","error")
    valide = False

terminal_message("1234","Troisième pile : ", new_line=False)
if troisieme_pile == [22, 57, 36]:
    terminal_message("1234","\u2705 succès","success")
else:
    terminal_message("1234","\u274C échec","error")
    valide = False

terminal_message("1234","Première élément : ", new_line=False)
if element_depile_1 == 21:
    terminal_message("1234","\u2705 succès","success")
else:
    terminal_message("1234","\u274C échec","error")
    valide = False

terminal_message("1234","Deuxième élément : ", new_line=False)
if element_depile_2 == 64:
    terminal_message("1234","\u2705 succès","success")
else:
    terminal_message("1234","\u274C échec","error")
    valide = False

assert valide is True,"Le code n'est pas valide !"
