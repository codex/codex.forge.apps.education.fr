Les variables de type `#!py list` sont gérées en mémoires comme des tableaux dynamiques :

* ajouter ou retirer à la fin d'un tableau (respectivement grâce aux méthodes `#!py append` et `#!py pop`) s'effectue sur une zone contiguë en mémoire et se fait donc à coût constant.

Le type `#!py list` permet donc de créer facilement des piles en conservant des algorithmes efficaces.
