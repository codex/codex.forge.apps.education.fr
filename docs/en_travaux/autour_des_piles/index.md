---
author: Pierre Marquestaut
title: Autour des piles
hide:
    - navigation
    - toc
tags:
    - pile
    - structure linéaire
    - en travaux
difficulty: 20
---


Les piles sont des structures linéaires de données dont les éléments ne peuvent être ajoutés ou retirés qu'à partir d'une même extrémité, que l'on nomme aussi le **sommet** de la pile.

![liste](pile.svg){width=35% .center .autolight}

Ce fonctionnement est appelé **DEPS** (*Dernier Entré, Premier Sorti*),  **LIFO** en anglais (*Last In, First Out*).

C'est le principe de la pile d'assiettes : on pose les assiettes sur les assiettes précedentes, et quand on prend une assiette, on la prend sur le dessus.

Il est possible d'effectuer les deux actions suivantes sur des piles :

* **empiler** : ajouter un élément au sommet de la pile
* **dépiler** : retirer un élément au sommet de la pile


On s'intéresse ici à une implantation rudimentaire des piles en Python à l'aide de listes.
Ce type dispose de méthodes pour ajouter ou supprimer des éléments.



???+ note "Empiler un élément"

    La méthode `#!py append` permet de mettre en œuvre l'action d'empiler une valeur :

    ```python
    >>> pile = [1, 2, 3]
    >>> pile.append(4)  # empile la valeur 4
    >>> pile
    [1, 2, 3, 4]
    ```


???+ note "Dépiler un élément"

    La méthode `#!py pop` et en particulier l'instruction `#!py pile.pop()` permet de mettre en œuvre l'action de dépiler un élément du sommet de la pile et de renvoyer sa valeur :

    ```python
    >>> pile = ["un", "deux", "trois", "quatre"]
    >>> pile.pop()  # dépile un élément
    'quatre'
    >>> pile
    ["un", 'deux', 'trois']
    >>> pile.pop()  # dépile un élément
    'trois'
    >>> pile
     ["un", 'deux']
    ```

Compléter le code ci-dessous :

{{ IDE('exo',STD_KEY="1234") }}