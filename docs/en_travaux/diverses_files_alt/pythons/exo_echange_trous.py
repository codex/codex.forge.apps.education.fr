# --------- PYODIDE:env --------- #
from collections import deque

class Deque(deque):
    def __repr__(self):
        return f"{list(self)} <- tête"
    
    def __eq__(self, autre):
        return list(self) == list(autre)

def cree_file_vide():
    """Renvoie une file vide"""
    return Deque()


def enfile(f, x):
    """Enfile x dans f"""
    f.appendleft(x)


def defile(f):
    "Défile f. Génère une erreur si f est vide"
    return f.pop()


def est_vide(f):
    """Renvoie le booléen indiquant si la file est vide"""
    return len(f) == 0

# --------- PYODIDE:code --------- #
def echange(f):
    un = ...
    deux = ...
    g = ...
    while ...:
        ...
    enfile(..., ...)
    ...
    while ...:
        ...
# --------- PYODIDE:corr --------- #
def echange(f):
    un = defile(f)
    deux = defile(f)
    g = cree_file_vide()
    while not est_vide(f):
        enfile(g, defile(f))
    enfile(f, deux)
    enfile(f, un)
    while not est_vide(g):
        enfile(f, defile(g))
# --------- PYODIDE:tests --------- #
f = cree_file_vide()
enfile(f, "a")
enfile(f, "b")
enfile(f, "c")
echange(f)
assert str(f) == "['c', 'a', 'b'] <- tête"
# --------- PYODIDE:secrets --------- #
f = cree_file_vide()
for x in range(5):
    enfile(f, x)
echange(f)
etat_initial = f.copy()
attendu = [4, 3, 2, 0, 1]
assert f == attendu, f"Erreur avec f = {etat_initial}"

f = cree_file_vide()
for x in "ab":
    enfile(f, x)
echange(f)
etat_initial = f.copy()
attendu = ["a", "b"]
assert f == attendu, f"Erreur avec f = {etat_initial}"
