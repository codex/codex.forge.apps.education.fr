# --------- PYODIDE:env --------- #
from collections import deque

class Deque(deque):
    def __repr__(self):
        return f"{list(self)} <- tête"
    
    def __eq__(self, autre):
        return list(self) == list(autre)

def cree_file_vide():
    """Renvoie une file vide"""
    return Deque()


def enfile(f, x):
    """Enfile x dans f"""
    f.appendleft(x)


def defile(f):
    "Défile f. Génère une erreur si f est vide"
    return f.pop()


def est_vide(f):
    """Renvoie le booléen indiquant si la file est vide"""
    return len(f) == 0

