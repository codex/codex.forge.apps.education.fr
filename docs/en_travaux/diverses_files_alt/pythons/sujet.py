# --- hdr, , taille_, echange_, dernier_ --- #
from collections import deque

class Deque(deque):
    def __repr__(self):
        return f"{list(self)} <- tête"
    
    def __eq__(self, autre):
        return list(self) == list(autre)

def cree_file_vide():
    """Renvoie une file vide"""
    return Deque()


def enfile(f, x):
    """Enfile x dans f"""
    f.appendleft(x)


def defile(f):
    "Défile f. Génère une erreur si f est vide"
    return f.pop()


def est_vide(f):
    """Renvoie le booléen indiquant si la file est vide"""
    return len(f) == 0

# --- exo, taille_ --- #
def taille(f):
    g = ...
    t = ...
    while not ...:
        ...
        ...
    while ...:
        ...
    return ...
# --- vide, taille_ --- #
def taille(f):
    ...
# --- corr, taille_ --- #
def taille(f):
    g = cree_file_vide()
    t = 0
    while not est_vide(f):
        t += 1
        enfile(g, defile(f))
    while not est_vide(g):
        enfile(f, defile(g))
    return t
# --- tests, taille_ --- #
f = cree_file_vide()
assert taille(f) == 0
enfile(f, "a")
enfile(f, "b")
assert taille(f) == 2
# --- secrets, taille_ --- #
n = 20
f = cree_file_vide()
assert taille(f) == 0
for i in range(1, n + 1):
    enfile(f, 3 * i + 9)
    etat_initial = [3 * k + 9 for k in range(i, 0, -1)]
    attendu = i
    assert taille(f) == attendu, f"Erreur avec f = {etat_initial}"
    assert list(f) == etat_initial, f"La file a été modifiée"
for i in range(n - 1, -1, -1):
    defile(f)
    etat_initial = [3 * k + 9 for k in range(n, n - i, -1)]
    attendu = i
    assert taille(f) == attendu, f"Erreur avec f = {etat_initial}"
    assert list(f) == etat_initial, f"La file a été modifiée"

# --- exo, echange_ --- #
def echange(f):
    un = ...
    deux = ...
    g = ...
    while ...:
        ...
    enfile(..., ...)
    ...
    while ...:
        ...
# --- vide, echange_ --- #
def echange(f):
    ...
# --- corr, echange_ --- #
def echange(f):
    un = defile(f)
    deux = defile(f)
    g = cree_file_vide()
    while not est_vide(f):
        enfile(g, defile(f))
    enfile(f, deux)
    enfile(f, un)
    while not est_vide(g):
        enfile(f, defile(g))
# --- tests, echange_ --- #
f = cree_file_vide()
enfile(f, "a")
enfile(f, "b")
enfile(f, "c")
echange(f)
assert str(f) == "['c', 'a', 'b'] <- tête"
# --- secrets, echange_ --- #
f = cree_file_vide()
for x in range(5):
    enfile(f, x)
echange(f)
etat_initial = f.copy()
attendu = [4, 3, 2, 0, 1]
assert f == attendu, f"Erreur avec f = {etat_initial}"

f = cree_file_vide()
for x in "ab":
    enfile(f, x)
echange(f)
etat_initial = f.copy()
attendu = ["a", "b"]
assert f == attendu, f"Erreur avec f = {etat_initial}"
# --- exo, dernier_ --- #
def dernier(f):
    g = ...
    elt = ...
    while ...:
        enfile(..., ...)
        elt = ...
    while ...:
        ...
    enfile(..., ...)
    return ...
# --- vide, dernier_ --- #
def dernier(f):
    ...
# --- corr, dernier_ --- #
def dernier(f):
    g = cree_file_vide()
    elt = defile(f)
    while not est_vide(f):
        enfile(g, elt)
        elt = defile(f)
    while not est_vide(g):
        enfile(f, defile(g))
    enfile(f, elt)
    return elt
# --- tests, dernier_ --- #
f = cree_file_vide()
enfile(f, "a")
enfile(f, "b")
enfile(f, "c")
assert dernier(f) == 'c'
assert str(f) == "['c', 'b', 'a'] <- tête"
# --- secrets, dernier_ --- #
f = cree_file_vide()
for x in range(5):
    enfile(f, x)
attendu = 4
etat_initial = [4, 3, 2, 1, 0]
assert dernier(f) == attendu, f"Erreur avec f = {etat_initial}"
assert f == etat_initial, f"La file a été modifiée"

f = cree_file_vide()
enfile(f, "toto")
attendu = "toto"
etat_initial = ["toto"]
assert dernier(f) == attendu, f"Erreur avec f = {etat_initial}"
assert f == etat_initial, f"La file a été modifiée"