# --------- PYODIDE:env --------- #
from collections import deque

class Deque(deque):
    def __repr__(self):
        return f"{list(self)} <- tête"
    
    def __eq__(self, autre):
        return list(self) == list(autre)

def cree_file_vide():
    """Renvoie une file vide"""
    return Deque()


def enfile(f, x):
    """Enfile x dans f"""
    f.appendleft(x)


def defile(f):
    "Défile f. Génère une erreur si f est vide"
    return f.pop()


def est_vide(f):
    """Renvoie le booléen indiquant si la file est vide"""
    return len(f) == 0

# --------- PYODIDE:code --------- #
def dernier(f):
    g = ...
    elt = ...
    while ...:
        enfile(..., ...)
        elt = ...
    while ...:
        ...
    enfile(..., ...)
    return ...
# --------- PYODIDE:corr --------- #
def dernier(f):
    g = cree_file_vide()
    elt = defile(f)
    while not est_vide(f):
        enfile(g, elt)
        elt = defile(f)
    while not est_vide(g):
        enfile(f, defile(g))
    enfile(f, elt)
    return elt
# --------- PYODIDE:tests --------- #
f = cree_file_vide()
enfile(f, "a")
enfile(f, "b")
enfile(f, "c")
assert dernier(f) == 'c'
assert str(f) == "['c', 'b', 'a'] <- tête"
# --------- PYODIDE:secrets --------- #
f = cree_file_vide()
for x in range(5):
    enfile(f, x)
attendu = 4
etat_initial = [4, 3, 2, 1, 0]
assert dernier(f) == attendu, f"Erreur avec f = {etat_initial}"
assert f == etat_initial, f"La file a été modifiée"

f = cree_file_vide()
enfile(f, "toto")
attendu = "toto"
etat_initial = ["toto"]
assert dernier(f) == attendu, f"Erreur avec f = {etat_initial}"
assert f == etat_initial, f"La file a été modifiée"