# --------- PYODIDE:env --------- #
from collections import deque

class Deque(deque):
    def __repr__(self):
        return f"{list(self)} <- tête"
    
    def __eq__(self, autre):
        return list(self) == list(autre)

def cree_file_vide():
    """Renvoie une file vide"""
    return Deque()


def enfile(f, x):
    """Enfile x dans f"""
    f.appendleft(x)


def defile(f):
    "Défile f. Génère une erreur si f est vide"
    return f.pop()


def est_vide(f):
    """Renvoie le booléen indiquant si la file est vide"""
    return len(f) == 0

# --------- PYODIDE:code --------- #
def taille(f):
    g = ...
    t = ...
    while not ...:
        ...
        ...
    while ...:
        ...
    return ...
# --------- PYODIDE:corr --------- #
def taille(f):
    g = cree_file_vide()
    t = 0
    while not est_vide(f):
        t += 1
        enfile(g, defile(f))
    while not est_vide(g):
        enfile(f, defile(g))
    return t
# --------- PYODIDE:tests --------- #
f = cree_file_vide()
assert taille(f) == 0
enfile(f, "a")
enfile(f, "b")
assert taille(f) == 2
# --------- PYODIDE:secrets --------- #
n = 20
f = cree_file_vide()
assert taille(f) == 0
for i in range(1, n + 1):
    enfile(f, 3 * i + 9)
    etat_initial = [3 * k + 9 for k in range(i, 0, -1)]
    attendu = i
    assert taille(f) == attendu, f"Erreur avec f = {etat_initial}"
    assert list(f) == etat_initial, f"La file a été modifiée"
for i in range(n - 1, -1, -1):
    defile(f)
    etat_initial = [3 * k + 9 for k in range(n, n - i, -1)]
    attendu = i
    assert taille(f) == attendu, f"Erreur avec f = {etat_initial}"
    assert list(f) == etat_initial, f"La file a été modifiée"

