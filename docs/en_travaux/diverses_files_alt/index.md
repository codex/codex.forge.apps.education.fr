---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Diverses files
difficulty: 250
tags:
    - en travaux
    - file
maj: 14/11/2024
---

On s'intéresse sur cette page au type abstrait de données « *File* » que l'on munit des fonctions primitives suivantes :

* `#!py cree_file_vide()` : crée et renvoie une file vide ;
* `#!py est_vide(f)` : renvoie le booléen indiquant si la file `#!py f` passée en paramètre est vide ou non ;
* `#!py enfile(f, x)` : enfile la valeur `#!py x` à la queue de la file `#!py f` ;
* `#!py defile(f)` : défile et renvoie la valeur à la tête de la file `#!py f`. Provoque une erreur si la file est vide.

Ces diverses fonctions sont déjà chargées.

Les lignes ci-dessous présentent un exemple d'utilisation. **On prendra soin, dans les différents exercices, de n'utiliser que les fonctions présentées ci-dessous**.

??? tip "Utilisation"

    ```pycon title=""
    >>> f = cree_file_vide()  # création d'une file vide
    >>> est_vide(f)           # la file est-elle vide ? -> Oui
    True
    >>> enfile(f, 0)          # enfile 0 dans la file
    >>> est_vide(f)           # la file est-elle vide ? -> Non
    False
    >>> enfile(f, 1)          # enfile 1 dans la file
    >>> f                     # affichage du contenu de f
    [1, 0] <- tête
    >>> defile(f)             # défile une valeur
    1
    >>> f
    [1] <- tête
    >>> q = cree_file_vide()
    >>> enfile(q, 1)
    >>> q
    [1] <- tête
    >>> f == q                # les files f et q sont-elles égales ? -> Oui
    True
    ```

Les exercices ci-dessous sont indépendants. Ils sont néanmoins présentés dans un certain ordre permettant d'aborder successivement plusieurs techniques.

??? warning "Jouons le jeu !"

    Disons-le dès maintenant : les files mises en œuvre ici utilisent la classe `#!py deque` du [module `#!py collections`](https://docs.python.org/fr/3/library/collections.html#collections.deque) et peuvent donc subir toutes les opérations classiques applicables à ces objets.

    **Toutefois**, ces exercices visent à utiliser certaines techniques classiques liées aux files. Il est donc demandé de les traiter **en se limitant à l'utilisation des fonctions primitives présentées**.
    
??? question "Calculer la taille d'une file"

    Écrire la fonction `#!py taille` qui prend en paramètre une file et renvoie son nombre d'éléments.

    A l'issue du traitement, la file doit avoir retrouvé son état initial.
    
    ```pycon title=""
    >>> f = cree_file_vide()
    >>> taille(f)
    0
    >>> enfile(f, "a")
    >>> enfile(f, "b")
    >>> taille(f)
    2
    ```
    
    ??? tip "Aide"
    
        On pourra utiliser un compteur et une file annexe.
        
    === "Version vide"
        {{ IDE('pythons/exo_taille_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_taille_trous') }}

??? question "Échanger les deux valeurs de tête"

    Écrire la fonction `#!py echange` qui prend en paramètre une file **contenant au moins deux éléments** et échange l'ordre des deux valeurs de tête.

    La transformation se fait en place : on modifie directement sur la file et il est donc inutile de la renvoyer.

    ```pycon title=""
    >>> f = cree_file_vide()
    >>> enfile(f, "a")
    >>> enfile(f, "b")
    >>> enfile(f, "c")
    >>> f
    ['c', 'b', 'a'] <- tête
    >>> renverse_deux(f)
    >>> f
    ['c', 'a', 'b'] <- tête
    ```
    
    === "Version vide"
        {{ IDE('pythons/exo_echange_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_echange_trous') }}


??? question "Récupérer la dernière valeur de la file"

    Écrire la fonction `#!py dernier` qui prend en paramètre une file **contenant au moins un élément** et renvoie la valeur de l'élément en queue de file.

    Attention, l'élément renvoyé ne doit pas être défilé. Ainsi, à l'issue du traitement, la file doit avoir retrouvé son état initial.
    
    ```pycon title=""
    >>> f = cree_file_vide()
    >>> enfile(f, "a")
    >>> enfile(f, "b")
    >>> enfile(f, "c")
    >>> f
    ['c', 'b', 'a'] <- tête
    >>> dernier(f)
    'c'
    >>> f
    ['c', 'b', 'a'] <- tête
    ```

    ??? tip "Aide"
    
        On pourra défiler un élément **avant** de rentrer dans la boucle classique.        
    
    === "Version vide"
        {{ IDE('pythons/exo_dernier_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_dernier_trous') }}
        