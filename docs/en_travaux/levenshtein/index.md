---
author: Vincent-Xavier Jumel
hide:
    - navigation
    - toc
title: Distance de Levenshtein
tags:
    - en travaux
    - récursivité
difficulty: 330
---
## Présentation du problème

Lors de la comparaison de chaines de caractères, on cherche parfois des
chaines plus ou moins proches les unes des autres. En particulier, on peut
définir d'un point de vue mathématique trois opérations élémentaires permettant
de passer d'une chaine à une autre :

+ substituer un caractère : `#!py "ABC" -> "AAC"` ;
+ insérer un caractère : `#!py "ABC" -> "ABDC"` ;
+ supprimer un caractère : `#!py "ABC" -> "AC"`.

On va supposer ici que le coût de ces opérations est le même. Le nombre
d'opérations élementaires pour passer d'une chaine à une autre constitue la
**distance de Levenshtein**.

On peut utiliser la formule récursive suivante, où on note $\|a\|$ la
longueur de la chaine $a$, et $a-1$  la chaîne $a$ tronquée de sa première lettre :

$$\qquad\operatorname{lev}(a,b) = 
\begin{cases}
  \max(\|a\|,\|b\|) & \text{ si } \min(\|a\|,\|b\|)=0, \\
  \operatorname{lev}(a-1,b-1) & \text{ si } a[0]=b[0], \\
  1 + \min \begin{cases}
          \operatorname{lev}(a-1,b)\\
          \operatorname{lev}(a,b-1)\\
          \operatorname{lev}(a-1,b-1)
       \end{cases} & \text{ sinon.}
\end{cases}
$$

L'objectif est d'écrire une fonction `lev` qui prends en paramètres deux chaînes de caractères `a` et `b` et qui renvoie un entier correspondant à la distance de Levenshtein. 

???+ example "Exemples"

    ```pycon title=""
    >>> lev("distance", "distance")
    0
    >>> lev("distance", "distante")
    1
    >>> lev("distance", "distances")
    1
    >>> lev("distance", "dispense")
    3
    >>> lev("distance", "pense")
    6
    >>> lev("distance", "haricots")
    8
    ```

??? abstract "Les tranches"

    On peut obtenir un extrait d'une chaîne de caractères en désignant une **tranche** (**slice** en anglais) . Pour cela il faut indiquer le premier élément et le dernier qui ne sera pas inclus séparés par `:`.

    ```pycon
    >>> mot = "tranche"
    >>> mot[2:6]
    'anch'
    >>> mot[2:]
    'anche'
    >>> mot[:4]
    'tran'
    ```

??? note "Aide"
    On pourra utiliser les fonctions native `min`et `max`
    ```pycon
    >>> min (5, 4)
    4
    >>> max(1, 3, 2)
    3
    ```
    

{{ IDE('exo') }}
