

# --------- PYODIDE:code --------- #

def lev(a, b):
    ...

# --------- PYODIDE:corr --------- #

def lev(a, b):
    longeur_a = len(a)
    longeur_b = len(b)
    if min(longeur_a, longeur_b) == 0:
        return max(longeur_a, longeur_b)
    elif a[0] == b[0]:
        return lev(a[1:longeur_a], b[1:longeur_b])
    else:
        return 1 + min(lev(a[1:longeur_a], b[1:longeur_b]),
                       lev(a, b[1:longeur_b]),
                       lev(a[1:longeur_a], b))

# --------- PYODIDE:tests --------- #

# Test de deux chaines identiques
assert lev("distance", "distance") == 0
# Test de chaines avec une substitution
assert lev("distance", "distante") == 1
# Test de chaines avec un ajout
assert lev("distance", "distances") == 1
# Test de chaines avec 3 suppressions
assert lev("distance", "pense") == 6
# Test de chaines avec 3 substitutions
assert lev("distance", "dispense") == 3
# Test de chaines différentes
assert lev("distance", "haricots") == 8

# --------- PYODIDE:secrets --------- #

# Tests
assert lev("dis","dis") == 0
assert lev("dis","dit") == 1
assert lev("dis","di") == 1
assert lev("dis","disa") == 1
assert lev("dis","nid") == 2
assert lev("dis","nez") == 3

# Autres tests
