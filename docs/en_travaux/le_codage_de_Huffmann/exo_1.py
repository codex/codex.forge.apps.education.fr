

# --------- PYODIDE:code --------- #

def compter_occurrences(chaine):
    ...

# --------- PYODIDE:corr --------- #

def compter_occurrences(chaine):
    occurrences = {}
    for caractere in chaine:
        if caractere in occurrences:
            occurrences[caractere] = occurrences[caractere] + 1
        else:
            occurrences[caractere] = 1
    return occurrences

# --------- PYODIDE:tests --------- #

assert compter_occurrences("que voulez vous que je vous dise") == {'q':2,'u':5, 'e':5, ' ':6, 'v':3, 'o':3,'l':1, 'z':1, 's':3, 'j':1, 'd':1, 'i':1}
assert compter_occurrences("aaa") == {"a":3}

# --------- PYODIDE:secrets --------- #


# tests secrets

from random import randrange
car = chr(randrange(32,127))
longueur = randrange(1,1000)
assert compter_occurrences(car*longueur) == {car:longueur}