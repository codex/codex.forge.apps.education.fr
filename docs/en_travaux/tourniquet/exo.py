# --- PYODIDE:env --- #
from collections import deque
from random import sample, randrange


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.appendleft(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.pop()

    def __repr__(self):
        """Convertit la file en une chaîne de caractères"""
        if self.est_vide():
            return "∅"
        return f"(queue) {' -> '.join(str(x) for x in self.valeurs)} (tête)"


class Processus:
    """Classe définissant un processus"""

    def __init__(self, pid, duree):
        self.pid = pid
        self._duree = duree  # ne pas modifier cet attribut

    def execute(self, quantum):
        """Diminue la durée d'exécution restante de quantum. Bloque à 0"""
        self._duree = max(0, self._duree - quantum)

    def est_fini(self):
        """Renvoie le booléen répondant à la question 'Le processus est-il terminé ?'"""
        return self._duree == 0

    def __repr__(self):
        """Affichage"""
        return f"(PID={self.pid} ; durée={self._duree})"


# --- PYODIDE:code --- #
def tourniquet(file, quantum):
    ...


# --- PYODIDE:corr --- #
def tourniquet(file, quantum):
    ordre = []
    while not file.est_vide():
        processus = file.defile()
        ordre.append(processus.pid)
        processus.execute(quantum)
        if not processus.est_fini():
            file.enfile(processus)
    return ordre


# --- PYODIDE:tests --- #
f = File()
f.enfile(Processus(0, 5))
f.enfile(Processus(1, 2))
assert tourniquet(f, 1) == [0, 1, 0, 1, 0, 0, 0]

f = File()
f.enfile(Processus(8, 5))
f.enfile(Processus(26, 2))
f.enfile(Processus(7, 6))
assert tourniquet(f, 6) == [8, 26, 7]


# --- PYODIDE:secrets --- #
from random import randrange, sample
def _tourniquet_(file, quantum):
    ordre = []
    while not file.est_vide():
        processus = file.defile()
        ordre.append(processus.pid)
        processus.execute(quantum)
        if not processus.est_fini():
            file.enfile(processus)
    return ordre


# file vide
assert tourniquet(f, 1) == [], f"Erreur avec une file vide"

# Un processus + quantum < duree
f = File()
f.enfile(Processus(5, 10))
ff = File()
ff.enfile(Processus(5, 10))
quantum = 1
attendu = [5] * 10
assert tourniquet(f, quantum) == attendu, f"Erreur avec f = {ff} et {quantum = }"

# Un processus + quantum = duree
f = File()
f.enfile(Processus(5, 10))
quantum = 10
attendu = [5]
assert tourniquet(f, quantum) == attendu, f"Erreur avec f = {ff} et {quantum = }"

# Un processus + quantum > duree
f = File()
f.enfile(Processus(5, 10))
quantum = 100
attendu = [5]
assert tourniquet(f, quantum) == attendu, f"Erreur avec f = {ff} et {quantum = }"

n = randrange(5, 8)
pids = sample(range(100), n)
f = File()
ff = File()
fff = File()
for _ in range(n):
    pid = pids.pop()
    d = randrange(1, 20)
    f.enfile(Processus(pid, d))
    ff.enfile(Processus(pid, d))
    fff.enfile(Processus(pid, d))
quantum = randrange(1, 50)
attendu = _tourniquet_(fff, quantum)
assert tourniquet(f, quantum) == attendu, f"Erreur avec f = {ff} et {quantum = }"