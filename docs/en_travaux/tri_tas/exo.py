

# --------- PYODIDE:env --------- #

from heapq import heappush, heappop

class TasMin:
    def __init__(self):
        self.donnees = []

    def est_vide(self):
        return self.donnees == []

    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini

# --------- PYODIDE:code --------- #

# La classe TasMin est incluse automatiquement pour cet exercice

def tri_par_tas(valeurs):
    ...

# --------- PYODIDE:corr --------- #

from heapq import heappush, heappop

class TasMin:
    def __init__(self):
        self.donnees = []

    def est_vide(self):
        return self.donnees == []

    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini


def tri_par_tas(valeurs):
    tas = TasMin()
    for x in valeurs:
        tas.ajoute(x)
    resultat = []
    while not tas.est_vide():
        resultat.append(tas.extrait_min())
    return resultat



# tests

assert tri_par_tas([55, 42, 12, 73]) == [12, 42, 55, 73]

assert tri_par_tas(['bac', 'a', 'abc', 'b']) == ['a', 'abc', 'b', 'bac']

# --------- PYODIDE:tests --------- #

assert tri_par_tas([55, 42, 12, 73]) == [12, 42, 55, 73]

assert tri_par_tas(['bac', 'a', 'abc', 'b']) == ['a', 'abc', 'b', 'bac']

# --------- PYODIDE:secrets --------- #


# autres tests

from random import sample
from heapq import heapify

taille = 10**5
valeurs = list(sample(range(10**9), taille))
copie = valeurs.copy()
heapify(copie)
attendu = [heappop(copie) for _ in range(taille)]
resultat = tri_par_tas(valeurs)
assert attendu == resultat