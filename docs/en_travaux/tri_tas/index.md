---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Tri par Tas
tags:
    - en travaux
    - tri
    - programmation orientée objet
---
# Tri par tas

!!! warning "Avertissement"
    Pour faire cet exercice, il est nécessaire d'avoir fait l'exercice de découverte de la structure de tas-min avec la fonction `est_tas_min`. Ou au moins, savoir ce qu'est la structure de tas.

Le tri par tas utilise la même stratégie que le tri par sélection : on extrait la valeur minimale, de manière répétitive, qui reste dans les données. Cette valeur est ajoutée à la fin de la liste triée en construction. La seule différence, c'est que le tri par tas utilise une structure efficace pour faire cela : un tas.

On utilisera, dans cet exercice, une implémentation de tas-min avec la bibliothèque standard de Python.

```py
from heapq import heappush, heappop

class TasMin:
    def __init__(self):
        self.donnees = []

    def est_vide(self):
        return self.donnees == []

    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini
```

!!! example "Exemple d'utilisation"
    ```pycon
    >>> nombres = TasMin()
    >>> nombres.est_vide()
    True
    >>> nombres.ajoute(7)
    >>> nombres.est_vide()
    False
    >>> nombres.ajoute(4)
    >>> nombres.ajoute(9)
    >>> nombres.extrait_min()
    4
    >>> nombres.extrait_min()
    7
    >>> nombres.est_vide()
    False
    >>> nombres.extrait_min()
    9
    >>> nombres.est_vide()
    True
    ```


L'objectif de l'exercice est d'implémenter une fonction `tri_par_tas` qui prend en paramètre une liste d'éléments comparables entre eux et qui renvoie cette liste triée.

???+ warning "Contrainte"

    Il est interdit d'utiliser les méthodes natives `#!py sort` et `#!py sorted`. Vous devez utiliser la classe `TasMin` construite avec l'aide de la bibliothèque standard de Python. Dans un autre exercice, vous aurez à construire cette classe **sans** la bibliothèque standard.

!!! example "Exemples"

    ```pycon title=""
    >>> tri_par_tas([55, 42, 12, 73])
    [12, 42, 55, 73]
    >>> tri_par_tas(['bac', 'a', 'abc', 'b'])
    ['a', 'abc', 'b', 'bac']
    ```

{{ IDE('exo', SANS='.sort,sorted') }}
