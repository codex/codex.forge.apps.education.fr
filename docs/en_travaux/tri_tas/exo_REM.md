Pour un tas de taille $n$,

- l'insertion a un cout en $\mathcal O(n)$
- l'extraction du minimum a un cout en $\mathcal O(n)$

Ainsi le cout d'un tri par tas d'un tableau de taille $n$ est en $\mathcal O(n\log(n))$, qui est le meilleur sans connaissance préalable sur les données.

En pratique, il existe un autre tri plus rapide, uniquement à un facteur multiplicatif constant près ; `quicksort` est environ deux fois plus rapide.

> Les tris simples (par insertion, par sélection, à bulles) sont lents en comparaison ; ils sont en $\mathcal O(n^2)$.
