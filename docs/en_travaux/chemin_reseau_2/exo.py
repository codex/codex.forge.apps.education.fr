# --------- PYODIDE:code --------- #
def trouver_chemin(reseau, depart, arrivee, visites=None):
    if visites is None:
        visites = {}
    visites[depart] = True
    if ...:
        return ...
    else:
        for etape in ...:
            if etape not in ...:
                chemin_trouve = trouver_chemin(reseau, ..., ..., visites)
                if len(chemin_trouve) > 0:
                    return [...] + ...
        return []


# --------- PYODIDE:corr --------- #
def trouver_chemin(reseau, depart, arrivee, visites=None):
    if visites is None:
        visites = {}
    visites[depart] = True
    if depart == arrivee:
        return [arrivee]
    else:
        for etape in reseau[depart]:
            if etape not in visites:
                chemin_trouve = trouver_chemin(reseau, etape, arrivee, visites)
                if len(chemin_trouve) > 0:
                    return [depart] + chemin_trouve
        return []


# --------- PYODIDE:tests --------- #
mon_reseau = {
    "Lycee": ["Routeur1"],
    "Routeur1": ["Lycee", "Routeur2", "Webcam"],
    "Webcam": ["Routeur1"],
    "Routeur3": ["Maison", "Routeur2"],
    "Routeur2": ["Routeur1", "Routeur4"],
    "Routeur4": ["Routeur3"],
    "Maison": ["Routeur3"],
}

mon_reseau_avec_panne = {
    "Lycee": ["Routeur1"],
    "Routeur1": ["Lycee", "Routeur2"],
    "Webcam": ["Routeur1"],
    "Routeur3": ["Maison"],
    "Routeur2": ["Routeur1", "Routeur4"],
    "Routeur4": ["Routeur3"],
    "Maison": ["Routeur3"],
}

assert trouver_chemin(mon_reseau, "Webcam", "Lycee") == ["Webcam", "Routeur1", "Lycee"]
assert trouver_chemin(mon_reseau, "Lycee", "Lycee") == ["Lycee"]
assert trouver_chemin(mon_reseau_avec_panne, "Maison", "Lycee") == []


# --------- PYODIDE:secrets --------- #
# Tests supplémentaires
mon_reseau = {
    "Lycee": ["Routeur1"],
    "Routeur1": ["Lycee", "Routeur2", "Webcam"],
    "Webcam": ["Routeur1"],
    "Routeur3": ["Maison", "Routeur2"],
    "Routeur2": ["Routeur1", "Routeur4"],
    "Routeur4": ["Routeur3"],
    "Maison": ["Routeur3"],
}
attendu = [
    "Lycee",
    "Routeur1",
    "Routeur2",
    "Routeur4",
    "Routeur3",
    "Maison",
]
depart =  "Lycee"
arrivee = "Maison"

assert trouver_chemin(mon_reseau, depart, arrivee) == attendu, f"Erreur avec {mon_reseau, depart, arrivee = }"


mon_reseau = {
    "Lycee": ["Routeur1"],
    "Routeur1": ["Lycee"],
    "IUT": ["Routeur1"],
    "Routeur3": ["Maison"],
    "Routeur2": ["Routeur1", "Routeur3", "Routeur4", "Maison"],
    "Routeur4": ["Maison", "Routeur2"],
    "Maison": ["Routeur2", "Routeur4"],
    "Routeur5": [],
}

attendu = [
    "Maison",
    "Routeur2",
    "Routeur1",
    "Lycee",
]
depart =  "Maison"
arrivee = "Lycee"

assert trouver_chemin(mon_reseau, depart, arrivee) == attendu, f"Erreur avec {mon_reseau, depart, arrivee = }"

attendu = []
depart =  "Routeur5"
arrivee = "IUT"
assert trouver_chemin(mon_reseau, depart, arrivee) == attendu, f"Erreur avec {mon_reseau, depart, arrivee = }"

attendu = ["IUT", "Routeur1", "Lycee"]
depart =  "IUT"
arrivee = "Lycee"
assert trouver_chemin(mon_reseau, depart, arrivee) == attendu, f"Erreur avec {mon_reseau, depart, arrivee = }"
