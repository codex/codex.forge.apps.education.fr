Il est possible d'utiliser le [module Python `heapq`](https://docs.python.org/fr/3/library/heapq.html).

Les fonctions principales suivantes sont fournies :

* Création d'un tas : utilisez une liste initialisée à `#!py tas = []`.

* Ajout d'une valeur : utilisez `#!py heapq.heappush(tas, valeur)`;

* Récupération du minimum : utilisez `#!py heapq.heappop(tas)`.


Ce qui donne une version bien plus courte et efficace :

```python
from heapq import heappush, heappop

class TasMin:
    def __init__(self):
        self.donnees = []
    
    def est_vide(self):
        return self.donnees == []
    
    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini
```
