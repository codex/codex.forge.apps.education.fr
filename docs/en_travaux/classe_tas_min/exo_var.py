from heapq import heappush, heappop

class TasMin:
    # version avec la bibliothèque

    def __init__(self):
        self.donnees = []
    
    def est_vide(self):
        return self.donnees == []
    
    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini
