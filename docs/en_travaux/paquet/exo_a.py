# --------- PYODIDE:env --------- #
class Paquet:
    def __init__(self, adresse_depart, adresse_arrivee):
        self.arrivee = adresse_arrivee
        self.duree_de_vie = 64
        self.passerelle = adresse_depart
        self.etat = "a_transferer"
    
    def __str__(self):
        return "arrivée :"+self.arrivee+" passerelle:"+self.passerelle+" TTL:"+str(self.duree_de_vie)+" état:"+self.etat
   


# --------- PYODIDE:code --------- #

def routage(paquet, table): 
    ...

# --------- PYODIDE:corr --------- #


def routage(paquet, table): 
    if paquet.arrivee in table:
        paquet.passerelle = table[paquet.arrivee]
    else:
        paquet.passerelle = table["Autres"]
    paquet.duree_de_vie -= 1
    if paquet.passerelle == paquet.arrivee:
        paquet.etat = "arrive"
    elif paquet.duree_de_vie == 0:
        paquet.etat = "rejete"
        
    
# --------- PYODIDE:tests --------- #

paquet1 = Paquet("Lycee", "Webcam")
paquet2 = Paquet("Lycee", "Maison")

table_lycee = {"Routeur1": "Routeur1",
             "Autres": "Routeur1"
              }

table_routeur1 =  {
        "Lycee": "Lycee",
        "Webcam": "Webcam",
        "Autres": "Routeur2"
    }

routage(paquet1, table_lycee)
assert paquet1.passerelle == 'Routeur1'
assert paquet1.duree_de_vie == 63
routage(paquet1, table_routeur1)
assert paquet1.passerelle == 'Webcam'
assert paquet1.duree_de_vie == 62
assert paquet1.etat == "arrive"
routage(paquet2, table_routeur1)
assert paquet2.passerelle == 'Routeur2'
assert paquet2.duree_de_vie == 63

# --------- PYODIDE:secrets --------- #
paquet1 = Paquet("Lycee", "Webcam")
paquet2 = Paquet("Lycee", "Inconnu")

table_routeur2 = {
        "Lycee": "Routeur1",
        "Webcam": "Routeur1",
        "Autres": "Routeur4"
    }

    
table_routeur3 =  {
        "Maison": "Maison",
        "Autres": "Routeur2"
    }

table_maison = {
        "Routeur3": "Routeur3",
        "Autres": "Routeur3"
    }

    
routage(paquet1, table_routeur2)
assert paquet1.passerelle == 'Routeur1'
assert paquet1.duree_de_vie == 63
routage(paquet1, table_routeur3)
assert paquet1.passerelle == 'Routeur2'
assert paquet1.duree_de_vie == 62
assert paquet1.etat == "a_transferer"

for _ in range(64):
    routage(paquet2, table_routeur2)
assert paquet2.etat == "rejete"

