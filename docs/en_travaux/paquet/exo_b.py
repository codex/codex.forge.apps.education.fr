# --------- PYODIDE:env --------- #
class Paquet:
    def __init__(self, adresse_depart, adresse_arrivee):
        self.arrivee = adresse_arrivee
        self.duree_de_vie = 64
        self.passerelle = adresse_depart
        self.etat = "a_transferer"

    def __str__(self):
        return "arrivée :"+self.arrivee+" passerelle:"+self.passerelle+" TTL:"+str(self.duree_de_vie)+" état:"+self.etat
   
    
def routage(paquet, table): 
    assert paquet.etat == "a_transferer", "Un paquet doit être à l'état a_transfere"
    if paquet.arrivee in table:
        paquet.passerelle = table[paquet.arrivee]
    else:
        paquet.passerelle = table["Autres"]
    paquet.duree_de_vie -= 1
    if paquet.passerelle == paquet.arrivee:
        paquet.etat = "arrive"
    elif paquet.duree_de_vie == 0:
        paquet.etat = "rejete"

# --------- PYODIDE:code --------- #
def transfert_paquet(paquet, reseau):
    ...

# --------- PYODIDE:corr --------- #
def transfert_paquet(paquet, reseau):
    while paquet.etat=="a_transferer":
        routage(paquet, reseau[paquet.passerelle])
    return paquet.etat
    
# --------- PYODIDE:tests --------- #

paquet1 = Paquet("Lycee", "Webcam")
paquet2 = Paquet("Lycee", "Maison")

mon_reseau = {
    "Lycee": {
        "Routeur1": "Routeur1",
        "Autres": "Routeur1"
    },
    "Webcam": {
        "Routeur1": "Routeur1",
        "Autres": "Routeur1"
    },
    "Routeur1": {
        "Lycee": "Lycee",
        "Webcam": "Webcam",
        "Autres": "Routeur2"
    },
    "Routeur2": {
        "Lycee": "Routeur1",
        "Webcam": "Routeur1",
        "Autres": "Routeur4"
    },
    "Routeur4": {
        "Autres": "Routeur3"
    },
    "Routeur3": {
        "Maison": "Maison",
        "Autres": "Routeur2"
    },
    "Maison": {
        "Routeur3": "Routeur3",
        "Autres": "Routeur3"
    },
}

paquet1 = Paquet("Lycee", "Webcam")
assert transfert_paquet(paquet1, mon_reseau) == "arrive"
paquet2 = Paquet("Lycee", "Inconnu")
transfert_paquet(paquet2, mon_reseau) == "rejete"


# --------- PYODIDE:secrets --------- #


paquet1 = Paquet("Lycee", "Webcam")
paquet1.duree_de_vie = 1
assert transfert_paquet(paquet1, mon_reseau) == "rejete"
paquet2 = Paquet("Lycee", "Maison") 
assert transfert_paquet(paquet2, mon_reseau) == "arrive"