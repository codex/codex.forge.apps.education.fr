---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Paquets
tags:
    - en travaux
    - programmation orientée objet
---


On souhaite transféré un paquet au travers de routeurs dans un réseau informatique. 

??? example "Voici un exemple de réseau"
    ```mermaid
    flowchart LR
        A([Lycee]) <--> B([Routeur1])
        B <--> E([Routeur2])
        C([Webcam]) <--> B
        D([Routeur3]) <--> G([Maison])
        D <--> E
        E <--> F([Routeur4])
        F <--> D
    ```
    

Ce paquet est modélisé par une  classe `Paquet`, décrite ci-dessous :

```mermaid
classDiagram
class Paquet{
       arrivee : str
       passerelle : str
       duree_de_vie : int
       etat : "a_transferer"
       __str__()
}
```

Un `paquet` est caractérisé par :

* son adresse du réseau de destination : `arrivee` de type `str`,
* l'adresse de passerelle (à savoir l'adresse de sa prochaine étape)  : `passerelle` de type `str`,
* sa durée de vie : `duree_de_vie` de type `int`,
* et son état, soit "a_transferer", soit "rejete", soit "arrive" : `etat` de type `str`.

!!! abstract "Durée de vie"
	La **durée de vie** ou TTL (Time To Live) d'un paquet est un nombre entier placée au niveau de l'en-tête du paquet IP qui indique le nombre maximal de routeurs de transit.

	Par défaut, la valeur est fixée à $64$ et est décrémentée de $1$ à chaque passage dans un routeur.
    
    Quand il ce nombre arrive à $0$, on considère que le paquet est perdu et le routeur le rejette.



En utilisant cette classe `Paquet`, les paquets suivants  :

* `paquet1` qui a pour destination "Webcam" et depuis "Lycee",
* `paquet2` qui a pour destination "Maison" et depuis "Lycee".

sont définis ainsi :

```pycon
>>> paquet1 = Paquet("Lycee", "Webcam")
>>> paquet2 = Paquet("Lycee", "Maison")
```

!!! abstract "Table de routage"
    Une table de routage associe pour chaque nœud d'arrivée la prochaine étape du chemin.

    Ainsi la table de routage du *Routeur2* est la suivante :

    | Destination | Prochaine étape |
    | :---------- | :-------------- |
    | Lycee       | Routeur1        |
    | Webcam      | Routeur1        |
    | Autres      | Routeur3        |

    Dans la table de routage, on trouve une route **par défaut**, indiquée ici par "Autres". 

    Quand un paquet arrive sur un routeur, on vérifie si l'adresse est présente dans la table de routage et on trouve la prochaine étape. Si l'adresse n'est pas présente, on prend la route par défaut.

    Ainsi, si un paquet arrive au *Routeur2* avec pour adresse de destination *Maison*, il sera redirigé vers le *Routeur3*.


Les deux questions ci-dessous peuvent être traitées de façon indépendante.

??? question "Question 1"

    **Créer** une fonction `routage` qui prend en paramètre un paquet et une table de routage et qui modifie *en place* le paquet.

    ```pycon
    >>> table_lycee = {"Routeur1": "Routeur1",
             "Autres": "Routeur1"
              }
    >>> routage(paquet1, table_lycee)
    >>> paquet1.passerelle
    'Routeur1'
    ```

    On certifie que le paquet passé en paramètre se trouve à l'état "a_transferer".

    {{ IDE('exo_a') }}

On représente l'ensemble du réseau par un dictionnaire dans lequel :

- les clés sont les chaînes de caractères correspondant aux noms des nœuds, 
  
- les valeurs associées sont des dictionnaires représentant la table de routage du nœuds correspondant.

??? question "Question 2"
    **Créer** une fonction `transfert_paquet` qui prend en paramètre un paquet et une table de routage et qui renvoie une chaine de caractères représentant l'état du paquet à la fin du routage.

    ```pycon
    >>> transfert_paquet(paquet1, mon_reseau)
    'arrive'
    >>> transfert_paquet(paquet2, mon_reseau)
    'rejete'
    ```
    Une fonction `routage` est chargée en mémoire. La question peut donc être traitée sans que la précédente soit résolue.

    {{ IDE('exo_b') }}


