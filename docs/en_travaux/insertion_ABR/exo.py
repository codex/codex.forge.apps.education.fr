# --------- PYODIDE:code --------- #
def insertion_abr(arbre, valeur):
    ...
# --------- PYODIDE:corr --------- #
def insertion_abr(arbre, valeur):
    if arbre is None:
        return (None, valeur,  None)

    sag, cle, sad = arbre
    if cle > valeur:
        return (insertion_abr(sag, valeur), cle, sad)
    elif cle < valeur:
        return (sag, cle, insertion_abr(sad, valeur)) 
    else:
        return arbre

# --------- PYODIDE:tests --------- #
n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
abr_1 = (n0, 1, n2)

assert insertion_abr(abr_1, 4) == ((None,0,None),1,(None,2,(None,3,(None,4,None))))
assert insertion_abr(abr_1, -5) == (((None,-5,None),0,None),1,(None,2,(None,3,None)))
assert insertion_abr(abr_1, 2) == ((None,0,None),1,(None,2,(None,3,None)))
# --------- PYODIDE:secrets --------- #
abr_2 = insertion_abr(abr_1, 4)
abr_3 = insertion_abr(abr_2, -5)
assert abr_3 == (((None,-5,None),0,None),1,(None,2,(None,3,(None,4,None))))
abr_4 = insertion_abr(abr_3, 2)
assert abr_4 == (((None,-5,None),0,None),1,(None,2,(None,3,(None,4,None))))

