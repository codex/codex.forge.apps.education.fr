---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Insertion dans un ABR
tags:
    - en travaux
    - arbre binaire
    - ABR
    - ep1
difficulty: 200
---

On considère un arbres binaires de recherche qui :

* soit est l’arbre vide identifié par `None` ;
* soit possède une valeur, un sous-arbre gauche `sag` et un sous-arbre droit `sad`, tous les trois regroupés dans un triplet `(sag, valeur, sad)`.

```mermaid
graph
    n1((1)) --- n0((0))
    n1 --- n2((2))
    n2 --- n4((" "))
    n2 --- n3((3))
```

Ainsi, l’arbre binaire de recherche abr_1 ci-dessus est créé par le code python suivant :
```python title=""
n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
abr_1 = (n0, 1, n2)
```

Écrire une fonction récursive `insertion_abr` qui prend en paramètres un arbre binaire de recherche  `abr` et une clé `valeur`, et qui renvoie un arbre binaire de recherche dans lequel `valeur` a été insérée dans un emplacement libre (c'est-à-dire à la place d'un arbre vide).
Dans le cas où `cle` est déjà présente dans `abr`, la fonction renvoie l’arbre inchangé.

!!! example "Exemple"
    ```pycon title=""
    >>> insertion_abr(abr_1, 4)
    ((None,0,None),1,(None,2,(None,3,(None,4,None))))
    >>> insertion_abr(abr_1, -5)
    (((None,-5,None),0,None),1,(None,2,(None,3,None)))
    >>> insertion_abr(abr_1, 2)
    ((None,0,None),1,(None,2,(None,3,None)))
    ```


{{ IDE('exo') }}