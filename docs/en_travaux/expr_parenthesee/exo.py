

# --------- PYODIDE:code --------- #

class Noeud:
    """
    Classe pour un nœud d'arbre disposant de 3 attributs :
    - gauche : le sous-arbre à gauche.
    - valeur : la valeur portée par le nœud,
    - droite : le sous-arbre à droite.
    """

    def __init__(self, gauche, valeur, droite):
        self.gauche = gauche
        self.valeur = valeur
        self.droite = droite

    def est_feuille(self):
        """Renvoie un booléen : le nœud est-il une feuille ?"""
        return self.gauche is None and self.droite is None


def expression_parenthesee(e):
    if e.est_feuille():
        return str(...)
    else:
        return '(' + expression_parenthesee(...) + ...

# --------- PYODIDE:corr --------- #

class Noeud:
    """
    Classe implémentant un nœud disposant de 3 attributs :
    - valeur : la valeur de l'étiquette,
    - gauche : le sous-arbre à gauche.
    - droite : le sous-arbre à droite.

    Ce n'est pas, ici, pour un arbre binaire, mais pour un arbre d'arité 2.
    """
    def __init__(self, gauche, valeur, droite):
        self.gauche = gauche
        self.valeur = valeur
        self.droite = droite

    def est_feuille(self):
        "Renvoie un booléen, la réponse à : le nœud est-il une feuille ?"
        return self.gauche is None and self.droite is None


def expression_parenthesee(e):
    if e.est_feuille():
        return str(e.valeur)
    else:
        return (
                '('
                + expression_parenthesee(e.gauche)
                + e.valeur
                + expression_parenthesee(e.droite)
                + ')'
        )


# tests

feuille = Noeud(None, 5, None)
assert expression_parenthesee(feuille) == '5'

somme = Noeud(Noeud(None, 2, None), '+', Noeud(None, 3, None))
assert expression_parenthesee(somme) == '(2+3)'

produit = Noeud(Noeud(None, 4, None), '*', somme)
assert expression_parenthesee(produit) == '(4*(2+3))'

somme_1 = Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))
somme_2 = Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None))
produit_1 = Noeud(Noeud(None, 3, None), '*', somme_1)
expression = Noeud(produit_1, '-', somme_2)

assert expression_parenthesee(expression) == '((3*(8+7))-(2+1))'

# --------- PYODIDE:tests --------- #

feuille = Noeud(None, 5, None)
assert expression_parenthesee(feuille) == '5'

somme = Noeud(Noeud(None, 2, None), '+', Noeud(None, 3, None))
assert expression_parenthesee(somme) == '(2+3)'

produit = Noeud(Noeud(None, 4, None), '*', somme)
assert expression_parenthesee(produit) == '(4*(2+3))'

somme_1 = Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))
somme_2 = Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None))
produit_1 = Noeud(Noeud(None, 3, None), '*', somme_1)
expression = Noeud(produit_1, '-', somme_2)

assert expression_parenthesee(expression) == '((3*(8+7))-(2+1))'

# --------- PYODIDE:secrets --------- #

# tests
nombre = Noeud(None, 5, None)
assert expression_parenthesee(nombre) == '5'

somme = Noeud(Noeud(None, 2, None), '+', Noeud(None, 3, None))
assert expression_parenthesee(somme) == '(2+3)'

produit = Noeud(Noeud(None, 4, None), '*', somme)
assert expression_parenthesee(produit) == '(4*(2+3))'

somme_1 = Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))
somme_2 = Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None))
produit_1 = Noeud(Noeud(None, 3, None), '*', somme_1)
expression = Noeud(produit_1, '-', somme_2)

assert expression_parenthesee(expression) == '((3*(8+7))-(2+1))'



# autres tests

nombre = Noeud(None, 42, None)
assert expression_parenthesee(nombre) == '42'

somme = Noeud(Noeud(None, 1, None), '+', Noeud(None, 2, None))
assert expression_parenthesee(somme) == '(1+2)'

quotient = Noeud(Noeud(None, 3, None), '/', somme)
assert expression_parenthesee(quotient) == '(3/(1+2))'

