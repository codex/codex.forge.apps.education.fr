

# --------- PYODIDE:code --------- #

def l_max_equilibree(bits):
    i_bonus_1 = [0]
    i_bonus_0 = [0]
    l_max = 0
    delta = 0
    for i, x in enumerate(bits):
        if x == 1:
            delta = ...
        else:
            delta = ...
        if delta > len(i_bonus_1) - 1:
            i_bonus_1.append(...)
        elif ...:
            i_bonus_0.append(...)
        elif delta >= 0:
            if i - i_bonus_1[delta] > l_max:
                l_max = ...
        else: # delta <= 0:
            if ...:
                ...
    return ...

# --------- PYODIDE:corr --------- #

def l_max_equilibree(bits):
    i_bonus_1 = [0]
    i_bonus_0 = [0]
    l_max = 0
    delta = 0
    for i, x in enumerate(bits):
        if x == 1:
            delta += 1
        else:
            delta -= 1
        if delta > len(i_bonus_1) - 1:
            i_bonus_1.append(i)
        elif -delta > len(i_bonus_0) - 1:
            i_bonus_0.append(i)
        elif delta >= 0:
            if i - i_bonus_1[delta] > l_max:
                l_max = i - i_bonus_1[delta]
        else: # delta <= 0:
            if i - i_bonus_0[-delta] > l_max:
                l_max = i - i_bonus_0[-delta]
    return l_max

# --------- PYODIDE:secrets --------- #

def secret_l_max_equilibree(bits):
    i_bonus_1 = [0]
    i_bonus_0 = [0]
    l_max = 0
    delta = 0
    for i, x in enumerate(bits):
        if x == 1:
            delta += 1
        else:
            delta -= 1
        if delta > len(i_bonus_1) - 1:
            i_bonus_1.append(i)
        elif -delta > len(i_bonus_0) - 1:
            i_bonus_0.append(i)
        elif delta >= 0:
            if i - i_bonus_1[delta] > l_max:
                l_max = i - i_bonus_1[delta]
        else: # delta <= 0:
            if i - i_bonus_0[-delta] > l_max:
                l_max = i - i_bonus_0[-delta]
    return l_max




# Tests
assert l_max_equilibree([0, 0, 1, 0, 0, 0, 1, 1, 0, 0]) == 6

assert l_max_equilibree([1, 1, 1, 1]) == 0

assert l_max_equilibree([1, 0, 1, 0, 1]) == 4


# Autres tests

from random import getrandbits
for _ in range(10):
    bits = bin(getrandbits(10**5+1))[3:]
    attendu = secret_l_max_equilibree(bits)
    assert l_max_equilibree(bits) == attendu
