Autre version légèrement plus synthétique :

```python
def somme_chiffres(effectifs_chiffres):
    resultat = 0
    for chiffre, effectif in enumerate(effectifs_chiffres):
        resultat += chiffre * effectif
    return resultat 
```