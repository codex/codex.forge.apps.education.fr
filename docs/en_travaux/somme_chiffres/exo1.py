

# --------- PYODIDE:code --------- #

#Exercice 1
def somme_chiffres(effectifs_chiffres):
    ...


# --------- PYODIDE:corr --------- #

def somme_chiffres(effectifs_chiffres):
    # assert len(effectifs_chiffres) == 10, Le tableau doit être de taille 10
    resultat = 0
    for i in range(10):
        resultat = resultat + effectifs_chiffres[i] * i
    return resultat

# --------- PYODIDE:secrets --------- #



assert somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) == 0
assert somme_chiffres([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) == 45
assert somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 1]) == 9
assert somme_chiffres([0, 9, 0, 0, 0, 0, 0, 0, 0, 0]) == 9
assert somme_chiffres(decomposition(111223)) == 10
assert somme_chiffres(decomposition(1337 ** 42)) == 595
