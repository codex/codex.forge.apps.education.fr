
# --------- PYODIDE:code --------- #
def decomposition(nombre):
    effectifs_chiffres = [0] * ...
    while nombre != ...:
        nombre, unite = ...
        effectifs_chiffres[...] = effectifs_chiffres[...] + 1
    return ...

# --------- PYODIDE:corr --------- #

def decomposition(nombre):
    effectifs_chiffres = [0] * 10
    while nombre != 0:
        nombre, unite = nombre // 10, nombre % 10
        effectifs_chiffres[unite] = effectifs_chiffres[unite] + 1
    return effectifs_chiffres


# --------- PYODIDE:tests --------- #
assert decomposition(342424) == [0, 0, 2, 1, 3, 0, 0, 0, 0, 0]
assert decomposition(10111213141516171819) == [1, 11, 1, 1, 1, 1, 1, 1, 1, 1]
assert decomposition(1234567890) == [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]


# --------- PYODIDE:secrets --------- #


assert decomposition(0) == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
assert decomposition(111223) == [0, 3, 2, 1, 0, 0, 0, 0, 0, 0]
assert decomposition(312121) == [0, 3, 2, 1, 0, 0, 0, 0, 0, 0]
assert decomposition(1337 ** 42) == [18, 13, 15, 8, 14, 9, 12, 8, 16, 19]

