---
author: Benoît Le Gouis
hide:
    - navigation
    - toc
title: Somme des chiffres d'un nombre
tags:
    - à trous
    - en travaux
---

# Somme des chiffres pour un nombre en écriture décimale

On veut calculer la somme des chiffres d'un nombre en écriture décimale. Ce genre d'opération est effectué de manière naturelle en primaire pour déterminer si un nombre
est un multiple de $3$ ou de $9$ : on fait la somme de ses chiffres, et on regarde si cette somme est elle-même un multiple de $3$ ou de $9$. 

En revanche, là où il est très facile pour un humain de savoir quels chiffres composent un nombre, cette décomposition ne va pas de soi pour un ordinateur.

Cet exercice réalise cette opération en deux étapes, une étape où on décompose un nombre au moyen de la fonction `decomposition`, où on compte le nombre d'occurrences de chacun de ses chiffres et on stocke ce résultat dans un tableau d'effectifs, et une étape où on prend un tel tableau avec la fonction `somme_chiffres`, et on calcule la somme des chiffres associée.

Ainsi le nombre $222270$ a pour tableaux d'effectifs `[1, 0, 4, 0, 0, 0, 0, 1, 0, 0]` et pour somme des chiffres $1×0 + 0×1 + 4×2 + 0×3+ 0×4 + 0×5 + 0×6 + 1×7+ 0×8 + 0×9 = 8+7 = 15$

!!! example "Exemple"
    ```pycon
    >>> decomposition(222270)
    [1, 0, 4, 0, 0, 0, 0, 1, 0, 0]
    >>> somme_chiffres([1, 0, 4, 0, 0, 0, 0, 1, 0, 0])
    15
    ```



??? question "Question 1"
    Compléter la fonction `decomposition` qui prend en argument un nombre entier positif `nombre` et qui renvoie le tableau `effectifs` tel que pour `#!py 0 <= i <= 9`, `effectifs[i]` contient le nombre d'occurrences de `i` dans `nombre`.

    {{ remarque('operateurs_division') }}

    ???+ example "Exemples"

        ```pycon title=""
        >>> decomposition(342424)
        [0, 0, 2, 1, 3, 0, 0, 0, 0, 0]
        >>> decomposition(10111213141516171819)
        [1, 11, 1, 1, 1, 1, 1, 1, 1, 1]
        >>> decomposition(1234567890)
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        >>> decomposition(1337 ** 42)
        [18, 13, 15, 8, 14, 9, 12, 8, 16, 19]
        >>> somme_chiffres(decomposition(111223))
        10
        ```
    === "Version vide"
        {{ IDE('exo2a') }}
    === "Version à compléter"
        {{ IDE('exo2b') }}

??? question "Question 2"
    Compléter la fonction `somme_chiffres` qui prend en argument un tableau d'occurrences de chiffres `effectif_chiffres` de taille $10$ (on vérifiera ce point), contenant pour `#!py 0 <= i <= 9` le nombre de fois où le chiffre `i` est présent et qui renvoie la somme des chiffres ainsi comptés.

    ???+ example "Exemples"

        ```pycon title=""
        >>> somme_chiffres([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
        45
        >>> somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
        9
        >>> somme_chiffres([0, 9, 0, 0, 0, 0, 0, 0, 0, 0])
        9
        >>> somme_chiffres([0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 10, 30, 12])
        AssertionError: Le tableau doit être de taille 10
        ```
    {{ IDE('exo1') }}