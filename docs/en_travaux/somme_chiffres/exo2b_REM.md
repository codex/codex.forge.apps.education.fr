Autre version légèrement plus synthétique :

```python
    def decomposition(nombre):
    effectifs_chiffres = [0] * 10
    while nombre != 0:
        nombre, unite = divmod(nombre, 10)
        effectifs_chiffres[unite] += 1
    return effectifs_chiffres
```