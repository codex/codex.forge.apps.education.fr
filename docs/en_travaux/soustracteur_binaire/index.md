---
title: Soustracteur binaire
author: Vincent-Xavier Jumel
hide:
    - navigation
    - toc
tags:
    - brouillon
    - binaire
---

# Additionner deux nombres binaires

Dans cet exercice, les nombres binaires sont représentés par des listes où
les bits de poids forts sont situés en tête et les bits de poids faibles à
la fin. Ainsi la liste `[1, 0, 1, 1]` représente l'entier $1×2^3 + 0×2^2 + 1×2^2 + 1×2^0 = 11$. **On ne demande pas de convertir les nombres dans ce sujet.**

On rappelle que la soustraction de nombres binaires se fait chiffre à chiffre (ici bit à bit), sans oublier l'éventuelle retenue. Ainsi la soustraction de 1 et 1 renvoie 0 et nécessite d'emprunter 0. On peut par exemple écrire :

```
retenues :    1 1
-----------
            1 1 0 1
            + 1 1 1
            -------
            0 1 1 0
```

Écrire une fonction `soustraction_binaire` qui prends deux listes de bits en
entrée et qui renvoie une liste de bits en sortie, correspondant à
l'addition binaire des deux nombres.


Une fonction `soustracteur` est fournie. Elle prend en entrée trois valeurs
et renvoie deux nombres, l'un étant le chiffre de poids faible de la somme
et le second le chiffre de poids fort, ce dernier pouvant être interprété
comme une retenue. Cette fonction simule les circuits électroniques usuellement présents dans un additionneur binaire. Ces circuits sont habituellement codés avec des portes logiques correspondant aux opérateurs `xor` (ou exclusif), `or` (ou), `and` (et) et `nand` (non et)

???+ warning "Contrainte"

    On n'utilisera ni la fonction `#!py bin` ni les opérateurs `#!py +`, `#!py -`, `#!py //` ou `#!py %`.

???+ example "Exemples"

    ```pycon title=""
    >>> additionneur(0, 1)
    (1, 1)
    >>> addition_binaire([1, 0, 1, 0], [1, 0, 1])
    [1, 1, 1, 1]
    >>> addition_binaire([1, 0, 1, 0], [1, 0, 1, 0, 1])
    [1, 1, 1, 1, 1]
    >>> addition_binaire([1, 1, 1], [1])
    [1, 0, 0, 0]
    >>> addition_binaire([1, 1, 1], [1, 1, 1])
    [1, 1, 1, 0]
    ```

{{ IDE('exo') }}
