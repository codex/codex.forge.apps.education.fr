# --------- PYODIDE:env --------- #
from js import fetch

url_fichier = "villes.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()
lignes = contenu.splitlines()

villes = []

descripteurs = ["dep", "nom", "cp", "nb_hab_2010" ,"nb_hab_1999" ,"nb_hab_2012",
               "dens", "surf" ,"long", "lat", "alt_min", "alt_max"]
nb_colonne = len(descripteurs)

for i in range(1, len(lignes)):
    valeurs = lignes[i].split(",")
    dico = {}
    for i in range(nb_colonne):
        dico[descripteurs[i]] = valeurs[i]
    villes.append(dico)

def serie_nombres(donnees, descripteur):
    return [enregistrement[descripteur] for enregistrement in donnees]

def frequences_chiffre(serie):
    nb_chiffres = 0
    entiers = [0]*10
    for nombre in serie:
        for caractere in nombre:
            if caractere != ".":
                entiers[int(caractere)] += 1
                nb_chiffres += 1
    
    pourcentage = [(e * 100) // nb_chiffres for e in entiers]
    return pourcentage


def affiche_histogramme(valeurs):
    assert len(valeurs) == 10, "le tableau doit contenir 10 éléments"
    x = [1, 2, 2, 3, 4, 4, 4, 4, 4, 1]
    plt.bar(range(10), valeurs, width = 0.6, color = 'yellow',
      edgecolor = 'blue', linewidth = 2,capsize = 10)
    plt.xticks(range(10), [k for k in range(10)])
    plt.xlabel('valeurs')
    plt.ylabel('nombres')
    plt.title('Exemple d\'histogramme simple')
    plt.show()

# --------- PYODIDE:code --------- #
surfaces = serie_nombres(villes, "surf")

freq_surfaces = frequences_chiffre(surfaces)

affiche_histogramme(freq_surfaces)

