# --------- PYODIDE:env --------- #
from js import fetch

url_fichier = "villes.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()
lignes = contenu.splitlines()

villes = []

descripteurs = ["dep", "nom", "cp", "nb_hab_2010" ,"nb_hab_1999" ,"nb_hab_2012",
               "dens", "surf" ,"long", "lat", "alt_min", "alt_max"]
nb_colonne = len(descripteurs)

for i in range(1, len(lignes)):
    valeurs = lignes[i].split(",")
    dico = {}
    for i in range(nb_colonne):
        dico[descripteurs[i]] = valeurs[i]
    villes.append(dico)

import matplotlib.pyplot as plt
PyodidePlot("matplot_fig").target()     # Cible la figure dans laquelle tracer dans la page

def frequences_chiffre(serie):
    ...


# --------- PYODIDE:code --------- #
def frequences_chiffre(serie):
    nb_chiffres = ...
    entiers = [0] * ...
    for nombre in ...:
        for caractere in ...:
            if ...:
                entiers[int(...)] += 1
                nb_chiffres += ...
    
    pourcentage = [(freq * ...) // ... for freq in entiers]
    return ...


# --------- PYODIDE:corr --------- #
def frequences_chiffre(serie):
    nb_chiffres = 0
    entiers = [0] * 10
    for nombre in serie:
        for caractere in nombre:
            if caractere != ".":
                entiers[int(caractere)] += 1
                nb_chiffres += 1
    
    pourcentage = [(e * 100) // nb_chiffres for e in entiers]
    return pourcentage



# --------- PYODIDE:tests --------- #
donnees_1 = ["1.0", "23", "4", "5.6", "78", "9"]
assert frequences_chiffre(donnees_1) == [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
donnees_2 = ["1", "1", "1"]
assert frequences_chiffre(donnees_2) == [0, 100, 0, 0, 0, 0, 0, 0, 0, 0]
# --------- PYODIDE:secrets --------- #
donnees_2 = ["12", "12", "12"]
assert frequences_chiffre(donnees_1) == [0, 50, 50, 0, 0, 0, 0, 0, 0]