# --------- PYODIDE:env --------- #
from js import fetch

url_fichier = "villes.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()
lignes = contenu.splitlines()

villes = []

descripteurs = ["dep", "nom", "cp", "nb_hab_2010" ,"nb_hab_1999" ,"nb_hab_2012",
               "dens", "surf" ,"long", "lat", "alt_min", "alt_max"]
nb_colonne = len(descripteurs)

for i in range(1, len(lignes)):
    valeurs = lignes[i].split(",")
    dico = {}
    for i in range(nb_colonne):
        dico[descripteurs[i]] = valeurs[i]
    villes.append(dico)

def serie_nombres(donnees, descripteur):
    ...


# --------- PYODIDE:code --------- #
def serie_nombres(donnees, descripteur):
    ...


# --------- PYODIDE:corr --------- #
def serie_nombres(donnees, descripteur):
    return [enregistrement[descripteur] for enregistrement in donnees]


# --------- PYODIDE:tests --------- #
villes_exemple = [{"dep": "64", "nom":"Anglet", "surf":"26.93"},
                {"dep": "64", "nom":"Biarritz", "surf":"11.66"},
                {"dep": "64", "nom":"Bayonne", "surf":"21.68"}]
assert serie_nombres(villes_exemple, "surf") == ["26.93", "11.66", "21.68"]

# --------- PYODIDE:secrets --------- #
assert serie_nombres(villes_exemple, "nom") == ["Anglet", "Biarritz", "Bayonne"]