---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Loi de Benford
tags:
    - liste/tableau
    - brouillon
difficulty : 250
---

La loi de Benford est une loi empirique créée par Frank Benford : cette loi indique que dans une série de nombres, les chiffres $1$, $2$ et $3$ apparaissent plus souvent que les chiffres $8$ et $9$.

![illustration](https://upload.wikimedia.org/wikipedia/commons/b/b7/Loi_de_Benford_freq_relat.PNG)

Dans cet exercice, on se propose de vérifier cette loi sur les caractéritisques des villes françaises.

Ces caractéristiques sont regroupées dans un fichier `CSV` dont voici quelques descripteurs :

|descripteur      | détails                         |
|:---------------:|:-------------------------------:|
|`"dep "`         |     Code du département	        |
|`"nom "`         |     Nom de la ville	            |
|`"cp "`          |     Code postal	                |
|`"nb_hab_2012 "` |    Nombre d'habitants en 2012	|
|`"surf "`        |    Superficie totale         	|

Les données sont extraits du fichier pour être placées dans un tableau de dictionnaires, dont les clés correspondent aux descripteurs.


??? question "Listes de valeurs "
    
    Ecrire la fonction `serie_nombres` qui prend en paramètres des données sous forme de tableau de dictionnaires ainsi qu'une chaine de caractère `descripteur` et renvoie un tableau contenant les données correspondant au descripteur donné.

    ```pycon title=""
    >>> villes_exemple = [{"dep": "64", "nom":"Anglet", "surf":"26.93"},
             ...         {"dep": "64", "nom":"Biarritz", "surf":"11.66"},
             ...         {"dep": "64", "nom":"Bayonne", "surf":"21.68"}]
    >>> serie_nombres(villes_exemple, "surf")
    ["26.93", "11.66", "21.68"]
    ```

    {{ IDE('exo') }}


??? question "Fréquences des chiffres"

    Ecrire la fonction `frequences_chiffre` qui prend en paramètres un tableau de nombres (entiers ou à virgule) sous forme de chaines de caractères et renvoie un tableau de $10$ valeurs correspondant au pourcentage d'apparition de chacun des chiffre parmi les donnés.

    ```pycon title=""
    >>> donnees_1 = ["1.0", "23", "4", "5.6", "78", "9"]
    >>> frequences_chiffre(donnees_1) 
    [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    >>> donnees_2 = ["1", "1", "1"]
    >>> frequences_chiffre(donnees_2) 
    [0, 100, 0, 0, 0, 0, 0, 0, 0, 0]
    ```

    === "Version vide"
        {{ IDE('exo2') }}
    === "Version à trous"
        {{ IDE('exo2b') }}

!!! exercise "Visualisation"

    Le code suivant doit permettre de vérifier graphiquement la loi.

    Les fonction `serie_nombres` et `frequences_chiffre` sont déjà chargées en mémoire.

    La procédure `affiche_histogramme` permet d'afficher les fréquences sous la forme d'un histogramme.

    {{ IDE('visu') }}
    {{ figure("matplot_fig",
            inner_text="L'histogramme s'affichera ici", 
            admo_title="Histogramme")}}