# --------- PYODIDE:env --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def taille(self):
        pass

    def hauteur(self):
        pass

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def taille(self):
        pass

    def hauteur(self):
        pass

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:code --------- #
ab_3 = AB(3)
ab_1 = AB(1, ab_3)
...
ab_int = ...
print("ab_int:", ab_int)

h = AB("h")
...
ab_str = ...
print("ab_str:", ab_str)
# --------- PYODIDE:corr --------- #
ab_3 = AB(3)
ab_1 = AB(1, ab_3)
ab_2 = AB(2, AB(4), AB(5))
ab_int = AB(0, ab_1, ab_2)

h = AB("h")
k = AB("k")
e = AB("e", h)
f = AB("f", ABVide(), k)
b = AB("b", ABVide(), e)
c = AB("c", f)
ab_str = AB("a", b, c)

# --------- PYODIDE:secrets --------- #
attendu = "(((∅, 3, ∅), 1, ∅), 0, ((∅, 4, ∅), 2, (∅, 5, ∅)))"
assert str(ab_int) == attendu
attendu = "((∅, b, ((∅, h, ∅), e, ∅)), a, ((∅, f, (∅, k, ∅)), c, ∅))"
assert str(ab_str) == attendu, "Erreur sur abr_str"
