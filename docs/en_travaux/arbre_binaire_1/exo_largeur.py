# --------- PYODIDE:env --------- #
from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.appendleft(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.pop()

    def __str__(self):
        """Convertit la file en une chaîne de caractères"""
        if self.est_vide():
            return "∅"
        return f"(queue) {' -> '.join(str(x) for x in self.valeurs)} (tête)"

class ABVide:
    def est_vide(self): return True
    def __str__(self): return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self): return False
    def __str__(self): return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:code --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def largeur(self):
        ...

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def largeur(self):
        ...

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:corr --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def largeur(self):
        return []

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def largeur(self):
        resultat = [self.valeur]
        file = File()
        file.enfile(self.gauche)
        file.enfile(self.droit)
        while not file.est_vide():
            a = file.defile()
            if not a.est_vide():
                resultat.append(a.valeur)
                file.enfile(a.gauche)
                file.enfile(a.droit)
        return resultat


    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:tests --------- #
vide = ABVide()
ab = AB(0, AB(1), AB(2, AB(3)))
assert vide.largeur() == []
assert ab.largeur() == [0, 1, 2, 3]
# --------- PYODIDE:secrets --------- #
a = AB(0, AB(1, AB(2, AB(3, AB(4)))))
attendu = [0, 1, 2, 3, 4]
assert a.largeur() == attendu, f"Erreur avec {a = }"
b = AB(17, AB(65), AB(12, AB(83)))
attendu = [17, 65, 12, 83]
assert b.largeur() == attendu, f"Erreur avec {b = }"
c = AB(100, AB(17), AB(20))
attendu = [100, 17, 20]
assert c.largeur() == attendu, f"Erreur avec {c = }"
d = AB(100, None, AB(20))
attendu = [100, 20]
assert d.largeur() == attendu, f"Erreur avec {d = }"
e = AB(100, AB(16))
attendu = [100, 16]
assert e.largeur() == attendu, f"Erreur avec {e = }"
f = AB(50)
f.gauche = e
f.droit = d
e.gauche = c
d.gauche = b
c.droit.droit = a
attendu = [50, 100, 100, 100, 17, 20, 17, 20, 65, 12, 0, 83, 1, 2, 3, 4]
assert f.largeur() == attendu, f"Erreur avec {f = }"