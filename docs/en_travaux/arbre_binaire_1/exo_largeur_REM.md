Il est également possible de "traiter" chaque valeur juste après le défilement, aboutissant à cette solution pour la méthode `largeur` de la classe `AB` :

```python title=""
    def largeur(self):
        resultat = []
        file = File()
        file.enfile(self)
        while not file.est_vide():
            a = file.defile()
            resultat.append(a.valeur)
            for enfant in (a.gauche, a.droit):
                if not enfant.est_vide():
                    file.enfile(enfant)
        return resultat
```

Ou encore, sans la boucle `#!py for` :

```python title=""
    def largeur(self):
        resultat = []
        file = File()
        file.enfile(self)
        while not file.est_vide():
            a = file.defile()
            resultat.append(a.valeur)
            if not a.gauche.est_vide():
                file.enfile(a.gauche)
            if not a.droit.est_vide():
                file.enfile(a.droit)
        return resultat
```