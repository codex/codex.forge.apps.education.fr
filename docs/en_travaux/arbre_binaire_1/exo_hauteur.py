# --------- PYODIDE:env --------- #
class ABVide:
    def est_vide(self): return True
    def __str__(self): return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self): return False
    def __str__(self): return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:code --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def hauteur(self):
        ...

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def hauteur(self):
        ...

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:corr --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def hauteur(self):
        return 0

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def hauteur(self):
        return 1 + max(self.gauche.hauteur(), self.droit.hauteur())

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:tests --------- #
vide = ABVide()
ab = AB(0, AB(1), AB(2, AB(3)))
assert vide.hauteur() == 0
assert ab.hauteur() == 3
# --------- PYODIDE:secrets --------- #
a = AB(0, AB(1, AB(2, AB(3, AB(4)))))
attendu = 5
assert a.hauteur() == attendu, f"Erreur avec {a = }"
b = AB(17, AB(65), AB(12, AB(83)))
attendu = 3
assert b.hauteur() == attendu, f"Erreur avec {b = }"
c = AB(120, AB(17), AB(20))
attendu = 2
assert c.hauteur() == attendu, f"Erreur avec {c = }"
d = AB(39, None, AB(23))
attendu = 2
assert d.hauteur() == attendu, f"Erreur avec {d = }"
e = AB(540, AB(16))
attendu = 2
assert e.hauteur() == attendu, f"Erreur avec {e = }"
f = AB(50)
f.gauche = e
f.droit = d
e.gauche = c
d.gauche = b
c.droit.droit = a
attendu = 9
assert f.hauteur() == attendu, f"Erreur avec {f = }"
