Il est à noter que la complexité en temps de cette approche n'est pas optimale, puisqu'on utilise des concaténations de listes.

Une possibilité pour éviter cela est d'utiliser une fonction annexe depuis laquelle on muterait toujours la même liste. Dans ce cas, la complexité en temps finale est $O(N)$ (pour N valeurs dans l'arbre).

```python title=""
    def prefixe(self):
        return self._prefixe_aux([])
    
    def _prefixe_aux(self, lst):
        # La méthode _prefixe_aux doit aussi être implémentée dans la classe ABVide.
        if not self.est_vide():
            lst.append(self.valeur)
            self.gauche._prefixe_aux(lst)
            self.droit._prefixe_aux(lst)
        return lst
```