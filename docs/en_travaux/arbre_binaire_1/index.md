---
author:
    - Nicolas Revéret
    - Frédéric Zinelli
hide:
    - navigation
    - toc
title: Autour des arbres binaires (1)
tags:
    - en travaux
    - arbre binaire
    - programmation orientée objet
    - récursivité
difficulty: 205
maj: 27/02/2025
---

??? info "Différentes représentations"

    Cet exercice demande d'écrire les fonctions calculant la taille et hauteur d'un arbre binaire.
    Il fait partie d'une série dans laquelle on utilise différentes représentations :

    * {{ lien_exo("Représentation avec une classe décrivant l'arbre vide", "arbre_binaire_1") }} (cet exercice) ;
    * {{ lien_exo("Représentation avec une classe décrivant les nœuds", "arbre_binaire_2") }}.

On rappelle qu'un arbre binaire est :

* soit *vide* ;
* soit composé d'un nœud appelé racine et portant une valeur, et de deux sous-arbres appelés *sous-arbre gauche* et *sous-arbre droit*.

On représente dans cet exercice les arbres binaires vides par une instance de la classe `ABVide`, ne possédant aucun attribut.

On représente quant à eux les arbres binaires non vides par une classe `AB` caractérisée par trois attributs :

* `#!py valeur` est à la valeur portée par la racine ;
* `#!py gauche` est un objet de type `#!py AB` ou `#!py ABVide`, et représente le sous-arbre gauche de l'arbre ;
* `#!py droit` est un objet de type `#!py AB` ou `#!py ABVide`, et représente le sous-arbre droit de l'arbre.


??? info "Les classes `ABVide` et `AB`"

    ```python title=""
    class ABVide:
        def __init__(self):
            pass

        def est_vide(self):
            return True

        def __str__(self):
            return "∅"

    class AB:
        def __init__(self, valeur, gauche=None, droit=None):
            self.valeur = valeur
            if gauche is None:
                gauche = ABVide()
            if droit is None:
                droit = ABVide()
            self.gauche = gauche
            self.droit = droit

        def est_vide(self):
            return False

        def __str__(self):
            return f"({self.gauche}, {self.valeur}, {self.droit})"
    ```

??? question "Créer un arbre binaire"

    Les deux classes `#!py ABVide` et `#!py AB` décrites ci-dessus sont déjà importées dans l'éditeur.

    Utiliser ces classes afin de représenter les deux arbres binaires ci-dessous (par commodité, on ne représente pas les arbres vides) :

    <center>

    ```mermaid
    flowchart TD
        A0((0))
        B0((1))
        C0((2))
        D0((3))
        E0[ ]
        F0((4))
        G0((5))

        A0 --> B0
        A0 --> C0
        B0 --> D0
        B0 ~~~ E0
        C0 --> F0
        C0 --> G0

        a((a))
        b((b))
        c((c))
        d(( ))
        e((e))
        f((f))
        g(( ))
        h((h))
        i(( ))
        j(( ))
        k((k))

        a --> b
        a --> c
        b ~~~ d
        b --> e
        c --> f
        c ~~~ g
        e --> h
        e ~~~ i
        f ~~~ j
        f --> k

        style E0 fill:none, stroke:none
        style d fill:none, stroke:none
        style g fill:none, stroke:none
        style i fill:none, stroke:none
        style j fill:none, stroke:none
    ```

    </center>

    `#!py ab_int` sera l'arbre portant des nombres entiers, `ab_str` celui portant des caractères.

    {{ IDE('exo_utilisation') }}

??? question "Taille"

    Compléter les méthodes `#!py taille` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la taille de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.taille()
        0
        >>> ab.taille()
        4
        ```

    {{ IDE('exo_taille') }}

??? question "Hauteur"

    Compléter les méthodes `#!py hauteur` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la taille de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.hauteur()
        0
        >>> ab.hauteur()
        3
        ```

    {{ IDE('exo_hauteur') }}

??? question "Parcours en largeur"

    Compléter les méthodes `#!py largeur` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la liste des valeurs des nœuds rencontrés lors du parcours en largeur de l'arbre binaire représenté par l'objet en question.

    La classe `#!py File` décrite ci-dessous est déjà importée dans l'éditeur.

    {{ remarque('classe_File') }}

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.largeur()
        []
        >>> ab.hauteur()
        [0, 1, 2, 3]
        ```

    {{ IDE('exo_largeur') }}

??? question "Parcours préfixe"

    Compléter les méthodes `#!py prefixe` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la liste des valeurs des nœuds rencontrés lors du parcours en profondeur préfixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.prefixe()
        []
        >>> ab.prefixe()
        [0, 1, 2, 3]
        ```

    {{ IDE('exo_prefixe') }}

??? question "Parcours infixe"

    Compléter les méthodes `#!py infixe` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la liste des valeurs des nœuds rencontrés lors du parcours en profondeur infixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.infixe()
        []
        >>> ab.prefixe()
        [1, 0, 3, 2]
        ```

    {{ IDE('exo_infixe') }}

??? question "Parcours suffixe"

    Compléter les méthodes `#!py suffixe` des deux classes `#!py ABVide` et `#!py AB`. Ces deux méthodes renvoient la liste des valeurs des nœuds rencontrés lors du parcours en profondeur suffixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = ABVide()
        >>> ab = AB(0, AB(1), AB(2, AB(3)))
        >>> vide.suffixe()
        []
        >>> ab.suffixe()
        [0, 1, 2, 3]
        ```

    {{ IDE('exo_suffixe') }}

<br>

---

<br>

??? note "Compléments"

    L'implémentation utilisée ici propose une particularité en termes de codage : si on observe attentivement les différentes méthodes qui ont été implémentées, on se rend compte que :

    * Les méthodes de la classe `ABVide` ne comportent que les cas de base des algorithmes récursifs.
    * Les méthodes de la classe `AB` ne comportent que les cas récursifs des algorithmes.

    On n'a donc plus besoin ici d'utiliser de conditions, car c'est la construction de l'arbre, avec un type d'objet ou l'autre, qui va d'une certaine façon "coder en dur" les conditions des algorithmes directement dans la structure de données.

    <br>

    ---

    <br>

    Il est possible de gagner un peu en empreinte mémoire sur ce type d'approche en réutilisant toujours le même objet `ABVide()` pour tous les arbres vides. Cet objet est alors un "singleton". Il peut par exemple être déclaré en tant que constante globale (ici, `VIDE`) et est ensuite utilisé dans le constructeur des objets `ABVide()` lorsqu'il manque l'un ou l'autre des sous-arbres en argument.

    ```python title=""
    class ABVide:
        def est_vide(self):
            return True

        def __str__(self):
            return "∅"

    VIDE = ABVide()

    class AB:
        def __init__(self, valeur, gauche=None, droit=None):
            self.valeur = valeur
            self.gauche = VIDE if gauche is None else gauche
            self.droit = VIDE if droit is None else droit
    ```

    _Remarque : il n'est en fait pas nécessaire d'implémenter la méthode `__init__` dans la classe `ABVide`, puisque rien n'y est fait de particulier._