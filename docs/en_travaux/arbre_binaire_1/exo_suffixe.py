# --------- PYODIDE:env --------- #
class ABVide:
    def est_vide(self): return True
    def __str__(self): return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self): return False
    def __str__(self): return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:code --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def suffixe(self):
        ...

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def suffixe(self):
        ...

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:corr --------- #
class ABVide:
    def __init__(self):
        pass

    def est_vide(self):
        return True

    def suffixe(self):
        return []

    def __str__(self):
        return "∅"


class AB:
    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        if gauche is None:
            gauche = ABVide()
        if droit is None:
            droit = ABVide()
        self.gauche = gauche
        self.droit = droit

    def est_vide(self):
        return False

    def suffixe(self):
        return self.gauche.suffixe() + self.droit.suffixe() + [self.valeur]

    def __str__(self):
        return f"({self.gauche}, {self.valeur}, {self.droit})"


# --------- PYODIDE:tests --------- #
vide = ABVide()
ab = AB(0, AB(1), AB(2, AB(3)))
assert vide.suffixe() == []
assert ab.suffixe() == [1, 3, 2, 0]
# --------- PYODIDE:secrets --------- #
a = AB(0, AB(1, AB(2, AB(3, AB(4)))))
attendu = [4, 3, 2, 1, 0]
assert a.suffixe() == attendu, f"Erreur avec {a = }"
b = AB(17, AB(65), AB(12, AB(83)))
attendu = [65, 83, 12, 17]
assert b.suffixe() == attendu, f"Erreur avec {b = }"
c = AB(100, AB(17), AB(20))
attendu = [17, 20, 100]
assert c.suffixe() == attendu, f"Erreur avec {c = }"
d = AB(100, None, AB(20))
attendu = [20, 100]
assert d.suffixe() == attendu, f"Erreur avec {d = }"
e = AB(100, AB(16))
attendu = [16, 100]
assert e.suffixe() == attendu, f"Erreur avec {e = }"
f = AB(50)
f.gauche = e
f.droit = d
e.gauche = c
d.gauche = b
c.droit.droit = a
attendu = [17, 4, 3, 2, 1, 0, 20, 100, 100, 65, 83, 12, 17, 20, 100, 50]
assert f.suffixe() == attendu, f"Erreur avec {f = }"
