Il est aussi possible de déclarer les arbre en une seule fois. Les deux méthodes ont leurs avantages et inconvénients :

* La déclaration en une fois permet de voir la structure de l'arbre facilement, si elle est écrite en jouant avec l'indentation.
* ...mais il est très facile de déclarer le mauvais arbre, car il est rapidement délicat de rester cohérent dans le placement des virgules et parenthèses dans la déclaration.

* La déclaration étape par étape facilite grandement l'écriture correcte de chaque objet.
* ...mais il est très difficile de se rendre compte de la structure de l'arbre final simplement en lisant la déclaration.

Ici, les déclarations "en une fois" pourraient être écrites comme suit :

```python
ab_int = AB(0,
            AB(1,
               AB(3)
            ),
            AB(2,
               AB(4),
               AB(5)
            )
        )

ab_str = AB("a",
             AB('b',
                 None,
                 AB("e",
                     AB('h')
                 )
             ),
             AB('c',
                 AB('f',
                     None,
                     AB('k')
                )
             )
         )
```