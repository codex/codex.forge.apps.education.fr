---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Courses de rentrée
tags:
    - en travaux
    - programmation orientée objet
difficulty: 210
maj: 23/09/2024
---
# Courses de rentrée


On travaille dans cet exercice sur la classe `Article` qui permet de décrire un article (trousse, gomme...) à acheter.

```mermaid
classDiagram
class Article{
       description : str
       prix : float
       reduction : int
       prix_apres_reduction() float
}
```

Un `article`  est caractérisé par sa `description` de type `str`, son `prix` de type `float`, et sa `remise` de type `int`, exprimée en pourcentage.

Cette classe dispose de la méthode `article.prix_apres_reduction` qui renvoie la valeur du prix auquel on a appliqué une réduction, le prix final étant arrondi au centime.

Le code de cette classe est donné ci-dessous :

```python
class Article:
    def __init__(self, description, prix):
        """ Création d'un nouvel article """
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        """ Renvoie le prix de l'article après réduction """
        return round(self.prix*(1-self.remise/100), 2)
```

??? question "Question 1"

    En utilisant cette classe `Article`, **créer** un tableau `courses` contenant les articles suivants dont on donne les valeurs des attributs `description` et `prix` :

    * un "cartable" à 45 €
    * une "boite de crayons de couleurs" à 9,95 €
    * une "trousse" à 12 €

    !!! warning "Descriptions"
        Attention à bien renseigner pour chaque article la description et le prix donnés ci-dessus.


    {{ IDE('exo_a') }}

??? question "Question 2"
    **Créer** une fonction `somme_articles` qui prend en paramètre une liste d'articles et renvoie le prix total des courses, arrondi au centime près.

    ```pycon
    >>> somme_articles(courses)
    66.95
    ```
    {{ IDE('exo_b') }}

??? question "Question 3"
    Le magasin propose 10% de remise sur les cartables et $20$% sur les trousses. Par contre, il n'y a pas de remise sur les boites de crayons de couleur.

    **Modifier** la valeur des remises sur les articles concernés.

    {{ IDE('exo_c') }}


??? question "Question 4"
    Ces remises ne s'appliquent que si le montant des courses est supérieur à $60$ €.

    **Créer** une fonction `somme_articles_reduction` qui prend en paramètre une liste d'articles et renvoie le total des courses, réduction déduite.

    On pourra utiliser la fonction `somme_articles`, déjà chargée en mémoire.

    La somme sera arrondie au centime près.

    ```pycon
    >>> somme_articles_reduction(courses)
    60.05
    ```
    {{ remarque('round') }}

    {{ IDE('exo_d') }}
