
# --------- PYODIDE:env --------- #

class Article:
    def __init__(self, description, prix):
        """Création d'un nouvel article"""
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        """Renvoie le prix de l'article après réduction"""
        return round(self.prix*(1-self.remise/100), 2)

cartable = Article("erreur", 0)
crayons = Article("erreur", 0)
trousse = Article("erreur", 0)
courses = []

# --------- PYODIDE:code --------- #


cartable = Article(..., ...)
crayons = ...
trousse = ...
courses = [..., ..., ...]



# --------- PYODIDE:corr --------- #

cartable = Article("cartable", 45)
crayons = Article("boite de crayons de couleurs", 9.95)
trousse = Article("trousse", 12)
courses = [cartable, crayons, trousse]

cartable.remise = 10
trousse.remise = 20


# --------- PYODIDE:tests --------- #

assert cartable.prix == 45
assert crayons.prix ==  9.95
assert courses[2].description == "trousse"
# --------- PYODIDE:secrets --------- #
from operator import attrgetter
def somme_articles(liste_de_courses):
    somme = 0
    for article in liste_de_courses:
        somme += article.prix
    return round(somme, 2)

assert trousse.prix ==  12
assert somme_articles(courses) == 66.95

attendu = [("boite de crayons de couleurs", 9.95), ("cartable", 45),  ("trousse", 12)]
courses.sort(key=attrgetter('description'))
for i in range(3):
    nom, prix = attendu[i]
    assert courses[i].description == nom
    assert courses[i].prix == prix
