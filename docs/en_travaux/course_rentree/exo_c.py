
# --------- PYODIDE:env --------- #

class Article:
    def __init__(self, description, prix):
        """Création d'un nouvel article"""
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        """Renvoie le prix de l'article après réduction"""
        return round(self.prix*(1-self.remise/100), 2)

cartable = Article("cartable", 45)
crayons = Article("boite de couleurs", 9.95)
trousse = Article("trousse", 12)
courses = [cartable, crayons, trousse]

# --------- PYODIDE:code --------- #

...

# --------- PYODIDE:corr --------- #

cartable.remise = 10
trousse.remise = 20


# --------- PYODIDE:secrets --------- #

assert trousse.remise == 20, "La valeur de l'attribut remise de l'objet trousse est incorrecte"
assert cartable.remise == 10, "La valeur de l'attribut remise de l'objet cartable est incorrecte"