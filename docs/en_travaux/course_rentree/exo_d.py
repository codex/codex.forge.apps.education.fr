
# --------- PYODIDE:env --------- #

class Article:
    def __init__(self, description, prix):
        """Création d'un nouvel article"""
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        """Renvoie le prix de l'article après réduction"""
        return round(self.prix*(1-self.remise/100), 2)

cartable = Article("cartable", 45)
crayons = Article("boite de couleurs", 9.95)
trousse = Article("trousse", 12)
courses = [cartable, crayons, trousse]
courses2 = [cartable, trousse]

cartable.remise = 10
trousse.remise = 20

def somme_articles(liste_de_courses):
    somme = 0
    for article in liste_de_courses:
        somme += article.prix
    return round(somme, 2)


# --------- PYODIDE:code --------- #


def somme_articles_reduction(liste_de_courses):
    ...


# --------- PYODIDE:corr --------- #



def somme_articles_reduction(liste_de_courses):
    somme = somme_articles(liste_de_courses)
    if somme > 60:
        somme = 0
        for article in liste_de_courses:
            somme += article.prix_apres_reduction()
        return round(somme, 2)
    else:
        return somme

# --------- PYODIDE:tests --------- #


assert somme_articles_reduction(courses) == 60.05
# test avec uniquement le cartable et la trousse
assert somme_articles_reduction(courses2) == 57

# --------- PYODIDE:secrets --------- #

cartable = Article("cartable", 45)
crayons = Article("boite de couleurs", 9.95)
trousse = Article("trousse", 12)
courses = [cartable, crayons, trousse]


#autres tests
cartable.remise = 0
trousse.remise = 0
assert somme_articles_reduction(courses) == 66.95
cartable.remise = 100
trousse.remise = 100
assert somme_articles_reduction(courses) == 9.95
crayons.remise = 30
assert somme_articles_reduction(courses) == 6.96
courses = []
assert somme_articles_reduction(courses) == 0
feutres = Article("trousse", 10)
feutres.remise = 50
cartable.remise = 10
trousse.remise = 20
crayons.remise = 0
courses = [cartable, crayons, trousse, feutres]
assert somme_articles_reduction(courses) == 65.05