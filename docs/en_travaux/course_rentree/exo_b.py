
# --------- PYODIDE:env --------- #

class Article:
    def __init__(self, description, prix):
        """Création d'un nouvel article"""
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        """Renvoie le prix de l'article après réduction"""
        return round(self.prix*(1-self.remise/100), 2)

cartable = Article("cartable", 45)
crayons = Article("boite de couleurs", 9.95)
trousse = Article("trousse", 12)
courses = [cartable, crayons, trousse]
courses2 = [cartable, trousse]


# --------- PYODIDE:code --------- #

def somme_articles(liste_de_courses):
    ...



# --------- PYODIDE:corr --------- #



def somme_articles(liste_de_courses):
    somme = 0
    for article in liste_de_courses:
        somme += article.prix
    return round(somme, 2)


# --------- PYODIDE:tests --------- #

assert somme_articles(courses) == 66.95
# test avec uniquement le cartable et la trousse
assert somme_articles(courses2) == 57


# --------- PYODIDE:secrets --------- #


#autres tests
courses = []
assert somme_articles(courses) == 0
cartable = Article("cartable", 45)
crayons = Article("boite de couleurs", 9.95)
feutres = Article("trousse", 10)
feutres.remise = 50
cartable.remise = 10
trousse.remise = 20
crayons.remise = 0
courses = [cartable, crayons, trousse, feutres]
assert somme_articles(courses) == 76.95