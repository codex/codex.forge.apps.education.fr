import geometry;
unitsize(1cm);
defaultpen(fontsize(16));
settings.outformat="svg";

real h = .7;
real l = .9;

int[][] tableau = {{0, 1, 1, 0},
                   {1, 1, 1, 0},
                   {0, 1, 0, 1},
                   {0, 0, 1, 0}};

int i_spe = -1;
int j_spe = -1; 

for (int i=0; i<4 ; ++i) {
    for (int j=0; j<4 ; ++j) {
        if ((i == i_spe) && (j == j_spe)) {
            label("\textbf{"+(string)tableau[i][j]+"}", ((j+.5)*l,(i+.5)*h));
        } else {
            label((string)tableau[i][j], ((j+.5)*l,(i+.5)*h));
        }
    }
}

label("M =", (-.2l,2*h), W);

draw(box((0, 0), (4*l, 4*h)));

bool tout_dessiner = false;

for (int i=0; i<4; ++i) {
    for (int j=0; j<4; ++j) {
        if ((i<3) && ((tout_dessiner) || (tableau[i][j] != tableau[i+1][j]))) {
            draw(shift((j*l,(i+1)*h))*((0,0)--(l,0)));
        }
        if ((j<3) && ((tout_dessiner) || (tableau[i][j] != tableau[i][j+1]))) {
            draw(shift(((j+1)*l,i*h))*((0,0)--(0,h)));
        }
    }
}
