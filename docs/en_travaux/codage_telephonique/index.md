---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Codage téléphonique
tags:
    - en travaux
    - dictionnaire
    - string
    - pile
    - récursivité
maj: 08/12/2024
difficulty: 301
---

Certains claviers de téléphones, virtuels ou « physiques », associent à chaque touche des caractères comme illustré sur la figure ci-dessous :

![Codage téléphonique](Telephone-keypad2.svg){.center width=25%}

Il est ainsi possible d'encoder un message sous la forme d'un nombre. Toutefois, le message ne doit être constitué que de caractères alphabétiques, écrits en minuscule et non accentués. Il est aussi possible de représenter l'espace en utilisant le numéro `#!py 0`. Dans toute la suite, on désignera par *caractères valides* ces caractères utilisables dans les messages.

Ce codage permet donc d'écrire le message `#!py "bonjour le monde"` sous la forme du nombre `#!py 2665687053066633`.

On souhaite utiliser cette méthode afin d'encoder et de décoder des messages.

??? question "1. Représenter le codage"

    On décide de représenter le codage illustré plus haut par un dictionnaire Python associant à chaque caractère valide, l'entier qui lui correspond.
    
    Ce dictionnaire sera nommé `#!py CARACTERE_CHIFFRE` et contiendra tous les caractères valides (lettres en minuscules et non accentuées, espace). On aura donc `#!py CARACTERE_CHIFFRE["a"] = 2` et `#!py CARACTERE_CHIFFRE[" "] = 0`.
    
    Compléter le dictionnaire `#!py CARACTERE_CHIFFRE`.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> CARACTERE_CHIFFRE["a"]
        2
        >>> CARACTERE_CHIFFRE[" "]
        0
        ```

    {{ IDE('./pythons/exo_dico') }}

??? question "2. Encoder un message"

    On fournit le dictionnaire `#!py CARACTERE_CHIFFRE` décrit dans la question précédente. **Ce dictionnaire est déjà chargé dans l'éditeur.**
    
    Écrire la fonction `encode` qui prend en paramètre une chaîne de caractères `#!py message` non vide et renvoie le nombre la représentant.
    
    On garantit que `#!py message` ne contient que des caractères valides.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> encode("voleur")
        865387
        >>> encode("volets")
        865387
        >>> encode("bonjour le monde")
        2665687053066633
        ```
    
    {{ IDE('./pythons/exo_codage') }}
    
    ??? tip "Aide"
    
        Le code représentant le message `#!py "abeille"` est égal à `#!py 10` fois celui représentant le message `#!py "abeill"` augmenté du code représentant `#!py "e"`.
        
        
??? question "3.1 Décoder un nombre (`#!py str`)"

    !!! note "`#!py type(code) is str`"
        
        Dans cette question, on représente les codes sous forme de chaînes de caractères.

    L'opération inverse est plus problématique : en effet, les nombres `#!py 2` à `#!py 9` correspondent chacun à plusieurs caractères.
    
    Le même code peut donc être interprété comme plusieurs messages distincts. C'est le cas par exemple du code `#!py "865387"` qui représente, entre autres, les messages `#!py "voleur"` et `#!py "volets"`.
    
    On fournit le dictionnaire `#!py CHIFFRE_CARACTERE = {"0": " ", "2": "abc", "3": "def", "4": "ghi", "5": "jkl", "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}` qui associe à chaque chiffre valide, les caractères représentés. Le chiffre `#!py "1"` ne représente aucun caractère ce qui explique qu'il n'apparaisse pas dans le dictionnaire.

    Écrire la fonction `decode` qui prend en paramètre un code valide et renvoie la liste des chaînes de caractères représentées par cet entier.
    
    On garantit que le code passé en paramètre compte moins de 5 chiffres valides et ne contient aucun chiffre `#!py "1"` (qui ne représente aucun caractère valide).
    
    La liste renvoyée pourra contenir des messages **dans n'importe quel ordre** : les tests compareront les versions triées des différentes listes.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> decode("6")
        ['m', 'n', 'o']
        >>> # ['n', 'o', 'm'] serait aussi accepté
        >>> decode("58")
        ['jt', 'ju', 'jv', 'kt', 'ku', 'kv', 'lt', 'lu', 'lv']
        ```
    
    {{ IDE('./pythons/exo_decodage_str') }}
    
    ??? tip "Aide (1)"
    
        On pourra utiliser :
        
        * soit une fonction récursive prenant en paramètres le code, l'indice du chiffre à lire dans celui-ci et un message en cours de construction ;
        * soit approche itérative utilisant une pile contenant des couples `#!py (indice du chiffre à lire dans le code, message)`. 
    
    ??? tip "Aide (2)"
    
        Quelle que soit l'approche choisie, un message est entièrement construit lorsque l'indice du chiffre à lire est égal à la longueur du code.
        
        On l'ajoute alors une liste de résultats.
        
        
    ??? tip "Aide (3)"
    
        Dans le cas où l'indice du chiffre à lire est strictement inférieur à la longueur du code, on lit ce chiffre.
        
        On ajoute alors chaque caractère qui lui est associé au message et, pour chaque message ainsi construit :
        
        1. (approche récursive) on lance un appel récursif avec comme paramètres le code, l'indice suivant et le nouveau message ;
        2. (approche itérative) on empile le couple formé de l'indice suivant et de ce nouveau message.

        Dans le cas de la pile, le traitement est terminé lorsque la pile est vide.

??? question "3.2 Décoder un nombre (`#!py int`)"

    !!! note "`#!py type(code) is int`"
        
        Dans cette question, on représente les codes sous forme de nombres entiers.

    L'opération inverse est plus problématique : en effet, les chiffres de `#!py 2` à `#!py 9` correspondent chacun à plusieurs caractères.
    
    Le même code peut donc être interprété comme plusieurs messages distincts. C'est le cas par exemple du nombre `#!py 865387` qui représente, entre autres, les messages `#!py "voleur"` et `#!py "volets"`.
    
    On fournit la liste `#!py CHIFFRE_CARACTERE = [" ", None, "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]` qui associe à chaque numéro/indice, les caractères représentés. Le nombre `#!py 1` ne représente aucun caractère ce qui explique que l'on ait `#!py CHIFFRE_CARACTERE[1] = None`.

    Écrire la fonction `decode` qui prend en paramètre un entier `#!py 0 < code < 10**4` et renvoie la liste des chaînes de caractères représentées par cet entier.
    
    On garantit que le code passé en paramètre est strictement positif et ne contient aucun chiffre `#!py 1` (qui ne représente aucun caractère valide).
    
    La liste renvoyée pourra contenir des messages **dans n'importe quel ordre** : les tests compareront les versions triées des différentes listes.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> decode(6)
        ['m', 'n', 'o']
        >>> # ['n', 'o', 'm'] serait aussi accepté
        >>> decode(58)
        ['jt', 'ju', 'jv', 'kt', 'ku', 'kv', 'lt', 'lu', 'lv']
        ```
    
    {{ IDE('./pythons/exo_decodage_int') }}
    
    ??? tip "Aide (1)"
    
        On pourra utiliser :
        
        * soit une fonction récursive prenant en paramètres le code et un message en cours de construction
        * soit une pile contenant des couples `#!py (code, message)`. 
    
    ??? tip "Aide (2)"
    
        Quelle que soit l'approche choisie, un message est entièrement construit lorsque le code envisagé est nul.
        
        On l'ajoute alors une liste de résultats.
        
        
    ??? tip "Aide (3)"
    
        Dans le cas où le code est non nul, on extrait son nombre des unités et celui des dizaines.
        
        On ajoute alors chaque caractère associé au chiffre des unités au message et, pour chaque message ainsi construit :
        
        1. (approche récursive) on lance un appel récursif avec comme paramètres le nombre de dizaines et ce nouveau message ;
        2. (approche avec une pile) on empile le couple formé du nombre de dizaines et de ce nouveau message.

        Dans le cas de la pile, le traitement est terminé lorsque la pile est vide.