# --------- PYODIDE:env --------- #
CARACTERE_CHIFFRE = {
    "a": 2,
    "b": 2,
    "c": 2,
    "d": 3,
    "e": 3,
    "f": 3,
    "g": 4,
    "h": 4,
    "i": 4,
    "j": 5,
    "k": 5,
    "l": 5,
    "m": 6,
    "n": 6,
    "o": 6,
    "p": 7,
    "q": 7,
    "r": 7,
    "s": 7,
    "t": 8,
    "u": 8,
    "v": 8,
    "w": 9,
    "x": 9,
    "y": 9,
    "z": 9,
    " ": 0,
}

CHIFFRE_CARACTERE = [
    " ",
    None,
    "abc",
    "def",
    "ghi",
    "jkl",
    "mno",
    "pqrs",
    "tuv",
    "wxyz",
]


def encode(message):
    pass


def decode(message):
    pass


# --------- PYODIDE:code --------- #
def decode(code):
    ...


# --------- PYODIDE:corr --------- #
def decode(code):
    messages = []
    pile = [(code, "")]
    while len(pile) != 0:
        code, message = pile.pop()
        if code == 0:
            messages.append(message)
        else:
            unites = code % 10
            dizaines = code // 10
            for caractere in CHIFFRE_CARACTERE[unites]:
                pile.append((dizaines, caractere + message))
    return messages


# --------- PYODIDE:tests --------- #
assert sorted(decode(6)) == ["m", "n", "o"]
assert sorted(decode(58)) == ["jt", "ju", "jv", "kt", "ku", "kv", "lt", "lu", "lv"]
# --------- PYODIDE:secrets --------- #
code = 234
attendu = [
    "adg",
    "adh",
    "adi",
    "aeg",
    "aeh",
    "aei",
    "afg",
    "afh",
    "afi",
    "bdg",
    "bdh",
    "bdi",
    "beg",
    "beh",
    "bei",
    "bfg",
    "bfh",
    "bfi",
    "cdg",
    "cdh",
    "cdi",
    "ceg",
    "ceh",
    "cei",
    "cfg",
    "cfh",
    "cfi",
]
assert sorted(decode(code)) == attendu, f"Erreur en décodant {code = }"
code = 5634
attendu = [
    "jmdg",
    "jmdh",
    "jmdi",
    "jmeg",
    "jmeh",
    "jmei",
    "jmfg",
    "jmfh",
    "jmfi",
    "jndg",
    "jndh",
    "jndi",
    "jneg",
    "jneh",
    "jnei",
    "jnfg",
    "jnfh",
    "jnfi",
    "jodg",
    "jodh",
    "jodi",
    "joeg",
    "joeh",
    "joei",
    "jofg",
    "jofh",
    "jofi",
    "kmdg",
    "kmdh",
    "kmdi",
    "kmeg",
    "kmeh",
    "kmei",
    "kmfg",
    "kmfh",
    "kmfi",
    "kndg",
    "kndh",
    "kndi",
    "kneg",
    "kneh",
    "knei",
    "knfg",
    "knfh",
    "knfi",
    "kodg",
    "kodh",
    "kodi",
    "koeg",
    "koeh",
    "koei",
    "kofg",
    "kofh",
    "kofi",
    "lmdg",
    "lmdh",
    "lmdi",
    "lmeg",
    "lmeh",
    "lmei",
    "lmfg",
    "lmfh",
    "lmfi",
    "lndg",
    "lndh",
    "lndi",
    "lneg",
    "lneh",
    "lnei",
    "lnfg",
    "lnfh",
    "lnfi",
    "lodg",
    "lodh",
    "lodi",
    "loeg",
    "loeh",
    "loei",
    "lofg",
    "lofh",
    "lofi",
]
assert sorted(decode(code)) == attendu, f"Erreur en décodant {code = }"
