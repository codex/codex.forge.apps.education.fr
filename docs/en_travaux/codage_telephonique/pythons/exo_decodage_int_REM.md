La correction proposée ci-dessous adopte la solution avec une pile.

L'approche récursive est proposée ci-dessous :

```python title=""
def decode(code):
    def aux(code, message):
        if code == 0:
            resultats.append(message)
        else:
            unites = code % 10
            dizaines = code // 10
            for caractere in CHIFFRE_CARACTERE[unites]:
                aux(dizaines, caractere + message)
    resultats = []
    aux(code, "")
    return resultats
```

On notera enfin que cette approche ne permet pas de gérer, en l'état, le code `#!py 0`. C'est néanmoins possible en ajoutant une sortie anticipée dès le début des fonctions :

```python title=""
def decode(code):
    if code == 0:
        return [" "]
    ...  # suite de la fonction
```
