# --------- PYODIDE:env --------- #
CARACTERE_CHIFFRE = {
    "a": 2,
    "b": 2,
    "c": 2,
    "d": 3,
    "e": 3,
    "f": 3,
    "g": 4,
    "h": 4,
    "i": 4,
    "j": 5,
    "k": 5,
    "l": 5,
    "m": 6,
    "n": 6,
    "o": 6,
    "p": 7,
    "q": 7,
    "r": 7,
    "s": 7,
    "t": 8,
    "u": 8,
    "v": 8,
    "w": 9,
    "x": 9,
    "y": 9,
    "z": 9,
    " ": 0,
}

CHIFFRE_CARACTERE = [
    " ",
    None,
    "abc",
    "def",
    "ghi",
    "jkl",
    "mno",
    "pqrs",
    "tuv",
    "wxyz",
]


def encode(message):
    pass


def decode(message):
    pass


# --------- PYODIDE:code --------- #
def encode(message):
    ...


# --------- PYODIDE:corr --------- #
def encode(message):
    resultat = 0
    for caractere in message:
        resultat = resultat * 10 + CARACTERE_CHIFFRE[caractere]
    return resultat


# --------- PYODIDE:tests --------- #
assert encode("voleur") == 865387
assert encode("volets") == 865387
assert encode("bonjour le monde") == 2665687053066633
# --------- PYODIDE:secrets --------- #
message = " "
attendu = 0
assert encode(message) == attendu, f"Erreur en encodant {message = }"
message = "x"
attendu = 9
assert encode(message) == attendu, f"Erreur en encodant {message = }"
message = "bel"
attendu = 235
assert encode(message) == attendu, f"Erreur en encodant {message = }"
message = "xo"
attendu = 96
assert encode(message) == attendu, f"Erreur en encodant {message = }"
message = "xor"
attendu = 967
assert encode(message) == attendu, f"Erreur en encodant {message = }"
message = "bonjour"
attendu = 2665687
assert encode(message) == attendu, f"Erreur en encodant {message = }"

