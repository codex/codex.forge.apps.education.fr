---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Diverses piles (POO)
difficulty: 250
tags:
    - en travaux
    - pile
    - programmation orientée objet
maj: 15/10/2024
---

??? note "Un exercice : deux versions"

    Cet exercice sur les piles est proposé en deux versions :
    
    * en programmation orientée objet (cet exercice) ;
    * {{ lien_exo("en modifiant directement les piles", "diverses_piles_alt") }}.


On s'intéresse sur cette page au type abstrait de données « *Pile* » que l'on représente à l'aide d'une classe `#!py Pile` dont on fournit le code ci-dessous.

??? tip "La classe `Pile`"

    ```python title=""
    class Pile:
        def __init__(self):
            """Initialise une pile vide"""
            self.valeurs = []

        def est_vide(self):
            """Renvoie un booléen : la pile est-elle vide ?"""
            return len(self.valeurs) == 0

        def empile(self, element):
            """Empile un élément au sommet de la pile"""
            self.valeurs.append(element)

        def depile(self):
            """
            Dépile un élément au sommet et le renvoie.
            Provoque une erreur si la pile est vide.
            """
            if self.est_vide():
                raise ValueError("Erreur, pile vide")
            else:
                return self.valeurs.pop()

        def __repr__(self):
            """Affiche la pile, en indiquant le sommet"""
            return f"| {' | '.join([str(x) for x in self.valeurs])} | <- sommet"

        def __eq__(self, autre):
            """
            Détermine si cette pile et l'autre sont égales
            (mêmes valeurs, dans le même ordre)
            """
            return self.valeurs == autre.valeurs
    ```

Cette classe possède deux méthodes spéciale `#!py __repr__` et `#!py __eq__` qui permettent :

* d'afficher le contenu de la pile (`#!py __repr__`) ;
* de tester l'égalité entre la pile étudiée et une autre pile passée en paramètre (`#!py __eq__`).

Les lignes ci-dessous présentent un exemple d'utilisation. **On prendra soin, dans les différents exercices, de n'utiliser que les méthodes présentées ci-dessous**.

??? tip "Utilisation"

    ```pycon title=""
    >>> p = Pile()    # création d'une pile vide
    >>> p.est_vide()  # la pile est-elle vide ? -> Oui
    True
    >>> p.empile(0)   # empile 0 dans la pile
    >>> p.est_vide()  # la pile est-elle vide ? -> Non
    False
    >>> p.empile(1)   # empile 0 dans la pile
    >>> p             # affichage du contenu de p
    | 0 | 1 | <- sommet
    >>> p.depile()    # dépile une valeur
    1
    >>> p
    | 0 | <- sommet
    >>> q = Pile()
    >>> q.empile(1)
    >>> q
    | 1 | <- sommet
    >>> p == q  # les piles p et q sont-elles égales ? -> Non
    False
    ```

Les exercices ci-dessous sont indépendants. Ils sont néanmoins présentés dans un certain ordre permettant d'aborder successivement plusieurs techniques.

??? warning "Jouons le jeu !"

    Ces exercices visent à utiliser certaines techniques classiques liées aux piles. Il est donc demandé de les traiter en se limitant à l'utilisation des méthodes la classe `#!py Pile`.
    
    Il est en particulier interdit d'accéder directement à l'attribut `#!py valeurs`.
    
    **Accéder ou modifier cet attribut fera échouer les tests secrets.**

??? question "Calculer la taille d'une pile"

    Écrire la fonction `#!py taille` qui prend en paramètre une pile et renvoie son nombre d'éléments.

    A l'issue du traitement, la pile doit avoir retrouvé son état initial.
    
    ```pycon title=""
    >>> p = Pile()
    >>> taille(p)
    0
    >>> p.empile("a")
    >>> p.empile("b")
    >>> taille(p)
    2
    ```
    
    ??? tip "Aide"
    
        On pourra utiliser une pile annexe.
        
        === "État initial"
            ![Etape 1](images/taille_1_transp.png){.center .autolight width=20%}
        === "1er transfert"
            ![Etape 2](images/taille_2_transp.png){.center .autolight width=20%}
        === "2nd transfert"
            ![Etape 3](images/taille_3_transp.png){.center .autolight width=20%}
        === "Retour à l'état initial"
            ![Etape 4](images/taille_4_transp.png){.center .autolight width=20%}
            
    
    === "Version vide"
        {{ IDE('pythons/exo_taille_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_taille_trous') }}

??? question "Renverser les deux valeurs au sommet"

    Écrire la fonction `#!py renverse_deux` qui prend en paramètre une pile et échange l'ordre des deux valeurs au sommet.

    La transformation se fait en place : on modifie directement sur la pile et il est donc inutile de la renvoyer.

    On garantit que la pile compte au moins deux éléments.
    
    ```pycon title=""
    >>> p = Pile()
    >>> p.empile("a")
    >>> p.empile("b")
    >>> p.empile("c")
    >>> p
    | a | b | c | <- sommet
    >>> renverse_deux(p)
    >>> p
    | a | c | b | <- sommet
    ```
    
    === "Version vide"
        {{ IDE('pythons/exo_renverse_deux_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_renverse_deux_trous') }}


??? question "Renverser les k valeurs au sommet"

    Écrire la fonction `#!py renverse_k` qui prend en paramètre une pile ainsi qu'un entier `#!py k` et renverse l'ordre des `#!py k` premiers éléments de la pile. Les autres éléments ne sont pas modifiés.

    La transformation se fait en place : on modifie directement sur la pile et il est donc inutile de la renvoyer.

    On garantit que la pile compte au moins `#!py k` éléments.
    
    
    ```pycon title=""
    >>> p = Pile()
    >>> p.empile("a")
    >>> p.empile("b")
    >>> p.empile("c")
    >>> p.empile("d")
    >>> p.empile("e")
    >>> p
    | a | b | c | d | e | <- sommet
    >>> renverse_k(p, 4)
    >>> p
    | a | e | d | c | b | <- sommet
    ```

    ??? tip "Aide"
    
        On pourra utiliser deux piles annexes.
        
        === "État initial"
            ![Etape 1](images/1_transp.png){.center .autolight width=20%}
        === "Fin de la 1ère phase"
            ![Etape 2](images/2_transp.png){.center .autolight width=20%}
        === "Fin de la 2nde phase"
            ![Etape 3](images/3_transp.png){.center .autolight width=20%}
        === "État final"
            ![Etape 4](images/4_transp.png){.center .autolight width=20%}
            
    
    === "Version vide"
        {{ IDE('pythons/exo_renverse_k_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_renverse_k_trous') }}


??? question "Fusionner deux piles de mêmes tailles"

    On considère `#!py p` et `#!py q` deux piles contenant le même nombre d'éléments.
    
    On appelle « *fusion* de `#!py p` et `#!py q` » la pile obtenue en empilant alternativement les valeurs dépilées de `#!py p` puis `#!py q`.

    Écrire la fonction `#!py fusion_simple` qui prend en paramètre deux piles **de même taille** et renvoie la pile résultante.

    Les piles `#!py p` et `#!py q` peuvent être modifiées lors du traitement.
    
    ```pycon title=""
    >>> p = Pile()
    >>> p.empile("c")
    >>> p.empile("a")
    >>> p
    | c | a | <- sommet
    >>> q = Pile()
    >>> q.empile("d")
    >>> q.empile("b")
    | d | b | <- sommet
    >>> fusion_simple(p, q)
    | a | b | c | d | <- sommet
    ```
    
    === "Version vide"
        {{ IDE('pythons/exo_fusion_simple_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_fusion_simple_trous') }}
        

??? question "Fusionner deux piles de tailles inconnues"

    On considère `#!py p` et `#!py q` deux piles dont on ignore le nombre d'éléments.
    
    On appelle « *fusion* de `#!py p` et `#!py q` » la pile obtenue en empilant alternativement les valeurs dépilées de `#!py p` puis `#!py q`.
    
    Les piles n'ayant pas nécessairement le même nombre d'éléments, il est possible lors de la fusion que l'une d'elles soit vide avant l'autre.
    Dans ce cas, **on terminera la fusion en vidant l'autre pile dans la pile résultante**.

    Écrire la fonction `#!py fusion` qui prend en paramètre deux piles **de tailles inconnues** et renvoie la pile résultante.

    Les piles `#!py p` et `#!py q` peuvent être modifiées lors du traitement.
    
    ```pycon title=""
    >>> p = Pile()
    >>> p.empile("e")
    >>> p.empile("c")
    >>> p.empile("a")
    >>> p
    | e | c | a | <- sommet
    >>> q = Pile()
    >>> q.empile("d")
    >>> q.empile("b")
    | d | b | <- sommet
    >>> fusion_simple(p, q)
    | a | b | c | d | e | <- sommet
    ```
    
    ??? tip "Aide"
    
        On pourra organiser le code en trois temps :
        
        * les deux piles sont non-vides ;
        * la pile `#!py p` est non-vide ;
        * la pile `#!py q` est non-vide.
    
    === "Version vide"
        {{ IDE('pythons/exo_fusion_vide') }}
    === "Version à compléter"
        {{ IDE('pythons/exo_fusion_trous') }}