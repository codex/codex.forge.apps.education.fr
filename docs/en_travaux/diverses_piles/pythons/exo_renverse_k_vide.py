# --------- PYODIDE:code --------- #
def renverse_k(p, k):
    ...


# --------- PYODIDE:corr --------- #
def renverse_k(p, k):
    annexe_1 = Pile()
    annexe_2 = Pile()
    for _ in range(k):
        annexe_1.empile(p.depile())
    for _ in range(k):
        annexe_2.empile(annexe_1.depile())
    for _ in range(k):
        p.empile(annexe_2.depile())


# --------- PYODIDE:tests --------- #
p = Pile()
p.empile("a")
p.empile("b")
p.empile("c")
p.empile("d")
p.empile("e")
renverse_k(p, 4)
q = Pile()
q.empile("a")
q.empile("e")
q.empile("d")
q.empile("c")
q.empile("b")
assert p == q


# --------- PYODIDE:secrets --------- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
attendu = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5, 6):
    p.empile(x)
    attendu.empile(x)
    r.empile(x)
k = 0
renverse_k(p, k)
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
k = 7
renverse_k(p, k)
attendu._valeurs_ = attendu._valeurs_[::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
p = Pile()
attendu = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5, 6):
    p.empile(x)
    attendu.empile(x)
    r.empile(x)
k = 5
renverse_k(p, k)
attendu._valeurs_[-5:] = attendu._valeurs_[-5:][::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
