# --------- PYODIDE:env --------- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self.valeurs = []

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self.valeurs) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self.valeurs.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self.valeurs])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self.valeurs == autre.valeurs


# --------- PYODIDE:code --------- #
def taille(p):
    ...


# --------- PYODIDE:corr --------- #
def taille(p):
    annexe = Pile()
    t = 0
    while not p.est_vide():
        annexe.empile(p.depile())
        t = t + 1
    while not annexe.est_vide():
        p.empile(annexe.depile())
    return t


# --------- PYODIDE:tests --------- #
p = Pile()
assert taille(p) == 0
p.empile("a")
p.empile("b")
assert taille(p) == 2
q = Pile()
q.empile("a")
q.empile("b")
assert p == q


# --------- PYODIDE:secrets --------- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5):
    p.empile(x)
    q.empile(x)
    r.empile(x)
assert taille(p) == 6
assert p == q, f"Erreur avec la pile {r}"

