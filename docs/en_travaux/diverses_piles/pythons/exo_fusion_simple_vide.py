# --------- PYODIDE:env --------- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self.valeurs = []

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self.valeurs) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self.valeurs.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self.valeurs])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self.valeurs == autre.valeurs


# --------- PYODIDE:code --------- #
def fusion_simple(p, q):
    ...


# --------- PYODIDE:corr --------- #
def fusion_simple(p, q):
    r = Pile()
    while not p.est_vide():
        r.empile(p.depile())
        r.empile(q.depile())
    return r


# --------- PYODIDE:tests --------- #
p = Pile()
q = Pile()
p.empile("c")
p.empile("a")
q.empile("d")
q.empile("b")
r = fusion_simple(p, q)
a = Pile()
a.empile("a")
a.empile("b")
a.empile("c")
a.empile("d")
assert r == a


# --------- PYODIDE:secrets --------- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
attendu = Pile()
assert fusion_simple(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = Pile()
qq = Pile()
for x in (0, 1, 2):
    p.empile(x)
    pp.empile(x)
for x in (4, 5, 6):
    q.empile(x)
    qq.empile(x)
attendu = Pile()
attendu._valeurs_ = [2, 6, 1, 5, 0, 4]
assert fusion_simple(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
