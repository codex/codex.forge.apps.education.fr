# --- hdr, , renverse_deux_, taille_, fusion_simple_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self.valeurs = []

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self.valeurs) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self.valeurs.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self.valeurs])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self.valeurs == autre.valeurs


# --- exo, taille_ --- #
""" # skip
def taille(p):
    annexe = ...
    t = ...
    while not ...:
        annexe.empile(...)
        t = ...
    while not ...:
        ...
    return ...
"""  # skip


# --- vide, taille_ --- #
def taille(p):
    ...


# --- corr, taille_ --- #
def taille(p):
    annexe = Pile()
    t = 0
    while not p.est_vide():
        annexe.empile(p.depile())
        t = t + 1
    while not annexe.est_vide():
        p.empile(annexe.depile())
    return t


# --- tests, taille_ --- #
p = Pile()
assert taille(p) == 0
p.empile("a")
p.empile("b")
assert taille(p) == 2
q = Pile()
q.empile("a")
q.empile("b")
assert p == q


# --- secrets, taille_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5):
    p.empile(x)
    q.empile(x)
    r.empile(x)
assert taille(p) == 6
assert p == q, f"Erreur avec la pile {r}"

# --- exo, renverse_deux_ --- #
""" # skip
def renverse_deux(p):
    a = p....()
    b = p....()
    p....(...)
    p....(...)
"""  # skip


# --- vide, renverse_deux_ --- #
def renverse_deux(p):
    ...


# --- corr, renverse_deux_ --- #
def renverse_deux(p):
    a = p.depile()
    b = p.depile()
    p.empile(a)
    p.empile(b)


# --- tests, renverse_deux_ --- #
p = Pile()
p.empile("a")
p.empile("b")
renverse_deux(p)
q = Pile()
q.empile("b")
q.empile("a")
assert p == q


# --- secrets, renverse_deux_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
r = Pile()
for x in (0, 1):
    p.empile(x)
    q.empile(x)
    r.empile(x)
renverse_deux(p)
q._valeurs_[-1], q._valeurs_[-2] = q._valeurs_[-2], q._valeurs_[-1]
assert p == q, f"Erreur avec la pile {r}"
p = Pile()
q = Pile()
for x in (0, 1, 2, 3, 4, 5):
    p.empile(x)
    q.empile(x)
    r.empile(x)
renverse_deux(p)
q._valeurs_[-1], q._valeurs_[-2] = q._valeurs_[-2], q._valeurs_[-1]
assert p == q, f"Erreur avec la pile {r}"
# --- exo, fusion_simple_ --- #
""" # skip
def fusion_simple(p, q):
    r = ...
    while ...:
        r....(...)
        ...
    return r
"""  # skip


# --- vide, fusion_simple_ --- #
def fusion_simple(p, q):
    ...


# --- corr, fusion_simple_ --- #
def fusion_simple(p, q):
    r = Pile()
    while not p.est_vide():
        r.empile(p.depile())
        r.empile(q.depile())
    return r


# --- tests, fusion_simple_ --- #
p = Pile()
q = Pile()
p.empile("c")
p.empile("a")
q.empile("d")
q.empile("b")
r = fusion_simple(p, q)
a = Pile()
a.empile("a")
a.empile("b")
a.empile("c")
a.empile("d")
assert r == a


# --- secrets, fusion_simple_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
attendu = Pile()
assert fusion_simple(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = Pile()
qq = Pile()
for x in (0, 1, 2):
    p.empile(x)
    pp.empile(x)
for x in (4, 5, 6):
    q.empile(x)
    qq.empile(x)
attendu = Pile()
attendu._valeurs_ = [2, 6, 1, 5, 0, 4]
assert fusion_simple(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
# --- exo, fusion_ --- #
""" # skip
def fusion(p, q):
    r = ...
    while ...:
        r....(...)
        ...
    while ...:
        r....(...)
    while ...:
        ...
    return r
"""  # skip


# --- vide, fusion_ --- #
def fusion(p, q):
    ...


# --- corr, fusion_ --- #
def fusion(p, q):
    r = Pile()
    while not p.est_vide() and not q.est_vide():
        r.empile(p.depile())
        r.empile(q.depile())
    while not p.est_vide():
        r.empile(p.depile())
    while not q.est_vide():
        r.empile(q.depile())
    return r


# --- tests, fusion_ --- #
p = Pile()
q = Pile()
p.empile("e")
p.empile("c")
p.empile("a")
q.empile("d")
q.empile("b")
r = fusion(p, q)
a = Pile()
a.empile("a")
a.empile("b")
a.empile("c")
a.empile("d")
a.empile("e")
assert r == a


# --- secrets, fusion_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
q = Pile()
attendu = Pile()
assert fusion(p, q) == attendu, "Erreur lors de la fusion de deux piles vides"
pp = Pile()
qq = Pile()
for x in (0, 1, 2, 3):
    p.empile(x)
    pp.empile(x)
for x in (4, 5, 6):
    q.empile(x)
    qq.empile(x)
attendu = Pile()
attendu._valeurs_ = [3, 6, 2, 5, 1, 4, 0]
assert fusion(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
pp = Pile()
qq = Pile()
for x in (0, 1):
    p.empile(x)
    pp.empile(x)
for x in (4, 5, 6):
    q.empile(x)
    qq.empile(x)
attendu = Pile()
attendu._valeurs_ = [1, 6, 0, 5, 4]
assert fusion(p, q) == attendu, f"Erreur lors de la fusion de {pp} et {qq}"
# --- exo, renverse_k_ --- #
""" # skip
def renverse_k(p, k):
    annexe_1 = ...
    annexe_2 = ...
    for _ in range(k):
        annexe_1....(p....)
    for _ in range(k):
        annexe_2....(....)
    for _ in range(k):
        ...
"""  # skip


# --- vide, renverse_k_ --- #
def renverse_k(p, k):
    ...


# --- corr, renverse_k_ --- #
def renverse_k(p, k):
    annexe_1 = Pile()
    annexe_2 = Pile()
    for _ in range(k):
        annexe_1.empile(p.depile())
    for _ in range(k):
        annexe_2.empile(annexe_1.depile())
    for _ in range(k):
        p.empile(annexe_2.depile())


# --- tests, renverse_k_ --- #
p = Pile()
p.empile("a")
p.empile("b")
p.empile("c")
p.empile("d")
p.empile("e")
renverse_k(p, 4)
q = Pile()
q.empile("a")
q.empile("e")
q.empile("d")
q.empile("c")
q.empile("b")
assert p == q


# --- secrets, renverse_k_ --- #
class Pile:
    def __init__(self):
        """Initialise une pile vide"""
        self._valeurs_ = []

    @property
    def valeurs():
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @property
    def valeurs(self):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    @valeurs.setter
    def valeurs(self, nouveau):
        raise AttributeError("Il est interdit d'accéder à l'attribut valeurs")

    def est_vide(self):
        """Renvoie un booléen : la pile est-elle vide ?"""
        return len(self._valeurs_) == 0

    def empile(self, element):
        """Empile un élément au sommet de la pile"""
        self._valeurs_.append(element)

    def depile(self):
        """
        Dépile un élément au sommet et le renvoie.
        Provoque une erreur si la pile est vide.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self._valeurs_.pop()

    def __repr__(self):
        """Affiche la pile, en indiquant le sommet"""
        return f"| {' | '.join([str(x) for x in self._valeurs_])} | <- sommet"

    def __eq__(self, autre):
        """
        Détermine si cette pile et l'autre sont égales
        (mêmes valeurs, dans le même ordre)
        """
        return self._valeurs_ == autre._valeurs_


p = Pile()
attendu = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5, 6):
    p.empile(x)
    attendu.empile(x)
    r.empile(x)
k = 0
renverse_k(p, k)
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
k = 7
renverse_k(p, k)
attendu._valeurs_ = attendu._valeurs_[::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
p = Pile()
attendu = Pile()
r = Pile()
for x in (0, 1, 2, 3, 4, 5, 6):
    p.empile(x)
    attendu.empile(x)
    r.empile(x)
k = 5
renverse_k(p, k)
attendu._valeurs_[-5:] = attendu._valeurs_[-5:][::-1]
assert p == attendu, f"Erreur en renversant {k} élément(s) dans {r}"
