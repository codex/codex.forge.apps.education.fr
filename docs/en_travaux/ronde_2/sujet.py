# --- hdr --- #
class Joueur:
    def __init__(self, nom, successeur=None):
        self.nom = nom
        self.successeur = successeur


def nouvelle_ronde(noms):
    assert len(noms) > 0, f"La liste noms doit contenir au moins un élément"
    premier = Joueur(noms[0])
    actuel = premier
    for i in range(1, len(noms)):
        prochain = Joueur(noms[i])
        actuel.successeur = prochain
        actuel = prochain
    prochain.successeur = premier
    return premier


def affiche_ronde(joueur):
    noms = [joueur.nom]
    actuel = joueur.successeur
    while actuel.nom != joueur.nom:
        noms.append(actuel.nom)
        actuel = actuel.successeur
    total = "╭→ " + " → ".join(noms)
    print(total + " ╮\n╰" + "─" * (len(total)) + "╯")


# --- exo --- #
""" # skip
def gagnant(noms):
    joueur = nouvelle_ronde(noms)
    while ...:
        joueur.successeur = ...
        ...
    return ...
"""  # skip


# --- vide --- #
def gagnant(noms):
    joueur = nouvelle_ronde(noms)
    ...


# --- corr --- #
def gagnant(noms):
    joueur = nouvelle_ronde(noms)
    while joueur.successeur != joueur:
        joueur.successeur = joueur.successeur.successeur
        joueur = joueur.successeur

    return joueur.nom


# --- tests --- #
noms = ["Riri", "Fifi", "Loulou"]
assert gagnant(noms) == "Loulou"
noms = ["Paul", "John", "Ringo", "Georges"]
assert gagnant(noms) == "Paul"
noms = ["Brian", "Ian", "Mick", "Keith", "Dick"]
assert gagnant(noms) == "Mick"
# --- secrets --- #
noms = ["John", "Ringo", "Georges", "Paul"]
assert gagnant(noms) == "John", f"Erreur avec {noms}"
# Remarque : Si n = 2^m + l et 0 <= l < 2^m, alors gagnant(n) = 2l+1
gagnants = {
    2: 1,
    3: 3,
    4: 1,
    5: 3,
    6: 5,
    7: 7,
    8: 1,
    9: 3,
    10: 5,
    11: 7,
    12: 9,
    13: 11,
    14: 13,
    15: 15,
    16: 1,
    17: 3,
    18: 5,
    19: 7,
    20: 9,
    21: 11,
    22: 13,
    23: 15,
    24: 17,
    25: 19,
    26: 21,
    27: 23,
    28: 25,
    29: 27,
    30: 29,
    31: 31,
    32: 1,
    33: 3,
    34: 5,
    35: 7,
    36: 9,
    37: 11,
    38: 13,
    39: 15,
    40: 17,
    41: 19,
    42: 21,
    43: 23,
    44: 25,
    45: 27,
    46: 29,
    47: 31,
    48: 33,
    49: 35,
    50: 37,
    51: 39,
    52: 41,
    53: 43,
    54: 45,
    55: 47,
    56: 49,
    57: 51,
    58: 53,
    59: 55,
    60: 57,
    61: 59,
    62: 61,
    63: 63,
    64: 1,
    65: 3,
    66: 5,
    67: 7,
    68: 9,
    69: 11,
    70: 13,
    71: 15,
    72: 17,
    73: 19,
    74: 21,
    75: 23,
    76: 25,
    77: 27,
    78: 29,
    79: 31,
    80: 33,
    81: 35,
    82: 37,
    83: 39,
    84: 41,
    85: 43,
    86: 45,
    87: 47,
    88: 49,
    89: 51,
    90: 53,
    91: 55,
    92: 57,
    93: 59,
    94: 61,
    95: 63,
    96: 65,
    97: 67,
    98: 69,
    99: 71,
}
for nb_joueurs, vainqueur in gagnants.items():
    vainqueur -= 1
    # assert (
    #     gagnant([k for k in range(nb_joueurs)]) == vainqueur
    # ), f"Erreur pour {nb_joueurs} joueurs"
# --- REM --- #
