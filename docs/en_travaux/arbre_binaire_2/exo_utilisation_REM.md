Il est aussi possible de déclarer les arbre en une seule fois. Les deux méthodes ont leurs avantages et inconvénients :

* La déclaration en une fois permet de voir la structure de l'arbre facilement, si elle est écrite en jouant avec l'indentation.
* ...mais il est très facile de déclarer le mauvais arbre, car il est rapidement délicat de rester cohérent dans le placement des virgules et parenthèses dans la déclaration.

* La déclaration étape par étape facilite grandement l'écriture correcte de chaque objet.
* ...mais il est très difficile de se rendre compte de la structure de l'arbre final simplement en lisant la déclaration.

Ici, les déclarations "en une fois" pourraient être écrites comme suit :

```python
ab_int = AB(
            AB(
                AB(None, 3, None),
                1,
                None,
            ),
            0,
            AB(
               AB(None, 4, None),
               2,
               AB(None, 5, None),
            )
        )

ab_str = AB(
            AB(
                None,
                'b',
                AB(
                    AB(None, 'h', None),
                    "e",
                    None,
                ),
            ),
            "a",
            AB(
                AB(
                    None,
                    'f',
                    AB(None, 'k', None),
                ),
                'c',
                None,
            )
         )
```