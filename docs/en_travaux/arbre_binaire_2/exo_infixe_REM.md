Il est à noter que la complexité en temps de cette approche n'est pas optimale, puisqu'on utilise des concaténations de listes.

Une possibilité pour éviter cela est d'utiliser une fonction annexe depuis laquelle on muterait toujours la même liste. Dans ce cas, la complexité en temps finale est $O(N)$ (pour N valeurs dans l'arbre).

```python title=""
    def infixe(self):
        return self._infixe_aux([])

    def _infixe_aux(self, lst):
        if not self.est_vide():
            self.racine.gauche._infixe_aux(lst)
            lst.append(self.racine.valeur)
            self.racine.droit._infixe_aux(lst)
        return lst
```

Noter qu'il suffit de déplacer la ligne `lst.append(ab.racine.valeur)`, par rapport à l'implémentation de la méthode `prefixe`.