Il est à noter que la complexité en temps de cette approche n'est pas optimale, puisqu'on utilise des concaténations de listes.

Une possibilité pour éviter cela est d'utiliser une fonction annexe depuis laquelle on muterait toujours la même liste. Dans ce cas, la complexité en temps finale est $O(N)$ (pour N valeurs dans l'arbre).


```python title=""
    def suffixe(self):
        return self._suffixe_aux([])

    def _suffixe_aux(self, lst):
        # La méthode _suffixe_aux doit aussi être implémentée dans la classe ABVide.
        if not self.est_vide():
            self.racine.gauche._suffixe_aux(lst)
            self.racine.droit._suffixe_aux(lst)
            lst.append(self.racine.valeur)
        return lst
```

Noter qu'il suffit de déplacer la ligne `lst.append(ab.racine.valeur)`, par rapport à l'implémentation des méthodes `prefixe` ou `infixe`.