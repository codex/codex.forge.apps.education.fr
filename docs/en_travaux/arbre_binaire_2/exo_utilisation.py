# --------- PYODIDE:env --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:code --------- #
ab_3 = AB(None, 3, None)
ab_1 = AB(ab_3, 1, None)
...
ab_int = ...

h = AB(None, "h", None)
...
ab_str = ...
# --------- PYODIDE:corr --------- #
ab_3 = AB(None, 3, None)
ab_4 = AB(None, 4, None)
ab_5 = AB(None, 5, None)
ab_1 = AB(ab_3, 1, None)
ab_2 = AB(ab_4, 2, ab_5)
ab_int = AB(ab_1, 0, ab_2)

h = AB(None, "h", None)
k = AB(None, "k", None)
e = AB(h, "e", None)
f = AB(None, "f", k)
b = AB(None, "b", e)
c = AB(f, "c", None)
ab_str = AB(b, "a", c)
# --------- PYODIDE:secrets --------- #
attendu = "(((∅, 3, ∅), 1, ∅), 0, ((∅, 4, ∅), 2, (∅, 5, ∅)))"
assert str(ab_int) == attendu
attendu = "((∅, b, ((∅, h, ∅), e, ∅)), a, ((∅, f, (∅, k, ∅)), c, ∅))"
assert str(ab_str) == attendu, "Erreur sur abr_str"
