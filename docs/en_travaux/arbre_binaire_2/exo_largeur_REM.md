Il est également possible de "traiter" chaque valeur juste après le défilement, aboutissant à cette solution pour la méthode `largeur` de la classe `AB` :

```python title=""
    def largeur(self):
        resultat = []
        file = File()
        file.enfile(self)
        while not file.est_vide():
            a = file.defile()
            resultat.append(a.racine.valeur)
            for enfant in (a.racine.gauche, a.racine.droit):
                if not enfant.est_vide():
                    file.enfile(enfant)
        return resultat
```

Ou encore, sans la boucle `#!py for` :

```python title=""
    def largeur(self):
        resultat = []
        file = File()
        file.enfile(self)
        while not file.est_vide():
            a = file.defile()
            resultat.append(a.racine.valeur)
            g = a.racine.gauche
            if not g.est_vide():
                file.enfile(g)
            d = a.racine.droit
            if not d.est_vide():
                file.enfile(d)
        return resultat
```