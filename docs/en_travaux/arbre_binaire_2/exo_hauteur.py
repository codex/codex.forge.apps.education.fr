# --------- PYODIDE:env --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:code --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def hauteur(self):
        ...

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:corr --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def hauteur(self):
        if self.est_vide():
            return 0
        return 1 + max(self.racine.gauche.hauteur(), self.racine.droit.hauteur())

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:tests --------- #
vide = AB(None, None, None)
ab_1 = AB(None, 1, None)
ab = AB(None, 0, AB(ab_1, 2, None))
assert vide.hauteur() == 0
assert ab.hauteur() == 3
# --------- PYODIDE:secrets --------- #
a = AB(None, 1, None)
b = AB(a, 2, None)
c = AB(None, 3, b)
d = AB(None, 4, None)
e = AB(c, 5, d)
f = AB(None, 6, e)
attendu = 1
assert a.hauteur() == attendu, f"Erreur avec {a = }"
attendu = 2
assert b.hauteur() == attendu, f"Erreur avec {b = }"
attendu = 3
assert c.hauteur() == attendu, f"Erreur avec {c = }"
attendu = 1
assert d.hauteur() == attendu, f"Erreur avec {1 = }"
attendu = 4
assert e.hauteur() == attendu, f"Erreur avec {e = }"
attendu = 5
assert f.hauteur() == attendu, f"Erreur avec {f = }"
