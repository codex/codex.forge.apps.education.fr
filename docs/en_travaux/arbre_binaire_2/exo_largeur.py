# --------- PYODIDE:env --------- #
from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.appendleft(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.pop()

    def __str__(self):
        """Convertit la file en une chaîne de caractères"""
        if self.est_vide():
            return "∅"
        return f"(queue) {' -> '.join(str(x) for x in self.valeurs)} (tête)"


class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:code --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def largeur(self):
        ...

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:corr --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def largeur(self):
        if self.est_vide():
            return []
        resultat = [self.racine.valeur]
        file = File()
        file.enfile(self.racine.gauche)
        file.enfile(self.racine.droit)
        while not file.est_vide():
            a = file.defile()
            if not a.est_vide():
                resultat.append(a.racine.valeur)
                file.enfile(a.racine.gauche)
                file.enfile(a.racine.droit)
        return resultat

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:tests --------- #
vide = AB(None, None, None)
ab_1 = AB(None, 1, None)
ab = AB(None, 0, AB(ab_1, 2, None))
assert vide.largeur() == []
assert ab.largeur() == [0, 2, 1]
# --------- PYODIDE:secrets --------- #
a = AB(None, 1, None)
b = AB(a, 2, None)
c = AB(None, 3, b)
d = AB(None, 4, None)
e = AB(c, 5, d)
f = AB(None, 6, e)
attendu = [1]
assert a.largeur() == attendu, f"Erreur avec {a = }"
attendu = [2, 1]
assert b.largeur() == attendu, f"Erreur avec {b = }"
attendu = [3, 2, 1]
assert c.largeur() == attendu, f"Erreur avec {c = }"
attendu = [4]
assert d.largeur() == attendu, f"Erreur avec {d = }"
attendu = [5, 3, 4, 2, 1]
assert e.largeur() == attendu, f"Erreur avec {e = }"
attendu = [6, 5, 3, 4, 2, 1]
assert f.largeur() == attendu, f"Erreur avec {f = }"
