# --------- PYODIDE:env --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:code --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def suffixe(self):
        ...

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:corr --------- #
class Noeud:
    def __init__(self, gauche, valeur, droit):
        self.gauche = gauche
        self.valeur = valeur
        self.droit = droit


class AB:
    def __init__(self, gauche, racine, droit):
        if racine is not None:
            if gauche is None:
                gauche = AB(None, None, None)
            if droit is None:
                droit = AB(None, None, None)
            racine = Noeud(gauche, racine, droit)
        self.racine = racine

    def est_vide(self):
        return self.racine is None

    def suffixe(self):
        if self.est_vide():
            return []
        return self.racine.gauche.suffixe() + self.racine.droit.suffixe() + [self.racine.valeur]

    def __str__(self):
        if self.est_vide():
            return "∅"
        else:
            return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"


# --------- PYODIDE:tests --------- #
vide = AB(None, None, None)
ab_1 = AB(None, 1, None)
ab = AB(None, 0, AB(ab_1, 2, None))
assert vide.suffixe() == []
assert ab.suffixe() == [1, 2, 0]
# --------- PYODIDE:secrets --------- #
a = AB(None, 1, None)
b = AB(a, 2, None)
c = AB(None, 3, b)
d = AB(None, 4, None)
e = AB(c, 5, d)
f = AB(None, 6, e)
attendu = [1]
assert a.suffixe() == attendu, f"Erreur avec {a = }"
attendu = [1, 2]
assert b.suffixe() == attendu, f"Erreur avec {b = }"
attendu = [1, 2, 3]
assert c.suffixe() == attendu, f"Erreur avec {c = }"
attendu = [4]
assert d.suffixe() == attendu, f"Erreur avec {d = }"
attendu = [1, 2, 3, 4, 5]
assert e.suffixe() == attendu, f"Erreur avec {e = }"
attendu = [1, 2, 3, 4, 5, 6]
assert f.suffixe() == attendu, f"Erreur avec {f = }"
