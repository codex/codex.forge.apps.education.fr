---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Autour des arbres binaires (2)
tags:
    - en travaux
    - arbre binaire
    - programmation orientée objet
    - récursivité
difficulty: 205
maj: 27/02/2025
---

??? info "Différentes représentations"

    Cet exercice demande d'écrire les fonctions calculant la taille et hauteur d'un arbre binaire.
    Il fait partie d'une série dans laquelle on utilise différentes représentations :

    * {{ lien_exo("Représentation avec une classe décrivant l'arbre vide", "arbre_binaire_1") }} ;
    * {{ lien_exo("Représentation avec une classe décrivant les nœuds", "arbre_binaire_2") }} (cet exercice).

On rappelle qu'un arbre binaire est :

* soit *vide* ;
* soit composé d'un nœud appelé racine et portant une valeur, et de deux sous-arbres appelés *sous-arbre gauche* et *sous-arbre droit*.

On représente dans cet exercice les arbres binaires à l'aide d'une classe `AB` ne possédant qu'un seul attribut `#!py racine`.

Si `#!py None` est affecté à cet attribut, alors l'arbre binaire est vide.

Dans le cas où l'arbre binaire n'est pas vide, son attribut `#!py racine` est un objet de type `#!py Noeud`qui a trois attributs :

* `#!py valeur` qui est la valeur portée par la racine ;
* `#!py gauche` est un objet de type `#!py AB` et représente le sous-arbre gauche de l'arbre ;
* `#!py droit` est un objet de type `#!py AB` et représente le sous-arbre droit de l'arbre.


??? info "Les classes `Noeud` et `AB`"

    ```python title=""
    class Noeud:
        def __init__(self, gauche, valeur, droit):
            self.gauche = gauche
            self.valeur = valeur
            self.droit = droit


    class AB:
        def __init__(self, gauche, racine, droit):
            if racine is not None:
                if gauche is None:
                    gauche = AB(None, None, None)
                if droit is None:
                    droit = AB(None, None, None)
                racine = Noeud(gauche, racine, droit)
            self.racine = racine

        def est_vide(self):
            return self.racine is None

        def __str__(self):
            if self.est_vide():
                return "∅"
            else:
                return f"({self.racine.gauche}, {self.racine.valeur}, {self.racine.droit})"
    ```

??? question "Créer un arbre binaire"

    Les deux classes `#!py Noeud` et `#!py AB` décrites ci-dessus est déjà importées dans l'éditeur.

    Utiliser ces classes afin de représenter les deux arbres binaires ci-dessous :

    <center>

    ```mermaid
    flowchart TD
        A0((0))
        B0((1))
        C0((2))
        D0((3))
        E0[ ]
        F0((4))
        G0((5))

        A0 --> B0
        A0 --> C0
        B0 --> D0
        B0 ~~~ E0
        C0 --> F0
        C0 --> G0

        a((a))
        b((b))
        c((c))
        d(( ))
        e((e))
        f((f))
        g(( ))
        h((h))
        i(( ))
        j(( ))
        k((k))

        a --> b
        a --> c
        b ~~~ d
        b --> e
        c --> f
        c ~~~ g
        e --> h
        e ~~~ i
        f ~~~ j
        f --> k

        style E0 fill:none, stroke:none
        style d fill:none, stroke:none
        style g fill:none, stroke:none
        style i fill:none, stroke:none
        style j fill:none, stroke:none
    ```

    </center>

    Par commodité, on n'a pas représenté les arbres vides.

    `#!py ab_int` sera l'arbre portant des nombres entiers, `ab_str` celui portant des caractères.

    {{ IDE('exo_utilisation') }}

??? question "Taille"

    Compléter la méthode `#!py taille` de la classe `#!py AB`. Cette méthode renvoie la taille de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.taille()
        0
        >>> ab.taille()
        3
        ```

    {{ IDE('exo_taille') }}

??? question "Hauteur"

    Compléter la méthode `#!py hauteur` de la classe `#!py AB`. Cette méthode renvoie la hauteur de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.hauteur()
        0
        >>> ab.hauteur()
        3
        ```

    {{ IDE('exo_hauteur') }}


??? question "Parcours en largeur"

    Compléter la méthode `#!py largeur` de la classe `#!py AB`. Cette méthode renvoie la liste des valeurs des nœuds rencontrés lors du parcours en largeur de l'arbre binaire représenté par l'objet en question.

    La classe `#!py File` décrite ci-dessous est déjà importée dans l'éditeur.

    {{ remarque('classe_File') }}

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.largeur()
        []
        >>> ab.largeur()
        [0, 2, 1]
        ```

    {{ IDE('exo_largeur') }}

??? question "Parcours préfixe"

    Compléter la méthode `#!py prefixe` de la classe `#!py AB`. Cette méthode renvoie la liste des valeurs des nœuds rencontrés lors du parcours en profondeur préfixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.prefixe()
        []
        >>> ab.prefixe()
        3
        ```

    {{ IDE('exo_prefixe') }}

??? question "Parcours infixe"

    Compléter la méthode `#!py infixe` de la classe `#!py AB`. Cette méthode renvoie la liste des valeurs des nœuds rencontrés lors du parcours en profondeur infixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.infixe()
        []
        >>> ab.infixe()
        3
        ```

    {{ IDE('exo_infixe') }}

??? question "Parcours suffixe"

    Compléter la méthode `#!py suffixe` de la classe `#!py AB`. Cette méthode renvoie la liste des valeurs des nœuds rencontrés lors du parcours en profondeur suffixe de l'arbre binaire représenté par l'objet en question.

    ???+ example "Exemples"

        ```pycon title=""
        >>> vide = AB(None, None, None)
        >>> ab_1 = AB(None, 1, None)
        >>> ab = AB(None, 0, AB(ab_1, 2, None))
        >>> vide.suffixe()
        []
        >>> ab.suffixe()
        3
        ```

    {{ IDE('exo_suffixe') }}


<br>

---

<br>

??? note "Compléments"

    Il est à noter qu'ici, il n'est pas possible de stocker `#!py None` en tant que valeur dans un arbre binaire implémenté de cette façon. En effet, si l'argument `valeur` est `#!py None`, l'instance interne de `Noeud` n'est jamais créée.
    Ce n'est généralement pas gênant, car il rare d'avoir besoin de stocker `#!py None` en plus d'autres types de données dans le même arbre binaire, mais il peut être intéressant de garder ce détail à l'esprit.

    <br>

    Il est également possible de déclarer la classe `Noeud` à l'intérieur de la classe `AB`, de manière à ne pas l'exposer à l'utilisateur. Elle est alors un attribut de classe, qui peut être accédé depuis une instance avec `self.Noeud` (Nota: le comportement des attributs de classes vs instances a un fonctionnement particulier à python).
    <br>Dans ce cas, l'implantation de la méthode `__init__` devient :

    ```python title=""
    class AB:

        class Noeud:
            def __init__(self, gauche, valeur, droit):
                self.valeur = valeur
                self.gauche = None
                self.droit = None

        def __init__(self, gauche, racine, droit):
            if racine is not None:
                if gauche is None:
                    gauche = AB(None, None, None)
                if droit is None:
                    droit = AB(None, None, None)
                racine = self.Noeud(gauche, racine, droit)
            self.racine = racine
    ```