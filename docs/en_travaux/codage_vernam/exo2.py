# --------- PYODIDE:env --------- #
from random import randint


#Génère une suite de bits à partir d'une chaine de caractères
def message_en_binaire(message):
    message_binaire = []
    for caractere in message :
        message_binaire += conv_bin(ord(caractere))
    return message_binaire

def conv_bin(n) :
    binaire = []
    while n>0 :
        binaire.append(n%2)
        n = n//2
    
    #on complète jusqu'à 8 bits
    while len(binaire) < 8 :
        binaire.append(0)
    binaire.reverse()
    return binaire

def generation_cle(message) :
    longueur = len(message)
    cle=[randint(0, 1)  for i in range(longueur)]
    return cle


# --------- PYODIDE:code --------- #

def chiffrement_Vernam(message):
    ...



# --------- PYODIDE:corr --------- #


def chiffrement_Vernam(message):
    
    message_binaire = message_en_binaire(message)
    cle = generation_cle(message_binaire)
    message_chiffre = []
    
    for i in range(len(message_binaire)):
        message_chiffre.append(message_binaire[i]^cle[i])
    return cle, message_chiffre

# --------- PYODIDE:tests --------- #
cle, chiffre = chiffrement_Vernam("Mon message secret")
assert len(cle)==len(chiffre)

# --------- PYODIDE:secrets --------- #

#Convertie un nombre binaire sous 8 bits en entier
def conv_dec(binaire):
    nombre = 0
    for i in range(8):
        exposant = 7 - i
        nombre += 2**exposant*binaire[i]
    return nombre

def decrypt (message, cle):
    decrypt = []
    clair = ""
    for i in range(len(message)):
        decrypt.append(message[i]^cle[i])
    while (len(decrypt) !=0) :
        mot =[]
        for i in range(8):
            mot.append(decrypt.pop(0))
        clair += chr(conv_dec(mot))
    return clair


cle, message_crypte = chiffrement_Vernam("NSI")
assert decrypt(message_crypte, cle) == "NSI"