---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Le chiffre de Vernam
tags:
    - en travaux
---

# Le chiffre de Vernam

!!! abstract "Le masque jetable"
    Le **masque jetable**, également appelé **chiffre de Vernam**, est un algorithme de cryptographie inventé par Gilbert Vernam en $1917$ et perfectionné par Joseph Mauborgne, qui rajouta la notion de clé aléatoire.

    Le chiffrement par la méthode du masque jetable consiste à combiner le message en clair avec une clé présentant les caractéristiques très particulières suivantes :

    * La clé doit être une suite de caractères au moins aussi longue que le message à chiffrer.
    * Les caractères composant la clé doivent être choisis de façon totalement aléatoire.
    * Chaque clé, ou « masque », ne doit être utilisée qu'une seule fois (d'où le nom de masque jetable).

    Ce chiffre peut aisément se faire à la fin, mais nous verrons ici la version informatisée qui repose sur l'opérateur OU-exclusif.



!!! note "Principe"
    Pour crypter un message avec le chiffre de Vernam, on procède ainsi :

    * on génère une clé aléatoire de même taille que le message
    * on transforme le message en une suite de bits
    * on réalise un OU-exclusif bit à bit entre le message et la clé

    Pour déchiffrer le message on refait la même opération avec cette même clé.



??? question "Question 1"

    Compléter la fonction `generation_cle` qui crée une clé aléatoire (sous forme d'une liste d'entiers) de la même taille que le message passé en paramètre.

    ```pycon
    >>> generation_cle([1,0,1,0,1,0,1,0])
    [1,1,1,1,0,0,1,0]
    >>> generation_cle([1,0,1,0,1,0,1,0])
    [0,1,1,0,0,1,1,0]
    ```
    {{ remarque('randint')  }}

    {{ IDE('exo') }}

??? question "Question 2"
    On souhaite convertir un message clair en un message chiffré.
    Pour cela :

    * on convertie le message (stocké dans une chaine de caractères) en une suite de nombre binaires (stocké dans une liste d'entiers)
    * on génère une clé aléatoire
    * on réalise le cryptage par une opération bit à bit

    ??? note "Fonctions fournies"
        Deux fonctions sont fournies pour vous aider et déjà chargées en mémoire :

        * La fonction `message_en_binaire` prend en paramètre une chaine de caractères et renvoie sa conversion en binaire sous la forme d'une liste d'entiers.

        ```pycon
        >>> generation_cle([1,0,1,0,1,0,1,0])
        [1,1,1,1,0,0,1,0]
        >>> generation_cle([1,0,1,0,1,0,1,0])
        [0,1,1,0,0,1,1,0]
        ```

        * La fonction `generation_cle` est aussi chargée en mémoire.
         
    ??? tips "OU-exclusif en Python"

        L’opérateur `#!py ^` en Python permet de réaliser l'opération OU-exclusif entre deux bits ou entre deux booléen. est pour le XOR bitwise et peut également être utilisé sur les booléens.
        Cet opération est décrite par la table de vérité suivante :

        A | B | A xor B
        --- | --- | --- 
        0 | 0 | 0 
        0 | 1 | 1 
        1 | 0 | 1 
        1 | 1 | 0 

        ```pycon
        >>> True ^ True
        False
        >>> True ^ False
        False
        ```
    Compléter la fonction `chiffrement_Vernam` qui prend en paramètre un message sous forme d'une chaîne de caractère et qui renvoie la clé ainsi que le message chiffré, sous la forme d'un couple de listes d'entiers.

    ```pycon
    >>> chiffrement_Vernam("M")
    ([0, 1, 1, 0, 0, 0, 1, 0], [0, 0, 1, 0, 1, 1, 1, 1])
    ```

    {{ IDE('exo2') }}

??? question "Question 3"
         
    On souhaite dechiffrer un message selon le chiffre de Vernam
    Pour cela :

    * on exécute l'opération de OU-EXCLUSIF entre la clé et le message
    * on établie des paquets de $8$ bits
    * on convertie chaque paquet en un caractère

    Compléter la fonction `dechiffrage_Vernam` qui prend en paramètre le message chiffré et la clé de chiffrement et qui renvoie le message déchiffré sous forme d'une chaîne de caractères.

    ```pycon
    >>> dechiffrage_Vernam([0, 0, 1, 0, 1, 1, 1, 1],[0, 1, 1, 0, 0, 0, 1, 0])
    'M'
    ```
    ??? note "Fonction fournie"
        Pour cette question, on fournit la fonction `conv_dec` qui prend en paramètre une liste de $8$ bits et renvoie sa conversion en binaire.

        ```pycon
        >>> conv_dec([1,0,1,0,1,0,1,0])
        170
        ```
    On rappelle qu'un caractère est encodé par un nombre entier que l'on obtient avec la fonction ord. Par exemple `#!py ord('A')` est évalué à $65$. Les codes des caractères alphabétiques non accentués en majuscule se suivent : `#!py ord('A')` vaut $65$, `#!py ord('B')`aut $66$, `#!py ord('C')` vaut $67$, ...

    Réciproquement, étant donné un entier positif, on obtient le caractère encodé par cet entier avec la fonction `chr`. Par exemple `#!py chr(65)` est évalué à 'A'.

    {{ IDE('exo3') }}