# --------- PYODIDE:env --------- #

def generation_cle(message) :
    pass

# --------- PYODIDE:code --------- #

from random import randint

def generation_cle(message) :
    longueur = ...
    # création de la clé par compréhension
    cle=...
    return cle


# --------- PYODIDE:corr --------- #

from random import randint

def generation_cle(message) :
    longueur = len(message)
    cle=[randint(0, 1)  for i in range(longueur)]
    return cle


# --------- PYODIDE:tests --------- #

assert len("0101010111") == len(generation_cle("0101010111"))
assert generation_cle("0101010111") != "0101010111"
assert len("0111") == len(generation_cle("0111"))
assert generation_cle("0111") != "0111"

# --------- PYODIDE:secrets --------- #


mess_crypt = generation_cle("0101010111")
for car in mess_crypt:
    assert car in (0, 1)