---
author: Denis Quenton
difficulty: 200
hide:
    - navigation
    - toc
title: Intersection d'intervalles
tags:
    - en travaux
    
---
# Intersection d'intervalles

On se propose d'étudier l'intersection de deux intervalles fermés de $\mathbb{R}$, $[a;\;b]$ et $[c;\;d]$.

On considère quatre nombres distincts $a,\;b,\;c,\;d$ et tels que $a<b$ et $c<d$.

On obtient alors les six situations possibles suivantes :

=== "Cas n°1 : $c<a$"
    ![image des six possibilités](./image_01.svg)
=== "Cas n°2 : $c<b$ (sachant que $c>a$)"
    ![image des six possibilités](./image_02.svg)
=== "Cas n°3 : reste $c>b$ (sachant que $c>a$)"
    ![image des six possibilités](./image_03.svg)

Définir la fonction `intersection_intervalle`, qui prend en paramètres quatre nombres `a`, `b`,`c` et `d`, et qui renvoie la borne inférieure et la borne supérieure de l'intersection des intervalles fermés $[a;\;b]$ et $[c;\;d]$.

La fonction renverra `(None, None)` pour une intersection vide.

???+ example "Exemple"

    $\left[5;\;7\right\rbrace \cap \left\lbrace1;\;3\right] = \emptyset$. L'intersection est vide.

    et

    $\left[5;\;7\right\rbrace \cap \left\lbrace1;\;9\right] = \left[5;\;7\right]$. 


    ```pycon title=""
    >>> intersection_intervalle(5,7,1,3) 
    (None, None)
    >>> intersection_intervalle(5,7,1,9)
    (5,7)
    ```

{{ IDE('exo', SANS='min,max')}}