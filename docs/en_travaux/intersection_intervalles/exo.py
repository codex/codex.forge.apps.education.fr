# --- PYODIDE:ignore --- #
"""
Les sections `ignore` sont... ignorées. Vous pouvez les utiliser pour laisser
des commentaires dans vos fichiers, ou y archiver du code python qui ne sera
pas utilisé pour le site construit.
---------------------------------------------------------------------------

La section `env` (ci-dessous) est exécutée avant le code utilisateur.
Son contenu n'est pas visible de l'utilisateur mais tout ce qui y est défini
est ensuite disponible dans l'environnement.
Si le code de la section ENV lève une erreur, rien d'autre ne sera exécuté.
"""
# --- PYODIDE:env --- #

"""rien d'utile cette fois ci"""


# --- PYODIDE:ignore --- #
"""
La section `code` est l'état initial du code fourni à l'utilisateur dans
l'éditeur, à l'exclusion des tests publics (voir section `tests`).
"""
# --- PYODIDE:code --- #

def intersection_intervalle(a,b,c,d):
    ...



# --- PYODIDE:ignore --- #
"""
La section `corr` contient le code qui sera affiché dans la correction, sous l'IDE.
"""
# --- PYODIDE:corr --- #

def intersection_intervalle(a,b,c,d):
    if c < a:
        if d < a:
            return None,None
        elif d < b:
            return a,d
        else:
            return a,b
    elif c < b:
        if d < b:
            return c,d
        else:
            return c,b
    else:
        return None,None



# --- PYODIDE:ignore --- #
"""
La section `tests` contient les tests publics qui seront affichés sous le code
utilisateur, dans l'éditeur.
"""
# --- PYODIDE:tests --- #

assert intersection_intervalle(5,7,1,3) == (None, None)
assert intersection_intervalle(5,7,1,9) == (5,7)



# --- PYODIDE:ignore --- #
"""
La section `secrets` contient les tests secrets pour les validations. Ces tests ne sont
pas visibles par l'utilisateur.

ATTENTION :
    Il est impératif d'utiliser des messages dans les assertions des tests secrets,
    sinon l'utilisateur ne peut pas déboguer son code car `print` est désactivé
    durant ces tests ! (sauf si... => Voir les options de configuration)
    À vous de choisir le niveau d'information que vous voulez fournir dans le message.

Par ailleurs, il est conseillé d'utiliser une fonction pour éviter que des variables
des tests ne se retrouvent dans l'environnement global.
"""
# --- PYODIDE:secrets --- #

def _intersection_intervalle(a,b,c,d):
    if c < a:
        if d < a:
            return None,None
        elif d < b:
            return a,d
        else:
            return a,b
    elif c < b:
        if d < b:
            return c,d
        else:
            return c,b
    else:
        return None,None
    
def tests():
#    for n in range(100):
#        val = est_pair(n)
#        exp = n%2 == 0
#
#        msg = f"est_pair({n})"                           # Minimum vital
#        msg = f"est_pair({n}): valeur renvoyée {val}"    # Conseillé
#        msg = f"est_pair({n}): {val} devrait être {exp}" # La totale
#
#        assert val == exp, msg
    assert intersection_intervalle(5,7,1,3) == (None, None), "[c,d] se trouve avant [a,b] ne fonctionne pas"
    assert intersection_intervalle(5,7,1,6) == (5, 6), "[c,d] qui possède une intersection non vide avec [a,b] ne fonctionne pas "
    assert intersection_intervalle(5,7,1,13) == (5, 7), "[c,d] se trouve avant [a,b] ne fonctionne pas "
    assert intersection_intervalle(5,7,6,6.5) == (6, 6.5), "[c,d] est inclus dans [a,b] ne fonctionne pas "
    assert intersection_intervalle(5,7,6,13) == (6, 7), "[c,d] qui possède une intersection non vide avec [a,b] ne fonctionne pas "
    assert intersection_intervalle(5,7,11,13) == (None, None), "[c,d] se trouve après [a,b] ne fonctionne pas "

tests()         # Ne pas oublier d'appeler la fonction de tests... ! x)
del tests       # Si vous ne voulez pas laisser de traces...
del _intersection_intervalle

# --- PYODIDE:post --- #
# La section post contient du code de "nettoyage", à appliquer systématiquement
# après que le code et les tests aient été lancés.
# Ce contenu est exécuté même si une erreur a été levée précédemment, SAUF si
# cette erreur provient de la section ENV.