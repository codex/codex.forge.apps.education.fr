

# --------- PYODIDE:code --------- #

def nb_chemins_entiers(nb_lignes, nb_colonnes):
    ...

# --------- PYODIDE:corr --------- #

from math import sqrt

def nb_chemins_entiers(nb_lignes, nb_colonnes):
    sauts = []
    DK = int(sqrt(nb_lignes * nb_lignes + nb_colonnes * nb_colonnes))
    for di in range(nb_lignes):
        for dj in range(nb_colonnes):
            for dk in range(1, DK + 1):
                if di * di + dj * dj == dk * dk:
                    sauts.append((di, dj))

    compte = [[0 for j in range(nb_colonnes)] for i in range(nb_lignes)]
    compte[0][0] = 1
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            for di, dj in sauts:
                if (di <= i) and (dj <= j):
                    compte[i][j] += compte[i - di][j - dj]

    return compte[nb_lignes - 1][nb_colonnes - 1]

# --------- PYODIDE:tests --------- #

assert nb_chemins_entiers(2, 2) == 2
assert nb_chemins_entiers(4, 5) == 290
assert nb_chemins_entiers(8, 7) == 160758
assert nb_chemins_entiers(21, 41) == 403280419507196134012726226

# --------- PYODIDE:secrets --------- #


# Autres tests

"""
m = 10**9
for k in range(42):
    i = 7331 * k % 41
    j = (1337 * k % 37) + (k - 1)%5
    m += 1
    print((i+1, j+1, m, nb_chemins_entiers(i+1, j+1) % m), end=", ")
    if k&1: print()
print(41, 42, None, nb_chemins_entiers(41, 42))
"""

# (i, j, m, ans % m)
tests = [
        (1, 5, 1000000001, 8), (34, 6, 1000000002, 939471436),
        (26, 12, 1000000003, 929825360), (18, 18, 1000000004, 881821360),
        (10, 24, 1000000005, 913223104), (2, 30, 1000000006, 294967272),
        (35, 31, 1000000007, 333373776), (27, 37, 1000000008, 203344054),
        (19, 6, 1000000009, 199597703), (11, 12, 1000000010, 846612186),
        (3, 18, 1000000011, 4390912), (36, 19, 1000000012, 230215118),
        (28, 25, 1000000013, 175237281), (20, 31, 1000000014, 287766234),
        (12, 37, 1000000015, 83241889), (4, 6, 1000000016, 762),
        (37, 7, 1000000017, 796255212), (29, 13, 1000000018, 604939046),
        (21, 19, 1000000019, 237374657), (13, 25, 1000000020, 909142996),
        (5, 31, 1000000021, 593437640), (38, 32, 1000000022, 291220944),
        (30, 38, 1000000023, 323561191), (22, 7, 1000000024, 901469600),
        (14, 13, 1000000025, 258066871), (6, 19, 1000000026, 199597686),
        (39, 20, 1000000027, 207420097), (31, 26, 1000000028, 843485890),
        (23, 32, 1000000029, 19011326), (15, 38, 1000000030, 341394200),
        (7, 7, 1000000031, 56554), (40, 8, 1000000032, 773223456),
        (32, 14, 1000000033, 896839144), (24, 20, 1000000034, 717418430),
        (16, 26, 1000000035, 895344788), (8, 32, 1000000036, 851891468),
        (41, 33, 1000000037, 784053673), (33, 2, 1000000038, 580962434),
        (25, 8, 1000000039, 505680289), (17, 14, 1000000040, 863182414),
        (9, 20, 1000000041, 585550443), (1, 21, 1000000042, 524288),
        (41, 42, None, 19316672283705314874347270403127531150),
]

for nb_lignes, nb_colonnes, m, attendu in tests:
    if m is not None:
        resultat = nb_chemins_entiers(nb_lignes, nb_colonnes) % m
        assert resultat == attendu, f"Erreur avec {nb_lignes=} et {nb_colonnes=}"
    else:
        resultat = nb_chemins_entiers(nb_lignes, nb_colonnes)
        assert resultat == attendu, f"Erreur avec {nb_lignes=} et {nb_colonnes=} ; vérifier votre génération de triangles rectangles ; au cas limite !"