---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Sauts entiers
tags:
    - en travaux
    - programmation dynamique
    - chemin
---

# Chemins à sauts entiers sur une grille

- On part du coin supérieur gauche d'une grille,
- on ne fait que des sauts qui font progresser dans le secteur du coin opposé,
- les sauts sont à aussi à distance entière.

On souhaite compter le nombre de tels chemins.

!!! info "Un chemin à sauts entiers d'une grille de 8 lignes et 7 colonnes"
    :warning: On compte ici **les nœuds de la grille** et non ses cases. Il y a bien 8 lignes et colonnes.
    En partant d'un coin, ce chemin est composé des sauts suivants :

    ![TODO Figure à venir](./fig87.svg){ .autolight align="right" }

    - $\overrightarrow{OA}$, 1 en bas ; de distance 1,
    - $\overrightarrow{AB}$, 4 en bas et 3 à droite ; de distance 5,
    - $\overrightarrow{BC}$, 3 à droite, de distance 3,
    - $\overrightarrow{CD}$, 2 en bas, de distance 2.

    Il y a $160\,758$ tels chemins entiers de $O$ à $D$.

!!! question "Requête"
    Écrire une fonction `nb_chemins_entiers`
    
    - qui prend en paramètres :
        - `nb_lignes` : un entier strictement positif, le nombre de lignes de la grille,
        - `nb_colonnes` : un entier strictement positif, le nombre de colonnes de la grille,
    - qui renvoie un entier : le nombre de chemins à sauts entiers
        - pour aller du coin supérieur gauche au coin inférieur droit,
        - en ne réalisant que des sauts $\overrightarrow{MN}$ :
            - $d_i$ en bas,
            - $d_j$ à droite,
            - de distance $\sqrt{d_i^2+d_j^2}$ entière,
            - avec $d_i, d_j \in \mathbb N$.
    
    Dans les tests,
    
    - on garantit que `nb_lignes < 42` et `nb_colonnes < 43`,
    - on utilisera la fonction une cinquantaine de fois au plus.

???+ example "Exemple"

    ```pycon title=""
    >>> nb_chemins_entiers(2, 2)
    2
    >>> nb_chemins_entiers(4, 5)
    290
    >>> nb_chemins_entiers(8, 7)
    160758
    >>> nb_chemins_entiers(21, 41)
    403280419507196134012726226
    ```

{{ IDE('exo') }}


??? tip "Indice 1"
    Il faudrait générer tous les sauts possibles dans cette grille, donc tous les triangles rectangles à côtés entiers `(di, dj, dk)` avec `#!py 0 ≤ di ≤ nb_lignes`, `#!py 0 ≤ dj ≤ nb_colonnes` et `di² + dj²` égal à `dk²`.

    On pourra, par exemple, pour chaque `(di, dj, dk)` vérifier si ce triplet convient.

    > Cette méthode est peu performante, mais suffisante ici.

??? tip "Indice 2"
    On pourra créer un tableau 2D qui compte le nombre de chemins, on le remplira de manière dynamique, ligne par ligne, par exemple.

??? tip "Indice 3"
    On pourra utiliser le squelette de code suivant :

    ```python
    from math import sqrt

    def nb_chemins_entiers(nb_lignes, nb_colonnes):
        sauts = []
        ...
        for di in range(nb_lignes):
            for dj in range(nb_colonnes):
                for dk in range(...):
                    if ...:
                        sauts.append((di, dj))
        
        compte = [[0 for j in range(nb_colonnes)] for i in range(nb_lignes)]
        compte[...][...] = ...
        for i in range(nb_lignes):
            for j in range(nb_colonnes):
                for di, dj in sauts:
                    if ...:
                        compte[i][j] += ...
        
        return ...
    ```
