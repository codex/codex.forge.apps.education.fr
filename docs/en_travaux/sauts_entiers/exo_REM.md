!!! tip "Astuce 1 : Pré-calculer"
    Pour apporter un peu d'efficacité, on peut

    - calculer préalablement le tableau 2D des résultats pour le pire des cas (41, 42),
    - créer le fonction qui ne fait que lire et renvoyer la réponse.


```python
from math import sqrt

def pre_calcul(nb_lignes, nb_colonnes):
    sauts = []
    DK = int(sqrt(nb_lignes * nb_lignes + nb_colonnes * nb_colonnes))
    for di in range(nb_lignes):
        for dj in range(nb_colonnes):
            for dk in range(1, DK + 1):
                if di * di + dj * dj == dk * dk:
                    sauts.append((di, dj))
    
    compte = [[0 for j in range(nb_colonnes)] for i in range(nb_lignes)]
    compte[0][0] = 1
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            for di, dj in sauts:
                if (di <= i) and (dj <= j):
                    compte[i][j] += compte[i - di][j - dj]
    
    return compte

compte = pre_calcul(41, 42)

def nb_chemins_entiers(nb_lignes, nb_colonnes):
    return compte[nb_lignes - 1][nb_colonnes - 1]
```

!!! tip "Astuce 2 : Utiliser la symétrie"
    Pour un problème similaire **avec le nombre de lignes égale au nombre de colonnes**, il pourrait être un peu plus efficace d'utiliser la symétrie du tableau 2D, et de ne stocker/calculer qu'une seule moitié.

    Le cœur du code devient alors

    ```python
    cote = nb_lignes = nb_colonnes
    compte = [[0 for j in range(i + 1)] for i in range(cote)]
    compte[0][0] = 1
    for i in range(cote):
        for j in range(i + 1):
            for di, dj in sauts:
                if (di <= i) and (dj <= j):
                    if i - di >= j - dj:
                        compte[i][j] += compte[i - di][j - dj]
                    else:
                        compte[i][j] += compte[j - dj][i - di]
    ```

!!! tip "Astuce 3 : Mieux pré-calculer les triangles"
    Il existe des méthodes plus efficaces pour générer les triplets pythagoriciens.

    Utiliser la structure arborescente d'arité 3 est très efficace, mais il y a une méthode simple intermédiaire : boucler sur deux côtés, calculer le troisième, vérifier qu'il est bien entier.

    ```python
    sauts = []
    for di in range(nb_lignes):
        di2 = di * di
        for dj in range(nb_colonnes):
            dj2 = dj * dj
            dk2 = di2 + dj2
            dk = round(sqrt(dk2))
            if (di2 + dj2 == dk * dk) and (dk > 0):
                sauts.append((di, dj))
    ```
