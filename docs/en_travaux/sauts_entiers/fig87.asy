import math;
size(7cm, 0);

add(grid(6, 7, black+1bp));

pair pO = (0, 7), pA = (0, 6), pB = (3, 2), pC = (6, 2), pD = (6, 0);

draw(pO--pA--pB--pC--pD, blue+2bp);


dot("$O$", pO, NW, black);
dot("$A$", pA, WNW, black);
dot("$B$", pB, 1.5*SW, black);
dot("$C$", pC, NE, black);
dot("$D$", pD, SE, black);

label("$1$", (pO+pA)/2, E);
label("$5$", (pA+pB)/2, NNE);
label("$3$", (pB+pC)/2, N);
label("$2$", (pC+pD)/2, E);
