

# --------- PYODIDE:code --------- #

def partition(nombres, pivot):
    ...

# --------- PYODIDE:corr --------- #

def partition(nombres, pivot):
    debut = 0
    milieu = 0
    fin = len(nombres) - 1

    while milieu <= fin:
        if nombres[milieu] < pivot:
            nombres[milieu], nombres[debut] = nombres[debut], nombres[milieu]
            debut += 1
            milieu += 1
        elif nombres[milieu] == pivot:
            milieu += 1
        else:
            nombres[milieu], nombres[fin] = nombres[fin], nombres[milieu]
            fin -= 1


# --------- PYODIDE:tests --------- #

nombres = [3, 0, 2, 1, 5, 1]
partition(nombres, 1)
assert nombres == [0, 1, 1, 5, 2, 3]

nombres = [7, 2, 5, 5, 9, 1, 9, 7, 2, 5]
partition(nombres, 7)
assert nombres == [2, 5, 5, 5, 1, 2, 7, 7, 9, 9]


# --------- PYODIDE:secrets --------- #

nombres = list(range(10, -1, -1))
partition(nombres, 5)
assert nombres == list(range(11))

nombres = [5] + [0] * 10 + [10] * 10
partition(nombres, 5)
assert nombres == [0] * 10 + [5] + [10] * 10


from random import shuffle

nombres = [-3] * 10 + [-2] * 10 + [-1] * 10 + [0] * 10
shuffle(nombres)
partition(nombres, -1)
assert (
    all([x < -1 for x in nombres[:20]])
    and all([x == -1 for x in nombres[20:30]])
    and all([x > -1 for x in nombres[30:]])
)
