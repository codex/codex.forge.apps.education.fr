---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Partition autour d'un pivot
tags:
    - en travaux
    - tri
difficulty: 150
---


On se donne un tableau `nombres` non-vide ne contenant que des nombres entiers.

On souhaite réaliser une **partition** de ce tableau autour d'une certaine valeur appelée « pivot ». Une fois ce « tri » effectué, le tableau comportera dans l'ordre :

* toutes les valeurs strictement inférieures au pivot,
* toutes les valeurs égales au pivot,
* toutes les valeurs strictement supérieures au pivot.

Il s'agit d'une partition et non d'un tri, car la première et la dernière zone **ne sont pas nécessairement triées**.

Par exemple, la partition de `#!py [3, 0, 2, 1, 5, 1]` autour de la valeur pivot `#!py 1` donnera `#!py [0, 1, 1, 5, 2, 3]`.

Pour ce faire, on crée et l'on maintient quatre zones dans le tableau :

* la première zone ne contient que des valeurs strictement inférieures au pivot (en bleu ci-dessous),

* la deuxième que des valeurs égales au pivot,
* la troisième contient les valeurs mal placées (en gris),
* la dernière zone ne contient que des valeurs strictement supérieures au pivot (en rouge).

![Étape intermédiaire](figures/bis_etape_3.svg){width="35%"}

Ces zones sont délimitées par les bornes suivantes (incluses) :

* Valeurs strictement inférieures au pivot : de l'indice `#!py 0` à `#!py debut - 1`,
* Valeurs égales au pivot : de `#!py debut` à `#!py milieu - 1`,
* Valeurs mal placées : de `#!py milieu` à `#!py fin`,
* Valeurs strictement supérieures au pivot : de `#!py fin + 1` à `#!py len(nombres) - 1`

Au fil de l'algorithme, la zone contenant les valeurs mal placées rétrécit. L'algorithme se termine lorsqu'elle est vide.

L'algorithme à coder est donc le suivant

* on crée une variable `#!py debut` égale à `#!py 0`,
* on crée une variable `#!py milieu` égale à `#!py 0`,
* on crée une variable `#!py fin` égale au dernier indice de `#!py nombres`,
* on répète les étapes ci-dessous tant que `#!py milieu` est inférieur ou égal à `#!py fin` :
    * si la valeur d'indice `#!py milieu` est strictement inférieure au pivot, on l'échange avec celle d'indice `#!py debut` et l'on incrémente `#!py debut` et `#!py milieu`,
    * si elle est égale au pivot, on incrémente `#!py milieu`,
    * sinon, elle est strictement supérieure au pivot : on l'échange avec celle d'indice `#!py fin` et l'on décrémente `#!py fin`.

=== "Étape 0"

    ![Étape 0](figures/bis_etape_0.svg){width="35%"}

    Dans l'état initial aucun nombre n'a été trié.

    * `#!py debut = 0`
    * `#!py milieu = 0`
    * `#!py fin = 5`
  
=== "Étape 1"

    ![Étape 1](figures/bis_etape_1.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 0` et `#!py 5`. `#!py fin` a été décrémenté.

    * `#!py debut = 0`
    * `#!py milieu = 0`
    * `#!py fin = 4`

=== "Étape 2"

    ![Étape 2](figures/bis_etape_2.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 0` et `#!py 1`. `#!py milieu` a été incrémenté.

    * `#!py debut = 0`
    * `#!py milieu = 1`
    * `#!py fin = 4`

=== "Étape 3"

    ![Étape 3](figures/bis_etape_3.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 1` et `#!py 0`. `#!py debut` et `#!py milieu` ont été incrémentés.

    * `#!py debut = 1`
    * `#!py milieu = 2`
    * `#!py fin = 4`

=== "Étape 4"

    ![Étape 4](figures/bis_etape_4.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 2` et `#!py 4`. `#!py fin` a été décrémenté.

    * `#!py debut = 1`
    * `#!py milieu = 2`
    * `#!py fin = 3`

=== "Étape 5"

    ![Étape 5](figures/bis_etape_5.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 2` et `#!py 3`. `#!py fin` a été décrémenté.

    * `#!py debut = 1`
    * `#!py milieu = 2`
    * `#!py fin = 2`

=== "Étape 6"

    ![Étape 6](figures/bis_etape_6.svg){width="35%"}

    On a échangé les valeurs d'indices `#!py 2` et `#!py 2`. `#!py milieu` a été incrémenté.

    * `#!py debut = 1`
    * `#!py milieu = 3`
    * `#!py fin = 2`

    `#!py milieu` est désormais strictement supérieur à `#!py fin`. On sort de la boucle.

!!! tip "Échange de valeurs"

    Il est possible d'échanger deux valeurs d'indices `#!py i` et `#!py j` en faisant `#!py nombres[i], nombres[j] = nombres[j], nombres[i]`.

{{ interdiction('sorted, list.sort') }}

!!! example "Exemples"

    ```pycon
    >>> # Le pivot est 1
    >>> nombres = [3, 0, 2, 1, 5, 1]
    >>> tri_pivot(nombres, 1)
    >>> nombres
    [0, 1, 1, 5, 2, 3]
    ```

    Les trois zones sont `#!py [0]`, `#!py [1, 1]` et `#!py [5, 2, 3]`.

    ```pycon
    >>> # Le pivot est 7
    >>> nombres = [7, 2, 5, 5, 9, 1, 9, 7, 2, 5]
    >>> tri_pivot(nombres, 7)
    >>> nombres
    [2, 5, 5, 5, 1, 2, 7, 7, 9, 9]
    ```

    Les trois zones sont `#!py [2, 5, 5, 5, 1, 2]`, `#!py [7, 7]` et `#!py [9, 9]`.

Écrire la procédure `partition` qui prend en paramètres un tableau de nombres entiers et un entier représentant la valeur du pivot, et qui modifie en place le tableau.

??? note "Rappel : procédure"
    Une **procédure** est une fonction qui ne renvoie rien.

    En python, même si une fonction ne contient pas le mot-clé `return`, elle renverra toujours par défaut l'objet `None`.


{{ IDE('exo', SANS = 'sort, sorted') }}
