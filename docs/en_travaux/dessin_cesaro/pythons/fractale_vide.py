# --------- PYODIDE:env --------- #
from js import document

_prefixes_possibles = ["turtle", "tt"]
if "restart" in globals():
    restart()
else:
    for pref in _prefixes_possibles:
        if pref in globals():
            p = eval(pref)
            p.restart()
            break


def m_a_j():
    cible = "figure1"
    if "done" in globals():
        done()
        document.getElementById(cible).innerHTML = Screen().html
    else:
        for pref in _prefixes_possibles:
            if pref in globals():
                p = eval(pref)
                p.done()
                document.getElementById(cible).innerHTML = p.Screen().html
                break


# --- PYODIDE:code --- #
from turtle import *
from math import cos, pi

# Réglages initiaux
animation("off")   # sans animations
penup()            # le crayon est levé
goto(-100, -110)   # position initiale, à ajuster
pendown()          # le crayon est baissé


def courbe(longueur, etape, angle): 
    ...


def fractale(longueur, etape, angle, n, interieur): 
    ...


fractale(400, 4, 60, 3, True)  # le flocon de Koch
# --- PYODIDE:corr --- #
from turtle import *
from math import cos, pi

# Réglages initiaux
animation("off")   # sans animations
penup()            # le crayon est levé
goto(-100, -110)   # position initiale, à ajuster
pendown()          # le crayon est baissé


def courbe(longueur, etape, angle):
    if etape == 0:
        forward(longueur)
    else:
        longueur = longueur / (2 + 2 * cos(pi * angle / 180))
        courbe(longueur, etape - 1, angle)
        left(angle)
        courbe(longueur, etape - 1, angle)
        right(2 * angle)
        courbe(longueur, etape - 1, angle)
        left(angle)
        courbe(longueur, etape - 1, angle)


def fractale(longueur, etape, angle, n, interieur):
    rotation = 360 / n
    for _ in range(n):
        courbe(longueur, etape, angle)
        if interieur:
            left(rotation)
        else:
            right(rotation)


fractale(400, 4, 60, 3, True)  # le flocon de Koch
# --- PYODIDE:post --- #
if "post_async" in globals():
    await post_async()
if "Screen" in globals():
    if Screen().html is None:
        forward(0)
    m_a_j()
elif "turtle" in globals():
    if turtle.Screen().html is None:
        turtle.forward(0)
    m_a_j()

# --------- PYODIDE:post_term --------- #
if any(pref in globals() for pref in _prefixes_possibles) and "m_a_j" in globals():
    m_a_j()
