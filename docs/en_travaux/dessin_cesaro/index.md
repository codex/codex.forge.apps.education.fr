---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Dessiner les courbes de Cesàro
difficulty: 250
tags:
    - en travaux
    - récursivité
maj: 16/11/2024
---

{{ remarque('tortue') }}

??? note "Suite du flocon de Koch"    
    
    Cet exercice est un prolongement de celui sur {{ lien_exo('le flocon de Koch', 'dessin_koch') }}.
    
    Il est recommandé de le traiter avant d'aborder celui-ci.

Quel est le point commun entre ces différentes fractales ?

???+ note "Courbes tournées vers l'intérieur"

    <center>

    ![Triangle](images/150_4_60_3_False.svg){width=20% .autolight}
    ![Carré](images/150_4_88_4_False.svg){width=20% .autolight}
    ![Pentagone](images/150_4_75_5_False.svg){width=20% .autolight}
    </center>

Et celles-ci ?

???+ note "Courbes tournées vers l'extérieur"
    
    <center>

    ![Triangle](images/150_4_60_3_True.svg){width=18% .autolight}
    ![Carré](images/150_4_88_4_True.svg){width=23% .autolight}
    ![Pentagone](images/150_4_75_5_True.svg){width=23% .autolight}
    </center>

Elles sont toutes construites à l'aide du **même algorithme** ! Par contre, de l'une à l'autre, on a fait varier différents paramètres :

* le nombre de côtés du polygone de base (nommé par la suite `#!py n` qui vaut ci-dessus $3$, $4$ ou $5$) ;
* l'angle utilisé entre certains segments (nommé `#!py angle` et valant ci-dessus $60$, $88$ ou $75$ degrés);
* le fait de tracer les figures vers l'intérieur du polygone (en haut) ou l'extérieur (en bas). On appelle `#!py interieur` le booléen indiquant si la figure est construite vers l'intérieur ou non.

Dans chaque cas, la construction est la même :

* on construit un polygone à `#!py n` côtés. Selon la valeur du booléen `#!py interieur`, on tourne vers la gauche ou vers la droite entre chaque côté ;
* chaque « côté » du polygone est en fait une courbe de Cesàro[^1] (voir ci-dessous).

[^1]: [Ernesto Cesàro](https://fr.wikipedia.org/wiki/Ernesto_Ces%C3%A0ro) (1859 - 1906) était un mathématicien italien.

???+ note "Courbe de Cesàro pour les figures centrales"

    ![Courbe de Cesràro](images/courbe_150_4_88_4.svg){width=25% .autolight .center}

Etant donné une longueur, un angle (exprimé en degrés) et un nombre d'étapes donnés (les paramètres `#!py longueur`, `#!py angle` et `#!py etape`), la courbe de Cesàro peut se construire de façon **récursive** :

* à l'étape $0$, on trace un segment de la longueur donnée ;
* aux autres étapes, on lance la construction de quatre courbes de Cesàro, plus petites, comptant une étape de moins. Le coefficient de réduction vaut `#!py 1/(2 + 2*cos(pi*angle/180))`. L'angle n'est pas modifié ;
* entre deux courbes, on tourne d'un multiple de l'angle passé en paramètre (`#!py angle`).

![D'une courbe à l'autre](images/base_cesaro.svg){ .autolight .center width=30%}

On demande donc de compléter les fonctions `#!py courbe` et `#!py fractale` ci-dessous dont les paramètres ont été décrits ci-dessus.

N'hésitez pas à faire varier les paramètres pour créer votre fractale !

Par défaut, on a supprimé les animations de la figure.

{{ IDE('pythons/fractale_vide', MODE="delayed_reveal")}}

{{ figure(div_class="py_mk_figure center", admo_title='Votre fractale') }}

??? tip "Les différents paramètres"

    === "`#!py fractale(300, 4, 60, 3, True)`"
        ![Triangle](images/150_4_60_3_False.svg){width=20% .autolight .center}
    === "`#!py fractale(300, 4, 88, 4, True)`"
        ![Carré](images/150_4_88_4_False.svg){width=20% .autolight .center}
    === "`#!py fractale(300, 4, 75, 5, True)`"
        ![Pentagone](images/150_4_75_5_False.svg){width=20% .autolight .center}
    === "`#!py fractale(300, 4, 60, 3, False)`"
        ![Triangle](images/150_4_60_3_True.svg){width=18% .autolight .center}
    === "`#!py fractale(300, 4, 88, 4, False)`"
        ![Carré](images/150_4_88_4_True.svg){width=23% .autolight .center}
    === "`#!py fractale(300, 4, 75, 5, False)`"
        ![Pentagone](images/150_4_75_5_True.svg){width=23% .autolight .center}
    
