import drawsvg as draw
from math import pi, cos, sin


def degres(x):
    return 180 * x / pi


def radians(x):
    return pi * x / 180


def forward(longueur, t=None):
    if t is None:
        t = tortue
    a = radians(t["h"])
    nv_x = t["x"] + longueur * cos(a)
    nv_y = t["y"] + longueur * sin(a)
    d.append(draw.Line(t["x"], t["y"], nv_x, nv_y, **ARGS))
    t["x"] = nv_x
    t["y"] = nv_y


def left(angle, t=None):
    if t is None:
        t = tortue
    t["h"] -= angle


def right(angle, t=None):
    if t is None:
        t = tortue
    t["h"] += angle


def heading(t=None):
    if t is None:
        t = tortue
    return t["h"]


def position(t=None):
    if t is None:
        t = tortue
    return t["x"], t["y"]


def setheading(deg, t=None):
    if t is None:
        t = tortue
    t["h"] = deg


def penup():
    pass


def pendown():
    pass


def goto(x, y, t=None):
    if t is None:
        t = tortue
    t["x"] = x
    t["y"] = y


def courbe(longueur, etape, angle):
    if etape == 0:
        forward(longueur)
    else:
        longueur = longueur/(2+2*cos(pi*angle/180))
        courbe(longueur, etape - 1, angle)
        left(angle)
        courbe(longueur, etape - 1, angle)
        right(2*angle)
        courbe(longueur, etape - 1, angle)
        left(angle)
        courbe(longueur, etape - 1, angle)

def fractale(longueur, etape, angle, nb_cotes, dehors): 
    rotation = 360 / nb_cotes
    for _ in range(nb_cotes):
        courbe(longueur, etape, angle)
        if dehors:
            right(rotation)
        else:
            left(rotation)


fractales = [
    (-150, -100, 150, 4, 60, 3, True),
    (-80, -100, 150, 4, 88, 4, True),
    (-80, -100, 150, 4, 75, 5, True),
    (-150, 100, 150, 4, 60, 3, False),
    (-80, 100, 150, 4, 88, 4, False),
    (-150, -100, 150, 4, 75, 5, False),
]

for x, y, longueur, etape, angle, nb_cotes, dehors in fractales:
    d = draw.Drawing(320, 400, origin="center")
    ARGS = {
        "stroke": "black",
        "stroke_width": 1,
    }
    tortue = {"x": x, "y": y, "h": 0}

    if dehors:
        courbe(longueur, etape, angle)
        d.save_svg(f"courbe_{longueur}_{etape}_{angle}_{nb_cotes}.svg")

    # d = draw.Drawing(320, 400, origin="center")
    # ARGS = {
    #     "stroke": "black",
    #     "stroke_width": 1,
    # }
    # tortue = {"x": x, "y": y, "h": 0}

    # fractale(longueur, etape, angle, nb_cotes, dehors)
    # d.save_svg(f"{longueur}_{etape}_{angle}_{nb_cotes}_{dehors}.svg")
