# --------- PYODIDE:code --------- #
CODE_COULEUR = {"rouge":(255,0,0), "vert":(0,255,0), "bleu":(0,0,255)}

class Balle:
    '''Classe pour définir une balle graphique'''
    
    def __init__(self, couleur):
        self.couleur = CODE_COULEUR[couleur]
        self.x = 100
        self.y = 100
        
    def deplacer(self):
        '''Mise à jour de la position de la balle par incrémentation des coordonnées'''
        x += 1
        y += 1
    
    def __str__(self):
        return "Position de la balle : " + self.x + ", " + self.y
    
#Création de trois balles de couleurs différentes
balles = [Ballon("jaune"), Ballon("rouge"), Ballon("vert")]

for i in range(4):
    balles[i].deplace()
print(balles[0])         


# --------- PYODIDE:corr --------- #
CODE_COULEUR = {"rouge":(255,0,0), "vert":(0,255,0), "bleu":(0,0,255)}

class Balle:
    '''Classe pour définir une balle graphique'''
    def __init__(self, couleur):
        self.couleur = CODE_COULEUR[couleur]
        self.x = 100
        self.y = 100
        
    def deplacer(self):
        '''Mise à jour de la position de la balle par incrémentation des coordonnées'''
        self.x += 1
        self.y += 1
    
    def __str__(self):
        return "Position de la balle : " + str(self.x) + ", " + str(self.y)
    
#Création de trois balles de couleurs différentes
balles = [Balle("bleu"), Balle("rouge"), Balle("vert")]

for i in range(3):
    balles[i].deplacer()

# --------- PYODIDE:tests --------- #
assert len(balles) == 3

# --------- PYODIDE:secrets --------- #
assert balles[1].couleur == (255,0,0), "La deuxième balle n'a pas le bon code couleur"
assert balles[2].couleur == (0,255,0), "La troisième balle n'a pas le bon code couleur"
assert not balles[0].couleur in [(0,255,0), (255,0,0)], "Les trois balle n'ont pas des codes couleur différents"
assert str(balles[0]) == "Position de la balle : 101, 101", "La première balle n'est pas à lon bonne position"
