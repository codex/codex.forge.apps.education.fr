---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Correction d'erreurs (4)
tags:
    - en travaux
    - programmation orientée objet
difficulty: 150
---

Corriger le programme pour qu'il puisse s'exécuter sans erreur.

Le code doit créer $3$ objets de type `Balle` de couleurs différentes et les faire déplacer une seule fois.

{{IDE('exo')}}