# --------- PYODIDE:env --------- #
MOUVEMENTS = ((0, 1), (1, 0), (0, -1), (-1, 0))


def decoupe(instructions):
    return [int(c) if c.isnumeric() else c for c in instructions]


class Robot:
    def __init__(self, hauteur, largeur):
        self.hauteur = hauteur
        self.largeur = largeur
        self.grille = [[" " for _ in range(largeur)] for _ in range(hauteur)]
        self.i = 0
        self.j = 0
        self.grille[self.i][self.j] = "*"
        self.direction = 0

    def avance(self):
        """Fait avancer le robot d'une case (seulement si possible)"""
        di, dj = MOUVEMENTS[self.direction]
        if 0 <= self.i + di < self.hauteur and 0 <= self.j + dj < self.largeur:
            self.i += di
            self.j += dj
            self.grille[self.i][self.j] = "*"

    def droite(self):
        """Fait tourner le robot d'un quart de tour vers la droite"""
        self.direction = (self.direction + 1) % 4

    def gauche(self):
        """Fait tourner le robot d'un quart de tour vers la gauche"""
        self.direction = (self.direction - 1) % 4

    def dessine_parcours(self):
        """Affiche les cases parcourues et la position actuelle du robot"""
        affichage = [
            ["" for _ in range(self.largeur + 2)] for _ in range(self.hauteur + 2)
        ]
        for j in range(1, self.largeur + 1):
            affichage[0][j] = "─"
            affichage[-1][j] = "─"
        for i in range(1, self.hauteur + 1):
            affichage[i][0] = "│"
            affichage[i][-1] = "│"
        affichage[0][0] = "┌"
        affichage[-1][0] = "└"
        affichage[0][-1] = "┐"
        affichage[-1][-1] = "┘"
        for i in range(self.hauteur):
            for j in range(self.largeur):
                affichage[1 + i][1 + j] = self.grille[i][j]
        affichage[self.i + 1][self.j + 1] = [">", "v", "<", "^"][self.direction]
        print("\n".join("".join(ligne) for ligne in affichage))


# --------- PYODIDE:code --------- #
def deroule(instructions, i):
    resultat = []
    while i < len(instructions):
        instru = instructions[i]
        if instru == "(":
            sous_resultat, i = ...
            n = ...
            resultat = resultat + ... * ...
        elif instru == ")":
            return ..., ...
        else:
            ...
        i = i + 1
    return ..., ...


def execute(robot, instructions):
    instructions, _ = deroule(instructions, 0)
    
    i = 0
    while i < ...:
        # lecture de l'instruction
        instru = instructions[...]
        if i < ... and ...:
            n = ...
            i = ...
        else:
            n = ...
            i = ...

        # exécution de l'instruction
        for k in range(n):
            if instru == "D":
                ...
            ...  # plusieurs lignes possibles
# --------- PYODIDE:corr --------- #
def deroule(instructions, i):
    resultat = []
    while i < len(instructions):
        instru = instructions[i]
        if instru == "(":
            sous_resultat, i = deroule(instructions, i + 1)
            n = instructions[i]
            resultat += sous_resultat * n
        elif instru == ")":
            return resultat, i + 1
        else:
            resultat.append(instru)
        i += 1
    return resultat, i


def execute(robot, instructions):
    instructions, _ = deroule(instructions, 0)

    i = 0
    while i < len(instructions):
        # lecture de l'instruction
        instruction = instructions[i]
        if i < len(instructions) - 1 and isinstance(instructions[i + 1], int):
            n = instructions[i + 1]
            i += 2
        else:
            n = 1
            i += 1

        # exécution de l'instruction
        for _ in range(n):
            if instruction == "D":
                robot.droite()
            elif instruction == "G":
                robot.gauche()
            else:
                robot.avance()


# --------- PYODIDE:tests --------- #
robot = Robot(4, 4)
instructions = ["(", "A", 3, "D", ")", 4]
attendu = [
    ["*", "*", "*", "*"],
    ["*", " ", " ", "*"],
    ["*", " ", " ", "*"],
    ["*", "*", "*", "*"]
]
execute(robot, instructions)
assert robot.grille == attendu

robot = Robot(8, 8)
instructions = ["(", "(", "A", 2, "D", ")", 4, "A", 8, "D", ")", 4]
attendu = [
    ["*", "*", "*", "*", "*", "*", "*", "*"],
    ["*", " ", "*", " ", " ", "*", " ", "*"],
    ["*", "*", "*", " ", " ", "*", "*", "*"],
    ["*", " ", " ", " ", " ", " ", " ", "*"],
    ["*", " ", " ", " ", " ", " ", " ", "*"],
    ["*", "*", "*", " ", " ", "*", "*", "*"],
    ["*", " ", "*", " ", " ", "*", " ", "*"],
    ["*", "*", "*", "*", "*", "*", "*", "*"],
]
execute(robot, instructions)
assert robot.grille == attendu

# --------- PYODIDE:secrets --------- #
hauteur = 3
largeur = 3
instructions = list("AADAADAADADA")
attendu = [["*", "*", "*"], ["*", "*", "*"], ["*", "*", "*"]]
robot = Robot(hauteur, largeur)
execute(robot, instructions)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 4
largeur = 10
robot = Robot(hauteur, largeur)
code = ["A", 100, "D", 41, "A", 35, "D", 40, "A"]
attendu = [
    ["*", "*", "*", "*", "*", "*", "*", "*", "*", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "G", ")", 40, "A"]
attendu = [
    ["*", "*", "*"]
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "(", "(", "G", ")", 4, ")", 4, ")", 4, "A"]
attendu = [
    ["*", "*", "*"]
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "(", "(", "A", ")", 4, ")", 4, ")", 0]
attendu = [
    ["*", "*", " "]
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"
