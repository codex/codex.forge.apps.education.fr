---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Programmer un robot (3)
tags:
    - en travaux
    - string
    - récursivité
difficulty: 350
maj: 30/04/2024
---

??? note "Série d'exercices"

    Cet exercice fait partie d'une série :

    * « {{ lien_exo("Programmer un robot (0)", "robot_0") }} »,
    
    * « {{ lien_exo("Programmer un robot (1)", "robot_1") }} »,

    * « {{ lien_exo("Programmer un robot (2)", "robot_2") }} »,
    
    * « {{ lien_exo("Programmer un robot (3)", "robot_3") }} »,
    
    * « {{ lien_exo("Programmer un robot (4)", "robot_4") }} ».

    Il est fortement conseillé d'avoir traité les exercices précédents avant d'entreprendre celui-ci.

??? info "Rappels sur le fonctionnement du `Robot`"

    On considère dans cet exercice un robot se déplaçant sur une grille de dimensions finies. Initialement, il se trouve sur la case en haut à gauche de la grille et est dirigé vers la droite.

    Ce robot est représenté en Python par un objet de la classe `Robot`. L'interface de la classe `Robot` est la suivante :

    * `#!py robot = Robot(4, 5)` : instancie un objet de type `Robot` évoluant dans une grille de 4 cases de haut et 5 de large. Cet objet est affecté à la variable `robot` ;

    * `robot.avance()` : fait avancer le robot d'une case dans la direction actuelle. **Un déplacement qui ferait sortir le robot de la grille est ignoré** ;
    * `robot.droite()` : fait tourner le robot d'un quart de tour vers la gauche ;
    * `robot.gauche()` : fait tourner le robot d'un quart de tour vers la gauche ;
    * `robot.dessine_parcours()` : affiche la grille, les cases déjà parcourues et la position actuelle du robot dans la console.

    Un objet de type `Robot` contient aussi un attribut `grille`. Il s'agit d'une liste de listes gardant la trace des cases visitées (marquées par `#!py "*"`) ou non (laissées vides `#!py " "`).

    ??? example "Exemple d'utilisation d'un `Robot`"

        ```pycon title=""
        >>> robot = Robot(4, 3)       # la grille fait 4 case de haut sur 3 de large
        >>> robot.grille              # le robot est en haut à gauche
        [['*', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
        >>> robot.dessine_parcours()  # le robot pointe vers la droite
        ┌───┐
        │>  │
        │   │
        │   │
        │   │
        └───┘
        >>> robot.avance()
        >>> robot.avance()
        >>> robot.droite()
        >>> robot.avance()
        >>> robot.avance()
        >>> robot.avance()
        >>> robot.dessine_parcours()
        ┌───┐
        │***│
        │  *│
        │  *│
        │  v│
        └───┘
        >>> robot.gauche()
        >>> robot.avance()
        >>> robot.dessine_parcours()
        ┌───┐
        │***│
        │  *│
        │  *│
        │  >│
        └───┘
        >>> robot.grille
        [['*', '*', '*'], [' ', ' ', '*'], [' ', ' ', '*'], [' ', ' ', '*']]
        ```


    ??? info "La classe `Robot`"

        ```python title=""
        MOUVEMENTS = ((0, 1), (1, 0), (0, -1), (-1, 0))


        class Robot:
            def __init__(self, hauteur, largeur):
                self.hauteur = hauteur
                self.largeur = largeur
                self.grille = [[" " for _ in range(largeur)] for _ in range(hauteur)]
                self.i = 0
                self.j = 0
                self.grille[self.i][self.j] = "*"
                self.direction = 0

            def avance(self):
                """Fait avancer le robot d'une case (seulement si possible)"""
                di, dj = MOUVEMENTS[self.direction]
                if 0 <= self.i + di < self.hauteur and 0 <= self.j + dj < self.largeur:
                    self.i += di
                    self.j += dj
                    self.grille[self.i][self.j] = "*"

            def droite(self):
                """Fait tourner le robot d'un quart de tour vers la droite"""
                self.direction = (self.direction + 1) % 4

            def gauche(self):
                """Fait tourner le robot d'un quart de tour vers la gauche"""
                self.direction = (self.direction - 1) % 4

            def dessine_parcours(self):
                """Affiche les cases parcourues et la position actuelle du robot"""
                affichage = [
                    ["" for _ in range(self.largeur + 2)] for _ in range(self.hauteur + 2)
                ]
                for j in range(1, self.largeur + 1):
                    affichage[0][j] = "─"
                    affichage[-1][j] = "─"
                for i in range(1, self.hauteur + 1):
                    affichage[i][0] = "│"
                    affichage[i][-1] = "│"
                affichage[0][0] = "┌"
                affichage[-1][0] = "└"
                affichage[0][-1] = "┐"
                affichage[-1][-1] = "┘"
                for i in range(self.hauteur):
                    for j in range(self.largeur):
                        affichage[1 + i][1 + j] = self.grille[i][j]
                affichage[self.i + 1][self.j + 1] = [">", "v", "<", "^"][self.direction]
                print("\n".join("".join(ligne) for ligne in affichage))
        ```

    La classe `Robot` est déjà chargée dans l'éditeur, **vous pouvez l'utiliser sans l'importer**.

    On programme ce robot en lui faisant effectuer différentes actions :

    * le code `#!py "A"` fait avancer le robot ;
    * le code `#!py "D"` le fait tourner vers la droite ;
    * le code `#!py "G"` le fait tourner vers la gauche.

    Il est aussi possible d'utiliser des instructions du type `#!py [action, n]` dans lesquelles :

    * `#!py action` est une des trois actions décrites ci-dessus ;
    * `#!py n` est un nombre entier positif ou nul.

    Lors d'une répétition, on exécute un maximum d'actions valides. Si par exemple, le robot ne peut avancer que de 5 cases, les instructions `#!py ["A", 8]` le feront avancer de 5 cases, seules les trois dernières actions seront ignorées.


??? tip "Fonction `decoupe`"

    On fournit une fonction `decoupe` rudimentaire permettant de transformer une chaîne de caractères formant une suite d'instructions en une liste.
    
    ```python title=""
    def decoupe(instructions):
        return [int(c) if c.isnumeric() else c for c in instructions]

    decoupe("A3GD5")  # renvoie ['A', 3, 'G', 'D', 5]
    ```
    
    Cette fonction est déjà importée dans l'éditeur. Vous pouvez l'utiliser pour écrire des tests personnels.
    
    Cette fonction **ne gère que les entiers positifs strictement inférieurs à 10** et ne filtre pas les instructions incorrectes. Ainsi `#!py decoupe("Z12")` renvoie `#!py ['Z', 1, 2]`. 
    
    {{ lien_exo("Le premier exercice de la série", "robot_0") }} a pour objectif d'écrire une version valide de cette fonction.


Les instructions permettant de dessiner un carré de 4 cases de côtés sont `#!py ["A", 3, "D", "A", 3, "D", "A", 3, "D", "A", 3]`. Cette série d'instructions comporte un *motif* `#!py ["A", 3, "D"]` répété plusieurs fois.

On autorise désormais les répétitions de *motifs* en écrivant des instructions au format `#!py ["(", <instructions>, ")", n]` dans lesquelles :

* `#!py <instructions>` est une suite d'instructions valides ;
* `#!py n` est un nombre entier positif ou nul.

En utilisant ce format, les instructions construisant le carré deviennent : `#!py ["(", "A", 3, "D", ")", 4]`.

Il est possible d'imbriquer des motifs répétés comme dans `#!py ["(", "(", "A", 2, "D", ")", 4, "A", 8, "D", ")", 4]` qui permet d'obtenir :

```text title="Motifs imbriqués"
┌────────┐
│********│
│* *  * *│
│***  ***│
│*      *│
│*      *│
│***  ***│
│* *  * *│
│********│
└────────┘
```

Si certaines actions d'un motif répété font sortir le robot de la grille elles sont ignorées. Les autres instructions sont par contre exécutées.

Il existe plusieurs façon d'interpréter une série d'instructions de ce type. On propose ci-dessous une méthode permettant de *dérouler* les motifs répétés avant l'exécution proprement dite.

??? tip "Aide : dérouler les instructions"

    L'objectif de cette première étape est d'éliminer les motifs répétés en les transformant en une série d'instructions simples. On transforme ainsi `#!py ["(", "A", 3, "D", ")", 4]` en `#!py ["A", 3, "D", "A", 3, "D", "A", 3, "D", "A", 3, "D"]`.

    La fonction `deroule` est récursive et prend deux paramètres :

    * la suite d'instructions à convertir,
    * l'indice de la première instruction à considérer.

    Cette fonction construit une liste d'instructions en y ajoutant toutes les instructions simples (`#!py "A"`, `#!py "D"`, `#!py "G"` ou un entier positif ou nul) rencontrées lors du parcours à partir de l'indice fourni en argument.

    Si lors du parcours la fonction rencontre une parenthèse ouvrante :

    * elle s'appelle elle-même à partir de l'indice suivant celui de cette parenthèse,
    * elle ajoute à la série d'instructions simples, le résultat de l'appel précédent répété autant de fois que nécessaire.

    Enfin, si lors du parcours la fonction rencontre une parenthèse fermante, elle renvoie la liste d'instructions simples construite ainsi que l'indice de la prochaine instruction à étudier (qui est celui du nombre de répétitions du motif).

    ![Appels récursifs](deroule.PNG){.center .autolight width=50%}


Écrire la fonction `execute` (et la fonction `deroule` si vous le souhaitez) qui prend en paramètres un objet de type `Robot` et une liste d'instructions et fait effectuer chacune de ces instructions par le robot.

On garantit que :

* toutes les instructions sont valides (`#!py "A"`, `#!py "D"`, `#!py "G"`, `#!py "("`, `#!py ")"` ou un entier positif ou nul),
* chaque entier suit une action valide ou une parenthèse fermante,
* la suite d'instructions est correctement parenthésée.

???+ example "Exemple"

    ```pycon title=""
    >>> robot = Robot(4, 4)
    >>> instructions = ["(", "A", 3, "D", ")", 4]
    >>> execute(robot, instructions)
    >>> robot.dessine_parcours()
    ┌────┐
    │>***│
    │*  *│
    │*  *│
    │****│
    └────┘
    ```

=== "Version vide"
    {{ IDE('exo_vide')}}
=== "Version à compléter"
    {{ IDE('exo_trous')}}

