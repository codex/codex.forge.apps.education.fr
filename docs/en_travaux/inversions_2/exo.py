

# --------- PYODIDE:code --------- #

def inversions(tab):
    ...

# --------- PYODIDE:corr --------- #

def inversions(tab):
    if len(tab) <= 1:
        return 0

    N = len(tab)
    gauche = tab[: N // 2]
    droite = tab[N // 2 :]
    inv_gauche = inversions(gauche)
    inv_droite = inversions(droite)

    total = 0
    i_gauche = 0
    i_droite = 0
    i_tab = 0

    while i_gauche < len(gauche) and i_droite < len(droite):
        if gauche[i_gauche] <= droite[i_droite]:
            tab[i_tab] = gauche[i_gauche]
            i_gauche += 1
        else:
            total += len(gauche) - i_gauche
            tab[i_tab] = droite[i_droite]
            i_droite += 1
        i_tab += 1

    while i_gauche < len(gauche):
        tab[i_tab] = gauche[i_gauche]
        i_gauche += 1
        i_tab += 1

    while i_droite < len(droite):
        tab[i_tab] = droite[i_droite]
        i_droite += 1
        i_tab += 1

    return inv_gauche + inv_droite + total


# Tests
assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3

# --------- PYODIDE:tests --------- #

assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3

# --------- PYODIDE:secrets --------- #

import random
import itertools


class Entier:
    def __init__(self, valeur, compteur):
        self.valeur = valeur
        self.compteur = compteur

    def __lt__(self, autre):
        next(self.compteur)
        return self.valeur.__lt__(autre.valeur)

    def __gt__(self, autre):
        next(self.compteur)
        return self.valeur.__gt__(autre.valeur)

    def __ge__(self, autre):
        next(self.compteur)
        return self.valeur.__ge__(autre.valeur)

    def __le__(self, autre):
        next(self.compteur)
        return self.valeur.__le__(autre.valeur)

    def __eq__(self, autre):
        next(self.compteur)
        return self.valeur.__eq__(autre.valeur)

    def __ne__(self, autre):
        next(self.compteur)
        return self.valeur.__ne__(autre.valeur)


def _inversions(tab):
    if len(tab) <= 1:
        return 0

    N = len(tab)
    gauche = tab[: N // 2]
    droite = tab[N // 2 :]
    inv_gauche = _inversions(gauche)
    inv_droite = _inversions(droite)

    total = 0
    i_gauche = 0
    i_droite = 0
    i_tab = 0

    while i_gauche < len(gauche) and i_droite < len(droite):
        if gauche[i_gauche] <= droite[i_droite]:
            tab[i_tab] = gauche[i_gauche]
            i_gauche += 1
        else:
            total += len(gauche) - i_gauche
            tab[i_tab] = droite[i_droite]
            i_droite += 1
        i_tab += 1

    while i_gauche < len(gauche):
        tab[i_tab] = gauche[i_gauche]
        i_gauche += 1
        i_tab += 1

    while i_droite < len(droite):
        tab[i_tab] = droite[i_droite]
        i_droite += 1
        i_tab += 1

    return inv_gauche + inv_droite + total
# Tests supplémentaires
maximum = 10**6
for _ in range(20):
    valeurs = random.sample(range(maximum), random.randint(10**3, 2 * 10**3))
    N = len(valeurs)

    compteur = itertools.count()
    tab_correction = [Entier(k, compteur) for k in valeurs]
    resultat = _inversions(tab_correction)
    comparaisons_correction = next(compteur)

    compteur = itertools.count()
    tab_utilisateur = [Entier(k, compteur) for k in valeurs]
    assert (
        inversions(tab_utilisateur) == resultat
    ), f"Mauvais nombre d'inversions avec un tableau de taille {N}"
    comparaisons_utilisateur = next(compteur)
    assert (
        comparaisons_utilisateur <= 2 * comparaisons_correction
    ), f"Trop de comparaisons. Il en faut moins de {2 * comparaisons_correction}, votre code en a effectué {comparaisons_utilisateur}"