---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Inversions dans un grand tableau
tags:
    - en travaux
    - diviser pour régner
status: relecture
---

On considère dans cet exercices des tableaux d'entiers.

Soit `tab` un tel tableau. On appelle **inversion** un couple d'indices distincts `i`et `j` tel que :

* `i` est plus **petit** que `j` ;
* `tab[i]` est strictement plus **grand** que `tab[j]`.

Considérons par exemple dans le tableau :

 ```py
 # indices  0  1  2  3
 tab     = [7, 5, 9, 6]
 ```
 
 Ce tableau compte 3 inversions :

* pour les indices `#!py 0` et `#!py 1` car `tab[0]` (qui vaut `#!py 7`) est strictement supérieur à `tab[1]` (qui vaut `#!py 5`),
* pour les indices `#!py 0` et `#!py 3` car `tab[0]` (qui vaut `#!py 7`) est strictement supérieur à `tab[3]` (qui vaut `#!py 6`),
* pour les indices `#!py 2` et `#!py 3` car `tab[2]` (qui vaut `#!py 9`) est strictement supérieur à `tab[3]` (qui vaut `#!py 6`).


On demande d'écrire la fonction `inversions` qui prend en argument un tableau d'entiers et renvoie son nombre d'inversions.

On convient qu'un tableau vide compte 0 inversions.

Les tableaux utilisés dans les tests sont de **grande taille** (1 000 éléments ou plus). Ainsi, une approche *naïve* à l'aide d'une double boucle effectuera de trop nombreuses comparaisons (près de 500 000 pour un tableau de 1 000 valeurs !). À titre de comparaison, le corrigé de cet exercice effectue moins de 10 000 comparaisons pour un tableau de même taille.

Afin de mesurer l'efficacité de l'algorithme utilisé, les tests de cet exercice comptent donc le nombre de comparaisons de valeurs effectuées. Le test échoue si ce nombre est supérieur à deux fois le nombre de comparaisons effectuées par l'algorithme du corrigé.

???+ example "Exemples"

    ```pycon title=""
    >>> inversions([])
    0
    >>> inversions([5, 6, 7, 9])
    0
    >>> inversions([7, 5, 9, 6])
    3
    ```

{{ IDE('exo') }}

??? tip "Conseil (1)"

    On pourra utiliser une méthode de tri *efficace* (de coût non quadratique).

??? tip "Conseil (2)"

    Il est facile de repérer les inversions en utilisant le tri fusion...

??? tip "Conseil (3)"

    Commencez par compter les inversions dans la partie gauche du tableau, celles dans la partie droite puis celles existant entre les deux parties.