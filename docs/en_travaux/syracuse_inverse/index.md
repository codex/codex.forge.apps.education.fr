---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Arbre de Collatz
tags:
    - en travaux
    - arbre
maj: 16/05/2024
---

La suite de *Syracuse*, autrement connue sous le nom de suite de *Collatz*, est définie de la façon suivante :

* on choisit comme valeur de départ un entier **strictement positif**,
* à chaque étape on calcule une nouvelle valeur en appliquant les règles suivantes :
    * si la valeur actuelle est paire, la nouvelle valeur est égale à sa moitié,
    * sinon, la nouvelle valeur est égale à son triple augmenté de 1.

Exprimé en termes mathématiques, on a donc $u_0$ un entier strictement positif et pour tout $n$ entier naturel :

$$u_{n+1}=\begin{cases}
    \dfrac{u_n}{2}&\text{si } n \text{ est pair}\\
    3u_n+1&\text{sinon }\\
\end{cases}$$

En prenant comme valeur initiale $u_0=3$ on obtient la succession suivante :

```mermaid
    graph LR
    Q(3) --> R(10)
    R --> A(5)
    A --> B(16)
    B --> C(8)
    C --> D(4)
    D --> E(2)
    E --> F(1)
    F --> D
```

Comme on peut le voir, lorsque l'on a atteint la valeur $1$, on rentre dans un cycle $1\rightarrow 4\rightarrow 2\rightarrow 1\rightarrow \dots$.

Partant de $3$, il faut calculer sept termes avant d'obtenir $1$.

Il est possible de construire un graphe, appelé *graphe de Collatz*, représentant les trajectoires de différents entiers jusqu'à ce qu'ils atteignent la valeur $1$.  On ne représente pas le cycle $1\rightarrow4\rightarrow2\rightarrow1$ et **l'on admet que pour les valeurs étudiées dans cet exercice il s'agit d'un arbre enraciné** dont la racine est le nœud de valeur $1$.

```mermaid
    graph LR
        subgraph sg ["h=7"]
        K(21)
        L(128)
        J(20)
        I(3)
        end
        G(10)
        H(64)
        F(32)
        E(5)
        D(16)
        C(8)
        B(4)
        A(2)
        R(1)
        K --> H
        L --> H
        I --> G
        J --> G
        H --> F
        G --> E
        E --> D
        F --> D
        D --> C
        C --> B
        B --> A
        A --> R
```

L'arbre représenté ci-dessus est de hauteur $7$, ses feuilles ont pour valeurs $\left[3;20;21;128\right]$.

La racine de l'arbre a pour valeur $1$. Les autres nœuds sont obtenus de la façon suivante. Soit $n$ la valeur d'un nœud :

* dans tous les cas, ce nœud a pour enfant un nouveau nœud de valeur $2n$,
* si de plus, $n$ est strictement supérieur à $6$ et si la division euclidienne de $n$ par $6$ donne un reste de $4$, on ajoute un autre enfant de valeur $\dfrac{n - 1}{3}$.

On demande d'écrire la fonction `feuilles` qui prend en argument la hauteur de l'arbre souhaité et renvoie la liste **triée** des valeurs des feuilles de cet arbre.


Les hauteurs passées en argument seront toujours des entiers compris entre $0$ et $30$.

??? note "Trier une liste"
    
    On rappelle qu'il est possible de trier une liste `nombres` en faisant `nombres.sort()` (tri en place) ou `sorted(nombres)` (qui renvoie une nouvelle liste triée contenant les mêmes éléments).
    
    ```pycon title="Tri en place"
    >>> nombres = [2, 0, 1]
    >>> nombres.sort()
    >>> nombres
    [0, 1, 2]
    ```
    
    ```pycon title="Nouvelle liste"
    >>> nombres = [2, 0, 1]
    >>> sorted(nombres)
    [0, 1, 2]
    >>> nombres
    [2, 0, 1]
    ```

???+ example "Exemples"

    ```pycon title=""
    >>> feuilles(0)
    [1]
    >>> feuilles(1)
    [2]
    >>> feuilles(5)
    [5, 32]
    >>> feuilles(7)
    [3, 20, 21, 128]
    >>> feuilles(14)
    [11, 68, 69, 70, 75, 384, 416, 424, 426, 452, 453, 454, 2560, 2688, 2720, 2728, 2730, 16384]
    ```

??? tip "Aide"

    Chaque niveau d'indice supérieur ou égal à $1$ peut être déterminé à partir du niveau précédent.

{{ IDE('exo') }}