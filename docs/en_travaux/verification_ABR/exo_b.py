# --------- PYODIDE:env --------- #
def minimum_abr(arbre):
    ...
# --------- PYODIDE:code --------- #
def minimum_abr(arbre):
    ...
# --------- PYODIDE:corr --------- #
def minimum_abr(arbre):
    if arbre[0] is None:
        return arbre[1]
    else:
        return minimum_abr(arbre[0])

# --------- PYODIDE:tests --------- #
n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
ab1 = (n0, 1, n2)


assert minimum_abr(ab1) == 0
# --------- PYODIDE:secrets --------- #
ab3 = (None, 5, None)
assert minimum_abr(ab3) == 5

ab5 = (((None, 3, None), 4, None), 5, ((None, 6, None), 7, None))
assert minimum_abr(ab5) == 3