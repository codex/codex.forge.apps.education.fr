Il est possible de réaliser une version itérative de la fonction :
```python
def minimum_abr(arbre):
    while arbre[0] != None:
        arbre = arbre[0]
    return arbre[1]
```