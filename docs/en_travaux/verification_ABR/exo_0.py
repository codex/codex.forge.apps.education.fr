# --------- PYODIDE:env --------- #

import p5, js

from lib_draw import AffichageAbCompact

afficheur = AffichageAbCompact(
    est_vide = lambda ab: not ab,
    gauche = lambda ab: ab[0],
    droit  = lambda ab: ab[2],
    valeur = lambda ab: ab[1],
    creer_vide = lambda: None,
    taille_fenetre = (750, 400),
)

def est_trie(tableau):
    for i in range(1, len(tableau)):
        if tableau[i - 1] > tableau[i]:
            return False
    return True

def infixe(arbre):
    if arbre is None:
        return []
    else:
        sag, valeur, sad = arbre
        return infixe(sag) + [valeur] + infixe(sad)

# --------- PYODIDE:code --------- #
def est_abr(arbre):
    ...
# --------- PYODIDE:corr --------- #
def est_trie(tableau):
    for i in range(1, len(tableau)):
        if tableau[i - 1] >= tableau[i]:
            return False
    return True

def infixe(arbre):
    if arbre is None:
        return []
    else:
        sag, valeur, sad = arbre
        return infixe(sag) + [valeur] + infixe(sad)

def est_abr(arbre):
    return est_trie(infixe(arbre))

# --------- PYODIDE:tests --------- #

a0 = (None, 0, None)
a3 = (None, 3, None)
a2 = (None, 2, a3)
ab1 = (a0, 1, a2)
assert est_abr(ab1) is True

a4 = (None, 4, None)
a0 = (None, 0, a4)
a3 = (None, 3, None)
a2 = (None, 2, a3)
ab2 = (a0, 1, a2)
assert est_abr(ab2) is False

a0 = (None, 0, None)
a3 = (None, 3, None)
a4 = (None, 2, None)
a2 = (a4, 2, a3)
ab3 = (a0, 1, a2)
assert est_abr(ab3) is False

ab4 = None
assert est_abr(ab4) is True

# --------- PYODIDE:secrets --------- #
@auto_run               # pylint: disable=E0602
async def _():
    # global timer      # To make tries through the terminal


    from math import inf
    from random import choice, randrange as rand
    from collections import Counter

    from lib_tests import build_announcer, timer

    CHECK = False       # Comment out to control from outside
    USE_SLOW = False    # Comment out to control from outside

    DRAW = plt = False
    # DRAW = True ; import matplotlib.pyplot as plt
        # Uncomment to see the matplotlib drawing
        # (import commented out to avoid automatic installation)

    key = "Yepikaïe1"
    annonce = build_announcer(key)



    async def test_ab(ab, exp=None):
        if exp is None:
            exp = ref_est_abr(ab)
        act = est_abr(ab)

        if act is not exp or CHECK:
            nope  = 'n\'est pas'
            title = f"Cet arbre { 'est' if exp else nope } un ABR."
            afficheur.changer_arbre(ab,title)
            await js.sleep()
            if CHECK:
                input('resume?')

        assert act is exp, "Les tests ont échoué avec l'arbre affiché ci-dessous"
        return exp



    def lin_build(data, l, h):
        """
        Construit un ABR équilibrée à partir d'une liste triée. Si l'élément vaut None,
        renvoie simplement None et interrompt la construction pour ce sous-arbre.
        """
        m = l+h >> 1
        if not data[m]:
            return None
        abr = (
            None if l==m else lin_build(data, l, m-1),
            data[m],
            None if m==h else lin_build(data, m+1, h),
        )
        return abr



    def almost_abr(size=31, tweak=True):
        v = rand(15)
        lst = [ (v:=v+1+rand(5)) for _ in range(size) ]

        profile = rand(4)
        if tweak:
            if profile == 0:
                i,j = rand(size), rand(size)
                lst[j] = lst[i]

            elif profile == 1:
                i = rand(size)
                j = choice( (i-1,)*(i>0) + (i+1,)*(i<30) )
                lst[j] = lst[i]

            elif profile == 2:
                i = rand(size)
                lst[i] += rand(-10,10)

            nullify = {rand(size) for _ in range(rand(7))} - {size-1 >> 1}
            for i in nullify:
                lst[i] = None

        return lin_build(lst, 0, size-1), profile




    def ref_est_abr(arbre):
        def interne(mini, ab, maxi):
            if not ab:
                return True
            g,v,d = ab
            return mini < v and v < maxi and interne(mini, g, v) and interne(v, d, maxi)

        return interne(-inf, arbre, inf)



    async with annonce("Tests fixes"):
        ab4 = (None, 5, None)
        await test_ab(ab4, True)
        
        ab5 = ((None, 7, None), 5, None)
        await test_ab(ab5, False)
      

        ab6 = (((None, 3, None), 4, None), 5, ((None, 6, None), 7, None))
        await test_ab(ab6, True)
       

        ab7 = (None, 9, ab6)
        await test_ab(ab7, False)
       

        ab8 = (ab6, 9, (None, 10, None))
        await test_ab(ab8, True)

        ab9 = (((None, 2, (None, 1, None)), 4, None), 5, ((None, 6, None), 7, None))
        await test_ab(ab9, False)


        ab10 = (None, 3, ab5)
        await test_ab(ab10, False)
      





    async with annonce("Tests aléatoires"):
        stats = [Counter() for _ in range(4)]
        for _ in range(100):
            ab, profile = almost_abr()
            exp = await test_ab(ab)
            stats[profile][exp] += 1

        if CHECK:
            print(stats)