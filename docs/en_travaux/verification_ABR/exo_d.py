# --------- PYODIDE:env --------- #
def infixe(arbre):
    ...
# --------- PYODIDE:code --------- #
def infixe(arbre):
    ...
# --------- PYODIDE:corr --------- #
def infixe(arbre):
    if arbre is None:
        return []
    else:
        sag, valeur, sad = arbre
        return infixe(sag) + [valeur] + infixe(sad)

# --------- PYODIDE:tests --------- #
n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
ab1 = (n0, 1, n2)


assert infixe(ab1) == [0, 1, 2, 3]
# --------- PYODIDE:secrets --------- #
ab3 = (None, 5, None)
assert infixe(ab3) == [5]

ab4 = None
assert infixe(ab4) == []

ab5 = (((None, 3, None), 4, None), 5, ((None, 6, None), 7, None))
assert infixe(ab5) == [3, 4, 5, 6, 7]