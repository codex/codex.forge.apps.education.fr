### Aller plus loins { .fake_rem_h3 }

L'exercice {{ lien_exo("Vérification d'UN ABR (2)", "verification_ABR_2") }} permet d'aller plus loin sur le sujet, en imposant de valider des ABR avec doublons, en utilisant une méthode optimale.

### Optimalité en temps { .fake_rem_h3 }

Si la fonction `est_triee` est ici optimale (linéaire en temps).

---8<--- "docs/en_travaux/verification_ABR/exo_d_REM.md"