La solution proposée ici n'est pas optimale car il est nécessaire de parcourir plusieurs fois l'arbre : on parcourt les branches minimale et maximale de chaque sous-arbre pour vérifier s'ils sont des ABR, ce qui implique que l'on va passer plusieurs fois sur les mêmes noeuds/valeurs.


L'exercice {{ lien_exo("Vérification d'UN ABR (2)", "verification_ABR_2") }} permet d'aller plus loin sur le sujet, en imposant justement de valider des ABR avec doublons, en utilisant une méthode optimale.
