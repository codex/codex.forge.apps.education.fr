La fonction `infixe` utilisée ici n'est pas optimale en temps, car elle utilise des concaténations de listes :

```python title=""
    return infixe(sag) + [valeur] + infixe(sad)
```

<br>

Obtenir une fonction `infixe` linéaire en temps requiert un peu plus de logistique et peut être réalisé de différentes façons :

=== "Second argument"

    On pourrait modifier la fonction infixe pour qu'elle prenne une liste en second argument, dans laquelle les valeurs seraient ajoutées au fur et à mesure. La fonction renverrait cette liste de manière à pouvoir la récupérer facilement dans la fonction appelante.

    Globalement, on aurait alors le jeu de fonctions suivant :

    ```python title=""
    def est_trie(tableau):
        for i in range(1, len(tableau)):
            if tableau[i - 1] > tableau[i]:
                return False
        return True

    def infixe(arbre, lst):
        if arbre is not None:
            sag, valeur, sad = arbre
            infixe(sag, lst)
            lst.append(valeur)  # Traitement infixe, entre les appels aux enfants
            infixe(sad, lst)
        return lst

    def verification_abr(arbre):
        return est_trie(infixe(arbre, []))
    ```

=== "Sous-fonction"

    On pourrait aussi utiliser une sous-fonction dans `infixe`, pour éviter d'avoir à passer une liste en argument :

    ```python title=""
    def est_trie(tableau):
        for i in range(1, len(tableau)):
            if tableau[i - 1] > tableau[i]:
                return False
        return True

    def infixe(arbre):

        def interne(arbre, lst):         # cette fonction n'a plus besoin de renvoyer lst
            if arbre is not None:
                sag, valeur, sad = arbre
                interne(sag, lst)
                lst.append(valeur)  # Traitement infixe, entre les appels aux enfants
                interne(sad, lst)
            return lst
    
        return  interne(arbre, [])

    def verification_abr(arbre):
        return est_trie(infixe(arbre))
    ```

    Des trois versions proposées, c'est la plus propre, car elle encapsule la logique interne de la fonction, empêchant l'utilisateur de la modifier.


=== "Argument par défaut"

    Cette solution est la moins bonne des trois, même si c'est la plus simple à mettre en place.

    Le désavantage est qu'on expose un argument supplémentaire que l'utilisateur est sensé ignorer. Mais s'il ne l'ignore pas, il peut en fait "casser" la logique du parcours infixe. C'est un peu comme si on proposait à l'utilisateur de casser l'encapsulation en programmation objet.

    Il est à noter que la première approche, avec deux arguments, est sujette au même problème, à la différence prêt que l'utilisateur _doit_ passer une liste en argument. C'est donc de sa responsabilité de passer une liste vide, comme attendu par la fonction : la spécification est "explicite" (sous réserve d'écrire le docstring de la fonction), il n'y a rien de "caché sous le tapis" comme c'est le cas avec l'argument par défaut.

    On rappelle également que les arguments par défaut _mutables_, en python, sont ___à éviter comme la peste___. La meilleure façon de gérer cette approche amènerait donc au code suivant :

    ```python title=""
    def est_trie(tableau):
        for i in range(1, len(tableau)):
            if tableau[i - 1] > tableau[i]:
                return False
        return True

    def infixe(arbre, lst=None):
        if lst is None:
            lst = []
        if arbre is not None:
            sag, valeur, sad = arbre
            infixe(sag, lst)
            lst.append(valeur)  # Traitement infixe, entre les appels aux enfants
            infixe(sad, lst)
        return lst

    def verification_abr(arbre):
        return est_trie(infixe(arbre))
    ```
