Il est possible de réaliser une version itérative de la fonction :
```python
def maximum_abr(arbre):
    while arbre[2] != None:
        arbre = arbre[2]
    return arbre[1]
```