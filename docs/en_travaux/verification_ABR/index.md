---
author:
    - Pierre Marquestaut
    - Frédéric Zinelli
hide:
    - navigation
    - toc
title: Vérification d'un ABR (1)
tags:
    - en travaux
    - arbre binaire
    - ABR
difficulty: 200
---

!!! abstract "Arbre binaire"
    On considère un arbre binaire qui :

    * soit est l’arbre vide identifié par `None` ;
    * soit possède une valeur, un sous-arbre gauche `sag` et un sous-arbre droit `sad`, tous les trois regroupés dans un triplet `(sag, valeur, sad)`.

!!! example "Exemple"
    ```mermaid
    graph
        n1((1)) --- n0((0))
        n1 --- n2((2))
        n2 --- n4((" "))
        n2 --- n3((3))
        style n4 fill:none,stroke:none,color:none
    ```

    Ainsi, l’arbre binaire ci-dessus est créé par le code python suivant :
    ```python title=""
    a0 = (None, 0, None)
    a3 = (None, 3, None)
    a2 = (None, 2, a3)
    ab1 = (a0, 1, a2)
    ```

!!! abstract "Arbre binaire de recherche"
    On définit un arbre binaire de recherche comme étant un arbre binaire qui respecte la propriété suivante :
    pour chaque noeud de l'arbre,

    * les valeurs du sous-arbre gauche sont toutes inférieures à celle de la racine ;
    * les valeurs du sous-arbre droit sont toutes supérieures à celle de la racine ;
    * les valeurs contenues dans l'ABR sont uniques.

!!! example "Exemples"
    Ainsi l'arbre précédent est bien un ABR.

    En revanche l'arbre ci-dessous n'en est pas un :

    ```mermaid
    graph
    n1((1)) --- n0((0))
    n0 --- n6((" "))
    n0 --- n5((4))
    n1 --- n2((2))
    n2 --- n4((" "))
    n2 --- n3((3))
    ```

    Il est défini par le code suivant :
    ```python title=""
    a4 = (None, 4, None)
    a0 = (None, 0, a4)
    a3 = (None, 3, None)
    a2 = (None, 2, a3)
    ab2 = (a0, 1, a2)
    ```

Il est possible d'essayer de résoudre librement cet exercice ou de suivre l'une des deux méthodes proposées :

=== "Méthode libre"

    Écrire une fonction récursive `est_abr` qui prend en paramètres un arbre binaire `arbre`, et qui renvoie `True` si l'arbre est un arbre binaire de recherche et `False` sinon.

    !!! example "Exemple"
        ```pycon title=""
        >>> est_abr(abr1)
        True
        >>> est_abr(abr2)
        False
        ```
    {{ IDE('exo_0', STD_KEY="Yepikaïe1" ) }}

=== "Méthode avec les extremums"

    ??? question "fonction `#!py maximum_abr`"
        Écrire une fonction récursive `maximum_abr` qui prend en paramètre un arbre binaire `arbre` non vide, et qui renvoie sa valeur maximum.

        !!! warning "propriété"
            On considérera que l'arbre binaire passé en paramètre respecte les propriétés d'un ABR.

        !!! example "Exemple"
            ```pycon title=""
            >>> maximum_abr(ab1)
            3
            ```

        {{ IDE('exo_a') }}

    ??? question "fonction `#!py minimum_abr`"
        Écrire une fonction récursive `minimum_abr` qui prend en paramètre un arbre binaire `arbre` non vide, et qui renvoie sa valeur minimale.

        !!! warning "propriété"
            On considérera que l'arbre binaire passé en paramètre respecte les propriétés d'un ABR.

        !!! example "Exemple"
            ```pycon title=""
            >>> minimum_abr(ab1)
            0
            ```

        {{ IDE('exo_b') }}

    ??? question "fonction `#!py est_abr`"
        Écrire une fonction récursive `est_abr` qui prend en paramètres un arbre binaire `arbre`, et qui renvoie `True` si l'arbre est un arbre binaire de recherche et `False` sinon.

        ???+ tips "fonctions `#!py maximum_abr` et `#!py minimum_abr`"
            On pourra s'aider des fonctions `#!py maximum_abr` et `#!py minimum_abr` créées précédemment et déjà chargées en mémoire.

        !!! example "Exemple"
            ```pycon title=""
            >>> est_abr(abr1)
            True
            >>> est_abr(abr2)
            False
            ```

        {{ IDE('exo_c', STD_KEY="abcde") }}



=== "Méthode du tableau infixe"
    ??? question "fonction `#!py infixe`"
        Écrire une fonction récursive `infixe` qui prend en paramètre un arbre binaire `arbre` non vide, et qui renvoie un tableau contenant ses valeurs dans l'ordre infixe.

        !!! example "Exemple"
            ```pycon title=""
            >>> infixe(ab1)
            [0, 1, 2, 3]
            ```

        {{ IDE('exo_d') }}

    ??? question "fonction `#!py est_trie`"
        Écrire une fonction `est_trie` qui prend en paramètre un tableau de valeurs, et qui renvoie `True` si les valeurs sont triées dans l'ordre strictement croissant et `False` sinon.

        {{ remarque('tri_interdit')}}

        !!! example "Exemple"
            ```pycon title=""
            >>> est_trie([0, 1, 2, 3])
            True
            >>> est_trie([0, 4, 1, 2, 3])
            False
            ```

        {{ IDE('exo_e', SANS=".sort, sorted") }}

    ??? question "fonction `#!py est_abr`"
        Écrire une fonction récursive `est_abr` qui prend en paramètres un arbre binaire `arbre`, et qui renvoie `True` si l'arbre est un arbre binaire de recherche et `False` sinon.

        ???+ tips "fonctions `#!py infixe` et `#!py est_trie`"
            On pourra s'aider des fonctions `#!py infixe` et `#!py est_trie` créées précédemment et déjà chargées en mémoire.

        !!! example "Exemple"
            ```pycon title=""
            >>> est_abr(abr1)
            True
            >>> est_abr(abr2)
            False
            ```

        {{ IDE('exo_f', STD_KEY="Yepikaïe") }}

Si une erreur est levée durant les tests secrets, l'arbre ayant provoqué l'erreur est affiché ci-dessous :

{{ figure(admo_title="Dernier arbre testé (en cas d'erreur)") }}
