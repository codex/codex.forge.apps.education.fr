# --------- PYODIDE:env --------- #
def maximum_abr(arbre):
    ...
# --------- PYODIDE:code --------- #
def maximum_abr(arbre):
    ...
# --------- PYODIDE:corr --------- #
def maximum_abr(arbre):
    if arbre[2] is None:
        return arbre[1]
    else:
        return maximum_abr(arbre[2])

# --------- PYODIDE:tests --------- #
n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
ab1 = (n0, 1, n2)


assert maximum_abr(ab1) == 3
# --------- PYODIDE:secrets --------- #
ab3 = (None, 5, None)
assert maximum_abr(ab3) == 5

ab5 = (((None, 3, None), 4, None), 5, ((None, 6, None), 7, None))
assert maximum_abr(ab5) == 7