L'erreur vient du fait que le sous-bloc (à l'intérieur du if) doit être indenté, décalé. 

L'usage veut que l'on utilise soit quatre espaces soit une tabulation.