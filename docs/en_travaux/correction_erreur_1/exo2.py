# --------- PYODIDE:code --------- #
def plus_grand(a, b):
   
    if a > b:
        return a
    else:
    return b
            

# --------- PYODIDE:corr --------- #
def plus_grand(a, b):
   
    if a > b:
        return a
    else:
        return b
        
 

# --------- PYODIDE:tests --------- #
assert plus_grand(1, 2) == 2

# --------- PYODIDE:secrets --------- #
assert plus_grand(2, 2) == 2
assert plus_grand(8, 2) == 8
assert plus_grand(2, -2) == 2