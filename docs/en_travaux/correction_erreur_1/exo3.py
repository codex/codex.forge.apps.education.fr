

# --------- PYODIDE:code --------- #
def double_pair(tableau):
   
    for i in [0, 2, 4:
        tableau[i] = tableau[i]*2
            

# --------- PYODIDE:corr --------- #
def double_pair(tableau):
   
    for i in [0, 2, 4]:
        tableau[i] = tableau[i]*2
        
 

# --------- PYODIDE:tests --------- #
tableau_entiers = [5, 4, 6, 9, 8]
double(tableau_entiers) 

# --------- PYODIDE:secrets --------- #
tableau_entiers = [5, 4, 6, 9, 8]
double(tableau_entiers) 
assert tableau_entiers == [10, 4, 12, 9, 16]