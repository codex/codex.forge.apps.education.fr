---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Correction d'erreurs (1)
tags:
    - en travaux
difficulty: 150
---

Avant d'exécuter le code, l'interpréteur vérifie que sa syntaxe est conforme. Si la syntaxe est conforme, il exécute le code. Dans le cas contraire il **lève une exception**.

!!! abstract "Anatomie d'une exception"
    Commençons par générer un « bug » en exécutant le code ci-dessous :
    {{IDE('script')}}

    Commençons par la fin. On trouve sur la dernière ligne deux informations :

    * le type d'erreur (ici `SyntaxError`) ;
    * un message expliquant plus précisément l'erreur.

    Les lignes précédentes constituent le traceback. On y trouve :

    * le numéro de la ligne où se situe l'erreur : `File "<exec>", line 4`,
    * l'endroit de la ligne où se trouve l'erreur. (ici, on pointe le guillement qui n'a pas été refermé)



Corriger chaque programme pour qu'il puisse s'exécuter sans erreur.



??? question "Programme 1"

    {{IDE('exo1')}}

??? question "Programme 2"

    {{IDE('exo2')}}

??? question "Programme 3"

    {{IDE('exo3')}}