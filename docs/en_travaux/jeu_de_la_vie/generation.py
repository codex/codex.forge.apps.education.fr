# --- HDR, generation_ --- #
from random import random

MORT = 0
VIVANT = 1


def grille_aleatoire(taille, proba_vie):
    return [
        [VIVANT if random() < proba_vie else MORT for _ in range(taille)]
        for _ in range(taille)
    ]


def voisins_valides(grille, i, j):
    taille = len(grille)

    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    voisins = []
    for di, dj in directions:
        if 0 <= i + di < taille and 0 <= j + dj < taille:
            voisins.append((i + di, j + dj))
    return voisins


def voisins_vivants(grille, i, j):
    total = 0
    for i_voisin, j_voisin in voisins_valides(grille, i, j):
        total += grille[i_voisin][j_voisin]
    return total


def generation(grille):
    pass
# --- vide, generation_ --- #
MORT = 0
VIVANT = 1


def generation(grille):
    ...


# --- exo, generation_ --- #
""" # skip
MORT = 0
VIVANT = 1


def generation(grille):
    taille = len(...)
    nouvelle = grille_aleatoire(..., 0)

    for i in range(...):
        for j in range(...):
            if grille[i][j] == ... and ...:
                nouvelle[i][j] = ...
            elif grille[i][j] == ... and ...:
                nouvelle[i][j] = ...
    return nouvelle
"""  # skip


# --- corr, generation_ --- #
MORT = 0
VIVANT = 1


def generation(grille):
    taille = len(grille)
    nouvelle = grille_aleatoire(taille, 0)

    for i in range(taille):
        for j in range(taille):
            if grille[i][j] == MORT and voisins_vivants(grille, i, j) == 3:
                nouvelle[i][j] = VIVANT
            elif grille[i][j] == VIVANT and 2 <= voisins_vivants(grille, i, j) <= 3:
                nouvelle[i][j] = VIVANT
    return nouvelle


# --- tests, generation_ --- #
grille = [[1, 1, 0], [0, 1, 1], [0, 0, 1]]
assert generation(grille) == [[1, 1, 1], [1, 0, 1], [0, 1, 1]]
# --- secrets, generation_ --- #
from random import randrange

grille = [[0]]
assert generation(grille) == [[0]]


def _generation_(grille):
    taille = len(grille)
    nouvelle = grille_aleatoire(taille, 0)

    for i in range(taille):
        for j in range(taille):
            if grille[i][j] == MORT and voisins_vivants(grille, i, j) == 3:
                nouvelle[i][j] = VIVANT
            elif grille[i][j] == VIVANT and 2 <= voisins_vivants(grille, i, j) <= 3:
                nouvelle[i][j] = VIVANT
    return nouvelle


taille = randrange(3, 5)
grille = grille_aleatoire(taille, 0.4)
for i in range(taille):
    for j in range(taille):
        attendu = _generation_(grille)
        assert generation(grille) == attendu, f"Erreur pour la {grille = }"
