# --------- PYODIDE:env --------- #
from random import random

MORT = 0
VIVANT = 1


def grille_aleatoire(taille, proba_vie):
    return [
        [VIVANT if random() < proba_vie else MORT for _ in range(taille)]
        for _ in range(taille)
    ]


def egales(a, b):
    return sorted(a) == sorted(b)


def voisins_valides(grille, i, j):
    pass


def generation(grille):
    pass
# --------- PYODIDE:code --------- #
MORT = 0
VIVANT = 1

def voisins_valides(grille, i, j):
    taille = len(grille)

    directions = ((-1, -1), (-1, 0), ...)
    voisins = []
    for di, dj in directions:
        if ... <= i + di < ... and ...:
            voisins.append((..., ...))
    return ...


# --------- PYODIDE:corr --------- #
MORT = 0
VIVANT = 1


def voisins_valides(grille, i, j):
    taille = len(grille)

    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    voisins = []
    for di, dj in directions:
        if 0 <= i + di < taille and 0 <= j + dj < taille:
            voisins.append((i + di, j + dj))
    return voisins


# --------- PYODIDE:tests --------- #
grille = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
resultat = voisins_valides(grille, 0, 0)
attendu = [(0, 1), (1, 0), (1, 1)]
assert egales(resultat, attendu) is True
resultat = voisins_valides(grille, 0, 1)
attendu = [(0, 0), (0, 2), (1, 0), (1, 1), (1, 2)]
assert egales(resultat, attendu) is True
resultat = voisins_valides(grille, 1, 1)
attendu = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 2)]
assert egales(resultat, attendu) is True
# --------- PYODIDE:secrets --------- #
from random import randrange

grille = [[0]]
assert voisins_valides(grille, 0, 0) == []


def _voisins_valides_(grille, i, j):
    n = len(grille)

    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    voisins = []
    for di, dj in directions:
        if 0 <= i + di < n and 0 <= j + dj < n:
            voisins.append((i + di, j + dj))
    return voisins


taille = randrange(3, 5)
grille = [[0 for _ in range(taille)] for _ in range(taille)]
for i in range(taille):
    for j in range(taille):
        resultat = voisins_valides(grille, i, j)
        attendu = _voisins_valides_(grille, i, j)
        assert egales(
            resultat, attendu
        ), f"Erreur dans une grille de dimension {taille = } pour la cellule de coordonnées {i, j = }"
