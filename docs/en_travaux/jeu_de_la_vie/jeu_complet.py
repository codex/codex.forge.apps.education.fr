# --- PYODIDE:env --- #
from random import random
import asyncio
import js

MORT = 0
VIVANT = 1


appel = False
grille_appel = None
nb_generations_appel = None
TAILLE = 10

def voisins(i, j):
    return [
        (
            (i + di) if 0 <= i + di < TAILLE else TAILLE - 1 if i + di == -1 else 0,
            (j + dj) if 0 <= j + dj < TAILLE else TAILLE - 1 if j + dj == -1 else 0,
        )
        for di, dj in (
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        )
    ]


def grille_aleatoire(taille, proba_vie):
    return [
        [VIVANT if random() < proba_vie else MORT for _ in range(taille)]
        for _ in range(taille)
    ]


def grille_en_dict(grille):
    dico = {}
    for i in range(TAILLE):
        for j in range(TAILLE):
            if grille[i][j]:
                dico[i, j] = dico.get((i, j), 0) + 1
                for ii, jj in voisins(i, j):
                    dico[ii, jj] = dico.get((ii, jj), 0) + 2
    return dico


def generation(grille):
    nouveau = dict()
    nes_x = []
    nes_y = []
    morts_x = []
    morts_y = []
    for i, j in grille:
        vivant = grille[i, j] & 1
        nb_voisins = grille[i, j] // 2
        if (not vivant) and nb_voisins == 3:
            nouveau[i, j] = nouveau.get((i, j), 0) + 1
            nes_x.append(j)
            nes_y.append(i)
            for ii, jj in voisins(i, j):
                nouveau[ii, jj] = nouveau.get((ii, jj), 0) + 2
        elif vivant:
            if 2 <= nb_voisins <= 3:
                nouveau[i, j] = nouveau.get((i, j), 0) + 1
                for ii, jj in voisins(i, j):
                    nouveau[ii, jj] = nouveau.get((ii, jj), 0) + 2
            else:
                morts_x.append(j)
                morts_y.append(i)

    return nouveau, nes_x, nes_y, morts_x, morts_y


def visualisation(grille, nb_generations):
    global appel, grille_appel, nb_generations_appel, TAILLE
    TAILLE = len(grille)
    appel = True
    grille_appel = grille_en_dict(grille)
    nb_generations_appel = nb_generations


# --- PYODIDE:code --- #
# Réglages de l'animation
TAILLE_CELLULE = 5
DELAI_SECONDES = 0.2
COULEUR_VIVANTE = "#7e56c2"

# Réglages du jeu
TAILLE = 100
grille = grille_aleatoire(TAILLE, 0.1)
nb_generations = 50

# lancement du jeu
visualisation(grille, nb_generations)


# --- PYODIDE:post --- #
async def jeu_de_la_vie_post(grille, nb_generations):
    # Dessin initial
    nes_x = [j for i, j in grille if grille[i, j] & 1]
    nes_y = [i for i, j in grille if grille[i, j] & 1]
    js.dessine(nes_x, nes_y, [], [])
    await asyncio.sleep(DELAI_SECONDES)

    # Dessins des générations
    for _ in range(nb_generations):
        grille, nes_x, nes_y, morts_x, morts_y = generation(grille)
        js.dessine(
            nes_x,
            nes_y,
            morts_x,
            morts_y,
        )
        await asyncio.sleep(DELAI_SECONDES)


if appel:
    js.initialisationJDV(TAILLE, TAILLE_CELLULE, COULEUR_VIVANTE)
    await jeu_de_la_vie_post(grille_appel, nb_generations_appel)
