Il est désormais possible de calculer différentes générations successives du jeu de la vie.

On propose pour cela la fonction `jeu_de_la_vie` ci-dessous :

```python
def jeu_de_la_vie(grille, nb_generations):
    for _ in range(nb_generations):
        grille = generation(grille)
```