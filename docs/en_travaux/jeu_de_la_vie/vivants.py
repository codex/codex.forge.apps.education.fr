# --- HDR, vivants_ --- #
from random import random

MORT = 0
VIVANT = 1


def grille_aleatoire(taille, proba_vie):
    return [
        [VIVANT if random() < proba_vie else MORT for _ in range(taille)]
        for _ in range(taille)
    ]


def voisins_valides(grille, i, j):
    taille = len(grille)

    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    voisins = []
    for di, dj in directions:
        if 0 <= i + di < taille and 0 <= j + dj < taille:
            voisins.append((i + di, j + dj))
    return voisins


def voisins_vivants(grille, i, j):
    pass


def generation(grille):
    pass
# --- vide, vivants_ --- #
MORT = 0
VIVANT = 1


def voisins_vivants(grille, i, j):
    ...


# --- exo, vivants_ --- #
""" # skip
MORT = 0
VIVANT = 1

def voisins_vivants(grille, i, j):
    total = ...
    for i_voisin, j_voisin in voisins_valides(grille, i, j):
        total = ...
    return ...
"""  # skip


# --- corr, vivants_ --- #
MORT = 0
VIVANT = 1


def voisins_vivants(grille, i, j):
    total = 0
    for i_voisin, j_voisin in voisins_valides(grille, i, j):
        total += grille[i_voisin][j_voisin]
    return total


# --- tests, vivants_ --- #
grille = [[1, 1, 0], [0, 1, 1], [0, 0, 1]]
assert voisins_vivants(grille, 0, 0) == 2
assert voisins_vivants(grille, 1, 1) == 4
assert voisins_vivants(grille, 2, 1) == 3
# --- secrets, vivants_ --- #
from random import randrange

grille = [[0]]
assert voisins_vivants(grille, 0, 0) == 0


def _voisins_vivants_(grille, i, j):
    total = 0
    for i_voisin, j_voisin in voisins_valides(grille, i, j):
        total += grille[i_voisin][j_voisin]
    return total


taille = randrange(3, 5)
grille = grille_aleatoire(taille, 0.4)
for i in range(taille):
    for j in range(taille):
        attendu = _voisins_vivants_(grille, i, j)
        assert (
            voisins_vivants(grille, i, j) == attendu
        ), f"Erreur pour la {grille = } et la cellule de coordonnées {i, j = }"
