---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Le jeu de la vie
tags:
    - en travaux
    - grille
difficulty: 210
maj: 26/06/2024
---

Le jeu de la vie est un « [automate cellulaire](https://fr.wikipedia.org/wiki/Automate_cellulaire) » inventé en 1970 par [John Horton Conway](https://fr.wikipedia.org/wiki/John_Horton_Conway).

Une « partie » se déroule sur une grille, théoriquement infinie, divisée en cellules carrées. Chaque cellule est soit « vivante », soit « morte ».

On appelle *voisin* d'une cellule, une cellule qui partage au moins un « coin » avec celle-ci. On a représenté ci-dessous une cellule (coloriée) et ses huit voisins (pointés).

![Voisins d'une celllule](voisins.svg){width=15% .center .autolight}

L'état actuel d'une grille représente une « *génération* ». Afin de calculer la génération suivante, on parcourt toutes les cellules de la grille actuelle et, pour chacune, on applique l'une des règles suivantes :

* si elle est **morte** et possède **exactement trois voisines vivantes** alors elle sera vivante à la prochaine génération. Si elle ne possède pas trois voisines vivantes, elle reste morte ;

* si elle est **vivante** et possède **deux ou trois voisines vivantes** alors elle restera vivante à la prochaine génération. Dans le cas contraire, elle meurt.

Malgré leur simplicité, ces règles permettent d'engendrer des comportements complexes[^1].

[^1]: il a ainsi été montré que le jeu de la vie est [*Turing-complet*](https://fr.wikipedia.org/wiki/Turing-complet).

On se propose dans cet exercice de mettre en œuvre le jeu de la vie avec Python. Pour cela, plusieurs questions sont proposées. Ces questions sont **indépendantes** et peuvent être traitées dans l'ordre souhaité. La dernière question regroupe l'ensemble du code et permet de visualiser l'évolution d'une grille.

La grille de jeu sera représentée par une liste de listes contenant les valeurs `#!py 0` (cellule morte) ou `#!py 1` (cellule vivante). On garantit que les grilles sont toutes des carrés de largeur et hauteur égales à un entier $n$ strictement positif.

Dans toute la suite, les variables `i` désigneront des indices de lignes et `j` des indices de colonnes.

On fournit une fonction `grille_aleatoire` qui prend pour paramètres la dimension `taille` d'une grille (`#!py taille` est un entier strictement positif indiquant le nombre de lignes et de colonnes de la grille) et un nombre flottant `proba_vie` (compris entre `#!py 0.0` et `#!py 1.0`). Cette fonction renvoie une grille de la dimension souhaitée dans laquelle chaque cellule à une probabilité `#!py proba_vie` d'être vivante.

Ainsi :

* `#!py grille_aleatoire(7, 0.3)` renvoie une grille de $7\times 7$ dans laquelle chaque cellule a une probabilité de $p=0,3$ d'être vivante ;

* `#!py grille_aleatoire(10, 0.0)` renvoie une grille de $10\times 10$ entièrement remplie de cellules mortes.

Cette fonction est d'ores et déjà accessible dans tous les éditeurs. Il est inutile de l'importer.

??? note "Code de `grille_aleatoire`"

    On fournit le code de la fonction `grille_aleatoire` pour information.

    ```python
    from random import random
    
    MORT = 0
    VIVANT = 1

    def grille_aleatoire(taille, proba_vie):
        return [
            [VIVANT if random() < proba_vie else MORT for _ in range(taille)]
            for _ in range(taille)
        ]
    ```

??? tip "Visualisation"

    On propose dans un premier temps de visualiser le fonctionnement du jeu de la vie en observant l'évolution d'une grille lors de plusieurs générations.
    
    On fournit donc une fonction `visualisation` qui prend en paramètres une liste de listes `grille` représentant l'état initial de la grille et un entier `nb_generations` qui indique le nombre de générations à simuler.
    
    Cette fonction calcule l'état de la grille au fil du nombre de générations indiquées **et dessine la grille correspondante sous l'éditeur**. À ce titre certains réglages sont modifiables par le biais des variables `TAILLE_CELLULE` (taille d'une cellule en pixels), `DELAI_SECONDES` (délai entre les affichages de deux grilles) et `COULEUR_VIVANTE` (couleur d'une cellule vivante).
    
    Cette section ne comporte pas de tests mais seulement une représentation visuelle. Vous pouvez l'utiliser afin de tester des grilles aléatoires ou des motifs particuliers.
    
    ???+ warning "Restons raisonnables"
    
        Ne perdez pas de vue que nous travaillons dans le navigateur et que l'exécution de code Python n'est pas immédiate.
        
        Dessiner des grilles de grande taille ou demander un grand nombre de générations risque dans certains cas de ralentir le fonctionnement du navigateur.
        
    {{ IDE('jeu_complet') }}

    
    <div class="admonition" style="margin:auto;padding:5px;width:100%">
    <div id="texteJDV" style="width: 100%;text-align: center;">La grille sera dessinée ici</div>
    <canvas id="canvasJDV" width="0" height="0" class="center autolight"></canvas>
    </div>
    
??? question "Voisins valides"

    Une grille étant donnée, on souhaite calculer la grille de la génération suivante. Pour cela, il faut, pour chaque cellule, compter son nombre de voisins vivants.
    
    Les cellules du centre de la grille ont bien huit voisins. Celles des « bords » n'en possèdent toutefois pas huit. Une cellule située dans un « coin » de la grille ne possède par exemple que trois voisins.
    
    On demande dans un premier temps d'écrire une fonction `voisins_valides` qui prend en paramètres une liste de listes `grille` représentant une grille ainsi que deux coordonnées valides `i` et `j` et renvoie la liste des coordonnées des voisins valides de la cellule de coordonnées `#!py (i, j)`.
    
    ??? note "Égalité de listes"
    
        Les tests de cet exercice utilisent une fonction `egales` qui renvoie `#!py True` si les deux listes passées en paramètres contiennent exactement les mêmes éléments **sans tenir compte de leur ordre d'apparition dans chacune**.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> grille = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        >>> voisins_valides(grille, 0, 0)
        [(0, 1), (1, 0), (1, 1)]
        >>> voisins_valides(grille, 0, 1)
        [(0, 0), (0, 2), (1, 0), (1, 1), (1, 2)]
        >>> voisins_valides(grille, 1, 1)
        [(0, 0), (0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 2)]
        ```
    
    === "Version vide"
        {{ IDE('exo_voisins_vide') }}
    === "Version à compléter"
        {{ IDE('exo_voisins_trous') }}

??? question "Voisins vivants"

    Une version fonctionnelle de la fonction `voisins_valides` de la question précédente est disponible dans cette question. **Il est inutile de l'importer**.

    Cette fonction étant connue, il faut désormais parcourir l'ensemble des voisins valides d'une cellule afin de connaître le nombre de voisins vivants parmi ceux-ci.

    On demande d'écrire une fonction `voisins_vivants` qui prend en paramètres une liste de listes `grille` représentant une grille ainsi que deux coordonnées valides `i` et `j` et renvoie le nombre de voisins vivants de la cellule de coordonnées `#!py (i, j)`.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> grille = [[1, 1, 0], [0, 1, 1], [0, 0, 1]]
        >>> voisins_vivants(grille, 0, 0)
        2
        >>> voisins_vivants(grille, 1, 1)
        4
        >>> voisins_vivants(grille, 2, 1)
        3
        ```
    
    === "Version vide"
        {{ IDE('exo_vivants_vide') }}
    === "Version à compléter"
        {{ IDE('exo_vivants_trous') }}

??? question "Génération suivante"

    Des versions fonctionnelles des fonctions `voisins_valides` et `voisins_vivants` des questions précédentes sont disponibles dans cette question. **Il est inutile de les importer**.

    Ces fonctions étant connues, on peut désormais, connaissant une grille, calculer la grille à la génération suivante. Pour ce faire :
    
    * on crée une nouvelle grille ne contenant que des cellules mortes,
    
    * on parcourt toutes les cellules de la grille actuelle et, pour chacune, on met à jour l'état de la cellule correspondante de la génération suivante en appliquant les règles du jeu de la vie.

    On rappelle les règles du jeu :
    
    * si la cellule actuelle est **morte** et possède **exactement trois voisines vivantes** alors elle sera vivante à la prochaine génération. Si elle ne possède pas trois voisines vivantes, elle reste morte ;

    * si la cellule actuelle est **vivante** et possède **deux ou trois voisines vivantes** alors elle restera vivante à la prochaine génération. Dans le cas contraire, elle meurt.

    On demande d'écrire une fonction `generation` qui prend en paramètres une liste de listes `grille` représentant une grille et renvoie une nouvelle grille représentant la génération suivante.
    
    ???+ example "Exemple"
    
        ```pycon title=""
        >>> grille = [[1, 1, 0], [0, 1, 1], [0, 0, 1]]
        >>> generation(grille)
        [[1, 1, 1], [1, 0, 1], [0, 1, 1]]
        ```
    
    === "Version vide"
        {{ IDE('exo_generation_vide') }}
    === "Version à compléter"
        {{ IDE('exo_generation_trous') }}

<script>
const canvasJDV = document.getElementById("canvasJDV");
const ctxJDV = canvasJDV.getContext("2d");
ctxJDV.globalCompositeOperation = 'difference';
var taille_cellule = 0;
var couleur_vivant = "#7e56c2";
var couleur_fond = "#eee";
var couleur_texte = "#00f";
var gen = 0;
var hauteur_texte = 20;
    
function initialisationJDV(taille, taille_cell, couleur_viv) {
    let div_texte = document.getElementById("texteJDV")
    if (typeof(div_texte) != 'undefined' && div_texte != null) {
        div_texte.remove();
    }
    taille_cellule = taille_cell;
    canvasJDV.width = taille_cellule * taille;
    canvasJDV.height = taille_cellule * taille + hauteur_texte;
    ctxJDV.font = "20px Consolas";
    ctxJDV.fillStyle = couleur_fond;
    ctxJDV.fillRect(0, 0, canvasJDV.width, canvasJDV.height);
    couleur_vivant = couleur_viv;
    gen = 0;
}

function dessine(nes_x, nes_y, morts_x, morts_y) {
    ctxJDV.fillStyle = couleur_vivant;
    for (var i = 0; i < nes_x.length; i++) {
        ctxJDV.fillRect(nes_x[i] * taille_cellule, nes_y[i] * taille_cellule, taille_cellule-1, taille_cellule-1);
    };
    
    ctxJDV.fillStyle = couleur_fond;
    for (var i = 0; i < morts_x.length; i++) {
        ctxJDV.fillRect(morts_x[i] * taille_cellule, morts_y[i] * taille_cellule, taille_cellule-1, taille_cellule-1);
    };
    ctxJDV.fillStyle = couleur_fond;
    ctxJDV.fillRect(0, canvasJDV.height - hauteur_texte, canvasJDV.width, hauteur_texte)
    ctxJDV.fillStyle = couleur_texte;
    ctxJDV.fillText(gen, 0, canvasJDV.height);
    gen = gen + 1;
}
</script>