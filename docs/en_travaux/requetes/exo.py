

# --------- PYODIDE:env --------- #

# Début du code qui devrait être caché
from js import fetch, alert

url_fichier = "zoo_traduction.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

nom_fichier = "zoo.csv"
with open(nom_fichier, "w") as fichier:
    fichier.write(contenu)

animaux = []

with open("zoo.csv", "r") as fichier:
    fichier.readline()
    for ligne in fichier:
        valeurs = ligne.strip().split(",")
        for i in set(range(1, len(valeurs))) - {len(valeurs) - 4}:
            valeurs[i] = bool(int(valeurs[i]))
        valeurs[-4] = int(valeurs[-4])
        animaux.append(valeurs)
# --------- PYODIDE:code --------- #

# Quels sont les noms des animaux volants ?
requete_1 = [animal[0] for animal in animaux if animal[5]]

# Combien y-a-t'il d'animaux aquatiques sans pattes ?
requete_2 = len([animal[0] for animal in animaux if animal[6] and animal[13] == 0])

# Quels sont les noms des animaux aquatiques sans pattes ?
requete_3 = ...

# Quels sont les noms des prédateurs ?
requete_4 = ...

# Combien y-a-t'il d'animaux sans plumes ?
requete_5 = ...

# Quels sont les noms des animaux ayant plus de 5 pattes (exclu) ?
requete_6 = ...

# Combien y-a-t'il de prédateurs volants ?
requete_7 = ...

# Chercher toutes les informations de la cigale (chaîne "cigale")
requete_8 = ...

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et ne pondant des oeufs ?
requete_9 = ...

# Chercher le nombre de pattes d'un sirenidae ? (extraire le résultat de la liste)
requete_10 = ...

# --------- PYODIDE:corr --------- #

# Tests
# Quels sont les noms des animaux volants ?
requete_1 = [animal[0] for animal in animaux if animal[5]]

# Combien y-a-t'il d'animaux aquatiques sans pattes ?
requete_2 = len([animal[0] for animal in animaux if animal[6] and animal[13] == 0])

# Quels sont les noms des animaux aquatiques sans pattes ?
requete_3 = [animal[0] for animal in animaux if animal[6] and animal[13] == 0 ]

# Quels sont les noms des prédateurs ?
requete_4 = [animal[0] for animal in animaux if animal[7] ]

# Combien y-a-t'il d'animaux sans plumes ?
requete_5 = len([animal[0] for animal in animaux if not animal[2]])

# Quels sont les noms des animaux ayant plus de 5 pattes (exclu) ?
requete_6 = [animal[0] for animal in animaux if animal[13] >5]

# Combien y-a-t'il de prédateurs volants ?
requete_7 = len([animal[0] for animal in animaux if animal[5] and animal[7]])

# Chercher toutes les informations de la cigale (chaîne `"cigale"`)
requete_8 = [animal for animal in animaux if animal[0]=="cigale"]

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et ne pondant des oeufs ?
requete_9 = len([animal[0] for animal in animaux if animal[15] and animal[16] and not animal[2]])

# Chercher le nombre de pattes d'un sirenidae ? (extraire le résultat de la liste)
requete_10 = [animal[13] for animal in animaux if animal[0]=="sirenidae"]


# --------- PYODIDE:secrets --------- #

ok = True
# Quels sont les noms des animaux aquatiques sans pattes ?
attendu_3 = [animal[0] for animal in animaux if animal[6] and animal[13] == 0 ]

# Quels sont les noms des prédateurs ?
attendu_4 = [animal[0] for animal in animaux if animal[7] ]

# Combien y-a-t'il d'animaux sans plumes ?
attendu_5 = len([animal[0] for animal in animaux if not animal[2]])

# Quels sont les noms des animaux ayant plus de 5 pattes (exclu) ?
attendu_6 = [animal[0] for animal in animaux if animal[13] >5]

# Combien y-a-t'il de prédateurs volants ?
attendu_7 = len([animal[0] for animal in animaux if animal[5] and animal[7]])

# Chercher toutes les informations de la cigale (chaîne `'cigale`) 
attendu_8 = [animal for animal in animaux if animal[0] == "cigale"]

# Combien y-a-t'il d'animaux domestiques, de la taille d'un chat et ne pondant des oeufs ? 
attendu_9 = len([animal for animal in animaux if animal[15] and animal[16] and animal[3]])

# Chercher le nombre de pattes du papillon ? (extraire le résultat de la liste) 
attendu_10 =  [animal[13] for animal in animaux if animal[0] == "sirenidae"]


requetes = [requete_3, requete_4, requete_5, requete_6, requete_7, requete_8, requete_9, requete_10]
attendus = [attendu_3, attendu_4, attendu_5, attendu_6, attendu_7, attendu_8, attendu_9, attendu_10]

for i in range(len(requetes)):
    n = i+3
    terminal_message("bk4fg1", f"Requête {n} : " , new_line=False)
    if requetes[i] == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif requetes[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"
