---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Traitement des données issues de l'ISS
tags:
    - brouillon
    - en travaux
    - grille

difficulty: 150
maj: 31/10/2024
---
# 

Un Micro-contrôleur situé à bord du module européen Columbus de la station spatiale internationale permet d'effectuer des relevés de températures, d'accélérations, etc...

![le module Columbus](https://upload.wikimedia.org/wikipedia/commons/1/14/S122e008264.jpg){ .center }


On dispose des données relevées pendant deux jours, regroupées dans un fichier CSV dont voici la struture :



|   Attributs   |           Description           | Type Python | Indice |
| :-----------: | :-----------------------------: | :---------: | :----: |
|  `Date/Time`  |             Date et heure       | `str`       |   `0`  |
|    `Latitude` |           Latitude              | `float`     |   `1`  |
| `Longitude`   |      Longitude                  | `float`     |   `2`  |
| `Temperature` |       Température               | `float`     |   `3`  |
|   `Humidity`  |    Humidité                     | `float`     |   `4`  |
|   `Pressure`  |         Pression                | `float`     |   `5`  |
|  `Compass`    |       Direction du Nord         | `float`     |   `6`  |
|  `MagX`       |Intensité magnétique sur l'axe X | `float`     |   `7`  |
|    `MagY`     |Intensité magnétique sur l'axe Y | `float`     |   `8`  |
|  `MagZ`       |Intensité magnétique sur l'axe Z | `float`     |   `9`  |
|     `Pitch`   |    Tangage                      | `int`       |   `10` |
|  `Roll`       |        Roulis                   | `int`       |   `11` |
|  `Yaw`        |     Lacet                       | `int`       |   `12` |
|   `AccelX`    |   Accélération sur l'axe X      | `float`     |   `13` |
|    `AccelY`   |       Accélération sur l'axe Y  | `float`     |   `14` |
| `AccelZ`      |  Accélération sur l'axe Z       | `float`     |   `15` |
| `R`           | Composante rouge de la lumière  | `int`       |   `16` |
| `G`           | Composante verte de la lumière  | `int`       |   `17` |
| `B`           | Composante bleue de la lumière  | `int`       |   `18` |
| `C`           | Intensité de la lumière         | `int`       |   `19` |
| `Motion`      | Détection d'un mouvement        | `bool`      |   `20` |
| `reset`       | Redémarrage                     | `int`       |   `21` |

Attention : les données sont présentes dans le fichier CSV sous forme de chaîne de caractères. Avant de les traiter, il faudra impérativement les convertir en entier ou flottant.

Ces données ont été importées dans une liste Python nommée `enregistrements`. 


??? question "Requêtes"
    Compléter le code ci-dessous en effectuant les requêtes proposées.

    {{ IDE('exo_a',STD_KEY="bk4fg1") }}  

??? question "fonction `#!py maximale_temperatures`"
    Écrire une fonction `maximale_temperatures` qui prend en paramètres un `tableau` de nombres flottants non vide, et qui renvoie sa valeur maximale.

    Le tableau `temperatures` provient d'une requête réalisée à la question précédente, alors que le `temperatures_50` correspond au 50 premières valeurs.

    !!! example "Exemple"
        ```pycon title=""
        >>> maximale_temperatures(temperatures)
        35.0
        ```
    {{ remarque("proximite_flottants")}}

    {{ IDE('exo_b') }}

??? question "fonction `#!py moyenne_pressions`"
    Écrire une fonction `moyenne_pressions` qui prend en paramètres un `tableau` de nombres flottants non vide, et qui renvoie sa valeur moyenne, arrondie au deuxième chiffre après la virgule.

    Le tableau `pressions` provient d'une requête réalisée à la question précédente.

    !!! example "Exemple"
        ```pycon title=""
        >>> moyenne_pressions(pressions)
        1015.05
        ```
    {{ remarque("round")}}

    {{ IDE('exo_c') }}

??? question "fonction `#!py dernier_redemarrage`"
    La colonne *reset* indique une réinitialisation du processeur du micro-contrôleur.

    La valeur est à 1000 lors de la fin de la réinitialisation.

    Écrire une fonction `dernier_redemarrage` qui prend en paramètres un `tableau` de nombres flottants non vide, et qui renvoie sa valeur minimale.

    Le tableau `pressions` provient d'une requête réalisée à la question précédente.

    !!! example "Exemple"
        ```pycon title=""
        >>> dernier_redemarrage(reste)
        0
        ```

    {{ IDE('exo_d') }}