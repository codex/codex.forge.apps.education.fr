

# --------- PYODIDE:env --------- #

# Début du code qui devrait être caché
from js import fetch, alert

url_fichier = "AstroPI.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

nom_fichier = "pi.csv"
with open(nom_fichier, "w") as fichier:
    fichier.write(contenu)


enregistrements = []

with open("pi.csv", "r") as fichier:
    fichier.readline()
    for ligne in fichier:
        valeurs = ligne.strip().split(",")
        enregistrements.append(valeurs)
# --------- PYODIDE:code --------- #

# Déterminer les valeurs de température sur les deux jours
temperatures = ...

# Déterminer les valeurs de pression sur les deux jours
pressions = ...

# Déterminer la valeur du reset et la date à laquelle il se produit
reset = ...

# --------- PYODIDE:corr --------- #

# Déterminer les valeurs de température sur les deux jours
temperatures = [float(enregistrements[k][3]) for k in range(len(enregistrements))]

# Déterminer les valeurs de pression sur les deux jours
pressions = [float(enregistrements[k][5]) for k in range(len(enregistrements))]

# Déterminer la valeur du reset et la date à laquelle il se produit
reset = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(len(enregistrements))]


# --------- PYODIDE:secrets --------- #

ok = True
# Quels sont les noms des animaux aquatiques sans pattes ?
attendu_1 = [float(enregistrements[k][3]) for k in range(len(enregistrements))]

# Quels sont les noms des prédateurs ?
attendu_2 = [float(enregistrements[k][5]) for k in range(len(enregistrements))]

# Combien y-a-t'il d'animaux sans plumes ?
attendu_3 = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(len(enregistrements))]


requetes = [temperatures, pressions, reset]
attendus = [attendu_1, attendu_2, attendu_3]

for i in range(len(requetes)):
    n = i
    terminal_message("bk4fg1", f"Requête {n} : " , new_line=False)
    if requetes[i] == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif requetes[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"


