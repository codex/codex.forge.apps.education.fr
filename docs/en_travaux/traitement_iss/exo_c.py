# --------- PYODIDE:env --------- #

# Début du code qui devrait être caché
from js import fetch, alert

url_fichier = "AstroPI.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

nom_fichier = "pi.csv"
with open(nom_fichier, "w") as fichier:
    fichier.write(contenu)

enregistrements = []
with open("pi.csv", "r") as fichier:
    fichier.readline()
    for ligne in fichier:
        valeurs = ligne.strip().split(",")
        enregistrements.append(valeurs)


pressions = [float(enregistrements[k][5]) for k in range(len(enregistrements))]
pressions_50 = [float(enregistrements[k][5]) for k in range(50)]
pressions_1000 = [float(enregistrements[k][5]) for k in range(1000)]

# --------- PYODIDE:code --------- #

def moyenne_pressions(tableau):
    ...

# --------- PYODIDE:tests --------- #

assert abs(moyenne_pressions(pressions_50) - 1015.05) < 1e-6
assert abs(moyenne_pressions(pressions) - 1013.99) < 1e-6

# --------- PYODIDE:corr --------- #

def moyenne_pressions(tableau):
    
    somme = 0
    for valeur in tableau:
        somme += valeur
    return round(somme / len(tableau), 2)


# --------- PYODIDE:secrets --------- #
assert abs(moyenne_pressions(pressions_1000) - 1015.03) < 1e-6

