# --------- PYODIDE:env --------- #

# Début du code qui devrait être caché
from js import fetch, alert

url_fichier = "AstroPI.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

nom_fichier = "pi.csv"
with open(nom_fichier, "w") as fichier:
    fichier.write(contenu)
    
enregistrements = []
with open("pi.csv", "r") as fichier:
    fichier.readline()
    for ligne in fichier:
        valeurs = ligne.strip().split(",")
        valeurs[0] = valeurs[0].split('"')[1]
        enregistrements.append(valeurs)


reset = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(len(enregistrements))]
reset_50 = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(50)]
reset_1000 = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(1000)]
# --------- PYODIDE:code --------- #

def dernier_redemarrage(tableau):
    ...

# --------- PYODIDE:tests --------- #

assert dernier_redemarrage(reset) == '2022-02-01 07:05:17.485777'
assert dernier_redemarrage(reset_50) == '2022-01-31 12:12:03.024742'
# --------- PYODIDE:corr --------- #

def dernier_redemarrage(tableau):
    
    date = None
    for enregistrement in tableau:
        if enregistrement[1] == 1000:
            date = enregistrement[0] 
    return date


# --------- PYODIDE:secrets --------- #
assert dernier_redemarrage(reset_1000) == '2022-01-31 12:12:03.024742'
reset_50 = [(enregistrements[k][0],int(enregistrements[k][21])) for k in range(1, 50)]
assert dernier_redemarrage(reset_50) == None

