# --------- PYODIDE:env --------- #

# Début du code qui devrait être caché
from js import fetch, alert

url_fichier = "AstroPI.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

nom_fichier = "pi.csv"
with open(nom_fichier, "w") as fichier:
    fichier.write(contenu)


enregistrements = []
with open("pi.csv", "r") as fichier:
    fichier.readline()
    for ligne in fichier:
        valeurs = ligne.strip().split(",")
        enregistrements.append(valeurs)


temperatures = [float(enregistrements[k][3]) for k in range(len(enregistrements))]
temperatures_50 = [float(enregistrements[k][3]) for k in range(50)]
temperatures_1000 = [float(enregistrements[k][3]) for k in range(1000)]
# --------- PYODIDE:code --------- #

def maximum_temperature(tableau):
    ...

# --------- PYODIDE:tests --------- #

assert abs(maximum_temperature(temperatures_50) - 32.9) < 1e-6
assert abs(maximum_temperature(temperatures) - 35.0) < 1e-6
# --------- PYODIDE:corr --------- #

def maximum_temperature(tableau):
    
    valeur_max =tableau[0]
    for i in range(1,len(tableau)):
        if tableau[i] > valeur_max:
            valeur_max = tableau[i]
    return valeur_max


# --------- PYODIDE:secrets --------- #
tableau = [12, 10, 13, 14, 18, 17]
assert maximum_temperature(tableau) == 18
tableau = [-5, -10, -2, -7, -18, -1]
assert maximum_temperature(tableau) == -1

assert abs(maximum_temperature(temperatures_1000) - 33.6) < 1e-6

