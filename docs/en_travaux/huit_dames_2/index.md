---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Problème des huit dames (2)
difficulty: 280
tags:
    - à trous
    - en travaux
    - booléen
---

Aux échecs, la dame est capable de se déplacer dans toutes les directions.

<center>
![Mouvement des dames](mvt_dame.svg){width="25%" }
![Disposition valide](valide_1.svg){width="25%" }
</center>

Le problème des huit dames consiste à placer $8$ dames sur un échiquier classique ($8$ lignes et $8$ colonnes) de façon à ce qu'aucune d'entre elles ne soit menacée par une autre. Ainsi il n'y a qu'une seule dame par ligne, colonne ou diagonale. La figure ci-dessus (à droite) illustre une disposition valide.

On considère dans cet exercice des « échiquiers » de taille $n \ge 1$ variable. On garantit que l'échiquier est bien carré. Selon la taille de l'échiquier, on pourra placer plus ou moins de dames.

Le problème considéré dans cet exercice consiste donc à placer exactement $n$ dames dans un échiquier de $n \times n$ cases de façon à ce qu'aucune d'entre elles ne soit menacée par une autre.

Puisqu'une disposition des dames valides doit comporter exactement une dame par colonne on peut se contenter, pour chaque colonne, de ne garder trace que de la ligne sur laquelle se trouve l'unique dame de cette colonne.

Cette représentation du problème prend la forme d'une liste de `#!py n` éléments dans laquelle l'élément d'indice `#!py j` est égal à l'indice `#!py i` de la ligne sur laquelle se trouve la dame.

??? example "Exemples de listes"

    La disposition valide ci-dessus sera par exemple représentée en machine par :

    ```python
    # colonne  0  1  2  3  4  5  6  7
    valide =  [6, 4, 2, 0, 5, 7, 1, 3]
    ```

    On peut lire que la dame de la première colonne (indice `#!py 0`) est située sur la $7$-ème ligne (indice `#!py 6`).

    La liste ci-dessous représente une disposition dans un échiquier de taille $n=3$ : 
        
    ```python
    # colonne   0  1  2
    invalide = [1, 2, 0]
    ```

    Cette seconde disposition est invalide car deux dames sont sur une même diagonale (aux colonnes d'indices `#!py 0` et `#!py 1`).

Vous devez écrire la fonction `disposition_valide` qui prend en argument une liste `disposition` contenant `n` valeurs et renvoie le booléen répondant à la question « *la disposition proposée satisfait-elles le problème des `n` dames ?* ».

On garantit que les valeurs présentes dans la liste `disposition` sont des numéros de lignes valides (compris entre `#!py 0` et `#!py n - 1` inclus l'un et l'autre).  

??? tip "Aide (1)"

    Pour vérifier qu'il n'y a qu'une seule dame par ligne, on utilisera un tableau de booléens, initialement tous égaux à `#!py False`.
    
    On lira ensuite les numéros de lignes fournis dans la `disposition` : si le tableau contient un `#!py False` à cet indice, cela signifie que la ligne n'est pas encore occupée. Dans le cas contraire...

??? tip "Aide (2)"

    La vérification des diagonales peut se faire astucieusement en jouant sur les indices. En effet, si deux dames sont sur une  même diagonale, la de leurs numéros de lignes et celle de leurs numéros de colonnes sont égales.

    Par exemple, dans la disposition `#!py [2, 4, 1, 6, 3, 5, 7, 0]`, la troisième (indice `#!py 2`) et la cinquième (indice `#!py 4`) dames sont sur la même diagonale : `#!py 4 - 2` est égal à `#!py abs(3 - 1)`.

    ![Diagonales](verif_diag.svg){width="25%" .center}

    La valeur absolue permet de plus de vérifier en une seule condition les diagonales haut-droite et les bas-droite.

???+ example "Exemples"

    On utilise les échiquiers `valide` et `invalide` définis plus haut.

    ```pycon title=""
    >>> # colonne 0  1  2  3  4  5  6  7
    >>> valide = [6, 4, 2, 0, 5, 7, 1, 3]
    >>> disposition_valide(valide)
    True
    >>> # colonne   0  1  2
    >>> invalide = [1, 2, 0]
    >>> disposition_valide(invalide)
    False
    ```

=== "Version vide"
    {{ IDE('./pythons/exo_a') }}
=== "Version à compléter"
    {{ IDE('./pythons/exo_b') }}
