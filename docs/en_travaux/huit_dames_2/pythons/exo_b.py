

# --------- PYODIDE:code --------- #

def disposition_valide(disposition):
    n = len(disposition)

    # Vérifications des lignes
    lignes_occupees = [...] * ...
    for ligne in disposition:
        if ...[...]:
            return False
        else:
            ...[...] = ...

    # Vérification des diagonales
    for j_1 in range(n - 1):
        i_2 = ...
        for j_2 in range(j_1 + 1, n):
            i_1 = ...
            if (... - ...) == abs(... - ...):
                return ...
    return True

# --------- PYODIDE:corr --------- #

def disposition_valide(disposition):
    n = len(disposition)

    # Vérifications des lignes
    lignes_occupees = [False] * n
    for ligne in disposition:
        if lignes_occupees[ligne]:
            return False
        else:
            lignes_occupees[ligne] = True

    # Vérification des diagonales
    for j_1 in range(n - 1):
        i_1 = disposition[j_1]
        for j_2 in range(j_1 + 1, n):
            i_2 = disposition[j2]
            if (j_2 - j_1) == abs(i_2 - i_1):
                return False
    return True

# --------- PYODIDE:tests --------- #

# colonne 0  1  2  3  4  5  6  7
valide = [6, 4, 2, 0, 5, 7, 1, 3]
assert disposition_valide(valide) is True

# colonne   0  1  2
invalide = [1, 2, 0]
assert disposition_valide(invalide) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
# Tests supplémentaires
disposition = [2, 0, 6, 4, 7, 1, 3, 5]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [4, 0, 3, 5, 7, 1, 6, 2]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [3, 0, 2, 4, 1]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [2, 0, 6, 4, 7, 2, 3, 5]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [4, 0, 3, 7, 5, 2, 6, 2]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [4, 0, 3, 7, 5, 2, 6, 2]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [3, 2, 0, 4, 1]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"

disposition = [2, 0, 3, 1]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [1, 3, 5, 0, 2, 4]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [7, 2, 0, 5, 1, 4, 6, 3]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [2, 4, 1, 7, 5, 3, 6, 0]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [1, 9, 2, 5, 7, 11, 0, 12, 4, 6, 14, 3, 10, 8, 13]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [
    0,
    13,
    3,
    1,
    19,
    14,
    5,
    20,
    16,
    4,
    15,
    7,
    9,
    17,
    24,
    22,
    18,
    23,
    21,
    10,
    2,
    11,
    6,
    8,
    12,
]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [
    0,
    11,
    3,
    1,
    20,
    10,
    12,
    4,
    2,
    5,
    27,
    25,
    7,
    17,
    21,
    26,
    15,
    32,
    30,
    28,
    33,
    29,
    24,
    6,
    8,
    13,
    16,
    18,
    31,
    14,
    9,
    23,
    19,
    22,
]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"
disposition = [
    0,
    17,
    1,
    5,
    2,
    12,
    7,
    16,
    26,
    3,
    22,
    41,
    23,
    27,
    6,
    36,
    42,
    20,
    13,
    43,
    24,
    38,
    19,
    45,
    47,
    44,
    4,
    40,
    48,
    14,
    9,
    11,
    25,
    15,
    39,
    18,
    10,
    8,
    28,
    35,
    46,
    49,
    33,
    29,
    21,
    32,
    34,
    31,
    37,
    30,
]
assert disposition_valide(disposition) is True, f"Erreur avec {disposition = }"

disposition = [2, 0, 3, 0]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [1, 3, 5, 0, 3, 4]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [7, 2, 0, 1, 5, 4, 6, 3]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [2, 4, 0, 7, 5, 3, 6, 1]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [1, 9, 2, 5, 7, 11, 0, 12, 4, 6, 14, 3, 10, 8, 1]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [
    0,
    7,
    3,
    1,
    19,
    14,
    5,
    20,
    16,
    4,
    15,
    13,
    9,
    17,
    24,
    22,
    18,
    23,
    21,
    10,
    2,
    11,
    6,
    8,
    12,
]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [
    0,
    11,
    3,
    1,
    20,
    10,
    12,
    4,
    6,
    5,
    27,
    25,
    7,
    17,
    21,
    26,
    15,
    32,
    30,
    28,
    33,
    29,
    24,
    2,
    8,
    13,
    16,
    18,
    31,
    14,
    9,
    23,
    19,
    22,
]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"
disposition = [
    0,
    17,
    1,
    5,
    2,
    12,
    7,
    16,
    26,
    3,
    22,
    41,
    23,
    27,
    6,
    36,
    42,
    20,
    13,
    43,
    24,
    38,
    19,
    45,
    47,
    44,
    4,
    40,
    48,
    14,
    9,
    11,
    25,
    15,
    39,
    18,
    10,
    8,
    28,
    35,
    46,
    49,
    33,
    29,
    21,
    32,
    34,
    31,
    37,
    0,
]
assert disposition_valide(disposition) is False, f"Erreur avec {disposition = }"