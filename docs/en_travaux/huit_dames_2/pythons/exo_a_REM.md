La vérification de la présence d'une seule dame par ligne utilise le tableau de booléens décrit dans l'énoncé.

Concernant les diagonales on utilise le test `if abs(disposition[j_2] - disposition[j_1]) == (j_2 - j_1):` où `j_1` et `j_2` sont les indices des colonnes étudiées (`j_2` > `j_1`). Supposons que les dames des colonnes `3` et `7` soient sur la même diagonale orientée vers le nord-est. L'écart `j_2 - j_1` entre les indices des colonnes doit alors être égal à l'écart *en valeur absolue** entre les indices des lignes concernés.
