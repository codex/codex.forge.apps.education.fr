# --- vide, longueur_ --- #
from math import sqrt


def f(x):
    return x**2


def longueur(a, b, n): 
    ...


# --- exo, longueur_ --- #
from math import sqrt


def f(x):
    return x**2


def longueur(a, b, n):
    h = ...
    somme = ...
    for i in range(...):
        x_i = a + ...
        somme = ...
    return ...


# --- corr, longueur_ --- #
from math import sqrt


def f(x):
    return x**2


def longueur(a, b, n):
    h = (b - a) / n
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (f(x_i + h) - f(x_i)) ** 2))
    return somme


# --- tests, longueur_ --- #
assert longueur(-1, 1, 1) == 2.0
assert abs(longueur(-1, 1, 10) - 2.951919570266655) <= 1e-6
assert abs(longueur(0, 2, 30) - 4.646424448462787) <= 1e-6
# --- secrets, longueur_ --- #


def _f_(x):
    return x**2


def _longueur_(a, b, n):
    h = (b - a) / n
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (_f_(x_i + h) - _f_(x_i)) ** 2))
    return somme


from random import randrange

a = randrange(-10, 0)
b = randrange(0, 10)
n = randrange(10, 50)
attendu = _longueur_(a, b, n)
assert abs(longueur(a, b, n) - attendu) <= 1e-6, f"Erreur avec {(a, b, n) = }"

# --- vide, pi_ --- #
from math import sqrt


def f(x): 
    ...


def approximation_pi(n): 
    ...


# --- exo, pi_ --- #
from math import sqrt


def f(x):
    return ...


def approximation_pi(n):
    h = ... / ...
    a = ...
    somme = 0
    for i in range(n):
        x_i = ...
        somme = ...
    return somme


# --- corr, pi_ --- #
from math import sqrt


def f(x):
    return sqrt(1 - x**2)


def approximation_pi(n):
    h = 2 / n
    a = -1
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (f(x_i + h) - f(x_i)) ** 2))
    return somme


# --- tests, pi_ --- #
assert abs(approximation_pi(2) - 2.8284271247461903) <= 1e-6
assert abs(approximation_pi(4) - 3.035276180410083) <= 1e-6
assert abs(approximation_pi(1024) - 3.1415672753316537) <= 1e-6
# --- secrets, pi_ --- #


def _f_(x):
    return sqrt(1 - x**2)


def _approximation_pi_(n):
    h = 2 / n
    a = -1
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (_f_(x_i + h) - _f_(x_i)) ** 2))
    return somme


from random import randrange

for k in range(5, 15):
    n = 2**k
    attendu = _approximation_pi_(n)
    assert abs(approximation_pi(n) - attendu) <= 1e-6, f"Erreur avec {n = }"
