# --------- PYODIDE:code --------- #
from math import sqrt


def f(x):
    return x**2


def longueur(a, b, n):
    h = ...
    somme = ...
    for i in range(...):
        x_i = a + ...
        somme = ...
    return ...


# --------- PYODIDE:corr --------- #
from math import sqrt


def f(x):
    return x**2


def longueur(a, b, n):
    h = (b - a) / n
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (f(x_i + h) - f(x_i)) ** 2))
    return somme


# --------- PYODIDE:tests --------- #
assert longueur(-1, 1, 1) == 2.0
assert abs(longueur(-1, 1, 10) - 2.951919570266655) <= 1e-6
assert abs(longueur(0, 2, 30) - 4.646424448462787) <= 1e-6
# --------- PYODIDE:secrets --------- #


def _f_(x):
    return x**2


def _longueur_(a, b, n):
    h = (b - a) / n
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (_f_(x_i + h) - _f_(x_i)) ** 2))
    return somme


from random import randrange

a = randrange(-10, 0)
b = randrange(0, 10)
n = randrange(10, 50)
attendu = _longueur_(a, b, n)
assert abs(longueur(a, b, n) - attendu) <= 1e-6, f"Erreur avec {(a, b, n) = }"

