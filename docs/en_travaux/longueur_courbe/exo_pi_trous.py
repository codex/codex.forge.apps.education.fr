# --------- PYODIDE:code --------- #
from math import sqrt


def f(x):
    return ...


def approximation_pi(n):
    h = ... / ...
    a = ...
    somme = 0
    for i in range(n):
        x_i = ...
        somme = ...
    return somme


# --------- PYODIDE:corr --------- #
from math import sqrt


def f(x):
    return sqrt(1 - x**2)


def approximation_pi(n):
    h = 2 / n
    a = -1
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (f(x_i + h) - f(x_i)) ** 2))
    return somme


# --------- PYODIDE:tests --------- #
assert abs(approximation_pi(2) - 2.8284271247461903) <= 1e-6
assert abs(approximation_pi(4) - 3.035276180410083) <= 1e-6
assert abs(approximation_pi(1024) - 3.1415672753316537) <= 1e-6
# --------- PYODIDE:secrets --------- #


def _f_(x):
    return sqrt(1 - x**2)


def _approximation_pi_(n):
    h = 2 / n
    a = -1
    somme = 0
    for i in range(n):
        x_i = a + i * h
        somme = somme + sqrt((h**2 + (_f_(x_i + h) - _f_(x_i)) ** 2))
    return somme


from random import randrange

for k in range(5, 15):
    n = 2**k
    attendu = _approximation_pi_(n)
    assert abs(approximation_pi(n) - attendu) <= 1e-6, f"Erreur avec {n = }"
