---
author:
    - Mireille Coilhac
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Longueur d'une portion de courbe
tags:
    - en travaux
    - maths
    - fonctions
difficulty: 170
maj: 08/07/2024
---

On considère dans cet exercice une fonction $f$ continue (c'est à dire que l'on peut tracer sans lever le crayon). 
On appelle $C_f$ sa représentation graphique dans un repère orthonormé.

On cherche à calculer une valeur approchée de la longueur d'une portion de $C_f$.

Le principe est de remplacer la portion de la courbe par une succession de segments reliant des points de $C_f$ 
régulièrement espacés et de calculer la somme des longueurs de ces segments.

Dans l'exemple qui suit, on peut mesurer la longueur de différents segments tracés. Pour un petit nombre de segments, la mesure obtenue sera très imprécise mais, plus on augmentera ce nombre, plus la longueur calculée se rapprochera de la longueur de la portion de courbe.

=== "Deux segments"
    ![Longueur de portion de courbe](longueur_2.svg){ width=30% .autolight .center}
=== "Quatre segments"
    ![Longueur de portion de courbe](longueur_4.svg){ width=30% .autolight .center}
=== "Six segments"
    ![Longueur de portion de courbe](longueur_6.svg){ width=30% .autolight .center}
=== "Huit segments"
    ![Longueur de portion de courbe](longueur_8.svg){ width=30% .autolight .center}
=== "Dix segments"
    ![Longueur de portion de courbe](longueur_10.svg){ width=30% .autolight .center}

De façon formelle, on désire donc déterminer la longueur de $C_f$ pour $x$ appartenant à l'intervalle $[a;b]$
Pour cela nous allons créer $n$ subdivisions de longueur $h = \dfrac{b-a}{n}$.

Nous considérerons les points de $C_f$ d'abscisses respectives :

$$
\begin{align*}
x_0 &=a\\
\\
x_1 &=a + h\\
\\
x_2 &=a + 2h\\
\\
...\\
\\
x_n &=a + nh\\
    &= b
\end{align*}
$$


De façon générale, on a, pour $0 \leqslant i \leqslant n$ :

$$x_{i}=a + i\times h$$

La longueur d'un segment dont les extrémités ont pour abscisse $x_i$ et $x_{i+1}$ peut se calculer à l'aide du théorème de Pythagore en faisant :

$$
\begin{align*}
L_i &=\sqrt{(x_{i+1}-x_i)^2+(f(x_i+h)-f(x_i))^2}\\
    &=\sqrt{h^2+(f(x_i+h)-f(x_i))^2}
\end{align*}
$$

{{ remarque('assertion') }}

{{ remarque('proximite_flottants') }}

{{ remarque('racine_carree') }}

??? question "Longueur d'une portion de courbe de la fonction carré"

    On considère la fonction carré $x\mapsto x^2$ définie avec Python par :
    
    ```python title=""
    def f(x):
        return x**2
    ```

    On appelle $C_f$ sa représentation graphique dans un repère orthonormé.

    Écrire la fonction `longueur` qui prend en paramètres les nombres `a`, `b`, `n` correspondant respectivement 
    au début et à la fin de l'intervalle, ainsi qu'au nombre de segments considérés.
    
    Cette fonction renvoie une valeur approchée de la longueur de $C_f$ sur l'intervalle $[a;b]$ en utilisant $n$ segments.
        
    ???+ example "Exemples"

        ```pycon title=""
        >>> longueur(-1, 1, 1)  # [a, b] = [-1, 1] et 1 segment
        2.0
        >>> longueur(-1, 1, 10)  # [a, b] = [-1, 1] et 10 segments
        2.951919570266655
        >>> longueur(0, 2, 30)  # [a, b] = [0, 2] et 30 segments
        4.646424448462787
        ```

    === "Version vide"
        {{ IDE('exo_longueur_vide') }}
    === "Version à compléter"
        {{ IDE('exo_longueur_trous') }}

??? question "Approximation de $\pi$"

    La méthode précédente permet de déterminer une valeur approchée de $\pi$.

    Considérons en effet la fonction $f$ définie sur $[-1; 1]$ par $f(x)= \sqrt{1-x^2}$. Sa représentation graphique est un demi-cercle de rayon $1$ centré en l'origine du repère.

    Le périmètre ce demi-cercle vaut donc : 
    
    $$
    \begin{align*}
        P   &= \dfrac12 \times 2 \times \pi \times 1\\
            &= \pi
    \end{align*}
    $$

    Il est donc possible d'obtenir une valeur approchée de $\pi$ en appliquant la méthode précédente à la fonction $f$ entre $-1$ et $1$.

    === "$2$ segments"
        ![Approximation de $pi$](pi_2.svg){ width=30% .autolight .center}
    === "$4$ segments"
        ![Approximation de $pi$](pi_4.svg){ width=30% .autolight .center}
    === "$8$ segments"
        ![Approximation de $pi$](pi_8.svg){ width=30% .autolight .center}
    === "$1\,024$ segments"
        ![Approximation de $pi$](pi_1024.svg){ width=30% .autolight .center}
    
    Compléter ci-dessous la fonction `approximation_pi` qui prend en paramètre un entier `n` et renvoie une valeur approchée de la longueur de la fonction $f$ entre $a=-1$ et $b=1$ et utilisant $n$ segments.

    ??? note "Puissances de $2$"
    
        Afin d'éviter des erreurs d'arrondis, les valeurs de `#!py n` proposées seront toujours des puissances de `#!py 2`.

    ???+ example "Exemples"

        ```pycon title=""
        >>> approximation_pi(2)
        2.8284271247461903
        >>> approximation_pi(4)
        3.035276180410083
        >>> approximation_pi(1024)
        3.1415672753316537
        ```
        
    === "Version vide"
        {{ IDE('exo_pi_vide') }}
    === "Version à compléter"
        {{ IDE('exo_pi_trous') }}








