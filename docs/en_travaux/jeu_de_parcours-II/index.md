---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Jeu de parcours(II)
tags:
    - à trous
    - en travaux
    - booléen
difficulty: 220
---

# Jeu de parcours (II)

Dans un jeu de hasard, on se déplace sur une grille de case en case, certaines cases peuvent faire avancer le pion, quand d'autres le font reculer.

Le parcours est de taille variable dont la sortie (située en dehors du parcours) est repérée par le nombre de cases du parcours. 

Sur chaque case, se trouve le numéro de la prochaine case où doit se déplacer le pion.

!!! example "Exemples de parcours"
    Ce premier parcours possède $6$ cases. Le chiffre $6$ indique donc la sortie du parcours.

    ![Alt text](oie.drawio.svg)

    Ce deuxième parcours possède $8$ cases. Le chiffre $8$ indique donc la sortie du parcours.
    
    ![Alt text](oie4.svg)



Le pion se trouve sur la case départ et on jette une seule fois un dé à six face. On déplace alors le pion sur le parcours d'autant de cases que la valeur indiquée par le dé.

Suivant la première case où atterrit le pion, comprise entre $0$ et $5$, il peut y avoir deux résultats possibles :

* soit on arrive sur la case d'arrivée, ce qui signifie que la partie est gagnée,
* soit on tourne en rond et dans ce cas la partie est perdue.


!!! example "Exemples de déplacements"
    Dans cet exemple, la case d'arrivée est désignée par la valeur $6$, qui est le nombre de cases du parcours.

    Si le lancer de dé donne $4$, alors on déplace le pion de $4$ cases et il atterrit sur la case $3$. Le pion suit les cases suivantes : $4$ - $1$ - $6$ et la partie est alors gagnée..

    ![Alt text](oie2.drawio.svg)

    Si le lancer de dé donne $1$, alors on déplace le pion de $1$ case et il atterrit sur la case $0$. Le pion va sur la case suivante $2$ puis revient sur la case $0$ et la partie est alors perdue.

    ![Alt text](oie3.drawio.svg)

    Ainsi, les lancers $1$, $3$ et $6$ sont perdants, alors que les lancers $2$, $4$ et $5$ sont gagnants.

Le parcours de jeu est représenté sous la forme d'un tableau d'entiers `jeu_de_parcours`: si le pion est positionné sur la case d'indice `i` de la grille, `#!py jeu_de_parcours[i]` contient l'indice de la case suivante sur laquelle le pion doit se déplacer. On certifie que cet indice est valide, c'est à dire compris entre $0$ et la longueur de la grille.

```python title=""
jeu_de_parcours = [2, 6, 0, 4, 1, 0]
```

Pour détecter que l'on tourne en rond indéfiniment, on va cocher chaque fois que l'on passe sur une case. 

On souhaite comptabiliser le nombre de déplacements nécessaires pour gagner une partie, sachant que le déplacement suite au lancer de dé ne compte pas.

!!! example "Compte des déplacements"

    Si le lancer de dé donne $4$, il lui faut  $3$ déplacements pour atteindre la case d'arrivée en suivant les cases $4$ - $1$ - $6$.

    ![Alt text](oie2.drawio.svg)

??? question "Longueur du parcours"
    Écrire la fonction `longueur_parcours` qui prend en paramètre le parcours et la valeur du dé et renvoie un entier correspondant au nombre de déplacements du pion jusqu'à l'arrivée. Si la partie est perdue, la fonction renverra $-1$.

    On pourra constater que la partie est gagnée si elle se termine sans qu'aucune boucle n'ait été détectée.


    ??? tip "Indice"

        On pourra créer un tableau `deja_vu` (de booléens) de même taille que le parcours et initialement rempli de `False`. Chaque fois que l'on passe sur une case du parcours, on placera un `True` dans ce tableau `deja_vu` à la même position.

    {{ IDE('exo') }}

??? question "Minimum de déplacements"

    On souhaite maintenant déterminer, pour un parcours donné, le lancer de dé qui permet d'arriver avec le minimum de déplacements.

    En vous utilisant la fonction `longueur_parcours`, écrire la fonction `parcours_minimum` qui prend en paramètre le parcours et renvoie un entiers compris entre $1$ et $6$ correspondant au lancer qui entraine le moins de déplacement, ou `None` si aucun lancer n'est gagnant.

    {{ IDE('exo_b') }}
