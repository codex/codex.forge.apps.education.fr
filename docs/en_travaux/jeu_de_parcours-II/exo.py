

# --------- PYODIDE:code --------- #

def longueur_parcours(parcours, dé):
    case = dé - 1
    nombre_cases = ...
    longueur = 0
    while case != nombre_cases:
        if ... :
            ...
            ...
            ...
        else:
            return -1

    return longueur

# --------- PYODIDE:corr --------- #

def longueur_parcours(parcours, dé):
    case = dé - 1
    arrivée = len(parcours)
    nombre_cases = len(parcours)
    longueur = 0
    deja_vu = [False for _ in range(nombre_cases)]

    while case != nombre_cases :
        if deja_vu[case] != 1:
            deja_vu[case] = 1
            case = parcours[case]
            longueur += 1
        else:
            return -1

    return longueur

# --------- PYODIDE:tests --------- #

jeu_de_parcours = [2, 6, 0, 4, 1, 0]

assert longueur_parcours(jeu_de_parcours, 1) == -1
assert longueur_parcours(jeu_de_parcours, 2) == 1
assert longueur_parcours(jeu_de_parcours, 3) == -1
assert longueur_parcours(jeu_de_parcours, 4) == 3
assert longueur_parcours(jeu_de_parcours, 5) == 2
assert longueur_parcours(jeu_de_parcours, 6) == -1

# --------- PYODIDE:secrets --------- #

jeu_de_parcours = [0, 8, 10, 13, 18, 15, 20, 16, 14, 1, 20,
              15, 21, 11, 17, 19, 4, 12, 7, 16, 9]


assert longueur_parcours(jeu_de_parcours, 1) == -1
assert longueur_parcours(jeu_de_parcours, 2) == 5
assert longueur_parcours(jeu_de_parcours, 3) == 9
assert longueur_parcours(jeu_de_parcours, 4) == -1
assert longueur_parcours(jeu_de_parcours, 5)  == -1
assert longueur_parcours(jeu_de_parcours, 6) == -1

jeu_de_parcours_secret = [1, 2, 3, 4, 5, 6]
assert longueur_parcours(jeu_de_parcours_secret, 1)  == 6
assert longueur_parcours(jeu_de_parcours_secret, 2)  == 5
assert longueur_parcours(jeu_de_parcours_secret, 3)  == 4
assert longueur_parcours(jeu_de_parcours_secret, 4)  == 3
assert longueur_parcours(jeu_de_parcours_secret, 5)  == 2
assert longueur_parcours(jeu_de_parcours_secret, 6)  == 1

jeu_de_parcours_secret = [0, 1, 2, 3, 4, 5, 6]
assert longueur_parcours(jeu_de_parcours_secret, 1)  == -1
assert longueur_parcours(jeu_de_parcours_secret, 2)  == -1
assert longueur_parcours(jeu_de_parcours_secret, 3)  == -1
assert longueur_parcours(jeu_de_parcours_secret, 4)  == -1
assert longueur_parcours(jeu_de_parcours_secret, 5)  == -1
assert longueur_parcours(jeu_de_parcours_secret, 6)  == -1

jeu_de_parcours_secret = [666 for _ in range (666)]
assert longueur_parcours(jeu_de_parcours_secret, 1)  == 1
assert longueur_parcours(jeu_de_parcours_secret, 2)  == 1
assert longueur_parcours(jeu_de_parcours_secret, 3)  == 1
assert longueur_parcours(jeu_de_parcours_secret, 4)  == 1
assert longueur_parcours(jeu_de_parcours_secret, 5)  == 1
assert longueur_parcours(jeu_de_parcours_secret, 6)  == 1
