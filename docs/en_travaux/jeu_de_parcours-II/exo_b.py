# --------- PYODIDE:env --------- #


def longueur_parcours(parcours, dé):
    case = dé - 1
    arrivée = len(parcours)
    nombre_cases = len(parcours)
    longueur = 0
    deja_vu = [False for _ in range(nombre_cases)]

    while case != nombre_cases :
        if deja_vu[case] != 1:
            deja_vu[case] = 1
            case = parcours[case]
            longueur += 1
        else:
            return -1

    return longueur


# --------- PYODIDE:code --------- #

def parcours_minimum(parcours):
    ...

# --------- PYODIDE:corr --------- #

def parcours_minimum(parcours):
    lancer_gagnant = None
    minimum = len(parcours)
    for i in range (1, 7):
        longueur = longueur_parcours(parcours, i)
        if longueur != -1 and longueur < minimum:
            lancer_gagnant = i
            minimum = longueur
    return  lancer_gagnant

# --------- PYODIDE:tests --------- #

jeu_de_parcours = [2, 6, 0, 4, 1, 0]

assert parcours_minimum(jeu_de_parcours) == 2

# --------- PYODIDE:secrets --------- #

jeu_de_parcours = [0, 8, 10, 13, 18, 15, 20, 16, 14, 1, 20,
              15, 21, 11, 17, 19, 4, 12, 7, 16, 9]

assert parcours_minimum(jeu_de_parcours) == 2

jeu_de_parcours_secret = [1, 2, 3, 4, 5, 6]
assert parcours_minimum(jeu_de_parcours_secret) == 6


jeu_de_parcours_secret = [0, 1, 2, 3, 4, 5, 6]
assert parcours_minimum(jeu_de_parcours_secret) is None

jeu_de_parcours_secret = [666 for _ in range (666)]
assert parcours_minimum(jeu_de_parcours_secret) == 1