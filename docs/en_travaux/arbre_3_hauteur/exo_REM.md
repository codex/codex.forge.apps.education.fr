Pour calculer la profondeur d'un nœud, on parcourt un chemin allant de la racine jusqu'à ce nœud.
Si on calculait une par une la profondeur de chaque nœud, la longueur totale parcourue serait
la *longueur de cheminement externe* présentée dans l'exercice [arbre binaire cheminement](../arbre_binaire_chemin/index.md) 

On remarque que pour un nœud quelconque, la somme de sa hauteur et de sa profondeur est la hauteur de l'arbre. Donc, si on connait la hauteur $h$ d'un nœud et sa profondeur $p$, alors la hauteur de l'arbre est $h+p$.   
Cette propriété permet d'obtenir simplement le dictionnaire des hauteurs des nœuds à partir du dictionnaire des profondeurs des nœuds et de la hauteur de l'arbre.
