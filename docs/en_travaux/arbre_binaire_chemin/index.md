---
author: Serge Bays
hide:
    - navigation
    - toc
title: Arbre binaire cheminement
tags:
    - en travaux
    - arbre binaire
    - récursivité
difficulty: 250
---

# Arbre binaire et arbre
À tout arbre binaire à $n$ nœuds, on peut associer un arbre à $2n+1$ nœuds, où chaque nœud a soit deux enfants (c'est un nœud interne), soit aucun enfant (c'est un nœud externe).
Les nœuds internes sont représentés par des cercles, les nœuds externes par des carrés.

Exemple d'un arbre binaire et de l'arbre associé:

![arbre binaire](abr1.svg){width=30% .autolight .center}

??? "On peut numéroter les nœuds pour l'implémentation."

    ![arbre binaire](abr2.svg){width=30% .autolight .center}


Les $n$ nœuds sont numérotés de $0$ à $n-1$. L'ordre n'a pas d'importance.
L'arbre dessiné ci-dessus est alors représenté par le dictionnaire `{0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}`.

Pour calculer les longueurs présentées dans la suite, on utilise un parcours en profondeur.

1. On s'intéresse à la **longueur de cheminement externe** qui est la somme des longueurs de tous les chemins allant de la racine à un nœud externe.
Pour l'arbre dessiné plus haut, cette longueur vaut 12.


    ??? question "Longueur de cheminement externe"

        Écrire une fonction récursive `longueur_ce` qui prend en paramètres un arbre, un nœud, une longueur et renvoie la longueur de cheminement externe. Pour l'appel initial, les arguments passés à la fonction sont l'arbre dont on veut obtenir la longueur de cheminement externe, le numéro du nœud racine et 0 pour la longueur.
		
		??? example "Exemples"

		    ```pycon
		    >>> longueur_ce({0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}, 0, 0)
		    12
		    ```
    
        {{ IDE('quest_1/exo_1') }}

2. On affecte un poids à chaque nœud externe. Les différents poids sont donnés par un dictionnaire. On s'intéresse à la *longueur de cheminement externe pondérée* qui est la somme des longueurs de tous les chemins allant de la racine à un nœud externe, chaque chemin étant pondéré par le poids affecté au nœud externe correspondant.


    ??? question "Longueur de cheminement externe pondérée"
	
	    Exemple avec les deux arbres ci-dessous:

        ![arbre binaire](abr3.svg){width=30% .autolight .center}
		
		Pour l'arbre à gauche, on obtient 32, pour l'arbre à droite, on obtient 26 .
	    
		Écrire une fonction `longueur_ponderere` qui prend en paramètres un arbre, les poids, le nœud racine, une longueur et renvoie la longueur de cheminement externe pondérée.
		
		??? example "Exemples"

		    ```pycon
		    >>> arbre = {0: [1, 2], 1: [3, 4], 2: [5, 6]}
		    >>> poids = {3: 4, 4: 1, 5: 9, 6: 2}
		    >>> longueur_ponderee(arbre, poids, 0, 0)
		    32
		    ```

        {{ IDE('quest_2/exo_2') }}

