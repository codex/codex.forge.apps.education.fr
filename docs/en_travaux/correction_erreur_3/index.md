---
author:
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Correction d'erreurs (3)
tags:
    - en travaux
    - programmation orientée objet
difficulty: 150
---


On souhaite créer une classe `Joueur` qui initialise un attribut `pointdevie` à $20$ points et qui en enlève un à chaque appel de la méthode `touche`.

Trois élèves ont créé ce programme mais chacun a commis une erreur.

Corriger chaque programme pour qu'il puisse s'exécuter sans erreur.



??? question "Programme 1"

    {{IDE('exo1')}}

??? question "Programme 2"

    {{IDE('exo2')}}

??? question "Programme 3"

    {{IDE('exo3')}}