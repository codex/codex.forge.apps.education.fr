L'exception `AttributeError` lorsque à l'exécution on tente d'accéder à un attribut absent de la classe.

Cela se produit généralement lorsque l'attribut est mal orthographié.