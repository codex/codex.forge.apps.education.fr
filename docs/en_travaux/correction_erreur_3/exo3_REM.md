L'interpréteur est sensible à la casse, c'est-à-dire il différencie les majuscules des minuscules.

Par convention, les noms des classes commencent par une majuscule alors que leurs instances commencent par une minuscule.