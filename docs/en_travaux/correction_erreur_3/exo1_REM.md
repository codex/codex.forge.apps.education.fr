A l'intérieur d'une définition de classe, un attribut doit être précédé de `self.`.

Dans le cas contraire, il est considéré comme une variable qui est locale à la méthode.