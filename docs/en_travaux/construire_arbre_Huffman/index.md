---
author:
    - Fédéric Leleu
    - Pierre Marquestaut
    - Denis Quenton
difficulty: 350
hide:
    - navigation
    - toc
title: Codage de Huffman (construction de l'arbre)
tags:
    - en travaux
    - arbre
    - file
    - dictionnaire
    - programmation orientée objet
---

!!! abstract "Présentation rapide"

    Le codage de Huffman est un codage qui permet de compresser des données. Dans le cas d'un texte, codé en UTF-8, chaque caractère prendra la place de un à 4 octets.   
    Dans le cas du codage de Huffman , plus un caractère est fréquent dans le texte, plus court sera son codage en binaire. 

    !!! example "Exemple"
        Le texte  : `que voulez vous que je vous dise` sera codé par :  
        `#!py 01111011101110000011011000011010001111000001101010111011110111011110010110111000001101010111100110110010110`.  
        Soit $107$ bits au lieu de $256$ avec le codage UTF-8. Le caractère espace `' '` sera codé par `#!py 111`, tandis que le caractère `'l'` sera codé par `#!py 1 0000`.

    ??? note "Série d'exercices"
        Cet exercice fait partie d'une série :

        * « {{ lien_exo("Construction de l'arbre", "construire_arbre_Huffman") }} »,
        
        * « {{ lien_exo("Encoder avec l'arbre de Huffman", "encoder_avec_arbre_Huffman") }} »,

        * « {{ lien_exo("Décoder avec l'arbre de Huffman", "decoder_avec_arbre_Huffman") }} ».
        

Ce codage est basé sur la construction d'un arbre, objectif de cet exercice.

!!! note

    Toutes les fonctions créées dans des questions sont utilisables dans les questions suivantes.

## Principe de construction

+   Le codage de Huffman est basé sur un arbre, dont les feuilles sont liées aux caractères du texte.
+   On détermine les occurrences de tous les caractères d'un texte.
+   On crée les feuilles du futur arbre. Chaque feuille aura pour étiquette un caractère et son nombre d'occurrences dans le texte.
+   On fusionne les feuilles et les nœuds  deux par deux. 
+   Chaque fusion produit un nouvel arbre dont le nœud racine a pour étiquette un poids égal à la somme des poids des étiquettes des nœuds fusionnés.


???+ example "Exemple de construction de l'arbre"

    === "Étape 0"
        Prenons pour exemple le texte `que voulez vous que je vous dise`.

        Le nombre d'occurrences de chaque lettre est : 

        | q   | u   | e   |     | v   | o   | l   | z   | s   | j   | d   | i   | 
        | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
        | 2   | 5   | 5   | 6   | 3   | 3   | 1   | 1   | 3   | 1   | 1   | 1   | 


        On peut alors créer les feuilles du futur arbre.
        
        La donnée associée à chaque nœud est un caractère et son poids qui est son nombre d'occurrences.

        ```mermaid
        graph TD
            N7(l:1):::feuille
            N8(z:1):::feuille
            N90(j:1):::feuille
            N10(d:1):::feuille
            N1(i:1):::feuille
            N11(q:2):::feuille
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            classDef feuille fill:#F88
        ```

        Ces feuilles sont triées selon l'ordre croissant du poids de leur nœud.

    === "Étape 1"

        L'arbre de Huffman est un arbre construit progressivement à partir des feuilles.  

        À partir des deux nœuds ayant les plus petits poids, on crée un arbre :
        
        +   la racine aura pour donnée un symbole vide et comme poids la somme des poids des deux nœuds ;
        +   les sous arbres droit et gauche seront les deux nœuds sélectionnés.  
        
        Ici les feuilles `l:1` et `z:1` ont été fusionnés de façon à former un nouvel arbre dont le poids de la racine est `1 + 1 = 2`
        
        ```mermaid
        graph TD
            N90(j:1):::feuille
            N10(d:1):::feuille
            N1(i:1):::feuille
            N11(q:2):::feuille
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N13(( :2)) --> N7(l:1):::feuille
            N13 --> N8(z:1):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 2"
        Parmi l'ensemble des nœuds, d'une part les feuilles et d'autre part les nœuds fusionnés, on choisit les deux nœuds de poids les plus faibles, pour créer un arbre dont la racine aura pour donnée la somme des poids des deux nœuds et, pour sous arbres gauche et droit, les nœuds choisis.

        ```mermaid
        graph TD
            N1(i:1):::feuille
            N11(q:2):::feuille
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N12(( :2)) --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13(( :2)) --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 3"
        On poursuit jusqu'à ce qu'il ne reste qu'un seul nœud.  
        On peut remarquer que le poids de ce nœud sera égal à la somme des poids des nœuds initiaux, c'est à dire à la longueur du message.

        ```mermaid
        graph TD
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N12(( :2)) --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13(( :2)) --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N14(( :3)) --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 4"
         
        Ici on a fusionné deux nœuds déjà construits car leur poids (2) étaient plus petits que celui des feuilles (3 au minimum.)

        ```mermaid
        graph TD
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N14(( :3)) --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N15(( :4)) --> N12(( :2))
            N15 --> N13(( :2))
            N12(( :2)) --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13(( :2)) --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            
            classDef feuille fill:#F88
        ```

    === "Étape 5"

        ```mermaid
        graph TD
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N14(( :3)) --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N15(( :4)) --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N16(( :6)) --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            classDef feuille fill:#F88
        ``` 

    === "Étape 6"
        Ici les deux nœuds aux poids les plus petits, sont la feuille `s:3` et le nœud construit, qui avait un poids de 3

        ```mermaid
        graph TD
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            N15(( :4)) --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N16(( :6)) --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17(( :6)) --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            classDef feuille fill:#F88

        ```

    === "Étape 7"

        ```mermaid
        graph TD
            N3(e:5):::feuille
            N4(_:6):::feuille
            N16(( :6)) --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17(( :6)) --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N18(( :9)) --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 8"

        ```mermaid
        graph TD
            N16(( :6)) --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17(( :6)) --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N18(( :9)) --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N19(( :11)) --> N3(e:5):::feuille
            N19 --> N4(_:6):::feuille
            classDef feuille fill:#F88
        ```
    === "Étape 9"

        ```mermaid
        graph TD
            N18(( :9)) --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N19(( :11)) --> N3(e:5):::feuille
            N19 --> N4(_:6):::feuille
            N20(( :12)) --> N16(( :6))
            N20 --> N17(( :6))
            N16 --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17 --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 10"
        ```mermaid
        graph TD
            N20(( :12))
            N21(( :20))
            N20 --> N16(( :6))
            N20 --> N17(( :6))
            N16 --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17 --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N21 --> N18(( :9))
            N18 --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N21 --> N19(( :11))
            N19 --> N3(e:5):::feuille
            N19 --> N4(_:6):::feuille
            classDef feuille fill:#F88
        ```

    === "Étape 11"
        On poursuit jusqu'à ce qu'il ne reste qu'un seul nœud dont le poids sera égal à la somme des poids des nœuds initiaux, c'est à dire à la longueur du message. 

        ```mermaid
        graph TD
            N22(( :32)) --> N20(( :12))
            N22 --> N21(( :20))
            N20 --> N16(( :6))
            N20 --> N17(( :6))
            N16 --> N5(v:3):::feuille
            N16 --> N6(o:3):::feuille
            N17 --> N9(s:3):::feuille
            N17 --> N14(( :3))
            N14 --> N1(i:1):::feuille
            N14 --> N11(q:2):::feuille
            N21 --> N18(( :9))
            N18 --> N15(( :4))
            N18 --> N2(u:5):::feuille
            N15 --> N12(( :2))
            N15 --> N13(( :2))
            N12 --> N7(l:1):::feuille
            N12 --> N8(z:1):::feuille
            N13 --> N90(j:1):::feuille
            N13 --> N10(d:1):::feuille
            N21 --> N19(( :11))
            N19 --> N3(e:5):::feuille
            N19 --> N4(_:6):::feuille
            classDef feuille fill:#F88

        ```

???+ success "L'algorithme"

    ```text
    Tant qu'il reste au moins deux nœuds:
        prendre un nœud de poids minimal et le retirer de l'ensemble des nœuds
        prendre un second nœud de poids minimal parmi les nœuds restants et le retirer de l'ensemble des nœuds
        créer un nouveau nœud (sans symbole), de poids la somme des 2 poids des nœuds précédents
        ajouter ce nœud à l'ensemble des nœuds fusionnés
    Renvoyer l'arbre
    ```

    On peut remarquer que lorsqu'on fusionne deux nœuds, le nouveau poids est obligatoirement supérieur ou égal au poids des nœuds qui ont déjà étaient fusionnés.  

Pour la suite :

+   chaque nœud(feuille) est un arbre binaire dont l'étiquette de la racine sera un dictionnaire dont les clés seront `caractère` et `poids`.
+   une file contiendra l'ensemble des feuilles enfilées dans l'ordre croissant.
+   une autre file contiendra l'ensemble des nœuds fusionnés au fur et à mesure.
+   si la file contenant les feuilles, ne comporte qu'une seule feuille, l'arbre correspondant est cette feuille.
+   la file contenant les feuilles sera systématiquement vide à la fin du traitement.
    

???+ note "Éléments fournis"
    
    +   Une classe `Arbre` dont un exemple d'utilisation est donné ci-dessous.

        ??? example "La classe `Arbre`"

            La classe d'arbre proposée dispose de l'interface suivante :

            Constructeur :
            
            +   `Arbre(donnee)` : crée un arbre dont la racine a l'étiquette `donnee`
            +   `Arbre(donnee, sous_arbre_gauche, sous_arbre_droit)` : crée un arbre dont la racine a l'étiquette `donnee` et deux sous-arbres `sous_arbre_gauche` et sous_arbre_droit`

            Méthodes fournies :

            +   donnee() : renvoie l'étiquette de la racine
            +   sous_arbre_gauche() : renvoie le sous-arbre droit
            +   sous_arbre_droit() : renvoie le sous-arbre gauche
            +   est_feuille() 

            ??? example "Exemple d'utilisation"
                ```pycon
                >>> t1 = Arbre("b")
                >>> t2 = Arbre(1, t1, t1)
                >>> t2.donnee()
                1
                >>> t2.sous_arbre_gauche().est_feuille()
                True
                >>> t2.sous_arbre_droit().est_feuille()
                True
                >>> t2.est_feuille()
                False
                ```
                On ajoute la possibilité d'afficher un arbre, dont les étiquettes sont de forme `{"caractere":…, "poids":…}`, sous forme classique pour visualiser le contenu comme dans les exemples ci-dessus.
                
                ```pycon
                >>> a1 = Arbre({"caractere":"a","poids":1})
                >>> a2 = Arbre({"caractere":"b","poids":2})
                >>> a3 = Arbre({"caractere":"","poids":3},a1,a2)
                >>> a3.affiche()
                     ':3'      
                    /    \     
                'a:1'      'b:2'
                
                ```       
    +   Une structure de file avec la classe `File` dont un exemple d'utilisation est donné ci-dessous.

        ??? example "La classe File"

            Afin d'effectuer la comparaison, sans avoir à défiler les éléments, on ajoute la méthode `sommet`, qui renvoie le prochain élément qui sera défilé, sans le retirer.
            
            Les méthodes :  

            + constructeur : File()
            + est_vide() : renvoie un `True` si la file est vide, `False` sinon.
            + enfile(x) : place `x` à la queue de la file
            + defile() : renvoie le premier élément de la file
            + sommet() : renvoie le sommet de la file, sans le défiler.

            La fonction len() renvoie la longueur de la file passée en paramètre.

            ??? example "Exemple d'utilisation"
                ```pycon
                >>> f1 = File()
                >>> f1.est_vide()
                True
                >>> f1.enfile(10)
                >>> f1.enfile(20)
                >>> len(f1)
                2
                >>> f1.sommet()
                10
                >>> len(f1)
                2
                >>> f1.defile()
                10
                >>> len(f1)
                1
                >>> f1.defile()
                20
                
                ```
    +   Une fonction `construit_file_caracteres` qui prend en paramètre une liste de dictionnaires sous la forme `[{'caractere': 'l', 'poids': 1}, {'caractere': 'o', 'poids': 3},...]`, et qui renvoie une file de feuilles dont l'étiquette est un dictionnaire `{'caractère': valeur, 'poids': valeur}`. Les éléments de la file sont triés par ordre croissant. L'élément de poids le plus petit sera le premier élément défilé.

        ??? example "Exemple d'utilisation"

            ```pycon
            >>> occurrences_1 = [{'caractere': 'l', 'poids': 1}, {'caractere': 'o', 'poids': 3}]
            >>> file1 = construit_file_caracteres(occurrences_1) 
            ```
            L'objet `file1` contient dans l'ordre `Arbre({'caractere': 'l', 'poids': 1})`, `Arbre({'caractere': 'o', 'poids': 3})`




??? question "Question 1 : `plus_leger(file1, file2)`"

    Créer la fonction `plus_leger` qui prend comme arguments deux files, dont les éléments sont des feuilles dont les étiquettes sont des dictionnaires `{'caractère': valeur, 'poids': valeur}` et renvoie l'élément avec le poids le plus faible, en le défilant.

    Différents cas :

    === "Cas 1 : une file vide"

        Si l'une des files est vide, c'est le sommet de l'autre file qui est renvoyé.

        ```mermaid
        graph RL

        subgraph resul [Plus léger]
        R1(l:1):::feuille_resul
        end
        subgraph sub1 [file 1]
            direction TB
            N7(l:1):::feuille_resul 
            N8(z:1):::feuille
            N90(j:1):::feuille
            N10(d:1):::feuille
            N1(i:1):::feuille
            N11(q:2):::feuille
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
            
        end    

        subgraph sub2 [file 2]
                direction TB
            IN7( ):::invisible
            IN8( ):::invisible
            IN90( ):::invisible
            IN10( ):::invisible
            IN1( ):::invisible
            IN11( ):::invisible
            
            

        end   
        classDef feuille fill:#F88
        classDef feuille_resul fill:#FF8
        classDef invisible opacity:0
        sub1 --> resul 
        sub2 --> resul
        ```

        ```mermaid
        graph RL

        subgraph resul [Plus léger]
        subgraph noeud0 [ ]
                direction TB
                    IN16(( :6)):::feuille_resul --> IN5(v:3):::feuille
                    IN16 --> IN6(o:3):::feuille
                end
                
        end
        subgraph sub1 [file 1]
            
            direction TB
            IN7( ):::invisible
            IN8( ):::invisible
            IN90( ):::invisible
            IN10( ):::invisible
            IN1( ):::invisible
            IN11( ):::invisible
            
            
        end    

        subgraph sub2 [file 2]
                direction TB   
                subgraph noeud1 [ ]
                direction TB
                    N16(( :6)):::feuille_resul --> N5(v:3):::feuille
                    N16 --> N6(o:3):::feuille
                end
                subgraph noeud2 [ ]
                direction TB
                    N17(( :6)) --> N9(s:3):::feuille
                    N17 --> N14(( :3))
                    N14 --> N1(i:1):::feuille
                    N14 --> N11(q:2):::feuille
                end
                subgraph noeud3 [ ]
                direction TB
                    N18(( :9)) --> N15(( :4))
                    N18 --> N2(u:5):::feuille
                    N15 --> N12(( :2))
                    N15 --> N13(( :2))
                    N12 --> N7(l:1):::feuille
                    N12 --> N8(z:1):::feuille
                    N13 --> N90(j:1):::feuille
                    N13 --> N10(d:1):::feuille
                end
                subgraph noeud4 [ ]
                direction TB
                    N19(( :11)) --> N3(e:5):::feuille
                    N19 --> N4(_:6):::feuille
                end
        end   
        classDef feuille fill:#F88
        classDef feuille_resul fill:#FF8
        classDef invisible opacity:0
        sub1 --> resul 
        sub2 --> resul
        ```       

    === "Cas 2 : sans égalité des poids les plus petit"

        C'est le cas général, c'est l'élément au poids le plus petit qui est renvoyé.

        ```mermaid
        
        graph RL

        subgraph resul [Plus léger]
            direction TB
            IN1(i:1):::feuille_resul      
        end
        subgraph sub1 [file 1]
            direction TB
            N1(i:1):::feuille_resul
            N11(q:2):::feuille
            N5(v:3):::feuille
            N6(o:3):::feuille
            N9(s:3):::feuille
            N2(u:5):::feuille
            N3(e:5):::feuille
            N4(_:6):::feuille
        end    

        subgraph sub2 [file 2]
            direction TB 
            subgraph noeud1 [ ]
                direction TB
                N12(( :2)) --> N7(l:1):::feuille
                N12 --> N8(z:1):::feuille
            end
            subgraph noeud2 [ ]
                direction TB
                N13(( :2)) --> N90(j:1):::feuille
                N13 --> N10(d:1):::feuille
            end      
        end   
        classDef feuille fill:#F88
        classDef feuille_resul fill:#FF8
        classDef invisible opacity:0
        sub1 --> resul 
        sub2 --> resul
        ```
    === "Cas 3 : égalité"
        Si il y a égalité, c'est l'élément de la file 1 qui sera renvoyé.

        ```mermaid
        graph RL

        subgraph resul [Plus léger]
            direction TB
            IN5(v:3):::feuille_resul      
        end
        subgraph sub1 [file 1]
            direction TB
                    N5(v:3):::feuille_resul
                    N6(o:3):::feuille
                    N9(s:3):::feuille
                    N2(u:5):::feuille
                    N3(e:5):::feuille
                    N4(_:6):::feuille
            end    
            subgraph sub2 [file 2]
            direction TB 
            subgraph noeud1 [ ]
                direction TB
                    N14(( :3)) --> N1(i:1):::feuille
                    N14 --> N11(q:2):::feuille 
            end
            subgraph noeud2 [ ]
                direction TB
                N15(( :4)) --> N12(( :2))
                N15 --> N13(( :2))
                N12(( :2)) --> N7(l:1):::feuille
                N12 --> N8(z:1):::feuille
                N13(( :2)) --> N90(j:1):::feuille
                N13 --> N10(d:1):::feuille
            end      
        end   
        classDef feuille fill:#F88
        classDef feuille_resul fill:#FF8
        classDef invisible opacity:0
        sub1 --> resul 
        sub2 --> resul
        ```
    
    On assure qu'au moins une des deux files n'est pas vide.

    ???+ example "Exemple de format des paramètres et du résultat renvoyé"

        +   paramètres : Deux objets `File`, qui contiennent des nœuds dont la donnée de la racine est un dictionnaire `{'caractere': …, 'poids': …}`
        +   objet renvoyé : un objet `Arbre`, dont la donnée de la racine est un dictionnaire `{'caractere': …, 'poids': …}`
        
    ??? note "Aide pour créer ses propres tests"
                

        ???+ example "Exemple"

            Si `liste_occurrences_triee = [{'caractere': 'l', 'poids': 1}, {'caractere': 'z', 'poids': 1}, {'caractere': 'v', 'poids': 3}]`
            

        +   Pour créer des files de feuilles, on fournit la fonction `construit_file_caracteres` qui prend comme argument une liste triée de dictionnaires dont les clés sont `caractère` et `poids`, et qui renvoie une file qui contient des feuilles dont les données sont les dictionnaires de la liste, rangées dans le même ordre.  

            ```python title=""
            occurrences_1 = [{'caractere': 'l', 'poids': 1}, {'caractere': 'o', 'poids': 3}]
            file1 = construit_file_caracteres(occurrences_1) 
            ```
            L'objet `file1` contient dans l'ordre `Arbre({'caractere': 'l', 'poids': 1})`, `Arbre({'caractere': 'o', 'poids': 3})`

        +   Pour créer des files avec des nœuds.  
            Créer des feuilles, éventuellement des arbres puis enfiler les éléments dans des files.

            ```python title=""
            feuille1 = Arbre({'caractere': 'l', 'poids': 1})
            feuille2 = Arbre({'caractere': 'o', 'poids': 3})
            file1 = File()
            file1.enfile(feuille1)
            file2.enfile(feuille2)
            ```

    {{ IDE('exo_1') }}

??? question "Question 2 : `construit_arbre(file)`"
    
    ??? success "Rappel de l'algorithme de construction de l'arbre."


        ```text
        Tant qu'il reste au moins deux nœuds:
            prendre un nœud de poids minimal et le retirer de l'ensemble des nœuds
            prendre un second nœud de poids minimal parmi les nœuds restants et le retirer de l'ensemble des nœuds
            créer un nouveau nœud (sans symbole), de poids la somme des 2 poids des nœuds précédents
            ajouter ce nœud à l'ensemble des nœuds fusionnés
        Renvoyer l'arbre
        ```

    Créer la fonction `construit_arbre` qui prend comme argument une file `file_caracteres`, comme ci-dessus et renvoie un arbre de Huffman construit avec la méthode décrite ci-dessus.

    La file `file_caracteres` est supposée triée. Le premier élément qui sera défilé aura le poids le plus petit.

    {{ IDE('exo_2') }}

??? complement "Complement `arbre_Huffman(chaine)`"

    Pour créer un arbre de Huffman à partir d'une chaîne de caractères, il faut :
    
    +   déterminer les occurrences
    +   créer la file des feuilles
    +   créer l'arbre.
    
    Voici un IDE qui permet de tester le résultat, avec vos propres chaînes de caractères. La fonction à utiliser est `arbre_Huffman(chaine)` qui prend en paramètre une chaine de caractères et renvoie un arbre de Huffman construit.  
    Pour afficher le résultat sous forme d'arbre, utiliser la méthode `affiche()`. (L'affichage étant textuelle, attention à ne pas mettre un arbre trop grand)

    ```pycon title="Exemple"
    >>> arbre = arbre_Huffman("Coucou !")
    >>> arbre.affiche()

    ```    
    {{ IDE('exo_3') }}
