# --- hdr, 1, 2, 3 --- #
class ArbreBinaireError(Exception):
    def __init__(self, msg):
        self.message = msg


class ArbreBinaire():
    def __init__(self, *args):
        if len(args) == 0:
            self.__content = None
        elif len(args) != 3:
            raise ArbreBinaireError("mauvais nombre d'arguments pour construire un arbre binaire")
        elif not isinstance(args[1], ArbreBinaire) or not isinstance(args[2], ArbreBinaire):
            raise ArbreBinaireError("mauvais type d'argument pour construire un arbre binaire")
        else:
            self.__content = [args[0], args[1], args[2]]

    def est_vide(self):
        return self.__content is None
        
    def donnee(self):
        """renvoie la donnée de la racine de l'arbre binaire, si non vide"""
        try:
            return self.__content[0]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def set_donnee(self, x):
        try:
            self.__content[0] = x
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def sous_arbre_gauche(self):
        try:
            return self.__content[1]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  sous arbre gauche")

    def sous_arbre_droit(self):
        try:
            return self.__content[2]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  right subtree")
            
    def __str__(self):
        if self.est_vide():
            return '( )'
        else:
            repr_left = str(self.sous_arbre_gauche())
            repr_right = str(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(str(self.donnee()), repr_left, repr_right)

    def __repr__(self):
        if self.est_vide():
            return '(\u2205)'
        else:
            repr_left = repr(self.sous_arbre_gauche())
            repr_right = repr(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(repr(self.donnee()), repr_left, repr_right)

    def est_feuille(self):
        '''
        prédicat pour tester si un arbre est une feuille ou non, i.e. un arbre de taille 1.
        '''
        return (not self.est_vide() and 
                self.sous_arbre_gauche().est_vide() and
                self.sous_arbre_droit().est_vide())
    
    def __eq__(self, arbre):
        if self.est_vide() and arbre.est_vide():
            return True
        elif (not self.est_vide() and arbre.est_vide()) or \
             (self.est_vide() and not arbre.est_vide()) or \
             self.donnee() != arbre.donnee():
            return False
        else:
            return self.sous_arbre_gauche() == arbre.sous_arbre_gauche() and \
                   self.sous_arbre_droit() == arbre.sous_arbre_droit()
        
    def affiche(self):
        if self.est_vide():
            print (" ")
        else:
            print( sous_arbre_affiche(self)[0])
      
def feuille(data) :
    return ArbreBinaire(data, ArbreBinaire(), ArbreBinaire())

def sous_arbre_affiche(ss_arbre):
    def etiquette(arbre):
        return arbre.donnee()["caractere"] + ":" + str(arbre.donnee()["poids"])
    chaine, deb, fin, larg = "", 0, 0 ,0
    if ss_arbre.est_vide():
        return "", 0, 0, 0
    elif ss_arbre.sous_arbre_gauche().est_vide() and ss_arbre.sous_arbre_droit().est_vide():
        return repr(etiquette(ss_arbre)), 0, len(repr(etiquette(ss_arbre))), len(repr(etiquette(ss_arbre)))
    else:
        racG = racD = lienG1 = lienG2 = lienD1 = lienD2 = chaineG =chaineD = ""
        largG = largD = 0
        if not ss_arbre.sous_arbre_gauche().est_vide():
            chaineG, debG, finG, largG = sous_arbre_affiche(ss_arbre.sous_arbre_gauche())
            lienG1 = " " * (finG+1) + "_" * (largG-finG-1)
            lienG2 = " " * finG + "/" + " " * (largG-finG-1)
            racG = " " * (largG == finG)
        if not ss_arbre.sous_arbre_droit().est_vide():
            chaineD, debD, finD, largD = sous_arbre_affiche(ss_arbre.sous_arbre_droit())
            lienD1 = "_" * (debD - 1) + " " * (largD-debD + 1)
            lienD2 = " " * (debD - 1) + chr(92) + " " * (largD - debD)
            racD = " " * (debD == 0)
        # crée la partie haute du bloc
        # comportant la racine et les branches vers les sous arbres
        larg  = len(lienG1) + len((repr(etiquette(ss_arbre)))) + len(lienD1)
        deb = len(lienG1)
        fin = len(lienG1) + len((repr(etiquette(ss_arbre))))
        chaine = lienG1 + repr(etiquette(ss_arbre)) + lienD1 
        chaine = chaine + "\n" + lienG2 + " " * len(repr(etiquette(ss_arbre))) + lienD2 
        #complete la chaine gauche et la chaine droite pour qu'elles aient le même
        # nombre de lignes
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        complete = " " * (len((repr(etiquette(ss_arbre)))))
        completeG = "\n" + " " * (largG)
        completeD = "\n" + " " * (largD)
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        chaineG = chaineG + completeG * (lenD-lenG)
        chaineD = chaineD + completeD * (lenG-lenD)
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        
        # forme le nouveau bloc ave la racine et les branches et les sous arbre
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        for i in range(lenG):
            chaine = chaine + "\n" + liste_chaineG[i] + racG + complete + racD + liste_chaineD[i]
    return chaine, deb, fin, larg

class Element():
    def __init__(self, valeur, suivant = None,precedent = None):
        self.valeur = valeur
        self.suivant = suivant
        self.precedent = precedent
    
    def get_valeur(self):
        return self.valeur
    
    def get_suivant(self):
        return self.suivant
    
    def get_precedent(self):
        return self.precedent
    
    def set_precedent(self, elt):
        self.precedent = elt
        
class File():
    def __init__(self):
        self.entree = None
        self.sortie = None
        self.longueur = 0
        
    def est_vide(self):
        return self.sortie == None
    
    def enfile(self, val):
        if self.est_vide():
            self.entree = Element(val)
            self.sortie = self.entree
        else:
            ancien_debut = self.entree
            nouveau_debut = Element(val, suivant = ancien_debut)
            ancien_debut.set_precedent(nouveau_debut)
            self.entree = nouveau_debut
        self.longueur += 1
    
    def defile(self):
        assert not self.est_vide(), "On ne peut pas défiler une file vide."
        element = self.sortie
        precedent = element.get_precedent()
        self.sortie = precedent
        self.longueur -= 1
        return element.get_valeur()
    
    def sommet(self):
        return self.sortie.get_valeur()
    
    def __len__(self):
        return self.longueur
    
    def __str__(self):
        """renvoie un chaîne de caractères pour afficher une file"""
        chaine = ""
        file_temp = File()
        while not self.est_vide():
            elt = self.defiler()
            chaine = chaine  + " <- " + str(elt)
            file_temp.enfiler((elt))
        while not file_temp.est_vide():
            self.enfiler(file_temp.defiler())
        if not self.est_vide():
            chaine = chaine +" <-"
        return chaine

def compte_occurrences(chaine):
    occurrences = {}
    for caractere in chaine:
        if caractere in occurrences:
            occurrences[caractere] = occurrences[caractere] + 1
        else:
            occurrences[caractere] = 1
    return occurrences

def insere(caractere, poids, occurrences_triees):
    occurrences_triees.append({'caractere':caractere,
                               'poids':poids})
    pos = len(occurrences_triees)-1
    while pos>0 and occurrences_triees[pos]['poids'] < occurrences_triees[pos-1]['poids']:
        occurrences_triees[pos], occurrences_triees[pos-1] = occurrences_triees[pos-1], occurrences_triees[pos]
        pos = pos - 1

def trie_occurrences(occurrences):
    occurrences_triees = []
    for caractere in occurrences:
        poids = occurrences[caractere]
        insere(caractere, poids, occurrences_triees)
    return occurrences_triees
   
def construit_file_caracteres(occurrences_triees):
    file = File()
    for noeud in occurrences_triees:
        file.enfile(feuille(noeud))
    return file

# --- exo, 1 --- #
def plus_leger(file1, file2):
    ...

# --- corr, 1 | hdr, 2, 3 --- #
def plus_leger(file1, file2):
    if file1.est_vide():
        return file2.defile()
    elif file2.est_vide():
        return file1.defile()
    else :
        sommet_1 = file1.sommet().donnee()['poids']
        sommet_2 = file2.sommet().donnee()['poids']
        if sommet_1 <= sommet_2:
            return file1.defile()
        else:
            return file2.defile()
# --- tests, 1 --- #

occurrences_1 = [{'caractere': 'l', 'poids': 1}, {'caractere': 'o', 'poids': 3},]
occurrences_2 = [{'caractere': 'i', 'poids': 1}, {'caractere': 'v', 'poids': 3},  {'caractere': 'u', 'poids': 5}, {'caractere': 'e', 'poids': 5}]
file1 = construit_file_caracteres(occurrences_1) # permet de construire une file de feuilles , à partir d'une liste des dictionnaires
file2 = construit_file_caracteres(occurrences_2)
assert plus_leger(file1, file2) == feuille({'caractere': 'l', 'poids': 1})
assert plus_leger(file1, file2) == feuille({'caractere': 'i', 'poids': 1})
assert plus_leger(file1, file2) == feuille({'caractere': 'o', 'poids': 3})
assert plus_leger(file1, file2) == feuille({'caractere': 'v', 'poids': 3})
assert plus_leger(file1, file2) == feuille({'caractere': 'u', 'poids': 5})

occurrences_1 = [{'caractere': 'o', 'poids': 3},]
occurrences_2 = []
file1 = construit_file_caracteres(occurrences_1) 
file2 = construit_file_caracteres(occurrences_2)
assert plus_leger(file1, file2) == feuille({'caractere': 'o', 'poids': 3})

# --- secrets, 1 --- #
occurrences_1 = [{'caractere': 'o', 'poids': 3},]
occurrences_2 = []
file1 = construit_file_caracteres(occurrences_2) 
file2 = construit_file_caracteres(occurrences_1)
assert plus_leger(file1, file2) == feuille({'caractere': 'o', 'poids': 3}), "Si une des files est vide, le plus léger est le premier élément de l'autre file"

from random import randrange
car = chr(randrange(32,127))
longueur = randrange(1,1000)
file1 = construit_file_caracteres([{'caractere':car, 'poids':longueur}])
assert plus_leger(file1, File()) == feuille({'caractere': car, 'poids':longueur})

# --- hdr, 2 --- #

# --- exo, 2 --- #
def construit_arbre(file_caracteres):
    ...

# --- corr, 2 | hdr, 3 --- #
def construit_arbre(file_caracteres):
    file_fusionnes = File()
    while len(file_caracteres) + len(file_fusionnes)>1:
        noeud1 = plus_leger(file_caracteres, file_fusionnes)
        noeud2 = plus_leger(file_caracteres, file_fusionnes)
        donnee = {'caractere': "", 'poids' : noeud1.donnee()['poids'] + noeud2.donnee()['poids']}
        arbre = ArbreBinaire(donnee, noeud1, noeud2)
        file_fusionnes.enfile(arbre)
    if not file_caracteres.est_vide():
        return file_caracteres.defile()
    return file_fusionnes.defile()        
# --- tests, 2 --- #
occurrences_triees = [{'caractere': 'l', 'poids': 1}, {'caractere': 'z', 'poids': 1}, {'caractere': 'j', 'poids': 1}, {'caractere': 'd', 'poids': 1}, {'caractere': 'i', 'poids': 1}, {'caractere': 'q', 'poids': 2}, {'caractere': 'v', 'poids': 3}, {'caractere': 'o', 'poids': 3}, {'caractere': 's', 'poids': 3}, {'caractere': 'u', 'poids': 5}, {'caractere': 'e', 'poids': 5}, {'caractere': ' ', 'poids': 6}]
file1 = construit_file_caracteres(occurrences_triees)
assert str(construit_arbre(file1)) == "({'caractere': '', 'poids': 32}, ({'caractere': '', 'poids': 12}, ({'caractere': '', 'poids': 6}, ({'caractere': 'v', 'poids': 3}, ( ), ( )), ({'caractere': 'o', 'poids': 3}, ( ), ( ))), ({'caractere': '', 'poids': 6}, ({'caractere': 's', 'poids': 3}, ( ), ( )), ({'caractere': '', 'poids': 3}, ({'caractere': 'i', 'poids': 1}, ( ), ( )), ({'caractere': 'q', 'poids': 2}, ( ), ( ))))), ({'caractere': '', 'poids': 20}, ({'caractere': '', 'poids': 9}, ({'caractere': '', 'poids': 4}, ({'caractere': '', 'poids': 2}, ({'caractere': 'l', 'poids': 1}, ( ), ( )), ({'caractere': 'z', 'poids': 1}, ( ), ( ))), ({'caractere': '', 'poids': 2}, ({'caractere': 'j', 'poids': 1}, ( ), ( )), ({'caractere': 'd', 'poids': 1}, ( ), ( )))), ({'caractere': 'u', 'poids': 5}, ( ), ( ))), ({'caractere': '', 'poids': 11}, ({'caractere': 'e', 'poids': 5}, ( ), ( )), ({'caractere': ' ', 'poids': 6}, ( ), ( )))))"

occurrences_triees = [{'caractere': 'b', 'poids': 6}]
file1 = construit_file_caracteres(occurrences_triees)
assert str(construit_arbre(file1)) == "({'caractere': 'b', 'poids': 6}, ( ), ( ))"

# --- secrets, 2 --- #
from random import randrange
car = chr(randrange(32,127))
longueur = randrange(1,1000)
file1 = construit_file_caracteres([{'caractere':car, 'poids':longueur}])
assert str(construit_arbre(file1)) == str(f"({{'caractere': {repr(car)}, 'poids': {repr(longueur)}}}, ( ), ( ))")

# --- hdr, 3 --- #
def arbre_Huffman(chaine):
    occurrences = trie_occurrences(compte_occurrences(chaine))
    file_caracteres = construit_file_caracteres(occurrences)
    arbre = construit_arbre(file_caracteres)
    return arbre

# --- exo, 3 --- #
# bac à sable pour tester des situations


   
