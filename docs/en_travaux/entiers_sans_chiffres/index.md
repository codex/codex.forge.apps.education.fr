---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Entiers sans chiffres
tags:
    - en travaux
    - fonctions
    - int
---

# Écrire les entiers sans les chiffres

Supposons que le clavier devant nous soit abîmé : aucune des touches permettant de saisir des chiffres ne fonctionne ! Comment faire pour représenter les entiers ? Pour les additionner et les multiplier ? Les soustraire ?

On choisit tout d'abord de représenter zéro par un tuple vide `ZERO = tuple()`.

On fournit de plus la fonction `successeur` qui prend la représentation d'un entier $n$ en argument et renvoie celle de son successeur $n+1$ :

```python
def successeur(n):
    return (n,)
```

!!! note "Remarque"

    L'appel `successeur(ZERO)` renvoie `((),)` qui représente donc le nombre `UN`.
    
    **On ne demande pas** de travailler avec ces notations sous forme de *tuples*.

Avec ces deux éléments, valeur de départ `ZERO` et fonction `successeur`, il est possible de représenter tous les entiers.

```pycon
>>> ZERO = tuple()
>>> UN     = successeur(ZERO)
>>> DEUX   = successeur(UN)
>>> TROIS  = successeur(DEUX)
>>> QUATRE = successeur(TROIS)
>>> CINQ   = successeur(QUATRE)
>>> SIX    = successeur(CINQ)
>>> SEPT   = successeur(SIX)
>>> HUIT   = successeur(SEPT)
```

??? note "Remarque"

    La fonction `successeur` et les « entiers » définis ci-dessus sont directement accessibles dans le corps du programme. Il est inutile de les retaper.

Vous devez compléter trois fonctions `addition`, `multiplication` et `soustraction` ci-dessous prenant chacune en arguments les représentations `a` et `b` de deux entiers et renvoyant respectivement leur somme (`a` plus `b`), leur produit (`a` fois `b`) et leur différence (`a` moins `b`).

Les valeurs de `a` et `b` **ne devront pas** être modifiées lors du traitement.

**Les calculs sur les entiers ne seront effectués qu'à l'aide de la fonction `successeur` et des fonctions codées précédemment**.

!!! warning "Attention"

    Si `b` est plus grand que `a`, la différence « `a` moins `b` » n'est pas possible dans l'ensemble des entiers positifs.

    Dans ce cas, la fonction `soustraction` lèvera une erreur liée à une valeur incorrecte. Pour ce faire, on utilisera l'instruction `#!py raise ValueError`.

???+ example "Exemples"

    ```pycon title=""
    >>> addition(UN, UN) == DEUX
    True
    >>> addition(TROIS, DEUX) == CINQ
    True
    >>> multiplication(ZERO, CINQ) == ZERO
    True
    >>> multiplication(DEUX, TROIS) == SIX
    True
    >>> soustraction(CINQ, TROIS) == DEUX
    True
    ```

{{ IDE('exo') }}

??? tip "Aide pour `addition`"

    Additionner `a` et `b` c'est déterminer le `b`-ième successeur de `a`. On pourra utiliser une variable temporaire initialisée à `ZERO` (et pas au nombre `#!py 0` !) afin de « compter » le nombre de successeurs calculés.


??? tip "Aide pour `multiplication`"

    Multiplier `a` et `b` c'est additionner `b` fois `a` à lui-même.

??? tip "Aide pour `soustraction`"

    Soustraire `b` à `a` c'est déterminer l'« entier » qu'il faut ajouter à `b` pour obtenir `a`. On pourra donc calculer les différents successeurs de `b` jusqu'à obtenir `a`. Il faudra prendre soin de garder trace du « nombre » de successeurs calculés.
 
    Un test astucieux sur la valeur de ce « nombre » permet de repérer les soustractions impossibles.