On adapte dans cet exercice une partie des [*axiomes de Peano*](https://fr.wikipedia.org/wiki/Axiomes_de_Peano) permettant de construire les entiers naturels 0, 1, 2, ... .

On a proposé dans le corrigé des solutions itératives. Il est aussi possible d'utiliser des fonctions annexes récursives :

```python
def addition(a, b):
    def addition_rec(somme, etapes):
        if etapes == b:
            return somme
        else:
            return addition_rec(
                successeur(somme),
                successeur(etapes)
                )

    return addition_rec(a, ZERO)


def multiplication(a, b):
    def multiplication_rec(produit, etapes):
        if etapes == b:
            return produit
        else:
            return multiplication_rec(
                addition(produit, a),
                successeur(etapes)
                )

    return multiplication_rec(ZERO, ZERO)


def soustraction(a, b):
    def soustraction_rec(b, difference):
        if difference == a:
            raise ValueError
        elif b == a:
            return difference
        else:
            return soustraction_rec(
                successeur(b),
                successeur(difference)
            )
    return soustraction_rec(b, ZERO)
```