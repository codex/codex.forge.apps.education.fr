# --------- PYODIDE:env --------- #

class Entier:
    def __init__(self, i):
        self.i = i
        self.suivant = None
    
    def __str__(self):
        return str(self.i)

# --------- PYODIDE:code --------- #

class ImageRec:
    def __init__(self, images, j):
        self.debut = None
        self.courant = None
        for i in range(len(images)):
            if j == ...:
                maillon = ...
                self.ajouter(...)
    
    def ajouter(self, maillon):
        if self.debut:
            ...
        else:
            ...
        self.courant = ...
    
    def __str__(self):
        ch = ""
        maillon = self.debut
        while maillon:
            ch += str(maillon) + ", "
            maillon = maillon.suivant
        return ch[0:len(ch)-2]

# --------- PYODIDE:corr --------- #

class ImageRec:
    def __init__(self, images, j):
        self.debut = None
        self.courant = None
        for i in range(len(images)):
            if j == images[i]:
                maillon = Entier(i)
                self.ajouter(maillon)
    
    def ajouter(self, maillon):
        if self.debut:
            self.courant.suivant = maillon
        else:
            self.debut = maillon
        self.courant = maillon
    
    def __str__(self):
        ch = ""
        maillon = self.debut
        while maillon:
            ch += str(maillon) + ", "
            maillon = maillon.suivant
        return ch[0:len(ch)-2]

# --------- PYODIDE:tests --------- #

images = [2, 0, 3, 2, 2, 0]
imrec = ImageRec(images, 0)
assert str(imrec) == '1, 5'
imrec = ImageRec(images, 2)
assert str(imrec) == '0, 3, 4'
imrec = ImageRec(images, 3)
assert str(imrec) == '2'

images = [2, 2, 2, 2, 2]
imrec = ImageRec(images, 2)
assert str(imrec) == '0, 1, 2, 3, 4'

# --------- PYODIDE:secrets --------- #

# tests

images = [2, 0, 3, 2, 2, 0]
imrec = ImageRec(images, 0)
assert str(imrec) == '1, 5'
imrec = ImageRec(images, 2)
assert str(imrec) == '0, 3, 4'
imrec = ImageRec(images, 3)
assert str(imrec) == '2'

images = [2, 2, 2, 2, 2]
imrec = ImageRec(images, 2)
assert str(imrec) == '0, 1, 2, 3, 4'

# autres tests
images = [0, 1, 0, 1, 0, 1]
imrec = ImageRec(images, 0)
assert str(imrec) == '0, 2, 4'
imrec = ImageRec(images, 1)
assert str(imrec) == '1, 3, 5'

images = [0, 10]
imrec = ImageRec(images, 0)
assert str(imrec) == '0'
imrec = ImageRec(images, 10)
assert str(imrec) == '1'
