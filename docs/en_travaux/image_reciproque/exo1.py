

# --------- PYODIDE:code --------- #

def image_rec(images, m):
    # le tableau images est la liste des images
    # m est l'image max
    n = len(images)
    tetes = [-1] * ...
    suivants = [-1] * ...
    for i in range(...):
        j = ...
        suivants[...] = ...
        tetes[...] = ...
    return ...

# --------- PYODIDE:corr --------- #

def image_rec(images, m):
    # le tableau images est la liste des images
    # m est l'image maximale
    n = len(images)
    tetes = [-1] * (m+1)
    suivants = [-1] * n
    for i in range(n):
        j = images[i]
        suivants[i] = tetes[j]
        tetes[j] = i
    return tetes, suivants

# --------- PYODIDE:tests --------- #

img = [2, 0, 3, 2, 2, 0]
tetes, suivants = image_rec(img, 3)
assert tetes == [5, -1, 4, 2]
assert suivants == [-1, -1, -1, 0, 3, 1]

img = [0, 1, 2, 3, 4]
tetes, suivants = image_rec(img, 4)
assert tetes == [0, 1, 2, 3, 4]
assert suivants == [-1, -1, -1, -1, -1]

img = [2, 2, 2, 2, 2]
tetes, suivants = image_rec(img, 2)
assert tetes == [-1, -1, 4]
assert suivants == [-1, 0, 1, 2, 3]

# --------- PYODIDE:secrets --------- #

# tests

img = [2, 0, 3, 2, 2, 0]
tetes, suivants = image_rec(img, 3)
assert tetes == [5, -1, 4, 2]
assert suivants == [-1, -1, -1, 0, 3, 1]

img = [0, 1, 2, 3, 4]
tetes, suivants = image_rec(img, 4)
assert tetes == [0, 1, 2, 3, 4]
assert suivants == [-1, -1, -1, -1, -1]

img = [2, 2, 2, 2, 2]
tetes, suivants = image_rec(img, 2)
assert tetes == [-1, -1, 4]
assert suivants == [-1, 0, 1, 2, 3]

# autres tests
img = [0, 0, 0, 0]
tetes, suivants = image_rec(img, 0)
assert tetes == [3]
assert suivants == [-1, 0, 1, 2]

img = [4, 3, 2, 1, 0]
tetes, suivants = image_rec(img, 4)
assert tetes == [4, 3, 2, 1, 0]
assert suivants == [-1, -1, -1, -1, -1]

img = [0, 1, 0, 1, 0, 1]
tetes, suivants = image_rec(img, 1)
assert tetes == [4, 5]
assert suivants == [-1, -1, 0, 1, 2, 3]

img = [10, 0]
tetes, suivants = image_rec(img, 10)
assert tetes == [1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0]
assert suivants == [-1, -1]

img = [0, 10]
tetes, suivants = image_rec(img, 10)
assert tetes == [0, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1]
assert suivants == [-1, -1]
