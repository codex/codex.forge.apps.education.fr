# --------- PYODIDE:env --------- #

def image_rec(images, m):
    n = len(images)
    tetes = [-1] * (m+1)
    suivants = [-1] * n
    for i in range(n):
        j = images[i]
        suivants[i] = tetes[j]
        tetes[j] = i
    return tetes, suivants

# --------- PYODIDE:code --------- #

def antecedents(tetes, suivants, j):
    i = ...
    liste = []
    while ...:
        liste.append(...)
        i = ...
    return liste

# --------- PYODIDE:corr --------- #

def antecedents(tetes, suivants, j):
    i = tetes[j]
    liste = []
    while i != -1:
        liste.append(i)
        i = suivants[i]
    return liste

# --------- PYODIDE:tests --------- #

img = [0, 1, 2, 3, 4]
tetes, suivants = image_rec(img, 4)
assert antecedents(tetes, suivants, 1) == [1]

img = [2, 2, 2, 2, 2]
tetes, suivants = image_rec(img, 2)
assert antecedents(tetes, suivants, 2) == [4, 3, 2, 1, 0]
assert antecedents(tetes, suivants, 1) == []
        
img = [2, 0, 3, 2, 2, 0]
tetes, suivants = image_rec(img, 3)
assert antecedents(tetes, suivants, 2) == [4, 3, 0]

# --------- PYODIDE:secrets --------- #

# tests

img = [0, 1, 2, 3, 4]
tetes, suivants = image_rec(img, 4)
assert antecedents(tetes, suivants, 1) == [1]

img = [2, 2, 2, 2, 2]
tetes, suivants = image_rec(img, 2)
assert antecedents(tetes, suivants, 2) == [4, 3, 2, 1, 0]
assert antecedents(tetes, suivants, 1) == []
        
img = [2, 0, 3, 2, 2, 0]
tetes, suivants = image_rec(img, 3)
assert antecedents(tetes, suivants, 2) == [4, 3, 0]

# autres tests
img = [0, 0, 0, 0]
tetes, suivants = image_rec(img, 0)
assert antecedents(tetes, suivants, 0) == [3, 2, 1, 0]

img = [4, 3, 2, 1, 0]
tetes, suivants = image_rec(img, 4)
assert antecedents(tetes, suivants, 3) == [1]

img = [0, 1, 0, 1, 0, 1]
tetes, suivants = image_rec(img, 1)
assert antecedents(tetes, suivants, 0) == [4, 2, 0]
assert antecedents(tetes, suivants, 1) == [5, 3, 1]

img = [10, 0]
tetes, suivants = image_rec(img, 10)
assert antecedents(tetes, suivants, 0) == [1]
assert antecedents(tetes, suivants, 10) == [0]
