La liste des antécédents de chaque entier $j$ est construite avec le principe d'une liste chaînée: $j \rightarrow i_k \rightarrow i_{k-1} \rightarrow ... \rightarrow i_2 \rightarrow i_1 \rightarrow -1$

En Python, en utilisant un dictionnaire et des tableaux dynamiques, on pourrait écrire la fonction ainsi:

```python
def image_rec(images):
    dico = {}
    for i in range(len(images)):
        if images[i] not in dico:
            dico[images[i]] = [i]
        else:
            dico[images[i]].append(i)
    return dico
```

Ce programme utilise des tableaux *dynamiques*, (utilisation de la méthode `append`), pour stocker les antécédents d'un entier.

La structure même de dictionnaire est implémentée à l'aide de plusieurs tableaux et consomme plus d'espace mémoire que la solution proposée avec les deux tableaux `tetes` et `suivants`.