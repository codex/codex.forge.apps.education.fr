

# --------- PYODIDE:code --------- #

def manquant(tableau):
    ...

# --------- PYODIDE:corr --------- #

def manquant(tableau):
    debut = 0
    fin = len(tableau) - 1

    while debut <= fin:
        milieu = (debut + fin) // 2
        if tableau[milieu] == milieu:
            debut = milieu + 1
        elif tableau[milieu - 1] != milieu - 1:
            fin = milieu - 1
        else:
            return milieu

# --------- PYODIDE:tests --------- #

assert manquant([0, 2]) == 1
assert manquant([0, 1, 3]) == 2
assert manquant([0, 1, 2, 4, 5]) == 3
assert manquant([0, 1, 2, 3, 4, 6]) == 5

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from random import randrange

assert manquant([0, 2, 3, 4, 5, 6]) == 1, "Erreur avec [0, 2, 3, 4, 5, 6]"
assert manquant([0, 1, 2, 3, 4, 6]) == 5, "Erreur avec [0, 1, 2, 3, 4, 6]"


class Tableau(list):
    def __init__(self, N, valeur_manquante):
        assert N > 1_000, "La longueur doit être supérieure à 1000"
        assert N > valeur_manquante, "La valeur manquante doit être strictement inférieure à N"
        self._N = N
        self._manquant = valeur_manquante
        self._compteur_lectures = 0
        self._lectures_max = 500

    def __len__(self):
        return self._N

    def __eq__(self, autre):
        if self._N != len(autre):
            return False

        for i in range(self._N):
            if self.__getitem__(i) != autre[i]:
                return False

        return True

    def __getitem__(self, i):
        if type(i) is slice:
            indices = range(*i.indices(self._N))
            return [self.__getitem__(k) for k in indices]
        self._compteur_lectures += 1
        if self._compteur_lectures > self._lectures_max:
            raise LookupError(f"Il faut réaliser strictement moins de {self._lectures_max} lectures dans le tableau")
        if i < 0:
            i = self._N + i
        if not (0 <= i < self._N):
            raise IndexError("L'indice demandé est invalide")
        return i if i < self._manquant else i + 1

    def __getitem_bis__(self, i):
        return i if i < self._manquant else i + 1

    def __setitem__(self, i, x):
        raise NotImplementedError("Il est interdit de modifier les valeurs")

    def __str__(self):
        return f"[{self.__getitem_bis__(0)}, {self.__getitem_bis__(1)}, ..., {self.__getitem_bis__(self._N)}]"

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        for i in range(self._N):
            yield self.__getitem__(i)


for _ in range(20):
    N = randrange(10**5, 10**7)
    attendu = randrange(0, N)
    tableau = Tableau(N, attendu)
    assert manquant(tableau) == attendu, f"Erreur avec {tableau}"