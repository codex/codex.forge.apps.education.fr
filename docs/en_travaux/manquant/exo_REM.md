On utilise une recherche dichotomique basée sur l'observation suivante : les valeurs du tableau situées avant la valeur manquante sont égales à leur position dans le tableau (leur indice), celles situées après sont différentes.

La valeur manquante est ainsi la première valeur du tableau différente de son indice.

L'énoncé garantit la présence d'un unique nombre manquant. On est donc certain que la boucle principale va se terminer et même que la condition `#!py while debut <= fin:` est toujours vérifiée. On pourrait donc construire l'algorithme autour d'une répétition indéfinie qui se terminera lorsque la valeur manquante a été trouvée. On utilise ainsi un `#!py while True` :

```python
def manquant(tableau):
    debut = 0
    fin = len(tableau) - 1

    while True:  # répétition jusqu'à ce que la valeur manquante soit trouvée
        milieu = (debut + fin) // 2
        if tableau[milieu] == milieu:
            debut = milieu + 1
        elif tableau[milieu - 1] != milieu - 1:
            fin = milieu - 1
        else:
            return milieu
```
