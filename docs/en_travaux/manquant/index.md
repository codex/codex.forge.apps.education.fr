---
author: Nicolas Revéret
difficulty: 335
hide:
    - navigation
    - toc
title: Nombre manquant
tags:
    - en travaux
    - diviser pour régner
    - dichotomie
---

# Nombre manquant

Soit $n$ un nombre entier supérieur ou égal à $2$. On se donne un tableau contenant tous les entiers entre $0$ et $n$ sauf un nombre qui a été supprimé. Ce tableau est trié dans l'ordre croissant.

Le nombre supprimé n'est ni $0$ ni $n$.

Par exemple, pour $n=5$, on peut prendre `#!py [0, 1, 2, 4, 5]`. Tous les entiers entre $0$ et $5$ sont présents sauf le $3$.

Écrire la fonction `#!py manquant` qui prend en paramètre un tel tableau et renvoie la valeur manquante.

???+ warning "Attention"

    Certains des tableaux utilisés dans les tests sont très grands. Une méthode de coût linéaire sera inefficace face à ceux-ci.

    On limite donc le nombre de lectures dans chaque tableau à 500. Passé cette valeur maximale, tout nouvel accès provoquera une erreur.

???+ example "Exemples"

    ```pycon title=""
    >>> manquant([0, 2]) 
    1
    >>> manquant([0, 1, 3]) 
    2
    >>> manquant([0, 1, 2, 4, 5]) 
    3
    >>> manquant([0, 1, 2, 3, 4, 6]) 
    5
    ```

{{ IDE('exo', MAX=10) }}

??? tip "Aide (1)"
  
    Le tableau peut être partagé en deux zones : les valeurs situées avant la valeur manquante et celles situées après.

??? tip "Aide (2)"
  
    Une recherche dichotomique est plus efficace qu'une approche linéaire. Les valeurs situées avant la valeur manquante sont égales à leur position dans le tableau (leur indice), celles situées après sont différentes.

??? tip "Aide (3)"
  
    La valeur manquante est la **première** valeur du tableau différente de son indice.
