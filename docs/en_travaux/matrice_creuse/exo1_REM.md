Dans une matrice creuse de taille $n\times n$, le nombre de coefficients non nuls est de l'ordre de $n$.
Sinon, on parle de matrice dense.

On rencontre des matrices creuses en particulier avec les "gros" systèmes linéaires obtenus par la discrétisation d’équations aux dérivées partielles. Par "gros" systèmes, on entend des systèmes de l'ordre de 10000 ou 100000 équations. 
Dans ce cas, les coefficients non nuls se trouvent sur la diagonale principale et les deux diagonales qui la longent (matrice tridiagonale). Pour une matrice $n \times n$
à $n^2$ coefficients, on a environ $3n$ coefficients non nuls.

C’est pour cette raison qu’on ne stocke pas simplement les indices des lignes contenant des coeffients non nuls dans le tableau `lignes` 
qui aurait une taille $3n$ alors qu’il a ici une taille $n+1$, (économie d’espace).

Une autre raison est la facilité avec ce type de stockage pour effectuer des calculs algébriques comme le produit d'une matrice par un vecteur

Ce type de stockage porte différents noms: le stockage Morse, en anglais Compressed Sparse Row (CSR) ou Compressed Row Storage (CRS) ou Yale format.
