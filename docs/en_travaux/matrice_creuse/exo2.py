# --------- PYODIDE:env --------- #

def stockage(m):
    n = len(m)  # nombre de lignes
    nb = 0  # nombre de coefficients non nuls
    for i in range(n):
        for j in range(n):
            if m[i][j] != 0:
                nb = nb + 1
    lignes = [0] * (n+1)
    colonnes = [0] * nb
    valeurs = [0] * nb
    nb = 0
    for i in range(n):
        lignes[i] = nb
        for j in range(n):
            if m[i][j] != 0:
                colonnes[nb] = j
                valeurs[nb] = m[i][j]
                nb = nb + 1
    lignes[n] = nb
    return valeurs, colonnes, lignes

# --------- PYODIDE:code --------- #

def produit(valeurs, colonnes, lignes, x):
    ...


# --------- PYODIDE:corr --------- #

def produit(valeurs, colonnes, lignes, x):
    n = len(lignes)-1
    y = [0] * n
    for i in range(n):
        for k in range(lignes[i], lignes[i+1]):
            j = colonnes[k]
            y[i] = y[i] + valeurs[k] * x[j]
    return y

# --------- PYODIDE:tests --------- #

m = [[0, 1, 0, 0, 0],
     [1, -1, 0, 0, 0],
     [0, 0, 2, 0, 0],
     [0, 0, -2, 1, 0],
     [0, 0, 0, 0, 3]]
va, co, li = stockage(m)
assert produit(va, co, li, [1, 1, 1, 1, 1]) == [1, 0, 2, -1, 3]
assert produit(va, co, li, [0, 0, 0, 0, 0]) == [0, 0, 0, 0, 0]
assert produit(va, co, li, [0, 0, 1, 0, 0]) == [0, 0, 2, -2, 0]

# --------- PYODIDE:secrets --------- #

# tests

m = [[0, 1, 0, 0, 0],
     [1, -1, 0, 0, 0],
     [0, 0, 2, 0, 0],
     [0, 0, -2, 1, 0],
     [0, 0, 0, 0, 3]]
va, co, li = stockage(m)
assert produit(va, co, li, [1, 1, 1, 1, 1]) == [1, 0, 2, -1, 3]
assert produit(va, co, li, [0, 0, 0, 0, 0]) == [0, 0, 0, 0, 0]
assert produit(va, co, li, [0, 0, 1, 0, 0]) == [0, 0, 2, -2, 0]


# autres tests
m = [[1, 1, 1, 1],[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]
va, co, li = stockage(m)
assert produit(va, co, li, [1, 2, 3, 4]) == [10, 10, 10, 10]
assert produit(va, co, li, [5, 0, 0, 0]) == [5, 5, 5, 5]
assert produit(va, co, li, [0, 6, 0, 0]) == [6, 6, 6, 6]
assert produit(va, co, li, [0, 0, 7, 0]) == [7, 7, 7, 7]
assert produit(va, co, li, [0, 0, 0, 8]) == [8, 8, 8, 8]
