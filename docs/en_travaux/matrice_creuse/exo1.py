

# --------- PYODIDE:code --------- #

def stockage(m):
    ...


# --------- PYODIDE:corr --------- #

def stockage(m):
    n = len(m)  # nombre de lignes
    nb = 0  # nombre de coefficients non nuls
    for i in range(n):
        for j in range(n):
            if m[i][j] != 0:
                nb = nb + 1
    lignes = [0] * (n+1)
    colonnes = [0] * nb
    valeurs = [0] * nb
    nb = 0
    for i in range(n):
        lignes[i] = nb
        for j in range(n):
            if m[i][j] != 0:
                colonnes[nb] = j
                valeurs[nb] = m[i][j]
                nb = nb + 1
    lignes[n] = nb
    return valeurs, colonnes, lignes

# --------- PYODIDE:tests --------- #

m = [[0, 1, 0, 0, 0],
     [1, -1, 0, 0, 0],
     [0, 0, 2, 0, 0],
     [0, 0, -2, 1, 0],
     [0, 0, 0, 0, 3]]
va, co, li = stockage(m)
assert va == [1, 1, -1, 2, -2, 1, 3]
assert co == [1, 0, 1, 2, 2, 3, 4]
assert li == [0, 1, 3, 4, 6, 7]

m = [[1, 0, 0, 0, 0],
     [0, 1, 0, 0, 0],
     [0, 0, 1, 0, 0],
     [0, 0, 0, 1, 0],
     [0, 0, 0, 0, 1]]
va, co, li = stockage(m)
assert va == [1, 1, 1, 1, 1]
assert co == [0, 1, 2, 3, 4]
assert li == [0, 1, 2, 3, 4, 5]

# --------- PYODIDE:secrets --------- #

# tests

m = [[0, 1, 0, 0, 0], [1, -1, 0, 0, 0], [0, 0, 2, 0, 0],
     [0, 0, -2, 1, 0], [0, 0, 0, 0, 3]]
va, co, li = stockage(m)
assert va == [1, 1, -1, 2, -2, 1, 3]
assert co == [1, 0, 1, 2, 2, 3, 4]
assert li == [0, 1, 3, 4, 6, 7]

m = [[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 1, 0, 0],
     [0, 0, 0, 1, 0], [0, 0, 0, 0, 1]]
va, co, li = stockage(m)
assert va == [1, 1, 1, 1, 1]
assert co == [0, 1, 2, 3, 4]
assert li == [0, 1, 2, 3, 4, 5]

# autres tests

m = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
va, co, li = stockage(m)
assert va == []
assert co == []
assert li == [0, 0, 0, 0]

m = [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]
va, co, li = stockage(m)
assert va == [1] * 16
assert co == [0, 1, 2, 3] * 4
assert li == [0, 4, 8, 12, 16]

m = [[0, 1, 0, 0], [-1, 0, 1, 0], [0, -1, 0, 1], [0, 0, -1, 0]]
va, co, li = stockage(m)
assert va == [1, -1, 1, -1, 1, -1]
assert co == [1, 0, 2, 1, 3, 2]
assert li == [0, 1, 3, 5, 6]
