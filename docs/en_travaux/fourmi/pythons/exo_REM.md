Le nom du dresseur fait référence à [Christopher Langton](https://fr.wikipedia.org/wiki/Christopher_Langton) qui a inventé cet *automate cellulaire* appelé *fourmi de Langton*.

Malgré des règles très simples, il semble que cette fourmi finisse toujours par engendrer une *autouroute* comme illustrée ci-dessous (Par <a href="//commons.wikimedia.org/wiki/File:LangtonsAnt.png" title="File:LangtonsAnt.png">Original&nbsp;: </a> <a href="//commons.wikimedia.org/wiki/User:Krwawobrody" title="User:Krwawobrody">Krwawobrody</a> et <a href="//commons.wikimedia.org/wiki/User:Ferkel" title="User:Ferkel">Ferkel</a> Vecteur&nbsp;: <a href="//commons.wikimedia.org/wiki/User:OmegaFallon" title="User:OmegaFallon">OmegaFallon</a> — <span class="int-own-work" lang="fr">Travail personnel</span>; vectorisation de: <a href="//commons.wikimedia.org/wiki/File:LangtonsAnt.png" title="File:LangtonsAnt.png">LangtonsAnt.png</a>, <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en" title="Creative Commons Zero, Public Domain Dedication">CC0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=130154824">Lien</a>)

![Autouroute](images/LangtonsAnt.svg){.center width=40%}

