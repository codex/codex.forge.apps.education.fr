---
author:
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Zoom matrice image
tags:
    - à trous
    - en travaux
    - grille
    - ep2
difficulty: 150
--- 


{{ run('scripts/dessin1') }}
{{ figure("dessin",
            inner_text="Un dessin va s'afficher...", 
            admo_title="Dessin de coeur") }}

{{ version_ep() }}

On travaille sur des dessins en noir et blanc obtenu à partir de pixels noirs et blancs :
La figure « cœur » ci-dessus va servir d'exemple.
On la représente par une grille de nombres, c'est-à-dire par une liste composée de sous-listes de même longueur.
Chaque sous-liste représentera donc une ligne du dessin.


La fonction `zoom_liste` prend en arguments une liste `liste_depart` et un entier `k`. Elle renvoie une liste où chaque élément de `liste_depart` est dupliqué `k` fois.

???+ example "Exemple"

    ```pycon title=""
    >>> ligne =  [0, 1, 0]
    >>> zoom_liste(ligne, 2)
    [0, 0, 1, 1, 0, 0]
    ```

La fonction `zoom_dessin` prend en argument la grille `dessin` et renvoie une grille où toutes les lignes de `dessin` sont zoomées `k` fois et répétées `k` fois.

???+ example "Exemple"

    ```pycon title=""
    >>> croix =  [[0, 1, 0],
    ...           [1, 1, 1],
    ...           [0, 1, 0]
    ...           ]
    >>> zoom_dessin(croix, 2)
    [[0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0], [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1], [0, 0, 1, 1, 0, 0], [0, 0, 1, 1, 0, 0]]
    ```


??? tip "fonction `affiche`"
    
    La fonction `affiche` permet d'afficher le dessin. Les pixels noirs (1 dans la grille) seront représentés par le caractère "*" et les blancs (0 dans la grille) par deux espaces.

    ```pycon title=""
    >>> coeur = [
    ...       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ...       [0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0],
    ...       [0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0],
    ...       [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    ...       [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    ...       [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    ...       [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    ...       [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
    ...       [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
    ...       [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    ...       [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    ...       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ...    ]
    >>> affiche(coeur)

        * *       * *      
      *     *   *     *    
    *         *         *  
    *                   *  
    *                   *  
      *               *    
        *           *      
          *       *        
            *   *          
              *            
                          
    >>> affiche(zoom_dessin(coeur,3))

                    * * * * * *                   * * * * * *                  
                    * * * * * *                   * * * * * *                  
                    * * * * * *                   * * * * * *                  
              * * *             * * *       * * *             * * *            
              * * *             * * *       * * *             * * *            
              * * *             * * *       * * *             * * *            
        * * *                         * * *                         * * *      
        * * *                         * * *                         * * *      
        * * *                         * * *                         * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
              * * *                                           * * *            
              * * *                                           * * *            
              * * *                                           * * *            
                    * * *                               * * *                  
                    * * *                               * * *                  
                    * * *                               * * *                  
                          * * *                   * * *                        
                          * * *                   * * *                        
                          * * *                   * * *                        
                                * * *       * * *                              
                                * * *       * * *                              
                                * * *       * * *                              
                                      * * *                                    
                                      * * *                                    
                                      * * *                                    
    ```

??? tip "fonction `affiche_pixel`"
    
    La fonction `affiche_pixel` permet d'afficher le dessin avec la représentation des pixels noirs et blancs. 


Compléter le code dans l'IDE pour obtenir les résultats ci-dessus :

=== "Version vide"
    {{ IDE('exo_b', TERM_H=50) }}
=== "Version à compléter" 
    {{ IDE('exo', TERM_H=50) }}

{{ figure("cible",
            inner_text="Le dessin sera tracé ici", 
            admo_title="Dessin en pixel") }}