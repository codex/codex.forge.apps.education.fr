

# --------- PYODIDE:code --------- #

class AdresseIP:
    """Définit une adresse IPV4 associée à son masque de sous-réseau"""

    def __init__(self, adresse, masque):
        self.adresse = ...
        self.masque = ...

    def nombre_magique(self):
        """renvoie l'indice de l'octet significatif et le nombre magique"""
        i = 3
        while self.masque[i] == 0:
            i -= 1
        octet_significatif = self.masque[i]
        return i, 256 - octet_significatif

    def plage(self):
        """Calcule la première et la dernière adresse et renvoie
        le couple constitué de ces 2 listes
        """
        indice, magic = self.nombre_magique()
        multiple = (self.adresse[indice] // magic) * magic

        adr_debut = [..., ..., ..., ...]
        for i in range(indice):
            adr_debut[i] = ...
        adr_debut[indice] = ...

        adr_fin = [..., ..., ..., ...]
        for i in range(indice):
            adr_fin[i] = ...
        adr_fin[indice] = ...
        return adr_debut, adr_fin

# --------- PYODIDE:corr --------- #

class AdresseIP:
    """Définit une adresse IPV4 associée à son masque de sous-réseau"""

    def __init__(self, adresse, masque):
        self.adresse = [int(i) for i in adresse.split('.')]
        self.masque = [int(i) for i in masque.split('.')]

    def nombre_magique(self):
        """renvoie l'indice de l'octet significatif et le nombre magique"""
        i = 3
        while self.masque[i] == 0:
            i -= 1
        octet_significatif = self.masque[i]
        return i, 256 - octet_significatif

    def plage_adresses(self):
        indice, magic = self.nombre_magique()
        debut_octet_significatif = (self.adresse[indice] // magic) * magic
        adr_debut = [0, 0, 0, 0]
        for i in range(indice):
            adr_debut[i] = self.adresse[i]
        adr_debut[indice] = debut_octet_significatif

        adr_fin = [255, 255, 255, 255]
        for i in range(indice):
            adr_fin[i] = self.adresse[i]
        adr_fin[indice] = debut_octet_significatif + magic - 1
        return adr_debut, adr_fin

# --------- PYODIDE:tests --------- #

adresse_enonce = AdresseIP('192.168.0.1', '255.224.0.0')
assert adresse_enonce.plage_adresses() == ([192, 160, 0, 0], [192, 191, 255, 255])

# --------- PYODIDE:secrets --------- #

# tests

adresse_enonce = AdresseIP('192.168.0.1', '255.224.0.0')
assert adresse_enonce.plage_adresses() == ([192, 160, 0, 0], [192, 191, 255, 255])

# Autres tests

adr_1 = AdresseIP('10.45.185.24', '255.255.248.0')
assert adr_1.plage_adresses() == ([10, 45, 184, 0], [10, 45, 191, 255])

adr_2 = AdresseIP('172.16.1.220', '255.255.0.0')
assert adr_2.plage_adresses() == ([172, 16, 0, 0], [172, 16, 255, 255])

adr_3 = AdresseIP('192.154.88.133', '255.255.255.192')
assert adr_3.plage_adresses() == ([192, 154, 88, 128], [192, 154, 88, 191])

adr_4 = AdresseIP('131.108.78.235', '255.255.248.0')
assert adr_4.plage_adresses() == ([131, 108, 72, 0], [131, 108, 79, 255])
