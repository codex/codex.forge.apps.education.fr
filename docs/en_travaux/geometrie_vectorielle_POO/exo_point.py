

# --------- PYODIDE:code --------- #


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def abscisse(self):
        ...

    def ...(...):
        ...





# --------- PYODIDE:corr --------- #

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def abscisse(self):
        return self.x

    def ordonnee(self):
        return self.y

 

# --------- PYODIDE:tests --------- #

A = Point(0, 2)
assert A.abscisse() == 0
assert A.ordonnee() == 2

# --------- PYODIDE:secrets --------- #


# tests secrets
B = Point(3, 6)
assert B.abscisse() == 3
assert B.ordonnee() == 6