

# --------- PYODIDE:env --------- #

from math import sqrt

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def ordonnee(self):
        return self.y

    def abscisse(self):
        return self.x 


# --------- PYODIDE:code --------- #

from math import sqrt
class Vecteur:
    def __init__(self, point1, point2):
        self.x = point2.abscisse() - point1.abscisse()
        self.y = point2.ordonnee() - point1.ordonnee()

    def norme(self):
        ...




# --------- PYODIDE:corr --------- #

from math import sqrt

class Vecteur:
    def __init__(self, point1, point2):
        self.x = point2.abscisse() - point1.abscisse()
        self.y = point2.ordonnee() - point1.ordonnee()

    def norme(self):
        return sqrt(self.x ** 2 + self.y ** 2)


# --------- PYODIDE:tests --------- #

A = Point(0, 2)
B = Point(3, 6)
AB = Vecteur(A, B)
assert abs(5.0 - AB.norme()) < 1e-6

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    A = Point(randrange(-200, 200) / 2, randrange(-200, 200) / 2)
    B = Point(randrange(-200, 200) / 2, randrange(-200, 200) / 2)
    V = Vecteur(A, B)
    attendu_x = B.x - A.x
    attendu_y = B.y - A.y
    attendu = sqrt((attendu_x) ** 2 + (attendu_y) ** 2)
    assert (
        abs(V.norme() - attendu) < 10**-6
    ), f"Erreur avec A {A.x, A.y} et B {B.x, B.y}"