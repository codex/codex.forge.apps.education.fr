

# --------- PYODIDE:env --------- #


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def ordonnee(self):
        return self.y

    def abscisse(self):
        return self.x 


# --------- PYODIDE:code --------- #

class Vecteur:
    def __init__(self, point1, point2):
        self.x = point2.abscisse() - point1.abscisse()
        self.y = point2.ordonnee() - point1.ordonnee()

    def est_colineaire(self, autre_vecteur):
        ...



# --------- PYODIDE:corr --------- #

class Vecteur:
    def __init__(self, point1, point2):
        self.x = point2.abscisse() - point1.abscisse()
        self.y = point2.ordonnee() - point1.ordonnee()

    def est_colineaire(self, autre_vecteur):
        determinant = self.x * autre_vecteur.y - self.y * autre_vecteur.x
        return determinant == 0

# --------- PYODIDE:tests --------- #

A = Point(0, 2)
B = Point(3, 6)
AB = Vecteur(A, B)
C = Point(6, 12)
D = Point(0, 4)
CD = Vecteur(C, D)
assert AB.est_colineaire(CD)

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    x_u = randrange(-200, 200) / 2
    x_v = randrange(-200, 200) / 2
    y_u = randrange(-200, 200) / 2
    y_v = randrange(-200, 200) / 2
    x_w = randrange(-200, 200) / 2
    y_w = randrange(-200, 200) / 2
    U = Point(x_u, y_u)
    V = Point(x_v, y_v)
    W = Point(x_w, y_w)
    vect1 = Vecteur(U,V)
    vect2 = Vecteur(U,W)
    attendu = vect1.x * vect2.y - vect1.y * vect2.x == 0
    assert (
        vect1.est_colineaire(vect2) is attendu
    ), f"Erreur avec vecteur(u) {vect1.x, vect1.y} et vecteur(v) {vect2.x, vect2.y}"