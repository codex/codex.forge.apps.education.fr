---
author: 
    - Pierre Marquestaut
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Géométrie vectorielle en POO
tags:
    - maths
    - programmation orientée objet
    - en travaux
difficulty: 100
maj: 01/03/2024
---

On se propose de créer une classe `Vecteur` qui permet de réaliser des calculs de géométrie vectorielle. 

Cette classe s'appuie sur la classe `Point` que l'on devra compléter et dont la définition partielle de la classe est présentée ci-dessous :

```python title=""
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
```

??? question "Classe `Point`"
    Compléter les méthodes `abscisse` et `ordonnee` de la classe `Point` qui renvoient respectivement les attributs `x` et `y` de l'objet.


    {{ IDE('exo_point') }}

??? question "Constructeur de la classe `Vecteur`"
    Soit $A$ et $B$ deux points du plan. On souhaite connaître les coordonnées du vecteur $\overrightarrow{AB}$
        
    Compléter le constructeur de la classe `Vecteur` qui prend en paramètres deux instances `point1` et `point2` de la classe `Point` et initialise les attributs `x` et `y`, qui représentent les coordonnées du vecteur fomré par ces deux points.
        
    ???+ example "Exemples"

        ```pycon title=""
        >>> A = Point(0, 2)
        >>> B = Point(3, 6)
        >>> AB = Vecteur(A, B)
        >>> AB.x
        3
        >>> AB.y
        4
        ```

    {{ IDE('exo_a') }}

??? question "Méthode `norme`"
    
    Soit $A$ et $B$ deux points du plan. On souhaite connaître la norme du vecteur $\lvert\lvert\overrightarrow{AB} \lvert\lvert$.

    Écrire la méthode `norme` de la classe `Vecteur` qui renvoie la norme de ce vecteur.

    ???+ example "Exemples"

        ```pycon title=""
        >>> AB.norme()
        5.0
        ```

    {{ remarque('racine_carree') }}
          

    {{ remarque('proximite_flottants') }}

    {{ IDE('exo_b') }}


??? question "Méthode `est_colineaire`"
    Écrire la méthode `est_colineaire` qui prend en paramètre une instance de la classe `Vecteur` et renvoie la valeur booléenne indiquant si ces vecteurs sont colinéaires ou non.

    !!! note "Remarque"
        Du fait des problèmes de comparaison entre nombres flottants, on se limitera à des coordonnées entières.

    ???+ example "Exemples"

        ```pycon title=""
        >>> A = Point(0, 2)
        >>> B = Point(3, 6)
        >>> AB = Vecteur(A, B)
        >>> C = Point(6, 12)
        >>> D = Point(0, 4)
        >>> CD = Vecteur(C, D)
        >>> AB.est_colineaire(CD)
        True
        >>> E = Point(0, 0)
        >>> CE = Vecteur(C, E)
        >>> AB.est_colineaire(CE)
        False
        ```    


    {{ IDE('exo_c') }}
