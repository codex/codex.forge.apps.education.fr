On peut dans ce cas utiliser indifféremment les **assesseurs** `abscisse` et `ordonnee` ou directement les attributs `x` et `y`.

```python
class Vecteur:
    def __init__(self, point1, point2):
        self.x = point2.x - point1.x
        self.y = point2.y - y
```