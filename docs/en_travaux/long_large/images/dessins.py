import drawsvg as draw
from math import pi, cos, sin


def degres(x):
    return 180 * x / pi


def radians(x):
    return pi * x / 180


def forward(longueur, t=None):
    if t is None:
        t = tortue
    a = radians(t["h"])
    nv_x = t["x"] + longueur * cos(a)
    nv_y = t["y"] + longueur * sin(a)
    d.append(draw.Line(t["x"], t["y"], nv_x, nv_y, **ARGS))
    t["x"] = nv_x
    t["y"] = nv_y


def left(angle, t=None):
    if t is None:
        t = tortue
    t["h"] -= angle


def right(angle, t=None):
    if t is None:
        t = tortue
    t["h"] += angle


def heading(t=None):
    if t is None:
        t = tortue
    return t["h"]


def position(t=None):
    if t is None:
        t = tortue
    return t["x"], t["y"]


def setheading(deg, t=None):
    if t is None:
        t = tortue
    t["h"] = deg


def penup():
    pass


def pendown():
    pass


def goto(x, y, t=None):
    if t is None:
        t = tortue
    t["x"] = x
    t["y"] = y


def arbre(longueur, etape):
    pile = [(0, 100, -90, longueur, etape)]
    while pile:
        x, y, d, l, e = pile.pop()
        penup()
        goto(x, y)
        setheading(d)
        pendown()
        forward(l)
        if e > 0:
            x, y = position()
            d = heading()
            pile.append((x, y, d - 90, l * 0.5, e - 1))
            pile.append((x, y, d, l * 0.5, e - 1))
            pile.append((x, y, d + 90, l * 0.5, e - 1))


d = draw.Drawing(320, 400, origin="center")
ARGS = {
    "stroke": "black",
    "stroke_width": 1,
}
tortue = {"x": 0, "y": 0, "h": 0}

longueur = 100
etape = 5
arbre(longueur, etape)
d.save_svg(f"arbre_{longueur}_{etape}.svg")
