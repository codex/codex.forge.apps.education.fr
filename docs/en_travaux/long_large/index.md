---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: De long en large
difficulty: 250
tags:
    - en travaux
    - pile
    - file
maj: 21/11/2024
---

{{ remarque('tortue') }}

??? note "Nouvel arbre" 
    
    Cet exercice est un prolongement de celui sur {{ lien_exo("les dessins d'arbres", 'dessin_arbres') }}.

On souhaite dessiner l'arbre ci-dessous en utilisant le module `turtle`.

![Arbre](images/arbre_100_5.svg){ .autolight .center width=30%}

On précise que la branche principale a une longueur de `#!py 200` pixels et que l'on a tracé `#!py 5` niveaux de profondeur. A chaque étape, la longueur est divisée par `#!py 2`.

Cet arbre a une structure **récursive** : les sous-arbres de droite, du haut et de gauche ont, à un niveau de profondeur près, la même forme que l'arbre entier.

<center>

![Sous-arbre droit ](images/gauche_100_5.svg){ .autolight width=15%}
![Sous-arbre haut ](images/haut_100_5.svg){ .autolight width=15%}
![Sous-arbre gauche](images/droite_100_5.svg){ .autolight width=15%}
</center>

Il est donc possible de construire cet arbre en utilisant une fonction récursive. C'est ce qui est proposé dans {{ lien_exo('cet exercice', 'dessin_arbres') }}.

On propose ici de procéder différemment. Il est en effet possible de dessiner l'arbre en utilisant des parcours en profondeur ou en largeur :

* dans le parcours en profondeur, on dessine l'arbre « branche » par « branche ». Ce parcours utilise une **Pile** ;
* dans le parcours en largeur, on dessine l'arbre « niveau » par « niveau ». Ce parcours utilise une **File** ;

Dans les deux cas, l'algorithme est identique :

```text title="Algorithme commun"
Soit s la Structure de données adaptée au parcours (Pile ou File)
Ajouter à s la position, la direction, la longueur et le nombre d'étapes initiaux
Tant que s n'est pas vide :
    Retirer de s la position, la direction, la longueur et le nombre d'étapes actuels
    Se déplacer à la position souhaitée
    S'orienter vers la direction souhaitée
    Tracer un segment de la longueur souhaitée
    Si le niveau est strictement positif :
        Ajouter à s la nouvelle position, la direction - 90°, la longueur * 0.5 et le niveau - 1
        Ajouter à s la nouvelle position, la direction      , la longueur * 0.5 et le niveau - 1
        Ajouter à s la nouvelle position, la direction + 90°, la longueur * 0.5 et le niveau - 1
```

On fournit les classes `Pile` et `File` décrites ci-dessous :

{{ remarque('classe_Pile') }}

{{ remarque('classe_File') }}

On demande donc d'écrire les fonctions `dessin_profondeur` et `dessin_largeur` qui prennent l'une et l'autre en paramètres la longueur et le nombre de niveaux initial et dessinent l'arbre en effectuant respectivement un parcours en profondeur ou en largeur.

{{ IDE('pythons/arbres_vide', MODE="delayed_reveal")}}

{{ figure(div_class="py_mk_figure center", admo_title='Vos arbres') }}