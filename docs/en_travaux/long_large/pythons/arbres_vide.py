# --------- PYODIDE:env --------- #
from js import document
from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.appendleft(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.pop()

    def __str__(self):
        """Convertit la file en une chaîne de caractères"""
        return f"{list(self.valeurs)}"


class Pile:
    """Classe définissant une structure de pile"""

    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon"""
        return self.contenu == []

    def empile(self, x):
        """Place x au sommet de la pile"""
        self.contenu.append(x)

    def depile(self):
        """Retire et renvoie l'élément placé au sommet de la pile.
        Provoque une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.contenu.pop()


_prefixes_possibles = ["turtle", "tt"]
if "restart" in globals():
    restart()
else:
    for pref in _prefixes_possibles:
        if pref in globals():
            p = eval(pref)
            p.restart()
            break


def m_a_j():
    cible = "figure1"
    if "done" in globals():
        done()
        document.getElementById(cible).innerHTML = Screen().html
    else:
        for pref in _prefixes_possibles:
            if pref in globals():
                p = eval(pref)
                p.done()
                document.getElementById(cible).innerHTML = p.Screen().html
                break


# --- PYODIDE:code --- #
from turtle import *

# Réglages initiaux
speed(10)  # vitesse de l'animation
penup()  # le crayon est levé
goto(0, -200)  # position initiale
setheading(90)  # la direction initiale
pendown()  # le crayon est baissé


def dessin_profondeur(longueur, etape):
    x, y = position()
    d = heading()
    pile = Pile()
    pile.empile((..., ..., ..., ..., ...))
    while ...:
        ...
        

def dessin_largeur(longueur, etape):
    ...


dessin_profondeur(200, 5)
# Retour au point de départ
penup()
goto(0, -200)
setheading(90)
pendown()
# Changement de couleur
pencolor("red")

dessin_largeur(200, 5)
# --- PYODIDE:corr --- #
from turtle import *

# Réglages initiaux
speed(10)  # vitesse de l'animation
penup()  # le crayon est levé
goto(0, -200)  # position initiale
setheading(90)  # la direction initiale
pendown()  # le crayon est baissé


def dessin_profondeur(longueur, etape):
    x, y = position()
    d = heading()
    pile = Pile()
    pile.empile((x, y, d, longueur, etape))
    while not pile.est_vide():
        x, y, d, l, e = pile.depile()
        penup()
        goto(x, y)
        setheading(d)
        pendown()
        forward(l)
        if e > 0:
            x, y = position()
            d = heading()
            pile.empile((x, y, d - 90, l * 0.5, e - 1))
            pile.empile((x, y, d, l * 0.5, e - 1))
            pile.empile((x, y, d + 90, l * 0.5, e - 1))


def dessin_largeur(longueur, etape):
    x, y = position()
    d = heading()
    file = File()
    file.enfile((x, y, d, longueur, etape))
    while not file.est_vide():
        x, y, d, l, e = file.defile()
        penup()
        goto(x, y)
        setheading(d)
        pendown()
        forward(l)
        if e > 0:
            x, y = position()
            d = heading()
            file.enfile((x, y, d - 90, l * 0.5, e - 1))
            file.enfile((x, y, d, l * 0.5, e - 1))
            file.enfile((x, y, d + 90, l * 0.5, e - 1))


dessin_profondeur(200, 5)
# Retour au point de départ
penup()
goto(0, -200)
setheading(90)
pendown()
# Changement de couleur
pencolor("red")

dessin_largeur(200, 5)
# --- PYODIDE:post --- #
if "post_async" in globals():
    await post_async()
if "Screen" in globals():
    if Screen().html is None:
        forward(0)
    m_a_j()
elif "turtle" in globals():
    if turtle.Screen().html is None:
        turtle.forward(0)
    m_a_j()

# --------- PYODIDE:post_term --------- #
if any(pref in globals() for pref in _prefixes_possibles) and "m_a_j" in globals():
    m_a_j()
