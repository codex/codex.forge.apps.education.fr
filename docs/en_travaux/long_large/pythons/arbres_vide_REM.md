Comme on peut le voir, les deux parcours tracent le même arbre en suivant un « chemin » différent.

Il est intéressant de noter que le parcours en profondeur peut aussi être codé à l'aide d'une fonction récursive :

```python title="Parcours en profondeur récursif"
def dessin_profondeur(x, y, direction, longueur, etape):
    penup()
    goto(x, y)
    setheading(direction)
    pendown()
    forward(longueur)
    if etape > 0:
        x, y = position()
        direction = heading()
        longueur *= 0.5
        etape -= 1
        dessin_profondeur(x, y, direction - 90, longueur, etape)
        dessin_profondeur(x, y, direction, longueur, etape)
        dessin_profondeur(x, y, direction + 90, longueur, etape)
```

Cette approche est *presque* identique à celle utilisant une pile. En effet, ici, le
premier appel récursif trace le sous-arbre de « droite ». On trace ensuite l'arbre du « haut » et enfin celui de « gauche ».

Dans l'approche avec une pile, les informations décrivant le sous-arbre de « droite » sont empilées en premier : elles sont donc effectuées en dernier... La construction se fait *à l'envers*, de la gauche vers la droite !

