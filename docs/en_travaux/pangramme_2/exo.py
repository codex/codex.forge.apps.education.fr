

# --------- PYODIDE:code --------- #

def indice(minuscule):
    return ord(...) - ord(...)


def minuscule(i):
    return chr(ord(...) + ...)


def lettres_manquantes(phrase):
    presences = [...] * ...
    for caractere in phrase:
        if "a" <= caractere <= ...:
            presences[...] = True

    resultat = []
    for i in range(...):
        if ...:
            resultat.append(...)

    return resultat

# --------- PYODIDE:corr --------- #

def indice(minuscule):
    return ord(minuscule) - ord("a")


def minuscule(i):
    return chr(ord("a") + i)


def lettres_manquantes(phrase):
    presences = [False] * 26
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    resultat = []
    for i in range(26):
        if not presences[i]:
            resultat.append(minuscule(i))

    return resultat


# Tests
assert indice("a") == 0
assert indice("z") == 25
assert minuscule(1) == "b"
assert minuscule(24) == "y"
assert lettres_manquantes("portez ce vieux whisky au juge blond qui fume !") == []
assert lettres_manquantes("portez un vieux whisky au juge blond qui fume !") == ["c"]
assert lettres_manquantes("portez ce vieux whisky au juge blond !") == ["f", "m", "q"]

# --------- PYODIDE:tests --------- #

assert indice("a") == 0
assert indice("z") == 25
assert minuscule(1) == "b"
assert minuscule(24) == "y"
assert lettres_manquantes("portez ce vieux whisky au juge blond qui fume !") == []
assert lettres_manquantes("portez un vieux whisky au juge blond qui fume !") == ["c"]
assert lettres_manquantes("portez ce vieux whisky au juge blond !") == ["f", "m", "q"]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
import string

alphabet = list(string.ascii_lowercase)
for i, lettre in enumerate(alphabet):
    assert indice(lettre) == i
    assert minuscule(i) == lettre
assert lettres_manquantes("a") == alphabet[1:]
assert lettres_manquantes("") == alphabet
assert lettres_manquantes(" ".join(alphabet[2:])) == ["a", "b"]
assert lettres_manquantes(", ".join(alphabet)) == []