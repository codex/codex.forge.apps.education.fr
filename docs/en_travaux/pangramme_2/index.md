---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Presque Pangramme
tags:
    - à trous
    - en travaux
    - booléen
    - string
difficulty: 250
---
# Trouver les lettres manquantes dans un pangramme

Un *pangramme* est une phrase contenant toutes les lettres de l'alphabet.

Il existe dans l'alphabet latin utilisé en français 26 lettre minuscules « classiques ». Si l'on tient compte des lettres accentuées et des ligatures (*à*, *â*, ..., *ç*, *æ* et *œ*), le décompte passe à 42 caractères distincts.

**On se limite dans cet exercice aux lettres minuscules de l'alphabet : *a*, *b*, *c*, ..., *z*.**

Un exemple classique de pangramme est « *portez ce vieux whisky au juge blond qui fume* ». Chaque lettre y apparaît au moins une fois.

Par contre « *portez __un__ vieux whisky au juge blond qui fume* » n'est pas un pangramme : il manque la lettre *c*.

Vous devez écrire trois fonctions :

* `indice` prend en argument une lettre minuscule entre `#!py "a"` et `#!py "z"` et renvoie son indice dans l'alphabet (en débutant à `#!py 0` pour `#!py "a"`) ;
* `minuscule` prend en argument un indice entre `#!py 0` et `#!py 25` (inclus l'un et l'autre) et renvoie la lettre de l'alphabet correspondante (`minuscule(1)` renvoie `#!py "b"`) ;
* `lettres_manquantes` qui :
    * prend en argument une chaîne de caractères `phrase`,
    * renvoie la liste des lettres manquantes dans cette chaîne pour qu'elle soit un pangramme.

On prendra soin de renvoyer les lettres manquantes **dans l'ordre alphabétique**.

???+ example "Exemples"

    ```pycon title=""
    >>> indice("a")
    0
    >>> indice("z")
    25
    >>> minuscule(1)
    'b'
    >>> minuscule(24)
    'y'
    ```

    ```pycon
    >>> lettres_manquantes("portez ce vieux whisky au juge blond qui fume !")
    []
    >>> lettres_manquantes("portez un vieux whisky au juge blond qui fume !")
    ['c']
    >>> lettres_manquantes("portez ce vieux whisky au juge blond !")
    ['f', 'm', 'q']
    ```

??? tip "Indice"
    On pourra utiliser une liste `presences` contenant initialement 26 fois la valeur `#!py False`. Cette liste sera mise à jour lors d'un parcours de la chaîne de caractères : lorsque l'on rencontre une lettre, on met `#!py True` dans la cellule correspondante.

    Une fois la phrase lue en entier, les lettres manquantes peuvent être déterminées en parcourant la liste de booléens.


{{ IDE('exo') }}
