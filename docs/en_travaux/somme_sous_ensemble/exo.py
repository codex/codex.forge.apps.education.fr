

# --------- PYODIDE:code --------- #

def somme_possible(ens, s, i, memo):
    if s == 0:
        ...
    if i >= ... or s < ...:
        ...
    if (s, i) not in memo:
        avec_elt_i = ...
        sans_elt_i = ...
        memo[(s, i)] = ...
    return memo[(s, i)]

# --------- PYODIDE:corr --------- #

def somme_possible(ens, s, i, memo):
    if s == 0:
        return True
    if i >= len(ens) or s < 0:
        return False
    if (s, i) not in memo:
        avec_elt_i = somme_possible(ens, s - ens[i], i+1, memo)
        sans_elt_i = somme_possible(ens, s, i+1, memo)
        memo[(s, i)] = avec_elt_i or sans_elt_i
    return memo[(s, i)]

# --------- PYODIDE:tests --------- #

ens = [4, 2, 8, 1]
assert somme_possible(ens, 7, 0, {})
ens = [5, 3, 3, 8, 4, 3]
assert somme_possible(ens, 10, 0, {})
for s in range(3, 24):
    assert somme_possible(ens, s, 0, {})
assert not somme_possible(ens, 24, 0, {})
assert not somme_possible(ens, 25, 0, {})

# --------- PYODIDE:secrets --------- #

ens = [4, 2, 8, 1]
for s in range(1, 16):
    assert somme_possible(ens, s, 0, {}), f"Résultat incorrect pour {ens} et {s}"
ens = [1, 2, 4, 5, 10, 20, 40, 50, 100]
for s in range(1, 233):
    assert somme_possible(ens, s, 0, {}), f"Résultat incorrect pour {ens} et {s}"
