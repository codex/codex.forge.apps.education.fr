Il s'agit d'une recherche de maximum classique. La liste étant non-vide, on initialise la variable `maxi` avec la première valeur.

Il ets aussi possible d'utiliser une version récursive :

```python
def maximum(nombres, i=0):
    if  i + 1 == len(nombres):
        return nombres[i]
    else:
        maxi = maximum(nombres, i+1)
        if nombres[i] > maxi:
            return nombres[i]
        else:
            return maxi
```
