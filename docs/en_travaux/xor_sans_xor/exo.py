

# --------- PYODIDE:code --------- #

def maximum(nombres):
    ...

# --------- PYODIDE:corr --------- #

def maximum(nombres):
    maxi = nombres[0]
    for x in nombres:
        if x > maxi:
            maxi = x
    return maxi


# Tests
assert maximum([98, 12, 104, 23, 131, 9]) == 131
assert maximum([-27, 24, -3, 15]) == 24

# --------- PYODIDE:tests --------- #

assert maximum([98, 12, 104, 23, 131, 9]) == 131
assert maximum([-27, 24, -3, 15]) == 24

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
def max(liste):
    raise Exception("Vous avez utilisé la fonction max. C'est explicitement interdit.")

def sorted(liste):
    raise Exception("Vous avez utilisé la fonction sorted. C'est explicitement interdit.")

class Liste(list):
    def sort(self):
        raise Exception("Vous avez utilisé la méthode sort. C'est explicitement interdit.")

assert maximum([1, 2, 3, 4, 5]) == 5
assert maximum([5, 4, 3, 2, 1]) == 5
assert maximum([5, 5, 5]) == 5
assert abs(maximum([5.01, 5.02, 5.0]) - 5.02) < 10**-6
assert maximum([-5, -4, -3, -8, -6]) == -3
assert maximum(Liste([1,2])) == 2