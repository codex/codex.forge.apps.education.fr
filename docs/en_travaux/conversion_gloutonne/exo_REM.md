👉 Cet algorithme glouton nécessite de commencer par les plus grandes puissances de 2 utiles.

Contrairement à ce que nous avons fait dans l'algorithme glouton de rendu de monnaie, il n'est pas nécessaire de constituer l'ensemble de la liste des puissances de $2$ utiles équivalente à la liste des pièces du rendu de monnaie. Il est en effet possible de trouver la puissance de $2$ suivante par simple division entière par $2$.

Visualisation du principe du binaire : [Cartes binaires](https://www.csfieldguide.org.nz/en/interactives/binary-cards/){ .md-button target="_blank" rel="noopener" }

