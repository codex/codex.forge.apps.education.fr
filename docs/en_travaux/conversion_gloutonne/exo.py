

# --------- PYODIDE:code --------- #

def binaire(nombre):
    ...

# --------- PYODIDE:corr --------- #

def binaire(nombre):
    if nombre == 0:
        return "0"

    resultat = ""

    puissance_max = 1
    while 2 * puissance_max <= nombre :
        puissance_max = 2 * puissance_max

    while puissance_max > 0:
        if nombre >= puissance_max:
            resultat = resultat + "1"
            nombre = nombre - puissance_max
        else:
            resultat = resultat + "0"
        puissance_max = puissance_max // 2

    return resultat

# --------- PYODIDE:tests --------- #

assert binaire(43) == '101011'
assert binaire(32) == '100000'
assert binaire(0) == '0'
assert binaire(54321) == '1101010000110001'

# --------- PYODIDE:secrets --------- #


# Autres tests

assert binaire(10922) == '10101010101010'
assert binaire(32319) == '111111000111111'
assert binaire(4095) == '111111111111'

from random import randrange
for _ in range(20):
     n = randrange(1, 2**20)
     attendu = bin(n)[2:]
     assert binaire(n) == attendu, f"Erreur avec la conversion de {n}"