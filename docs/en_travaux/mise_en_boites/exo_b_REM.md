Il s'agit du problème classique du *bin-packing*. On applique ici la méthode de la « *première position décroissante* » : on considère les objets dans l'ordre décroissant des poids et on place chacun dans la première boîte pouvant l'accueillir.

La boucle principale de la fonction `rangement` est une boucle `#!py while`. On aurait pu utiliser une boucle `#!py for` en s'appuyant sur une instruction `#!py break` et un booléen pour indiquer que l'objet a été rangé :

```python
def rangement(poids_objets, poids_max):
    boites = []
    poids_boites = []
    for poids in poids_objets:
        fait = False
        for i in range(len(boites)):
            if poids_boites[i] + poids <= poids_max:
                boites[i].append(poids)
                poids_boites[i] += poids
                fait = True
                break  # sortie de la boucle si l'objet est rangé
        if not fait:
            boites.append([poids])
            poids_boites.append(poids)
    return boites
```

D'un point de vue algorithmique, le problème du *bin-packing* est un problème difficile. La méthode proposée ici permet d'obtenir des résultats **satisfaisants** (utilisant *peu* de boîtes) mais pas toujours **optimaux** (utilisant un *minimum* de boîtes).

Par exemple, dans la situation `#!py poids_objets = [5, 4, 3, 2, 2, 2, 2]` et `#!py poids_max = 10`, l'algorithme utilisé utilise trois boîtes (`#!py [[5, 4], [3, 2, 2, 2], [2]]`) alors que la solution optimale `#!py [[5, 3, 2], [4, 2, 2, 2]]` n'en utilise que deux.

