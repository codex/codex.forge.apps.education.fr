

# --------- PYODIDE:code --------- #

def rangement(poids_objets, poids_max):
    ...

# --------- PYODIDE:corr --------- #

def rangement(poids_objets, poids_max):
    boites = []
    poids_boites = []

    for poids in poids_objets:
        # On parcourt les boites
        i = 0
        while i < len(boites) and poids_boites[i] + poids > poids_max:
            i += 1
        # On a besoin d'une nouvelle boîte
        if i == len(boites):
            boites.append([])
            poids_boites.append(0)
        # On ajoute l'objet
        boites[i].append(poids)
        poids_boites[i] += poids

    return boites

# --------- PYODIDE:tests --------- #

assert rangement([7, 4, 3, 1], 8) == [[7, 1], [4, 3]]
assert rangement([7, 4, 3, 1], 7) == [[7], [4, 3], [1]]
assert rangement([7, 4, 3, 1], 15) == [[7, 4, 3, 1]]

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
poids_objets, poids_max = [10, 10, 10], 10
attendu = [[10], [10], [10]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [1, 2, 3, 4], 10
attendu = [[1, 2, 3, 4]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [1, 2, 3, 4], 9
attendu = [[1, 2, 3], [4]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [9, 4, 3, 3], 10
attendu = [[9], [4, 3, 3]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [9, 9, 1, 1], 10
attendu = [[9, 1], [9, 1]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [9, 9, 9, 1], 10
attendu = [[9, 1], [9], [9]]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [5] * 10 + [1] * 10, 6
attendu = [[5, 1] for _ in range(10)]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [5] * 10 + [1] * 10, 5
attendu = [[5] for _ in range(10)] + [[1] * 5 for _ in range(2)]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"
poids_objets, poids_max = [1] * 10 + [5] * 10, 5
attendu = [[1] * 5 for _ in range(2)] + [[5] for _ in range(10)]
assert rangement(poids_objets, poids_max) == attendu, f"Erreur avec {poids_objets = } et {poids_max = }"