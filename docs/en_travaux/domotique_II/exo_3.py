# --------- PYODIDE:env --------- #
import p5
class Lampe:
    def __init__(self, nom, zone, pl):
        self.nom = nom
        self.zone = zone
        self.point_lumineux = pl
        self.etat_allume = False


    def allume(self):
        self.etat_allume = True
    
    def eteint(self):
        self.etat_allume = False

    def allume_si_zone(self, zone):
        if zone == self.zone:
            self.etat_allume = True

    def adresse(self):
        return (self.zone, self.point_lumineux)

eclairages = [Lampe("Salon1",0 ,0),
            Lampe("Salon2",0 ,1),    
            Lampe("Cuisine1",1 ,0),
            Lampe("Cuisine2",1 ,1),
            Lampe("Couloir",2 ,0),
            Lampe("SdB",3 ,0),
            Lampe("Chambre",4 ,0)]

def extinction_generale(lampes):
    for lampe in lampes:
        lampe.eteint()

# --------- PYODIDE:code --------- #
def allumer_groupe(lampes, groupe):
    ...


# --------- PYODIDE:corr --------- #
def allumer_groupe(lampes, groupe):
    for lampe in lampes:
        if lampe.adresse() in groupe:
            lampe.allume()




# --------- PYODIDE:tests --------- #
groupe1 = [(2, 0), (3, 0)]
allumer_groupe(eclairages, groupe1)

assert eclairages[4].etat_allume
assert eclairages[5].etat_allume

# --------- PYODIDE:secrets --------- #
groupe2 = [(0,0), (0, 1)]
eclairage_test = [Lampe("Salon1",0 ,0),
            Lampe("Salon2",0 ,1),    
            Lampe("Cuisine1",1 ,0),
            Lampe("Cuisine2",1 ,1),
            Lampe("Couloir",2 ,0),
            Lampe("SdB",3 ,0),
            Lampe("Chambre",4 ,0)]

allumer_groupe(eclairage_test, groupe2)

assert eclairage_test[0].etat_allume
assert eclairage_test[1].etat_allume

extinction_generale(eclairage_test)

groupe3 = [(0,0), (0, 1), (1,0), (1, 1), (2, 0), (3, 0), (4, 0)  ]
allumer_groupe(eclairage_test, groupe3)
for i in range(7):
    assert eclairage_test[i].etat_allume

# --------- PYODIDE:post --------- #
class App:
    def __init__(self):
        p5.run(self.setup,self.draw, target="cible_3")

    def draw(self):
        nb = 0
        for L in eclairages:
            if L.etat_allume:
                p5.fill(255,255,0)
            else:
                p5.fill(0)
            x = 20+nb*60
            x_texte = x - len(L.nom)
            p5.circle(x, 50, 30)
            p5.stroke(0)
            p5.textSize(13)
            p5.strokeWeight(1)
            p5.fill(0)
            p5.text(L.nom, x-20,95)
            print(L.nom)
            nb +=1
        p5.noLoop()

    def setup(self):
        """
        Fonction setup nécessaire au module p5.
        Définit tous les paramètres nécessaires à l'affichage et affiche 
        """
        self.i = 0
        p5.createCanvas(600, 200)
        p5.textFont("Calibri")
        p5.textSize(40)


        # AFFICHAGES PRÉLIMINAIRES (ou définifs s'il n'y a pas d'animation)
        p5.stroke(0)
        p5.textSize(22)
        p5.strokeWeight(1)
        
app = App()
