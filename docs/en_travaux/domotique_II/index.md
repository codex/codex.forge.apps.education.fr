---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: La domotique (II)
tags:
    - en travaux
    - programmation orientée objet
difficulty: 100
maj: 31/03/2024
---

On souhaite créer une application domotique afin de pouvoir contrôler l'éclairage dans les différentes pièce d'une maison.

Cet exercice utilise la classe `Lampe` permettant de contrôler une lampe.

Chaque lampe est désignée par un nom (chaîne de caractères), mais aussi par une adresse, composée d'un numéro de zone (entier compris entre 0 et 20) et un numéro de point lumineux (entier compris entre 0 et 20).

La variable booléenne `etat_allume` permet de faire basculer la lampe d'un état éteint (valeur `False`) à un éteint allumé  (valeur `True`), et inversement. Par défaut, la lampe est éteinte.
 
??? info "La classes `Lampe`"

	La classe `Lampe` est définie ainsi :
	
	```mermaid
	classDiagram
    class Lampe {
        __init__()
        str denomination
        int zone
        int point_lumineux
        bool etat_allume
        adresse()
        allume()
        eteint()
        allume_si_zone()
        actionne_telerupteur() 
    }
    ```
La classe dispose notamment des méthodes :

* `adresse` qui renvoie un couple d'entiers, composé du numéro de zone et du numéro de point lumineux,
* `allume` qui allume la lampe,
* `allume_si_zone` qui prend en paramètre un entier correspondant à la zone souhaitée et allume la lampe si elle fait partie de la zone.

La liste `eclairages` contient les différents éclairages de la maison sous forme d'instances de la classe `Lampe`.

Les caractéristiques de ces éclairages sont les suivants :

|   Dénomination|           Zone           | Point lumineux | 
| :-----------: | :----------------------: | :------------: | 
|     `Salon1`     |            $0$        | $0$            |      
|     `Salon2`     |            $0$        | $1$            | 
|     `Cuisine1`   |            $1$        | $0$            |
|     `Cuisine2`   |            $1$        | $1$            |
|     `Couloir`    |            $2$        | $0$            |
|     `SdB`        |            $3$        | $0$            |
|     `Chambre`    |            $4$        | $0$            |


!!! note "Fonction `affiche`"
    Dans les questions suivantes, la fonction `affiche`, présente dans les tests, permet de visualiser l'état des lampes dans la fenêtre située sous la console.


??? question "Question 1"
    Écrire la procédure `extinction_generale` qui prend une liste d'instances de la classe `Lampe` et qui éteint toutes les lampes.

    ```pycon title=""
    >>> eclairage[4].allume()
    >>> eclairage[6].allume()
    >>> extinction_generale(eclairages)
    >>> eclairage[4].etat_allume
    False
    >>> eclairage[6].etat_allume
    False
    ```
    {{ IDE('exo_1')}}
    {{ figure('cible_1') }} 



??? question "Question 2"
    Écrire la procédure `allumer_zone` qui prend une liste d'instances de la classe `Lampe` et une zone de type entier et qui allume toutes les lampes situées dans la zone.

    ```pycon title=""
    >>> allumer_zone(eclairages, 0)
    >>> eclairage[0].etat_allume
    True
    >>> eclairage[1].etat_allume
    True
    ```

    {{ IDE('exo_2')}}
    {{ figure('cible_2') }} 

??? question "Question 3"
    Il est possible de définir des *groupes*, qui regroupent des lampes qui peuvent être situées dans des zones différentes.

    Un groupe est défini par une liste d'adresses, couples d'entiers composés du numéro de zone et du numéro du point lumineux.

    Écrire la procédure `allumer_groupe` qui prend une liste d'instances de la classe `Lampe` et une liste d'adresses et qui allume toutes les lampes du groupe.

    ```pycon title=""
    >>> groupe1 = [(2, 0), (3, 0)]
    >>> allumer_groupe(eclairages, groupe1)
    >>> eclairage[4].etat_allume
    True
    >>> eclairage[5].etat_allume
    True
    ```
    
    {{ IDE('exo_3')}}
    {{ figure('cible_3') }} 
