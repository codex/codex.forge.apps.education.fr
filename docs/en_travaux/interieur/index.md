---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Colorier l'intérieur
tags:
    - à trous
    - en travaux
    - grille
difficulty: 250
MAJ: 02/01/2025
---


{{ IDE('exo') }}

{{ figure("coloriage", admo_title="Coloriage" ) }}
