# --- PYODIDE:env --- #
from copy import deepcopy
import p5

COULEURS = [
    (0, 0, 0),
    (255, 0, 0),
    (0, 255, 0),
    (0, 0, 255),
    (255, 255, 255),
]

COULEUR_GRILLE = (100, 100, 100)

FPS = 40
TAILLE_CELLULE = 15


# Réglages du jeu
def dessine():
    largeur = len(grille[0])
    hauteur = len(grille)
    p5.strokeWeight(1)
    p5.stroke(*COULEUR_GRILLE)
    for i in range(hauteur):
        for j in range(largeur):
            couleur = COULEURS[grille[i][j]]
            p5.fill(*couleur)
            p5.rect(j * TAILLE_CELLULE, i * TAILLE_CELLULE, TAILLE_CELLULE, TAILLE_CELLULE)


def initialisation():
    hauteur = len(grille)
    largeur = len(grille[0])
    p5.createCanvas(largeur * TAILLE_CELLULE, hauteur * TAILLE_CELLULE)
    p5.frameRate(FPS)
    p5.noLoop()


# --- PYODIDE:code --- #
grille = [
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
    [4, 0, 0, 0, 4, 4, 0, 4, 4, 0, 4, 0, 0, 0],
    [4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0],
    [4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0],
    [4, 0, 4, 0, 4, 0, 0, 4, 4, 0, 4, 0, 4, 0],
    [4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0],
    [4, 0, 0, 0, 4, 4, 0, 4, 4, 0, 0, 0, 4, 0],
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
]


def colorie(grille, couleur):
    hauteur = len(grille)
    largeur = len(grille[0])
    for i in range(hauteur):
        intersections = 0
        zone_coloriee = False
        for j in range(largeur):
            if grille[i][j] == 4:
                zone_coloriee = False
                if intersections % 2 == 1:
                    grille[i][j] = couleur
            elif not zone_coloriee and grille[i][j] != 4:
                intersections += 1
                zone_coloriee = True
    return grille

grille = colorie(grille, 2)

# --- PYODIDE:tests --- #
assert True
# --- PYODIDE:secrets --- #
assert True


# --- PYODIDE:corr --- #
def colorie(grille, couleur):
    hauteur = len(grille)
    largeur = len(grille[0])
    for i in range(hauteur):
        dehors = True
        precedent = 4
        for j in range(largeur):
            if grille[i][j] == 4 and not dehors:
                grille[i][j] = couleur
            elif grille[i][j] != 4 and precedent == 4:
                dehors = not dehors
            precedent = grille[i][j]
    return grille


# --- PYODIDE:post --- #
p5.run(initialisation, dessine, target="coloriage")
