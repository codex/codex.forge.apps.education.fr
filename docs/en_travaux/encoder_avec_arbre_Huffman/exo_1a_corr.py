def lettre_binaire(arbre, codage="", codages_alphabet  =None):
    if codages_alphabet  == None:
        codages_alphabet  ={}
    if arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        codages_alphabet [caractere] = codage
    else:
        gauche = arbre.sous_arbre_gauche()
        droit = arbre.sous_arbre_droit()
        lettre_binaire(gauche, codage + "0",codages_alphabet )
        lettre_binaire(droit, codage + "1",codages_alphabet )
    return codages_alphabet 
