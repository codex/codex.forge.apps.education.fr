# --- hdr, 1, 2, 3 --- #
class ArbreBinaire():
    def __init__(self, *args):
        if len(args) == 0:
            self.__content = None
        elif len(args) != 3:
            raise ArbreBinaireError("mauvais nombre d'arguments pour construire un arbre binaire")
        elif not isinstance(args[1], ArbreBinaire) or not isinstance(args[2], ArbreBinaire):
            raise ArbreBinaireError("mauvais type d'argument pour construire un arbre binaire")
        else:
            self.__content = [args[0], args[1], args[2]]

    def est_vide(self):
        return self.__content is None
        
    def donnee(self):
        """renvoie la donnée de la racine de l'arbre binaire, si non vide"""
        try:
            return self.__content[0]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def set_donnee(self, x):
        try:
            self.__content[0] = x
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def sous_arbre_gauche(self):
        try:
            return self.__content[1]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  sous arbre gauche")

    def sous_arbre_droit(self):
        try:
            return self.__content[2]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  right subtree")
            
    def __str__(self):
        if self.est_vide():
            return '( )'
        else:
            repr_left = str(self.sous_arbre_gauche())
            repr_right = str(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(str(self.donnee()), repr_left, repr_right)

    def __repr__(self):
        if self.est_vide():
            return '(\u2205)'
        else:
            repr_left = repr(self.sous_arbre_gauche())
            repr_right = repr(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(repr(self.donnee()), repr_left, repr_right)

    def est_feuille(self):
        '''
        prédicat pour tester si un arbre est une feuille ou non, i.e. un arbre de taille 1.
        '''
        return (not self.est_vide() and 
                self.sous_arbre_gauche().est_vide() and
                self.sous_arbre_droit().est_vide())
    
    def __eq__(self, arbre):
        if self.est_vide() and arbre.est_vide():
            return True
        elif (not self.est_vide() and arbre.est_vide()) or \
             (self.est_vide() and not arbre.est_vide()) or \
             self.donnee() != arbre.donnee():
            return False
        else:
            return self.sous_arbre_gauche() == arbre.sous_arbre_gauche() and \
                   self.sous_arbre_droit() == arbre.sous_arbre_droit()
        
    def affiche(self):
        if self.est_vide():
            print (" ")
        else:
            print( sous_arbre_affiche(self)[0])
      
def feuille(data) :
    return ArbreBinaire(data, ArbreBinaire(), ArbreBinaire())

def sous_arbre_affiche(ss_arbre):
    def etiquette(arbre):
        return arbre.donnee()["caractere"] + ":" + str(arbre.donnee()["poids"])
    chaine, deb, fin, larg = "", 0, 0 ,0
    if ss_arbre.est_vide():
        return "", 0, 0, 0
    elif ss_arbre.sous_arbre_gauche().est_vide() and ss_arbre.sous_arbre_droit().est_vide():
        return repr(etiquette(ss_arbre)), 0, len(repr(etiquette(ss_arbre))), len(repr(etiquette(ss_arbre)))
    else:
        racG = racD = lienG1 = lienG2 = lienD1 = lienD2 = chaineG =chaineD = ""
        largG = largD = 0
        if not ss_arbre.sous_arbre_gauche().est_vide():
            chaineG, debG, finG, largG = sous_arbre_affiche(ss_arbre.sous_arbre_gauche())
            lienG1 = " " * (finG+1) + "_" * (largG-finG-1)
            lienG2 = " " * finG + "/" + " " * (largG-finG-1)
            racG = " " * (largG == finG)
        if not ss_arbre.sous_arbre_droit().est_vide():
            chaineD, debD, finD, largD = sous_arbre_affiche(ss_arbre.sous_arbre_droit())
            lienD1 = "_" * (debD - 1) + " " * (largD-debD + 1)
            lienD2 = " " * (debD - 1) + chr(92) + " " * (largD - debD)
            racD = " " * (debD == 0)
        # crée la partie haute du bloc
        # comportant la racine et les branches vers les sous arbres
        larg  = len(lienG1) + len((repr(etiquette(ss_arbre)))) + len(lienD1)
        deb = len(lienG1)
        fin = len(lienG1) + len((repr(etiquette(ss_arbre))))
        chaine = lienG1 + repr(etiquette(ss_arbre)) + lienD1 
        chaine = chaine + "\n" + lienG2 + " " * len(repr(etiquette(ss_arbre))) + lienD2 
        #complete la chaine gauche et la chaine droite pour qu'elles aient le même
        # nombre de lignes
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        complete = " " * (len((repr(etiquette(ss_arbre)))))
        completeG = "\n" + " " * (largG)
        completeD = "\n" + " " * (largD)
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        chaineG = chaineG + completeG * (lenD-lenG)
        chaineD = chaineD + completeD * (lenG-lenD)
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        
        # forme le nouveau bloc ave la racine et les branches et les sous arbre
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        for i in range(lenG):
            chaine = chaine + "\n" + liste_chaineG[i] + racG + complete + racD + liste_chaineD[i]
    return chaine, deb, fin, larg

class Liste():
    def __init__(self, valeur = None, suivant = None):
        self.valeur = valeur
        self.suivant = suivant
        
    def __str__(self):
        """affiche les valeurs de la liste. La première valeur affichée est
        la dernière valeur ajoutée à la liste
        
        Exemples :
        
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> L.ajoute_en_tete("B")
        >>> L.ajoute_en_tete("C")
        >>> print(L)
        C, B, A
        
        """
        chaine = ""
        if self.valeur is not None:
            chaine = chaine + str(self.valeur)
            if self.suivant is not None and self.suivant.valeur is not None:
                chaine = chaine + ", " + str(self.suivant)
        return chaine
    
    def longueur(self):
        """Retourne la longueur de la liste

        Exemple :
        >>> L = Liste()
        >>> L.longueur()
        0
        >>> L.ajoute_en_tete("A")
        >>> L.longueur()
        1
        
        """
        if self.valeur is None:
            return 0
        else:
            return 1 + self.suivant.longueur()
    
    def est_vide(self):
        """Renvoie True est vide, False sinon
        
        Exemple :
        >>> L = Liste()
        >>> L.est_vide()
        True
        >>> L.ajoute_en_tete("A")
        >>> L.est_vide()
        False
        
        """
        return self.suivant is None

    def get_tete(self):
        """Renvoie l'élément en tête de liste sans le supprimer
           Renvoie None si la liste est vide.
        """
        if not self.est_vide():
            return self.valeur
        else:
            return None

    def ajoute_en_tete(self,x):
        """ajoute l'élément x en tête de liste
        
        Exemple :
        
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> print(L)
        A
        
        """
        suiv, val = self.suivant, self.valeur
        self.suivant = Liste()
        self.suivant.suivant, self.suivant.valeur = suiv, val
        self.valeur = x

    def ajoute_en_queue(self,x):
        """Ajoute un élément en queue de liste
        
        Exemple :
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> L.ajoute_en_queue("B")
        >>> print(L)
        A, B
        

        """
        if self.valeur is not None:
            self.suivant.ajoute_en_queue(x)
        else:
            self.valeur = x
            self.suivant = Liste()

    def supprime_tete(self):
        """Supprime l'élément en tête de liste et le renvoie
            Renvoie None si la liste est vide.
        """
        if self.valeur is not None:
            value = self.valeur
            self.valeur = self.suivant.valeur
            self.suivant = self.suivant.suivant
        else:
            value = None
        return value
    
def cons(x,L):
    """Construis une nouvelle liste à partir de la liste L
    
    """
    newL = Liste()
    newL.valeur = x
    newL.suivant = L
    return newL
        
class Pile(Liste):
    """ définit une pile vide"""
    def __init__(self):
        """Crée une pile vide"""
        Liste.__init__(self)
        self.longueur = 0
    
    def sommet(self):
        """renvoie le sommet de la pile sans le retirer"""
        return self.get_tete()
    
    def empile(self, x):
        """empile l'élément x au sommet de la pile"""
        self.ajoute_en_tete(x)
        self.longueur += 1
        
    def depile(self):
        """supprime l'élément au sommet de la pile et renvoie sa valeur"""
        if not self.est_vide():
            self.longueur -= 1
        return self.supprime_tete()
    
    def sommet(self):
        if not self.est_vide():
            return self.get_tete()
    
    def __len__(self):
        return self.longueur
 
    def __str__(self):
        """renvoie une représentation de la pile sous forme de chaine de caracteres"""
        if not self.est_vide():
            bord = "-"* (len(Liste.__str__(self))+6) + "\n"
            chaine = "<-> " + Liste.__str__(self) + " |\n"
        else:
            bord = "-"* 6 + "\n"
            chaine = "<->  |\n"
            
        return bord + chaine + bord

class File(object):
    """ définit une file vide"""
    def __init__(self, valeur = None, suivant = None):
        """Crée une file vide"""
        self._entree = Pile()
        self._sortie = Pile()
        self.longueur = 0
        
    def est_vide(self):
        """renvoie True si la file est vide, False sinon"""
        return self._entree.est_vide() and self._sortie.est_vide()
    
    def enfile(self, x):
        """enfile l'élément x"""
        self._entree.empile(x)
        self.longueur += 1
        
    def defile(self):
        """defile l'élément x"""
        if self._sortie.est_vide():
            if self._entree.est_vide():
                return None
            else:
                while not self._entree.est_vide():
                    self._sortie.empile(self._entree.depile())
                if not self.est_vide():
                        self.longueur -= 1
                return self._sortie.depile()
        if not self.est_vide():
            self.longueur -= 1
        return self._sortie.depile()
    
    def sommet(self):
        if self._sortie.est_vide():
            if self._entree.est_vide():
                return None
            else:
                while not self._entree.est_vide():
                    self._sortie.empile(self._entree.depile())
                
        return self._sortie.sommet()
    
    def __len__(self):
        return self.longueur
        
    def __str__(self):
        """renvoie une représentation de la file sous forme de chaine de caracteres"""

        entree = File()
        if not self.est_vide():
            chaine = "<- | "
            while not self.est_vide():
                val = self.defile()
                chaine = chaine + str(val) + " | "
                entree.enfile(val)
            while not entree.est_vide():
                val = entree.defile()
                self.enfile(val)
            bord = "-"* (len(chaine)+2) + "\n"
            
            return bord + chaine + "<-\n" + bord
        else:
            return str(None)
class ArbreBinaireError(Exception):
    def __init__(self, msg):
        self.message = msg

class ArbreBinaire():
    def __init__(self, *args):
        if len(args) == 0:
            self.__content = None
        elif len(args) != 3:
            raise ArbreBinaireError("mauvais nombre d'arguments pour construire un arbre binaire")
        elif not isinstance(args[1], ArbreBinaire) or not isinstance(args[2], ArbreBinaire):
            raise ArbreBinaireError("mauvais type d'argument pour construire un arbre binaire")
        else:
            self.__content = [args[0], args[1], args[2]]

    def est_vide(self):
        return self.__content is None
        
    def donnee(self):
        """renvoie la donnée de la racine de l'arbre binaire, si non vide"""
        try:
            return self.__content[0]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def set_donnee(self, x):
        try:
            self.__content[0] = x
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  racine")

    def sous_arbre_gauche(self):
        try:
            return self.__content[1]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  sous arbre gauche")

    def sous_arbre_droit(self):
        try:
            return self.__content[2]
        except TypeError:
            raise ArbreBinaireError("un arbre vide n'a pas de  right subtree")
            
    def __str__(self):
        if self.est_vide():
            return '( )'
        else:
            repr_left = str(self.sous_arbre_gauche())
            repr_right = str(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(str(self.donnee()), repr_left, repr_right)

    def __repr__(self):
        if self.est_vide():
            return '(\u2205)'
        else:
            repr_left = repr(self.sous_arbre_gauche())
            repr_right = repr(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(repr(self.donnee()), repr_left, repr_right)

    def est_feuille(self):
        '''
        prédicat pour tester si un arbre est une feuille ou non, i.e. un arbre de taille 1.
        '''
        return (not self.est_vide() and 
                self.sous_arbre_gauche().est_vide() and
                self.sous_arbre_droit().est_vide())
    
    def __eq__(self, arbre):
        if self.est_vide() and arbre.est_vide():
            return True
        elif (not self.est_vide() and arbre.est_vide()) or \
             (self.est_vide() and not arbre.est_vide()) or \
             self.donnee() != arbre.donnee():
            return False
        else:
            return self.sous_arbre_gauche() == arbre.sous_arbre_gauche() and \
                   self.sous_arbre_droit() == arbre.sous_arbre_droit()
        
    def affiche(self):
        if self.est_vide():
            print (" ")
        else:
            print( sous_arbre_affiche(self)[0])
      
def feuille(data) :
    return ArbreBinaire(data, ArbreBinaire(), ArbreBinaire())

def sous_arbre_affiche(ss_arbre):
    def etiquette(arbre):
        return arbre.donnee()["caractere"] + ":" + str(arbre.donnee()["poids"])
    chaine, deb, fin, larg = "", 0, 0 ,0
    if ss_arbre.est_vide():
        return "", 0, 0, 0
    elif ss_arbre.sous_arbre_gauche().est_vide() and ss_arbre.sous_arbre_droit().est_vide():
        return repr(etiquette(ss_arbre)), 0, len(repr(etiquette(ss_arbre))), len(repr(etiquette(ss_arbre)))
    else:
        racG = racD = lienG1 = lienG2 = lienD1 = lienD2 = chaineG =chaineD = ""
        largG = largD = 0
        if not ss_arbre.sous_arbre_gauche().est_vide():
            chaineG, debG, finG, largG = sous_arbre_affiche(ss_arbre.sous_arbre_gauche())
            lienG1 = " " * (finG+1) + "_" * (largG-finG-1)
            lienG2 = " " * finG + "/" + " " * (largG-finG-1)
            racG = " " * (largG == finG)
        if not ss_arbre.sous_arbre_droit().est_vide():
            chaineD, debD, finD, largD = sous_arbre_affiche(ss_arbre.sous_arbre_droit())
            lienD1 = "_" * (debD - 1) + " " * (largD-debD + 1)
            lienD2 = " " * (debD - 1) + chr(92) + " " * (largD - debD)
            racD = " " * (debD == 0)
        # crée la partie haute du bloc
        # comportant la racine et les branches vers les sous arbres
        larg  = len(lienG1) + len((repr(etiquette(ss_arbre)))) + len(lienD1)
        deb = len(lienG1)
        fin = len(lienG1) + len((repr(etiquette(ss_arbre))))
        chaine = lienG1 + repr(etiquette(ss_arbre)) + lienD1 
        chaine = chaine + "\n" + lienG2 + " " * len(repr(etiquette(ss_arbre))) + lienD2 
        #complete la chaine gauche et la chaine droite pour qu'elles aient le même
        # nombre de lignes
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        complete = " " * (len((repr(etiquette(ss_arbre)))))
        completeG = "\n" + " " * (largG)
        completeD = "\n" + " " * (largD)
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        chaineG = chaineG + completeG * (lenD-lenG)
        chaineD = chaineD + completeD * (lenG-lenD)
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        
        # forme le nouveau bloc ave la racine et les branches et les sous arbre
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        for i in range(lenG):
            chaine = chaine + "\n" + liste_chaineG[i] + racG + complete + racD + liste_chaineD[i]
    return chaine, deb, fin, larg

class Liste():
    def __init__(self, valeur = None, suivant = None):
        self.valeur = valeur
        self.suivant = suivant
        
    def __str__(self):
        """affiche les valeurs de la liste. La première valeur affichée est
        la dernière valeur ajoutée à la liste
        
        Exemples :
        
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> L.ajoute_en_tete("B")
        >>> L.ajoute_en_tete("C")
        >>> print(L)
        C, B, A
        
        """
        chaine = ""
        if self.valeur is not None:
            chaine = chaine + str(self.valeur)
            if self.suivant is not None and self.suivant.valeur is not None:
                chaine = chaine + ", " + str(self.suivant)
        return chaine
    
    def longueur(self):
        """Retourne la longueur de la liste

        Exemple :
        >>> L = Liste()
        >>> L.longueur()
        0
        >>> L.ajoute_en_tete("A")
        >>> L.longueur()
        1
        
        """
        if self.valeur is None:
            return 0
        else:
            return 1 + self.suivant.longueur()
    
    def est_vide(self):
        """Renvoie True est vide, False sinon
        
        Exemple :
        >>> L = Liste()
        >>> L.est_vide()
        True
        >>> L.ajoute_en_tete("A")
        >>> L.est_vide()
        False
        
        """
        return self.suivant is None

    def get_tete(self):
        """Renvoie l'élément en tête de liste sans le supprimer
           Renvoie None si la liste est vide.
        """
        if not self.est_vide():
            return self.valeur
        else:
            return None

    def ajoute_en_tete(self,x):
        """ajoute l'élément x en tête de liste
        
        Exemple :
        
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> print(L)
        A
        
        """
        suiv, val = self.suivant, self.valeur
        self.suivant = Liste()
        self.suivant.suivant, self.suivant.valeur = suiv, val
        self.valeur = x

    def ajoute_en_queue(self,x):
        """Ajoute un élément en queue de liste
        
        Exemple :
        >>> L = Liste()
        >>> L.ajoute_en_tete("A")
        >>> L.ajoute_en_queue("B")
        >>> print(L)
        A, B
        

        """
        if self.valeur is not None:
            self.suivant.ajoute_en_queue(x)
        else:
            self.valeur = x
            self.suivant = Liste()
  
    def supprime_tete(self):
        """Supprime l'élément en tête de liste et le renvoie
            Renvoie None si la liste est vide.
        """
        if self.valeur is not None:
            value = self.valeur
            self.valeur = self.suivant.valeur
            self.suivant = self.suivant.suivant
        else:
            value = None
        return value
    
def cons(x,L):
    """Construis une nouvelle liste à partir de la liste L
    
    """
    newL = Liste()
    newL.valeur = x
    newL.suivant = L
    return newL
        
class Pile(Liste):
    """ définit une pile vide"""
    def __init__(self):
        """Crée une pile vide"""
        Liste.__init__(self)
        self.longueur = 0
    
    def sommet(self):
        """renvoie le sommet de la pile sans le retirer"""
        return self.get_tete()
    
    def empile(self, x):
        """empile l'élément x au sommet de la pile"""
        self.ajoute_en_tete(x)
        self.longueur += 1
        
    def depile(self):
        """supprime l'élément au sommet de la pile et renvoie sa valeur"""
        if not self.est_vide():
            self.longueur -= 1
        return self.supprime_tete()
    
    def sommet(self):
        if not self.est_vide():
            return self.get_tete()
    
    def __len__(self):
        return self.longueur
 
    
    def __str__(self):
        """renvoie une représentation de la pile sous forme de chaine de caracteres"""
        if not self.est_vide():
            bord = "-"* (len(Liste.__str__(self))+6) + "\n"
            chaine = "<-> " + Liste.__str__(self) + " |\n"
        else:
            bord = "-"* 6 + "\n"
            chaine = "<->  |\n"
            
        return bord + chaine + bord

class File(object):
    """ définit une file vide"""
    def __init__(self, valeur = None, suivant = None):
        """Crée une file vide"""
        self._entree = Pile()
        self._sortie = Pile()
        self.longueur = 0
        
    def est_vide(self):
        """renvoie True si la file est vide, False sinon"""
        return self._entree.est_vide() and self._sortie.est_vide()
    
    def enfile(self, x):
        """enfile l'élément x"""
        self._entree.empile(x)
        self.longueur += 1
        
    def defile(self):
        """defile l'élément x"""
        if self._sortie.est_vide():
            if self._entree.est_vide():
                return None
            else:
                while not self._entree.est_vide():
                    self._sortie.empile(self._entree.depile())
                if not self.est_vide():
                        self.longueur -= 1
                return self._sortie.depile()
        if not self.est_vide():
            self.longueur -= 1
        return self._sortie.depile()
    
    def sommet(self):
        if self._sortie.est_vide():
            if self._entree.est_vide():
                return None
            else:
                while not self._entree.est_vide():
                    self._sortie.empile(self._entree.depile())
                
        return self._sortie.sommet()
    
    def __len__(self):
        return self.longueur
        
    def __str__(self):
        """renvoie une représentation de la file sous forme de chaine de caracteres"""

        entree = File()
        if not self.est_vide():
            chaine = "<- | "
            while not self.est_vide():
                val = self.defile()
                chaine = chaine + str(val) + " | "
                entree.enfile(val)
            while not entree.est_vide():
                val = entree.defile()
                self.enfile(val)
            bord = "-"* (len(chaine)+2) + "\n"
            
            return bord + chaine + "<-\n" + bord
        else:
            return str(None)

def compte_occurrences(chaine):
    occurrences = {}
    for caractere in chaine:
        if caractere in occurrences:
            occurrences[caractere] = occurrences[caractere] + 1
        else:
            occurrences[caractere] = 1
    return occurrences

def insere(caractere, poids, occurrences_triees):
    occurrences_triees.append({'caractere':caractere,
                               'poids':poids})
    pos = len(occurrences_triees)-1
    while pos>0 and occurrences_triees[pos]['poids'] < occurrences_triees[pos-1]['poids']:
        occurrences_triees[pos], occurrences_triees[pos-1] = occurrences_triees[pos-1], occurrences_triees[pos]
        pos = pos - 1

def trie_occurrences(occurrences):
    occurrences_triees = []
    for caractere in occurrences:
        poids = occurrences[caractere]
        insere(caractere, poids, occurrences_triees)
    return occurrences_triees
   
def construit_file_caracteres(occurrences_triees):
    file = File()
    for noeud in occurrences_triees:
        file.enfile(feuille(noeud))
    return file

def plus_leger(file1, file2):
    if file1.est_vide():
        return file2.defile()
    elif file2.est_vide():
        return file1.defile()
    elif file1.sommet().donnee()['poids'] <= file2.sommet().donnee()['poids']:
        return file1.defile()
    else:
        return file2.defile()

def construit_arbre(file_caracteres):
    file_fusionnes = File()
    while len(file_caracteres) + len(file_fusionnes)>1:
        noeud1 = plus_leger(file_caracteres, file_fusionnes)
        noeud2 = plus_leger(file_caracteres, file_fusionnes)
        donnee = {'caractere': "", 'poids' : noeud1.donnee()['poids'] + noeud2.donnee()['poids']}
        arbre = ArbreBinaire(donnee, noeud1, noeud2)
        file_fusionnes.enfile(arbre)
    if not file_caracteres.est_vide():
        return file_caracteres.defile()
    return file_fusionnes.defile()        

def arbre_Huffman(chaine):
    occurrences = trie_occurrences(compte_occurrences(chaine))
    file_caracteres = construit_file_caracteres(occurrences)
    arbre = construit_arbre(file_caracteres)
    return arbre

# --- vide, 1 --- #
def lettre_binaire(arbre):
    ...

# --- exo, 1 --- #
''' # skip
def lettre_binaire(arbre, code="", codes =None):
    if codes == None:
        codes ={}
    if arbre.est_feuille():
        caractere = arbre.donnee()[...]
        codes[caractere] = ...
    else:
        gauche = arbre.sous_arbre_gauche()
        droit = arbre.sous_arbre_droit()
        lettre_binaire(gauche, code + ...,codes)
        lettre_binaire(droit, code + ...,codes)
    return ...
''' # skip
# --- corr, 1 | hdr, 2, 3 --- #
def lettre_binaire(arbre, code="", codes =None):
    if codes == None and arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        return {caractere:'0'}
    if codes == None:
        codes ={}
    if arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        codes[caractere] = code
    else:
        gauche = arbre.sous_arbre_gauche()
        droit = arbre.sous_arbre_droit()
        lettre_binaire(gauche, code + "0",codes)
        lettre_binaire(droit, code + "1",codes)
    return codes
# --- tests, 1 --- #
arbre = arbre_Huffman("que voulez vous que je vous dise")
codage = lettre_binaire(arbre)
assert codage == {' ': '111', 'e': '110', 'u': '101', 'd': '10011', 'j': '10010', 'z': '10001', 'l': '10000', 'q': '0111', 'i': '0110', 's': '010', 'o': '001', 'v': '000'}
# --- rem, 1 --- #
''' # skip
Une version avec un parcours en largeur, qui implique d'enfiler le code courant avec les futurs nœuds à visiter.

```python
def lettre_binaire(arbre):
    codes = {}
    code = ""
    if arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        return {caractere:'0'}
    file = File()
    file.enfile((arbre, code))
    while not file.est_vide():
        racine,code = file.defile()
        if racine.est_feuille():
            caractere = racine.donnee()["caractere"]
            codes[caractere] = code
        else:
            gauche = racine.sous_arbre_gauche()
            droit = racine.sous_arbre_droit()
            file.enfile((gauche, code + "0"))
            file.enfile((droit, code + "1"))
    return codes 
```
''' # skip

# --- exo, 2 --- #
def encode_texte_codage(texte, codage):
    ...
# --- corr, 2 | hdr, 3 --- #
def encode_texte_codage(texte, codage):
    texte_code = list()
    for caractere in texte:
        texte_code.append(codage[caractere])
        print(texte_code)
    return "".join(texte_code)
# --- tests, 2 --- #
codage == {' ': '111', 'e': '110', 'u': '101', 'd': '10011', 'j': '10010', 'z': '10001', 'l': '10000', 'q': '0111', 'i': '0110', 's': '010', 'o': '001', 'v': '000'}
texte_code = encode_texte_codage("que voulez vous que je vous dise", codage)
assert texte_code == "01111011101110000011011000011010001111000001101010111011110111011110010110111000001101010111100110110010110"

codage == {' ': '111'}
texte_code = encode_texte_codage("   ", codage)
assert texte_code == "111111111"

# --- exo, 3 --- #
def encode_Huffman(chaine):
    ...
# --- corr, 3 --- #
def encode_Huffman(chaine):
    arbre = arbre_Huffman(chaine)
    dico_encodage = lettre_binaire(arbre)
    chaine_encodee = encode_texte_codage(chaine, dico_encodage)
    return chaine_encodee

# --- tests, 3 --- #
texte_code = encode_Huffman("que voulez vous que je vous dise")
assert texte_code == "01111011101110000011011000011010001111000001101010111011110111011110010110111000001101010111100110110010110"

texte_code = encode_Huffman("aaaaaaaaab")
assert texte_code == "1111111110"

# --- secrets, 3 --- #
from random import randrange
numero = randrange(32,125)
car1 = chr(numero)
car2 = chr(numero + 1)
longueur = randrange(1,1000)
texte_code = encode_Huffman(car1*longueur + car2*(longueur//2))
assert texte_code == "1"*longueur+"0"*(longueur//2)
