

# --------- PYODIDE:env --------- #

class Arbre():
    def __init__(self, *args):
        if len(args) == 1:
            self.__content = [args[0]]
        else:
            self.__content = [args[0], args[1], args[2]]

    def donnee(self):
        """renvoie la donnée de la racine de l'arbre binaire, si non vide"""
        return self.__content[0]

    def sous_arbre_gauche(self):
        return self.__content[1]

    def sous_arbre_droit(self):
        return self.__content[2]

    def __str__(self):
        if self.est_feuille():
            return str(self.donnee())
        else:
            repr_left = str(self.sous_arbre_gauche())
            repr_right = str(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(str(self.donnee()), repr_left, repr_right)

    def __repr__(self):
        if len(self.__content) == 1:
            return repr(self.donnee())
        else:
            repr_left = repr(self.sous_arbre_gauche())
            repr_right = repr(self.sous_arbre_droit())
            return '({:s}, {:s}, {:s})'.format(repr(self.donnee()), repr_left, repr_right)

    def est_feuille(self):
        '''
        prédicat pour tester si un arbre est une feuille ou non, i.e. un arbre de taille 1.
        '''
        return len(self.__content) == 1

    def __eq__(self, arbre):
        if  self.donnee() != arbre.donnee() :
            return False
        elif self.est_feuille() == arbre.est_feuille():
            return True
        elif ((self.est_feuille() and not arbre.est_feuille()) or
            (not self.est_feuille() and arbre.est_feuille())):
            return False
        else:
            return self.sous_arbre_gauche() == arbre.sous_arbre_gauche() and \
                   self.sous_arbre_droit() == arbre.sous_arbre_droit()

    def affiche(self):
        print( sous_arbre_affiche(self)[0])

def sous_arbre_affiche(ss_arbre):
    def etiquette(arbre):
        return arbre.donnee()["caractere"] + ":" + str(arbre.donnee()["poids"])
    chaine, deb, fin, larg = "", 0, 0 ,0
    if ss_arbre.est_feuille():
        return repr(etiquette(ss_arbre)), 0, len(repr(etiquette(ss_arbre))), len(repr(etiquette(ss_arbre)))
    else:
        racG = racD = lienG1 = lienG2 = lienD1 = lienD2 = chaineG =chaineD = ""
        largG = largD = 0

        chaineG, debG, finG, largG = sous_arbre_affiche(ss_arbre.sous_arbre_gauche())
        lienG1 = " " * (finG+1) + "_" * (largG-finG-1)
        lienG2 = " " * finG + "/" + " " * (largG-finG-1)
        racG = " " * (largG == finG)
        chaineD, debD, finD, largD = sous_arbre_affiche(ss_arbre.sous_arbre_droit())
        lienD1 = "_" * (debD - 1) + " " * (largD-debD + 1)
        lienD2 = " " * (debD - 1) + chr(92) + " " * (largD - debD)
        racD = " " * (debD == 0)

        # crée la partie haute du bloc
        # comportant la racine et les branches vers les sous arbres
        larg  = len(lienG1) + len((repr(etiquette(ss_arbre)))) + len(lienD1)
        deb = len(lienG1)
        fin = len(lienG1) + len((repr(etiquette(ss_arbre))))
        chaine = lienG1 + repr(etiquette(ss_arbre)) + lienD1
        chaine = chaine + "\n" + lienG2 + " " * len(repr(etiquette(ss_arbre))) + lienD2
        #complete la chaine gauche et la chaine droite pour qu'elles aient le même
        # nombre de lignes
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")
        complete = " " * (len((repr(etiquette(ss_arbre)))))
        completeG = "\n" + " " * (largG)
        completeD = "\n" + " " * (largD)
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        chaineG = chaineG + completeG * (lenD-lenG)
        chaineD = chaineD + completeD * (lenG-lenD)
        liste_chaineG = chaineG.split("\n")
        liste_chaineD = chaineD.split("\n")

        # forme le nouveau bloc ave la racine et les branches et les sous arbre
        lenG = len(liste_chaineG)
        lenD = len(liste_chaineD)
        for i in range(lenG):
            chaine = chaine + "\n" + liste_chaineG[i] + racG + complete + racD + liste_chaineD[i]
    return chaine, deb, fin, larg

class Element():
    def __init__(self, valeur, suivant = None,precedent = None):
        self.valeur = valeur
        self.suivant = suivant
        self.precedent = precedent

    def get_valeur(self):
        return self.valeur

    def get_suivant(self):
        return self.suivant

    def get_precedent(self):
        return self.precedent

    def set_precedent(self, elt):
        self.precedent = elt

class File():
    def __init__(self):
        self.entree = None
        self.sortie = None
        self.longueur = 0

    def est_vide(self):
        return self.sortie == None

    def enfile(self, val):
        if self.est_vide():
            self.entree = Element(val)
            self.sortie = self.entree
        else:
            ancien_debut = self.entree
            nouveau_debut = Element(val, suivant = ancien_debut)
            ancien_debut.set_precedent(nouveau_debut)
            self.entree = nouveau_debut
        self.longueur += 1

    def defile(self):
        assert not self.est_vide(), "On ne peut pas défiler une file vide."
        element = self.sortie
        precedent = element.get_precedent()
        self.sortie = precedent
        self.longueur -= 1
        return element.get_valeur()

    def sommet(self):
        return self.sortie.get_valeur()

    def __len__(self):
        return self.longueur

    def __str__(self):
        """renvoie un chaîne de caractères pour afficher une file"""
        chaine = ""
        file_temp = File()
        while not self.est_vide():
            elt = self.defiler()
            chaine = chaine  + " <- " + str(elt)
            file_temp.enfiler((elt))
        while not file_temp.est_vide():
            self.enfiler(file_temp.defiler())
        if not self.est_vide():
            chaine = chaine +" <-"
        return chaine

def compte_occurrences(chaine):
    occurrences = {}
    for caractere in chaine:
        if caractere in occurrences:
            occurrences[caractere] = occurrences[caractere] + 1
        else:
            occurrences[caractere] = 1
    return occurrences

def insere(caractere, poids, occurrences_triees):
    occurrences_triees.append({'caractere':caractere,
                               'poids':poids})
    pos = len(occurrences_triees)-1
    while pos>0 and occurrences_triees[pos]['poids'] < occurrences_triees[pos-1]['poids']:
        occurrences_triees[pos], occurrences_triees[pos-1] = occurrences_triees[pos-1], occurrences_triees[pos]
        pos = pos - 1

def trie_occurrences(occurrences):
    occurrences_triees = []
    for caractere in occurrences:
        poids = occurrences[caractere]
        insere(caractere, poids, occurrences_triees)
    return occurrences_triees

def construit_file_caracteres(occurrences_triees):
    file = File()
    for noeud in occurrences_triees:
        file.enfile(Arbre(noeud))
    return file

def plus_leger(file1, file2):
    if file1.est_vide():
        return file2.defile()
    elif file2.est_vide():
        return file1.defile()
    elif file1.sommet().donnee()['poids'] <= file2.sommet().donnee()['poids']:
        return file1.defile()
    else:
        return file2.defile()

def construit_arbre(file_caracteres):
    file_fusionnes = File()
    while len(file_caracteres) + len(file_fusionnes)>1:
        noeud1 = plus_leger(file_caracteres, file_fusionnes)
        noeud2 = plus_leger(file_caracteres, file_fusionnes)
        donnee = {'caractere': "", 'poids' : noeud1.donnee()['poids'] + noeud2.donnee()['poids']}
        arbre = Arbre(donnee, noeud1, noeud2)
        file_fusionnes.enfile(arbre)
    if not file_caracteres.est_vide():
        return file_caracteres.defile()
    return file_fusionnes.defile()

def arbre_Huffman(chaine):
    occurrences = trie_occurrences(compte_occurrences(chaine))
    file_caracteres = construit_file_caracteres(occurrences)
    arbre = construit_arbre(file_caracteres)
    return arbre

# --------- PYODIDE:code --------- #

def lettre_binaire(arbre, codage="", codages_alphabet = None):
    if codages_alphabet  == None:
        codages_alphabet  ={}
    if arbre.est_feuille():
        caractere = arbre.donnee()[...]
        codages_alphabet [caractere] = ...
    else:
        gauche = arbre.sous_arbre_gauche()
        droit = arbre.sous_arbre_droit()
        lettre_binaire(gauche, codage + ...,codages_alphabet )
        lettre_binaire(droit, codage + ...,codages_alphabet )
    return ...

# --------- PYODIDE:corr --------- #

def lettre_binaire(arbre, codage="", codages_alphabet  =None):
    if codages_alphabet  == None and arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        return {caractere:'0'}
    if codages_alphabet  == None:
        codages_alphabet  ={}
    if arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        codages_alphabet [caractere] = codage
    else:
        gauche = arbre.sous_arbre_gauche()
        droit = arbre.sous_arbre_droit()
        lettre_binaire(gauche, codage + "0",codages_alphabet )
        lettre_binaire(droit, codage + "1",codages_alphabet )
    return codages_alphabet 

# --------- PYODIDE:tests --------- #

arbre = arbre_Huffman("que voulez vous que je vous dise")
codage = lettre_binaire(arbre)
assert codage == {' ': '111', 'e': '110', 'u': '101', 'd': '10011', 'j': '10010', 'z': '10001', 'l': '10000', 'q': '0111', 'i': '0110', 's': '010', 'o': '001', 'v': '000'}

# --------- PYODIDE:secrets --------- #

# No secret tests...?
