# tests
arbre = arbre_Huffman("que voulez vous que je vous dise")
codage = lettre_binaire(arbre)
assert codage == {' ': '111', 'e': '110', 'u': '101', 'd': '10011', 'j': '10010', 'z': '10001', 'l': '10000', 'q': '0111', 'i': '0110', 's': '010', 'o': '001', 'v': '000'}
