Une version avec un parcours en largeur, qui implique d'enfiler le code courant avec les futurs nœuds à visiter.

```python
def lettre_binaire(arbre):
    codages_alphabet  = {}
    codage = ""
    if arbre.est_feuille():
        caractere = arbre.donnee()["caractere"]
        return {caractere:'0'}
    file = File()
    file.enfile((arbre, codage))
    while not file.est_vide():
        racine,codage = file.defile()
        if racine.est_feuille():
            caractere = racine.donnee()["caractere"]
            codages_alphabet [caractere] = codage
        else:
            gauche = racine.sous_arbre_gauche()
            droit = racine.sous_arbre_droit()
            file.enfile((gauche, codage + "0"))
            file.enfile((droit, codage + "1"))
    return codages_alphabet  
```

