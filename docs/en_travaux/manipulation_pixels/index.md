---
author:
    - Romain Janvier
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Manipulation de pixels
tags:
    - à trous
    - en travaux
    - grille
    - ep2
---

On considère une image en 256 niveaux de gris que l'on représente par une grille de nombres, c'est-à-dire une liste composée de sous-listes toutes de longueurs identiques.

La largeur de l'image est donc la longueur d'une sous-liste et la hauteur de l'image est le nombre de sous-listes. On donne d'ailleurs les deux fonctions `largeur` et `hauteur` définies ci-dessous :

!!! note "largeur et hauteur"

    ```python
    def largeur(image):
        return len(image[0])
    
    def hauteur(image):
        return len(image)
    ```

Chaque sous-liste représente une ligne de l'image et chaque élément des sous-listes est un entier compris entre 0 et 255, représentant l'intensité lumineuse du pixel.

On s'intéresse à deux opérations :

- Le négatif d'une image est l'image constituée des pixels `x_n` tels que `x_n + x_i = 255` où `x_i` est le pixel correspondant de l'image initiale.
- La _binarisation_ d'une image qui consiste à remplacer tous les pixels par des 0 ou des 1 en fonction la position de la valeur initiale par rapport à un seuil donné.

???+ example "Exemples"

    ```pycon title=""
    >>> image_1 = [[20, 34, 254, 145, 6], 
    ...            [23, 124, 217, 225, 69], 
    ...            [197, 174, 207, 25, 87], 
    ...            [255, 0, 24, 197, 189]]
    >>> hauteur(image_1)
    4
    >>> largeur(image_1)
    5
    >>> negatif(image_1)
    [[235, 221, 1, 110, 249], [232, 131, 38, 30, 186], [58, 81, 48, 230, 168], [0, 255, 231, 58, 66]]
    >>> binaire(image_1, 120)
    [[0, 0, 1, 1, 0], [0, 1, 1, 1, 0], [1, 1, 1, 0, 0], [1, 0, 0, 1, 1]]
    ```

Compléter le code ci-dessous :

{{ IDE('exo') }}
