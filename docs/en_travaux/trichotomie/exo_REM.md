On procède de façon analogue à la recherche dichotomique.

Dans cette démarche, on divise la largeur de la zone de recherche par 3 à chaque itération. On pourrait donc penser que cette méthode est plus efficace que la recherche dichotomique qui, faut-il le rappeler, divise la zone de recherche par 2 à chaque étape.

Ce gain a toutefois un coût : la recherche trichotomique effectue en moyenne plus de comparaisons que la dichotomie classique. Dans le cas d'une recherche dans un tableau d'entiers, cela a peu d'importance. Mais si l'opération de comparaison "coûte" cher, ces opérations supplémentaires peuvent pénaliser la méthode : la recherche dichotomique sera plus efficace.