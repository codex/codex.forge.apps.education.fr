

# --------- PYODIDE:code --------- #

def trichotomie(tableau, cible):
    ...

# --------- PYODIDE:corr --------- #

def trichotomie(tableau, cible):
    debut = 0
    fin = len(tableau) - 1
    while debut <= fin:
        tiers_1 = debut + (fin - debut) // 3
        tiers_2 = fin - (fin - debut) // 3
        if cible < tableau[tiers_1]:
            fin = tiers_1 - 1
        elif tableau[tiers_1] < cible < tableau[tiers_2]:
            debut = tiers_1 + 1
            fin = tiers_2 - 1
        elif tableau[tiers_2] < cible:
            debut = tiers_1 + 1
        elif cible == tableau[tiers_1]:
            return tiers_1
        else:
            return tiers_2
    return None

# --------- PYODIDE:tests --------- #

tableau = [17, 19, 20, 21, 23, 25, 29, 30]
assert trichotomie(tableau, 17) == 0
assert trichotomie(tableau, 23) == 4
assert trichotomie(tableau, -1) is None
assert trichotomie(tableau, 24) is None
assert trichotomie(tableau, 50) is None

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from random import randrange, choice
from bisect import bisect_left

for _ in range(20):
    tableau = [k for k in range(randrange(10**5, 10**6))]
    x = choice(tableau)
    assert trichotomie(tableau, x) == bisect_left(tableau, x)
    assert trichotomie(tableau, -1) is None
    assert trichotomie(tableau, tableau[-1] + 1) is None
    assert trichotomie(tableau, x - 0.5) is None