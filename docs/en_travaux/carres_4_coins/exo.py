

# --------- PYODIDE:env --------- #


from js import document


class Figure:
    def __init__(self, x_min, y_min, x_max, y_max, width):
        self.x_min = float(x_min)
        self.x_max = float(x_max)
        self.y_min = float(y_min)
        self.y_max = float(y_max)
        self.width = round(width)
        self.height = round(self.width * (self.y_max - self.y_min) / (self.x_max - self.x_min))

        self.prog = []
        self.test = []

    def _repr_svg_(self):
        BASE = 2**20  # 1048576
        if self.width < self.height:
            WIDTH = BASE  # coordonnées
            HEIGHT = BASE * self.height // self.width
            k = HEIGHT / (self.y_max - self.y_min)
            assert k > 0
        else:
            HEIGHT = BASE  # coordonnées
            WIDTH = BASE * self.width // self.height
            k = WIDTH / (self.x_max - self.x_min)
            assert k > 0

        def _j(x):
            j = round((x - self.x_min) * k)
            assert 0 <= j <= WIDTH, f"{j} {WIDTH}"
            return j

        def _i(y):
            i = round((self.y_max - y) * k)
            assert 0 <= i <= HEIGHT, f"{i} {HEIGHT} {WIDTH} {y} {k}"
            return i

        def _l(a):
            return round(a * k)

        ans = ['&lt?xml version="1.0" encoding="UTF-8"?>']
        ans.append(
            f'&ltsvg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="{self.width}px" height="{self.height}px" viewBox="0 0 {WIDTH} {HEIGHT}" fill="none" stroke="black" >'
        )
        if self.prog != []:
            for a, _, x, y in self.prog:
                i = _i(y + a / 2)
                j = _j(x - a / 2)
                r = _l(a / 8)
                l = _l(a)
                e = _l(a / 16)
                ans.append(f'&ltrect x="{j}" y="{i}" rx="{r}" ry="{r}" width="{l}" height="{l}" stroke-width="{e}" />')
        ans.append("&lt/svg>")
        return "\n".join(ans).replace("&lt", "<")

    def draw(self, truc):
        x, y, a = truc
        self.test.append(truc)
        assert a > 0, "Le côté doit être strictement positif"
        self.prog.append((a, 13 * x + 7 * y, x, y))

    def done(self, elt_id="fig_geometry"):
        elt = document.getElementById(elt_id)
        elt.innerHTML = self._repr_svg_()


def square(x, y, a):
    return (x, y, a)




# --------- PYODIDE:code --------- #

def motif_carre_4(x, y, a, n):
    ...  # À compléter


fig = Figure(x_min=-4, y_min=-4, x_max=+4, y_max=+4, width=256)

motif_carre_4(0, 0, 4, 4)

fig.done()

# --------- PYODIDE:corr --------- #

def motif_carre_4(x, y, a, n):
    if n > 0:
        fig.draw(square(x, y, a))
        b = a / 2
        for dx, dy in [(-b, -b), (b, -b), (-b, b), (b, b)]:
            motif_carre_4(x + dx, y + dy, b, n - 1)

fig = Figure(x_min=-4, y_min=-4, x_max=+4, y_max=+4, width=256)

motif_carre_4(0, 0, 4, 4)

fig.done()

# --------- PYODIDE:secrets --------- #

attendus = []
def motif_carre_4_test(x, y, a, n):
    if n > 0:
        attendus.append((x, y, a))
        b = a / 2
        for dx, dy in [(-b, -b), (b, -b), (-b, b), (b, b)]:
            motif_carre_4_test(x + dx, y + dy, b, n - 1)


motif_carre_4_test(0, 0, 4, 4)
nb_carres = len(attendus)
valides = [False for f in range(nb_carres)]
assert len(fig.test) == nb_carres, "Il n'y a pas le bon nombre de carrés"
for carre in attendus:
    assert carre in fig.test, "Les carrés ne sont pas les bons"
