---
author:
    - Pierre Marquestaut
    - D'après sujet de remplacement métropole 2021
hide:
    - navigation
    - toc
title: Yaourt
difficulty: 150
tags:
    - programmation orientée objet
    - en travaux
maj: 06/06/2024
---

Une entreprise fabrique des yaourts qui peuvent être soit nature (sans arôme), soit aromatisés (fraise, abricot ou vanille).
Pour pouvoir traiter informatiquement les spécificités de ce produit, on va donc créer une classe `Yaourt` qui possèdera un certain nombre d’attributs :

* Son *genre* : nature ou aromatisé
* Son *arôme* : fraise, abricot, vanille ou aucun
* Sa *date de durabilité minimale* (DDM) exprimée par un entier compris entre 1 et 365 (on ne gère pas les années bissextiles). Par exemple, si la DDM est égale à 15, la date de durabilité minimale est le 15 janvier.

Les attributs `arome` et `genre` sont liés entre eux : pour conserver la cohérence des valeurs, on ne peut pas en modifier un sans modifier l'autre. On va par conséquent les utiliser comme des **attributs privés**, c'est-à-dire qu'il ne peuvent être directement manipulés qu'à l'intérieur de la définition de classe.

On va créer également des méthodes permettant d’interagir avec l’objet `Yaourt` pour attribuer un arôme ou récupérer un genre par exemple. 

```pycon
>>> mon_yaourt1 = Yaourt('aucun',18)
>>> mon_yaourt1.obtenir_arome()
"aucun"
>>> mon_yaourt1.obtenir_genre() 
"nature"
>>> mon_yaourt1.modifier_arome("abricot")
>>> mon_yaourt1.obtenir_arome() 
"abricot"
>>> mon_yaourt1.obtenir_genre() 
"aromatise"
```

!!! note "méthode `modifier_genre`" 
    On pourra remarquer que la fonction `modifier_genre` ne sert à rien lorsque l'on manipule une instance, mais peut en revanche servir à l'intérieur de la définition de la classe.


On peut représenter cette classe par le diagramme ci-dessous :

```mermaid
classDiagram
    class Yaourt {
     __init__()
    -str arome
    -str genre
    +int DDM
    obtenir_arome()
    obtenir_genre()
    modifier_arome()
    modifier_genre()
    }
```   


Compléter la classe `Yahourt` :

{{ IDE('exo', MAX_SIZE=45) }}