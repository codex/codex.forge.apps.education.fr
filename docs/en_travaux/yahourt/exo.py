# --------- PYODIDE:code --------- #

class Yaourt:
    def __init__(self,arome,duree):
        ...

    def obtenir_genre(self):
        ...

    def obtenir_arome(self):
        ...
      
    def modifier_genre(self,arome):
        ...
            
    def modifier_arome(self,arome):
        ...

# --------- PYODIDE:corr --------- #


class Yaourt:
    def __init__(self,arome,duree):
        self.arome = arome
        self.DDM = duree
        if arome == 'aucun':
            self.genre = 'nature'
        else:
            self.genre = 'aromatise'

    def obtenir_genre(self):
        return self.genre
    
    def obtenir_arome(self):
        return self.arome
    
    
    def modifier_genre(self,arome):
        if arome == 'aucun':
            self.genre = 'nature'
        else:
            self.genre = 'aromatise'
            
    def modifier_arome(self,arome):
        self.arome = arome
        self.modifier_genre(arome)

# --------- PYODIDE:tests --------- #
mon_yaourt1 = Yaourt('aucun',18)
assert mon_yaourt1.obtenir_arome() == "aucun"
assert mon_yaourt1.obtenir_genre() == "nature"
mon_yaourt1.modifier_arome("abricot")
assert mon_yaourt1.obtenir_arome() == "abricot"
assert mon_yaourt1.obtenir_genre() == "aromatise"

# --------- PYODIDE:secrets --------- #


mon_yaourt2 = Yaourt('fraise',24)
assert mon_yaourt2.obtenir_genre() == "aromatise"
mon_yaourt2.modifier_genre("aucun")
assert mon_yaourt2.obtenir_genre() == "nature"
mon_yaourt2.modifier_arome("melon")
assert mon_yaourt2.obtenir_arome() == "melon"
assert mon_yaourt2.obtenir_genre() == "aromatise"