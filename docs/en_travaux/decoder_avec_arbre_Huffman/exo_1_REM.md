Dans la solution, la méthode de chaine de caractères `join` permet, de joindre tous les éléments (nécessairement de type str) de la liste en les séparant par la chaine de caractère. Dans le cas présent la chaine est vide, le résultat est donc une concaténation de tous les caractères de la liste.

Cette méthode est plus efficace que des concaténations successives.
