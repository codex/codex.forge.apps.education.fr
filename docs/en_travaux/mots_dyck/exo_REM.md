On utilise la méthode proposée dans le sujet.

Il est aussi possible de résoudre cet exercice en utilisant une fonction récursive :

```python
def mots_dyck(n):
    def mots_dyck_rec(n, mot, ouvrantes, fermantes):
        if len(mot) == 2 * n:
            mots.append(mot)
        else:
            if ouvrantes < n:
                mots_dyck_rec(n, mot + "(", ouvrantes + 1, fermantes)
            if ouvrantes - fermantes > 0:
                mots_dyck_rec(n, mot + ")", ouvrantes, fermantes + 1)

    mots = []
    mots_dyck_rec(n, "", 0, 0)
    return mots
```

Le nombre de mots de Dyck de taille $n$ est donné par le $n$-ième [nombre de Catalan](https://fr.wikipedia.org/wiki/Nombre_de_Catalan). On pourra à leur propos traiter l'exercice [suivant](https://e-nsi.gitlab.io/pratique/N2/849-nb_catalan_1/sujet/)

??? note "Pas que des parenthèses !"

    Techniquement, il est possible de définir les mots de Dyck en utilisant d'autres caractères que des parenthèses (par exemple des déplacements vers la « droite » ou vers le « haut » pour représenter les déplacements restant sous « la diagonale d'une grille ») ou plus de paires de caractères (par exemple des parenthèses et des crochets).

