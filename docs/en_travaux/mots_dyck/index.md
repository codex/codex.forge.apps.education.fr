---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Mots de Dyck
tags:
    - en travaux
    - récursivité
    - pile
---

# Mots de Dyck

On cherche dans cet exercice à générer l'ensemble des *mots de Dyck* de taille $n$.

Un mot de Dyck de longueur $n$ est une expression bien parenthésée comptant $n$ paires de parenthèses.

Par exemple, `(())()` est un mot de Dyck de longueur $3$. `())` n'est pas un mot de Dyck car il y a une parenthèse fermante de trop.

??? note "Définition précise"
    
    Un mot de Dyck (unilatère) de taille $n$ compte exactement $n$ parenthèses ouvrantes `'('` et exactement $n$ parenthèses fermantes `')'`.
    
    De plus, à chaque endroit de la chaîne, le nombre de parenthèses ouvrantes déjà rencontrées est toujours supérieur ou égal à celui de parenthèses fermantes.

Il est possible de générer les mots de Dyck de taille $n$ en utilisant une pile :

* on empile initialement le mot vide ;
* tant que la pile est non vide :
    * on dépile un mot ;
    * si ce mot est de la longueur souhaitée, on l'ajoute à la liste de mots de Dyck cherchés ;
    * sinon s'il est possible de lui ajouter une parenthèse ouvrante, on l'ajoute et l'on empile le nouveau mot ;
    * enfin s'il est possible de lui ajouter une parenthèse fermante, on l'ajoute et l'on empile le nouveau mot.

Ce procédé est illustré ci-dessous dans le cas de la construction de mot de taille $n=3$.

```mermaid
graph LR
    R{ } --> N1{"("}
    N1 --> N2{"(("}
    N1 --> N3{"()"}
    N2 --> N4{"((("}
    N2 --> N5{"(()"}
    N3 --> N6{"()("}
    N4 --> N7{"((()"}
    N5 --> N8{"(()("}
    N5 --> N9{"(())"}
    N6 --> N10{"()(("}
    N6 --> N11{"()()"}
    N7 --> N12{"((())"}
    N8 --> N13{"(()()"}
    N9 --> N14{"(())("}
    N10 --> N15{"()(()"}
    N11 --> N16{"()()("}
    N12 --> N17{"((()))"}
    N13 --> N18{"(()())"}
    N14 --> N19{"(())()"}
    N15 --> N20{"()(())"}
    N16 --> N21{"()()()"}
```

Écrire la fonction `mots_dyck` prenant en argument un entier `n` et renvoyant la liste des mots de Dyck de taille `n`. Ces mots comptent donc `n` **paires** de parenthèses.

Il est inutile de trier la liste des résultats.

???+ example "Exemples"

    ```pycon title=""
    >>> mots_dyck(0)
    ['']
    >>> mots_dyck(1)
    ['()']
    >>> mots_dyck(2)
    ['(())', '()()']
    >>> mots_dyck(3)
    ['((()))', '(()())', '(())()', '()(())', '()()()']
    ```

{{ IDE('exo') }}

??? tip "Astuce (1)"

    Il n'est pas possible d'ajouter une parenthèse ouvrante si le mot en compte déjà `n`.

??? tip "Astuce (2)"
    
    Il n'est pas possible d'ajouter une parenthèse fermante si le mot compte autant d'ouvrantes que de fermantes.

??? tip "Astuce (3)"
    
    On pourra empiler des tuples du type `(mot, nb_ouvrantes, nb_fermantes)`.
