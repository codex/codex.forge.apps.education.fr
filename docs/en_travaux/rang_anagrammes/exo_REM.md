On organise le code en plusieurs fonctions :

* `occurrences` compte le nombre d'occurrences de chaque lettre dans `mot`. On peut utiliser le résultat afin de lister les lettres présentes dans `mot` : ce sont les clés du dictionnaire.

* `factorielle` renvoie la factorielle d'un entier positif `n`. On utilise le cas de base `n == 0` pour simplifier le calcul dans la fonction `multinomial`.

* `multinomial` calcule le nombre de permutations d'un ensemble de `n` éléments, en tenant compte des répétitions fournies dans `repetitions`. 

    Si une des valeurs de `repetition` vaut `#!py 0`, la convention 0!=1 permet de tout de même calculer le résultat.

!!! note "Coefficient multinomial"

    Le nombre de permutations calculé par la fonction précédente est appelé *coefficient multinomial* en mathématiques.

* `rang` calcule la valeur cherchée :

  *  On commence par déterminer les occurrences de chaque lettre dans `mot` (`decomptes`).
  *  Les clés de ce dictionnaire nous indique les lettres présentes dans la chaîne.
  *  On trie cette liste et on la parcourt jusqu'à rencontrer la première lettre de `mot`.
  *  Pour chaque lettre lue précédent mot, on compte le nombre d'anagrammes commençant par cette lettre. Il s'agit du nombre de permutations de `longueur - 1` éléments, chacun étant répété autant de fois que nécessaire (sauf pour la lettre lue qui est répétée une fois de moins)
  *  Une fois le parcours effectué on somme le total avec le rang du mot privé de sa première lettre.