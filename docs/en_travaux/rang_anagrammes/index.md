---
author: Nicolas Revéret
difficulty: 350
hide:
    - navigation
    - toc
title: Rang d'un anagramme
tags:
    - en travaux
    - dictionnaire
    - récursivité
---

# Rang d'un anagramme

On considère dans cet exercice des « mots » comptant au moins une lettre. Ces « mots » n'existent pas nécessairement dans le dictionnaire.

On n'utilise que des lettres en casse majuscule et non accentuées.

Les anagrammes d'un mot sont tous les mots que l'on peut écrire en utilisant les mêmes lettres. Là encore, les anagrammes ne sont pas nécessairement dans le dictionnaire.

Voici par exemple les premiers anagrammes de `#!py "GALA"` :

|   Anagramme   | Rang  |
| :-----------: | :---: |
| `#!py "AAGL"` |   1   |
| `#!py "AALG"` |   2   |
| `#!py "ALAG"` |   3   |
| `#!py "ALGA"` |   4   |
| `#!py "AGAL"` |   5   |
| `#!py "AGLA"` |   6   |
| `#!py "GAAL"` |   7   |
| `#!py "GALA"` |   8   |
|      ...      |  ...  |

!!! note "Répétitions"

    Dans le cas où le mot compte plusieurs fois la même lettre on n'écrit qu'une seule fois les anagrammes correspondants. Par exemple, `#!py "ABA"` ne compte que 3 anagrammes : `#!py "AAB"`, `#!py "ABA"` et `#!py "BAA"`.

On peut associer à chaque anagramme son **rang** dans la liste triée dans l'ordre alphabétique de tous les anagrammes du mot. Ainsi, le rang de `#!py "GALA"` est 8.

On demande d'écrire une fonction `rang` qui prend en argument une chaîne de caractères `mot` et renvoie son rang.

!!! danger "Attention"
    
    Il est possible de déterminer le rang d'un mot en énumérant tous les anagrammes dans l'ordre alphabétique mais cette version devient rapidement très longue à exécuter.
    
    Par exemple, le rang de `#!py "ENUMERATION"` est 1 971 860 !

???+ example "Exemples"

    ```pycon title=""
    >>> rang("GALA")
    8
    >>> rang("BCA")
    4
    >>> rang("ENUMERATION")
    1971860
    ```

{{ IDE('exo') }}

??? tip "Astuce (1)"

    Faites quelques essais. Avec `#!py "RANG"` et `#!py "SARAH"` par exemple...

??? tip "Astuce (2)"

    La liste triée des anagrammes de `#!py "SARAH"` contient tout d'abord les anagrammes débutant par `#!py "A"`, puis ceux débutant par `#!py "H"` *etc*

??? tip "Astuce (3)"

    Le nombre d'anagrammes d'un mot de $N$ lettres dans lequel chaque lettre est répétée $(n_0, n_1, ...,n_k)$ fois ($n_0+ n_1+ ...+n_k = N$) est égal à :

    $$\frac{N!}{n_0!\times n_1!\times ...\times n_k!}$$

    On rappelle que $N! = N\times (N-1)\times (N-2)\times ...\times 1$. On convient si besoin que $0!=1$.

??? tip "Astuce (4)"

    On peut utiliser un code récursif. Si l'on souhaite déterminer le rang d'un mot, il faut déjà compter le nombre d'anagrammes débutant par une lettre placée plus tôt dans l'alphabet puis lui ajouter le rang du mot privé de sa première lettre.

    Le rang de `#!py "SARAH"` est par exemple égal :
    
    * au nombre d'anagrammes débutant par `#!py "A"`, par `#!py "H"` et par `#!py "R"`,
    * additionné au rang de `#!py "ARAH"`.

    Un mot de une lettre a toujours un rang égal à 1 !