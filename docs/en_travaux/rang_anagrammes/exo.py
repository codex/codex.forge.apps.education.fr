

# --------- PYODIDE:code --------- #

def rang(mot):
    ...

# --------- PYODIDE:corr --------- #

def occurrences(mot):
    decomptes = {}
    for lettre in mot:
        if lettre in decomptes:
            decomptes[lettre] += 1
        else:
            decomptes[lettre] = 1
    return decomptes


def factorielle(n):
    if n == 0:
        return 1
    return n * factorielle(n - 1)


def multinomial(n, repetitions):
    coeff = factorielle(n)
    for x in repetitions:
        coeff //= factorielle(x)
    return coeff


def rang(mot):
    if len(mot) == 1:
        return 1

    longueur = len(mot)
    decomptes = occurrences(mot)
    presentes = [lettre for lettre in decomptes]
    lettres_triees = sorted(presentes)
    total = 0
    i = 0
    while lettres_triees[i] != mot[0]:
        debut = lettres_triees[i]
        repetitions = []
        for lettre in presentes:
            if lettre != debut:
                repetitions.append(decomptes[lettre])
            else:
                repetitions.append(decomptes[lettre] - 1)
        total += multinomial(longueur - 1, repetitions)
        i += 1
    return total + rang(mot[1:])


# Tests
assert rang("GALA") == 8, "Erreur sur le rang de GALA"
assert rang("BCA") == 4, "Erreur sur le rang de BCA"
assert rang("ABAACAC") == 32, "Erreur sur le rang de ABAACAC"
assert rang("ENUMERATION") == 1971860, "Erreur sur le rang de ENUMERATION"

# --------- PYODIDE:tests --------- #

assert rang("GALA") == 8, "Erreur sur le rang de GALA"
assert rang("BCA") == 4, "Erreur sur le rang de BCA"
assert rang("ABAACAC") == 32, "Erreur sur le rang de ABAACAC"
assert rang("ENUMERATION") == 1971860, "Erreur sur le rang de ENUMERATION"

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
from math import factorial
from string import ascii_uppercase as ALPHABET
from random import choice, randrange


def rang_corr(mot):
    if len(mot) == 1:
        return 1

    repetitions = {}
    for lettre in mot:
        repetitions[lettre] = repetitions.get(lettre, 0) + 1
    uniques = set(mot)
    uniques_triees = sorted(uniques)
    total = 0
    i = 0
    while uniques_triees[i] != mot[0]:
        debut = uniques_triees[i]
        denominateur = factorial(repetitions[debut] - 1)
        for c in uniques - {debut}:
            denominateur *= factorial(repetitions[c])
        total += factorial(len(mot) - 1) // denominateur
        i += 1
    return total + rang_corr(mot[1:])


for _ in range(20):
    taille = randrange(5, 21)
    mot = "".join([choice(ALPHABET) for _ in range(taille)])
    attendu = rang_corr(mot)
    assert rang(mot) == attendu, f"Erreur avec {mot}"