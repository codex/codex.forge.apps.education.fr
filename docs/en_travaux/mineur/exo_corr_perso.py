import numpy as np
from random import randint


def meilleur_total(mine, i, j):
    if j == -1 or j == largeur:
        return -1
    elif i == hauteur-1:
        totaux[i, j] = mine[i][j]
    elif (i, j) not in totaux:
        totaux[i, j], chemins[i, j] = max((meilleur_total(mine, i+1, j-1), -1),
                                          (meilleur_total(mine, i+1, j),    0),
                                          (meilleur_total(mine, i+1, j+1), +1))
        totaux[i, j] += mine[i][j]
    return totaux[i, j]


def chemin(chemins, i, j):
    etapes = [(i, j)]
    while (i, j) in chemins:
        etapes.append((i+1, j+chemins[i, j]))
        i, j = i+1, j+chemins[i, j]
    return " -> ".join([str(etp) for etp in etapes])


def dimensions(terrain):
    """Renvoie le tuple (hauteur, largeur) du terrain"""
    return len(terrain), len(terrain[0])


def mine_aleatoire(largeur, hauteur, mini=1, maxi=10):
    return [[randint(mini, maxi) for k in range(largeur)] for _ in range(hauteur)]


def liste_memoire(memoire):
    return [[memoire[i, j] if (i, j) in memoire else 0 for j in range(largeur)] for i in range(hauteur)]


def meilleur_total_iter(mine):
    richesse_actuelle = [0]*largeur
    for i in range(hauteur-1, -1, -1):
        richesse_dessus = [0]*largeur
        richesse_dessus[0] = mine[i][0] + max(richesse_actuelle[0], richesse_actuelle[1])
        for j in range(1, largeur-1):
            richesse_dessus[j] = mine[i][j] + max(richesse_actuelle[j-1],
                                      richesse_actuelle[j],
                                      richesse_actuelle[j+1])
        richesse_dessus[largeur-1] = mine[i][largeur-1] + \
            max(richesse_actuelle[largeur-2], richesse_actuelle[largeur-1])

        richesse_actuelle = richesse_dessus
    return richesse_actuelle


def meilleur_total_rec(mine, niveau=0):
    if niveau == hauteur-1:
        return [richesse for richesse in mine[hauteur-1]]
    else:
        richesse_dessous = meilleur_total_rec(mine, niveau+1)
        richesse_niveau = [0]*largeur
        richesse_niveau[0] = mine[niveau][0] + \
            max(richesse_dessous[0], richesse_dessous[1])
        for j in range(1, largeur-1):
            richesse_niveau[j] = mine[niveau][j] + max(richesse_dessous[j-1],
                                                       richesse_dessous[j],
                                                       richesse_dessous[j+1])
        richesse_niveau[largeur-1] = mine[niveau][largeur-1] + \
            max(richesse_dessous[largeur-2], richesse_dessous[largeur-1])

    return richesse_niveau


totaux = {}
chemins = {}
mine = [
    [7, 9, 10, 7, 10, 8,  5],
    [5, 3,  1, 7,  5, 7,  1],
    [8, 9,  7, 7,  2, 3,  2],
    [7, 7,  2, 4,  6, 5,  7],
    [6, 7,  1, 6,  6, 6,  1],
    [3, 2,  5, 3,  2, 2, 10]
]
# mine = mine_aleatoire(4, 6)
hauteur, largeur = dimensions(mine)
print(np.asmatrix(mine))
depart = 0, largeur//2
print(meilleur_total(mine, *depart))
print(chemin(chemins, *depart))
print(f"{len(chemins)=}")
print(np.asmatrix(liste_memoire(totaux)))
print(meilleur_total_iter(mine))
print(meilleur_total_rec(mine))
mine = [
    [1, 0],
    [0, 1],
    [1, 0],
    [0, 1],
    [1, 0],
    [0, 1]
]
hauteur, largeur = dimensions(mine)

assert meilleur_total_iter(mine) == [6, 5]
# print(np.asmatrix(liste_memoire(chemins)))
