Il s'agit en quelque sorte de trouver le chemin traversant la grille de poids maximal. On utilise ici une approche de programmation dynamique.

L'approche itérative adoptée ici permet de ne calculer les richesses cumulées que d'une ligne à la fois et "économise" ainsi la mémoire.

On pourrait aussi utiliser une approche récursive avec une fonction "interne" à la fonction de base. Le cas de base est alors le fond de la mine (niveau `hauteur - 1`) :

```python
hauteur, largeur = dimensions(mine)
def meilleure_richesse_cumulee(mine):
    def richesses_niveau(mine, niveau):
        if niveau == hauteur - 1: # Cas de base (le fond de la mine)
            return [richesse for richesse in mine[hauteur - 1]]
        else:  # Cas général
            richesses_niveau_dessous = meilleur_total_rec(mine, niveau + 1)
            richesses_niveau = [0]*largeur
            # Colonne de gauche
            richesses_niveau[0] = mine[niveau][0] + max(richesses_niveau_dessous[0],
                                                        richesses_niveau_dessous[1])
            # Colonnes centrales
            for j in range(1, largeur - 1):
                richesses_niveau[j] = mine[niveau][j] + max(richesses_niveau_dessous[j - 1],
                                                            richesses_niveau_dessous[j],
                                                            richesses_niveau_dessous[j + 1])
            # Colonne de droite
            richesses_niveau[-1] = mine[niveau][-1] + max(richesses_niveau_dessous[-2],
                                                            richesses_niveau_dessous[-1])
        return richesses_niveau

    return max(richesses_niveau(mine, 0))
```

Une fois ce problème résolu, les plus curieux pourront s'intéresser aux problèmes suivants du projet Euler :

* [Maximum path sum I](https://projecteuler.net/problem=18)
* [Maximum path sum II](https://projecteuler.net/problem=67)
* [Path sum: two ways](https://projecteuler.net/problem=81)
* [Path sum: three ways](https://projecteuler.net/problem=82)
* [Path sum: four ways](https://projecteuler.net/problem=83)