---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Somme d'une liste de valeurs
tags:
    - en travaux
    - liste/tableau
difficulty: 120
---

En Python, les tableaux permettent de contenir des listes de valeurs, peu importe le type de ces valeurs :

```python title=""
tableau_entiers = [5, 6, 4, 7, 2, 8]
tableau_tableaux = [[0, 1], [3, 4]]
tableau_caractère = ["a", "b", "c"]
```
On souhaite pouvoir écrire une fonction qui réalise la somme des valeurs contenues dans un tableaux, et ce quel que soit le type de ces valeurs.

???+ info "Soyons précis"
    Dans le cas de caractères ou de tableaux, on ne parlera pas de somme mais plutôt de **concaténation** :
    ```pycon title=""
    >>> "a" + "b"
    "ab"
    >>> [1, 2, 3, 4] + [5, 6, 7, 8]
    [1, 2, 3, 4, 5, 6, 7, 8]
    ```


    
**Créer** une fonction `somme_valeurs` qui prend en paramètre une liste non vide de valeurs et qui renvoie la somme des valeurs de cette liste.

```pycon title=""
>>> tableau_entiers = [5, 6, 4, 7, 2, 8]
>>> somme_valeurs(tableau_entiers) 
32
>>> tableau_tableaux = [[0, 1], [3, 4]]
>>> somme_valeurs(tableau_tableaux)  
[0, 1, 3, 4]
>>> tableau_caractère = ["a", "b", "c"]
>>> somme_valeurs(tableau_tableaux)  
"abc"
```
??? tip "Aide"
    On pourra initialiser la somme à la première valeur du tableau...

??? warning "Types"
    Dans les tests, seront testés d'autres types de variable que ceux présentés dans l'énoncé, mais il sera toujours possible d’additionner les valeurs deux à deux.

{{ IDE('exo', SANS = "sum") }}




