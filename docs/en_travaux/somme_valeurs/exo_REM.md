Il est à noter que cette fonction ne peut s'appliquer que sur des valeurs dont le type dispose de l'opérateur `+` (par exemple des intervalles de type `range`ne disposent pas de cet opérateur).

On pourra également noter que la complexité en temps de cette fonction n'est **linéaire** que si l'opérateur `+` s'exécute à **coût constant**, ce qui est le cas pour le type `int` ou `float`.<br>
En revanche, ce n'est pas le cas lorsque l'opérateur `+` est utilisé pour une concaténation. En effet , la *concaténation* de deux objets (tableaux, tuples ou chaînes de caractères) est réalisée par la création d'un troisième objet, contenant l'ensemble des éléments des deux autres : le coût en temps de cette opération est donc linéaire car dépend du nombre total d'éléments.<br>

Il en résulte que la fonction `somme_valeurs` voit sa complexité en temps devenir **quadratique** si on lui passe un tableau de chaînes de caractères, de tuples ou de tableaux.<br>
Par exemple, si on passe en paramètre un tableau de $N$ chaînes de $1$ caractère : 

* à la première itération, la concaténation crée une chaîne de $2$ caractères au moyen de $2$ opérations, 
* à la deuxième itération, la concaténation crée une chaîne de $3$ caractères au moyen de $3$ opérations, 
* ...
* à la dernière itération, la concaténation crée une chaîne de $N$ caractères au moyen de $N$ opérations.

Au total, il aura donc fallu $(N+2)(N-1)/2$ opérations.


Le programme suivant permet à la fonction de conserver une compléxité linéaire pour les tableaux de tableaux :

```python
def somme_valeurs(tableau): 
    somme = tableau[0]
    taille = len(tableau)
    for i in range(1, taille):
        somme +=  tableau[i]
    return somme
```

En effet, pour les variables de type `list`, l'instruction `tableau_1 += tableau_2` est équivalente à `tableau_1.extend(tableau_2)` et consiste à rajouter les éléments de `tableau_2` dans `tableau_1` sans créer un autre tableau : le coût ne dépend plus que du nombre d'éléments à rajouter et non du nombre total d'éléments.<br>
**Attention :** les variables de type `str` ou `tuple` sont immuables et à ce titre ne peuvent être modifiées. L'opérateur `+=` créera toujours un nouvel objet.<br>
Il en résulte que la fonction `somme_valeurs` voit sa complexité redevenir **linéaire** si on lui passe un tableau de tableaux (mais reste quadratique pour les tuples et les chaînes de caractères)<br>
Par exemple, si on passe en paramètre un tableau de $N$ tableaux de $1$ élément : 

* à la première itération, l'opération insère $1$ élement au tableau contenant déjà $1$ élément au moyen de $1$ opération, 
* à la deuxième itération, l'opération insère $1$ élement au tableau contenant déjà $2$ éléments au moyen de $1$ opération,
* ... 
* à la dernière itération, l'opération insère $1$ élement au tableau contenant déjà $N-1$ éléments au moyen de $1$ opération.

Au total, il aura donc fallu $N-1$ opérations.