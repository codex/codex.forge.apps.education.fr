# --------- PYODIDE:code --------- #

def somme_valeurs(tableau): 
    ...

# --------- PYODIDE:corr --------- #


def somme_valeurs(tableau): 
    somme = tableau[0]
    taille = len(tableau)
    for i in range(1, taille):
        somme =  somme + tableau[i]
    return somme
        
    
# --------- PYODIDE:tests --------- #

tableau_entiers = [5, 6, 4, 7, 2, 8]
assert somme_valeurs(tableau_entiers) == 32
tableau_tableaux = [[0, 1], [3, 4]]
assert somme_valeurs(tableau_tableaux) == [0, 1, 3, 4]
tableau_caractère = ["a", "b", "c"]
assert somme_valeurs(tableau_caractère) == "abc"

# --------- PYODIDE:secrets --------- #
n = 100
mes_valeurs = [k for k in range(n + 1)]
reponse = n*(n+1)/2
assert somme_valeurs(mes_valeurs) == reponse


n = 1000
mes_valeurs = [k for k in range(n + 1)]
reponse = n*(n+1)/2
assert somme_valeurs(mes_valeurs) == reponse


class Duree:
    def __init__(self, h, m, s):
        """Instanciation
        Les minutes et secondes doivent être compris entre 0 et 59
        """
        assert 0 <= m < 60 and 0 <= s < 60
        self.heures = h
        self.minutes = m
        self.secondes = s

    def en_secondes(self):
        """Conversion en secondes"""
        return self.heures * 3600 + self.minutes * 60 + self.secondes

    def ajoute_minutes(self, mins):
        """Ajout de mins minutes"""
        self.heures += (self.minutes + mins) // 60
        self.minutes = (self.minutes + mins) % 60

    def ajoute_secondes(self, secs):
        """Ajout de secs minutes"""
        self.ajoute_minutes((self.secondes + secs) // 60)
        self.secondes = (self.secondes + secs) % 60

    def __repr__(self):
        """Mise en forme pour l'affichage"""
        return f"{self.heures}:{self.minutes}:{self.secondes}"

    def __eq__(self, autre):
        """Test d'égalité"""
        return self.en_secondes() == autre.en_secondes()

    def __add__(self, autre):
        """Addition de cette durée et de autre"""
        cumul = Duree(0, 0, 0)
        cumul.ajoute_secondes(self.en_secondes() + autre.en_secondes())
        return cumul


tableau_durees = [Duree(2, 45, 52), Duree(2, 45, 52), Duree(5, 12, 26)]
assert somme_valeurs(tableau_durees) == Duree(10, 44, 10)

tableau_tuples = [("a", "b"), ("b", "c")]
assert somme_valeurs(tableau_tuples) == ("a", "b", "b", "c")
