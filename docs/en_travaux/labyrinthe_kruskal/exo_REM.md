L'algorithme de Kruskal est un algorithme permettant de construire un arbre couvrant minimal associé à graphe pondéré.

Dans son fonctionnement classique, on parcourt les arêtes du graphe dans l'ordre de leurs poids croissants. Dans la version proposée ici, le parcours se fait dans un ordre aléatoire.

L'étape compliquée de l'algorithme est la fusion des zones. Le choix proposé ici de stocker la zone de chaque cellule comme attribut de celle-ci n'est pas le plus efficace en termes de complexité : à chaque fusion on doit parcourir l'ensemble des cellules.

On pourrait procéder autrement en utilisant par exemple des ensembles, `#!py set` avec Python, mais cette structure n'est pas au programme de NSI.

L'aléatoire est contraint dans cet exercice afin de toujours obtenir les mêmes labyrinthes pour des dimensions données.

Cette contrainte est liée au choix de la graine de l'aléatoire `#!py random.seed(graine)` qui est ici fixée à `1`. Retirer cette ligne fait que Python choisira l'heure actuelle comme graine et génèrera des mélanges pseudo-aléatoires plus vraisemblables. Voir la [documentation officielle](https://docs.python.org/fr/3/library/random.html#random.seed).