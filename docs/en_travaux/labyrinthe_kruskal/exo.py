

# --------- PYODIDE:env --------- #


import random


class Cellule:
    def __init__(self, zone):
        self.est = True
        self.sud = True
        self.zone = zone

    def __str__(self) -> str:
        if self.est and self.sud:
            return "_|"
        elif self.est:
            return " |"
        elif self.sud:
            return "__"
        else:
            return "  "

    def __repr__(self) -> str:
        return self.__str__()


def base_labyrinthe(hauteur, largeur):
    lab = []
    for i in range(hauteur):
        ligne = [Cellule(i * largeur + j) for j in range(largeur)]
        lab.append(ligne)
    return lab


def dessin_console(labyrinthe):
    lignes = ["\n " + "__" * (LARGEUR - 1) + "_"]
    for i in range(HAUTEUR):
        ligne = ["|"] + [str(labyrinthe[i][j]) for j in range(LARGEUR)]
        lignes.append("".join(ligne))
    dessin = "\n".join(lignes) + "\n"
    return dessin


def murs_aleatoires(hauteur, largeur, graine=1):
    murs = []
    for i in range(hauteur - 1):
        for j in range(largeur - 1):
            murs.append((i, j, "est"))
            murs.append((i, j, "sud"))
    for i in range(hauteur - 1):
        murs.append((i, largeur - 1, "nord"))
    for j in range(largeur - 1):
        murs.append((hauteur - 1, j, "est"))
    random.seed(graine)
    random.shuffle(murs)
    return murs




# --------- PYODIDE:code --------- #

def labyrinthe(hauteur, largeur):
    # Initialisation
    lab = base_labyrinthe(hauteur, largeur)
    murs = murs_aleatoires(hauteur, largeur)

    # Traitement
    while ... != 0:
        i1, j1, orientation = murs.pop()
        ...

    return lab


# Test
HAUTEUR, LARGEUR = 4, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|    _|  ___| |__ | |
|_|  _|__ |__  _____|
|     |____ |     | |
|_|_|_________|_|___|
"""
)

# --------- PYODIDE:corr --------- #

import random


class Cellule:
    def __init__(self, zone):
        self.est = True
        self.sud = True
        self.zone = zone

    def __str__(self) -> str:
        if self.est and self.sud:
            return "_|"
        elif self.est:
            return " |"
        elif self.sud:
            return "__"
        else:
            return "  "

    def __repr__(self) -> str:
        return self.__str__()


def base_labyrinthe(hauteur, largeur):
    lab = []
    for i in range(hauteur):
        ligne = [Cellule(i * largeur + j) for j in range(largeur)]
        lab.append(ligne)
    return lab


def dessin_console(lab):
    hauteur, largeur = len(lab), len(lab[0])
    lignes = ["\n " + "__" * (largeur - 1) + "_"]
    for i in range(hauteur):
        ligne = ["|"] + [str(lab[i][j]) for j in range(largeur)]
        lignes.append("".join(ligne))
    dessin = "\n".join(lignes) + "\n"
    return dessin


def murs_aleatoires(hauteur, largeur, graine=1):
    murs = []
    for i in range(hauteur - 1):
        for j in range(largeur - 1):
            murs.append((i, j, "est"))
            murs.append((i, j, "sud"))
    for i in range(hauteur - 1):
        murs.append((i, largeur - 1, "nord"))
    for j in range(largeur - 1):
        murs.append((hauteur - 1, j, "est"))
    random.seed(graine)
    random.shuffle(murs)
    return murs


def labyrinthe(hauteur, largeur):
    lab = base_labyrinthe(hauteur, largeur)
    murs = murs_aleatoires(hauteur, largeur)

    while len(murs) != 0:
        i1, j1, orientation = murs.pop()
        if orientation == "est":
            di = 0
            dj = 1
        else:
            di = 1
            dj = 0
        i2 = i1 + di
        j2 = j1 + dj
        if lab[i1][j1].zone != lab[i2][j2].zone:
            if orientation == "est":
                lab[i1][j1].est = False
            else:
                lab[i1][j1].sud = False

            nouvelle_zone = lab[i1][j1].zone
            ancienne_zone = lab[i2][j2].zone
            for i in range(hauteur):
                for j in range(largeur):
                    if lab[i][j].zone == ancienne_zone:
                        lab[i][j].zone = nouvelle_zone

    return lab


# Test
HAUTEUR, LARGEUR = 4, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|    _|  ___| |__ | |
|_|  _|__ |__  _____|
|     |____ |     | |
|_|_|_________|_|___|
"""
)

# --------- PYODIDE:secrets --------- #

# Test
HAUTEUR, LARGEUR = 4, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|    _|  ___| |__ | |
|_|  _|__ |__  _____|
|     |____ |     | |
|_|_|_________|_|___|
"""
)

# Tests supplémentaires
HAUTEUR, LARGEUR = 4, 4
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 _______
| |  __ |
| |___| |
|    ___|
|_|_____|
"""
)

HAUTEUR, LARGEUR = 5, 5
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 _________
|__ |__  _|
|____     |
|__  _|_|_|
|  ____  _|
|___|_____|
"""
)

HAUTEUR, LARGEUR = 10, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|  __    _| |  _|__ |
|_|  _|  ___|  _|   |
|__  _|_|__  _|__ |_|
|  __        ______ |
|_|  _|_| |_|  ____ |
| |_| | | | |___|___|
|  ____    _|__ |__ |
|__   |_|  ____     |
|  _|__ |_|  _|_| | |
|___|_________|___|_|
"""
)
