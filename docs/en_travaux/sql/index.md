---
title: SQL
author: Vincent-Xavier Jumel
hide:
    - navigation
    - toc
tags:
    - à trous
    - en travaux
    - brouillon
difficulty: 130
---

# Un peu de SQL

On a enregistré en mémoire une base de données dont la structure est présentée par le schéma ci-dessous :

![sql](sql.svg)

Le champ `prof` vaut $1$ si l'ordinateur est situé au bureau professeur de la salle, et $0$ sinon.

Dans cet exercice, la fonction `execute_requete` est fournie : elle permet d'exécuter une requête sur la base de données.

 
L'exercice utilise les mots `SELECT`
`FROM` `WHERE` `JOIN` `INSERT INTO` `VALUES` et `ORDER BY`


On demande de compléter le code proposé pour réaliser les requêtes SQL
suivantes :


!!! example "Exemples"

    ```pycon title=""
    >>> execute_requete("SELECT COUNT(*) FROM Ordinateur")
    [(5,)]
    ```


{{ IDE('exo',STD_KEY="bk4fg1") }}
