
# --------- PYODIDE:env --------- #
import sqlite3


def initialisation_base_de_donnee():
    base = sqlite3.connect(":memory:")
    curseur = base.cursor()

    curseur.execute(
        """
    CREATE TABLE Ordinateur
        (nom_ordi text, salle text, marque_ordi text, modele_ordi,
        annee int, prof int, PRIMARY KEY (nom_ordi ));
    """
    )
    curseur.execute(
        """
    CREATE TABLE Videoprojecteur (nom_video text,salle text, marque_video text,
    modele_video text, tni int)
    """
    )
    curseur.execute(
        """
    CREATE TABLE Imprimante
    (nom_imprimante text, marque_imp text, modele_imp text, nom_ordi text,
    PRIMARY KEY (nom_imprimante, nom_ordi))
    """
    )
    curseur.execute(
        """
    INSERT INTO Ordinateur (nom_ordi, salle, marque_ordi, modele_ordi,
    annee, prof) VALUES
    ("Gen-24", "012", "HP", "compaq pro 6300", 2012, 1),
    ("Tech-62", "114", "Lenovo", "p300", 2015, 1),
    ("Gen-132", "223", "Dell", "Insipiron Compact", 2019, 1),
    ("Gen-133", "223", "Dell", "Insipiron Compact", 2019, 0),
    ("Gen-134", "223", "Dell", "Insipiron Compact", 2019, 0)
    """
    )
    curseur.execute(
        """
    INSERT INTO Imprimante (nom_imprimante, marque_imp, modele_imp,
    nom_ordi) VALUES
    ("imp_BTS_NB", "HP", "Laserjet pro M15w", "Tech-62"),
    ("imp_BTS_Couleur", "Canon", "Megatank Pixma G5050", "Tech-62"),
    ("imp_salle-info1", "Brother", "2360DN", "Gen-132"),
    ("imp_salle-info1", "Brother", "2360DN", "Gen-133"),
    ("imp_salle-info1", "Brother", "2360DN", "Gen-134")
    """
    )
    curseur.execute(
        """
    INSERT INTO Videoprojecteur (nom_video, salle, marque_video, modele_video, tni)
    VALUES
    ("VP_labo", "012", "Epson", "xb27", 1),
    ("VP_BTS",  "114", "Sanyo", "PLV-Z3", 0),
    ("VP_salle-info1", "223", "Optoma", "HD143X", 0),
    ("VP_225", "225", "Optoma", "HD143X", 1)
    """
    )
    base.commit()
    return curseur


curseur = initialisation_base_de_donnee()


def execute_requete(requete: str) -> list:
    if requete != ...:
        curseur.execute(requete)
        return curseur.fetchall()

# --------- PYODIDE:code --------- #
#Obtenir la salle et la marque de tous les ordinateurs
requete_1 = execute_requete(...)

#Obtenir le nom et la salle des ordinateurs professeurs
requete_2 = execute_requete(...)

#Obtenir la liste des ordinateurs entrés à partir de 2017
# classés par ordre croissant d'ancienneté
requete_3 = execute_requete(...)

#Obtenir le nom de l'imprimante, le nom de la salle et de le nom de l'ordinateur connecté à cette imprimante
requete_4 = execute_requete(...)

#Insérer un nouveau vidéo-projecteur "VP_315" de marque NEC, modèle ME402X, sans TNI dans la salle 315.
requete_5 = execute_requete(...)


# --------- PYODIDE:corr --------- #

#Obtenir la salle et la marque de tous les ordinateurs
requete_1 = execute_requete("SELECT salle, marque_ordi FROM Ordinateur")

#Obtenir le nom et la salle des ordinateurs professeurs
requete_2 = execute_requete("SELECT nom_ordi, salle FROM Ordinateur WHERE prof = 1")

#Obtenir la liste des ordinateurs entrés à partir de 2017
# classés par ordre croissant d'ancienneté
requete_3 = execute_requete(
    "SELECT * from Ordinateur WHERE annee >= 2017 ORDER BY annee ASC"
)

#Obtenir le nom de l'imprimante, le nom de la salle et de le nom de l'ordinateur connecté à cette imprimante
requete_4 = execute_requete(
    """
    SELECT nom_imprimante, salle, Ordinateur.nom_ordi
    FROM Imprimante
    JOIN Ordinateur ON Imprimante.nom_ordi = Ordinateur.nom_ordi
"""
)

#Insérer un nouveau vidéo-projecteur "VP_315" de marque NEC, modèle ME402X, sans TNI dans la salle 315.
requete_5 = execute_requete(
    """
    INSERT INTO Videoprojecteur (nom_video, salle, marque_video, modele_video, tni)
    VALUES ("VP_315", "315", "NEC", "ME402X", 0)
"""
)

# --------- PYODIDE:secrets --------- #
attendu_1 = [
    ("012", "HP"),
    ("114", "Lenovo"),
    ("223", "Dell"),
    ("223", "Dell"),
    ("223", "Dell"),
]
attendu_2 = sorted([('Gen-24', '012'), ('Tech-62', '114'), ('Gen-132', '223')] )
attendu_3 = [
    ("Gen-132", "223", "Dell", "Insipiron Compact", 2019, 1),
    ("Gen-133", "223", "Dell", "Insipiron Compact", 2019, 0),
    ("Gen-134", "223", "Dell", "Insipiron Compact", 2019, 0),
]
attendu_4 = [
    ("imp_BTS_Couleur", "114", "Tech-62"),
    ("imp_BTS_NB", "114", "Tech-62"),
    ("imp_salle-info1", "223", "Gen-132"),
    ("imp_salle-info1", "223", "Gen-133"),
    ("imp_salle-info1", "223", "Gen-134"),
]

requete_5 =  execute_requete("SELECT * FROM Videoprojecteur WHERE salle = '315'")
attendu_5 = [
    ("VP_315", "315", "NEC", "ME402X", 0)
]

ok = True



requetes = [requete_1, requete_2, requete_3, requete_4, requete_5]
attendus = [attendu_1, attendu_2, attendu_3, attendu_4, attendu_5]

for i in range(len(requetes)):
    n = i
    terminal_message("bk4fg1", f"Requête {n} : " , new_line=False)
    if sorted(requetes[i]) == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif requetes[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
        terminal_message("bk4fg1", f"Résultat : {requetes[i]} ")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")
        terminal_message("bk4fg1", f"Résultat : {requetes[i]} ")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"