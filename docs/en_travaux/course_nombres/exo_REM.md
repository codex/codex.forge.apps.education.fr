# Commentaires

Le calcul de la distance directe entre deux indices `i` et `j` correspond à la différence `d = i - j` lorsque `i` est plus grand que `j` et à `d = j - i` dans le cas contraire. Il faut aussi établir la distance indirecte du chemin qui traverse les extrémités du tableau au lieu d'emprunter les cases intermédiaires. Elle est tout simplement égale à la longueur du tableau moins la distance directe `len(tab) - d`. Il ne reste plus qu'à choisir la plus petite de ces deux distances, pour minimiser le déplacement à effectuer.

Comme il faut parcourir les nombres dans l'ordre croissant, il suffit d'établir la liste de leurs indices dans l'ordre où il faut les parcourir. Pour y parvenir on peut construire la liste des couples `(nombre,indice)` et la trier
suivant l'ordre croissant des nombres afin de constituer ensuite la liste des indices.

La fonction `rattraper` se réduit alors à sommer les distances entre les indices successifs.

# Autre solution

Voici une autre solution, qui utilise la fonction valeur absolue `abs` dans
le calcul de la diistance et la fonction de tri `sorted` pour trier la
liste des indices.


```python
def distance(i, j, n):
    """ Renvoie le nombre minimum de déplacements à effectuer pour se rendre
    à l'indice j en partant de l'indice i dans un tableau de longueur n """
    return min(abs(i-j), n-abs(i-j))


def trier_indices(tab):
    """ Renvoie la liste des indices des nombres du tableau tab
    trié suivant l'ordre croissant des nombres du tableau tab """
    ti = [(x, i) for i,x in enumerate(tab)]
    return [i for t,i in sorted(ti)]


def rattraper(tab):
    """ Renvoie le nombre minimal de déplacements à effectuer pour rattraper
    chaque nombre du tableau tab, dans l'ordre croissant de leur valeur """
    dist = 0
    indices = trier_indices(tab)
    for k in range(1, len(tab)):
        i, j = indices[k-1], indices[k]
        dist += distance(i, j, len(tab))
    return dist
```

