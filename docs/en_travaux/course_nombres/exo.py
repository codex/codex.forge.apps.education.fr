

# --------- PYODIDE:code --------- #

def distance(i, j, n):
    """ Renvoie le nombre minimum de déplacements à effectuer pour se rendre
    à l'indice j en partant de l'indice i dans un tableau de longueur n """
    ...


def trier_indices(tab):
    """ Renvoie la liste des indices des nombres du tableau tab
    trié suivant l'ordre croissant des nombres du tableau tab """
    ...


def rattraper(tab):
    """ Renvoie le nombre minimal de déplacements à effectuer pour rattraper
    chaque nombre du tableau tab, dans l'ordre croissant de leur valeur """
    ...

# --------- PYODIDE:corr --------- #

def distance(i, j, n):
    """ Renvoie le nombre minimum de déplacements à effectuer pour se rendre
    à l'indice j en partant de l'indice i dans un tableau de longueur n """
    if i > j:
        dist = i - j
    else:
        dist = j - i
    return min(dist, n-dist)  # deplacements direct ou par les extrémités


def trier_indices(tab):
    """ Renvoie la liste des indices des nombres du tableau tab
    trié suivant l'ordre croissant des nombres du tableau tab """
    n = len(tab)
    # on commence par associer les nombres à leurs indices dans tab
    tab_ind = [(tab[i], i) for i in range(n)]
    # on effectue un tri par sélection mais on peut utiliser la méthode sort()
    for i in range(n-1):
        imin = i
        for j in range(i+1, n):
            if tab_ind[imin][0] > tab_ind[j][0]:
                imin = j
        tab_ind[i], tab_ind[imin] = tab_ind[imin], tab_ind[i]
    # on renvoie uniquement la liste des indices
    return [tab_ind[i][1] for i in range(n)]


def rattraper(tab):
    """ Renvoie le nombre minimal de déplacements à effectuer pour rattraper
    chaque nombre du tableau tab, dans l'ordre croissant de leur valeur """
    indices = trier_indices(tab)
    n = len(tab)
    i = indices[0]
    total = 0
    for k in range(1, n):
        j = indices[k]
        total += distance(i, j, n)
        i = j
    return total


# Tests
assert distance(0, 4, 5) == 1
assert distance(2, 3, 5) == 1
assert distance(4, 1, 5) == 2

assert trier_indices([1, 2, 3, 4]) == [0, 1, 2, 3]
assert trier_indices([4, 3, 2, 1]) == [3, 2, 1, 0]
assert trier_indices([3, 1, 2, 4, 5]) == [1, 2, 0, 3, 4]

assert rattraper([1, 2, 3, 4, 5]) == 4
assert rattraper([3, 1, 2, 4, 5]) == 6
assert rattraper([4, 3, 2, 1]) == 3
assert rattraper([5, 7, 4]) == 2

# --------- PYODIDE:tests --------- #

assert distance(0, 4, 5) == 1
assert distance(2, 3, 5) == 1
assert distance(4, 1, 5) == 2

assert trier_indices([1, 2, 3, 4]) == [0, 1, 2, 3]
assert trier_indices([4, 3, 2, 1]) == [3, 2, 1, 0]
assert trier_indices([3, 1, 2, 4, 5]) == [1, 2, 0, 3, 4]

assert rattraper([1, 2, 3, 4, 5]) == 4
assert rattraper([3, 1, 2, 4, 5]) == 6
assert rattraper([4, 3, 2, 1]) == 3
assert rattraper([5, 7, 4]) == 2

# --------- PYODIDE:secrets --------- #

assert distance(0, 4, 5) == 1, "distance numéro 1"
assert distance(2, 3, 5) == 1, "distance numéro 2"
assert distance(3, 2, 5) == 1, "distance numéro 3"
assert distance(4, 1, 5) == 2, "distance numéro 4"
assert distance(1, 4, 6) == 3, "distance numéro 5"

assert trier_indices([1, 2, 3, 4]) == [0, 1, 2, 3], "trier_indices numéro 1"
assert trier_indices([4, 3, 2, 1]) == [3, 2, 1, 0], "trier_indices numéro 2"
assert trier_indices([3, 1, 2, 4, 5]) == [1, 2, 0, 3, 4], "trier_indices numéro 3"
assert trier_indices([3, 6, 9, 2, 4]) == [3, 0, 4, 1, 2], "trier_indices numéro 4"
assert trier_indices([9, 4, 8, 7, 2, 6, 1, 0, 3, 5]) == [7, 6, 4, 8, 1, 9, 5, 3, 2, 0], "trier_indices numéro 5"

assert rattraper([1, 2, 3, 4, 5]) == 4, "rattraper numéro 1"
assert rattraper([4, 3, 2, 1]) == 3, "rattraper numéro 2"
assert rattraper([5, 7, 4]) == 2, "rattraper numéro 3"
assert rattraper([3, 1, 2, 4, 5]) == 6, "rattraper numéro 4"
assert rattraper([9, 4, 8, 7, 2, 6, 1, 0, 3, 5]) == 21, "rattraper numéro 5"
