---
author: Jean Diraison
difficulty: 350
hide:
    - navigation
    - toc
title: Course aux plus grands
tags:
    - en travaux
---

# La course aux plus grands

Vous êtes le plus petit nombre d'un tableau et vous devez rattraper, dans l'ordre croissant, chacun des nombres qui vous est supérieur en vous déplaçant le moins possible. Pour réduire vos déplacements, vous devrez parfois passer les limites du tableau afin de vous retrouver automatiquement transporté à l'autre bout.

## Exemple

Prenons l'exemple du tableau `[3, 1, 2, 4, 5]`.

- Vous êtes le 1 (à l'indice 1) et commencez par rejoindre le 2 (à l'indice 2) en avançant de 1 position.
- Vous rejoignez ensuite le 3 (à l'indice 0) en reculant de 2 positions.
- Vous atteignez alors le 4 (à l'indice 3) en reculant de 2 positions (dans l'autre sens il faudrait avancer de 3).
- Vous arrivez enfin au 5 (à l'indice 4) en avançant de 1 position.

Vous totalisez ainsi 6 déplacements (1+2+2+1) pour rattraper tous vos successeurs.

![Exemple 3-1-2-4-5](exemple-31245.gif)

## Attendus

Votre challenge consiste à créer une fonction `rattraper` qui prend en paramètre le tableau `tab` d'entiers distincts et qui renvoie le nombre minimum de déplacements à effectuer pour rattraper tous les successeurs, du plus petit nombre, dans l'ordre croissant.

Pour mieux structurer votre solution, vous commencerez par créer une fonction `distance` de paramètres `i`, `j` et `n` représentant respectivement l'indice de départ et l'indice d'arrivé dans un tableau de longueur `n` et qui renvoie le nombre minimum de déplacements à effectuer pour se rendre à l'indice `j` en partant de l'indice `i`.

Vous créerez aussi la fonction `trier_indices` dont le seul paramètre `tab` est le tableau des entiers distincts et qui renvoie la liste des indices des nombres du tableau `tab` triée suivant l'ordre croissant des entiers de `tab`.


???+ example "Exemple"

    ```pycon title=""
    >>> distance(1, 3, 5)
    2
    >>> distance(0, 4, 5)
    1
    >>> trier_indices([3, 1, 2, 4, 5])
    [1, 2, 0, 3, 4]
    >>> rattraper([3, 1, 2, 4, 5])
    6
    ```

{{ IDE('exo') }}

