# --------- PYODIDE:env --------- #
def indice_prochain_True(booleens, indice):
    pass


# --------- PYODIDE:code --------- #
def indice_prochain_True(booleens, i):
    taille = ...
    i = ...
    if i == ...:
        i = 0
    while not booleens[...]:
        i = ...
        if ...:
            ...
    return ...


# --------- PYODIDE:corr --------- #
def indice_prochain_True(booleens, i):
    taille = len(booleens)
    i += 1
    if i == taille:
        i = 0
    while not booleens[i]:
        i += 1
        if i == taille:
            i = 0
    return i


# --------- PYODIDE:tests --------- #
booleens = [True, True, False, True, False]
assert indice_prochain_True(booleens, 0) == 1
assert indice_prochain_True(booleens, 1) == 3
assert indice_prochain_True(booleens, 3) == 0
# --------- PYODIDE:secrets --------- #
booleens = [True, True, False, False] * 10
for i in range(len(booleens)):
    attendu = (i + 1) if (i % 4 == 0) else (4 * (i // 4 + 1)) % 40
    assert indice_prochain_True(booleens, i) == attendu, f"Erreur avec {booleens, i}"


