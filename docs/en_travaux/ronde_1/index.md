---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Jeu de la ronde (1)
tags:
    - à trous
    - booléen
    - int
    - en travaux
difficulty: 220
---

Des enfants jouent dans une cour de récréation : ils se placent en rond et choisissent au hasard l'un d'entre eux.

A partir de là, la règle est simple :

* Le premier enfant tape sur l'épaule de son voisin de gauche : celui-ci est éliminé. Au début l'enfant $1$ élimine donc l'enfant $2$ ;

* On passe à l'enfant restant suivant et l'on recommence : il élimine celui situé à sa gauche. Dans l'exemple, l'enfant $3$ élimine le $4$ ;
* On continue encore et encore jusqu'à ce qu'il ne reste plus qu'un seul enfant : c'est le gagnant ! 

![Le jeu de la ronde](ronde.svg){ width="30%" .center }

Dans le cas où $13$ enfants jouent comme sur la figure, le gagnant est l'enfant n°$11$.

La question est simple : qui va gagner ?

Dans cet exercice on utilise un tableau de booléen nommé `joueurs` dans lequel chaque valeur indique que le joueur concerné est encore en jeu (`True`) ou a déjà été éliminé  (`False`).

!!! caution "Attention"

    Les indices des listes Python étant comptabilisés à partir de `#!py 0`, avoir `#!py joueurs[2]` égal à `False` signifie que le joueur n°$3$ a été éliminé.

Ce tableau sera initialisé avec autant de valeurs `True` qu'il y a de joueurs.

La partie se déroulera ainsi :

* on garde trace de l'indice du joueur qui doit jouer ;

* on cherche dans le tableau l'indice du prochain joueur encore en jeu ;

* on « élimine » ce joueur ;

* on cherche l'indice du prochain joueur encore en jeu : ce sera à lui de jouer.

Ces différentes étapes continuent jusqu'à ce qu'il ne reste plus qu'un seul joueur encore en jeu.

Compléter les fonctions ci-dessous.

??? question "Fonction `indice_prochain_True`"

    La fonction `indice_prochain_True` prend en paramètres une liste de booléens ainsi qu'un indice et renvoie l'indice de la prochaine valeur de la liste égale à `True`.
    
    On prendra soin de « boucler » les indices : si celui-ci dépasse la longueur de la liste, on le ramène à `#!py 0`.
  
    On garantit que la liste `booleens` contient au moins une valeur `True`.

    ???+ example "Exemples"

        ```pycon title=""
        >>> booleens = [True, True, False, True, False]
        >>> indice_prochain_True(booleens, 0)
        1
        >>> indice_prochain_True(booleens, 1)
        3
        >>> indice_prochain_True(booleens, 3)
        0
        ```

    === "Version vide"
        {{ IDE('exo_true_vide')}}
    === "Version à compléter"
        {{ IDE('exo_true_trous')}}

??? question "Fonction `gagnant`"

    La fonction `gagnant` prend en paramètre le nombre de joueurs au début de la partie (`#!py n > 0`) et renvoie la position du joueur gagnant.
    
    Attention, les positions des joueurs sont comptabilisées à partir de $1$.

    ???+ example "Exemples"

        ```pycon title=""
        >>> gagnant(1)
        1
        >>> gagnant(5)
        3
        >>> gagnant(13)
        11
        ```

    === "Version vide"
        {{ IDE('exo_ronde_vide')}}
    === "Version à compléter"
        {{ IDE('exo_ronde_trous')}}