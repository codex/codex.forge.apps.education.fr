On débute par incrémenter la valeur de `i` afin de passer au « joueur » suivant. Si l'indice dépasse la taille du tableau on lui donne la valeur `#!py 0`.

On répète cet incrément dans une boucle jusqu'à rencontrer une valeur égale à `#!py True`.
