Dans la fonction `gagnant`, on commence par créer la liste de booléens, tous égaux à `#!py True` (tous les joueurs sont encore en jeu).

On désigne le joueur dont c'est le tour avec `i` (à `#!py 0` au début pour le joueur $1$).

On lance ensuite la partie qui continue jusqu'à ce que le nombre de joueurs soit égal à `#!py 1`.

A chaque étape :

* on cherche l'indice du prochain joueur encore en jeu ;
* on  l'élimine ;
* on passe au joueur suivant en effectuant une nouvelle recherche.

A la fin de la fonction, on renvoie `#!py i + 1` afin de tenir compte du décalage entre les indices dans la liste et les numéros des joueurs.