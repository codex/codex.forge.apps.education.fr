# Exercices en cours de rédaction

Les exercices de cette section sont en cours de rédaction et peuvent donc comporter quelques imprécisions, fautes d'orthographes voire, parfois, des *bugs* 🐛...

Merci de contacter les auteurs si vous pensez à une amélioration possible ou si l'exercice vous semble tout à fait satisfaisant !