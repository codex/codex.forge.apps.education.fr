---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Jouons au golf !
tags:
    - à trous
    - en travaux
    - glouton
    - grille
difficulty: 320
---

On cherche dans cet exercice à simuler le déplacement d'une balle de golf roulant sur le *green*.

Le terrain sera représenté par une liste Python dont chaque élément est une liste de nombres entiers positifs. Les nombres contenus dans les sous-listes sont les hauteurs des différents points du terrain.

On donne un exemple ci-dessous (en bleu foncé les points de faible hauteur, en rouge foncé ceux de grande hauteur).

![terrain en 2D](images/grille8.png){ width=40% .center}

La liste Python correspondant à ce terrain est donnée ci-dessous :

```python title=""
terrain = [
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
    [100,  39,  17,  16,  30,  39,  56,  62,  51, 100],
    [100,  39,  18,   4,  10,  20,  43,  62,  53, 100],
    [100,  27,  22,  12,  18,  34,  43,  46,  42, 100],
    [100,  15,  23,  19,  30,  49,  45,  38,  30, 100],
    [100,  39,  43,  25,  27,  39,  36,  40,  40, 100],
    [100,  64,  59,  38,  36,  43,  39,  31,  31, 100],
    [100,  60,  49,  38,  42,  47,  55,  40,  22, 100],
    [100,  51,  38,  33,  45,  43,  49,  49,  28, 100],
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
]
```

On peut vérifier dans la grille que le point de la ligne `#!py 2` et de la colonne `#!py 3` est le plus bas du terrain : `#!py terrain[2][3]` vaut `#!py 4` qui est la valeur minimale.

??? example "Un grand terrain de jeu"

    La grille peut aussi prendre de grandes dimensions.
    
    ![terrain en 2D](images/grille64.png){ width=40% .center}
    
    On se rend alors compte plus facilement de l'existence de plusieurs « points bas locaux » :
    
    * le point le plus bas de **l'ensemble du terrain** a pour coordonnées `#!py (50, 3)` et sa hauteur vaut `#!py 16` ;

    * une bille lâchée dans le carré entouré en vert finira néanmoins sa course dans un « minimum local », le point de coordonnées `#!py (26, 22)` et de hauteur égale à `#!py 30`.

On garantit que toutes les sous-listes ont la même dimension. On précise de plus que les points des quatre bords ont tous des hauteurs strictement supérieures aux points de l'intérieur du terrain.

La balle se trouve initialement sur la ligne `i` et la colonne `j`. La hauteur du point correspondant est donc `#!py terrain[i][j]`.

Depuis cette position initiale, la balle va « rouler » vers l'une des cases adjacentes dont la hauteur est **strictement inférieure** à celle de sa position actuelle.

Elle peut se déplacer dans 8 directions :

* ↖ en haut à gauche,
* ↑ en haut,
* ↗ en haut à droite,
* ← à gauche,
* → à droite,
* ↙ en bas à gauche,
* ↓ en bas,
* ↘ en bas à droite.

Ces déplacements sont déclarés dans la liste Python ci-dessous :

```python title=""
DEPLACEMENTS = [
    (-1, -1), ( -1, 0), (-1, +1),
    ( 0, -1),           ( 0, +1),
    (+1, -1), (+1,  0), (+1, +1)
]
```

On pourra facilement parcourir ces déplacements et récupérer les déplacements horizontaux et verticaux à l'aide d'une boucle de la forme `#!py for di, dj in DEPLACEMENTS:`.

Dans le cas où plusieurs cases adjacentes ont des hauteurs inférieures à la case de la balle, celle-ci ira vers la case dont la hauteur est minimale (ce déplacement entraîne donc une variation maximale).

La bille roule jusqu'à arriver sur un point de hauteur minimale : tous les points voisins ont une hauteur **supérieure ou égale** à la sienne.

Écrire la fonction `rouler` :

* prenant en arguments le `terrain` (liste de listes) et la position de départ (numéro de ligne `i` et numéro de colonne `j`),

* et renvoyant la position finale de la balle sous forme d'un tuple.

???+ example "Exemple"

    On utilise le terrain de jeu défini plus haut.
    
    ```pycon title=""
    >>> rouler(terrain, 6, 1)
    (4, 1)
    >>> rouler(terrain, 1, 1)
    (2, 3)
    >>> rouler(terrain, 1, 7)
    (2, 3)
    ```

??? tip "Aide"

    L'algorithme permettant de déterminer la position finale peut être construit ainsi :

    * on parcourt l'ensemble des déplacements possibles afin de déterminer celui qui entraîne la variation de hauteur maximale. Lors de la recherche, la valeur du maximum est initialisée à `#!py 0` (pas de déplacement),
    
    * dans le cas où deux déplacements entraînent la même variation, on convient de **conserver le premier rencontré** dans la liste des déplacements possibles,
    
    * ce déplacement optimal trouvé, on l'applique à la balle.


=== "Version vide"
    {{ IDE('exo_vide') }}
=== "Version à compléter"
    {{ IDE('exo_trous') }}
