# --------- PYODIDE:code --------- #
def double(tableau) :
    for i in range(6):
        tableau[i] = tableau[i]*2

            

# --------- PYODIDE:corr --------- #
def double(tableau) :
    for i in range(len(tableau)):
        tableau[i] = tableau[i]*2



# --------- PYODIDE:tests --------- #
tableau_entiers = [5, 4, 6, 9, 8, 10]
double(tableau_entiers)
assert tableau_entiers == [10, 8, 12, 18, 16, 20]


# --------- PYODIDE:secrets --------- #
tableau_entiers = [5, 4, 6, 9, 8]
double(tableau_entiers) 
assert tableau_entiers == [10, 8, 12, 18, 16]


