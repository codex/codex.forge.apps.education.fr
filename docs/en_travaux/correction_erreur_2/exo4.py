

# --------- PYODIDE:code --------- #
def moyenne(tab_valeurs):
    somme = 0
    for val in tab_valeurs :
        somme = somme + val
    moyenne = somme/len(tab_valeurs)
    return "la moyenne est de "+ moyenne


            

# --------- PYODIDE:corr --------- #
def moyenne(tab_valeurs):
    somme = 0
    for val in tab_valeurs :
        somme = somme + val
    moyenne = somme/len(tab_valeurs)
    return "la moyenne est de "+ moyenne





# --------- PYODIDE:tests --------- #
notes = [16, 12, 12, 16]
assert moyenne(notes) == "la moyenne est de 14"

# --------- PYODIDE:secrets --------- #
notes = [12, 12, 12, 12, 12]
assert moyenne(notes) == "la moyenne est de 12"
