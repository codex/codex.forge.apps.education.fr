# --------- PYODIDE:code --------- #
def moyenne_math(tab_notes):
    somme = 0
    for notes in tab_notes :
        somme = somme + notes["mathématiques"]
    moyenne = somme/len(notes)
    return moyenne

            

# --------- PYODIDE:corr --------- #
def moyenne_math(tab_notes):
    somme = 0
    for notes in tab_notes :
        somme = somme + notes["mathématiques"]
    moyenne = somme/len(notes)
    return moyenne



# --------- PYODIDE:tests --------- #
releves = [{"francais":14, "maths":12, "histroire":11},
           {"francais":17, "maths":8, "histroire":16},
           {"francais":14, "maths":16, "histroire":13}]
assert moyenne_math(releves) == 12


# --------- PYODIDE:secrets --------- #
releves = [{"francais":14, "maths":10, "histroire":11},
           {"francais":17, "maths":10, "histroire":16},
           {"francais":14, "maths":10, "histroire":13}]
assert moyenne_math(releves) == 10