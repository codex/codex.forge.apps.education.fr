---
author:
    - Pierre Marquestaut
    - Nicolas Reveret
hide:
    - navigation
    - toc
title: Correction d'erreurs (2)
tags:
    - en travaux
difficulty: 150
---

Une fois la syntaxe vérifiée, l'interpréteur lance l'exécution et d'autres exceptions peuvent alors être levées. 

!!! abstract "Anatomie d'une exception"
    Commençons par générer un « bug » en exécutant le code ci-dessous :
    {{IDE('script')}}

    Commençons par la fin. On trouve sur la dernière ligne deux informations :

    * le type d'erreur (ici `TypeError`) ;
    * un message expliquant plus précisément l'erreur.

    Les lignes précédentes constituent le traceback. Il faut le lire de bas en haut :

    * la dernière ligne indique l'instruction qui a provoqué cette erreur,
    * les lignes du dessus remontent la « généalogie » de cette erreur.

    Ici on lit donc (de bas en haut):

    |**Sortie dans la console**      |**Commentaire**|
    |:------------------------------------------------------:|:------------------:|
    |Traceback (most recent call last):                    |                	|
    |File "<exec>", line 7, in <module>                    | 	L'appel qui a lancé toute la démarche était à la ligne 7, dans le corps du programme.|
    |File "<exec>", line 5, in f 	                       |L'appel du dessus a été effectué à la ligne 5, dans la fonction f|
    |File "<exec>", line 2, in ajoute_5                     |	L'erreur a été provoquée par l'instruction à la ligne 2, dans la fonction ajoute_5|
    |TypeError: can only concatenate str (not "int") to str |	L'erreur est une TypeError car on cherche à concaténer un str et un int|



Corriger chaque programme pour qu'il puisse s'exécuter sans erreur.



??? question "Programme 1"

    {{IDE('exo1')}}

??? question "Programme 2"

    {{IDE('exo2')}}

??? question "Programme 3"

    {{IDE('exo3')}}

??? question "Programme 4"

    {{IDE('exo4')}}

??? question "Programme 5"

    {{IDE('exo5')}}