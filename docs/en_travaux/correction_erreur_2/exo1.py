# --------- PYODIDE:code --------- #
def somme_des_inverses(n) :
    '''Calcul la somme 1/1 + 1/2 + ... + 1/n'''
    total = 0
    for i in range(n) :
        total = total + 1/i
    return total

            

# --------- PYODIDE:corr --------- #
def somme_des_inverses(n) :
    '''Calcul la somme 1/1 + 1/2 + ... + 1/n'''
    total = 0
    for i in range(n) :
        total = total + 1/(i + 1)
    return total



# --------- PYODIDE:tests --------- #
assert abs(2.083333333333333 - somme_des_inverses(4)) < 1e-6
assert abs(1.5 - somme_des_inverses(2)) < 1e-6

# --------- PYODIDE:secrets --------- #
assert somme_des_inverses(1) == 1
