# --------- PYODIDE:code --------- #
def somme(tab_valeurs):
    for valeur in tab_valeurs :
        somme_entiers = somme_entiers + valeur
    return somme_entiers

            

# --------- PYODIDE:corr --------- #
def somme(tab_valeurs):
    somme = 0
    for valeur in tab_valeurs :
        somme_entiers = somme_entiers + valeur
    return somme_entiers



# --------- PYODIDE:tests --------- #
notes = [15, 12, 10, 16]
assert somme(notes) = 53

# --------- PYODIDE:secrets --------- #
notes = [1, 1, 1, 1, 1]
assert somme(notes) = 5



