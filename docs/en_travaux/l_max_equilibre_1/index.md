---
license: "by-nc-sa"
author: Franck Chambon
difficulty: 350
hide:
    - navigation
    - toc
title: Intervalle équilibré
tags:
    - en travaux
    - boucle
---
# Longueur maximale d'intervalle équilibré

On considère une liste `bits` constituée uniquement de 0 et 1. Écrire une fonction telle que `l_max_equilibree(bits)` renvoie la longueur maximale d'une tranche qui contient autant de 0 que de 1.

???+ example "Exemples"

    ```pycon
    >>> bits = [0, 0, 1, 0, 0, 0, 1, 1, 0, 0]
    >>> l_max_equilibree(bits)
    6
    ```
    En effet, la tranche `[1, 0, 0, 0, 1, 1]` est équilibrée et de longueur maximale.

    ```pycon
    >>> bits = [1, 1, 1, 1]
    >>> l_max_equilibree(bits)
    0
    ```
    En effet, la tranche `[]` est équilibrée et de longueur maximale.

    ```pycon
    >>> bits = [1, 0, 1, 0, 1]
    >>> l_max_equilibree(bits)
    4
    ```
    En effet, la tranche `[1, 0, 1, 0]` est équilibrée et de longueur maximale. Il y a également `[0, 1, 0, 1]` équilibrée et aussi de longueur maximale.


On écrira ici un algorithme qui fait une double boucle. Vous retrouverez ensuite cet exercice à faire avec une simple boucle.

{{ IDE('exo') }}


??? tip "Indice 1"
    On pourra construire une liste des cumuls du nombre de 1 de l'indice 0 jusqu'à l'indice `i` exclu.

    Par exemple, avec `bits = [1, 1, 0, 0, 1]`,

    on aurait `cumul = [0, 1, 2, 2, 2, 3]` qui possède un élément de plus que `bits`.

??? tip "Indice 2"
    On pourra faire une boucle pour tous les indices de fin d'une tranche et pour chaque indice de fin, une boucle pour chaque indice de début possible.

??? tip "Indice 3"
    Pour un indice de début et de fin, on a une largeur de tranche et on peut avec une soustraction de deux valeurs de `cumul` connaitre le nombre de 1. On peut donc facilement savoir si la tranche est équilibrée.

??? tip "Indice 4"
    On pourra compléter le code

    ```python
    def l_max_equilibree(bits):
        cumul = [0]
        for x in bits:
            cumul.append(...)
        l_max = ...
        for i_fin in range(...):
            for i_debut in range(...):
                if ...:
                    if ... > l_max:
                        l_max = ...
        return ...
    ```
