# --------- PYODIDE:env --------- #

def arithmetique(N):
    pass


# --------- PYODIDE:code --------- #

def arithmetique(N):
    return [... for n in range(...)]

# --------- PYODIDE:corr --------- #

def arithmetique(N):
    return [5 + 10 * n for n in range(N)]


# --------- PYODIDE:tests --------- #

assert arithmetique(2) == [5, 15]
assert arithmetique(3) == [5, 15, 25]
assert arithmetique(4) == [5, 15, 25, 35]
assert arithmetique(10) == [5, 15, 25, 35, 45, 55, 65, 75, 85, 95]

# --------- PYODIDE:secrets --------- #


# tests secrets
assert arithmetique(100)[99] == 995