La solution propose la construction du tableau par ajouts successifs.

Etant donné que l'on connait en début de fonction le nombre d'élément du tableau, la création du tableau aurait également pu se faire par construction :

```python
termes = [0 for k in range(N)]
```

puis complété en utilisant l'indice :

```python
for n in range(N):
    termes[i] = 5 + 10 * i
```