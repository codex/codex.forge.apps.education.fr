# --------- PYODIDE:env --------- #
import matplotlib
# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécifique (id="cible")
from js import document 
document.pyodideMplTarget = document.getElementById("cible")
# on vide la div si elle contenait du texte
document.getElementById("cible").textContent = ""

def arithmetique(N):
    return [5 + 10 * n for n in range(N)]

# --------- PYODIDE:code --------- #

import matplotlib.pyplot as plt
n =10
x = [i for i in range(n)]
y = arithmetique(n)

# Tracé des éléments de la suite
plt.plot(range(n), y, 'og')
plt.show()