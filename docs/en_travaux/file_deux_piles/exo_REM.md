Dans cet exercice, on utilise une structure de pile dont les opérations d'empilement et de dépilement se font à coût constant $O(1)$.

Qu'en est-il de la structure de file constituée de deux piles ?


* L'opération d'**enfilement** consiste à empiler l'élément dans la pile d'entrée. Cette opération se fait donc à coût constant.

* Lorsqu'un seul élément dans la file, l'opération de **défilement** revient à réaliser trois opérations : dépiler l'élément de la pile d'entrée, l'empiler dans la pile de sortie puis le dépiler de cette même pile. Ces trois opérations se font chacune à coût constant et donc l'opération de défilement est également à coût constant.

![défiler](images/meilleur_cas.svg){ width=40% }

Dans le pire des cas, pour défiler le premier éléments d’une file à $n$ éléments avec les $n$ éléments dans `p1`, on a $2n$ opérations pour dépiler chaque élément de `p1` et l’empiler sur `p2`, puis une opération de dépilement, soit $2n+1$ opérations : le coût est donc linéaire.

![défiler](images/pirecas.svg){ width=40% }

En revanche, les éléments suivants n'auront besoin que d'une seule opération pour être défilé.

![défiler](images/pirecas2.svg){ width=40% }

Ainsi, dans le pire des cas, pour défiler n éléments d’une file à n éléments, on a $2n$ opérations pour dépiler chaque élément de `p1` et l’empiler sur `p2`, puis une opération de dépilement pour chaque élément, donc un total de $3n$ opérations. En moyenne le défilement d’un élément coûte donc $3n/n=3$ opérations.

Le coût global est constant, on parle ici de **coût amorti**.
