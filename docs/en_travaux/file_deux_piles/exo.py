

# --------- PYODIDE:env --------- #
valide = [False] * 4

def creer_pile_vide():
    '''Renvoie une liste vide'''
    global valide
    valide[0] = True
    return []

def est_pile_vide(p):
    '''Renvoie True si la pile est vide'''
    global valide
    valide[1] = True
    return p == []

def empiler(p, elt) :
    '''
    Renvoie une nouvelle pile où elt est le sommet
    et pile la suite de notre nouvelle pile.
    '''
    global valide
    valide[2] = True
    p.append(elt)



def depiler(p):
    '''
    Supprime le sommet et le renvoie 
    '''
    global valide
    valide[3] = True
    return p.pop()

# --------- PYODIDE:code --------- #

def creer_file_vide():
    ...

def est_file_vide(file):
    ...

def enfiler(file, valeur):
    ...

def defiler(file):
    ...

# --------- PYODIDE:corr --------- #

def creer_file_vide():
    '''Renvoie une file vide'''
    return [creer_pile_vide(), creer_pile_vide()]

def est_file_vide(file):
    '''Renvoie True si la file est vide'''
    return est_pile_vide(file[0]) and est_pile_vide(file[1])

def enfiler(file, valeur):
    '''Ajoute l'élément valeur à la file'''
    empiler(file[0], valeur)

def defiler(file):
    '''
    Défile un élément de la file
    Renvoie cet élément
    '''
    if est_pile_vide(file[1]):
        while not est_pile_vide(file[0]):
            valeur =  depiler(file[0])
            empiler(file[1], valeur)

    return depiler(file[1])

# --------- PYODIDE:tests --------- #

ma_file = creer_file_vide()
assert est_file_vide(ma_file) is True
enfiler(ma_file, 1)
enfiler(ma_file, 2)
assert est_file_vide(ma_file) is False
enfiler(ma_file, 3)
enfiler(ma_file, 4)
enfiler(ma_file, 5)
assert defiler(ma_file) == 1
assert defiler(ma_file) == 2

# --------- PYODIDE:secrets --------- #


# autres tests
ma_file = creer_file_vide()
assert est_file_vide(ma_file) is True
enfiler(ma_file, "a")
assert defiler(ma_file) == "a"
enfiler(ma_file, "b")
assert est_file_vide(ma_file) is False
enfiler(ma_file, "c")
enfiler(ma_file, "d")
enfiler(ma_file, "e")
assert defiler(ma_file) == "b"
assert defiler(ma_file) == "c"

assert valide == [True] * 4,  "Les fonctions fournies n'ont pas été utilisées"