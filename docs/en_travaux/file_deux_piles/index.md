---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: File avec deux piles
tags:
    - en travaux
    - structure linéaire
difficulty: 250
---


On choisit d'implémenter une `file` à l'aide d'un couple `[p1, p2]` où `p1` et `p2` sont des piles. Ainsi `file[0]` et `file[1]` sont respectivement les piles `p1` et `p2`.

??? note "Enfiler une valeur"

    Pour enfiler un nouvel élément `valeur` dans `file`, on l'empile dans `p1`.

    ![enfiler](images/file2pile1.svg){ width=40% }

??? note "Défiler une valeur"

    Pour défiler `file`, deux cas se présentent :

    === "La pile `p2` n'est pas vide"

        Dans ce cas, on dépile `p2`.

        ![défiler](images/file2pile2.svg){ width=40% }

    === "La pile `p2` est vide"

        Dans ce cas, on dépile les éléments de `p1` en les empilant dans `p2` jusqu'à ce que `p1` soit vide, puis on dépile `p2`.

        ![défiler](images/file2pile3.svg){ width=40% }


??? info "Interface des piles"
    La structure de pile est utilisable au travers de leur interface constituée des fonctions suivantes :

    * `creer_pile_vide` : renvoie une pile vide.
    * `depiler` : prend en paramètre une `pile` et renvoie le sommet qui est dépilé.
    * `empiler` : prend un `element` quelconque et une `pile` et renvoie une nouvelle pile où `element` est le sommet.
    * `est_pile_vide` : prend en paramètre une `pile` et renvoie True si la pile est vide.
    

    ```pycon
    >>> ma_pile = creer_pile_vide()
    >>> est_pile_vide(ma_pile)
    True
    >>> empiler(ma_pile, 5)
    >>> empiler(ma_pile, 4)
    >>> depiler(ma_pile)
    4
    ```

A partir des opérations sur les piles fournis dans le module, **écrire** en Python les fonctions suivantes :

  * une fonction `creer_file_vide()` qui renvoie une `file` vide de tout élément.
  * une fonction `est_file_vide(file)` qui prend en argument une `file` et qui renvoie `True` si la `file` est vide, `False` sinon.
  * une fonction `enfiler(file, valeur)` qui prend en arguments une `file` et un élément `valeur` et qui ajoute `valeur` en queue de la `file`.
  * une fonction `defiler(file)` qui prend en argument une `file` et qui renvoie l'élément en tête de la `file` en le retirant.

!!! example "Exemple"
    ```pycon
    >>> ma_file = creer_file_vide()
    >>> est_file_vide(ma_file)
    True
    >>> enfiler(ma_file, 1)
    >>> enfiler(ma_file, 2)
    >>> est_file_vide(ma_file)
    False
    >>> defiler(ma_file)
    1
    >>> defiler(ma_file)
    2
    ```

!!! warning "Contraintes"
    On doit utiliser exclusivement les fonctions fournies. 
  
    Aucune connaissance de la manière dont sont stockées les valeurs de la pile n'est nécessaire.

{{ IDE('exo') }}

