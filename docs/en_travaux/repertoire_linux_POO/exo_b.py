# --------- PYODIDE:env --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}  # dictionnaire vide
        Fichier.affiche = affiche

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

# --------- PYODIDE:code --------- #
def affiche(self, chaine=""):
    print(chaine, self.nom)
    if ...:
        for fichier in self.liste_fichiers.values():
            ...

# --------- PYODIDE:tests --------- #
# Ceci n'est pas un vrai test. Il sera toujours validé, même si
# l'affichage n'est pas le bon
racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
user1 = home.cree_repertoire("user1")
user1.cree_fichier("log.txt")
repertoire_TP = user1.cree_repertoire("TP")
racine.affiche()

# --------- PYODIDE:corr --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def affiche(self, chaine=""):
        print(chaine, self.nom)
        if self.est_repertoire:
            for fichier in self.liste_fichiers.values():
                fichier.affiche(chaine+"--")

# --------- PYODIDE:secrets --------- #
import sys

__racine = Fichier("/", True)
__etc = __racine.cree_repertoire("etc")
__home = __racine.cree_repertoire("home")
__user1 = __home.cree_repertoire("user1")
__user1.cree_fichier("log.txt")
__repertoire_TP = __user1.cree_repertoire("TP")

__racine.affiche()
lignes_texte = sys.stdout.getvalue().rstrip().split("\n")
assert lignes_texte == [' /', '-- etc', '-- home', '---- user1', '------ log.txt', '------ TP'], "Mauvais affichage pour /"
sys.stdout.truncate(0)
sys.stdout.seek(0)

__home.affiche()
lignes_texte = sys.stdout.getvalue().rstrip().split("\n")
assert lignes_texte == [' home', '-- user1', '---- log.txt', '---- TP'], "Mauvais affichage pour home"
sys.stdout.truncate(0)
sys.stdout.seek(0)

__truc = __etc.cree_repertoire("truc")
__truc.cree_fichier("truc1.txt")
__truc.cree_fichier("truc2.txt")
__racine.affiche()
lignes_texte = sys.stdout.getvalue().rstrip().split("\n")
assert lignes_texte == [' /', '-- etc', '---- truc', '------ truc1.txt', '------ truc2.txt', '-- home', '---- user1', '------ log.txt', '------ TP'], "Mauvais affichage si on rajoute des fichiers et dossiers dans etc"
sys.stdout.truncate(0)
sys.stdout.seek(0)

__etc.affiche()
lignes_texte = sys.stdout.getvalue().rstrip().split("\n")
assert lignes_texte == [' etc', '-- truc', '---- truc1.txt', '---- truc2.txt'], "Mauvais affichage pour une autre structure de dossiers"
sys.stdout.truncate(0)
sys.stdout.seek(0)



