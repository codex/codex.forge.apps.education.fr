En Python, il n'est pas possible de modifier un dictionnaire pendant qu'on le parcourt.

La solution proposée ici est de créer une liste contenant les couples `(nom, fichier)` du dictionnaire et de parcourir cette liste.

Pour la créer, on a utilisé la méthode `item()`, propre à Python, mais on aurait également pu utiliser une construction par compréhension :

```python
noms_fichiers = [(nom, self.liste_fichiers[nom]) for nom in self.liste_fichiers]
```
