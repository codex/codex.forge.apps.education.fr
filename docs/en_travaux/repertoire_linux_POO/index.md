---
author: Pierre Marquestaut
hide:
    - navigation
    - toc
title: Répertoires sous Linux
tags:
    - à trous
    - en travaux
    - programmation orientée objet
    - récursivité
---

# Répertoires sous Linux

!!! abstract "Tout est fichier"
    Sous le système d'exploitation Linux, un **fichier** est une unité de stockage fondamentale qui contient des données ou des informations.
    Sont considérés comme fichiers :

    * les fichiers *réguliers* (images, textes, vidéos, programmes...),
    * les répertoires,
    * les liens,
    * les accès aux périphériques
    * etc...

    Autrement dit : « sous Linux, tout est fichier. »

Ainsi, la classe `Fichier` permet de représenter fichiers réguliers et répertoires.

```mermaid
classDiagram
class Fichier{
       nom: str
       est_repertoire: bool
       liste_fichiers: list
       cree_fichier(nom: str) Fichier
       cree_repertoire(nom: str) Fichier
       est_vide() 
       supprime(nom: str)
       affiche() 
       vide()
       vide_recursif()
}
```

* Chaque fichier est caractérisé par son `nom` et par un attribut `est_repertoire`, booléen qui indique si c'est un fichier ou un répertoire.
* S'il s'agit d'un répertoire, il dispose d'un attribut `liste_fichiers` qui est la liste des fichiers contenus dans le répertoire.


Un répertoire peut contenir des fichiers réguliers mais aussi d'autres répertoires qui peuvent également contenir des fichiers réguliers ou d'autres répertoires et ainsi de suite...

On présente ci-dessous un extrait de l'arborescence des répertoires sous Linux :

![arborescence](repertoire.svg)

Le répertoire `/` est le répertoire racine.

??? note "Code de la classe"
    On présente ici le code incomplet de la classe `Fichier` :

    ```python
    class Fichier:
        def __init__(self, nom, est_repertoire):
            self.nom = nom
            self.est_repertoire = est_repertoire
            self.liste_fichiers = None
            if est_repertoire:
                self.liste_fichiers = {}  # dictionnaire vide

        def cree_fichier(self, nom):
            pass

        def cree_repertoire(self, nom):
            pass

        def affiche(self, chaine=""):
            pass

        def est_vide(self):
            pass

        def supprime(self, cible):
            pass

        def vide(self):
            pass

        def vide_recursif(self):
            pass
    ```

{{ remarque('methodes') }}

??? question "Question 1"
    Dans le cas où un fichier est un répertoire, ses méthodes `cree_fichier()` et `cree_repertoire()` permettent respectivement de créer un nouveau fichier ou un nouveau répertoire, à l'intérieur du répertoire initial.

    Ainsi le code suivant permet de reproduire cette arborescence : 

    ```pycon
    >>> racine = Fichier("/", True)
    >>> etc = racine.cree_repertoire("etc")
    >>> home = racine.cree_repertoire("home")
    >>> user1 = home.cree_repertoire("user1")
    >>> user1.cree_fichier("log.txt")
    >>> repertoire_TP = user1.cree_repertoire("TP")
    >>> repertoire_TP.cree_fichier("TP1")
    >>> repertoire_TP.cree_fichier("TP2")
    ```
    Compléter les méthodes suivantes qui :
    
    * prennent en paramètre le `nom` du fichier à créer, 
    * créent ce fichier pour le mettre dans la liste des fichiers du répertoire,
    * et renvoie l'instance de ce fichier :

    {{ IDE('exo_a') }}

??? question "Question 2"
    Dans le cas où un fichier est un répertoire, la méthode `affiche()` permet d’afficher le contenu du répertoire. Si des éléments du répertoire sont eux-même des répertoires, leur contenu est affiché décalé par `#!py "--"`.

    ```pycon
    >>> racine.affiche()
    /
    -- etc
    -- home
    ---- user1
    ------ log.txt
    ------ TP
    -------- TP1.odt
    -------- TP2.odt
    ```

    Compléter la méthode `affiche` qui :

    * affiche (par la fonction `print`) le nom du fichier décalé par la `chaine` de caratères passée en paramètres,
    * si c'est un répertoire, affiche les fichiers qu'il contient, décalés par `#!py "--"`.

    {{ IDE('exo_b') }}

    ??? tips "Indice"
        La définition de la classe `Fichier` est récursive : un répertoire est un objet de type `Fichier` qui contient lui-même des objets de type `Fichier`.

        On peut appliquer les méthodes sur l'objet en lui-même, mais aussi sur les objets de type `Fichier` qu'il contient.

??? question "Question 3"
    La méthode `supprime` permet de supprimer un fichier dont le nom est passé en paramètre. Par contre, on ne peut supprimer un répertoire que si celui-ci est vide. 

    ```pycon
    >>> user1.supprime('log.txt')
    >>> user1.supprime('TP')
    "Le répertoire TP n'est pas vide"
    >>> racine.affiche()
    /
    -- etc
    -- home
    ---- user1
    ------ TP
    ```

    {{ IDE('exo_c') }}

??? question "Question 4"
    La méthode `vide` permet de supprimer l'ensemble des fichiers d'un répertoire. Par contre, on ne peut supprimer un répertoire que si celui-ci est vide.

    ```pycon
    >>> home.vide()
    "Le répertoire TP n'est pas vide"
    >>> racine.affiche()
    /
    -- etc
    -- home
    ---- user1
    ------ TP
    -------- TP1.odt
    -------- TP2.odt
    ```

    {{ IDE('exo_d') }}

??? question "Question 5"
    La méthode `vide_recursif` permet de supprimer l'ensemble des fichiers d'un répertoire. Si, parmi ces fichiers, un répertoire n'est pas vide, il est à son tour vidé avant d'être supprimé, et ainsi de suite...

    ```pycon
    >>> home.vide_recursif()
    >>> racine.affiche()
    /
    -- etc
    -- home
    ```

    Compléter le code de la classe.

        
    {{ IDE('exo_e') }}
