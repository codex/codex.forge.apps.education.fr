# --------- PYODIDE:env --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}  # dictionnaire vide
        Fichier.est_vide = est_vide
        Fichier.supprime = supprime

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

# --------- PYODIDE:code --------- #

def est_vide(self):
    ...


def supprime(self, cible):
    if cible in self.liste_fichiers:
        fichier = self.liste_fichiers[cible]
        if  not ... or ...:
            del self.liste_fichiers[cible]

# --------- PYODIDE:tests --------- #

racine = Fichier("/", True)
assert racine.est_vide() is True
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
assert racine.est_vide() is False
racine.supprime("etc")
assert sorted(racine.liste_fichiers.keys()) == ['home']

# --------- PYODIDE:corr --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier


    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def affiche(self, chaine=""):
        print(chaine, self.nom)
        if self.est_repertoire:
            for fichier in self.liste_fichiers.values():
                fichier.affiche(chaine+"--")

    def est_vide(self):
        return len(self.liste_fichiers)==0


    def supprime(self, cible):
        if cible in self.liste_fichiers:
            fichier = self.liste_fichiers[cible]
            if  not fichier.est_repertoire or fichier.est_vide():
                del self.liste_fichiers[cible]




# --------- PYODIDE:secrets --------- #

racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
assert sorted(racine.liste_fichiers.keys()) == ['etc', 'home'], sorted(racine.liste_fichiers.keys())
user1 = home.cree_repertoire("user1")
user1.cree_fichier("log.txt")
repertoire_TP = user1.cree_repertoire("TP")
assert sorted(user1.liste_fichiers.keys()) == ['TP', 'log.txt']
user1.supprime("log.txt")
assert sorted(user1.liste_fichiers.keys()) == ['TP']
