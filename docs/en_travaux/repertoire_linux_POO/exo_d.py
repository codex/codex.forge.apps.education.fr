# --------- PYODIDE:env --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}  # dictionnaire vide
        Fichier.vide = vide
   

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def est_vide(self):
        return len(self.liste_fichiers)==0
# --------- PYODIDE:code --------- #
def vide(self):
    noms_fichiers = list(self.liste_fichiers.items())

    for nom, fichier in noms_fichiers:
        if not ... or ...:
            del self.liste_fichiers[nom]
# --------- PYODIDE:tests --------- #

racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
user1 = home.cree_repertoire("user1")
user1.cree_fichier("log.txt")
repertoire_TP = user1.cree_repertoire("TP")
repertoire_TP.cree_fichier("TP1.odt")
repertoire_TP.cree_fichier("TP2.odt")

user1.vide()
assert sorted(user1.liste_fichiers.keys()) == ['TP']
# --------- PYODIDE:corr --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def vide(self):
        noms_fichiers = list(self.liste_fichiers.items())

        for nom, fichier in noms_fichiers:
            if not fichier.est_repertoire or fichier.est_vide():
                del self.liste_fichiers[nom]

# --------- PYODIDE:secrets --------- #

racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
user1 = home.cree_repertoire("user1")
user1.cree_fichier("log.txt")
repertoire_TP = user1.cree_repertoire("TP")
repertoire_TP.cree_fichier("TP1.odt")
repertoire_TP.cree_fichier("TP2.odt")

user1.vide()
assert sorted(user1.liste_fichiers.keys()) == ['TP']
repertoire_TP.vide()
user1.vide()
assert sorted(user1.liste_fichiers.keys()) == []