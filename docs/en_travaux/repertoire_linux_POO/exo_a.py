# --------- PYODIDE:env --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        self.liste_fichiers = None
        if est_repertoire:
            self.liste_fichiers = {}  # dictionnaire vide
        Fichier.cree_fichier = cree_fichier
        Fichier.cree_repertoire = cree_repertoire


# --------- PYODIDE:code --------- #
def cree_fichier(self, nom):
    nouveau_fichier = ...
    self.liste_fichiers[nom] = nouveau_fichier

    return nouveau_fichier


def cree_repertoire(self, nom):
    nouveau_fichier = ...
    self.liste_fichiers[nom] = nouveau_fichier

    return nouveau_fichier

# --------- PYODIDE:tests --------- #
racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
home = racine.cree_repertoire("home")
assert sorted(racine.liste_fichiers.keys()) == ['etc', 'home'], sorted(racine.liste_fichiers.keys())
user1 = home.cree_repertoire("user1")
user1.cree_fichier("log.txt")
repertoire_TP = user1.cree_repertoire("TP")
assert sorted(user1.liste_fichiers.keys()) == ['TP', 'log.txt']

# --------- PYODIDE:corr --------- #

class Fichier:
    def __init__(self, nom, est_repertoire):
        self.nom = nom
        self.est_repertoire = est_repertoire
        if est_repertoire:
            self.liste_fichiers = {}

    def cree_fichier(self, nom):
        nouveau_fichier = Fichier(nom, False)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

    def cree_repertoire(self, nom):
        nouveau_fichier = Fichier(nom, True)
        self.liste_fichiers[nom] = nouveau_fichier

        return nouveau_fichier

   
# --------- PYODIDE:secrets --------- #

racine = Fichier("/", True)
etc = racine.cree_repertoire("etc")
assert etc.est_repertoire is True, "Un répertoire ne doit pas être configuré comme un simple fichier"
home = racine.cree_repertoire("home")
assert sorted(racine.liste_fichiers.keys()) == ['etc', 'home'], sorted(racine.liste_fichiers.keys())
user1 = home.cree_repertoire("user1")
fichier = user1.cree_fichier("log.txt")
assert fichier.est_repertoire is False, "Un simple fichier ne doit pas être configuré comme un répertoire"


