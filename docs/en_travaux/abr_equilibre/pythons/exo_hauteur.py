# --------- PYODIDE:env --------- #


def abr_vide():
    """Renvoie un ABR vide"""
    return [None, None, None]


def est_vide(a):
    """Détermine si l'ABR passé en argument est vide ou non"""
    return a[1] is None


def sag(a):
    """Renvoie le sous-arbre gauche de a
    Provoque une erreur si a est vide
    """
    if est_vide(a):
        raise ValueError("L'arbre est vide")
    return a[0]


def valeur(a):
    """Renvoie la valeur portée par la racine de a
    Provoque une erreur si a est vide
    """
    if est_vide(a):
        raise ValueError("L'arbre est vide")
    return a[1]


def sad(a):
    """Renvoie le sous-arbre droit de a
    Provoque une erreur si a est vide
    """
    if est_vide(a):
        raise ValueError("L'arbre est vide")
    return a[2]


def insere(a, x):
    """Insère x dans l'ABR a qui est modifié en place"""
    if est_vide(a):
        a[0:3] = [abr_vide(), x, abr_vide()]
    val = valeur(a)
    if x < val:
        insere(sag(a), x)
    elif val < x:
        insere(sad(a), x)


def representation(a):
    def aux(a):
        if est_vide(a):
            return "∅"
        return f"({aux(a[0])}, {a[1]}, {aux(a[2])})"

    return aux(a)


def hauteur(a):
    pass


def infixe(a):
    """Renvoie la liste des valeurs rencontrées lors du parcours infixe de l'ABR"""

    def aux(a):
        if not est_vide(a):
            aux(sag(a))
            parcours.append(valeur(a))
            aux(sad(a))

    parcours = []
    aux(a)
    return parcours


def est_equilibre(a):
    pass


def abr_equilibre(valeurs_triees):
    pass


# --------- PYODIDE:code --------- #
def hauteur(a): 
    ...


# --------- PYODIDE:corr --------- #
def hauteur(a):
    if est_vide(a):
        return 0
    return 1 + max(hauteur(sag(a)), hauteur(sad(a)))


# --------- PYODIDE:tests --------- #
abr = abr_vide()
for x in [1, 2, 3]:
    insere(abr, x)
assert hauteur(abr) == 3


abr = abr_vide()
for x in [2, 1, 3]:
    insere(abr, x)
assert hauteur(abr) == 2
# --------- PYODIDE:secrets --------- #
from random import sample

abr = abr_vide()
assert hauteur(abr) == 0, f"Erreur avec {representation(abr)}"

for x in range(10):
    insere(abr, x)
    attendu = x + 1
    assert hauteur(abr) == attendu, f"Erreur avec {representation(abr)}"


def __hauteur__(a):
    if est_vide(a):
        return 0
    return 1 + max(__hauteur__(sag(a)), __hauteur__(sad(a)))


nombres = sample(range(100), 20)
abr = abr_vide()
for x in nombres:
    insere(abr, x)

attendu = __hauteur__(abr)
assert hauteur(abr) == attendu, f"Erreur avec {representation(abr)}"


