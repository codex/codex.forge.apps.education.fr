---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Arbre binaire de recherche équilibré
difficulty: 310
tags:
    - en travaux
    - ABR
    - récursivité
maj: 11/03/2025
---

On s'intéresse dans cet exercice à des arbres binaires de recherche (ABR) sans doublons. On fournit différentes fonctions permettant de travailler avec de tels arbres :

* `#!py abr_vide` : crée et renvoie un ABR vide ;
* `#!py est_vide` : renvoie le booléen indiquant si l'ABR passé en argument est vide ou non ;
* `#!py sag` : renvoie le sous-arbre gauche de l'arbre passé en argument. Provoque une erreur si celui-ci est vide ;
* `#!py valeur` : renvoie la valeur portée par la racine de l'arbre passé en argument. Provoque une erreur si celui-ci est vide ;
* `#!py sad` : renvoie le sous-arbre droit de l'arbre passé en argument. Provoque une erreur si celui-ci est vide ;
* `#!py insere` : insère `#!py x` dans l'ABR passé en argument. L'arbre est directement modifié : la fonction ne renvoie rien. Les doublons sont ignorés ;
* `#!py infixe` : renvoie la liste des valeurs rencontrées lors du parcours infixe de l'arbre passé en argument ;
* `#!py representation` : renvoie une chaîne de caractère décrivant l'arbre. Cette chaîne est au format :
    *  `#!py ∅` si l'arbre est vide ;
    *  `#!py (sous-arbre gauche, valeur, sous-arbre droit)` dans le cas contraire.


```pycon title="Exemple d'utilisation"
>>> abr = abr_vide()
>>> est_vide(abr)
True
>>> insere(abr, 2)
>>> insere(abr, 1) 
>>> insere(abr, 3) 
>>> est_vide(abr)  
False
>>> representation(abr)
'((∅, 1, ∅), 2, (∅, 3, ∅))'
>>> infixe(abr)
[1, 2, 3]
```

??? info "Les fonctions utilisées"

    ```python title=""
    def abr_vide():
        """Renvoie un ABR vide"""
        return [None, None, None]

    def est_vide(a):
        """Détermine si l'ABR passé en argument est vide ou non"""
        return a[1] is None

    def sag(a):
        """Renvoie le sous-arbre gauche de a
        Provoque une erreur si a est vide
        """
        if est_vide(a):
            raise ValueError("L'arbre est vide")
        return a[0]

    def valeur(a):
        """Renvoie la valeur portée par la racine de a
        Provoque une erreur si a est vide
        """
        if est_vide(a):
            raise ValueError("L'arbre est vide")
        return a[1]

    def sad(a):
        """Renvoie le sous-arbre droit de a
        Provoque une erreur si a est vide
        """
        if est_vide(a):
            raise ValueError("L'arbre est vide")
        return a[2]

    def insere(a, x):
        """Insère x dans l'ABR a qui est modifié en place"""
        if est_vide(a):
            a[0:3] = [abr_vide(), x, abr_vide()]
        val = valeur(a)
        if x < val:
            insere(sag(a), x)
        elif val < x:
            insere(sad(a), x)

    def infixe(a):
        """Renvoie la liste des valeurs rencontrées lors du parcours infixe de l'ABR"""

        def aux(a):
            if not est_vide(a):
                aux(sag(a))
                parcours.append(valeur(a))
                aux(sad(a))

        parcours = []
        aux(a)
        return parcours

    def representation(a):
        def aux(a):
            if est_vide(a):
                return "∅"
            return f"({aux(a[0])}, {a[1]}, {aux(a[2])})"

        return aux(a)
    ```

??? question "1. Hauteur d'un arbre"

    Écrire la fonction `#!py hauteur` qui prend en paramètre un arbre binaire de recherche et renvoie sa hauteur.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> abr = abr_vide()
        >>> for x in [1, 2, 3]:
        ...     insere(abr, x)
        >>> hauteur(abr)
        3
        >>> abr = abr_vide()
        >>> for x in [2, 1, 3]:
        ...     insere(abr, x)
        >>> hauteur(abr)
        2
        ```
    
    {{ IDE('./pythons/exo_hauteur') }}


??? question "2. Arbre binaire équilibré"

    Un arbre binaire est dit **équilibré** :
    
    * s'il est vide
    * ou si les hauteurs de ses sous-arbres gauche et droit ne diffèrent que d'une unité au maximum. Cette propriété doit être aussi vérifiée récursivement pour tous les sous arbres.

    Dans la figure ci-dessous, l'arbre de gauche est équilibré mais pas celui de droite.
    
    <center>

    ```mermaid
    flowchart TD
        A0((0))
        B0((1))
        C0((2))
        D0((3))
        E0[ ]
        F0((4))
        G0((5))

        A0 --> B0
        A0 --> C0
        B0 --> D0
        B0 ~~~ E0
        C0 --> F0
        C0 --> G0

        a((a))
        b((b))
        c((c))
        d(( ))
        e((e))
        f((f))
        g(( ))
        i(( ))
        j(( ))
        k((k))

        a --> b
        a --> c
        b ~~~ d
        b --> e
        c --> f
        c ~~~ g
        e ~~~ i
        f ~~~ j
        f --> k

        style E0 fill:none, stroke:none
        style d fill:none, stroke:none
        style g fill:none, stroke:none
        style i fill:none, stroke:none
        style j fill:none, stroke:none
    ```

    </center>

    Écrire la fonction `#!py est_equilibre` qui prend en paramètre un arbre binaire et renvoie le booléen indiquant s'il est équilibré ou non.
    
    Une version valide de la fonction `#!py hauteur` est accessible dans l'éditeur ci-dessous. Vous pouvez directement l'utiliser sans l'importer.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> abr = abr_vide()
        >>> for x in [1, 2, 3]:
        ...     insere(abr, x)
        >>> est_equilibre(abr)
        False
        >>> abr = abr_vide()
        >>> for x in [2, 1, 3]:
        ...     insere(abr, x)
        >>> est_equilibre(abr)
        True
        ```
    
    {{ IDE('./pythons/exo_est_equilibre') }}

    ??? tip "Aide"
    
        Un arbre binaire vide est équilibré.
        
        Dans le cas où l'arbre n'est pas vide, la vérification nécessite trois étapes :
        
        * vérifier que les hauteurs des sous-arbres gauche et droit diffèrent de moins d'une unité,
        * vérifier que le sous-arbre gauche est équilibré,
        * vérifier que le sous-arbre droit est équilibré.

        Si l'une de ses vérification échoue, l'arbre n'est pas équilibré.

??? question "3. Construire un arbre binaire équilibré"

    Écrire la fonction `#!py abr_equilibre` qui prend en paramètre une liste **triée**, sans doublons, d'éléments comparables et renvoie un arbre binaire équilibré contenant ces valeurs. Cet arbre n'est pas nécessairement unique mais il doit impérativement **être équilibré** et **contenir toutes les valeurs demandées**.
    
    Des versions valides des fonctions `#!py hauteur` et `#!py est_equilibre` sont accessibles dans l'éditeur ci-dessous. Vous pouvez directement les utiliser sans l'importer.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> abr = abr_equilibre([1, 2, 3])
        >>> est_equilibre(abr)
        True
        >>> infixe(abr)
        [1, 2, 3]
        >>> abr = abr_equilibre(["a", "b", "c", "d", "e", "f"])
        >>> est_equilibre(abr)
        True
        >>> infixe(abr)
        ['a', 'b', 'c', 'd', 'e', 'f']
        ```
    
    ??? tip "Aide"
    
        L'aspect trié de la liste des valeurs permet de définir astucieusement l'ordre dans lequel insérer les valeurs.
    
    === "Version vide"
        {{ IDE('./pythons/exo_construction_vide') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_construction') }}
    