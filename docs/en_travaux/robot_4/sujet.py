# --- hdr --- #
MOUVEMENTS = ((0, 1), (1, 0), (0, -1), (-1, 0))


def decoupe(instructions):
    return [int(c) if c.isnumeric() else c for c in instructions]


class Robot:
    def __init__(self, hauteur, largeur):
        self.hauteur = hauteur
        self.largeur = largeur
        self.grille = [[" " for _ in range(largeur)] for _ in range(hauteur)]
        self.i = 0
        self.j = 0
        self.grille[self.i][self.j] = "*"
        self.direction = 0

    def avance(self):
        """Fait avancer le robot d'une case (seulement si possible)"""
        di, dj = MOUVEMENTS[self.direction]
        if 0 <= self.i + di < self.hauteur and 0 <= self.j + dj < self.largeur:
            self.i += di
            self.j += dj
            self.grille[self.i][self.j] = "*"

    def droite(self):
        """Fait tourner le robot d'un quart de tour vers la droite"""
        self.direction = (self.direction + 1) % 4

    def gauche(self):
        """Fait tourner le robot d'un quart de tour vers la gauche"""
        self.direction = (self.direction - 1) % 4

    def dessine_parcours(self):
        """Affiche les cases parcourues et la position actuelle du robot"""
        affichage = [
            ["" for _ in range(self.largeur + 2)] for _ in range(self.hauteur + 2)
        ]
        for j in range(1, self.largeur + 1):
            affichage[0][j] = "─"
            affichage[-1][j] = "─"
        for i in range(1, self.hauteur + 1):
            affichage[i][0] = "│"
            affichage[i][-1] = "│"
        affichage[0][0] = "┌"
        affichage[-1][0] = "└"
        affichage[0][-1] = "┐"
        affichage[-1][-1] = "┘"
        for i in range(self.hauteur):
            for j in range(self.largeur):
                affichage[1 + i][1 + j] = self.grille[i][j]
        affichage[self.i + 1][self.j + 1] = [">", "v", "<", "^"][self.direction]
        print("\n".join("".join(ligne) for ligne in affichage))


# --- vide --- #
def deroule(instructions, i, memoire):
    ...


def execute_brut(robot, instructions, memoire):
    ...


def execute(robot, instructions):
    instructions, _, memoire = deroule(instructions, 0, {})
    execute_brut(robot, instructions, memoire)


# --- exo --- #
def deroule(instructions, i, memoire):
    resultat = []
    while i < len(instructions):
        instru = instructions[i]
        if instru == "(":
            sous_resultat, i = deroule(..., ..., ...)
            n = ...
            resultat += ... * ...
            i = ...
        elif instru == "debut_motif":
            nom_motif = ...
            sous_resultat, i = deroule(..., ..., ...)
            memoire[...] = ...
        elif instru == ")" or instru == "fin_motif":
            return ..., ..., ...
        else:
            resultat.append(...)
            i = ...
    return ..., ..., ...

def execute_brut(robot, instructions, memoire):
    i = 0
    while i < ...:
        # lecture de l'instruction
        instru = instructions[...]
        if i < ... and ...:
            n = ...
            i = ...
        else:
            n = ...
            i = ...

        # exécution de l'instruction
        for k in range(n):
            if instru == "D":
                ...
            ...  # plusieurs lignes possibles


def execute(robot, instructions):
    instructions, _, memoire = deroule(instructions, 0, {})
    execute_brut(robot, instructions, memoire)


# --- corr --- #
def deroule(instructions, i, memoire):
    resultat = []
    while i < len(instructions):
        instru = instructions[i]
        if instru == "(":
            sous_resultat, i, memoire = deroule(instructions, i + 1, memoire)
            n = instructions[i]
            resultat += sous_resultat * n
            i += 1
        elif instru == "debut_motif":
            nom = instructions[i + 1]
            sous_resultat, i, memoire = deroule(instructions, i + 2, memoire)
            memoire[nom] = sous_resultat
        elif instru == ")" or instru == "fin_motif":
            return resultat, i + 1, memoire
        else:
            resultat.append(instru)
            i += 1
    return resultat, i, memoire


def execute_brut(robot, instructions, memoire):
    i = 0
    while i < len(instructions):
        # lecture de l'instruction
        instru = instructions[i]
        if i < len(instructions) - 1 and isinstance(instructions[i + 1], int):
            n = instructions[i + 1]
            i += 2
        else:
            n = 1
            i += 1

        # exécution de l'instruction
        for _ in range(n):
            if instru == "D":
                robot.droite()
            elif instru == "G":
                robot.gauche()
            elif instru == "A":
                robot.avance()
            else:
                execute_brut(robot, memoire[instru], memoire)


def execute(robot, instructions):
    instructions, _, memoire = deroule(instructions, 0, {})
    execute_brut(robot, instructions, memoire)


# --- tests --- #
robot = Robot(4, 4)
instructions = [
    "debut_motif",
    "carré",
    "(",
    "A",
    3,
    "D",
    ")",
    4,
    "fin_motif",
    "carré",
]
attendu = [
    ["*", "*", "*", "*"],
    ["*", " ", " ", "*"],
    ["*", " ", " ", "*"],
    ["*", "*", "*", "*"],
]
execute(robot, instructions)
assert robot.grille == attendu

robot = Robot(2, 4)
instructions = [
    "trait",
    "debut_motif",
    "carré",
    "(",
    "A",
    3,
    "D",
    ")",
    4,
    "fin_motif",
    "debut_motif",
    "trait",
    "A",
    3,
    "fin_motif",
]
attendu = [
    ["*", "*", "*", "*"],
    [" ", " ", " ", " "],
]
execute(robot, instructions)
assert robot.grille == attendu
# --- secrets --- #
hauteur = 3
largeur = 3
instructions = list("AADAADAADADA")
attendu = [["*", "*", "*"], ["*", "*", "*"], ["*", "*", "*"]]
robot = Robot(hauteur, largeur)
execute(robot, instructions)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 4
largeur = 10
robot = Robot(hauteur, largeur)
code = ["A", 100, "D", 41, "A", 35, "D", 40, "A"]
attendu = [
    ["*", "*", "*", "*", "*", "*", "*", "*", "*", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
    [" ", " ", " ", " ", " ", " ", " ", " ", " ", "*"],
]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "G", ")", 40, "A"]
attendu = [["*", "*", "*"]]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "(", "(", "G", ")", 4, ")", 4, ")", 4, "A"]
attendu = [["*", "*", "*"]]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

hauteur = 1
largeur = 3
robot = Robot(hauteur, largeur)
code = ["A", 1, "(", "(", "(", "A", ")", 4, ")", 4, ")", 0]
attendu = [["*", "*", " "]]
execute(robot, code)
assert robot.grille == attendu, f"Erreur avec {instructions = }"

robot = Robot(1, 4)
instructions = ["debut_motif", "toto", "A", 1, "fin_motif", "A"]
attendu = [["*", "*", " ", " "]]
execute(robot, instructions)
assert robot.grille == attendu

robot = Robot(1, 5)
instructions = ["debut_motif", "toto", "A", 1, "fin_motif", "toto", 3]
attendu = [["*", "*", "*", "*", " "]]
execute(robot, instructions)
assert robot.grille == attendu
