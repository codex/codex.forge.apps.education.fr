On explore les différentes possibilités avant de revenir en arrière dès que l'on observe que la disposition est invalide.

Cette façon de faire classique est appelée *retour sur trace* (*backtracking* en anglais).
