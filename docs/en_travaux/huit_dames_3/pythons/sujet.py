# --- hdr --- #
def est_coherente(n ,disposition):
    # Vérifications des lignes
    nb_dames_placees = len(disposition)
    lignes_occupees = [False] * n
    for ligne in disposition:
        if lignes_occupees[ligne]:
            return False
        else:
            lignes_occupees[ligne] = True

    # Vérification des diagonales
    for j_1 in range(nb_dames_placees - 1):
        for j_2 in range(j_1 + 1, nb_dames_placees):
            if abs(disposition[j_2] - disposition[j_1]) == (j_2 - j_1):
                return False
    return True


# --- exo --- #
""" # skip
def compte_valides(n, disposition=None):
    if disposition is None:
        disposition = []
        
    # On a placé toutes les dames
    if ...:
        return ...

    # Il reste des dames à placer
    nb_valides = ...
    for ligne in range(n):
        disposition.append(...)
        if ...:
            nb_valides = ...
        ....pop()

    return ...
"""  # skip


# --- vide --- #
def compte_valides(n, disposition=None):
    ...


# --- corr --- #
def compte_valides(n, disposition=None):
    if disposition is None:
        disposition = []
        
    # On a placé toutes les dames
    if len(disposition) == n:
        return 1

    # Il reste des dames à placer
    nb_valides = 0
    for ligne in range(n):
        disposition.append(ligne)
        if est_coherente(n ,disposition):
            nb_valides += compte_valides(n, disposition)
        disposition.pop()

    return nb_valides


# --- tests --- #
# Echiquier de dimension n = 3 -> aucune disposition valide
assert compte_valides(3) == 0

# Echiquier de dimension n = 4 -> deux dispositions valides
assert compte_valides(4) == 2
# --- secrets --- #
# Tests supplémentaires
valides = {1: 1, 2: 0, 5: 10, 6: 4, 7: 40, 8: 92}
for n, attendu in valides.items():
    assert compte_valides(n) == attendu, f"Erreur avec {n = }"

# --- REM --- #
""" # skip
On explore les différentes possibilités avant de revenir en arrière dès que l'on observe que la disposition est invalide.

Cette façon de faire classique est appelée *retour sur trace* (*backtracking* en anglais).
"""  # skip
