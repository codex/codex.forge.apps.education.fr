---
date:
  created: 2025-02-23
draft: false
title: Un blog sur CodEx
slug: blog
categories:
    - Nouveautés
---

CodEx s'enrichit d'une rubrique "Actualités" dans laquelle seront présentées les nouveautés sur CodEx, les évolutions du site, les exercices du mois, et encore plein d'informations.

<!-- more -->

À bientôt dans la page d'actualités,

L'équipe de CodEx.