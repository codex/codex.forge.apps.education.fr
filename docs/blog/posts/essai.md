---
date:
  created: 2025-02-23
  updated: 2025-02-23
draft: true
title: Un essai d'article
slug: abracadabra
categories:
    - Nouveautés
---

* Le titre définit automatiquement le slug de l'article.
* Il est tjrs possible de le définir via la métadonnée `title`. Si le titre est défini aussi en md, avec `# h1`, c'et la métadonnée qui "gagne".
* Il est possible de définir le slug de la page manuellement. ___Je vous le conseille vivement !___, comme ça, si le titre change, le lien vers l'article ne casse pas (et s'il y a une faut de frappe dans le slug, ce n'est pas très grave)


Voici les meta données du la page : j'y ai laissé trainer les champs qui devraient vous être utiles.

```yaml title=""
---
date:
  created: 2025-02-23
  updated: 2025-02-23
draft: false
title: Un essai d'article
slug: abracadabra
categories:
    - Nouveautés
---
```


Dans `mkdocs.yml:plugins.blog`, vous avez différentes options modifiables, notamment la fate à partir de laquelle les articles sont archivés, les options de pagination (nombre de posts par page. Je n'ai pas testé les rendus.)

Il ne FAUDRA PAS définir de nouveaux tags pour le blog, car nos tags n'ont rien à voir. ça n'empêche pas d'ajouter des tags existant à des articles si vous voulez, mais je doute que ça soit utile.

Il y a par contre des catégories, définissables dans `mkdocs.yml:plugins.blog` (j'en ai mis 2 à vous de voir ce que vous voulez mettre).

Il faudra aussi voir où vous voulez placer le blog dans la nav...