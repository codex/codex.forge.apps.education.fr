---
hide:
    - navigation
    - toc
---

# 👏 Crédits

Ce site est construit et entretenu par des **professeurs d'informatique**. L'ensemble des contenus est sous [licence CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1){ target="_blank}.

Il est donc **possible** de *partager* et *modifier* ce contenu.

Vous êtes par contre **tenus** de *créditer* les auteurs, de *ne pas faire un usage commercial* du contenu et de le partager *sous la même licence*.

!!! question "Contact"

    Vous avez une question, une suggestion d'amélioration ? Vous pouvez contacter les auteurs en écrivant à cette adresse : [`codex@aeif.fr`](mailto:codex@aeif.fr).

??? tip "Contributeurs"

     Les enseignants ci-dessous ont tous participé à la rédaction et ou à la relecture des exercices présents sur le site.

    <section class="conteneur-cartes" markdown>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Serge Bays</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Vincent Bouillot</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Franck Chambon</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Mireille Coilhac</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Guillaume Connan</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Jean Diraison</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Sébastien Hoarau</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Romain Janvier</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Vincent-Xavier Jumel</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Quentin Konieczko</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Gilles Lassus</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Benoît Le Gouis</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Fédéric Leleu</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Pierre Marquestaut</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: François Meyer</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Fabrice Nativel</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Charles Poulmaire</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Denis Quenton</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Nicolas Revéret</article>
        <article class="carte-contributeur" markdown>:fontawesome-solid-user-pen: Frédéric Zinelli</article>
    </section>
    
    Les vidéos de présentation de CodEx ont été réalisées par *Pierre Marquestaut*.

---------------------------

Ce site est soutenu par l'[Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.

[![AEIF](./images/logo_aeif_300.png){.center width=10%}](https://aeif.fr/index.php/category/non-classe/)

---------------------------

Le site est hébergé par la forge des communs numériques éducatifs <a href="https://docs.forge.apps.education.fr/" target="_blank">
<span aria-label="Avatar" aria-hidden="true" data-type="round" data-color="3" class="_avatar_k41ul_17 mx_BaseAvatar" style="--cpd-avatar-size: 16px;"><img loading="lazy" alt="" src="https://matrix.agent.education.tchap.gouv.fr/_matrix/media/v3/thumbnail/matrix.agent.education.tchap.gouv.fr/de0e2fe63b40dd452178360baa3ff29ba16d8b98?width=16&amp;height=16&amp;method=crop" crossorigin="anonymous" referrerpolicy="no-referrer" class="_image_k41ul_49" data-type="round" width="16px" height="16px"></span><span class="mx_Pill_text">Centre de documentation</span></a>

[![forge](./images/brigit_et_komit_transparent.png){.center width=10%}](https://docs.forge.apps.education.fr/){target="_blank"}

---------------------------

Le site utilise les technologies suivantes (entre autres) :

* [**mkdocs**](https://www.mkdocs.org/) : génération du site `html` à partir de fichiers `markdown` ;

* [**mkdocs-material**](https://squidfunk.github.io/mkdocs-material/) : thème de `mkdocs`;
* [**pyodide**](https://pyodide.org/en/stable/) : portage de **Python** dans le navigateur ;
* [**pyodide-mkdocs-theme**](https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme) : adaptation de **Pyodide** à **mkdocs**, gestion des exercices, tests et corrigés. Développé par Frédéric Zinelli, basé sur un travail initial de [Vincent Bouillot](hhttps://gitlab.com/bouillotvincent/pyodide-mkdocs).
* les bibliothèques `alien-python` et `pyxel-art` ont été initalement développées par Mathieu Degrange sous  [licence MIT](https://github.com/DegrangeM/alien-python/blob/master/LICENSE), puis par Germain BECKER.


 <p xmlns:cc="http://creativecommons.org/ns#" >Le logo du site a été dessiné par Lalie Martinon. Il est sous licence  <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

---------------------------

Une mesure d'audience non intrusive est effectuée avec le logiciel libre [Matomo](https://matomo.org/)

Le code JavaScript exécuté sert pour la console de développement intégré sur les pages et nous l'hébergeons également. Seules quelques bibliothèques telles que [`MathJax`](https://www.mathjax.org/) et [`Jquery`](https://jquery.com/) sont récupérées à partir de réseaux de diffusion de contenu (ou _CDN_).
