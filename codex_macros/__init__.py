
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

from . import (
    includer,
    recherche,
    insertion_remarque,
    liens_internes,
    parcours,
)



def define_env(env:PyodideMacrosPlugin):
    """Macro hook function (equivalent to on_config plugin hook)"""


    def build_macros_for(macros_factories, *args):
        """Register all the functions as macros"""
        for func in macros_factories:
            macro = func(env, *args)
            env.macro(macro)

    # ------------------------------------------------
    # CodEx specific

    build_macros_for([
        insertion_remarque.interdiction,
        insertion_remarque.remarque,
        insertion_remarque.version_ep,
        includer.double_IDE,
    ])

    EXOS = liens_internes.define_env(env)

    parcours.define_env(env, EXOS)
