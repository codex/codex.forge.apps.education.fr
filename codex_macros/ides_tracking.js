import { getIdeDataFromStorage, subscribeWhenReady } from 'functools'



// Add all the IDE buttons in the page, if desired.
// (done in recherche.md AND parcours.md)
$(`[parkour^=editor_]`).each(function(){
  const editorId = this.getAttribute('parkour')
  const [store,_] = getIdeDataFromStorage(editorId)
  const done   = store.done ?? 0
  const color  = !done ? "unset" : done<0 ? 'red' : 'green'
  const status = !done ? "Pas essayé" : done>0 ? "Terminé" : "Échec"
  const tipTxt = [status, store.name??""].filter(Boolean).join('<br>')
  $(this).css('--ide-btn-color',  color)
         .find('span.tooltiptext').html(tipTxt).css('width','unset')
})





const cssPx =(jObj, prop)=> +jObj.css(prop).slice(0,-2)

const round_1 =(n)=> +n.toFixed(1)





function insertIdesIcons(){


  const extractIdeButtonsDivsWithLinkRectBox=()=>{
    const ideButtonsAndRects = []
    svg.find('a')                  // <a> are wrapping the rectangle of one exercice in the svg
        .attr('target', '_blank')   // Update links: force opening in a new tab
        .each(function(){
          const rect = $(this)
          const url  = rect.attr('href') ?? rect.attr('xlink:href')

          if(!url) return           // Not an exercice/IDE

          const exoName = url.slice(0,-1).split('/').pop()
          const buttons = nodesByExos.get(exoName)
          ideButtonsAndRects.push( [buttons.clone(), this.getBoundingClientRect()] )
        })
    return ideButtonsAndRects
  }


  /**Reorganize the DOM around the svg to add the IDE buttons, and the required logistic to
   * rescale/move the whole thing according to the window size changes.
   * */
  const rebuildDomAroundMermaidAndAddButtons=()=>{

    // Reorganize the DOM:
    topDiv.insertBefore(jMermaid)
    topDiv.append(jMermaid.detach(), parkourDiv.append(innerPk))

    // Set initial appropriate size for innerPk:
    for(const prop of ['width', 'height']){
      innerPk.css(prop, svg.css(prop))
    }

    // Define the initial height for the buttons' images:
    const hImg   = round_1( 1/3 * Math.min(...overlay.map(([,rect])=>rect.height)) )
    const gapImg = round_1( hImg/4 )
    const hImgPx = hImg+'px'

    // Initial placement (centering the buttons shifting them half on the left cannot be done
    // here, because the buttons div are currently in a div with display:none, so they do not
    // have any width yet...)
    for(const [btns,rect] of overlay){
      const left = rect.x - srcSvgBox.x + rect.width / 2
      const top  = rect.y - srcSvgBox.y + rect.height + gapImg
      btns.css('left', left+'px').css('top',  top+'px').css('--dim', hImgPx)
      innerPk.append(btns)
    }
  }


  /**Put in place the resize observer, so that buttons always follow the svg:
   * */
  const AddResizeObserver=()=>{

    let fixedCentering = false
    new ResizeObserver(_=>{

      // If not handled yet, fix the buttons centering (shifting them half way left and up):
      if(!fixedCentering){
        fixedCentering = true
        for(const [btns,] of overlay){
          const left  = cssPx(btns, 'left')
          const width = cssPx(btns, 'width')
          btns.css('left', left - width/2 + 'px')
        }
      }

      // Translate the parkourDiv to follow the svg top left corner:
      const svgBox = svg[0].getBoundingClientRect()
      const topBox = topDiv[0].getBoundingClientRect()
      const pkDivLeft = (svgBox.x - topBox.x)+'px'
      parkourDiv.css('left', pkDivLeft)

      // Rescale innerPk, resizing from its top left corner:
      const scale = svgBox.width / srcSvgBox.width
      const deltaX = (svgBox.width - srcSvgBox.width) / 2
      const deltaY = (svgBox.height - srcSvgBox.height) / 2
      innerPk.css('transform', `matrix(${ scale }, 0, 0, ${ scale }, ${ deltaX }, ${ deltaY })`)

    }).observe(this)
  }


  /* ---------------------------------------------------------------

    - `topDiv` wraps the mermaid div and the parkourDiv one, linking them together and
      isolating them from other elements in the page.
    - `parkourDiv` has 0 width and height, it's just an anchor for the inner div.
      Its position is to be aligned on the top left corner of the svg
    - `innerPk` has the exact same size then the initial svg.

    Buttons are initially placed in innerPk to match the expected position (with an extra
    roundtrip to handle proper centering of the buttons).
    After that, when the page is resized:

        1. `parkourDiv` follows the svg corner
        2. `innerPk` is scaled so that it's current width matches the svg's one.
            Note that scaling must be done from the top left corner, which involves an
            additional translation.
  */
  const jMermaid  = $(this)
  const svg       = $(this.shadowRoot).find('svg')
  const srcSvgBox = svg[0].getBoundingClientRect()
  const overlay   = extractIdeButtonsDivsWithLinkRectBox()

  if(!overlay.length) return;

  const topDiv     = $('<div class="parkour-wrapper"></div>')
  const parkourDiv = $('<div class="parkour-inner-wrapper"></div>')
  const innerPk    = parkourDiv.clone()

  rebuildDomAroundMermaidAndAddButtons()
  AddResizeObserver()
}





// Handle mermaid parkour graphs
const hiddenBtns    = $("#hidden-ides-buttons")
const isParkourPage = Boolean(hiddenBtns.length)
const nodesByExos   = new Map()                   // {ExoName: jQuery node}


if(isParkourPage){

  // Move all h3 into the parent details>summary:
  $('h3').each(function(){
    const h3 = $(this)
    h3.parent().find('summary').append( h3.detach() )
  })


  // Fix the auto-focus on tabbed labels:
  const admonitionsLinks = $("nav.md-nav--secondary nav a")
  admonitionsLinks.on('click', function(event) {
    event.preventDefault();

    const headerHeight = $('header.md-header').outerHeight() + 20
      // Has to stay dynamic: may vary depending on the viewport width

    const lien = $(this).attr('href');
    const elt = $(lien);
    const parent = elt.parent();
    const details = elt.parent().parent();
    details.attr('open', '');
    $(document.documentElement).scrollTop(parent.offset().top - headerHeight - 3);
  })



  const setupParkours=()=>{

    hiddenBtns.children().each(function(){
      const jDiv = $(this)
      nodesByExos.set(jDiv.attr('btns_for'), jDiv)
    })

    $('div.mermaid').each(function(i){
      const waitingForSizedSvg = () =>{
        const svgHeight = Boolean(this.shadowRoot) && $(this.shadowRoot).find('svg').css('height') || '0px'
        const out = svgHeight != '0px'
        return out
      }
      subscribeWhenReady('mermaidParkour'+i, insertIdesIcons.bind(this), {
        delay:200, runOnly:true, waitFor: waitingForSizedSvg,
        maxTries:Infinity,    // Needed because some graphs are in other tabs, now, so they might never get loaded
      })
    })
  }


  // WARNING: material is removing .mermaid classes before the document/page loads the mermaid
  // script (to avoid it trying to render stuff ignoring mermaid config from material...), then put them
  // back in place. So the mermaid variable must be awaited for...
  subscribeWhenReady('global-mermaid', setupParkours, {
    delay:75, runOnly:true, waitFor:()=>Boolean(globalThis.mermaid), maxTries:100,
  })
}
