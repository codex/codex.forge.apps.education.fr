"""
#########################################################
# Fichiers ajoutés pour Codex                           #
# Création de la page de recherche : fonction recherche #
# Gestion des tags : variable env.conf['tagsValides']   #
#########################################################
"""

from datetime import datetime, date
import json
import re

from typing import Any, Dict, List, Tuple
from dataclasses import dataclass, field
from pathlib import Path

from mkdocs.exceptions import BuildError
from mkdocs.structure.pages import Page

from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin
from pyodide_mkdocs_theme.pyodide_macros import html_builder as Html

from codex_macros.ides_tracking import tracking_template


ExByCategory = Dict[str, List["Exercice"]]

DiffData = Tuple[str, str]
""" (category ID (ASCII + no spaces), category name (not ASCII + spaces)) """

DEFAULT_DIFF = 400


# In order:
DIFFICULTY_CATEGORIES: List[DiffData] = [
    ("debutant", "débutant"),
    ("facile", "facile"),
    ("moyen", "moyen"),
    ("difficile", "difficile"),
    ("non-renseigne", "non renseigné"),
]
OTHER_CATEGORIES = (
    "autre",
    "brouillon",
    "exam",
)

ALL_CATEGORIES = OTHER_CATEGORIES + tuple(cat_id for cat_id, _ in DIFFICULTY_CATEGORIES)

VALIDATED_DIR = "exercices"




@dataclass
class Exercice:
    """Regrouping the data for a file/problem targeted by the search engine"""

    path: Path  # mandatory; absolute

    title: str = "Pas de titre :("
    stripped_title: str = "pasdetitre"
    difficulty: int = DEFAULT_DIFF
    tags: List[str] = field(default_factory=list)
    is_exercise: bool = True  # "validé" ou "en travaux"
    is_validated: bool = False  # dans "./exercices/..."
    is_draft: bool = False  # "TROP en travaux"... XD
    is_exam: bool = False  # exam-type exercises, to be ignored in some occasion
    href: str = "" # Absolute url
    page_url: str = "" # Relative to the site_dir (used to identify an IDE against page.url)
    folder_name: str = "" # name of the directory holding the index.md file
    diff_category_data: DiffData = ("", "")
    index_file_last_update: str = "01/01/2024"  # Exercise's last update dd/mm/yyyyy
                                                # (default: before starting CodEx)







def dico_exos(env:PyodideMacrosPlugin) -> ExByCategory :
    macro_to_extract = 'IDE', 'terminal', 'py_btn'
    dico_tags        = env.variables["tagsValides"]

    exos_by_category = {cat_id: [] for cat_id in ALL_CATEGORIES}

    managers_by_page = {
        data.page_url: data
            for data in env.all_macros_data if data.macro in macro_to_extract
    }

    for data in managers_by_page.values():

        url_path = Path(data.full_page_url)
        exo      = Exercice(url_path)
        parts    = exo.path.parts
        metas: Dict[str,Any] = data.meta or {}

        if "title" in metas:
            exo.title = metas["title"]
            exo.stripped_title = re.sub(r"\W", "", exo.title)

        if "difficulty" in metas:
            exo.difficulty = int(metas["difficulty"])

        if "tags" in metas:
            exo.tags = [*filter(bool, metas["tags"])]
            for tag in tuple(exo.tags):
                if tag not in dico_tags:
                    raise BuildError(
                        f"[Tag incorrect] - l'exercice {exo.path} contient le tag '{tag}' incorrect"
                    )

        if "maj" in metas:
            update = metas["maj"]
            try:
                dd, mm, yyyy = map(int, update.split("/"))
                if yyyy < 100:     # Assume "YY" format only
                    yyyy += 2000
                if yyyy > datetime.now().year:
                    raise ValueError()

                date(yyyy,mm,dd)        # validation purpose only

            except ValueError:
                maj = metas and metas.get('maj', "(no maj date...)")
                raise BuildError(
                    f"[Date incorrecte] - l'exercice {exo.path} contient un format de date de maj "
                    f"incorrecte : {maj}. Le format attendu est jj/mm/aaaa. ou jj/mm/aa pour les dates > 1999."
                ) from None

            exo.index_file_last_update = f"{ dd :0>2}/{ mm :0>2}/{ yyyy }"


        exo.is_validated = len(parts) > 1  and  'exercices' == parts[-2]
        exo.is_exercise  = exo.is_validated or len(parts) > 1 and 'en_travaux' == parts[-2]
        exo.is_draft     = "brouillon" in exo.tags
        exo.is_exam      = "exam" in parts
        exo.folder_name  = url_path.name
        exo.page_url     = data.page_url
        exo.href         = data.full_page_url

        exo.diff_category_data = DIFFICULTY_CATEGORIES[exo.difficulty // 100]


        # We fill the dict with this target:
        if exo.is_draft:
            exos_by_category["brouillon"].append(exo)
        elif exo.is_exam:
            exos_by_category["exam"].append(exo)
        elif exo.is_exercise:
            diff_cat_id = exo.diff_category_data[0]
            exos_by_category[diff_cat_id].append(exo)
        else:
            exos_by_category["autre"].append(exo)

    return exos_by_category








def recherche(env: PyodideMacrosPlugin, cat_dict: ExByCategory, search_page:Page):
    """
    Création du tableau et insertion du code JS pour la page de recherche
    Nicolas Revéret
    (MAJ: Frédéric Zinelli)
    """

    # ajout du dictionnaire des tags valides à la variable d'environnement
    # ce dictionnaire devient accessible dans Jinja en faisant {{ config.tagsValides }}
    # voir overrides/partials/tags.html pour son utilisation (note: rendu après on_env).
    env.conf["tagsValides"] = env.variables["tagsValides"]


    def title_cell(exo:Exercice):
        has_quotes = exo.title[0] in "\"'" and exo.title[0] == exo.title[-1]
        title = exo.title
        if has_quotes:
            title = title[1:-1]
        link = Html.a(
            title,
            kls    = "md-tag",
            href   =  exo.href,
            target = '_blank',
        )
        tracking = tracking_template(exo.page_url)      # Placeholder for validation icons
        return Html.div(link+tracking, kls='ide_tracker_wrapper', attrs={'data-sort':exo.stripped_title})

    def diff_cell(diff_kls, diff_name):
        return Html.span(diff_name, kls=f'md-tag { diff_kls }')

    def add_tr_and_tds(kls:str, tds:List[str]):
        rendu.append(f'<tr class="{ kls }">')
        rendu.extend(map(Html.td, tds))
        rendu.append("</tr>")


    # Build the whole html table of elements
    rendu = ["""
<table class="table hover stripe row-border" id="tableau-recherche" style="width:100%;">
    <thead class="">
        <tr>
            <th>Exercice</th>
            <th>Difficulté</th>
            <th>Tags</th>
            <th>Mise à jour</th>
            <th>valeurDifficultes</th>
            <th>classeDifficultes</th>
            <th>timestamp</th>
        </tr>
    </thead>
    <tbody>
"""]
    is_dev_docs = env.docs_dir == "dev_docs"
    for cat_id, _ in DIFFICULTY_CATEGORIES:
        for exo in cat_dict[cat_id]:
            if not exo.is_draft or is_dev_docs:

                # Formatting is guaranteed to be "../../....", here:
                dd, mm, yyyy = exo.index_file_last_update.split("/")

                row = (
                    title_cell(exo),
                    diff_cell(*exo.diff_category_data),
                    " ".join(f"<span class='md-tag'>{ t }</span>" for t in exo.tags),
                    exo.index_file_last_update,
                    str(exo.difficulty),
                    str(exo.difficulty // 100),
                    yyyy + mm + dd,
                )
                add_tr_and_tds(kls="table-row", tds=row)

    rendu.append("</tbody></table>")


    # Add the script part after the table:
    rendu.append(
        "<script>$(document).ready(function () { gestion_tableau(); }) </script>"
    )

    search_page.content += "".join(rendu)





def exercices_aleatoires(_: PyodideMacrosPlugin, cat_dict: ExByCategory, home:Page):
    """
    Création d'un objet JS :
    {
        "debutant": [[titre de l'exercice, lien], [...]]] ,
        "facile": [[titre de l'exercice, lien], [...]]] ,
        "moyen": [[titre de l'exercice, lien], [...]]] ,
        "difficile": [[titre de l'exercice, lien], [...]]] ,
    }
    listant tous les exercices validés classés par difficulté
    Cet objet `exercices` est disponible en JS dans la page appelant cette macro


    Nicolas Revéret
    """
    exercices_dict = {
        cat_id: []
        for cat_id, _ in DIFFICULTY_CATEGORIES
        if cat_id != "non-renseigne"
    }
    for category, exercices in exercices_dict.items():
        for exo in cat_dict[category]:
            if exo.is_validated:
                exercices.append((exo.title, exo.href))

    js_code = f"""
<script>
var exercices = { json.dumps(exercices_dict) };"""+"""
$(document).ready(function(){ exercice_hasard() })
</script>
"""
    home.content += js_code
