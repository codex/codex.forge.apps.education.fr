
from typing import Dict
from collections import defaultdict

from pyodide_mkdocs_theme.pyodide_macros import html_builder as Html
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

from codex_macros.ides_tracking import tracking_template




def define_env(env: PyodideMacrosPlugin):

    EXOS: Dict[str,str] = {}            # Build the Dict[name,url]
    check_unique = defaultdict(set)

    docs = env.docs_dir_cwd_rel

    for top_folder in ('exercices', 'en_travaux', 'exam'):
        top = docs / top_folder
        for exo in top.iterdir():
            if exo.is_dir():
                name = exo.name
                url  = f"{ top_folder }/{ name }/"
                EXOS[name] = url
                check_unique[name].add(url)


    oops = ''.join(
        f'    { name }:' + ''.join(f"\n\t{env.docs_dir}/{p}" for p in paths)
            for name,paths in check_unique.items()
            if len(paths) > 1
    )
    if oops:
        raise FileExistsError(
            "Des noms de dossiers identiques contenant des fichiers 'index.md' ont été trouvés. "
            "Il n'est pas possible de construire le dictionnaire pour la macro concernant les "
            f"liens internes.\nNoms et fichiers concernés:\n{ oops }\n"
        )



    @env.macro
    def lien_exo(txt:str, target:str, tail:str="", new_tab:bool=False):
        """
        Crée automatiquement le lien vers l'exercice contenu dans le dossier indiqué,
        quelle que soit la localisation du fichier actuel.

        @txt:           Texte à afficher pour le lien
        @target:        Nom du dossier contenant l'exercice à cibler
        @tail="":       Pour cibler un autre fichier que index.md dans le répertoire
                        cible (images, svg, ...)
        @new_tab=False: Le lien s'ouvre dans un nouvel onglet

        @returns: une balise `<a>` avec le bon lien relatif, et le contenu de `txt_lien`

        @throws:
            * Au démarrage du build: FileExistsError, si deux dossier d'exercices sont trouvés avec
            des noms identiques
            * À la construction des pages: FileNotFoundError, si le dossier ciblé n'est pas trouvé.

        Frédéric Zinelli
        """
        if target not in EXOS:
            raise FileNotFoundError(f"""Pas d'exercice trouvé dans un dossier nommé {target!r}""")
        up = env.level_up_from_current_page()

        extra_args = {"target": '_blank'} if new_tab else {}
        link =  Html.a(txt, href=f'{ up }/{ EXOS[target] }{ tail }', **extra_args)
                # Use fo tail works because directory_urls=true
        return link




    @env.macro
    def lien_parcours(txt:str, target:str, tail:str="", new_tab:bool=True):
        """
        Liens internes spécifiques à la page des parcours, avec insertion automatique de la
        logistique pour montrer "l'état d'avancement" des IDEs dans chaque page.

        Frédéric Zinelli
        """
        link   = lien_exo(txt, target, tail, new_tab)
        tracer = tracking_template( EXOS[target] )
        html   = f'{ tracer } { link }'
        return html


    return EXOS
