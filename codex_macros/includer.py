from pathlib import Path
from textwrap import dedent
from typing import Any, Dict, Optional
from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin


# --- HACK ---
# (Allow rendering macros in REMs files)

from pyodide_mkdocs_theme.pyodide_macros.paths_utils import convert_url_to_utf8, to_uri
from pyodide_mkdocs_theme.pyodide_macros.macros.ide_ide import Ide


global_include = None

def hack_rem_inclusion(self:Ide, rem_path_kind:str):
    global global_include
    if not global_include:
        global_include = md_include(self.env)

    path_str = str(getattr(self.files_data, rem_path_kind))
    rem_uri  = to_uri( convert_url_to_utf8(path_str) )
    extra    = (rem_path_kind=='rem_rel_path') * 4*' '
    included = global_include(rem_uri, extra)
    return included

Ide._rem_inclusion = hack_rem_inclusion   # pylint: disable=protected-access

# --- HACK ---





MD_INCLUDE_PATH = []

def renderer(env:PyodideMacrosPlugin):

    def _render_inner_macros(content, disk_location='<unknown>', src=None, extra=""):

        indentations = env._parser.parse(content, disk_location)
        env._indents_store += [*reversed(indentations)]

        template = env.env.from_string(content)
        if src is not None:
            MD_INCLUDE_PATH.append(src)
        try:
            rendered = template.render(**env.variables)
        finally:
            if src is not None:
                MD_INCLUDE_PATH.pop()

        indent = env.get_macro_indent() + extra
        if indent:
            rendered = rendered.replace('\n', '\n' + indent)
        return rendered

    return _render_inner_macros




def md_include(env:PyodideMacrosPlugin):

    _render_inner_macros = renderer(env)

    def md_include(src:str, extra='', **__):
        """
        @str:   Fichier md à inclure (relatif au cwd)
        @extra: Deprecated in PMT, but not here, because the included file doesn't hold the
                indentation info otherwise. This is because I _FAKE_ the macro call, while
                in PMT, the markdown actually contains it when needed.
        """
        path     = Path(src)
        disk_loc = str( env.docs_dir_cwd_rel / path )
        content  = Path(path).read_text(encoding='utf-8')
        rendered = _render_inner_macros(content, disk_loc, src, extra=extra)
        return rendered

    return md_include






def double_IDE(env:PyodideMacrosPlugin):

    _render_inner_macros = renderer(env)

    def make_args(options:dict, common:dict):
        options = options or {}
        options.update(common)
        args = ''.join( f', {k}={v!r}' for k,v in options.items() if v is not None )
        return args


    def double_IDE(
        py1: str,
        py2: str,
        *,
        title1 = "Version vide",
        title2 = "Version à trous",
        ID:        Optional[int]  = None,
        SANS:      Optional[str]  = None,
        WHITE:     Optional[str]  = None,
        REC_LIMIT: Optional[int]  = None,
        MAX:       Optional[int]  = None,
        LOGS:      Optional[bool] = None,
        MAX_SIZE:  Optional[int]  = None,
        TERM_H:    Optional[int]  = None,
        options1:  Dict[str,Any]  = None,
        options2:  Dict[str,Any]  = None,
    ):
        common = {
            'ID': ID,
            'SANS': SANS,
            'WHITE': WHITE,
            'REC_LIMIT': REC_LIMIT,
            'MAX': MAX,
            'LOGS': LOGS,
            'MAX_SIZE': MAX_SIZE,
            'TERM_H': TERM_H,
        }

        args1   = make_args(options1, common)
        args2   = make_args(options2, common)
        op,clo  = '{{','}}'
        content = dedent(f"""
        === "{title1}"
            { op }IDE({py1!r}{ args1 }){ clo }

        === "{title2}"
            { op }IDE({py2!r}{ args2 }){ clo }
        """)
        rendered = _render_inner_macros(content)
        return rendered

    return double_IDE
