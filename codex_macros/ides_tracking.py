
from pathlib import Path
import re
from typing import Optional

from mkdocs.structure.pages import Page

from pyodide_mkdocs_theme.pyodide_macros import html_builder as Html
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin
from pyodide_mkdocs_theme.pyodide_macros.macros.ide_manager import IdeManager




TRACKING_TOKEN = 'IDE_TRACKING_TOKEN'
TRACKING_TEMPLATE = f"{ TRACKING_TOKEN }!{'{}'}!{ TRACKING_TOKEN }"



def tracking_template(url:str):
    """
    Build the template that will allow to spot where to put the needed html in the
    on_page_context event.
    """
    return TRACKING_TEMPLATE.format(url)



def handle_ides_tracking(page:Page, env:PyodideMacrosPlugin, *, wraps_tag='span'):
    """
    Mutate page.content to insert the IDE tracking logistic (only called on the pages requiring
    this logistic, during on_page_context hook).
    """
    # pylint: disable=protected-access

    js_code, css_code  = (
        Path('codex_macros','ides_tracking').with_suffix(ext).read_text(encoding='utf-8')
        for ext in ('.js', '.css')
    )
    html = re.sub(
        TRACKING_TEMPLATE.format("([^<]+)"),
        lambda m: insert_validations(env, m[1], wraps_tag),
        page.content
    )
    xtra = ""
    if page.file.src_uri.endswith('parcours.md'):
        xtra = Html.style(
            Path('codex_macros','parcours.css').read_text(encoding='utf-8')
        )
    css    = Html.style(css_code)
    script = Html.script(js_code, type='module')
    page.content = f"{ xtra }{ css }\n\n{ html }\n\n{ script }"




def insert_validations(
    env:PyodideMacrosPlugin,
    url:str,
    wraps_tag:Optional[str],
    **wraps_extras
):

    # All IDEs in the given page/url:
    ides_lst = env._pages_with_ides_to_test[url]

    buttons = ''.join(
        IdeManager.cls_create_button(
            env, 'check', disabled="", parkour=ide.storage_id,
            style="display: var(--show-tracking);"      # Flag for the recherche.md page
        )
        for ide in ides_lst if ide.has.check_btn
    )
    if not wraps_tag:
        return buttons
    return getattr(Html, wraps_tag)(buttons, kls='ide_tracker_wrapper', **wraps_extras)
