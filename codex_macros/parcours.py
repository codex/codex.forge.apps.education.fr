

import re

from dataclasses import dataclass
from typing import ClassVar, Dict, List, Set, Tuple, Union

from mkdocs.exceptions import BuildError
from mkdocs.structure.pages import Page

from pyodide_mkdocs_theme.pyodide_macros import html_builder as Html
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

from codex_macros.ides_tracking import insert_validations
from codex_macros.recherche import Exercice



ParkourChild = Union[str, Tuple[str, str]]

PARKOUR_MERMAIDS: List['ParkourMermaid'] = []
PARKOUR_MERMAID_REPL_TOKEN = "PARKOUR_MERMAID_REPL_TOKEN"



def define_env(env: PyodideMacrosPlugin, EXOS:Dict[str,str]):

    env.macro(PNode)
    PARKOUR_MERMAIDS.clear()


    @env.macro
    def parcours_mermaid(
        *graph:         PNode,
        chart:          str='TD',
        allow_non_exos: bool=False,
        **defs:         Dict[str,str]
    ):
        """
        @*graph:     N'importe quel nombre d'arguments `PNode` (voir ci-dessous)

        @chart="TD": Définit la direction de construction du graphe. Par défaut `"TD"`.

        @allow_non_exos=False: Si `True` il est possible de passer un texte quelconque pour
                     les étiquettes des objets PNode (ou pour leurs enfants).

        @**defs:     Des dictionnaires permettant de définir un peu tout et n'importe quoi.
                     Les arguments nommés doivent correspondre à une définition valide pour un
                     graphe mermaid.
                     Les couples clef/valeur du dictionnaire correspondent aux définitions
                     concrètes, pour ce type de déclaration. Exemples:

                        classDef = {'ep1': 'stroke:black;...', 'other': '...'}
                        linkStyle = {'default': ...}
        """
        PARKOUR_MERMAIDS.append(
            ParkourMermaid( chart, allow_non_exos, list(graph), defs, env.get_macro_indent(), EXOS )
        )
        return env.indent_macro(f"```mermaid\n{ PARKOUR_MERMAID_REPL_TOKEN }\n```")









class PNode:
    """
    Helper class (seen as a macro)

    PNode(name, *children, kls="")

    @name: Nom du dossier contenant l'exercice à cibler (pas besoin de préciser s'il est
           dans `exercices`, `en_travaux`, ...)

    @*children: Soit une chaîne de caractères indiquant le nom du dossier contenant l'exercice
           "suivant", soit un tuple ou une liste de 2 éléments, `(nom, link_label)`, où le second
           élément est une étiquette ajoutée sur le lien de @name à @nom dans le graphe.

    @kls:  Formatage ajouté au noeud, si défini. Doit correspondre à une des classDef
           définies pour le graphe mermaid, ou à une de celles définies par défaut dans
          `ParkourMermaid.get_as_mermaid_def`.
    """

    def __init__(self, name:str, *children:ParkourChild, kls:str=""):
        self.name = name
        self.kls = kls.lower()
        self.node = None
        self.node_def = ""
        self.click = ""
        self.children = [
            (child,'') if isinstance(child,str) else ( tuple(child)+('',) )[:2]
            for child in children
        ]

    def update(self, exos:Dict[str,Exercice], nodes:Dict[str,str]):

        self.node = nodes[self.name]
        name = self.name

        if self.name in exos:
            exo  = exos[self.name]
            name = exo.title
            self.click = f'\n    click { self.node } "../{ exo.page_url }"'

            if not self.kls:
                tags = [*filter(('ep1','ep2','supérieur').__contains__, exo.tags)]
                if len(tags) > 1:
                    raise BuildError(
                        f"L'exercice { exo.page_url } contient des tags incompatibles : "
                        f"{ ', '.join(tags) }.\nSeul un des tags ep1, ep2 et sup peut être "
                        "utilisé pour un exercice."
                    )
                self.kls = ''.join(tags).replace('supérieur', 'sup')

        self.node_def = f'\n    { self.node }["{ name }"]{ self.kls and ":::"+self.kls }'

    def gen_node(self):
        yield self.node_def
        yield self.click

    def gen_links(self, nodes:Dict[str,'PNode']):
        for child,label in self.children:
            other = nodes[child]
            label = label and f"|{ label }|"
            yield f"\n    { self.node } -->{ label } { other.node }"




@dataclass
class ParkourMermaid:
    """
    Helper class: represent the full set of data needed to build a mermaid graph representing
    a "parcours" (for the related page).
    """

    chart: str
    allow_non_exos: bool
    graph: List[PNode]
    defs: Dict[str,Dict[str,str]]

    #----------------------------------------

    indent: str
    EXOS: Dict[str,str]
    nodes_dct: Dict[str,str] = None

    DEFAULT_CLASSES: ClassVar[Set[str]] = set('ep1 ep2 sup'.split())

    def __post_init__(self):
        self.nodes_dct = {
            node.name: f"N{i}" for i,node in enumerate(self.graph)
        }
        for node in tuple(self.graph):
            for child,_ in node.children:
                if child not in self.nodes_dct:
                    self.nodes_dct[child] = f"N{ len(self.nodes_dct) }"
                    self.graph.append(PNode(child))
        self._validate_call()


    def _validate_call(self):
        """
        Apply the following verifications and raise BuildError if something doesn't match.
        For each PNode object:
            - The exercice name (holding folder) has to exist (unless `allow_non_exos` is True).
            - if a `@kls` is given, it must match an existing `classDef` of the graph.
            - The children exercices name (holding folder) have to exist (unless `allow_non_exos`).
        """
        error_msg = []

        for node in self.graph:
            tmp = []
            wrong_exo_name  = node.name not in self.EXOS and not self.allow_non_exos
            wrong_class_def = node.kls and not (
                node.kls in self.DEFAULT_CLASSES
                or "classDef" in self.defs and node.kls in self.defs["classDef"]
            )

            if wrong_exo_name:  tmp.append("unknown exercice!")
            if wrong_class_def: tmp.append(f"\n    Unknown class: {node.kls}")
            tmp.extend(
                f"\n    Unknown child ex: {child}"
                for child,_ in node.children
                if child not in self.EXOS and not self.allow_non_exos
            )

            if tmp:
                tmp[0] += f"\nFor PNode({node.name!r}): { tmp[0] }"
                error_msg.extend(tmp)

        if error_msg:
            raise BuildError(f"Invalid parcours_mermaid arguments:\n{ ''.join(error_msg) }")


    def get_as_mermaid_def(self, exos:Dict[str,Exercice]):
        """
        Generate the mermaid graph definition, with all the required logistic to insert
        the IDE buttons later.
        """
        lines = [
            f"\n    { def_name } { kls } { formatting }"
                for def_name, dct in self.defs.items()
                for kls, formatting in dct.items()
        ]

        for node in self.graph:
            node.update(exos, self.nodes_dct)
            lines.extend( node.gen_node() )

        p_nodes_dct = {node.name: node for node in self.graph}
        for node in self.graph:
            lines.extend( node.gen_links(p_nodes_dct) )

        graph = f"""
flowchart { self.chart }

    classDef ep1 stroke:#00BD00,stroke-width:2px
    classDef ep2 stroke:#FDBD27,stroke-width:2px
    classDef sup stroke:#FE1000,stroke-width:2px
    classDef default font-size:15px

    linkStyle default font-size:15px

{ ''.join(lines) }

    class { ','.join(n.node for n in self.graph) } parkour_item
"""
        return graph.strip().replace('\n', '\n'+self.indent)








def insert_mermaid_parcours_graphs( page:Page, name_to_exo:Dict[str,Exercice] ):
    """
    Mutate page.content, replacing all the PARKOUR_MERMAID_REPL_TOKEN strings with
    the corresponding mermaid graph definition (insertion is based on the order of
    the declarations in the page).
    """
    graphs = iter(PARKOUR_MERMAIDS)

    with_graphs = re.sub(
        PARKOUR_MERMAID_REPL_TOKEN,
        lambda _: next(graphs).get_as_mermaid_def(name_to_exo),
        page.content
    )
    page.content = with_graphs





def insert_mermaid_parcours_graphs_hidden_icons(
    page: Page,
    name_to_exo: Dict[str,Exercice],
    env: PyodideMacrosPlugin
):
    """
    Mutate page.content, adding an hidden div at the end holding the buttons for all the IDEs
    af a given exercice. These can by cloned and moved to the related graph once they are built
    in the final page.
    """
    pages_divs = {}
    for pkm in PARKOUR_MERMAIDS:
        for node in pkm.graph:
            if node.name in pages_divs or node.name not in name_to_exo:
                continue
            exo = name_to_exo[node.name]
            ides_buttons = insert_validations(env, exo.page_url, 'div', btns_for=node.name)
            pages_divs[node.name] = ides_buttons

    page.content += Html.div(
        ''.join(pages_divs.values()), id="hidden-ides-buttons", style="display:none"
    )
