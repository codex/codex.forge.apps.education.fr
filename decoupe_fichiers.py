#!/usr/bin/python
# -*- coding: utf-8 -*-
# Filename: decoupe_fichiers2.py

import re
import os
import sys


# Définit les suffixe des fichiers utilisés pour les IDE avec deux versions :
VIDE,TROU = "vide","trou"
# VIDE,TROU = "a","b"


# Pour identifier les différentes parties pour un exo unique
identifie_partie = re.compile('^# *--- *(?P<partie>[^ ]+) *--- *# *$')

# La même chose, mais au cas où il y a plusieurs exos
identifie_partie_et_suffixe = re.compile(
    '^# *--- *(?P<partie>[^, ]+) *, *(?P<suffixe>.+) *--- *# *$')

# Pour mettre le code dans plusieurs exos avec des parties différentes
# On suppose qu'il n'y a pas plus de 2 parties du genre :
# --- exo, 1 | hdr, 1 --- #
identifie_parties_et_suffixes = re.compile(
    r'^# *--- *(?P<partie1>[^, ]+) *, *(?P<suffixe1>[^|]+) *\| *(?P<partie2>[^, ]+) *, *(?P<suffixe2>.+) *--- *# *$')


# Pour détecter les ligne à ignorer
identifie_ligne_a_ignorer = re.compile("^(\'\'\'|\"\"\") *# *(?P<action>.*)$")
mots_pour_ignorer = ['skip', 'ignorer', 'a ignorer', 'à ignorer']


#noms_parties = ['entete', 'exo', 'hdr', 'corr', 'tests', 'secrets', 'rem']

class Partie:
    """
    Classe qui permet de gérer une partie (hdr, exo, corr...)
    nom : Nom de la partie (hdr, exo, corr...)
    separateur : Code qui est ajouté au début. Par exemple "# tests" au début de la partie tests.
    repeter_separateur : Indique s'il faut remettre le séparateur à la fin du code, comme pour HDR.
    """

    def __init__(self, nom, separateur, repeter_separateur=False):
        self.nom = nom
        if separateur != '':
            self.separateur = separateur + '\n'
        else:
            self.separateur = ''
        self.separateur_deja_ajoute = False
        self.repeter_separateur = repeter_separateur
        self.contenu = [self.separateur]

    def ajoute_contenu(self, contenu):
        """
        Rajoute le contenu qu'on vient de trouver pour cette partie.
        Si on voit revenir un nom de partie qu'on a déjà vu, on le rajoute à la suite de l'autre.
        """
        self.contenu.append(contenu)

    def est_definie(self):
        """
        Permet de savoir si on a définie cette partie dans le fichier.
        Il suffit de regarder s'il y a plus que le séparateur initial
        """
        return len(self.contenu) > 1

    def rendu(self):
        """
        Génère le code qui ira dans le fichier.
        """
        if self.est_definie():
            if self.repeter_separateur and not self.separateur_deja_ajoute:
                # On remet le séparateur à la toute fin
                self.contenu.append(self.separateur)
                self.separateur_deja_ajoute = True
            return ''.join(self.contenu)
        else:
            return ''

    def rendu_separateur(self):
        """
        Génère juste le séparateur si la partie n'est pas vide.
        """
        if self.est_definie():
            # on ne tient pas compte des séparateurs à mettre à la fin
            return ''.join(self.contenu[0])
        else:
            return ''

    def rendu_contenu(self):
        """
        Génère le code sans le séparateur initial si la partie n'est pas vide.
        """
        if self.est_definie():
            if self.repeter_separateur and not self.separateur_deja_ajoute:
                # On remet le séparateur à la toute fin
                self.contenu.append(self.separateur)
                self.separateur_deja_ajoute = True
            return ''.join(self.contenu[1:])
        else:
            return ''




def fait_parties():
    """
    Construit le dictionnaire qui contient toutes les parties qu'on va rechercher dans le fichier initial.
    """
    dico_parties = {'entete': Partie('entete', ''),
            'exo': Partie('exo', '# --------- PYODIDE:code --------- #'),
            'env': Partie('env', '# --------- PYODIDE:env --------- #'),
            'corr': Partie('corr', '# --------- PYODIDE:corr --------- #'),
            'tests': Partie('tests', '# --------- PYODIDE:tests --------- #'),
            'secrets': Partie('secrets', '# --------- PYODIDE:secrets --------- #'),
            'vide': Partie('vide', '# --------- PYODIDE:code --------- #'),
            'post': Partie('post', '# --------- PYODIDE:post --------- #'),
            'env_term': Partie('env_term', '# --------- PYODIDE:env_term --------- #'),
            'post_term': Partie('post_term', '# --------- PYODIDE:post_term --------- #'),
            'ignore': Partie('ignore', '# --------- PYODIDE:ignore --------- #'),
            'rem': Partie('rem', '')}

    dico_parties['hdr'] = dico_parties['env']  # pour des raisons de retrocompatibilité
    return dico_parties

def scanne_fichier_python(nom_fichier):
    actuels = ['exo']
    suffixes = [['']]
    parties_par_suffixe = dict() #{'': fait_parties()}
    with open(nom_fichier, "r", encoding="utf8") as fichier:
        for ligne in fichier.readlines():
            essai0 = identifie_parties_et_suffixes.match(ligne)
            essai1 = identifie_partie_et_suffixe.match(ligne)
            essai2 = identifie_partie.match(ligne)
            if essai0:
                print(essai0.groupdict())
                # premier groupe  `partie, suffixes`
                actuels = [essai0.groupdict()['partie1'].lower()]
                groupe_suffixes = essai0.groupdict()['suffixe1'].lower()
                suffixes = [ [s.strip() for s in groupe_suffixes.split(",")] ]

                # deuxième groupe  `partie, suffixes`
                actuels.append(essai0.groupdict()['partie2'].lower())
                groupe_suffixes = essai0.groupdict()['suffixe2'].lower()
                suffixes.append([s.strip() for s in groupe_suffixes.split(",")])

                for g_suffixes in suffixes:
                    for suffixe in g_suffixes:
                        if suffixe not in parties_par_suffixe:
                            parties_par_suffixe[suffixe] = fait_parties()
            elif essai1:
                actuels = [essai1.groupdict()['partie'].lower()]
                groupe_suffixes = essai1.groupdict()['suffixe'].lower()
                suffixes = [ [s.strip() for s in groupe_suffixes.split(",")] ]
                for suffixe in suffixes[0]:
                    if suffixe not in parties_par_suffixe:
                        parties_par_suffixe[suffixe] = fait_parties()
            elif essai2:
                actuels = [essai2.groupdict()['partie'].lower()]
                suffixe = ''
                if suffixe not in parties_par_suffixe:
                    parties_par_suffixe[suffixe] = fait_parties()
            else:
                essai3 = identifie_ligne_a_ignorer.match(ligne)
                ignorer = False
                if essai3 and essai3.groupdict()['action'].strip().lower() in mots_pour_ignorer:
                    ignorer = True
                if not ignorer:
                    for i, actuel in enumerate(actuels):
                        for suffixe in suffixes[i]:
                            parties_par_suffixe[suffixe][actuel].ajoute_contenu(ligne)
    return parties_par_suffixe


def prepare_rendu(parties_par_suffixe):
    """
    Génère un dictionnaire qui associe à chaque suffixe les rendus qui lui correspondent.
    """
    rendus_par_suffixe = dict()
    for suffixe in parties_par_suffixe:
        parties = parties_par_suffixe[suffixe]
        if parties['vide'].est_definie():
            # On va créer la version à trous et la version sans trous
            # La version "a", c'est la version avec le code minimal.
            # La version "b", c'est la version avec le code à trou.
            rendus_par_suffixe[suffixe + VIDE] = {nom: parties[nom].rendu() for nom in parties if nom not in ['exo', 'vide']}
            rendus_par_suffixe[suffixe + VIDE]['exo'] = parties['vide'].rendu()
            rendus_par_suffixe[suffixe + VIDE]["exo_sep"] = parties["vide"].rendu_separateur()
            rendus_par_suffixe[suffixe + VIDE]["corr_sep"] = parties["corr"].rendu_separateur()
            rendus_par_suffixe[suffixe + VIDE]["exo_cont"] = parties["vide"].rendu_contenu()
            rendus_par_suffixe[suffixe + VIDE]["corr_cont"] = parties["corr"].rendu_contenu()
            #rendus_par_suffixe[suffixe + VIDE]["post_sep"] = parties["post"].rendu_separateur()
            #rendus_par_suffixe[suffixe + VIDE]["post_cont"] = parties["post"].rendu_contenu()

            rendus_par_suffixe[suffixe + TROU] = {nom: parties[nom].rendu() for nom in parties if nom != 'vide'}
            rendus_par_suffixe[suffixe + TROU]["exo_sep"] = parties["exo"].rendu_separateur()
            rendus_par_suffixe[suffixe + TROU]["corr_sep"] = parties["corr"].rendu_separateur()
            rendus_par_suffixe[suffixe + TROU]["exo_cont"] = parties["exo"].rendu_contenu()
            rendus_par_suffixe[suffixe + TROU]["corr_cont"] = parties["corr"].rendu_contenu()
            #rendus_par_suffixe[suffixe + TROU]["post_sep"] = parties["post"].rendu_separateur()
            #rendus_par_suffixe[suffixe + TROU]["post_cont"] = parties["post"].rendu_contenu()
        else:
            rendus_par_suffixe[suffixe] = {nom:  parties[nom].rendu() for nom in parties}
            rendus_par_suffixe[suffixe]["exo_sep"] = parties["exo"].rendu_separateur()
            rendus_par_suffixe[suffixe]["corr_sep"] = parties["corr"].rendu_separateur()
            rendus_par_suffixe[suffixe]["exo_cont"] = parties["exo"].rendu_contenu()
            rendus_par_suffixe[suffixe]["corr_cont"] = parties["corr"].rendu_contenu()
            #rendus_par_suffixe[suffixe]["post_sep"] = parties["post"].rendu_separateur()
            #rendus_par_suffixe[suffixe]["post_cont"] = parties["post"].rendu_contenu()
    return rendus_par_suffixe

def genere_fichiers(rendus_par_suffixe, chemin, verbose=True):
    for suffixe in rendus_par_suffixe:
        prefixe = 'exo'
        if suffixe != '':
            prefixe = f'{prefixe}_{suffixe}'
        #print(f"** {prefixe} **")
        prefixe = os.path.join(chemin, prefixe)
        rendus = rendus_par_suffixe[suffixe]
        exo = rendus['ignore'] + rendus['env'] + rendus['env_term'] + rendus['exo_sep'] + rendus['entete'] + rendus['exo_cont']
        if rendus['corr_sep'] != '':
            exo += rendus['corr_sep'] + rendus['entete'] + rendus['corr_cont']
        exo += rendus['tests'] + rendus['secrets'] + rendus['post'] + rendus['post_term']
        #print(rendus)
        rem = rendus['rem']
        with open(f'{prefixe}.py', 'w', encoding='utf8') as fichier:
            fichier.write(exo)
            if verbose:
                print(f'{prefixe}.py')
        if len(rem) > 0:
            with open(f'{prefixe}_REM.md', 'w', encoding='utf8') as fichier:
                fichier.write(rem)
                if verbose:
                    print(f'{prefixe}_REM.md')

def decoupage(cible):
    if cible[-3:] != ".py" and os.path.exists(cible) and os.path.isdir(cible):
        # On nous a donné un dossier.
        # On va regarder sujet.py
        cible = os.path.join(cible, "sujet.py")
    if not os.path.exists(cible):
        print("On ne trouve pas le fichier {cible}")
        return 1
    print(f"on va decouper {cible}")
    chemin_absolu = os.path.abspath(cible)

    parties_par_suffixe = scanne_fichier_python(cible)
    rendus = prepare_rendu(parties_par_suffixe)
    genere_fichiers(rendus, os.path.dirname(chemin_absolu))
    return 0

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage : ./decoupe_fichiers2.py CIBLE [CIBLE]*")
        print("Action : découpe les fichiers CIBLE en fichiers pour mkdocs-pyodide")
        print("         CIBLE peut aussi être un dossier et on va regarder CIBLE/sujet.py")
        print("On peut aussi importer ce fichier et utiliser :")
        print("decouper_fichiers.decoupage(CIBLE)")
    else:
        for cible in sys.argv[1:]:
            decoupage(cible)