
# pylint: disable=E0602, E0401

import js, time
from typing import Any, Callable, List
from contextlib import asynccontextmanager
from itertools import zip_longest




# Exemple d'utilisation :
#       https://forge.apps.education.fr/codex/codex.forge.apps.education.fr/-/blob/main/docs/en_travaux/verification_ABR_2/exo.py?ref_type=heads
def build_announcer(key:str, src_format=None):
    def announcer(title:str, format=None):
        return annonce(key, title, format=format or src_format)
    return announcer



@asynccontextmanager
async def annonce(key:str, title:str, format=None):
    terminal_message(key, title+': ', new_line=False, format=format or 'none')
    await js.sleep()

    try:
        yield
        terminal_message(key, "OK!", format="success")
    except:
        terminal_message(key, "")       # consume current line
        raise
    finally:
        await js.sleep()



# Exemple d'utilisation :
#       https://forge.apps.education.fr/codex/codex.forge.apps.education.fr/-/blob/main/docs/en_travaux/verification_ABR_2/exo.py?ref_type=heads
async def timer(
    title: str,
    func_to_time: Callable,
    lst_of_calls_args: List[Any],
    *,
    key:str = None,
    deconstruct_args = False,
    lst_of_expected: List[Any] = None,
    assert_func = lambda act,exp: act == exp,
    leading_new_line = True,
    n_measures = 5,
    time_rounding = 3,
    feedback = True,
):
    """
    @key:               The STD_KEY argument value, to use with `terminal_message`.
    @deconstruct_args:  If True, @lst_of_calls_args contains tuples or list of args to deconstruct
                        when calling func_to_time
    @lst_of_expected:   If given, a list of the expected values. The size has to match the one of
                        @lst_of_calls_args.
    @n_measures:        Number of calls to apply to time one call. Both extrema are ignored, and
                        the remaining values are meaned. If @n_measures is less than 3, just mean
                        the value.
    @time_rounding:     Number of decimals to keep in the resulting time value.
    """


    def timing(args, exp):
        ts = []
        for _ in range(n_measures):
            start  = time.time()
            actual = func_to_time(*args) if deconstruct_args else func_to_time(args)
            stop   = time.time()
            ts.append(stop-start)

            if lst_of_expected:
                assert assert_func(actual, exp)

        if n_measures > 2:              # Exclude min and max
            _,*ts,_ = sorted(ts)
        return round( sum(ts)/len(ts), time_rounding)


    if lst_of_expected is not None and len(lst_of_expected) != len(lst_of_calls_args):
        raise ValueError(f"@lst_of_calls_args and @lst_of_expected should have the same length")

    if leading_new_line and title:
        title = '\n'+title

    if title and feedback:
        terminal_message(key, title+' ', new_line=False)

    timings = []
    for args, exp in zip_longest(lst_of_calls_args, lst_of_expected or [], fillvalue=None):
        t = timing(args, exp)
        timings.append(t)
        if feedback:
            terminal_message(key, ".", new_line=False)
            await js.sleep()

    if feedback:
        terminal_message(key, "")

    return timings
