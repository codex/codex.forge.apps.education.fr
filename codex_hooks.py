
from typing import Dict
from jinja2 import Environment

from mkdocs.structure.files import Files, File, InclusionLevel
from mkdocs.exceptions import BuildError
from mkdocs.config.defaults import MkDocsConfig
from mkdocs.structure.nav import Navigation
from mkdocs.structure.pages import Page
from mkdocs.plugins import event_priority

from codex_macros.recherche import (
    DIFFICULTY_CATEGORIES,
    ExByCategory,
    Exercice,
    dico_exos,
    exercices_aleatoires,
    recherche,
)
from codex_macros.ides_tracking import handle_ides_tracking
from codex_macros.parcours import insert_mermaid_parcours_graphs, insert_mermaid_parcours_graphs_hidden_icons





NAME_TO_EXO: Dict[str,Exercice] = {}

EXOS_BY_CATEGORIES: ExByCategory = {}
""" All the Exercice objects (Based on PMT MacroData objects), by category """

HOME      = 'index.md'
PARKOUR   = 'parcours.md'
RECHERCHE = 'recherche.md'

TO_BUILD_ON_CTX = (HOME, PARKOUR, RECHERCHE)
PAGES_UPDATES_OK = {}
""" Keep track of the pages that got updated or not (enforce contract) """


REDIRECTIONS = [            # exercices/A/ -> exercices/B/ uniquement !
    # ('huit_dames*', 'huit_reines*')
]



# Use a flag to bypass the contracts on IDE_TRACKING_OK during dirty serve:
def on_startup(command, dirty:bool):
    global is_dirty

    is_dirty = dirty




def on_files(files: Files, /, *, config: MkDocsConfig):

    # HACK-de-la-mort-qui-tue: change material's mermaid shadowRoot rendering mode on the fly,
    # # to get access to the shadowRoot content from JS...

    def is_material_bundle(file:File):
        return file.src_uri.startswith('assets/javascripts/bundle.') and file.src_uri.endswith('.min.js')

    bundle = next(filter(is_material_bundle, files))
    code   = bundle.content_string
    i      = code.find("attachShadow")
    j      = code.find("closed", i) + len("closed") + 1
    code   = f'{ code[:i] }attachShadow({ "{" }mode:"open"{ code[j:] }'
    bundle.content_string = code
    bundle.content_bytes  = bytes(code, encoding='utf-8')




def on_env(env:Environment, config:MkDocsConfig, files:Files):
    """
    Register extra functions to use in jinja templates rendering only.
    (This means all these functions have to return html instead of markdown,
    and they will only be used to render templates/"pages contexts").
    """
    global EXOS_BY_CATEGORIES, NAME_TO_EXO, PAGES_UPDATES_OK

    # Extract all the informations from PMT plugin.all_macros_data:
    EXOS_BY_CATEGORIES = dico_exos(config.plugins['pyodide_macros'])

    NAME_TO_EXO = {
        exo.folder_name: exo
            for lst_exos in EXOS_BY_CATEGORIES.values()
            for exo in lst_exos
    }

    PAGES_UPDATES_OK = {file: False for file in TO_BUILD_ON_CTX}


    # Add mkdocs env functions to handle licences and tags in each page (templates rendering)
    env.globals.update({ func.__name__:func for func in [
        build_license_icons,
        build_difficulty_tag,
        check_data,
    ]})


    # Redirections:     /en_travaux/blabla/* /exercices/blabla/:splat 301
    #                   (NOTE: doesn't work locally)
    ex_size = len('exercices')
    redirections = [
        f"/en_travaux{ exo.page_url[ex_size:] }* /{ exo.page_url }:splat 301"
            for exo in NAME_TO_EXO.values()
            if exo.is_validated
    ] + [
        f"/exercices/{ init }/* /exercices/{ changed }/:splat 301"
        for init,changed in REDIRECTIONS
    ]

    files.append(File.generated(
        config, '_redirects', content="\n".join(redirections), inclusion=InclusionLevel.NOT_IN_NAV
    ))




@event_priority(-2000)
def on_page_context(ctx, page:Page, *, config:MkDocsConfig, nav:Navigation):
    """
    Low priority to "mutate last", if the current page is the parcours.md one.
    If the given page contains global infos, build the related html and add it to page.content.
    """

    name = page.file.src_uri
    if name not in PAGES_UPDATES_OK:
        return

    # Ensure contract:
    if PAGES_UPDATES_OK[name]:
        raise BuildError(f"Found a second {name!r} file. There should be one only...")

    PAGES_UPDATES_OK[name] = True
    env = config.plugins['pyodide_macros']

    if name == HOME:
        exercices_aleatoires(env, EXOS_BY_CATEGORIES, page)

    elif name == RECHERCHE:
        recherche(env, EXOS_BY_CATEGORIES, page)
        handle_ides_tracking(page, env, wraps_tag=None)

    elif name == PARKOUR:
        insert_mermaid_parcours_graphs(page, NAME_TO_EXO)
        insert_mermaid_parcours_graphs_hidden_icons(page, NAME_TO_EXO, env)
        handle_ides_tracking(page, env, wraps_tag='span')   # TO DO LAST

    else:
        # Ensure no missing implementation branch
        PAGES_UPDATES_OK[name] = False




def on_post_build(config:MkDocsConfig):
    """
    Enforce contract about pages showing IDEs trackers.
    """
    if is_dirty:
        return

    missing = ', '.join(name for name,done in PAGES_UPDATES_OK.items() if not done)
    if missing:
        raise BuildError(
            f"Couldn't find the file{ 's'*(len(missing)>1) }: '{missing}'.\n"
            f"Please update the IDE_TRACKING_OK dict in { __file__ }."
        )




#-------------------------------------------------------------------




def check_data(page):
    """ (debugging purpose) """
    if 'author' not in page.meta and 'tags' in page.meta:
        print(page.url)
    return ""




def build_difficulty_tag(meta:dict):

    has_diff = 'difficulty' in meta
    i = meta['difficulty'] // 100 if has_diff else -1
    kls_name, msg = DIFFICULTY_CATEGORIES[i]
    if not has_diff:
        msg = "difficulté non renseignée"

    span = f'<span class="md-tag { kls_name }">{ msg }</span>'
    return span





DEFAULT_LICENSE  = 'by-sa'
LICENCE_TEMPLATE = '<img src="https://mirrors.creativecommons.org/presskit/icons/{}.svg?ref=chooser-v1">'
TAGS_TEMPLATE    = '<a class="md-tag" href="https://creativecommons.org/licenses/{}/4.0/deed.fr" target="_blank" rel="license noopener noreferrer">{}</a>'


def build_license_icons(meta:dict):
    """
    NOTE: The CC part stands for "creative commons", so the picture must always be there,
          but it's not necessary in the acronym.
    """

    exo_license: str = meta.get('license', DEFAULT_LICENSE)
    exo_license = exo_license.lower()

    use_cc = [] if 'cc' in exo_license else ['cc']
    items  = use_cc + exo_license.split('-')
    icons  = ''.join(map(LICENCE_TEMPLATE.format, items))
    return TAGS_TEMPLATE.format(exo_license, icons)
